package pvt.auna.fcompleja.service;

import java.util.ArrayList;
import java.util.List;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.CheckListPacPrefeInstiRequest;
import pvt.auna.fcompleja.model.api.request.CheckListRequisitoRequest;
import pvt.auna.fcompleja.model.api.request.ConfiguracionMarcadorRequest;
import pvt.auna.fcompleja.model.api.request.ContinuadorDocumentoRequest;
import pvt.auna.fcompleja.model.api.request.ContinuadorRequest;
import pvt.auna.fcompleja.model.api.request.EnvioCorreoRequest;
import pvt.auna.fcompleja.model.api.request.InformeSolEvaReporteRequest;
import pvt.auna.fcompleja.model.api.request.LineaTrataPrefeInstiRequest;
import pvt.auna.fcompleja.model.api.request.LineaTratamientoRequest;
import pvt.auna.fcompleja.model.api.request.ListaBandejaRequest;
import pvt.auna.fcompleja.model.api.request.ListaIndicadorRequest;
import pvt.auna.fcompleja.model.api.request.RegistrarAntecedenteRequest;
import pvt.auna.fcompleja.model.api.request.RegistroHistLineaTratRequest;
import pvt.auna.fcompleja.model.api.request.RptaBasalMarcadorRequest;
import pvt.auna.fcompleja.model.api.request.RptaEvaLiderTumorRequest;
import pvt.auna.fcompleja.model.api.request.SolbenRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.CondicionBasalPacienteRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.ProgramacionCmacRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.ResultadoBasalRequest;
import pvt.auna.fcompleja.model.api.request.seguridad.UsuarioRequest;
import pvt.auna.fcompleja.model.api.response.DocuHistPacienteResponse;
import pvt.auna.fcompleja.model.api.response.EvaluacionAutorizadorResponse;
import pvt.auna.fcompleja.model.api.response.HistLineaTratResponse;
import pvt.auna.fcompleja.model.api.response.ListaCasosCmacResponse;
import pvt.auna.fcompleja.model.api.response.MedicamentoContinuadorResponse;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.model.api.response.PreferenciaInstitucionalResponse;
import pvt.auna.fcompleja.model.api.response.RptaBasalMarcadorResponse;
import pvt.auna.fcompleja.model.bean.ParticipanteBean;

public interface DetalleSolicitudEvaluacionService {
	
	public ApiOutResponse consultarInformacionScgEva(InformeSolEvaReporteRequest request, AfiliadosRequest pacienteRequest);
	
	public HistLineaTratResponse consultarHistLineaTrat(LineaTratamientoRequest listaTrata);
	
	public ApiOutResponse insertarHistLineaTrat(RegistroHistLineaTratRequest request);

	public ApiOutResponse insertarCondicionBasalPaciente(CondicionBasalPacienteRequest request);
	
	public List<EvaluacionAutorizadorResponse> consultarEvaluacionAutorizador(SolbenRequest request);
	
	public ApiOutResponse insertarLineaTratamiento(LineaTrataPrefeInstiRequest request);
	
	public PreferenciaInstitucionalResponse insertarPreferenciaInsti( Integer codGrpDiag, Integer condicionCancer, Integer lineaTrata,
			Integer subCondicionCancer, Integer codMac);

	public ApiOutResponse actualizarCheckListGuardarDocumento(CheckListRequisitoRequest request);
	
	//GUARDAR O ACTUALIZAR CARGA DE ARCHIVO PASO 2
	public ApiOutResponse insActCheckListReqDocumento(CheckListRequisitoRequest request);
	
	public ApiOutResponse actualizarEliminacionDocumento(CheckListRequisitoRequest request);
	
	public ApiOutResponse actualizarDescargaDocumento(CheckListRequisitoRequest request);
	
	public ApiOutResponse insertarCargaDocumento(SolicitudEvaluacionRequest request);
	
	public DocuHistPacienteResponse consultarCheckListRequisito(CheckListRequisitoRequest request);
	
	public ApiOutResponse consultarCheckListPacPrefeInsti(SolicitudEvaluacionRequest request);
	
	public ApiOutResponse insActCheckListPacPrefeInsti(CheckListPacPrefeInstiRequest request);
	
	public ApiOutResponse insertarConfiguracionMarcador( ConfiguracionMarcadorRequest request);

	public List<RptaBasalMarcadorResponse> consultarRptaBasalMarcador(RptaBasalMarcadorRequest request);

	public ApiOutResponse insertarRptaEvaLiderTumor(RptaEvaLiderTumorRequest request);
	
	public ApiOutResponse insActMedicamentoContinuador(ContinuadorRequest request);
	
	public ApiOutResponse actualizarContinuadorDocumento(ContinuadorDocumentoRequest request);
	
	public MedicamentoContinuadorResponse consultarMedicamentoContinuador(ContinuadorRequest request);

	public ApiOutResponse consultarIndicador( ListaIndicadorRequest request);

	public ApiOutResponse insertarCheckListPaciente( ListaIndicadorRequest listaRequest);
	
	public ListaCasosCmacResponse listarEvaluacionCmac( InformeSolEvaReporteRequest requestCodSol);
	
	public ListaCasosCmacResponse registrarProgramacionCmac( ProgramacionCmacRequest requestfech);
	
	public ApiOutResponse consultarResultadoBasal(ResultadoBasalRequest request);
	
	public ApiOutResponse consultarSubcondicionCancer(LineaTrataPrefeInstiRequest request);
	
	public ApiOutResponse consultarPreferenciaInsti(LineaTrataPrefeInstiRequest request);
	
	public ApiOutResponse conInsActPreferenciaInstiPre(LineaTrataPrefeInstiRequest request);
	
	public ApiOutResponse insertarCheckListPaciente2(LineaTrataPrefeInstiRequest request);
	
	public ApiOutResponse consultarEvaluacionLiderTumor(ParticipanteBean request);
	
	public ApiOutResponse consultarSeguimiento(ListaBandejaRequest request);
	
	public ApiOutResponse consultarDocumentoContinuador(CheckListRequisitoRequest request);
	
	public ApiOutResponse insActContinuadorDocumentoCargar(CheckListRequisitoRequest request);
	
	public ApiOutResponse actContinuadorDocumentoElim(CheckListRequisitoRequest request);
	
	public ApiOutResponse actualizarCodigoEnvio(EnvioCorreoRequest request);
	
	public ApiOutResponse listarCodDocumentosChecklist(InformeSolEvaReporteRequest request);
	
	public ApiOutResponse actualizarTipoSolEvaluacion(SolicitudEvaluacionRequest request);
	
	public ApiOutResponse actualizarEvaluacionInformeAutorizador(SolicitudEvaluacionRequest request);
	
	public OncoWsResponse consultarUsuarioRol(UsuarioRequest request) throws Exception;
	
	public ApiOutResponse consultarInformAutorizCmac(ListaBandejaRequest request);

	public ApiOutResponse registrarAntedecentesPaciente(ArrayList<RegistrarAntecedenteRequest> request);
}
