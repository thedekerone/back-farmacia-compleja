import { Component, OnInit, Inject, forwardRef, ViewChild } from '@angular/core';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MatPaginatorIntl,
  MAT_DATE_LOCALE,
  MatDialogRef,
  MatDialog,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatSort,
  MatPaginator
} from '@angular/material';
import { MatPaginatorIntlEspanol } from 'src/app/directives/matpaginator-translate';
import { MY_FORMATS_AUNA, GRUPO_PARAMETRO, ESTADO_SEGUIMIENTO, ESTADO_MONITOREO, MENSAJES, ACCESO_MONITOREO } from 'src/app/common';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MessageComponent } from 'src/app/core/message/message.component';
import { DatePipe } from '@angular/common';
import { WsResponse } from 'src/app/dto/WsResponse';
import { date } from 'ngx-custom-validators/src/app/date/validator';
import { BandejaMonitoreoService } from 'src/app/service/BandejaMonitoreo/bandeja.monitoreo.service';
import { ListaParametroservice } from 'src/app/service/lista.parametro.service';
import { ParametroRequest } from 'src/app/dto/ParametroRequest';
import { UsuarioService } from 'src/app/dto/service/usuario.service';
import { SegEjecutivoRequest } from 'src/app/dto/request/BandejaMonitoreo/SegEjecutivoRequest';
import { SegEjecutivoResponse } from 'src/app/dto/response/BandejaMonitoreo/SegEjecutivoResponse';
import { MonitoreoResponse } from 'src/app/dto/response/BandejaMonitoreo/MonitoreoResponse';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BOpcionMenuLocalStorage } from 'src/app/dto/core/BOpcionMenuLocalStorage';

export interface DataDialog {
  title: string;
  monitoreo: MonitoreoResponse;
  tipo: number;
}

@Component({
  selector: 'app-seguimiento-ejecutivo',
  templateUrl: './seguimiento-ejecutivo.component.html',
  styleUrls: ['./seguimiento-ejecutivo.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS_AUNA },
    { provide: MatPaginatorIntl, useClass: forwardRef(() => MatPaginatorIntlEspanol) }
  ]
})
export class SeguimientoEjecutivoComponent implements OnInit {
  segEjecutivoFrmGrp: FormGroup = new FormGroup({
    codMonitoreoFrmCtrl: new FormControl(null),
    ejecutMonitoreoFrmCtrl: new FormControl(null),
    fecRegistroFrmCtrl: new FormControl(null),
    pEstSeguimientoFrmCtrl: new FormControl(null, [Validators.required]),
    detalleEventoFrmCtrl: new FormControl(null, [Validators.required])
  });
  get codMonitoreoFrmCtrl() { return this.segEjecutivoFrmGrp.get('codMonitoreoFrmCtrl'); }
  get ejecutMonitoreoFrmCtrl() { return this.segEjecutivoFrmGrp.get('ejecutMonitoreoFrmCtrl'); }
  get fecRegistroFrmCtrl() { return this.segEjecutivoFrmGrp.get('fecRegistroFrmCtrl'); }
  get pEstSeguimientoFrmCtrl() { return this.segEjecutivoFrmGrp.get('pEstSeguimientoFrmCtrl'); }
  get detalleEventoFrmCtrl() { return this.segEjecutivoFrmGrp.get('detalleEventoFrmCtrl'); }
  cmbEstadoSeguimiento: any[];
  json:JSON;

  dataSource: MatTableDataSource<SegEjecutivoResponse>;
  listaSegEjecutivo: SegEjecutivoResponse[];
  isLoading: boolean;
  displayedColumns: string[];

  opcionMenu: BOpcionMenuLocalStorage;
  valorMostrarOpcion = ACCESO_MONITOREO.mostrarOpcion;

  txtCodTareaMonit: number;
  txtEjecutivoMonit: number;
  txtFechaRegistro: number;
  cbEstadoSeguimiento: number;
  txtDetalleEvento: number;
  btnGrabar: number;
  btnSalir: number;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  columnsGrilla = [{
    columnDef: 'nomEjecutivoMonitoreo',
    header: 'EJECUTIVO DE MONITOREO',
    cell: (segEjecutivo: SegEjecutivoResponse) => `${segEjecutivo.nomEjecutivoMonitoreo}`
  }, {
    columnDef: 'detalleEvento',
    header: 'DETALLE DEL EVENTO',
    cell: (segEjecutivo: SegEjecutivoResponse) => `${segEjecutivo.detalleEvento}`
  }, {
    columnDef: 'fechaRegistro',
    header: 'FECHA REGISTRO',
    cell: (segEjecutivo: SegEjecutivoResponse) => this.datePipe.transform(segEjecutivo.fechaRegistro, 'dd/MM/yyyy')
  }, {
    columnDef: 'horaRegistro',
    header: 'HORA REGISTRO',
    cell: (segEjecutivo: SegEjecutivoResponse) => `${segEjecutivo.horaRegistro}`
  }, {
    columnDef: 'descEstadoSeguimiento',
    header: 'ESTADO DEL CASO',
    cell: (segEjecutivo: SegEjecutivoResponse) => `${segEjecutivo.descEstadoSeguimiento}`
  }];

  public columnsGrillaCasos = [{
    codAcceso: ACCESO_MONITOREO.tablaSegEjecutivo.indice,
    columnDef: 'no',
  },{
    codAcceso: ACCESO_MONITOREO.tablaSegEjecutivo.ejecutivoMonitoreo,
    columnDef: 'nomEjecutivoMonitoreo',
  },{
    codAcceso: ACCESO_MONITOREO.tablaSegEjecutivo.detalleEvento,
    columnDef: 'detalleEvento',
  },{
    codAcceso: ACCESO_MONITOREO.tablaSegEjecutivo.fechaRegistro,
    columnDef: 'fechaRegistro',
  },{
    codAcceso: ACCESO_MONITOREO.tablaSegEjecutivo.horaRegistro,
    columnDef: 'horaRegistro',
  },{
    codAcceso: ACCESO_MONITOREO.tablaSegEjecutivo.estadoCaso,
    columnDef: 'descEstadoSeguimiento',
  }];

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SeguimientoEjecutivoComponent>,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: DataDialog,
    private bandejaMonitoreoService: BandejaMonitoreoService,
    private parametroService: ListaParametroservice,
    private spinnerService: Ng4LoadingSpinnerService,
    @Inject(UsuarioService) private user: UsuarioService
  ) { }

  ngOnInit() {
    this.inicializarVariables();
    this.accesoOpcionMenu();
  }

  public onClose(): void {
    this.dialogRef.close(null);
  }

  public inicializarVariables() {
    this.dataSource = null;
    this.cmbEstadoSeguimiento = [];
    this.cargarComboEstadoSeguimiento();
    this.codMonitoreoFrmCtrl.setValue(this.data.monitoreo.codigoDescripcionMonitoreo);
    this.ejecutMonitoreoFrmCtrl.setValue(this.user.getApelPaterno + ' ' + this.user.getApelMaterno + ', ' + this.user.getNombres);
    this.fecRegistroFrmCtrl.setValue(new Date());

    this.listarSeguimientoEjecutivo();
  }

  public cargarComboEstadoSeguimiento() {
    let request = new ParametroRequest();
    request.codigoGrupo = GRUPO_PARAMETRO.estadoSeguimiento;//'65';
    this.parametroService.consultarParametro(request).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.cmbEstadoSeguimiento = data.data;
          this.cmbEstadoSeguimiento.unshift({ "codigoParametro": "", "nombreParametro": "SELECCIONE" });
        } else {
          console.error(data);
        }
        //SETEAR VALOR POR DEFECTO
        this.pEstSeguimientoFrmCtrl.setValue('');
      },
      error => {
        console.error('Error al listar parametros');
      }
    );
  }

  public listarSeguimientoEjecutivo(): void {
    this.isLoading = true;
    let request = new SegEjecutivoRequest();
    request.codMonitoreo = this.data.monitoreo.codigoMonitoreo;

    this.bandejaMonitoreoService.listarSeguimientoEjecutivo(request).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.listaSegEjecutivo = data.data;
          this.cargarDatosTabla();
        } else {
          console.error('ERROR:' + data.audiResponse.mensajeRespuesta);
        }
        this.isLoading = false;
      },
      error => {
        console.error('Error no se pudo obtener registros');
        this.isLoading = false;
      }
    );
  }

  public cargarDatosTabla(): void {
    if (this.listaSegEjecutivo.length > 0) {
      this.dataSource = new MatTableDataSource(this.listaSegEjecutivo);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  public crearTablaLineaTratamiento(): void {
    this.displayedColumns = [];

    this.columnsGrillaCasos.forEach(c => {
      this.opcionMenu.opcion.forEach(element => {
        if (c.codAcceso && c.codAcceso === element.codOpcion && Number(element.flagAsignacion) === ACCESO_MONITOREO.mostrarOpcion) {
          this.displayedColumns.push(c.columnDef);
        }
      });
    });
  }

  public regSeguimientoEjecutivo() {
    if (this.segEjecutivoFrmGrp.invalid) {
      this.pEstSeguimientoFrmCtrl.markAsTouched();
      this.detalleEventoFrmCtrl.markAsTouched();
    } else {
      this.spinnerService.show();
      let request = new SegEjecutivoRequest();
      request.codMonitoreo = this.data.monitoreo.codigoMonitoreo;
      request.codEjecutivoMonitoreo = this.user.getCodUsuario;
      request.nomEjecutivoMonitoreo = this.ejecutMonitoreoFrmCtrl.value;
      request.pEstadoSeguimiento = this.pEstSeguimientoFrmCtrl.value;
      request.detalleEvento = this.detalleEventoFrmCtrl.value;
      request.vistoRespMonitoreo = 0;
      request.usuariocrea = this.user.getCodUsuario + '';
      if (request.pEstadoSeguimiento == ESTADO_SEGUIMIENTO.atendido) {//243 => ATENDIDO
        request.pEstadoMonitoreo = ESTADO_MONITOREO.pendienteMonitoreo; // 118 => SE VUELVE A PENDIENTE DE MONITOREO
      }

      this.bandejaMonitoreoService.regSeguimientoEjecutivo(request).subscribe(
        (data: WsResponse) => {
          if (data.audiResponse.codigoRespuesta === '0') {
            this.spinnerService.hide();
            this.dialogRef.close(data.data);
          } else {
            this.spinnerService.hide();
            this.openDialogMensaje('Error al registrar seguimiento ejecutivo', null, true, false, null);
            console.error('ERROR:' + data.audiResponse.mensajeRespuesta);
          }
        },
        error => {
          this.spinnerService.hide();
          this.openDialogMensaje('Error al registrar seguimiento ejecutivo', null, true, false, null);
          console.error('Error registro datos evolucion');
        }
      );
    }
  }

  public openDialogMensaje(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.MONITOREO.TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  public accesoOpcionMenu() {
    const data = require('src/assets/data/permisosRecursos.json');
    const regSeguimientoEjec = data.bandejaMonitoreo.regSeguimientoEjec;
    this.opcionMenu = JSON.parse(localStorage.getItem('opcionMenu'));

    if (this.opcionMenu.opcion.length > 0) {
      this.opcionMenu.opcion.forEach(element => {
        const codOpcion = element.codOpcion;
        switch (codOpcion) {
          case regSeguimientoEjec.txtCodTareaMonit:
            this.txtCodTareaMonit = Number(element.flagAsignacion);
            break;
          case regSeguimientoEjec.txtEjecutivoMonit:
            this.txtEjecutivoMonit = Number(element.flagAsignacion);
            break;
          case regSeguimientoEjec.txtFechaRegistro:
            this.txtFechaRegistro = Number(element.flagAsignacion);
            break;
          case regSeguimientoEjec.cbEstadoSeguimiento:
            this.cbEstadoSeguimiento = Number(element.flagAsignacion);
            break;
          case regSeguimientoEjec.txtDetalleEvento:
            this.txtDetalleEvento = Number(element.flagAsignacion);
            break;
          case regSeguimientoEjec.btnGrabar:
            this.btnGrabar = Number(element.flagAsignacion);
            break;
          case regSeguimientoEjec.btnSalir:
            this.btnSalir = Number(element.flagAsignacion);
            break;
        }
      });
    }

    this.crearTablaLineaTratamiento();;
    // this.displayedColumns.push('item', 'marcador', 'perioMin', 'perioMax', 'sinRegistro', 'resultado', 'fecResultado');
  }
}
