package pvt.auna.fcompleja.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.config.PropiedadesOauth;

@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfigRemoteTokenService extends ResourceServerConfigurerAdapter {


	private static final String TOKEN_BASED_AUTH_ENTRY_POINT = "/api/**";
    private static final String TOKEN_EXCLUDE_REST_PUB = "/pub/*";

	@Autowired
	PropiedadesOauth propiedadesOauth;
	
	@Autowired
	private AseguramientoPropiedades aseProp;


    public OAuth2ResourceServerConfigRemoteTokenService(AseguramientoPropiedades aseProp) {
        this.aseProp = aseProp;
    }

    @Override
    public void configure(final HttpSecurity http) throws Exception {
          //@formatter:off
    			http.csrf().disable()
    					.exceptionHandling().and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
    					.and().authorizeRequests().antMatchers(TOKEN_BASED_AUTH_ENTRY_POINT)
    					.access("#oauth2.hasScope('read') and #oauth2.hasScope('write')") // Protected API End-points
    					.antMatchers(TOKEN_EXCLUDE_REST_PUB,"/credencial/*").permitAll().anyRequest().permitAll();
    	 //@formatter:on
    }

    @Primary
    @Bean
    public RemoteTokenServices tokenServices() {
        final RemoteTokenServices tokenService = new RemoteTokenServices();
        tokenService.setCheckTokenEndpointUrl(aseProp.getOauth2() + "/oauth/check_token");
        tokenService.setClientId(propiedadesOauth.getClientId());
        tokenService.setClientSecret(propiedadesOauth.getClientSecret());
        return tokenService;
    }

}
