package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.dao.BandejaSolicitudEvaluacionDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ListFiltroRolResponse;
import pvt.auna.fcompleja.model.api.ListaBandeja;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.EmailAlertaCmacRequest;
import pvt.auna.fcompleja.model.api.request.ListaBandejaRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.request.registrarEvaluacionCmac;
import pvt.auna.fcompleja.model.api.request.evaluacion.ProgramacionCmacRequest;
import pvt.auna.fcompleja.model.api.response.evaluacion.ListaEvaluacionXFecha;
import pvt.auna.fcompleja.model.api.response.evaluacion.ProgramacionCmacDetResponse;
import pvt.auna.fcompleja.model.mapper.ListaEvaluacionRowMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.DetalleXCodigoCmacRowMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.DetalleXFechaPartRowMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.ProgramacionCmacDetMapper;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;

@Repository
public class BandejaSolicitudEvaluacionDaoImpl implements BandejaSolicitudEvaluacionDao {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	DataSource dataSource;

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse BandejaEvaluacion(ListaBandejaRequest objA_evaluacion) throws Exception {
		ApiOutResponse outResponse = new ApiOutResponse();
		ArrayList<ListaBandeja> listaBandeja = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();

		MapSqlParameterSource in = new MapSqlParameterSource();

		try {
			// LISTAEVALUACIONROWMAPPER DEPENDENCIA CON LA BUSQUEDA POR CODIGO CMAC
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION).withProcedureName("SP_PFC_S_BANDEJA_EVALUACION")
					.returningResultSet("AC_LISTADETALLE", new ListaEvaluacionRowMapper());

			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_COD_DESC_SOL_EVA", Types.VARCHAR));
			in.addValue("PV_COD_DESC_SOL_EVA", objA_evaluacion.getCodigoEvaluacion());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_COD_CLINICA", Types.VARCHAR));
			in.addValue("PV_COD_CLINICA", objA_evaluacion.getCodigoClinica());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_COD_PACIENTE", Types.VARCHAR));
			in.addValue("PV_COD_PACIENTE", objA_evaluacion.getCodigoPaciente());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_FECHA_INICIO", Types.VARCHAR));
			in.addValue("PV_FECHA_INICIO", DateUtils.getDateToStringDDMMYYYY(objA_evaluacion.getFechaInicio()));
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_FECHA_FIN", Types.VARCHAR));
			in.addValue("PV_FECHA_FIN", DateUtils.getDateToStringDDMMYYYY(objA_evaluacion.getFechaFin()));
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PN_NRO_SCG_SOLBEN", Types.VARCHAR));
			in.addValue("PN_NRO_SCG_SOLBEN", objA_evaluacion.getNumeroScgSolben());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_ESTADO_SOL_EVALUACION", Types.VARCHAR));
			in.addValue("PV_ESTADO_SOL_EVALUACION", objA_evaluacion.getEstadoSolicitudEvaluacion());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_TIP_SCG_SOLBEN", Types.VARCHAR));
			in.addValue("PV_TIP_SCG_SOLBEN", objA_evaluacion.getTipoScgSolben());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PN_ROL_RESPONSABLE", Types.NUMERIC));
			in.addValue("PN_ROL_RESPONSABLE", objA_evaluacion.getRolResponsable());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_ESTADO_SCG_SOLBEN", Types.VARCHAR));
			in.addValue("PV_ESTADO_SCG_SOLBEN", objA_evaluacion.getEstadoScgSolben());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_CORREO_LIDER_TUMOR", Types.VARCHAR));
			in.addValue("PV_CORREO_LIDER_TUMOR", objA_evaluacion.getCorreoLiderTumor());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PN_NRO_CARTA_GARANTIA", Types.VARCHAR));
			in.addValue("PN_NRO_CARTA_GARANTIA", objA_evaluacion.getNumeroCartaGarantia());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_CORREO_CMAC", Types.VARCHAR));
			in.addValue("PV_CORREO_CMAC", objA_evaluacion.getCorreoCmac());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PN_AUTORIZADOR_PERTENENCIA", Types.NUMERIC));
			in.addValue("PN_AUTORIZADOR_PERTENENCIA", objA_evaluacion.getAutorizadorPertenencia());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_TIPO_EVALUACION", Types.VARCHAR));
			in.addValue("PV_TIPO_EVALUACION", objA_evaluacion.getTipoEvaluacion());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PN_LIDER_TUMOR", Types.NUMERIC));
			in.addValue("PN_LIDER_TUMOR", objA_evaluacion.getLiderTumor());
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_FEC_REUNION_CMAC", Types.VARCHAR));
			in.addValue("PV_FEC_REUNION_CMAC", DateUtils.getDateToStringDDMMYYYY(objA_evaluacion.getReunionCmac()));
			
			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			listaBandeja = (ArrayList<ListaBandeja>) out.get("AC_LISTADETALLE");
			
			LOGGER.info(out.get("PN_COD_RESULTADO"));
			LOGGER.info(out.get("PV_MSG_RESULTADO"));
			LOGGER.info(listaBandeja.size());

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());
			outResponse.setResponse(listaBandeja);

			if (outResponse.getCodResultado() != 0) {
				LOGGER.error(this.getClass().getName() + ".BandejaEvaluacion: " + outResponse.getMsgResultado());
				throw new Exception(this.getClass().getName() + ".BandejaEvaluacion: " + outResponse.getMsgResultado());
			}
			

		} catch (Exception e) {
			outResponse.setCodResultado(99);
			outResponse.setMsgResultado(e.getMessage());
			outResponse.setResponse(null);
			LOGGER.error(outResponse.getMsgResultado(), e);
		}

		return outResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse consultarEvaluacionXFecha(EmailAlertaCmacRequest objA_evaluacion) {
		// TODO Auto-generated method stub
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();
		ArrayList<ProgramacionCmacDetResponse> listaResponse = new ArrayList<>();
		ListaEvaluacionXFecha evaluacion = new ListaEvaluacionXFecha();
		ArrayList<ListFiltroRolResponse> responsePar = new ArrayList<>();

		try {
			/** Call store procedure */
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION).withProcedureName("SP_PFC_S_FEC_EVALUACION_MAC")
					.returningResultSet("TC_LIST", new ProgramacionCmacDetMapper())
					.returningResultSet("TC_LIST_PAR", new DetalleXFechaPartRowMapper());

			/** Asignar los parameters input a la consulta */
			LOGGER.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_FEC_MAC", objA_evaluacion.getFechaCmac(), Types.VARCHAR);

			out = simpleJdbcCall.execute(in);

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			
			listaResponse = (ArrayList<ProgramacionCmacDetResponse>) out.get("TC_LIST");
			responsePar = (ArrayList<ListFiltroRolResponse>) out.get("TC_LIST_PAR");
			
			if(listaResponse.size() == 0) {
				throw new Exception("No se encontraron resultado para la fecha ingresada.");
			}
			
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());
			

			evaluacion.setCodActa(listaResponse.get(0).getCodActa());
			evaluacion.setCodigoGrabado(listaResponse.get(0).getCodigoGrabado());
			evaluacion.setHoraCmac(listaResponse.get(0).getHoraCmac());
			evaluacion.setCodProgramacionCmac(listaResponse.get(0).getCodProgramacionCmac());
			evaluacion.setCodActaFtp(listaResponse.get(0).getCodActaFtp());
			evaluacion.setFecReunion(listaResponse.get(0).getFecReunion());
			evaluacion.setReporteActaEscaneada(listaResponse.get(0).getReporteActaEscaneada());
			evaluacion.setListaBandeja(listaResponse);
			evaluacion.setListFiltroRolResponse(responsePar);
			
			response.setResponse(evaluacion);

		} catch (Exception e) {
			LOGGER.error("Error al listar : ", e);
			response.setCodResultado(-1);
			response.setMsgResultado(e.getMessage());
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ApiOutResponse registrarEvaluacionCmac(registrarEvaluacionCmac objA_evaluacion) {
		// TODO Auto-generated method stub
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();

		try {
			/** Call store procedure */
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION).withProcedureName("SP_PFC_I_EVALUACION_CMAC");

			/** Asignar los parameters input a la consulta */
			LOGGER.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PV_EVALUACION", objA_evaluacion.getEvaluacion(), Types.VARCHAR);
			in.addValue("PV_PARTICIPANTE_MAC", objA_evaluacion.getParticipanteCmac(), Types.VARCHAR);
			in.addValue("PV_COD_ESCAN", objA_evaluacion.getCodigoScan(), Types.VARCHAR);
			in.addValue("PV_FECHA_PROG", objA_evaluacion.getFechaProgramada(), Types.VARCHAR);
			in.addValue("PV_HORA_PROG", objA_evaluacion.getHoraProgramada(), Types.VARCHAR);
			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */

			/** Seguimiento */
			in.addValue("PV_FECHA_ESTADO", DateUtils.getConcatDateHourDDMMYYYHHMMSS(objA_evaluacion.getFechaEstado()),Types.VARCHAR);
			in.addValue("PN_COD_ROL_CMAC", objA_evaluacion.getCodigoRolResponsableCmac(), Types.NUMERIC);
			in.addValue("PN_COD_CMAC", objA_evaluacion.getCodigoUsrResponsableCmac(), Types.NUMERIC);
			in.addValue("PN_COD_ACTA_FTP", objA_evaluacion.getCodActaFtp(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			LOGGER.error("Error al listar : ", e);
		}

		return response;
	}
	
	@Override
	public WsResponse eliminarRegEvaluacionCmac(SolicitudEvaluacionRequest request) {
		
		Map<String, Object> out = new HashMap<>();
		WsResponse response = new WsResponse();
		
		
		try {
			LOGGER.info("Asigna parametros input EliminarRegEvaluacionCmac");
			LOGGER.info("input : " + request.getCodSolicitudEvaluacion());
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource)
					.withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION)
					.withProcedureName("SP_PFC_SD_ELIM_EVALUACION_CMAC");
			
			/** Asignar los parameters input a la consulta */
			LOGGER.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();
			
			in.addValue("PN_COD_SOL_EVA", request.getCodSolicitudEvaluacion(), Types.NUMERIC);
			
			out = simpleJdbcCall.execute(in);
			
			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			response.setAudiResponse(new AudiResponse("","",out.get("PN_COD_RESULTADO").toString(),(String)out.get("PV_MSG_RESULTADO")));
			
			
		} catch (Exception e) {
			LOGGER.error("ERROR SySTEMA" + e.getMessage());
			LOGGER.error("Error No se puede eliminar el registro");
			response.setAudiResponse(new AudiResponse("","",out.get("PN_COD_RESULTADO").toString(),"ERROR DE BASE DE DATOS"));
		}
		
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse EvaluacionXCodigo(ListaBandejaRequest objA_evaluacion) {
		// TODO Auto-generated method stub

		ApiOutResponse ApiOutResponse = new ApiOutResponse();
		List<ListaBandeja> listaBandeja = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();

		try {
			/** Call store procedure */
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION).withProcedureName("SP_PFC_S_EVALUACIONXCODIGO")
					.returningResultSet("AC_LISTADETALLE", new ListaEvaluacionRowMapper());

			/** Asignar los parameters input a la consulta */
			LOGGER.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();
			
			in.addValue("pv_cod_desc_sol_eva", objA_evaluacion.getCodigoEvaluacion(), Types.VARCHAR);
			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */

			out = simpleJdbcCall.execute(in);
			
			listaBandeja = (List<ListaBandeja>) out.get("AC_LISTADETALLE");
			ApiOutResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			ApiOutResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());
			
			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			
			ApiOutResponse.setResponse((listaBandeja.size() != 0) ? listaBandeja.get(0) : null);

		} catch (Exception e) {
			LOGGER.error("Error al listar : ", e);
			ApiOutResponse.setCodResultado(-1);
			ApiOutResponse.setMsgResultado(e.getMessage());
			ApiOutResponse.setResponse(null);
		}

		return ApiOutResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse consultarEvaluacionXCodigo(EmailAlertaCmacRequest objA_evaluacion) {
		// TODO Auto-generated method stub
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();
		List<ListaBandeja> listaResponse = new ArrayList<>();

		try {
			/** Call store procedure */
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION).withProcedureName("SP_PFC_S_COD_EVALUACION_MAC")
					.returningResultSet("TC_LIST", new DetalleXCodigoCmacRowMapper());

			/** Asignar los parameters input a la consulta */
			LOGGER.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PV_COD_EVA", objA_evaluacion.getCodLagoSolEva(), Types.VARCHAR);
			in.addValue("PV_COD_PROG_CMAC", objA_evaluacion.getCodLargoMac(), Types.VARCHAR);
			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			listaResponse = (List<ListaBandeja>) out.get("TC_LIST");

			response.setResponse(listaResponse);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			LOGGER.error("Error al listar : ", e);
		}

		return response;
	}

	@Override
	public ApiOutResponse actualizarActaEscanProgramacionCmac(ProgramacionCmacRequest request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();

		try {
			/** Call store procedure */
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION).withProcedureName("SP_PFC_U_ACTAESC_PROG_CMAC");

			LOGGER.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_PROGRAMACION_CMAC", Integer.parseInt(request.getCodEvaluacion()), Types.NUMERIC);
			in.addValue("PV_REPORTE_ACTA_ESCANEADA", request.getCodReporteActaEscaneada(), Types.VARCHAR);
			
			out = simpleJdbcCall.execute(in);

			response.setResponse(request);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			LOGGER.error("Error al listar : ", e);
		}

		return response;
	}

}
