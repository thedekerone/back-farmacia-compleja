package pvt.auna.fcompleja.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pvt.auna.fcompleja.dao.DetalleSolicitudEvaluacionDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ListaBandeja;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.CheckListPacPrefeInstiRequest;
import pvt.auna.fcompleja.model.api.request.CheckListRequisitoRequest;
import pvt.auna.fcompleja.model.api.request.ConfiguracionMarcadorRequest;
import pvt.auna.fcompleja.model.api.request.ContinuadorDocumentoRequest;
import pvt.auna.fcompleja.model.api.request.ContinuadorRequest;
import pvt.auna.fcompleja.model.api.request.EnvioCorreoRequest;
import pvt.auna.fcompleja.model.api.request.InformeSolEvaReporteRequest;
import pvt.auna.fcompleja.model.api.request.LineaTrataPrefeInstiRequest;
import pvt.auna.fcompleja.model.api.request.LineaTratamientoRequest;
import pvt.auna.fcompleja.model.api.request.ListaBandejaRequest;
import pvt.auna.fcompleja.model.api.request.ListaIndicadorRequest;
import pvt.auna.fcompleja.model.api.request.RegistroHistLineaTratRequest;
import pvt.auna.fcompleja.model.api.request.RptaBasalMarcadorRequest;
import pvt.auna.fcompleja.model.api.request.RptaEvaLiderTumorRequest;
import pvt.auna.fcompleja.model.api.request.SolbenRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.CondicionBasalPacienteRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.IndicacionCriterioRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.ProgramacionCmacRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.ResultadoBasalRequest;
import pvt.auna.fcompleja.model.api.request.seguridad.UsuarioRequest;
import pvt.auna.fcompleja.model.api.response.CriterioExclusion;
import pvt.auna.fcompleja.model.api.response.CriterioInclusion;
import pvt.auna.fcompleja.model.api.response.DocuHistPacienteResponse;
import pvt.auna.fcompleja.model.api.response.EvaluacionAutorizadorResponse;
import pvt.auna.fcompleja.model.api.response.HistLineaTratResponse;
import pvt.auna.fcompleja.model.api.response.ListaCasosCmacResponse;
import pvt.auna.fcompleja.model.api.response.MedicamentoContinuadorResponse;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.model.api.response.PreferenciaInstitucionalResponse;
import pvt.auna.fcompleja.model.api.response.RptaBasalMarcadorResponse;
import pvt.auna.fcompleja.model.api.response.evaluacion.MetastasisResponse;
import pvt.auna.fcompleja.model.api.response.evaluacion.ResultadoBasalDetResponse;
import pvt.auna.fcompleja.model.bean.ParticipanteBean;
import pvt.auna.fcompleja.service.DetalleSolicitudEvaluacionService;
import pvt.auna.fcompleja.service.GenericoService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.util.DateUtils;

@Service
public class DetalleSolicitudEvaluacionServiceImpl implements DetalleSolicitudEvaluacionService {

	@Autowired
	DetalleSolicitudEvaluacionDao detalleEvaluaDao;

	@Autowired
	OncoPacienteService pcteService;

	AfiliadosRequest pacienteRequest;

	@Autowired
	GenericoService genericoService;

	@Override
	public ApiOutResponse consultarInformacionScgEva(InformeSolEvaReporteRequest request,
			AfiliadosRequest pacienteRequest) {
		return detalleEvaluaDao.consultarInformacionScgEva(request);
	}

	@Override
	public HistLineaTratResponse consultarHistLineaTrat(LineaTratamientoRequest listaTrata) {
		return detalleEvaluaDao.consultarHistLineaTrat(listaTrata);
	}

	@Override
	public ApiOutResponse insertarHistLineaTrat(RegistroHistLineaTratRequest request) {
		return detalleEvaluaDao.insertarHistLineaTrat(request);
	}

	@Override
	public ApiOutResponse insertarCondicionBasalPaciente(CondicionBasalPacienteRequest request) {
		String lineaLugarStr = "";
		String resultadoBasalStr = "";
		if (request.getLineaLugarMetastasis().size() > 0) {
			for (MetastasisResponse o : request.getLineaLugarMetastasis()) {
				lineaLugarStr = lineaLugarStr + o.getLineaMetastasis() + "," + o.getLugarMetastasis() + "|";
			}
			lineaLugarStr = lineaLugarStr.substring(0, lineaLugarStr.length() - 1);
			request.setStrLineaLugarMetastasis(lineaLugarStr);
		}
		if (request.getResultadoBasal().size() > 0) {
			for (ResultadoBasalDetResponse j : request.getResultadoBasal()) {
				resultadoBasalStr = resultadoBasalStr + j.getCodResultadoBasal() + "," + j.getResultado() + ","
						+ DateUtils.getDateToStringDDMMYYYY(j.getFecResultado()) + "|";
			}
			resultadoBasalStr = resultadoBasalStr.substring(0, resultadoBasalStr.length() - 1);
			System.out.println(resultadoBasalStr);
			request.setStrResultadoBasal(resultadoBasalStr);
		}
		return detalleEvaluaDao.insertarCondicionBasalPaciente(request);
	}

	@Override
	public List<EvaluacionAutorizadorResponse> consultarEvaluacionAutorizador(SolbenRequest request) {
		return detalleEvaluaDao.consultarEvaluacionAutorizador(request);
	}

	@Override
	public ApiOutResponse insertarLineaTratamiento(LineaTrataPrefeInstiRequest request) {
		return detalleEvaluaDao.insertarLineaTratamiento(request);
	}

	@Override
	public PreferenciaInstitucionalResponse insertarPreferenciaInsti(Integer codGrpDiag, Integer condicionCancer,
			Integer lineaTrata, Integer subCondicionCancer, Integer codMac) {

		return detalleEvaluaDao.insertarPreferenciaInsti(codGrpDiag, condicionCancer, lineaTrata, subCondicionCancer,
				codMac);
	}

	@Override
	public ApiOutResponse insertarConfiguracionMarcador(ConfiguracionMarcadorRequest request) {
		return detalleEvaluaDao.insertarConfiguracionMarcador(request);
	}

	@Override
	public List<RptaBasalMarcadorResponse> consultarRptaBasalMarcador(RptaBasalMarcadorRequest request) {
		return detalleEvaluaDao.consultarRptaBasalMarcador(request);
	}

	@Override
	public ApiOutResponse actualizarCheckListGuardarDocumento(CheckListRequisitoRequest request) {
		return detalleEvaluaDao.actualizarCheckListGuardarDocumento(request);
	}

	// GUARDAR O ACTUALIZAR CARGA DE ARCHIVO PASO 2
	@Override
	public ApiOutResponse insActCheckListReqDocumento(CheckListRequisitoRequest request) {
		return detalleEvaluaDao.insActCheckListReqDocumento(request);
	}

	@Override
	public ApiOutResponse actualizarEliminacionDocumento(CheckListRequisitoRequest request) {
		return detalleEvaluaDao.actualizarEliminacionDocumento(request);
	}

	@Override
	public ApiOutResponse actualizarDescargaDocumento(CheckListRequisitoRequest request) {
		return detalleEvaluaDao.actualizarDescargaDocumento(request);
	}

	@Override
	public ApiOutResponse insertarCargaDocumento(SolicitudEvaluacionRequest request) {
		return detalleEvaluaDao.insertarCargaDocumento(request);
	}

	@Override
	public DocuHistPacienteResponse consultarCheckListRequisito(CheckListRequisitoRequest request) {
		return detalleEvaluaDao.consultarCheckListRequisito(request);
	}

	@Override
	public ApiOutResponse consultarCheckListPacPrefeInsti(SolicitudEvaluacionRequest request) {
		return detalleEvaluaDao.consultarCheckListPacPrefeInsti(request);
	}

	@Override
	public ApiOutResponse insActCheckListPacPrefeInsti(CheckListPacPrefeInstiRequest request) {
		return detalleEvaluaDao.insActCheckListPacPrefeInsti(request);
	}

	@Override
	public ApiOutResponse insActMedicamentoContinuador(ContinuadorRequest request) {
		return detalleEvaluaDao.insActMedicamentoContinuador(request);
	}

	@Override
	public ApiOutResponse actualizarContinuadorDocumento(ContinuadorDocumentoRequest request) {
		return detalleEvaluaDao.actualizarContinuadorDocumento(request);
	}

	public MedicamentoContinuadorResponse consultarMedicamentoContinuador(ContinuadorRequest request) {
		return detalleEvaluaDao.consultarMedicamentoContinuador(request);
	}

	@Override
	public ApiOutResponse insertarRptaEvaLiderTumor(RptaEvaLiderTumorRequest request) {
		return detalleEvaluaDao.insertarRptaEvaLiderTumor(request);
	}

	@Override
	public ApiOutResponse consultarIndicador(ListaIndicadorRequest request) {
		return detalleEvaluaDao.consultarIndicador(request);
	}

	@Override
	public ApiOutResponse insertarCheckListPaciente(ListaIndicadorRequest request) {
		IndicacionCriterioRequest indicador = request.getIndicacionCriterio();
		List<CriterioInclusion> criterioInclusion = indicador.getListaCriterioInclusion();
		List<CriterioExclusion> criterioExclusion = indicador.getListaCriterioExclusion();
		String inclusionStr = "";
		String exclusionStr = "";

		for (CriterioInclusion inclusion : criterioInclusion) {
			inclusionStr = inclusionStr + inclusion.getCodigo() + "," + inclusion.getCriterioInclusion() + "|";
		}
		inclusionStr = inclusionStr.substring(0, inclusionStr.length() - 1);
		request.setInclusion(inclusionStr);

		for (CriterioExclusion exclusion : criterioExclusion) {
			exclusionStr = exclusionStr + exclusion.getCodigo() + "," + exclusion.getCriterioExclusion() + "|";
		}
		exclusionStr = exclusionStr.substring(0, exclusionStr.length() - 1);
		request.setExclusion(exclusionStr);

		return detalleEvaluaDao.insertarCheckListPaciente(request);
	}

	@Override
	public ListaCasosCmacResponse listarEvaluacionCmac(InformeSolEvaReporteRequest requestCodSol) {
		// TODO Auto-generated method stub
		return detalleEvaluaDao.listarEvaluacionCmac(requestCodSol);
	}

	@Override
	public ListaCasosCmacResponse registrarProgramacionCmac(ProgramacionCmacRequest requestfech) {
		// TODO Auto-generated method stub

		String codigoEvaluacion;
		List<ListaBandeja> listaBandeja = new ArrayList<>();

		listaBandeja = requestfech.getListaEvaluacion();

		StringBuilder listaCodEvaluacion = new StringBuilder();

		// ListaBandeja.
		for (ListaBandeja listaEvaluaciones : listaBandeja) {
			listaCodEvaluacion.append(listaEvaluaciones.getCodSolEvaluacion() + ",");
		}

		codigoEvaluacion = listaCodEvaluacion.substring(0, listaCodEvaluacion.length() - 1);
		requestfech.setCodEvaluacion(codigoEvaluacion);

		return detalleEvaluaDao.registrarProgramacionCmac(requestfech);
	}

	@Override
	public ApiOutResponse consultarResultadoBasal(ResultadoBasalRequest request) {
		return detalleEvaluaDao.consultarResultadoBasal(request);
	}

	@Override
	public ApiOutResponse consultarSubcondicionCancer(LineaTrataPrefeInstiRequest request) {
		return detalleEvaluaDao.consultarSubcondicionCancer(request);
	}

	@Override
	public ApiOutResponse consultarPreferenciaInsti(LineaTrataPrefeInstiRequest request) {
		return detalleEvaluaDao.consultarPreferenciaInsti(request);
	}

	@Override
	public ApiOutResponse conInsActPreferenciaInstiPre(LineaTrataPrefeInstiRequest request) {
		return detalleEvaluaDao.conInsActPreferenciaInstiPre(request);
	}

	@Override
	public ApiOutResponse insertarCheckListPaciente2(LineaTrataPrefeInstiRequest request) {
		return detalleEvaluaDao.insertarCheckListPaciente2(request);
	}

	@Override
	public ApiOutResponse consultarEvaluacionLiderTumor(ParticipanteBean request) {
		return detalleEvaluaDao.consultarEvaluacionLiderTumor(request);
	}

	@Override
	public ApiOutResponse consultarSeguimiento(ListaBandejaRequest request) {
		return detalleEvaluaDao.consultarSeguimiento(request);
	}

	@Override
	public ApiOutResponse consultarDocumentoContinuador(CheckListRequisitoRequest request) {
		return detalleEvaluaDao.consultarDocumentoContinuador(request);
	}

	@Override
	public ApiOutResponse insActContinuadorDocumentoCargar(CheckListRequisitoRequest request) {
		return detalleEvaluaDao.insActContinuadorDocumentoCargar(request);
	}

	public ApiOutResponse actContinuadorDocumentoElim(CheckListRequisitoRequest request) {
		return detalleEvaluaDao.actContinuadorDocumentoElim(request);
	}

	@Override
	public ApiOutResponse actualizarCodigoEnvio(EnvioCorreoRequest request) {
		return detalleEvaluaDao.actualizarCodigoEnvio(request);
	}

	@Override
	public ApiOutResponse listarCodDocumentosChecklist(InformeSolEvaReporteRequest request) {
		return detalleEvaluaDao.listarCodDocumentosChecklist(request);
	}

	@Override
	public ApiOutResponse actualizarEvaluacionInformeAutorizador(SolicitudEvaluacionRequest request) {
		return detalleEvaluaDao.actualizarEvaluacionInformeAutorizador(request);
	}
	
	@Override
	public OncoWsResponse consultarUsuarioRol(UsuarioRequest request) {
		return detalleEvaluaDao.consultarUsuarioRol(request);
	}

	@Override
	public ApiOutResponse actualizarTipoSolEvaluacion(SolicitudEvaluacionRequest request) {
		return detalleEvaluaDao.actualizarTipoSolEvaluacion(request);
	}

	@Override
	public ApiOutResponse consultarInformAutorizCmac(ListaBandejaRequest request) {
		return detalleEvaluaDao.consultarInformAutorizCmac(request);
	}

}
