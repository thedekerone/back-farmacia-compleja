import { Component, OnInit, Inject, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Parametro } from 'src/app/dto/Parametro';
import { PREFERENCIAINSTI, LINEATRATAMIENTO, GRUPO_PARAMETRO, MENSAJES, FLAG_REGLAS_EVALUACION, ACCESO_EVALUACION } from 'src/app/common';
import { DateAdapter, MatDialog } from '@angular/material';
import { ListaParametroservice } from 'src/app/service/lista.parametro.service';
import { DetalleSolicitudEvaluacionService } from 'src/app/service/detalle.solicitud.evaluacion.service';
import { DatePipe } from '@angular/common';
import { LineaTrataPrefeInstiRequest } from 'src/app/dto/request/LineaTrataPrefeInstiRequest';
import { ParametroRequest } from 'src/app/dto/ParametroRequest';
import { ParametroResponse } from 'src/app/dto/ParametroResponse';
import { WsResponse } from 'src/app/dto/WsResponse';
import { ApiOutResponse } from 'src/app/dto/response/ApiOutResponse';
import { EvaluacionService } from 'src/app/dto/service/evaluacion.service';
import { MessageComponent } from 'src/app/core/message/message.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UsuarioService } from 'src/app/dto/service/usuario.service';
import { BOpcionMenuLocalStorage } from 'src/app/dto/core/BOpcionMenuLocalStorage';

@Component({
  selector: 'app-preferencia-institucionales',
  templateUrl: './preferencia-institucionales.component.html',
  styleUrls: ['./preferencia-institucionales.component.scss']
})
export class PreferenciaInstitucionalesComponent implements OnInit, OnChanges {

  public prefInstFrmGrp: FormGroup = new FormGroup({
    'lineaTratamientoFrmCtrl': new FormControl(null, Validators.required),
    'nroLineaTrataFrmCtrl': new FormControl(null, Validators.required),
    'nroCursoFrmCtrl': new FormControl(null, Validators.required),
    'tipoTumorFrmCtrl': new FormControl(null, Validators.required),
    'respAlcanzadaFrmCtrl': new FormControl(null, Validators.required),
    'lugarProgresionFrmCtrl': new FormControl(null, Validators.required),
    'grupoDiagFrmCtrl': new FormControl(null, Validators.required),
    'condicionCancerFrmCtrl': new FormControl(null, Validators.required),
    'detalleCondicionFrmCtrl': new FormControl(null),
    'tratamientoFrmCtrl': new FormControl(null, Validators.required),
    'presentaNoPermitidaFrmCtrl': new FormControl(null, Validators.required),
    'tipoTratamientoFrmCtrl': new FormControl(null, Validators.required),
    'rgPrefInstiFrmCtrl': new FormControl(null, Validators.required),
    'observacionFrmCtrl': new FormControl(null, [])
    // 'observacionFrmCtrl': new FormControl(null, Validators.required)
  });

  get lineaTratamientoFrmCtrl() { return this.prefInstFrmGrp.get('lineaTratamientoFrmCtrl'); }
  get nroLineaTrataFrmCtrl() { return this.prefInstFrmGrp.get('nroLineaTrataFrmCtrl'); }
  get nroCursoFrmCtrl() { return this.prefInstFrmGrp.get('nroCursoFrmCtrl'); }
  get tipoTumorFrmCtrl() { return this.prefInstFrmGrp.get('tipoTumorFrmCtrl'); }
  get respAlcanzadaFrmCtrl() { return this.prefInstFrmGrp.get('respAlcanzadaFrmCtrl'); }
  get lugarProgresionFrmCtrl() { return this.prefInstFrmGrp.get('lugarProgresionFrmCtrl'); }
  get grupoDiagFrmCtrl() { return this.prefInstFrmGrp.get('grupoDiagFrmCtrl'); }
  get condicionCancerFrmCtrl() { return this.prefInstFrmGrp.get('condicionCancerFrmCtrl'); }
  get detalleCondicionFrmCtrl() { return this.prefInstFrmGrp.get('detalleCondicionFrmCtrl'); }
  get tratamientoFrmCtrl() { return this.prefInstFrmGrp.get('tratamientoFrmCtrl'); }
  get presentaNoPermitidaFrmCtrl() { return this.prefInstFrmGrp.get('presentaNoPermitidaFrmCtrl'); }
  get tipoTratamientoFrmCtrl() { return this.prefInstFrmGrp.get('tipoTratamientoFrmCtrl'); }
  get rgPrefInstiFrmCtrl() { return this.prefInstFrmGrp.get('rgPrefInstiFrmCtrl'); }
  get observacionFrmCtrl() { return this.prefInstFrmGrp.get('observacionFrmCtrl'); }

  mensajes: string;

  rbtPrefInsti: any[] = [{
    codigo: 1,
    titulo: 'SI',
    selected: false
  }, {
    codigo: 0,
    titulo: 'NO',
    selected: false
  }, {
    codigo: 2,
    titulo: 'NO APLICA',
    selected: false
  }];

  listaTipoTumor: Parametro[] = [];
  listaNroCursos: Parametro[] = [];
  listaRespAlcanzada: Parametro[] = [];
  listaLugarProgresion: Parametro[] = [];
  listaCondicionCancer: Parametro[] = [];
  listaSubCondicionCancer: Parametro[] = [];
  txtLineaTratamiento: string;
  detalleSubcondicion: string;
  verPrefInstitucionales: boolean;
  existeLineaTrata: boolean;
  tratamientoLabel: string;
  tipoTratamientoLabel: string;
  cumplePrefeDesaLabel: string;

  lineaTratRequest: LineaTrataPrefeInstiRequest;
  parametroRequest: ParametroRequest;

  grabarPaso: string;

  flagObservacions: Boolean;

  @Output() btnSiguiente = new EventEmitter<boolean>();

  opcionMenu: BOpcionMenuLocalStorage;
  txtNumeroLinea: number;
  cmbNumeroCurso: number;
  cmbTipoTumor: number;
  cmbRespuestaAlcanzada: number;
  cmbLugarProgresion: number;
  txtGrupoDiagnostico: number;
  cmbCondicion: number;
  cmbDetalleCondicion: number;
  txtTratamiento: number;
  txtPresentacion: number;
  txtTipoTratamiento: number;
  txtReferencia: number;
  txtObservacion: number;
  btnGrabar: number;
  btnSalir: number;
  btnSiguientePaso1: number;

  flagEvaluacion = FLAG_REGLAS_EVALUACION;
  valorMostrarOpcion = ACCESO_EVALUACION.mostrarOpcion;

  constructor(private adapter: DateAdapter<any>,
    public dialog: MatDialog,
    private spinnerService: Ng4LoadingSpinnerService,
    private listaParametroservice: ListaParametroservice,
    private detalleSolicitudEvaluacionService: DetalleSolicitudEvaluacionService,
    private datePipe: DatePipe,
    @Inject(UsuarioService) private userService: UsuarioService,
    @Inject(EvaluacionService) private solicitud: EvaluacionService) {
    this.adapter.setLocale('es-PE');
    this.accesoOpcionMenu();
  }

  ngOnInit() {
    this.iniciarVariables();
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  public iniciarVariables(): void {
    this.lineaTratRequest = new LineaTrataPrefeInstiRequest();
    this.parametroRequest = new ParametroRequest();
    this.respAlcanzadaFrmCtrl.disable();
    this.tratamientoLabel = 'TRATAMIENTO';
    this.tipoTratamientoLabel = 'TIPO TRATAMIENTO';
    this.cumplePrefeDesaLabel = 'CUMPLE PREFERENCIA INSTITUCIONAL';
  }

  public grabarParametros(): boolean {
    if (this.prefInstFrmGrp.invalid) {
      this.lineaTratamientoFrmCtrl.markAsTouched();
      this.lineaTratamientoFrmCtrl.markAsTouched();
      this.nroLineaTrataFrmCtrl.markAsTouched();
      this.nroCursoFrmCtrl.markAsTouched();
      this.tipoTumorFrmCtrl.markAsTouched();
      this.respAlcanzadaFrmCtrl.markAsTouched();
      this.lugarProgresionFrmCtrl.markAsTouched();
      this.grupoDiagFrmCtrl.markAsTouched();
      this.condicionCancerFrmCtrl.markAsTouched();
      this.detalleCondicionFrmCtrl.markAsTouched();
      this.tratamientoFrmCtrl.markAsTouched();
      this.presentaNoPermitidaFrmCtrl.markAsTouched();
      this.tipoTratamientoFrmCtrl.markAsTouched();
      this.rgPrefInstiFrmCtrl.markAsTouched();
      this.observacionFrmCtrl.markAsTouched();
      this.mensajes = 'Completar los campos en rojo';
      this.openDialogMensaje(MENSAJES.ERROR_CAMPOS, this.mensajes, true, false, null);
      return false;
    }

    if ( this.flagObservacions && (this.observacionFrmCtrl.value === null || this.observacionFrmCtrl.value.trim() === '') ) {
      this.mensajes = 'Se cambio la preferencia institucional, agregar observaciones.';
      this.observacionFrmCtrl.markAsTouched();
      this.openDialogMensaje(MENSAJES.ERROR_CAMPOS, this.mensajes, true, false, null);
      return false;
    }

    /*if (this.observacionFrmCtrl.enabled && (this.observacionFrmCtrl.value === null || this.observacionFrmCtrl.value.trim() === '')) {
      this.mensajes = 'Se cambio la preferencia institucional, agregar observaciones.';
      this.observacionFrmCtrl.markAsTouched();
      this.openDialogMensaje(MENSAJES.ERROR_CAMPOS, this.mensajes, true, false, null);
      return false;
    }*/

    this.lineaTratRequest = {
      codSolEva: this.solicitud.codSolEvaluacion,
      nroLineaTrata: this.nroLineaTrataFrmCtrl.value,
      nroCurso: this.nroCursoFrmCtrl.value,
      tipoTumor: this.tipoTumorFrmCtrl.value,
      lugarProgresion: this.lugarProgresionFrmCtrl.value,
      respAlcanzada: this.respAlcanzadaFrmCtrl.value,
      cumplePrefeInsti: this.rgPrefInstiFrmCtrl.value,
      condicionCancer: this.condicionCancerFrmCtrl.value,
      observacion: this.observacionFrmCtrl.value,
      codMac: this.lineaTratRequest.codMac,
      codGrupoDiag: this.solicitud.codGrupoDiagnostico,
      lineaTratamiento: this.lineaTratRequest.lineaTratamiento,
      codigoRolUsuario: this.userService.getCodRol,
      fechaEstado: this.datePipe.transform(new Date(), 'dd/MM/yyyy'),
      codigoUsuario: this.userService.getCodUsuario
    };

    return true;
  }

  public obtenerCombo(lista: any[], valor: number, descripcion: string) {
    if (lista !== null) {
      lista.unshift({
        'codigoParametro': valor,
        'nombreParametro': descripcion
      });
    }
  }

  public iniciarNuevaLineaTrataPrefeInsti() {
    this.btnSiguiente.emit(true);
    this.flagObservacions = false; // this.observacionFrmCtrl.disable();
    this.rgPrefInstiFrmCtrl.disable();
    this.verPrefInstitucionales = (this.solicitud.descGrupoDiagnostico === '' ||
      this.solicitud.descGrupoDiagnostico === null) ? false : true;
    this.cargarVariablesLineaTrataPrefInst();
    if (this.existeLineaTrata) {
      this.lineaTratRequest.codSolEva = this.solicitud.codSolEvaluacion;
      this.precargaLineaTrataPrefInst(this.existeLineaTrata);
    } else {
      this.lineaTratamientoFrmCtrl.disable();
      this.precargaLineaTrataPrefInst(this.existeLineaTrata);
    }
  }

  public cargarVariablesLineaTrataPrefInst(): void {
    this.lineaTratRequest = new LineaTrataPrefeInstiRequest();
    this.lineaTratRequest.nroLineaTrata = (this.solicitud.nroLineaTratamiento == null) ?
      1 : this.solicitud.nroLineaTratamiento + 1;
    this.nroLineaTrataFrmCtrl.setValue(this.lineaTratRequest.nroLineaTrata);

    this.consultarNroLineaTrata();

    this.lineaTratRequest.codGrupoDiag = (this.solicitud.codGrupoDiagnostico === null)
      ? null : this.solicitud.codGrupoDiagnostico;
    this.grupoDiagFrmCtrl.setValue((this.solicitud.descGrupoDiagnostico === '')
      ? null : this.solicitud.descGrupoDiagnostico);
  }

  public consultarNroLineaTrata() {
    switch (this.lineaTratRequest.nroLineaTrata) {
      case 1:
        this.txtLineaTratamiento = LINEATRATAMIENTO.primeraLineaTxt;
        this.lineaTratRequest.lineaTratamiento = LINEATRATAMIENTO.primeraLinea;
        this.existeLineaTrata = true;
        break;
      case 2: this.txtLineaTratamiento = LINEATRATAMIENTO.segundaLineaTxt;
        this.lineaTratRequest.lineaTratamiento = LINEATRATAMIENTO.segundaLinea;
        this.existeLineaTrata = true;
        break;
      case 3: this.txtLineaTratamiento = LINEATRATAMIENTO.terceraLineaTxt;
        this.lineaTratRequest.lineaTratamiento = LINEATRATAMIENTO.terceraLinea;
        this.existeLineaTrata = true;
        break;
      default:
        this.existeLineaTrata = false;
        this.rgPrefInstiFrmCtrl.setValue(2);
        this.rgPrefInstiFrmCtrl.disable();
      // this.descripcionMacFrmCtrl.setValue('No existen preferencias registradas para la línea de tratamiento');
    }

    this.lineaTratamientoFrmCtrl.setValue(this.txtLineaTratamiento);
  }

  public consultarTipoTumor(precargar: boolean, valor: any, valorRptaAlcanzada: any) {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.codGrupoTipoTumor;
    this.listaParametroservice.listaParametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.listaTipoTumor = (data.filtroParametro != null) ? data.filtroParametro : [];
          this.obtenerCombo(this.listaTipoTumor, null, '-- Seleccionar Tipo Tumor --');
          if (precargar) {
            this.tipoTumorFrmCtrl.setValue(valor);
            if (valorRptaAlcanzada !== null) {
              this.consultarRespuestaAlcanzada(true, valorRptaAlcanzada);
            }
          }
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar el Tipo de Tumor');
      }
    );
  }

  public consultarNroCursos(precargar: boolean, valor: any) {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.nroCursos;
    this.listaParametroservice.listaParametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.listaNroCursos = (data.filtroParametro != null) ? data.filtroParametro : [];
          this.obtenerCombo(this.listaNroCursos, null, '-- Seleccionar Nro. Cursos --');
          if (precargar) {
            this.nroCursoFrmCtrl.setValue(valor);
          }
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar Nro de Cursos');
      }
    );
  }

  public cargarRptaAlcanzada(event: Event): void {
    this.listaRespAlcanzada = [];
    this.consultarRespuestaAlcanzada(false, null);
  }

  public consultarRespuestaAlcanzada(precargar: boolean, valor: number) {
    if (typeof this.tipoTumorFrmCtrl.value === 'undefined' && this.tipoTumorFrmCtrl.value !== null) {
      return;
    }

    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.respuestaAlcanzada;
    this.parametroRequest.codigoParam = this.tipoTumorFrmCtrl.value;
    this.listaParametroservice.consultarParametro(this.parametroRequest).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.listaRespAlcanzada = (data.data != null) ? data.data : [];
          this.obtenerCombo(this.listaRespAlcanzada, null, '-- Seleccionar Respuesta Alcanzada --');
          if (precargar) {
            this.respAlcanzadaFrmCtrl.setValue(valor);
          }
          this.respAlcanzadaFrmCtrl.enable();
        } else {
          console.error(data);
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, data.audiResponse.mensajeRespuesta, true, false, null);
        }
      },
      error => {
        console.error(error);
        this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al cargar respuesta alcanzada.', true, false, null);
      }
    );
  }

  public consultarLugarProgresion(precargar: boolean, valor: any) {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.lugarProgresion;
    this.listaParametroservice.listaParametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.listaLugarProgresion = data.filtroParametro;
          this.obtenerCombo(this.listaLugarProgresion, null, '-- Seleccionar Lugar Progresion --');
          if (precargar) {
            this.lugarProgresionFrmCtrl.setValue(valor);
          }
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar el Lugar Progresion');
      }
    );
  }
  // Guardar Linea de Tratamiento / Preferencia Institucional Paso 1
  public insertarLineaTratamiento() {
    if (this.grabarParametros()) {
      this.spinnerService.show();
      this.detalleSolicitudEvaluacionService.insertarLineaTratamiento(this.lineaTratRequest).subscribe(
        (data: ApiOutResponse) => {
          if (data.codResultado === 0) {
            this.openDialogMensaje('Operación realizada exitosamente.', null, true, false, null);
            this.grabarPaso = '1';
            this.btnSiguiente.emit(false);
          } else {
            this.openDialogMensaje('Hubo un problema, por favor volver a intentar.', data.msgResultado, true, false, null);
            console.error(data);
            this.grabarPaso = '0';
            this.btnSiguiente.emit(true);
          }
          this.spinnerService.hide();
        },
        error => {
          console.error(error);
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Hubo un problema, por favor volver a intentar.', true, false, null);
          this.spinnerService.hide();
          this.btnSiguiente.emit(true);
        }
      );
    }
  }

  public consultarMacTratamiento(precargar: boolean, valor: any) {
    this.lineaTratRequest.condicionCancer = this.condicionCancerFrmCtrl.value;
    this.listaSubCondicionCancer = [];
    if (!precargar) {
      this.observacionFrmCtrl.setValue(null);
      this.flagObservacions = false; // this.observacionFrmCtrl.disable();
    }
    this.buscarPreferenciaInsti(precargar);
  }

  // PRECARGAR DATOS SI EXISTEN EN FARMACIA COMPLEJA PASO 1
  private precargaLineaTrataPrefInst(existeLinea: boolean) {
    this.lineaTratRequest.codSolEva = this.solicitud.codSolEvaluacion;
    this.lineaTratRequest.codGrupoDiag = this.solicitud.codGrupoDiagnostico;
    this.spinnerService.show();
    this.detalleSolicitudEvaluacionService.
      conInsActPreferenciaInstiPre(this.lineaTratRequest).subscribe(
        (data: WsResponse) => {
          if (data.data !== null) {

            this.lineaTratRequest.nroCurso = data.data.nroCurso;
            this.consultarNroCursos(true, this.lineaTratRequest.nroCurso);

            this.lineaTratRequest.tipoTumor = data.data.tipoTumor;
            this.lineaTratRequest.respAlcanzada = data.data.respuestaAlcanzada;

            this.consultarTipoTumor(true, data.data.tipoTumor, data.data.respuestaAlcanzada);

            this.lineaTratRequest.lugarProgresion = data.data.lugarProgresion;
            this.consultarLugarProgresion(true, data.data.lugarProgresion);

            this.listaCondicionCancer = data.data.cmbCondicion;

            this.lineaTratRequest.cumplePrefeInsti = data.data.cumplePrefInsti;
            this.lineaTratRequest.observacion = data.data.observacion;
            this.rgPrefInstiFrmCtrl.setValue(data.data.cumplePrefInsti);

            this.grabarPaso = data.data.grabar;

            if (this.grabarPaso === '1') {
              this.btnSiguiente.emit(false);
            }

            this.spinnerService.hide();
          } else {
            this.consultarNroCursos(false, null);
            this.consultarTipoTumor(false, null, null);
            this.consultarLugarProgresion(false, null);
            this.spinnerService.hide();
          }
          // this.consultarMacTratamiento(true, null);

          if (existeLinea && data.data !== null && data.data.condicion !== null) {
            this.lineaTratRequest.condicionCancer = data.data.condicion;
            this.condicionCancerFrmCtrl.setValue(data.data.condicion);
            this.consultarMacTratamiento(true, null);
          }
          this.spinnerService.hide();
        }, error => {
          console.error(error);
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al cargar data inicial grabada.', true, false, null);
          this.spinnerService.hide();
        }
      );
  }

  public logicaPreferenciaInstitucional(response: WsResponse): void {
    if (this.solicitud.codMac !== response.data[0].codTratamiento &&
      response.data[0].tipoTratamiento === PREFERENCIAINSTI.codPreferencia &&
      response.data[0].presentacNoPermitida === null) {
      this.rgPrefInstiFrmCtrl.setValue(0);
    } else if (this.solicitud.codMac === response.data[0].codTratamiento &&
      response.data[0].tipoTratamiento === PREFERENCIAINSTI.codPreferencia &&
      response.data[0].presentacNoPermitida === null) {
      this.rgPrefInstiFrmCtrl.setValue(1);
    } else if (this.solicitud.codMac !== response.data[0].codTratamiento &&
      response.data[0].tipoTratamiento === PREFERENCIAINSTI.codDesaprobacion &&
      response.data[0].presentacNoPermitida === null) {
      this.rgPrefInstiFrmCtrl.setValue(1);
    } else if (this.solicitud.codMac === response.data[0].codTratamiento &&
      response.data[0].tipoTratamiento === PREFERENCIAINSTI.codDesaprobacion &&
      response.data[0].presentacNoPermitida === null) {
      this.rgPrefInstiFrmCtrl.setValue(0);
    } else if (this.solicitud.codMac != null && response.data[0].codTratamiento === null &&
      response.data[0].tipoTratamiento === 240 && response.data[0].presentacNoPermitida === null) {
      this.rgPrefInstiFrmCtrl.setValue(2);
    } else if (this.solicitud.codMac !== response.data[0].codTratamiento &&
      response.data[0].tipoTratamiento === PREFERENCIAINSTI.codPreferencia &&
      response.data[0].presentacNoPermitida != null) {
      this.rgPrefInstiFrmCtrl.setValue(0);
    } else if (this.solicitud.codMac === response.data[0].codTratamiento &&
      response.data[0].tipoTratamiento === PREFERENCIAINSTI.codPreferencia &&
      response.data[0].presentacNoPermitida != null) {
      this.rgPrefInstiFrmCtrl.setValue(0);
    } else if (this.solicitud.codMac !== response.data[0].codTratamiento &&
      response.data[0].tipoTratamiento === PREFERENCIAINSTI.codDesaprobacion &&
      response.data[0].presentacNoPermitida != null) {
      this.rgPrefInstiFrmCtrl.setValue(0);
    } else if (this.solicitud.codMac === response.data[0].codTratamiento &&
      response.data[0].tipoTratamiento === PREFERENCIAINSTI.codDesaprobacion &&
      response.data[0].presentacNoPermitida != null) {
      this.rgPrefInstiFrmCtrl.setValue(0);
    } else if (this.solicitud.codMac != null && response.data[0].codTratamiento === null &&
      response.data[0].tipoTratamiento === 240 && response.data[0].presentacNoPermitida != null) {
      this.rgPrefInstiFrmCtrl.setValue(2);
    } else if (this.lineaTratRequest.condicionCancer === PREFERENCIAINSTI.codCondicion &&
      (response.data[0].codTratamiento === null || response.data[0].codTratamiento === 0)) {
      this.rgPrefInstiFrmCtrl.setValue(2);
    } else {
      this.rgPrefInstiFrmCtrl.setValue(null);
    }
  }

  public buscarPreferenciaInsti(precargar: boolean) {
    this.tratamientoLabel = PREFERENCIAINSTI.tratamiento;
    this.tipoTratamientoLabel = PREFERENCIAINSTI.tipoTratamiento;
    this.cumplePrefeDesaLabel = PREFERENCIAINSTI.cumplePrefe;
    this.lineaTratRequest.codSolEva = this.solicitud.codSolEvaluacion;
    this.lineaTratRequest.codGrupoDiag = this.solicitud.codGrupoDiagnostico;
    this.spinnerService.show();
    this.detalleSolicitudEvaluacionService.consultarPreferenciaInsti(this.lineaTratRequest).subscribe(
      (response: WsResponse) => {
        this.rgPrefInstiFrmCtrl.enable();
        if (response.data.length === 1) {
          this.detalleCondicionFrmCtrl.setValue(
            (response.data[0].detalleCondicion !== null) ?
              response.data[0].detalleCondicion : PREFERENCIAINSTI.noRegistrado);
          this.tratamientoFrmCtrl.setValue(
            (response.data[0].descripTratamiento !== null) ?
              response.data[0].descripTratamiento : PREFERENCIAINSTI.noRegistrado);
          this.presentaNoPermitidaFrmCtrl.setValue(
            (response.data[0].presentacNoPermitida === null) ?
              PREFERENCIAINSTI.noRegistrado : response.data[0].presentacNoPermitida);
          this.tipoTratamientoFrmCtrl.setValue(
            (response.data[0].descripTipoTratamiento !== null) ?
              response.data[0].descripTipoTratamiento : PREFERENCIAINSTI.noRegistrado);

          if (!precargar) {
            // CONTIENE LA LOGICA SI CUMPLE O NO CUMPLE
            this.logicaPreferenciaInstitucional(response);
          } else {
            this.condicionCancerFrmCtrl.setValue(this.lineaTratRequest.condicionCancer);
          }

          this.lineaTratRequest.cumplePrefeInsti = this.rgPrefInstiFrmCtrl.value;
          this.lineaTratRequest.codMac = this.solicitud.codMac;
          // this.rgPrefInsti = this.rgPrefInstiFrmCtrl.value;

        } else if (response.data.length > 1) {
          let descripcionTratamiento = '';
          let presentacionNoPermitida = '';
          let descripcionTipoTratamiento = '';
          this.tratamientoLabel = PREFERENCIAINSTI.tratamientoGuia;
          this.tipoTratamientoLabel = PREFERENCIAINSTI.tipoTratamientoGuia;
          this.cumplePrefeDesaLabel = PREFERENCIAINSTI.cumplePrefeGuia;

          this.detalleCondicionFrmCtrl.setValue(response.data[0].detalleCondicion);
          this.tipoTratamientoFrmCtrl.setValue(response.data[0].descripTipoTratamiento);

          response.data.forEach(element => {
            if (element.presentacNoPermitida !== null) {
              presentacionNoPermitida = presentacionNoPermitida + element.presentacNoPermitida + ' / ';
            } else {
              presentacionNoPermitida = presentacionNoPermitida + PREFERENCIAINSTI.noRegistrado + ' / ';
            }

            descripcionTratamiento = descripcionTratamiento +
              ((element.descripTratamiento !== null) ? element.descripTratamiento : PREFERENCIAINSTI.noRegistrado) + ' / ';

            descripcionTipoTratamiento = descripcionTipoTratamiento +
              ((element.descripTipoTratamiento !== null) ? element.descripTipoTratamiento : PREFERENCIAINSTI.noRegistrado) + ' / ';
          });
          descripcionTratamiento = descripcionTratamiento.substring(0, descripcionTratamiento.length - 3);
          presentacionNoPermitida = presentacionNoPermitida.substring(0, presentacionNoPermitida.length - 3);
          descripcionTipoTratamiento = descripcionTipoTratamiento.substring(0, descripcionTipoTratamiento.length - 3);
          this.tratamientoFrmCtrl.setValue(descripcionTratamiento);
          this.presentaNoPermitidaFrmCtrl.setValue(presentacionNoPermitida);
          this.tipoTratamientoFrmCtrl.setValue(descripcionTipoTratamiento);
          this.rgPrefInstiFrmCtrl.setValue(null);
        } else {
          this.detalleCondicionFrmCtrl.setValue(PREFERENCIAINSTI.noRegistrado);
          this.tratamientoFrmCtrl.setValue(PREFERENCIAINSTI.noRegistrado);
          this.presentaNoPermitidaFrmCtrl.setValue(PREFERENCIAINSTI.noRegistrado);
          this.tipoTratamientoFrmCtrl.setValue(PREFERENCIAINSTI.noRegistrado);
          this.rgPrefInstiFrmCtrl.setValue(2);
          this.lineaTratRequest.cumplePrefeInsti = this.rgPrefInstiFrmCtrl.value;
        }

        if (precargar) {
          this.rgPrefInstiFrmCtrl.setValue(this.lineaTratRequest.cumplePrefeInsti);
          this.observacionFrmCtrl.setValue(this.lineaTratRequest.observacion);
        }

        this.spinnerService.hide();

        if(this.grabarPaso === '1') {
          this.observacionFrmCtrl.enable();
        }

      }, error => {
        console.error(error);
        this.spinnerService.hide();
        this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al buscar preferencias.', true, false, null);
      }
    );
  }

  public preferenciaIntitucional(cambioCumplePrefeInsti: number) {
    if (this.condicionCancerFrmCtrl.value !== null &&
      cambioCumplePrefeInsti !== this.lineaTratRequest.cumplePrefeInsti) {
        this.flagObservacions = true; // this.observacionFrmCtrl.enable();
    } else {
      this.observacionFrmCtrl.setValue(null);
      this.flagObservacions = false; // this.observacionFrmCtrl.disable();
    }
    
  }

  // POP-UP MENSAJES
  public openDialogMensaje(
    message: String,
    message2: String,
    alerta: boolean,
    confirmacion: boolean,
    valor: any): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      disableClose: true,
      width: '400px',
      data: {
        title: MENSAJES.medicNuevo.lineTrataPrefInst.TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(rspta => {
    });
  }


  public accesoOpcionMenu() {

    const data = require('src/assets/data/permisosRecursos.json');
    const bandejaEvaluacion = data.bandejaEvaluacion.paso1;
    this.opcionMenu = JSON.parse(localStorage.getItem('opcionMenu'));

    if (this.opcionMenu.opcion.length > 0) {
      this.opcionMenu.opcion.forEach(element => {
        const codOpcion = element.codOpcion;
        switch (codOpcion) {
          case bandejaEvaluacion.txtNumeroLinea:
            this.txtNumeroLinea = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbNumeroCurso:
            this.cmbNumeroCurso = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbTipoTumor:
            this.cmbTipoTumor = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbRespuestaAlcanzada:
            this.cmbRespuestaAlcanzada = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbLugarProgresion:
            this.cmbLugarProgresion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtGrupoDiagnostico:
            this.txtGrupoDiagnostico = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbCondicion:
            this.cmbCondicion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbDetalleCondicion:
            this.cmbDetalleCondicion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtTratamiento:
            this.txtTratamiento = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtPresentacion:
            this.txtPresentacion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtTipoTratamiento:
            this.txtTipoTratamiento = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtReferencia:
            this.txtReferencia = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtObservacion:
            this.txtObservacion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnGrabar:
            this.btnGrabar = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnSalir:
            this.btnSalir = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnSiguientePaso1:
            this.btnSiguientePaso1 = Number(element.flagAsignacion);
            break;
        }
      });
    }
  }
}
