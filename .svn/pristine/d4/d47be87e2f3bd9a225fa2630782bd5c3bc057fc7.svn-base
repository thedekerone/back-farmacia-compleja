package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.config.ConfigFTPProp;
import pvt.auna.fcompleja.dao.MacDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionRequest;
import pvt.auna.fcompleja.model.bean.CheckListBean;
import pvt.auna.fcompleja.model.bean.ComplicacionesBean;
import pvt.auna.fcompleja.model.bean.CriteriosBean;
import pvt.auna.fcompleja.model.bean.FichaTecnicaBean;
import pvt.auna.fcompleja.model.bean.MACBean;
import pvt.auna.fcompleja.model.mapper.CheckListBeanRowMapper;
import pvt.auna.fcompleja.model.mapper.ComplicacionesMedicasRowMapper;
import pvt.auna.fcompleja.model.mapper.CriteriosBeanRowMapper;
import pvt.auna.fcompleja.model.mapper.FichaTecnicaRowMapper;
import pvt.auna.fcompleja.model.mapper.MACBeanRowMapper;
import pvt.auna.fcompleja.service.GenericoService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;

@SuppressWarnings("unchecked")
@Repository
public class MacDaoImpl implements MacDao {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	DataSource dataSource;
	
	@Autowired
	GenericoService genericoService;
	
	@Autowired 
	ConfigFTPProp configFTPProp;

	@Override
	public ApiOutResponse listFiltroMAC(MACBean filtro) {
		ApiOutResponse apiOut = new ApiOutResponse();
		ArrayList<MACBean> lista = new ArrayList<>();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_S_FILTRO_MAC")
				.returningResultSet("TC_LIST", new MACBeanRowMapper());

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();
			
			in.addValue("PV_P_COD", filtro.getCodigoLargo(), Types.VARCHAR);
			in.addValue("PV_P_DES", filtro.getDescripcion(), Types.VARCHAR);
			in.addValue("PV_P_COM", filtro.getNombreComercial(), Types.VARCHAR);
			in.addValue("PV_TIPO", (filtro.getBusqueda() != null) ? filtro.getBusqueda() : "0", Types.VARCHAR);
			
			/*
			 in.addValue("PV_P_COD", filtro.getDescripcion(), Types.VARCHAR);
			in.addValue("PV_P_DES", filtro.getNombreComercial(), Types.VARCHAR);
			in.addValue("PV_P_COM", filtro.getCodigoLargo(), Types.VARCHAR);
			in.addValue("PV_TIPO", (filtro.getBusqueda() != null) ? filtro.getBusqueda() : "0", Types.VARCHAR);
			 */
			
			out = simpleJdbcCall.execute(in);

			lista = (ArrayList<MACBean>) out.get("TC_LIST");
			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MESSAGE"));
			apiOut.setResponse(lista);

			if (apiOut.getCodResultado() == -1) {
				throw new Exception(apiOut.getMsgResultado());
			}

		} catch (Exception e) {
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
			LOGGER.error(e.getMessage());
			e.printStackTrace();
		}

		return apiOut;
	}

	@Override
	public ApiOutResponse registroMAC(MACBean objRegistro) {

		ApiOutResponse apiOut = new ApiOutResponse();
		MACBean mac = new MACBean();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_I_REGISTRO_MAC");

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_MAC", objRegistro.getCodigo(), Types.NUMERIC);
			in.addValue("PV_DESCRIPCION", objRegistro.getDescripcion(), Types.VARCHAR);
			in.addValue("PN_P_TIPO_MAC", objRegistro.getTipoMac(), Types.NUMERIC);
			in.addValue("PN_P_ESTADO_MAC", objRegistro.getEstadoMac(), Types.NUMERIC);
			in.addValue("pd_fecha_inscrip", objRegistro.getFechaInscripcion(), Types.DATE);
			in.addValue("pd_fec_inicio", objRegistro.getFechaInicioVigencia(), Types.DATE);
			in.addValue("pd_fec_fin", objRegistro.getFechaFinVigencia(), Types.DATE);
			in.addValue("pv_fec_creacion", DateUtils.getDateToStringDDMMYYYHHMMSS(objRegistro.getFechaCreacion()),
					Types.VARCHAR);
			in.addValue("pn_user_creacion", objRegistro.getCodUsuario(), Types.NUMERIC);
			in.addValue("pv_fec_modifica", DateUtils.getDateToStringDDMMYYYHHMMSS(objRegistro.getFechaModificacion()),
					Types.VARCHAR);
			in.addValue("pn_user_modifica", objRegistro.getCodUsuarioMod(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			mac.setCodigo(Integer.parseInt(out.get("PN_COD_OUT_MAC").toString()));
			mac.setCodigoLargo((String) out.get("PV_COD_LARGO_MAC"));

			apiOut.setResponse(mac);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	@Override
	public ApiOutResponse listaCheckListConfiguracion(CheckListBean obj) {

		ApiOutResponse apiOut = new ApiOutResponse();
		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_s_config_checklist")
				.returningResultSet("OP_OBJCURSOR", new CheckListBeanRowMapper());

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("pn_cod_grp_diag", obj.getCodGrpDiag(), Types.NUMERIC);
			in.addValue("pn_cod_mac", obj.getCodMac(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}
			apiOut.setResponse((ArrayList<CheckListBean>) out.get("OP_OBJCURSOR"));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	@Override
	public ApiOutResponse registroCheckListConfiguracion(CheckListBean obj) {

		ApiOutResponse apiOut = new ApiOutResponse();
		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_i_registro_cheklist");

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("pn_codMac", obj.getCodMac(), Types.NUMERIC);
			in.addValue("pn_grpDiagnostico", obj.getCodGrpDiag(), Types.NUMERIC);
			in.addValue("pn_codChkListIndi", obj.getCodChkListIndi(), Types.NUMERIC);
			in.addValue("pv_descripcion", obj.getDescripcion(), Types.VARCHAR);
			in.addValue("pn_pEstado", obj.getCodEstado(), Types.NUMERIC);
			in.addValue("pd_fechaIniVigencia", obj.getFechaIniVigencia(), Types.DATE);
			in.addValue("pd_fechaFinVigencia", obj.getFechaFinVigencia(), Types.DATE);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			obj.setCodChkListIndi(Integer.parseInt(out.get("PN_OUT_CODCHKLISTINDI").toString()));
			obj.setCodIndicadorLargo((String) out.get("PV_OUT_CODCHKLARGO"));

			apiOut.setResponse(obj);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	public ApiOutResponse listarCriterioInclusion(CriteriosBean obj) {
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_s_lista_critInclusion")
				.returningResultSet("TC_LIST", new CriteriosBeanRowMapper());

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("pn_codChkListIndi", obj.getCodChkListIndi(), Types.NUMERIC);
			in.addValue("pn_cod_mac", obj.getCodMac(), Types.NUMERIC);
			in.addValue("pn_grp_diag", obj.getCodGrpDiag(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			apiOut.setResponse((ArrayList<CriteriosBean>) out.get("TC_LIST"));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	public ApiOutResponse listarCriterioExclusion(CriteriosBean obj) {
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_S_LISTA_CRITEXCLUSION")
				.returningResultSet("TC_LIST", new CriteriosBeanRowMapper());

		try {

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_CODCHKLISTINDI", obj.getCodChkListIndi(), Types.NUMERIC);
			in.addValue("PN_COD_MAC", obj.getCodMac(), Types.NUMERIC);
			in.addValue("PN_GRP_DIAG", obj.getCodGrpDiag(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			apiOut.setResponse((ArrayList<CriteriosBean>) out.get("TC_LIST"));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	public ApiOutResponse registroCriterioExclusion(CriteriosBean obj) {

		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_i_regCritExclusion");

		try {

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("pn_codChkListIndi", obj.getCodChkListIndi(), Types.NUMERIC);
			in.addValue("pn_cod_mac", obj.getCodMac(), Types.NUMERIC);
			in.addValue("pn_grp_diag", obj.getCodGrpDiag(), Types.NUMERIC);
			in.addValue("pn_codExclusion", obj.getCodCriterio(), Types.NUMERIC);
			in.addValue("pn_descripcriterio", obj.getDescripcion(), Types.VARCHAR);
			in.addValue("pn_estado", obj.getCodEstado(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			obj.setCodCriterio(Integer.parseInt(out.get("PN_OUT_CODEXCLUSION").toString()));
			obj.setCodCriterioLargo((String) out.get("PN_OUT_CODEXCLUSION").toString());
			apiOut.setResponse(obj);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	public ApiOutResponse registroCriterioInclusion(CriteriosBean obj) {
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_i_regCritInclusion");

		MapSqlParameterSource in = new MapSqlParameterSource();

		try {
			in.addValue("pn_codChkListIndi", obj.getCodChkListIndi(), Types.NUMERIC);
			in.addValue("pn_cod_mac", obj.getCodMac(), Types.NUMERIC);
			in.addValue("pn_grp_diag", obj.getCodGrpDiag(), Types.NUMERIC);
			in.addValue("pn_codInclusion", obj.getCodCriterio(), Types.NUMERIC);
			in.addValue("pn_descripCriterio", obj.getDescripcion(), Types.VARCHAR);
			in.addValue("pn_estado", obj.getEstado(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			obj.setCodCriterio(Integer.parseInt(out.get("PN_OUT_CODINCLUSION").toString()));

			apiOut.setResponse(obj);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	public ApiOutResponse registroFichaTecnica(FichaTecnicaBean obj) {
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_i_registroFicha");

		try {

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("pn_codVersion", obj.getCodVersion(), Types.NUMERIC);
			in.addValue("pv_nombreArchivo", obj.getNombreArchivo(), Types.VARCHAR);
			in.addValue("pn_estado", obj.getCodEstado(), Types.NUMERIC);
			in.addValue("pn_codMac", obj.getCodMac(), Types.NUMERIC);
			in.addValue("pn_codigo_ficha", obj.getCodFichaTecnica(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}


			apiOut.setResponse(obj);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	public ApiOutResponse listarFichasTecnicas(FichaTecnicaBean obj) {
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_s_listarFichas")
				.returningResultSet("TC_LIST", new FichaTecnicaRowMapper());

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("pn_codMac", obj.getCodMac(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			apiOut.setResponse((ArrayList<FichaTecnicaBean>) out.get("TC_LIST"));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	@Override
	public ApiOutResponse registroComplicacionesMedicas(ComplicacionesBean obj) {
		// TODO Auto-generated method stub
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_i_registro_comp");

		try {

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("pv_cod_version", obj.getCodVersion(), Types.VARCHAR);
			in.addValue("pv_nombre_archivo", obj.getNombreArchivo(), Types.VARCHAR);
			in.addValue("pn_estado", obj.getCodEstado(), Types.NUMERIC);
			in.addValue("pn_codMac", obj.getCodMac(), Types.NUMERIC);
			in.addValue("pn_codigo_archivo_comp", obj.getCodigoArchivoComp(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			

			apiOut.setResponse(obj);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	@Override
	public ApiOutResponse listaComplicacionesMedicas(ComplicacionesBean obj) {
		// TODO Auto-generated method stub
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_s_listarComp")
				.returningResultSet("TC_LIST", new ComplicacionesMedicasRowMapper());

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("pn_codMac", obj.getCodMac(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			apiOut.setResponse((ArrayList<ComplicacionesBean>) out.get("TC_LIST"));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	@Override
	public ApiOutResponse registrarIndicacionCriterios(CheckListBean obj) {
		Map<String, Object> out = new HashMap<>();

		ApiOutResponse response = new ApiOutResponse();

		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_CONFIGURACION_SISTEMA)
					.withProcedureName("SP_PFC_I_CHKLIST_INDICACION");

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_CHKLIST_INDI", obj.getCodChkListIndi(), Types.NUMERIC);
			in.addValue("PN_COD_CHKLIST_INDI_LARGO", obj.getCodIndicadorLargo(), Types.VARCHAR);
			in.addValue("PV_DESCRIPCION", obj.getDescripcion(), Types.VARCHAR);
			in.addValue("PN_P_ESTADO", obj.getCodEstado(), Types.NUMERIC);
			in.addValue("PN_COD_MAC", obj.getCodMac(), Types.NUMERIC);
			in.addValue("PN_COD_GRP_DIAG", obj.getCodGrpDiag(), Types.NUMERIC);
			in.addValue("PV_L_CRITERIO_INCLU", obj.getlCriterioInclusion(), Types.VARCHAR);
			in.addValue("PV_L_CRITERIO_EXCLU", obj.getlCriterioExclusion(), Types.VARCHAR);
			
			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			response.setResponse(obj);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Error al listar : ", e);
		}
		return response;
	}

}
