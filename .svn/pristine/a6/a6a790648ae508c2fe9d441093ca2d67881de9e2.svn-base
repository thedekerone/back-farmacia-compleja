package pvt.auna.fcompleja.controller.api;

import org.jfree.util.Log;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ApiResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ClinicaRequest;
import pvt.auna.fcompleja.model.api.DiagnosticoRequest;
import pvt.auna.fcompleja.model.api.PacienteRequest;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.bean.ClinicaBean;
import pvt.auna.fcompleja.model.bean.DiagnosticoBean;
import pvt.auna.fcompleja.model.bean.InfoSolbenBean;
import pvt.auna.fcompleja.model.bean.PacienteBean;
import pvt.auna.fcompleja.model.api.request.InformacionScgPreRequest;
import pvt.auna.fcompleja.service.DetalleSolicitudPreliminarService;
import pvt.auna.fcompleja.service.OncoClinicaService;
import pvt.auna.fcompleja.service.OncoDiagnosticoService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

import java.util.Date;
import java.util.HashMap;

@RestController
@RequestMapping("/")
public class DetalleSolicitudPreliminarController {

	@Autowired
	private DetalleSolicitudPreliminarService detalleSolicitudPreliminarService;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	OncoPacienteService pcteService;

	@Autowired
	OncoClinicaService clinService;

	@Autowired
	OncoDiagnosticoService diagService;

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(DetalleSolicitudPreliminarController.class);

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/informacionScgPre", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> consultarInformacionScgPre(@RequestBody InformacionScgPreRequest request)
			throws Exception {
		LOGGER.info("API method: INFO DETALLE PRELIMINAR");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("[SERVICIO:  INFO DETALLE PRELIMINAR][INICIO]");
		LOGGER.info("[SERVICIO:  INFO DETALLE PRELIMINAR][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info(
				"[SERVICIO:  INFO DETALLE PRELIMINAR][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  INFO DETALLE PRELIMINAR][REQUEST][BODY][" + request.toString() + "]");

		ApiOutResponse service = null;

		try {
			service = detalleSolicitudPreliminarService.consultarInformacionScgPre(request);

			if (service != null && service.getResponse() != null) {
				audiResponse.setCodigoRespuesta("0");
				audiResponse.setFechaTransaccion(fechaTransaccion);
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setMensajeRespuesta("Información Obtenida.");

				InfoSolbenBean infoSolben = (InfoSolbenBean) service.getResponse();

				PacienteBean pacienteBean = obtenerPacienteOncosys(infoSolben, headers);
				ClinicaBean clinicaBean = obtenerClinicaOncosys(infoSolben, headers);
				DiagnosticoBean diagnosticoBean = obtenerDiagnosticoOncosys(infoSolben, headers);

				infoSolben.setPaciente((pacienteBean != null) ? pacienteBean.getApelNomb() : "");
				infoSolben.setClinica((clinicaBean != null) ? clinicaBean.getNomcli() : "");
				infoSolben.setDiagnostico((diagnosticoBean != null) ? diagnosticoBean.getDiagnostico() : "");
				infoSolben.setCodGrupoDiagnostico((diagnosticoBean != null && diagnosticoBean.getGrupo() != null)
						? diagnosticoBean.getGrupo().getCodigo()
						: "");
				infoSolben.setGrupoDiagnostico((diagnosticoBean != null && diagnosticoBean.getGrupo() != null)
						? diagnosticoBean.getGrupo().getDescripcion()
						: "");

			} else {
				LOGGER.warn("No existe información que mostrar");
				audiResponse.setCodigoRespuesta((service != null) ? service.getCodResultado().toString() : "99");
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(fechaTransaccion);
				audiResponse.setMensajeRespuesta((service != null) ? service.getMsgResultado() : "Ocurrio un error.");
			}

			response.setAudiResponse(audiResponse);
			response.setData((InfoSolbenBean) service.getResponse());

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Error lista al get scg solben");
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(fechaTransaccion);
			audiResponse.setMensajeRespuesta("Ocurrio un error.");
			response.setAudiResponse(audiResponse);
			response.setData(service);
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private PacienteBean obtenerPacienteOncosys(InfoSolbenBean infoSolben, HttpHeaders headers) {
		PacienteBean paciente = null;
		try {
			PacienteRequest pcteRq = new PacienteRequest();
			pcteRq.setPvTibus(3);
			pcteRq.setCodigoAfiliado(infoSolben.getCodAfiliado());
			pcteRq.setTipoDocumento("''");
			pcteRq.setNumeroDocumento("''");
			pcteRq.setNombres("''");
			pcteRq.setApellidoPaterno("''");
			pcteRq.setApellidoMaterno("''");

			paciente = (PacienteBean) pcteService.obtenerPaciente(pcteRq, headers).getData();

		} catch (Exception e) {
			Log.error("obtenerPacienteOncosys: No se logró obtener el Paciente de Oncosys. " + e.getMessage());
		}

		return paciente;
	}

	private ClinicaBean obtenerClinicaOncosys(InfoSolbenBean infoSolben, HttpHeaders headers) {
		ClinicaBean clinicaBean = null;
		try {
			ClinicaRequest cliRq = new ClinicaRequest();
			cliRq.setIni(ConstanteUtil.registroIni);
			cliRq.setFin(ConstanteUtil.registroFinClinicas);
			cliRq.setTipoBus(ConstanteUtil.tipoXCod);
			cliRq.setCodcli(infoSolben.getCodClinica());
			cliRq.setNomcli("''");

			clinicaBean = (ClinicaBean) clinService.obtenerClinica(cliRq, headers).getData();
		} catch (Exception e) {
			Log.error("obtenerClinicaOncosys: No se logró obtener clínica de Oncosys. " + e.getMessage());
			clinicaBean = null;
		}
		return clinicaBean;
	}

	private DiagnosticoBean obtenerDiagnosticoOncosys(InfoSolbenBean infoSolben, HttpHeaders headers) {
		DiagnosticoBean diagnostico = null;

		try {
			DiagnosticoRequest diaRq = new DiagnosticoRequest();
			diaRq.setRegistroInicio(ConstanteUtil.registroIni);
			diaRq.setRegistroFin(ConstanteUtil.registroIni);
			diaRq.setTipoBusqueda(ConstanteUtil.tipoXCod);
			diaRq.setCodigoDiagnostico(infoSolben.getCie10());
			diaRq.setNombreDiagnostico("");

			diagnostico = (DiagnosticoBean) diagService.obtenerDiagnostico(headers, diaRq).getData();
		} catch (Exception e) {
			Log.error("obtenerDiagnosticoOncosys: " + e.getMessage());
			diagnostico = null;
		}

		return diagnostico;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/updateEstadoSolicitudPreliminar", produces = "application/json; charset=UTF-8")
	public ResponseEntity<ApiResponse> updateEstadoSolicitudPreliminar(@RequestBody InformacionScgPreRequest request)
			throws Exception {

		LOGGER.info("API method: updateEstadoSolicitudPreliminar");

		ApiResponse response = detalleSolicitudPreliminarService.updateEstadoSolicitudPreliminar(request);

		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/actualizarEstado", produces = "application/json; charset=UTF-8")
	public ResponseEntity<ApiResponse> actualizarEstadoPreliminar(@RequestBody InformacionScgPreRequest request) {

		ApiResponse response = new ApiResponse();
		HashMap<?, ?> hashMap = new HashMap<>();

		try {
			hashMap = detalleSolicitudPreliminarService.actualizarEstadoPreliminar(request);

			response.setStatus((String) hashMap.get("status"));
			response.setMessage((String) hashMap.get("message"));
			response.setResponse(null);

		} catch (Exception e) {
			LOGGER.error("ERROR:   " + e);
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/updateEstadoPendInscriptionMAC", produces = "application/json; charset=UTF-8")
	public ResponseEntity<ApiResponse> updateEstadoPendInscriptionMAC(@RequestBody InformacionScgPreRequest request)
			throws Exception {

		LOGGER.info("API method: updateEstadoPendInscriptionMAC");

		ApiResponse response = detalleSolicitudPreliminarService.updateEstadoPendInscriptionMAC(request);

		return new ResponseEntity<>(response, HttpStatus.OK);

	}

}
