package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.dao.BandejaSolicitudPreliminarDao;
import pvt.auna.fcompleja.model.api.response.ListSolPreeliminarResponse;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.SolicitudesPreeliminarFiltroRequest;
import pvt.auna.fcompleja.model.mapper.ListadoRowMapper;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;

@Repository
public class BandejaSolicitudPreliminarDaoImpl implements BandejaSolicitudPreliminarDao {

	private static final Logger log = LoggerFactory.getLogger(BandejaSolicitudPreliminarDaoImpl.class);

	@Autowired
	DataSource dataSource;

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse listarSolicitudes(SolicitudesPreeliminarFiltroRequest objA_preeliminar) {
		List<ListSolPreeliminarResponse> ListSolPreeliminarBean = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();

		MapSqlParameterSource in = new MapSqlParameterSource();

		try {			
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_PRELIMINAR).withProcedureName("SP_PFC_S_BANDEJA_PRELIMINAR")
					.returningResultSet("AC_LISTA_DETALLE_PREELIMINAR", new ListadoRowMapper());

			in.addValue("PN_ID_SOL_PRELIMINAR", objA_preeliminar.getCodigoPreliminar(), Types.NUMERIC);
			//in.addValue("PV_PACIENTE", objA_preeliminar.getCodigoPaciente(), Types.VARCHAR);
			in.addValue("PV_COD_SCG_SOLBEN", objA_preeliminar.getCodigoScgSolben(), Types.VARCHAR);
			in.addValue("PV_TIPO_SCG_SOLBEN", objA_preeliminar.getTipoScgSolben(), Types.VARCHAR);
			in.addValue("PV_CLINICA", objA_preeliminar.getCodigoClinica(), Types.VARCHAR);
			in.addValue("PV_FECHA_INI_REGISTRO_SOL",
					DateUtils.getDateToStringDDMMYYYY(objA_preeliminar.getFechaInicio_pre()), Types.VARCHAR);
			in.addValue("PV_FECHA_FIN_REGISTRO_SOL",
					DateUtils.getDateToStringDDMMYYYY(objA_preeliminar.getFechaFinPre()), Types.VARCHAR);
			in.addValue("PV_ESTADO_SOL_PRE", objA_preeliminar.getEstadoPre(), Types.VARCHAR);
			in.addValue("PV_AUTORIZADOR_PERTENENCIA", objA_preeliminar.getAutorizadorPertenencia(), Types.VARCHAR);
			in.addValue("PN_INDEX", objA_preeliminar.getIndex(), Types.NUMERIC);
			in.addValue("PN_LONGITUD", objA_preeliminar.getSize(), Types.NUMERIC);
			in.addValue("PV_NOMBRE_PAC", objA_preeliminar.getNombre(), Types.VARCHAR);
			in.addValue("PV_APE_PAT_PAC", objA_preeliminar.getApePaterno(), Types.VARCHAR);
			in.addValue("PV_APE_MAT_PAC", objA_preeliminar.getApeMaterno(), Types.VARCHAR);
			in.addValue("PV_TIPO_DOC",objA_preeliminar.getTipoDoc(), Types.VARCHAR);
			in.addValue("PV_NUM_DOC", objA_preeliminar.getNroDoc(), Types.VARCHAR);
			
			out = (Map<String, Object>) simpleJdbcCall.execute(in);

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			
			outResponse.setTotal(Integer.parseInt(out.get("PN_TOTAL").toString()));
			ListSolPreeliminarBean = (List<ListSolPreeliminarResponse>) out.get("AC_LISTA_DETALLE_PREELIMINAR");
			outResponse.setResponse(ListSolPreeliminarBean);
			log.info((String)out.get("PV_MSG_RESULTADO"));

		} catch (Exception e) {
			outResponse.setCodResultado(99);
			outResponse.setMsgResultado(e.getMessage());
			log.error(outResponse.getMsgResultado());
			log.error("Error Lista de Solicitudes: ", e);
		}
		
		return outResponse;
	}

}
