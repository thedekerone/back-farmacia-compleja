package pvt.auna.fcompleja.service;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.EmailResponse;
import pvt.auna.fcompleja.model.api.ListaBandeja;
import pvt.auna.fcompleja.model.api.TipoCorreoResponse;
import pvt.auna.fcompleja.model.api.request.EmailAlertaCmacRequest;
import pvt.auna.fcompleja.model.api.request.EmailAlertaLiderTumorRequest;
import pvt.auna.fcompleja.model.api.request.EmailAlertaMonitoreoRequest;
import pvt.auna.fcompleja.model.api.request.EmailSolicInscripRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.response.EmailAlertaMonitoreoResponse;
import pvt.auna.fcompleja.model.api.response.EmailAlertaResponse;
import pvt.auna.fcompleja.model.api.response.EmailSolicInscripResponse;
import pvt.auna.fcompleja.model.api.response.ResponseGenerico;
import pvt.auna.fcompleja.model.api.response.evaluacion.ProgramacionCmacDetResponse;
import pvt.auna.fcompleja.model.bean.EmailBean;
import pvt.auna.fcompleja.model.bean.Mail;

public interface EmailService {
	
	public EmailBean obtenerDatosCorreo(Long codEmail) throws Exception;

	public TipoCorreoResponse obtenerDatoTipoCorreo(int codTipoEmail) throws Exception;

	public List<EmailSolicInscripResponse> sendSolicitudInscripcionMac(Mail mail, EmailSolicInscripRequest solInsrequest, byte[] byteFile) throws MessagingException, Exception;

	public List<EmailAlertaMonitoreoResponse> enviarEmailAlertaMonitoreo(EmailResponse emailResponse, EmailAlertaMonitoreoRequest request) throws MessagingException, Exception;

	public List<EmailAlertaResponse> enviarEmailAlertaCmac(EmailResponse emailResponse, EmailAlertaCmacRequest request) throws MessagingException, Exception;

	public List<EmailAlertaMonitoreoResponse> enviarEmailAlertaLiderTumor(
			Mail mail,
			EmailAlertaLiderTumorRequest request,
			byte[] byteFile1,byte[] byteFile2,byte[] byteFile3,byte[] byteFile4,byte[] byteFile5) throws MessagingException, Exception;

	public ResponseGenerico generarCorreo(EmailBean obj, HttpHeaders headers);

	public ResponseGenerico enviarCorreo(EmailBean obj, HttpHeaders headers);
	
	public ResponseGenerico enviarCorreoReunionMac(EmailBean obj, HttpHeaders headers);
	
	public ResponseGenerico verificarEnvioCorreo(EmailBean obj, HttpHeaders headers);
	
	public ApiOutResponse actParamsCorreoLiderTumor(SolicitudEvaluacionRequest solicitud);
	
	public String reenviarCorreoLiderTumor(ListaBandeja request, HttpHeaders headers);
	
	public String reenviarCorreoCmac(ListaBandeja request, HttpHeaders headers) throws Exception;
	
	public String actualizarCorreosPendLiderTumor(ListaBandeja request, HttpHeaders headers);
	
	public void actualizarCorreosPendLiderTumorV2(ListaBandeja request, HttpHeaders headers);
	
	public String actualizarCorreosPendCmac(ListaBandeja request, HttpHeaders headers) throws Exception;
	
	public void actualizarCorreosPendCmacV2(ListaBandeja request, HttpHeaders headers) throws Exception;
	
	public ResponseGenerico enviarCorreoGenerico(EmailBean obj, HttpHeaders headers);
	
	public ResponseGenerico reintentarEnvioCorreo(EmailBean obj, HttpHeaders headers);
	
	public ApiOutResponse actualizarEstSolicitudEvaluacion(EmailBean request);
	
	public ApiOutResponse actualizarEstCorreoSolEvaluacion(EmailBean request);
	
	public ApiOutResponse actualizarEstCorreoLidTumSolEvalucion(EmailBean request);
	
	public ApiOutResponse obtenerDatosAutorizadorPert(List<ProgramacionCmacDetResponse> request, HttpHeaders headers);

}
