package pvt.auna.fcompleja.model.bean;

public class ConclusionBean {
	
	private Integer codAnalisisConclusion;
	private String  medicamentoEvaluado;
	private Integer valCumpleChkPaciente;
	private String  cumpleChkPaciente;
	private Integer valCumplePreferenciaInst;
	private String  cumplePreferenciaInst;
	private String  descripcion;
	private String  medicamentoPreferido;
	private Integer codMac;
	private Integer valPertinencia;
	private String  pertinencia;
	private Integer valTiempoUso;
	private String  tiempoUso;
	private Integer valCondicionPaciente;
	private String  condicionPaciente;
	private Integer valResultadoAutorizador;
	private String  resultadoAutorizador;
	private String  comentario;
	private Integer nroLineaTratamiento;
	private String	grabar;
	
	
	public String getMedicamentoEvaluado() {
		return medicamentoEvaluado;
	}
	public void setMedicamentoEvaluado(String medicamentoEvaluado) {
		this.medicamentoEvaluado = medicamentoEvaluado;
	}
	public String getCumpleChkPaciente() {
		return cumpleChkPaciente;
	}
	public void setCumpleChkPaciente(String cumpleChkPaciente) {
		this.cumpleChkPaciente = cumpleChkPaciente;
	}
	public String getCumplePreferenciaInst() {
		return cumplePreferenciaInst;
	}
	public void setCumplePreferenciaInst(String cumplePreferenciaInst) {
		this.cumplePreferenciaInst = cumplePreferenciaInst;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getMedicamentoPreferido() {
		return medicamentoPreferido;
	}
	public void setMedicamentoPreferido(String medicamentoPreferido) {
		this.medicamentoPreferido = medicamentoPreferido;
	}
	public String getPertinencia() {
		return pertinencia;
	}
	public void setPertinencia(String pertinencia) {
		this.pertinencia = pertinencia;
	}
	public String getTiempoUso() {
		return tiempoUso;
	}
	public void setTiempoUso(String tiempoUso) {
		this.tiempoUso = tiempoUso;
	}
	
	public String getResultadoAutorizador() {
		return resultadoAutorizador;
	}
	public void setResultadoAutorizador(String resultadoAutorizador) {
		this.resultadoAutorizador = resultadoAutorizador;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Integer getNroLineaTratamiento() {
		return nroLineaTratamiento;
	}
	public void setNroLineaTratamiento(Integer nroLineaTratamiento) {
		this.nroLineaTratamiento = nroLineaTratamiento;
	}
	public Integer getCodAnalisisConclusion() {
		return codAnalisisConclusion;
	}
	public void setCodAnalisisConclusion(Integer codAnalisisConclusion) {
		this.codAnalisisConclusion = codAnalisisConclusion;
	}
	public Integer getValCumpleChkPaciente() {
		return valCumpleChkPaciente;
	}
	public void setValCumpleChkPaciente(Integer valCumpleChkPaciente) {
		this.valCumpleChkPaciente = valCumpleChkPaciente;
	}
	public Integer getValCumplePreferenciaInst() {
		return valCumplePreferenciaInst;
	}
	public void setValCumplePreferenciaInst(Integer valCumplePreferenciaInst) {
		this.valCumplePreferenciaInst = valCumplePreferenciaInst;
	}
	public Integer getCodMac() {
		return codMac;
	}
	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}
	public Integer getValPertinencia() {
		return valPertinencia;
	}
	public void setValPertinencia(Integer valPertinencia) {
		this.valPertinencia = valPertinencia;
	}
	public Integer getValTiempoUso() {
		return valTiempoUso;
	}
	public void setValTiempoUso(Integer valTiempoUso) {
		this.valTiempoUso = valTiempoUso;
	}
	public Integer getValCondicionPaciente() {
		return valCondicionPaciente;
	}
	public void setValCondicionPaciente(Integer valCondicionPaciente) {
		this.valCondicionPaciente = valCondicionPaciente;
	}
	public String getCondicionPaciente() {
		return condicionPaciente;
	}
	public void setCondicionPaciente(String condicionPaciente) {
		this.condicionPaciente = condicionPaciente;
	}
	public Integer getValResultadoAutorizador() {
		return valResultadoAutorizador;
	}
	public void setValResultadoAutorizador(Integer valResultadoAutorizador) {
		this.valResultadoAutorizador = valResultadoAutorizador;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	@Override
	public String toString() {
		return "ConclusionBean [codAnalisisConclusion=" + codAnalisisConclusion + ", medicamentoEvaluado="
				+ medicamentoEvaluado + ", valCumpleChkPaciente=" + valCumpleChkPaciente + ", cumpleChkPaciente="
				+ cumpleChkPaciente + ", valCumplePreferenciaInst=" + valCumplePreferenciaInst
				+ ", cumplePreferenciaInst=" + cumplePreferenciaInst + ", descripcion=" + descripcion
				+ ", medicamentoPreferido=" + medicamentoPreferido + ", codMac=" + codMac + ", valPertinencia="
				+ valPertinencia + ", pertinencia=" + pertinencia + ", valTiempoUso=" + valTiempoUso + ", tiempoUso="
				+ tiempoUso + ", valCondicionPaciente=" + valCondicionPaciente + ", condicionPaciente="
				+ condicionPaciente + ", valResultadoAutorizador=" + valResultadoAutorizador + ", resultadoAutorizador="
				+ resultadoAutorizador + ", comentario=" + comentario + ", nroLineaTratamiento=" + nroLineaTratamiento
				+ ", grabar=" + grabar + "]";
	}
	
}
