package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.ListRolResponsableResponse;

public class ListRolResponsableRowMapper implements RowMapper<ListRolResponsableResponse>{

	@Override
	public ListRolResponsableResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub}
		ListRolResponsableResponse listRolResponsableResponse = new ListRolResponsableResponse();
		
		listRolResponsableResponse.setCodigoRolResponsable(rs.getLong("COD_ROL"));
		listRolResponsableResponse.setNombreResponsable(rs.getString("DESCRIPCION"));
		
		return listRolResponsableResponse;
	}

}
