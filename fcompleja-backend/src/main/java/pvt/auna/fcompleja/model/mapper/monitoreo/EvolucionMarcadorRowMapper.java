package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionMarcadorRequest;

public class EvolucionMarcadorRowMapper implements RowMapper<EvolucionMarcadorRequest> {
	@Override
	public EvolucionMarcadorRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
		EvolucionMarcadorRequest evolucionMarcador = new EvolucionMarcadorRequest();

		evolucionMarcador.setCodEvolucionMarcador(rs.getLong("COD_EVOLUCION_MARCADOR"));
		evolucionMarcador.setCodEvolucion(rs.getLong("COD_EVOLUCION"));
		evolucionMarcador.setCodMarcador(rs.getLong("COD_MARCADOR"));
		evolucionMarcador.setCodResultado(rs.getInt("COD_RESULTADO"));
		evolucionMarcador.setResultado(rs.getString("RESULTADO"));
		evolucionMarcador.setFecResultado(rs.getDate("FEC_RESULTADO"));
		evolucionMarcador.setTieneRegHc(rs.getString("TIENE_REG_HC"));
		evolucionMarcador.setpPerMinima(rs.getInt("P_PER_MINIMA"));
		evolucionMarcador.setpPerMaxima(rs.getInt("P_PER_MAXIMA"));
		evolucionMarcador.setpTipoIngresoRes(rs.getInt("P_TIPO_INGRESO_RES"));
		evolucionMarcador.setDescPerMinima(rs.getString("DESC_PER_MINIMA"));
		evolucionMarcador.setDescPerMaxima(rs.getString("DESC_PER_MAXIMA"));
		evolucionMarcador.setUsuariocrea(rs.getString("USUARIO_CREA"));
		evolucionMarcador.setFechaCrea(rs.getDate("FECHA_CREA"));

		return evolucionMarcador;
	}
}
