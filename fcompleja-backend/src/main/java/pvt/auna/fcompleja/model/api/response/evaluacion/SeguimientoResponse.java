package pvt.auna.fcompleja.model.api.response.evaluacion;

public class SeguimientoResponse {

	private Integer codSeguimiento;
	private String  descEstadoEvaluacion;
	private String  FechaEvaluacion;
	private String  codRolRespEstado;
	private String  codUsrRespEstado;
	private String  codRolRespRegistroEstado;
	private String  codUsrRespRegistroEstado;
	/***/
	private String  descRolRespEstado;
	private String  descUsrRespEstado;
	private String  descRolRespRegistroEstado;
	private String  descUsrRespRegistroEstado;
	
	private String  seguimiento;
	
	public Integer getCodSeguimiento() {
		return codSeguimiento;
	}
	public void setCodSeguimiento(Integer codSeguimiento) {
		this.codSeguimiento = codSeguimiento;
	}
	public String getDescEstadoEvaluacion() {
		return descEstadoEvaluacion;
	}
	public void setDescEstadoEvaluacion(String descEstadoEvaluacion) {
		this.descEstadoEvaluacion = descEstadoEvaluacion;
	}
	public String getFechaEvaluacion() {
		return FechaEvaluacion;
	}
	public void setFechaEvaluacion(String fechaEvaluacion) {
		FechaEvaluacion = fechaEvaluacion;
	}
	public String getCodRolRespEstado() {
		return codRolRespEstado;
	}
	public void setCodRolRespEstado(String codRolRespEstado) {
		this.codRolRespEstado = codRolRespEstado;
	}
	public String getCodUsrRespEstado() {
		return codUsrRespEstado;
	}
	public void setCodUsrRespEstado(String codUsrRespEstado) {
		this.codUsrRespEstado = codUsrRespEstado;
	}
	public String getCodRolRespRegistroEstado() {
		return codRolRespRegistroEstado;
	}
	public void setCodRolRespRegistroEstado(String codRolRespRegistroEstado) {
		this.codRolRespRegistroEstado = codRolRespRegistroEstado;
	}
	public String getCodUsrRespRegistroEstado() {
		return codUsrRespRegistroEstado;
	}
	public void setCodUsrRespRegistroEstado(String codUsrRespRegistroEstado) {
		this.codUsrRespRegistroEstado = codUsrRespRegistroEstado;
	}
	public String getDescRolRespEstado() {
		return descRolRespEstado;
	}
	public void setDescRolRespEstado(String descRolRespEstado) {
		this.descRolRespEstado = descRolRespEstado;
	}
	public String getDescUsrRespEstado() {
		return descUsrRespEstado;
	}
	public void setDescUsrRespEstado(String descUsrRespEstado) {
		this.descUsrRespEstado = descUsrRespEstado;
	}
	public String getDescRolRespRegistroEstado() {
		return descRolRespRegistroEstado;
	}
	public void setDescRolRespRegistroEstado(String descRolRespRegistroEstado) {
		this.descRolRespRegistroEstado = descRolRespRegistroEstado;
	}
	public String getDescUsrRespRegistroEstado() {
		return descUsrRespRegistroEstado;
	}
	public void setDescUsrRespRegistroEstado(String descUsrRespRegistroEstado) {
		this.descUsrRespRegistroEstado = descUsrRespRegistroEstado;
	}
	public String getSeguimiento() {
		return seguimiento;
	}
	public void setSeguimiento(String seguimiento) {
		this.seguimiento = seguimiento;
	}
	
	@Override
	public String toString() {
		return "SeguimientoResponse [codSeguimiento=" + codSeguimiento + ", descEstadoEvaluacion="
				+ descEstadoEvaluacion + ", FechaEvaluacion=" + FechaEvaluacion + ", codRolRespEstado="
				+ codRolRespEstado + ", codUsrRespEstado=" + codUsrRespEstado + ", codRolRespRegistroEstado="
				+ codRolRespRegistroEstado + ", codUsrRespRegistroEstado=" + codUsrRespRegistroEstado
				+ ", descRolRespEstado=" + descRolRespEstado + ", descUsrRespEstado=" + descUsrRespEstado
				+ ", descRolRespRegistroEstado=" + descRolRespRegistroEstado + ", descUsrRespRegistroEstado="
				+ descUsrRespRegistroEstado + "]";
	}
	
	
	
}
