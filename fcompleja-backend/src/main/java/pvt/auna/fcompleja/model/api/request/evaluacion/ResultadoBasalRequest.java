package pvt.auna.fcompleja.model.api.request.evaluacion;

public class ResultadoBasalRequest {
	
	private Integer codSolEva;
	private Integer codMac;
	private Integer codGrpDiag;
	private Integer codCondBasal;
 
	public Integer getCodSolEva() {
		return codSolEva;
	}
	public void setCodSolEva(Integer codSolEva) {
		this.codSolEva = codSolEva;
	}
	public Integer getCodMac() {
		return codMac;
	}
	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}
	public Integer getCodGrpDiag() {
		return codGrpDiag;
	}
	public void setCodGrpDiag(Integer codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}
	public Integer getCodCondBasal() {
		return codCondBasal;
	}
	public void setCodCondBasal(Integer codCondBasal) {
		this.codCondBasal = codCondBasal;
	}
}
