package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.InfoSolbenBean;

public class DetalleEvaluacionRowMapper implements RowMapper<InfoSolbenBean> {

	@Override
	public InfoSolbenBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		InfoSolbenBean bean = new InfoSolbenBean();
		bean.setNroSCGSolben(rs.getString("COD_SCG_SOLBEN"));
		bean.setCodEstadoSolben(rs.getInt("ESTADO_SCG"));
		bean.setEstadoSCGSolben(rs.getString("DESCRIP_ESTADO_SOLBEN"));
		bean.setFecSCGSolben(rs.getDate("FEC_SCG_SOLBEN"));
		bean.setHoraSCGSolben(rs.getString("HORA_SCG_SOLBEN"));
		bean.setCodTipoSolben(rs.getInt("TIPO_SCG_SOLBEN"));
		bean.setTipoSolben(rs.getString("DESCRIP_TIPO_SCG_SOLBEN"));
		bean.setCodClinica(rs.getString("COD_CLINICA"));
		bean.setMedicoTratante(rs.getString("MEDICO_TRATANTE_PRESCRIPTOR"));
		bean.setCmpMedico(rs.getString("CMP_MEDICO"));
		bean.setFechaReceta(rs.getDate("FEC_RECETA"));
		bean.setFechaQuimio(rs.getDate("FEC_QUIMIO"));
		bean.setFechaHospitalInicio(rs.getDate("FEC_HOSP_INICIO"));
		bean.setFechaHospitalFin(rs.getDate("FEC_HOSP_FIN"));
		bean.setMedicamentos(rs.getString("DESC_MEDICAMENTO"));
		bean.setEsquemaQuimio(rs.getString("DESC_ESQUEMA"));
		bean.setPersonaContacto(rs.getString("PERSON_CONTACTO"));
		bean.setTotalPresupuesto(rs.getDouble("TOTAL_PRESUPUESTO"));
		bean.setObservacion(rs.getString("OBS_CLINICA"));
		bean.setEdad(rs.getInt("EDAD_PACIENTE"));
		bean.setCodDiagnostico(rs.getString("COD_DIAGNOSTICO"));
		bean.setContratante(rs.getString("DES_CONTRATANTE"));
		bean.setPlan(rs.getString("DES_PLAN"));
		bean.setCodAfiliado(rs.getString("COD_AFI_PACIENTE"));
		bean.setFechaAfiliado(rs.getDate("FEC_AFILIACION"));
		bean.setNroCartaGarantia(rs.getString("NRO_CG"));
		bean.setCodGrupoDiagnostico(rs.getString("COD_GRP_DIAG"));
		bean.setDescripCodMac(rs.getString("COD_MAC_LARGO"));
		bean.setDescripcionMac(rs.getString("DESCRIPCION"));
		bean.setEstadoSolEva(rs.getInt("P_ESTADO_SOL_EVA"));
		bean.setDescripEstadoSolEva(rs.getString("NOMBRE_ESTADO"));
		bean.setpTipoEvaluacion(rs.getInt("P_TIPO_EVA")); 
		bean.setFecSolEva(rs.getString("FEC_SOL_EVA_ACT"));
		bean.setCodHis(rs.getString("COD_HIS"));
		bean.setDescHis(rs.getString("DESC_HIS"));
		return bean;
	}
	

}
