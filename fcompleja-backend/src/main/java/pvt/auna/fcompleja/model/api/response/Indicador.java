package pvt.auna.fcompleja.model.api.response;

import java.io.Serializable;
import java.util.List;

public class Indicador implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer codigo;
	private String descripcion;
	private List<Indicador> listaCriterio;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<Indicador> getListaCriterio() {
		return listaCriterio;
	}
	public void setListaCriterio(List<Indicador> listaCriterio) {
		this.listaCriterio = listaCriterio;
	}
	
}
