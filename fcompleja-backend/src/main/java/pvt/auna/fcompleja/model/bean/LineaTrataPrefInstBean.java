package pvt.auna.fcompleja.model.bean;

import java.util.List;

import pvt.auna.fcompleja.model.api.ListFiltroParametroResponse;

public class LineaTrataPrefInstBean {
	private Integer condicion;
	private List<ListFiltroParametroResponse> cmbCondicion;
	private List<PreferenciaInstitucionalBean> preferenciaInsti;
	private Integer nroCurso;
	private Integer tipoTumor;
	private Integer lugarProgresion;
	private Integer respuestaAlcanzada;
	private Integer cumplePrefInsti;
	private String  grabar;
	private String  observacion;
	public Integer getCondicion() {
		return condicion;
	}
	public void setCondicion(Integer condicion) {
		this.condicion = condicion;
	}
	public List<ListFiltroParametroResponse> getCmbCondicion() {
		return cmbCondicion;
	}
	public void setCmbCondicion(List<ListFiltroParametroResponse> cmbCondicion) {
		this.cmbCondicion = cmbCondicion;
	}
	public List<PreferenciaInstitucionalBean> getPreferenciaInsti() {
		return preferenciaInsti;
	}
	public void setPreferenciaInsti(List<PreferenciaInstitucionalBean> preferenciaInsti) {
		this.preferenciaInsti = preferenciaInsti;
	}
	public Integer getNroCurso() {
		return nroCurso;
	}
	public void setNroCurso(Integer nroCurso) {
		this.nroCurso = nroCurso;
	}
	public Integer getTipoTumor() {
		return tipoTumor;
	}
	public void setTipoTumor(Integer tipoTumor) {
		this.tipoTumor = tipoTumor;
	}
	public Integer getLugarProgresion() {
		return lugarProgresion;
	}
	public void setLugarProgresion(Integer lugarProgresion) {
		this.lugarProgresion = lugarProgresion;
	}
	public Integer getRespuestaAlcanzada() {
		return respuestaAlcanzada;
	}
	public void setRespuestaAlcanzada(Integer respuestaAlcanzada) {
		this.respuestaAlcanzada = respuestaAlcanzada;
	}
	public Integer getCumplePrefInsti() {
		return cumplePrefInsti;
	}
	public void setCumplePrefInsti(Integer cumplePrefInsti) {
		this.cumplePrefInsti = cumplePrefInsti;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
}
