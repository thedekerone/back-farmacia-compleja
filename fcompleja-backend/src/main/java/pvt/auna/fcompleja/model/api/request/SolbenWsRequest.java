package pvt.auna.fcompleja.model.api.request;

public class SolbenWsRequest {
	private String cod_scg_solben;
	private String cod_afi_paciente;
	private String cod_estado_scg;
	private String fec_estado_scg;
	private String nro_cg;
	private String fec_cg;
	private String txt_dato_adic1;
	private String txt_dato_adic2;
	private String txt_dato_adic3;
	public String getCod_scg_solben() {
		return cod_scg_solben;
	}
	public void setCod_scg_solben(String cod_scg_solben) {
		this.cod_scg_solben = cod_scg_solben;
	}
	public String getCod_afi_paciente() {
		return cod_afi_paciente;
	}
	public void setCod_afi_paciente(String cod_afi_paciente) {
		this.cod_afi_paciente = cod_afi_paciente;
	}
	public String getCod_estado_scg() {
		return cod_estado_scg;
	}
	public void setCod_estado_scg(String cod_estado_scg) {
		this.cod_estado_scg = cod_estado_scg;
	}
	public String getFec_estado_scg() {
		return fec_estado_scg;
	}
	public void setFec_estado_scg(String fec_estado_scg) {
		this.fec_estado_scg = fec_estado_scg;
	}
	public String getNro_cg() {
		return nro_cg;
	}
	public void setNro_cg(String nro_cg) {
		this.nro_cg = nro_cg;
	}
	public String getFec_cg() {
		return fec_cg;
	}
	public void setFec_cg(String fec_cg) {
		this.fec_cg = fec_cg;
	}
	public String getTxt_dato_adic1() {
		return txt_dato_adic1;
	}
	public void setTxt_dato_adic1(String txt_dato_adic1) {
		this.txt_dato_adic1 = txt_dato_adic1;
	}
	public String getTxt_dato_adic2() {
		return txt_dato_adic2;
	}
	public void setTxt_dato_adic2(String txt_dato_adic2) {
		this.txt_dato_adic2 = txt_dato_adic2;
	}
	public String getTxt_dato_adic3() {
		return txt_dato_adic3;
	}
	public void setTxt_dato_adic3(String txt_dato_adic3) {
		this.txt_dato_adic3 = txt_dato_adic3;
	}
	@Override
	public String toString() {
		return "SolbenWsRequest [cod_scg_solben=" + cod_scg_solben + ", cod_afi_paciente=" + cod_afi_paciente
				+ ", cod_estado_scg=" + cod_estado_scg + ", fec_estado_scg=" + fec_estado_scg + ", nro_cg=" + nro_cg
				+ ", fec_cg=" + fec_cg + ", txt_dato_adic1=" + txt_dato_adic1 + ", txt_dato_adic2=" + txt_dato_adic2
				+ ", txt_dato_adic3=" + txt_dato_adic3 + "]";
	}
	
}
