package pvt.auna.fcompleja.model.api.response;

public class LineaTrataPrefeInstiResponse {
	
	private Integer codLineaTrata;
	private Integer codMedicNuevo;
	private String codRespuesta;
	private Integer codResultado;
	private String msgResultado;
	
	public Integer getCodLineaTrata() {
		return codLineaTrata;
	}
	public void setCodLineaTrata(Integer codLineaTrata) {
		this.codLineaTrata = codLineaTrata;
	}
	public Integer getCodMedicNuevo() {
		return codMedicNuevo;
	}
	public void setCodMedicNuevo(Integer codMedicNuevo) {
		this.codMedicNuevo = codMedicNuevo;
	}
	public String getCodRespuesta() {
		return codRespuesta;
	}
	public void setCodRespuesta(String codRespuesta) {
		this.codRespuesta = codRespuesta;
	}
	public Integer getCodResultado() {
		return codResultado;
	}
	public void setCodResultado(Integer codResultado) {
		this.codResultado = codResultado;
	}
	public String getMsgResultado() {
		return msgResultado;
	}
	public void setMsgResultado(String msgResultado) {
		this.msgResultado = msgResultado;
	}
	
}
