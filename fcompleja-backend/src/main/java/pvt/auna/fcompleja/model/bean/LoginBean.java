package pvt.auna.fcompleja.model.bean;

import java.io.Serializable;

public class LoginBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer estado;
	private Integer codResultado;
	private String msgResultado;

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Integer getCodResultado() {
		return codResultado;
	}

	public void setCodResultado(Integer codResultado) {
		this.codResultado = codResultado;
	}

	public String getMsgResultado() {
		return msgResultado;
	}

	public void setMsgResultado(String msgResultado) {
		this.msgResultado = msgResultado;
	}

	@Override
	public String toString() {
		return "LoginBean [estado=" + estado + ", codResultado=" + codResultado + ", msgResultado=" + msgResultado
				+ "]";
	}

}
