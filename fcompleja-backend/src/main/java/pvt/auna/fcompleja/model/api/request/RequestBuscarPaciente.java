package pvt.auna.fcompleja.model.api.request;

public class RequestBuscarPaciente {
	private String pvTibus;
	private String codigoAfiliado;
	private String tipoDocumento;
	private String numeroDocumento;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	
	public RequestBuscarPaciente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getPvTibus() {
		return pvTibus;
	}

	public void setPvTibus(String pvTibus) {
		this.pvTibus = pvTibus;
	}

	public String getCodigoAfiliado() {
		return codigoAfiliado;
	}

	public void setCodigoAfiliado(String codigoAfiliado) {
		this.codigoAfiliado = codigoAfiliado;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	
	@Override
	public String toString() {
		return "RequestBuscarPaciente [pvTibus=" + pvTibus + ", codigoAfiliado="
				+ codigoAfiliado + ", tipoDocumento=" + tipoDocumento + ", numeroDocumento=" + numeroDocumento +  
				", nombres=" + nombres + ", apellidoPaterno=" + apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno + "]";    
	}	
	
}
