package pvt.auna.fcompleja.model.api.response.monitoreo;

import java.io.Serializable;
import java.util.Date;

public class LineaTratamientoResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codLineaTratamiento;
	private String lineaTratamiento;
	private String macSolicitado;
	private String codEvaluacion;
	private Date fecAprobacion;
	private Date fecInicio;
	private Date fecFin;
	private String nroCurso;
	private String tipoTumor;
	private String respAlcansada;
	private String estado;
	private String motivoInactivacion;
	private String medicoTratantePrescriptor;
	private Long montoAutorizado;
	private String nroScgSolben;
	private Date fecScgSolben;
	private String nroInforme;
	private Date fecEmision;
	private Integer codAuditorEvaluacion;
	private String nomAuditorEvaluacion;
	private String nroCgSolben;
	private String fecCgSolben;
	private Integer codInformeAuto;

	public LineaTratamientoResponse() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodLineaTratamiento() {
		return codLineaTratamiento;
	}

	public void setCodLineaTratamiento(Long codLineaTratamiento) {
		this.codLineaTratamiento = codLineaTratamiento;
	}

	public String getLineaTratamiento() {
		return lineaTratamiento;
	}

	public void setLineaTratamiento(String lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}

	public String getMacSolicitado() {
		return macSolicitado;
	}

	public void setMacSolicitado(String macSolicitado) {
		this.macSolicitado = macSolicitado;
	}

	public String getCodEvaluacion() {
		return codEvaluacion;
	}

	public void setCodEvaluacion(String codEvaluacion) {
		this.codEvaluacion = codEvaluacion;
	}

	public Date getFecAprobacion() {
		return fecAprobacion;
	}

	public void setFecAprobacion(Date fecAprobacion) {
		this.fecAprobacion = fecAprobacion;
	}

	public Date getFecInicio() {
		return fecInicio;
	}

	public void setFecInicio(Date fecInicio) {
		this.fecInicio = fecInicio;
	}

	public Date getFecFin() {
		return fecFin;
	}

	public void setFecFin(Date fecFin) {
		this.fecFin = fecFin;
	}

	public String getNroCurso() {
		return nroCurso;
	}

	public void setNroCurso(String nroCurso) {
		this.nroCurso = nroCurso;
	}

	public String getTipoTumor() {
		return tipoTumor;
	}

	public void setTipoTumor(String tipoTumor) {
		this.tipoTumor = tipoTumor;
	}

	public String getRespAlcansada() {
		return respAlcansada;
	}

	public void setRespAlcansada(String respAlcansada) {
		this.respAlcansada = respAlcansada;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMotivoInactivacion() {
		return motivoInactivacion;
	}

	public void setMotivoInactivacion(String motivoInactivacion) {
		this.motivoInactivacion = motivoInactivacion;
	}

	public String getMedicoTratantePrescriptor() {
		return medicoTratantePrescriptor;
	}

	public void setMedicoTratantePrescriptor(String medicoTratantePrescriptor) {
		this.medicoTratantePrescriptor = medicoTratantePrescriptor;
	}

	public Long getMontoAutorizado() {
		return montoAutorizado;
	}

	public void setMontoAutorizado(Long montoAutorizado) {
		this.montoAutorizado = montoAutorizado;
	}

	public String getNroScgSolben() {
		return nroScgSolben;
	}

	public void setNroScgSolben(String nroScgSolben) {
		this.nroScgSolben = nroScgSolben;
	}

	public Date getFecScgSolben() {
		return fecScgSolben;
	}

	public void setFecScgSolben(Date fecScgSolben) {
		this.fecScgSolben = fecScgSolben;
	}

	public String getNroInforme() {
		return nroInforme;
	}

	public void setNroInforme(String nroInforme) {
		this.nroInforme = nroInforme;
	}

	public Date getFecEmision() {
		return fecEmision;
	}

	public void setFecEmision(Date fecEmision) {
		this.fecEmision = fecEmision;
	}

	public String getNomAuditorEvaluacion() {
		return nomAuditorEvaluacion;
	}

	public void setNomAuditorEvaluacion(String nomAuditorEvaluacion) {
		this.nomAuditorEvaluacion = nomAuditorEvaluacion;
	}

	public String getNroCgSolben() {
		return nroCgSolben;
	}

	public void setNroCgSolben(String nroCgSolben) {
		this.nroCgSolben = nroCgSolben;
	}

	public String getFecCgSolben() {
		return fecCgSolben;
	}

	public void setFecCgSolben(String fecCgSolben) {
		this.fecCgSolben = fecCgSolben;
	}

	public Integer getCodAuditorEvaluacion() {
		return codAuditorEvaluacion;
	}

	public void setCodAuditorEvaluacion(Integer codAuditorEvaluacion) {
		this.codAuditorEvaluacion = codAuditorEvaluacion;
	}

	public Integer getCodInformeAuto() {
		return codInformeAuto;
	}

	public void setCodInformeAuto(Integer codInformeAuto) {
		this.codInformeAuto = codInformeAuto;
	}

	@Override
	public String toString() {
		return "LineaTratamientoResponse [codLineaTratamiento=" + codLineaTratamiento + ", lineaTratamiento="
				+ lineaTratamiento + ", macSolicitado=" + macSolicitado + ", codEvaluacion=" + codEvaluacion
				+ ", fecAprobacion=" + fecAprobacion + ", fecInicio=" + fecInicio + ", fecFin=" + fecFin + ", nroCurso="
				+ nroCurso + ", tipoTumor=" + tipoTumor + ", respAlcansada=" + respAlcansada + ", estado=" + estado
				+ ", motivoInactivacion=" + motivoInactivacion + ", medicoTratantePrescriptor="
				+ medicoTratantePrescriptor + ", montoAutorizado=" + montoAutorizado + ", nroScgSolben=" + nroScgSolben
				+ ", fecScgSolben=" + fecScgSolben + ", nroInforme=" + nroInforme + ", fecEmision=" + fecEmision
				+ ", codAuditorEvaluacion=" + codAuditorEvaluacion + ", nomAuditorEvaluacion=" + nomAuditorEvaluacion
				+ ", nroCgSolben=" + nroCgSolben + ", fecCgSolben=" + fecCgSolben + ", codInformeAuto=" + codInformeAuto
				+ "]";
	}

}
