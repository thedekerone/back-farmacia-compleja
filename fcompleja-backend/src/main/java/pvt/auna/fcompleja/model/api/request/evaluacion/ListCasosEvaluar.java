package pvt.auna.fcompleja.model.api.request.evaluacion;

import java.util.List;

import pvt.auna.fcompleja.model.api.CasosEvaluar;


public class ListCasosEvaluar {
	
	private String fecha;
	private String hora;
	
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	private List<CasosEvaluar> listaCasosEvaluar;

	public List<CasosEvaluar> getListaCasosEvaluar() {
		return listaCasosEvaluar;
	}

	public void setListaCasosEvaluar(List<CasosEvaluar> listaCasosEvaluar) {
		this.listaCasosEvaluar = listaCasosEvaluar;
	}

	@Override
	public String toString() {
		return "ListCasosEvaluar [ listaEvaluacion=" + listaCasosEvaluar + "]";
	}	
	
}
