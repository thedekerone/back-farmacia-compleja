package pvt.auna.fcompleja.model.api.response.monitoreo;

import java.io.Serializable;
import java.util.List;

public class MarcadorResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codConfigMarca;
	private Long codMarcador;
	private String descripcion;
	private Integer rangoMinimo;
	private Integer rangoMaximo;
	private Integer pPerMinima;
	private Integer pPerMaxima;
	private Integer valPerMinima;
	private Integer valPerMaxima;
	private String descPerMinima;
	private String descPerMaxima;
	private String rango;
	private Integer pTipoIngresoRes;
	private String descTipoIngresoRes;
	private String unidadMedida;
	private Long codMarcadorDet;
	private String valorFijo;
	private List<DetalleMarcadorResponse> listaDetalleMarcador;

	public Long getCodConfigMarca() {
		return codConfigMarca;
	}

	public void setCodConfigMarca(Long codConfigMarca) {
		this.codConfigMarca = codConfigMarca;
	}

	public Long getCodMarcador() {
		return codMarcador;
	}

	public void setCodMarcador(Long codMarcador) {
		this.codMarcador = codMarcador;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getRangoMinimo() {
		return rangoMinimo;
	}

	public void setRangoMinimo(Integer rangoMinimo) {
		this.rangoMinimo = rangoMinimo;
	}

	public Integer getRangoMaximo() {
		return rangoMaximo;
	}

	public void setRangoMaximo(Integer rangoMaximo) {
		this.rangoMaximo = rangoMaximo;
	}

	public Integer getpPerMinima() {
		return pPerMinima;
	}

	public void setpPerMinima(Integer pPerMinima) {
		this.pPerMinima = pPerMinima;
	}

	public Integer getpPerMaxima() {
		return pPerMaxima;
	}

	public void setpPerMaxima(Integer pPerMaxima) {
		this.pPerMaxima = pPerMaxima;
	}

	public Integer getValPerMinima() {
		return valPerMinima;
	}

	public void setValPerMinima(Integer valPerMinima) {
		this.valPerMinima = valPerMinima;
	}

	public Integer getValPerMaxima() {
		return valPerMaxima;
	}

	public void setValPerMaxima(Integer valPerMaxima) {
		this.valPerMaxima = valPerMaxima;
	}

	public String getDescPerMinima() {
		return descPerMinima;
	}

	public void setDescPerMinima(String descPerMinima) {
		this.descPerMinima = descPerMinima;
	}

	public String getDescPerMaxima() {
		return descPerMaxima;
	}

	public void setDescPerMaxima(String descPerMaxima) {
		this.descPerMaxima = descPerMaxima;
	}

	public String getRango() {
		return rango;
	}

	public void setRango(String rango) {
		this.rango = rango;
	}

	public Integer getpTipoIngresoRes() {
		return pTipoIngresoRes;
	}

	public void setpTipoIngresoRes(Integer pTipoIngresoRes) {
		this.pTipoIngresoRes = pTipoIngresoRes;
	}

	public String getDescTipoIngresoRes() {
		return descTipoIngresoRes;
	}

	public void setDescTipoIngresoRes(String descTipoIngresoRes) {
		this.descTipoIngresoRes = descTipoIngresoRes;
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public Long getCodMarcadorDet() {
		return codMarcadorDet;
	}

	public void setCodMarcadorDet(Long codMarcadorDet) {
		this.codMarcadorDet = codMarcadorDet;
	}

	public String getValorFijo() {
		return valorFijo;
	}

	public void setValorFijo(String valorFijo) {
		this.valorFijo = valorFijo;
	}

	public List<DetalleMarcadorResponse> getListaDetalleMarcador() {
		return listaDetalleMarcador;
	}

	public void setListaDetalleMarcador(List<DetalleMarcadorResponse> listaDetalleMarcador) {
		this.listaDetalleMarcador = listaDetalleMarcador;
	}

	@Override
	public String toString() {
		return "MarcadorResponse [codConfigMarca=" + codConfigMarca + ", codMarcador=" + codMarcador + ", descripcion="
				+ descripcion + ", rangoMinimo=" + rangoMinimo + ", rangoMaximo=" + rangoMaximo + ", pPerMinima="
				+ pPerMinima + ", pPerMaxima=" + pPerMaxima + ", valPerMinima=" + valPerMinima + ", valPerMaxima="
				+ valPerMaxima + ", descPerMinima=" + descPerMinima + ", descPerMaxima=" + descPerMaxima + ", rango="
				+ rango + ", pTipoIngresoRes=" + pTipoIngresoRes + ", descTipoIngresoRes=" + descTipoIngresoRes
				+ ", unidadMedida=" + unidadMedida + ", codMarcadorDet=" + codMarcadorDet + ", valorFijo=" + valorFijo
				+ ", listaDetalleMarcador=" + listaDetalleMarcador + "]";
	}

}
