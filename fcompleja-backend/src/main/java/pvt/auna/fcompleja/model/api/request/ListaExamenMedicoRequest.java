package pvt.auna.fcompleja.model.api.request;

public class ListaExamenMedicoRequest {

	  private String descripcion;

	  public String getDescripcion() {
	  	return descripcion;
	  }

	  public void setDescripcion(String descripcion) {
	  	this.descripcion = descripcion;
	  } 
	    
}
