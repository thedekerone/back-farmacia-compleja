package pvt.auna.fcompleja.model.api;

public class FiltroLiderTumorRequest {

	private String rolLiderTumor;
	
	public String getRolLiderTumor() {
		return rolLiderTumor;
	}
	public void setRolLiderTumor(String rolLiderTumor) {
		this.rolLiderTumor = rolLiderTumor;
	}
		
}
