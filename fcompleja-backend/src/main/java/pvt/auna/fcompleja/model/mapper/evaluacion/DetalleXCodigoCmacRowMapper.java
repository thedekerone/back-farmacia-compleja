package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.ListaBandeja;

public class DetalleXCodigoCmacRowMapper implements RowMapper<ListaBandeja>{

	@Override
	public ListaBandeja mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ListaBandeja listaBandeja = new ListaBandeja();
		
		listaBandeja.setNumeroSolEvaluacion(rs.getString("COD_DESC_SOL_EVA"));
		listaBandeja.setCodMac(rs.getString("COD_MAC")); 
		listaBandeja.setCodigoPaciente(rs.getString("COD_AFI_PACIENTE"));
		listaBandeja.setCodigoDiagnostico(rs.getString("COD_DIAGNOSTICO"));
		listaBandeja.setDescripcionCmac(rs.getString("DESCRIPCION"));
		listaBandeja.setHoraCmac(rs.getString("HORA_REUNION"));
		listaBandeja.setCodSolEvaluacion(rs.getString("COD_SOL_EVA"));
		listaBandeja.setCodigoGrabado(rs.getString("COD_GRABADO"));
		listaBandeja.setCodActa(rs.getString("COD_ACTA"));
		listaBandeja.setObservacion(rs.getString("OBSERVACION"));
		listaBandeja.setDescripcionResultado(rs.getString("DESC_EVALUACION"));
		listaBandeja.setCodigoResultado(rs.getString("COD_RES_EVALUACION"));
		listaBandeja.setCodigoEstadoEvaluacion(rs.getString("P_ESTADO_SOL_EVA"));
		
		return listaBandeja;
	}
}
