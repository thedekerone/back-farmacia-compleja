package pvt.auna.fcompleja.model.bean;

import java.util.Date;

public class ConsumoBean {

    private String codMac;
    private String tipoEncuentro;
    private String codAfiliado;
    private String codDiagnostico;
    private Date fechaConsumo;
    private Long cantidadConsumo;
    private Double montoConsumo;
    private Long lineaTratamiento;

    public String getCodMac() {
        return codMac;
    }

    public void setCodMac(String codMac) {
        this.codMac = codMac;
    }

    public String getTipoEncuentro() {
        return tipoEncuentro;
    }

    public void setTipoEncuentro(String tipoEncuentro) {
        this.tipoEncuentro = tipoEncuentro;
    }

    public String getCodAfiliado() {
        return codAfiliado;
    }

    public void setCodAfiliado(String codAfiliado) {
        this.codAfiliado = codAfiliado;
    }

    public String getCodDiagnostico() {
        return codDiagnostico;
    }

    public void setCodDiagnostico(String codDiagnostico) {
        this.codDiagnostico = codDiagnostico;
    }

    public Date getFechaConsumo() {
        return fechaConsumo;
    }

    public void setFechaConsumo(Date fechaConsumo) {
        this.fechaConsumo = fechaConsumo;
    }

    public Long getCantidadConsumo() {
        return cantidadConsumo;
    }

    public void setCantidadConsumo(Long cantidadConsumo) {
        this.cantidadConsumo = cantidadConsumo;
    }

    public Double getMontoConsumo() {
        return montoConsumo;
    }

    public void setMontoConsumo(Double montoConsumo) {
        this.montoConsumo = montoConsumo;
    }

    public Long getLineaTratamiento() {
        return lineaTratamiento;
    }

    public void setLineaTratamiento(Long lineaTratamiento) {
        this.lineaTratamiento = lineaTratamiento;
    }
}
