package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.ListFiltroRolResponse;

public class FiltroUsrRolRowMapper implements RowMapper<ListFiltroRolResponse> {

	@Override
	public ListFiltroRolResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		ListFiltroRolResponse listFiltroRolResponse = new ListFiltroRolResponse();
		
		listFiltroRolResponse.setCodigoUsuario(rs.getInt("COD_USUARIO"));
		listFiltroRolResponse.setNombreUsuarioRol(rs.getString("PV_NOMBRE"));
		
		return listFiltroRolResponse;
	}

}
