package pvt.auna.fcompleja.model.api.request.evaluacion;

import java.io.Serializable;

public class AntecedentePatoPersonal implements Serializable {

	private static final long serialVersionUID = 1L;
	
	Boolean hta;
	Boolean ima_icc;
	Boolean reumatologicas;
	Boolean dim;
	Boolean epoc_epid;
	Boolean endocrinopatias;
	Boolean asma;
	String ram;
	String otros;
	String habitos_nocivos;
	Boolean aplica;
	
	
	public Boolean getHta() {
		return hta;
	}
	public void setHta(Boolean hta) {
		this.hta = hta;
	}
	public Boolean getIma_icc() {
		return ima_icc;
	}
	public void setIma_icc(Boolean ima_icc) {
		this.ima_icc = ima_icc;
	}
	public Boolean getReumatologicas() {
		return reumatologicas;
	}
	public void setReumatologicas(Boolean reumatologicas) {
		this.reumatologicas = reumatologicas;
	}
	public Boolean getDim() {
		return dim;
	}
	public void setDim(Boolean dim) {
		this.dim = dim;
	}
	public Boolean getEpoc_epid() {
		return epoc_epid;
	}
	public void setEpoc_epid(Boolean epoc_epid) {
		this.epoc_epid = epoc_epid;
	}
	public Boolean getEndocrinopatias() {
		return endocrinopatias;
	}
	public void setEndocrinopatias(Boolean endocrinopatias) {
		this.endocrinopatias = endocrinopatias;
	}
	public Boolean getAsma() {
		return asma;
	}
	public void setAsma(Boolean asma) {
		this.asma = asma;
	}
	public String getRam() {
		return ram;
	}
	public void setRam(String ram) {
		this.ram = ram;
	}
	public String getOtros() {
		return otros;
	}
	public void setOtros(String otros) {
		this.otros = otros;
	}
	public String getHabitos_nocivos() {
		return habitos_nocivos;
	}
	public void setHabitos_nocivos(String habitos_nocivos) {
		this.habitos_nocivos = habitos_nocivos;
	}
	public Boolean getAplica() {
		return aplica;
	}
	public void setAplica(Boolean aplica) {
		this.aplica = aplica;
	}	
}
