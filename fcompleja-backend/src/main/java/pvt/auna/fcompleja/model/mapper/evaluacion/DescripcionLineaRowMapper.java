package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.CondicionBasalBean;

public class DescripcionLineaRowMapper implements RowMapper<CondicionBasalBean> {

	@Override
	public CondicionBasalBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		CondicionBasalBean condicionBasalBean = new CondicionBasalBean();
		
		condicionBasalBean.setAntecedentesImp(rs.getString("antecedentes_imp"));
		condicionBasalBean.setEcog(rs.getString("ecog"));
		condicionBasalBean.setExisteToxicidad(rs.getString("existe_toxicidad"));
		condicionBasalBean.setTipoToxicidad(rs.getString("tipo_toxicidad"));

		return condicionBasalBean;
	}
}
