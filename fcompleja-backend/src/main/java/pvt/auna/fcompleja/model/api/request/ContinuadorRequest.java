package pvt.auna.fcompleja.model.api.request;

public class ContinuadorRequest {
	private Integer codSolicitudEvaluacion;
	private Integer resultadoAutorizador;
	private Integer codResponsableMonitoreo;
	private Integer codResultadoMonitoreo;
	private String comentario;
	private String nroTareaMonitoreo;
	private String fechaMonitoreo;
	private String documento;
	private Integer codigoRolUsuario;
	private String  fechaEstado;
	private Integer codigoUsuario;
	private Integer codigoRolMonitoreo;
	
	public Integer getCodSolicitudEvaluacion() {
		return codSolicitudEvaluacion;
	}
	public void setCodSolicitudEvaluacion(Integer codSolicitudEvaluacion) {
		this.codSolicitudEvaluacion = codSolicitudEvaluacion;
	}
	public Integer getResultadoAutorizador() {
		return resultadoAutorizador;
	}
	public void setResultadoAutorizador(Integer resultadoAutorizador) {
		this.resultadoAutorizador = resultadoAutorizador;
	}
	public Integer getCodResponsableMonitoreo() {
		return codResponsableMonitoreo;
	}
	public void setCodResponsableMonitoreo(Integer codResponsableMonitoreo) {
		this.codResponsableMonitoreo = codResponsableMonitoreo;
	}
	public Integer getCodResultadoMonitoreo() {
		return codResultadoMonitoreo;
	}
	public void setCodResultadoMonitoreo(Integer codResultadoMonitoreo) {
		this.codResultadoMonitoreo = codResultadoMonitoreo;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getNroTareaMonitoreo() {
		return nroTareaMonitoreo;
	}
	public void setNroTareaMonitoreo(String nroTareaMonitoreo) {
		this.nroTareaMonitoreo = nroTareaMonitoreo;
	}
	public String getFechaMonitoreo() {
		return fechaMonitoreo;
	}
	public void setFechaMonitoreo(String fechaMonitoreo) {
		this.fechaMonitoreo = fechaMonitoreo;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public Integer getCodigoRolUsuario() {
		return codigoRolUsuario;
	}
	public void setCodigoRolUsuario(Integer codigoRolUsuario) {
		this.codigoRolUsuario = codigoRolUsuario;
	}
	public String getFechaEstado() {
		return fechaEstado;
	}
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}
	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	public Integer getCodigoRolMonitoreo() {
		return codigoRolMonitoreo;
	}
	public void setCodigoRolMonitoreo(Integer codigoRolMonitoreo) {
		this.codigoRolMonitoreo = codigoRolMonitoreo;
	}
	
	@Override
	public String toString() {
		return "ContinuadorRequest [codSolicitudEvaluacion=" + codSolicitudEvaluacion + ", resultadoAutorizador="
				+ resultadoAutorizador + ", codResponsableMonitoreo=" + codResponsableMonitoreo
				+ ", codResultadoMonitoreo=" + codResultadoMonitoreo + ", comentario=" + comentario
				+ ", nroTareaMonitoreo=" + nroTareaMonitoreo + ", fechaMonitoreo=" + fechaMonitoreo + ", documento="
				+ documento + ", codigoRolUsuario=" + codigoRolUsuario + ", fechaEstado=" + fechaEstado
				+ ", codigoUsuario=" + codigoUsuario + ", codigoRolMonitoreo=" + codigoRolMonitoreo + "]";
	}
	
	
	
}
