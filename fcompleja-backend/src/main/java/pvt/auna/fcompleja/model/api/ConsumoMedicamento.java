package pvt.auna.fcompleja.model.api;

import java.io.Serializable;
import java.util.Date;

public class ConsumoMedicamento implements Serializable {

    private static final long serialVersionUID = 5671704606216878621L;

    private Long codConsumoMedicamento;
    private String codMac;
    private String tipoEncuentro;
    private Long encuentro;
    private String codigoSap;
    private String descripActivoMolecula;
    private String descripPresentGenerico;
    private String prestacion;
    private String codAfiliado;
    private String codPaciente;
    private String nombrePaciente;
    private String clinica;
    private Date fechaConsumo;
    private String descripGrpDiag;
    private String codDiagnostico;
    private String descripDiagnostico;
    private Long lineaTratamiento;
    private String medicoTratante;
    private String plan;
    private Long cantidadConsumo;
    private Double montoConsumo;
    private Long codHistControlGasto;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCodConsumoMedicamento() {
        return codConsumoMedicamento;
    }

    public void setCodConsumoMedicamento(Long codConsumoMedicamento) {
        this.codConsumoMedicamento = codConsumoMedicamento;
    }

    public String getCodMac() {
        return codMac;
    }

    public void setCodMac(String codMac) {
        this.codMac = codMac;
    }

    public String getTipoEncuentro() {
        return tipoEncuentro;
    }

    public void setTipoEncuentro(String tipoEncuentro) {
        this.tipoEncuentro = tipoEncuentro;
    }

    public Long getEncuentro() {
        return encuentro;
    }

    public void setEncuentro(Long encuentro) {
        this.encuentro = encuentro;
    }

    public String getCodigoSap() {
        return codigoSap;
    }

    public void setCodigoSap(String codigoSap) {
        this.codigoSap = codigoSap;
    }

    public String getDescripActivoMolecula() {
        return descripActivoMolecula;
    }

    public void setDescripActivoMolecula(String descripActivoMolecula) {
        this.descripActivoMolecula = descripActivoMolecula;
    }

    public String getDescripPresentGenerico() {
        return descripPresentGenerico;
    }

    public void setDescripPresentGenerico(String descripPresentGenerico) {
        this.descripPresentGenerico = descripPresentGenerico;
    }

    public String getPrestacion() {
        return prestacion;
    }

    public void setPrestacion(String prestacion) {
        this.prestacion = prestacion;
    }

    public String getCodAfiliado() {
        return codAfiliado;
    }

    public void setCodAfiliado(String codAfiliado) {
        this.codAfiliado = codAfiliado;
    }

    public String getCodPaciente() {
        return codPaciente;
    }

    public void setCodPaciente(String codPaciente) {
        this.codPaciente = codPaciente;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getClinica() {
        return clinica;
    }

    public void setClinica(String clinica) {
        this.clinica = clinica;
    }

    public Date getFechaConsumo() {
        return fechaConsumo;
    }

    public void setFechaConsumo(Date fechaConsumo) {
        this.fechaConsumo = fechaConsumo;
    }

    public String getDescripGrpDiag() {
        return descripGrpDiag;
    }

    public void setDescripGrpDiag(String descripGrpDiag) {
        this.descripGrpDiag = descripGrpDiag;
    }

    public String getCodDiagnostico() {
        return codDiagnostico;
    }

    public void setCodDiagnostico(String codDiagnostico) {
        this.codDiagnostico = codDiagnostico;
    }

    public String getDescripDiagnostico() {
        return descripDiagnostico;
    }

    public void setDescripDiagnostico(String descripDiagnostico) {
        this.descripDiagnostico = descripDiagnostico;
    }

    public Long getLineaTratamiento() {
        return lineaTratamiento;
    }

    public void setLineaTratamiento(Long lineaTratamiento) {
        this.lineaTratamiento = lineaTratamiento;
    }

    public String getMedicoTratante() {
        return medicoTratante;
    }

    public void setMedicoTratante(String medicoTratante) {
        this.medicoTratante = medicoTratante;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public Long getCantidadConsumo() {
        return cantidadConsumo;
    }

    public void setCantidadConsumo(Long cantidadConsumo) {
        this.cantidadConsumo = cantidadConsumo;
    }

    public Double getMontoConsumo() {
        return montoConsumo;
    }

    public void setMontoConsumo(Double montoConsumo) {
        this.montoConsumo = montoConsumo;
    }

    public Long getCodHistControlGasto() {
        return codHistControlGasto;
    }

    public void setCodHistControlGasto(Long codHistControlGasto) {
        this.codHistControlGasto = codHistControlGasto;
    }

    @Override
    public String toString() {
        return "ConsumoMedicamento{" +
                "codConsumoMedicamento=" + codConsumoMedicamento +
                ", codMac='" + codMac + '\'' +
                ", tipoEncuentro='" + tipoEncuentro + '\'' +
                ", encuentro=" + encuentro +
                ", codigoSap='" + codigoSap + '\'' +
                ", descripActivoMolecula='" + descripActivoMolecula + '\'' +
                ", descripPresentGenerico='" + descripPresentGenerico + '\'' +
                ", prestacion='" + prestacion + '\'' +
                ", codAfiliado='" + codAfiliado + '\'' +
                ", codPaciente='" + codPaciente + '\'' +
                ", nombrePaciente='" + nombrePaciente + '\'' +
                ", clinica='" + clinica + '\'' +
                ", fechaConsumo=" + fechaConsumo +
                ", descripGrpDiag='" + descripGrpDiag + '\'' +
                ", codDiagnostico='" + codDiagnostico + '\'' +
                ", descripDiagnostico='" + descripDiagnostico + '\'' +
                ", lineaTratamiento=" + lineaTratamiento +
                ", medicoTratante='" + medicoTratante + '\'' +
                ", plan='" + plan + '\'' +
                ", cantidadConsumo=" + cantidadConsumo +
                ", montoConsumo=" + montoConsumo +
                ", codHistControlGasto=" + codHistControlGasto +
                '}';
    }
}
