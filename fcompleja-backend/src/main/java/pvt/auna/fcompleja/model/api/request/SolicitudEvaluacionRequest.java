package pvt.auna.fcompleja.model.api.request;

public class SolicitudEvaluacionRequest {

	private Integer codSolicitudEvaluacion;
	private Integer codInformeAutorizador;
	private Integer codMac;
	private String codGrpDiag;
	private Integer tipoDocumento;
	private String descripcionDocumento;
	private String urlDescarga;

	private Integer estadoCorreoEnvCmac;
	private Integer estadoCorreoEnvLiderTumor;
	private Long codigoEnvioEnvMac;
	private Long codigoEnvioEnvLiderTumor;
	private Long codigoEnvioEnvAlerMonit;

	private Integer pTipoEva;

	public Integer getCodSolicitudEvaluacion() {
		return codSolicitudEvaluacion;
	}

	public void setCodSolicitudEvaluacion(Integer codSolicitudEvaluacion) {
		this.codSolicitudEvaluacion = codSolicitudEvaluacion;
	}

	public Integer getCodInformeAutorizador() {
		return codInformeAutorizador;
	}

	public void setCodInformeAutorizador(Integer codInformeAutorizador) {
		this.codInformeAutorizador = codInformeAutorizador;
	}

	public Integer getCodMac() {
		return codMac;
	}

	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}

	public String getCodGrpDiag() {
		return codGrpDiag;
	}

	public void setCodGrpDiag(String codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}

	public Integer getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(Integer tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDescripcionDocumento() {
		return descripcionDocumento;
	}

	public void setDescripcionDocumento(String descripcionDocumento) {
		this.descripcionDocumento = descripcionDocumento;
	}

	public String getUrlDescarga() {
		return urlDescarga;
	}

	public void setUrlDescarga(String urlDescarga) {
		this.urlDescarga = urlDescarga;
	}

	public Integer getEstadoCorreoEnvCmac() {
		return estadoCorreoEnvCmac;
	}

	public void setEstadoCorreoEnvCmac(Integer estadoCorreoEnvCmac) {
		this.estadoCorreoEnvCmac = estadoCorreoEnvCmac;
	}

	public Integer getEstadoCorreoEnvLiderTumor() {
		return estadoCorreoEnvLiderTumor;
	}

	public void setEstadoCorreoEnvLiderTumor(Integer estadoCorreoEnvLiderTumor) {
		this.estadoCorreoEnvLiderTumor = estadoCorreoEnvLiderTumor;
	}

	public Long getCodigoEnvioEnvMac() {
		return codigoEnvioEnvMac;
	}

	public void setCodigoEnvioEnvMac(Long codigoEnvioEnvMac) {
		this.codigoEnvioEnvMac = codigoEnvioEnvMac;
	}

	public Long getCodigoEnvioEnvLiderTumor() {
		return codigoEnvioEnvLiderTumor;
	}

	public void setCodigoEnvioEnvLiderTumor(Long codigoEnvioEnvLiderTumor) {
		this.codigoEnvioEnvLiderTumor = codigoEnvioEnvLiderTumor;
	}

	public Long getCodigoEnvioEnvAlerMonit() {
		return codigoEnvioEnvAlerMonit;
	}

	public void setCodigoEnvioEnvAlerMonit(Long codigoEnvioEnvAlerMonit) {
		this.codigoEnvioEnvAlerMonit = codigoEnvioEnvAlerMonit;
	}

	public Integer getpTipoEva() {
		return pTipoEva;
	}

	public void setpTipoEva(Integer pTipoEva) {
		this.pTipoEva = pTipoEva;
	}

	@Override
	public String toString() {
		return "SolicitudEvaluacionRequest [codSolicitudEvaluacion=" + codSolicitudEvaluacion
				+ ", codInformeAutorizador=" + codInformeAutorizador + ", codMac=" + codMac + ", codGrpDiag="
				+ codGrpDiag + ", tipoDocumento=" + tipoDocumento + ", descripcionDocumento=" + descripcionDocumento
				+ ", urlDescarga=" + urlDescarga + ", estadoCorreoEnvCmac=" + estadoCorreoEnvCmac
				+ ", estadoCorreoEnvLiderTumor=" + estadoCorreoEnvLiderTumor + ", codigoEnvioEnvMac="
				+ codigoEnvioEnvMac + ", codigoEnvioEnvLiderTumor=" + codigoEnvioEnvLiderTumor
				+ ", codigoEnvioEnvAlerMonit=" + codigoEnvioEnvAlerMonit + ", pTipoEva=" + pTipoEva + "]";
	}

}
