package pvt.auna.fcompleja.model.api.response;

import java.util.List;

public class DocuHistPacienteResponse {

	private List<HistPacienteResponse> historialLineaTrat;
	private List<HistPacienteResponse> documentoLineaTrat;
	private String 	grabar;
	private Integer codResultado;
	private String  msgResultado;
	
	
	public List<HistPacienteResponse> getHistorialLineaTrat() {
		return historialLineaTrat;
	}
	public void setHistorialLineaTrat(List<HistPacienteResponse> historialLineaTrat) {
		this.historialLineaTrat = historialLineaTrat;
	}
	public List<HistPacienteResponse> getDocumentoLineaTrat() {
		return documentoLineaTrat;
	}
	public void setDocumentoLineaTrat(List<HistPacienteResponse> documentoLineaTrat) {
		this.documentoLineaTrat = documentoLineaTrat;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public Integer getCodResultado() {
		return codResultado;
	}
	public void setCodResultado(Integer codResultado) {
		this.codResultado = codResultado;
	}
	public String getMsgResultado() {
		return msgResultado;
	}
	public void setMsgResultado(String msgResultado) {
		this.msgResultado = msgResultado;
	}
}
