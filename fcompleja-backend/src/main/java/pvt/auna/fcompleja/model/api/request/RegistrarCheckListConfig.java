package pvt.auna.fcompleja.model.api.request;

public class RegistrarCheckListConfig {
	private Number codMac;
	private Number grpDiagnostico;
	private Number codChkListIndi;
	private String descripcion;
	private Number pEstado;
	private String fechaIniVigencia;
	private String fechaFinVigencia;

	public RegistrarCheckListConfig() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Number getCodMac() {
		return codMac;
	}

	public void setCodMac(Number codMac) {
		this.codMac = codMac;
	}

	public Number getGrpDiagnostico() {
		return grpDiagnostico;
	}

	public void setGrpDiagnostico(Number grpDiagnostico) {
		this.grpDiagnostico = grpDiagnostico;
	}

	public Number getCodChkListIndi() {
		return codChkListIndi;
	}

	public void setCodChkListIndi(Number codChkListIndi) {
		this.codChkListIndi = codChkListIndi;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Number getpEstado() {
		return pEstado;
	}

	public void setpEstado(Number pEstado) {
		this.pEstado = pEstado;
	}

	public String getFechaIniVigencia() {
		return fechaIniVigencia;
	}

	public void setFechaIniVigencia(String fechaIniVigencia) {
		this.fechaIniVigencia = fechaIniVigencia;
	}

	public String getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	@Override
	public String toString() {
		return "RegistrarCheckListConfig [codMac=" + codMac + ", grpDiagnostico=" + grpDiagnostico + ", codChkListIndi="
				+ codChkListIndi + ", descripcion=" + descripcion + ", pEstado=" + pEstado + ", fechaIniVigencia="
				+ fechaIniVigencia + ", fechaFinVigencia=" + fechaFinVigencia + "]";
	}

}
