/**
 * 
 */
package pvt.auna.fcompleja.model.api.response;

import java.util.Date;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ReporteSolicitudesAutorizacionesResponse {
	
	private Integer fechaMes;
	private Integer fechaAno;
	private String codigoSolicitudEva;
	private String codigoPaciente;
	private String  codigoAfiliado;
	private Date    fechaAfiliacion;
	private String  tipoAfiliacion;
	private String documentoIdentidad;
	private String  paciente;
	private String  sexo;
	private Integer edad;
	private String  medicamento;
	private Date    fechaReceta;
	private String  cie10;
	private String 	diagnostico;
	private String  grupoDiagnostico;
	private String 	plan;
	private String	estadio;
	private String 	tnm;
	private String 	linea;
	private String	clinica;
	private	String	cmp;
	private String 	medicoTratantePrescriptor;
	private String	codigoSCGSolben;
	private Date	fechaRegistroSCGSolben;
	private String	horaRegistroSCGSolben;
	private String	tipoSCGSolben;
	private String	estadoSCGSolben;
	private String codigoSolicitudPre;
	private Date	fechaRegistroSolicitudPre;
	private String	horaRegistroSolicitudPre;
	private String	estadoSolicitudPre;
	private String 	autorizadorPertinenciaSolicitudPre;
	private Date	fechaHoraRegistroSolicitudEva;
	private	String	contratante;
	private String	cumplePrefInstitucional;
	private String 	indicacionesCheckListPaciente;
	private String 	cumpleIndicacionesCheckListPaciente;
	private	String	pertinencia;
	private String	condicionPaciente;
	private Integer tiempoUso;
	private String	estadoSolicitudEva;
	private String	rolResponsablePendEva;
	private String 	autorizadorPertinenciaSolicitudEva;
	private Date 	fechaEvaluacionAutorizadorPertinencia;
	private String 	resultadoEvaluacionAutorizadorPertinencia;
	private	String	liderTumor;
	private Date	fechaEvaluacionLiderTumor;
	private String	resultadoEvaluacionLiderTumor;
	private String  correoEnviadoCMAC;
	private Date	fechaReunionCMAC;
	private	String	resultadoEvaluacionCMAC;
	private String	nroCartaGarantia;
	private String	tiempoAutorPertinenciaEva;
	private String tiempoLiderTumor;
	private String tiempoCMAC;
	private String tiempoTotal;


	/**
	 * 
	 */
	public ReporteSolicitudesAutorizacionesResponse() {
		// TODO Auto-generated constructor stub
	}


	
	
	/**
	 * @param fechaMes
	 * @param fechaAno
	 * @param codigoSolicitudEva
	 * @param codigoPaciente
	 * @param codigoAfiliado
	 * @param fechaAfiliacion
	 * @param tipoAfiliacion
	 * @param documentoIdentidad
	 * @param paciente
	 * @param sexo
	 * @param edad
	 * @param medicamento
	 * @param fechaReceta
	 * @param cie10
	 * @param diagnostico
	 * @param grupoDiagnostico
	 * @param plan
	 * @param estadio
	 * @param tnm
	 * @param linea
	 * @param clinica
	 * @param cmp
	 * @param medicoTratantePrescriptor
	 * @param codigoSCGSolben
	 * @param fechaRegistroSCGSolben
	 * @param horaRegistroSCGSolben
	 * @param tipoSCGSolben
	 * @param estadoSCGSolben
	 * @param codigoSolicitudPre
	 * @param fechaRegistroSolicitudPre
	 * @param horaRegistroSolicitudPre
	 * @param estadoSolicitudPre
	 * @param autorizadorPertinenciaSolicitudPre
	 * @param fechaHoraRegistroSolicitudEva
	 * @param contratante
	 * @param cumplePrefInstitucional
	 * @param indicacionesCheckListPaciente
	 * @param cumpleIndicacionesCheckListPaciente
	 * @param pertinencia
	 * @param condicionPaciente
	 * @param tiempoUso
	 * @param estadoSolicitudEva
	 * @param rolResponsablePendEva
	 * @param autorizadorPertinenciaSolicitudEva
	 * @param fechaEvaluacionAutorizadorPertinencia
	 * @param resultadoEvaluacionAutorizadorPertinencia
	 * @param liderTumor
	 * @param fechaEvaluacionLiderTumor
	 * @param resultadoEvaluacionLiderTumor
	 * @param correoEnviadoCMAC
	 * @param fechaReunionCMAC
	 * @param resultadoEvaluacionCMAC
	 * @param nroCartaGarantia
	 * @param tiempoAutorPertinenciaEva
	 * @param tiempoLiderTumor
	 * @param tiempoCMAC
	 * @param tiempoTotal
	 */
	public ReporteSolicitudesAutorizacionesResponse(Integer fechaMes, Integer fechaAno, String codigoSolicitudEva,
			String codigoPaciente, String codigoAfiliado, Date fechaAfiliacion, String tipoAfiliacion,
			String documentoIdentidad, String paciente, String sexo, Integer edad, String medicamento,
			Date fechaReceta, String cie10, String diagnostico, String grupoDiagnostico, String plan, String estadio,
			String tnm, String linea, String clinica, String cmp, String medicoTratantePrescriptor,
			String codigoSCGSolben, Date fechaRegistroSCGSolben, String horaRegistroSCGSolben, String tipoSCGSolben,
			String estadoSCGSolben, String codigoSolicitudPre, Date fechaRegistroSolicitudPre,
			String horaRegistroSolicitudPre, String estadoSolicitudPre, String autorizadorPertinenciaSolicitudPre,
			Date fechaHoraRegistroSolicitudEva, String contratante, String cumplePrefInstitucional,
			String indicacionesCheckListPaciente, String cumpleIndicacionesCheckListPaciente, String pertinencia,
			String condicionPaciente, Integer tiempoUso, String estadoSolicitudEva, String rolResponsablePendEva,
			String autorizadorPertinenciaSolicitudEva, Date fechaEvaluacionAutorizadorPertinencia,
			String resultadoEvaluacionAutorizadorPertinencia, String liderTumor, Date fechaEvaluacionLiderTumor,
			String resultadoEvaluacionLiderTumor, String correoEnviadoCMAC, Date fechaReunionCMAC,
			String resultadoEvaluacionCMAC, String nroCartaGarantia, String tiempoAutorPertinenciaEva,
			String tiempoLiderTumor, String tiempoCMAC, String tiempoTotal) {
		super();
		this.fechaMes = fechaMes;
		this.fechaAno = fechaAno;
		this.codigoSolicitudEva = codigoSolicitudEva;
		this.codigoPaciente = codigoPaciente;
		this.codigoAfiliado = codigoAfiliado;
		this.fechaAfiliacion = fechaAfiliacion;
		this.tipoAfiliacion = tipoAfiliacion;
		this.documentoIdentidad = documentoIdentidad;
		this.paciente = paciente;
		this.sexo = sexo;
		this.edad = edad;
		this.medicamento = medicamento;
		this.fechaReceta = fechaReceta;
		this.cie10 = cie10;
		this.diagnostico = diagnostico;
		this.grupoDiagnostico = grupoDiagnostico;
		this.plan = plan;
		this.estadio = estadio;
		this.tnm = tnm;
		this.linea = linea;
		this.clinica = clinica;
		this.cmp = cmp;
		this.medicoTratantePrescriptor = medicoTratantePrescriptor;
		this.codigoSCGSolben = codigoSCGSolben;
		this.fechaRegistroSCGSolben = fechaRegistroSCGSolben;
		this.horaRegistroSCGSolben = horaRegistroSCGSolben;
		this.tipoSCGSolben = tipoSCGSolben;
		this.estadoSCGSolben = estadoSCGSolben;
		this.codigoSolicitudPre = codigoSolicitudPre;
		this.fechaRegistroSolicitudPre = fechaRegistroSolicitudPre;
		this.horaRegistroSolicitudPre = horaRegistroSolicitudPre;
		this.estadoSolicitudPre = estadoSolicitudPre;
		this.autorizadorPertinenciaSolicitudPre = autorizadorPertinenciaSolicitudPre;
		this.fechaHoraRegistroSolicitudEva = fechaHoraRegistroSolicitudEva;
		this.contratante = contratante;
		this.cumplePrefInstitucional = cumplePrefInstitucional;
		this.indicacionesCheckListPaciente = indicacionesCheckListPaciente;
		this.cumpleIndicacionesCheckListPaciente = cumpleIndicacionesCheckListPaciente;
		this.pertinencia = pertinencia;
		this.condicionPaciente = condicionPaciente;
		this.tiempoUso = tiempoUso;
		this.estadoSolicitudEva = estadoSolicitudEva;
		this.rolResponsablePendEva = rolResponsablePendEva;
		this.autorizadorPertinenciaSolicitudEva = autorizadorPertinenciaSolicitudEva;
		this.fechaEvaluacionAutorizadorPertinencia = fechaEvaluacionAutorizadorPertinencia;
		this.resultadoEvaluacionAutorizadorPertinencia = resultadoEvaluacionAutorizadorPertinencia;
		this.liderTumor = liderTumor;
		this.fechaEvaluacionLiderTumor = fechaEvaluacionLiderTumor;
		this.resultadoEvaluacionLiderTumor = resultadoEvaluacionLiderTumor;
		this.correoEnviadoCMAC = correoEnviadoCMAC;
		this.fechaReunionCMAC = fechaReunionCMAC;
		this.resultadoEvaluacionCMAC = resultadoEvaluacionCMAC;
		this.nroCartaGarantia = nroCartaGarantia;
		this.tiempoAutorPertinenciaEva = tiempoAutorPertinenciaEva;
		this.tiempoLiderTumor = tiempoLiderTumor;
		this.tiempoCMAC = tiempoCMAC;
		this.tiempoTotal = tiempoTotal;
	}




	/**
	 * @return the fechaMes
	 */
	public Integer getFechaMes() {
		return fechaMes;
	}


	/**
	 * @param fechaMes the fechaMes to set
	 */
	public void setFechaMes(Integer fechaMes) {
		this.fechaMes = fechaMes;
	}


	/**
	 * @return the fechaAno
	 */
	public Integer getFechaAno() {
		return fechaAno;
	}


	/**
	 * @param fechaAno the fechaAno to set
	 */
	public void setFechaAno(Integer fechaAno) {
		this.fechaAno = fechaAno;
	}


	/**
	 * @return the codigoSolicitudEva
	 */
	public String getCodigoSolicitudEva() {
		return codigoSolicitudEva;
	}


	/**
	 * @param codigoSolicitudEva the codigoSolicitudEva to set
	 */
	public void setCodigoSolicitudEva(String codigoSolicitudEva) {
		this.codigoSolicitudEva = codigoSolicitudEva;
	}


	/**
	 * @return the codigoPaciente
	 */
	public String getCodigoPaciente() {
		return codigoPaciente;
	}


	/**
	 * @param codigoPaciente the codigoPaciente to set
	 */
	public void setCodigoPaciente(String codigoPaciente) {
		this.codigoPaciente = codigoPaciente;
	}


	/**
	 * @return the codigoAfiliado
	 */
	public String getCodigoAfiliado() {
		return codigoAfiliado;
	}


	/**
	 * @param codigoAfiliado the codigoAfiliado to set
	 */
	public void setCodigoAfiliado(String codigoAfiliado) {
		this.codigoAfiliado = codigoAfiliado;
	}


	/**
	 * @return the fechaAfiliacion
	 */
	public Date getFechaAfiliacion() {
		return fechaAfiliacion;
	}


	/**
	 * @param fechaAfiliacion the fechaAfiliacion to set
	 */
	public void setFechaAfiliacion(Date fechaAfiliacion) {
		this.fechaAfiliacion = fechaAfiliacion;
	}


	/**
	 * @return the tipoAfiliacion
	 */
	public String getTipoAfiliacion() {
		return tipoAfiliacion;
	}


	/**
	 * @param tipoAfiliacion the tipoAfiliacion to set
	 */
	public void setTipoAfiliacion(String tipoAfiliacion) {
		this.tipoAfiliacion = tipoAfiliacion;
	}


	/**
	 * @return the documentoIdentidad
	 */
	public String getDocumentoIdentidad() {
		return documentoIdentidad;
	}


	/**
	 * @param documentoIdentidad the documentoIdentidad to set
	 */
	public void setDocumentoIdentidad(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}


	/**
	 * @return the paciente
	 */
	public String getPaciente() {
		return paciente;
	}


	/**
	 * @param paciente the paciente to set
	 */
	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}


	/**
	 * @return the sexo
	 */
	public String getSexo() {
		return sexo;
	}


	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}


	/**
	 * @return the edad
	 */
	public Integer getEdad() {
		return edad;
	}


	/**
	 * @param edad the edad to set
	 */
	public void setEdad(Integer edad) {
		this.edad = edad;
	}


	/**
	 * @return the medicamento
	 */
	public String getMedicamento() {
		return medicamento;
	}


	/**
	 * @param medicamento the medicamento to set
	 */
	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}


	/**
	 * @return the fechaReceta
	 */
	public Date getFechaReceta() {
		return fechaReceta;
	}


	/**
	 * @param fechaReceta the fechaReceta to set
	 */
	public void setFechaReceta(Date fechaReceta) {
		this.fechaReceta = fechaReceta;
	}


	/**
	 * @return the cie10
	 */
	public String getCie10() {
		return cie10;
	}


	/**
	 * @param cie10 the cie10 to set
	 */
	public void setCie10(String cie10) {
		this.cie10 = cie10;
	}


	/**
	 * @return the diagnostico
	 */
	public String getDiagnostico() {
		return diagnostico;
	}


	/**
	 * @param diagnostico the diagnostico to set
	 */
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}


	/**
	 * @return the grupoDiagnostico
	 */
	public String getGrupoDiagnostico() {
		return grupoDiagnostico;
	}


	/**
	 * @param grupoDiagnostico the grupoDiagnostico to set
	 */
	public void setGrupoDiagnostico(String grupoDiagnostico) {
		this.grupoDiagnostico = grupoDiagnostico;
	}


	/**
	 * @return the plan
	 */
	public String getPlan() {
		return plan;
	}


	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan) {
		this.plan = plan;
	}


	/**
	 * @return the estadio
	 */
	public String getEstadio() {
		return estadio;
	}


	/**
	 * @param estadio the estadio to set
	 */
	public void setEstadio(String estadio) {
		this.estadio = estadio;
	}


	/**
	 * @return the tnm
	 */
	public String getTnm() {
		return tnm;
	}


	/**
	 * @param tnm the tnm to set
	 */
	public void setTnm(String tnm) {
		this.tnm = tnm;
	}


	/**
	 * @return the linea
	 */
	public String getLinea() {
		return linea;
	}


	/**
	 * @param linea the linea to set
	 */
	public void setLinea(String linea) {
		this.linea = linea;
	}


	/**
	 * @return the clinica
	 */
	public String getClinica() {
		return clinica;
	}


	/**
	 * @param clinica the clinica to set
	 */
	public void setClinica(String clinica) {
		this.clinica = clinica;
	}


	/**
	 * @return the cmp
	 */
	public String getCmp() {
		return cmp;
	}


	/**
	 * @param cmp the cmp to set
	 */
	public void setCmp(String cmp) {
		this.cmp = cmp;
	}


	/**
	 * @return the medicoTratantePrescriptor
	 */
	public String getMedicoTratantePrescriptor() {
		return medicoTratantePrescriptor;
	}


	/**
	 * @param medicoTratantePrescriptor the medicoTratantePrescriptor to set
	 */
	public void setMedicoTratantePrescriptor(String medicoTratantePrescriptor) {
		this.medicoTratantePrescriptor = medicoTratantePrescriptor;
	}


	/**
	 * @return the codigoSCGSolben
	 */
	public String getCodigoSCGSolben() {
		return codigoSCGSolben;
	}


	/**
	 * @param codigoSCGSolben the codigoSCGSolben to set
	 */
	public void setCodigoSCGSolben(String codigoSCGSolben) {
		this.codigoSCGSolben = codigoSCGSolben;
	}


	/**
	 * @return the fechaRegistroSCGSolben
	 */
	public Date getFechaRegistroSCGSolben() {
		return fechaRegistroSCGSolben;
	}


	/**
	 * @param fechaRegistroSCGSolben the fechaRegistroSCGSolben to set
	 */
	public void setFechaRegistroSCGSolben(Date fechaRegistroSCGSolben) {
		this.fechaRegistroSCGSolben = fechaRegistroSCGSolben;
	}


	/**
	 * @return the horaRegistroSCGSolben
	 */
	public String getHoraRegistroSCGSolben() {
		return horaRegistroSCGSolben;
	}


	/**
	 * @param horaRegistroSCGSolben the horaRegistroSCGSolben to set
	 */
	public void setHoraRegistroSCGSolben(String horaRegistroSCGSolben) {
		this.horaRegistroSCGSolben = horaRegistroSCGSolben;
	}


	/**
	 * @return the tipoSCGSolben
	 */
	public String getTipoSCGSolben() {
		return tipoSCGSolben;
	}


	/**
	 * @param tipoSCGSolben the tipoSCGSolben to set
	 */
	public void setTipoSCGSolben(String tipoSCGSolben) {
		this.tipoSCGSolben = tipoSCGSolben;
	}


	/**
	 * @return the estadoSCGSolben
	 */
	public String getEstadoSCGSolben() {
		return estadoSCGSolben;
	}


	/**
	 * @param estadoSCGSolben the estadoSCGSolben to set
	 */
	public void setEstadoSCGSolben(String estadoSCGSolben) {
		this.estadoSCGSolben = estadoSCGSolben;
	}


	/**
	 * @return the codigoSolicitudPre
	 */
	public String getCodigoSolicitudPre() {
		return codigoSolicitudPre;
	}


	/**
	 * @param codigoSolicitudPre the codigoSolicitudPre to set
	 */
	public void setCodigoSolicitudPre(String codigoSolicitudPre) {
		this.codigoSolicitudPre = codigoSolicitudPre;
	}


	/**
	 * @return the fechaRegistroSolicitudPre
	 */
	public Date getFechaRegistroSolicitudPre() {
		return fechaRegistroSolicitudPre;
	}


	/**
	 * @param fechaRegistroSolicitudPre the fechaRegistroSolicitudPre to set
	 */
	public void setFechaRegistroSolicitudPre(Date fechaRegistroSolicitudPre) {
		this.fechaRegistroSolicitudPre = fechaRegistroSolicitudPre;
	}


	/**
	 * @return the horaRegistroSolicitudPre
	 */
	public String getHoraRegistroSolicitudPre() {
		return horaRegistroSolicitudPre;
	}


	/**
	 * @param horaRegistroSolicitudPre the horaRegistroSolicitudPre to set
	 */
	public void setHoraRegistroSolicitudPre(String horaRegistroSolicitudPre) {
		this.horaRegistroSolicitudPre = horaRegistroSolicitudPre;
	}


	/**
	 * @return the estadoSolicitudPre
	 */
	public String getEstadoSolicitudPre() {
		return estadoSolicitudPre;
	}


	/**
	 * @param estadoSolicitudPre the estadoSolicitudPre to set
	 */
	public void setEstadoSolicitudPre(String estadoSolicitudPre) {
		this.estadoSolicitudPre = estadoSolicitudPre;
	}


	/**
	 * @return the autorizadorPertinenciaSolicitudPre
	 */
	public String getAutorizadorPertinenciaSolicitudPre() {
		return autorizadorPertinenciaSolicitudPre;
	}


	/**
	 * @param autorizadorPertinenciaSolicitudPre the autorizadorPertinenciaSolicitudPre to set
	 */
	public void setAutorizadorPertinenciaSolicitudPre(String autorizadorPertinenciaSolicitudPre) {
		this.autorizadorPertinenciaSolicitudPre = autorizadorPertinenciaSolicitudPre;
	}


	/**
	 * @return the fechaHoraRegistroSolicitudEva
	 */
	public Date getFechaHoraRegistroSolicitudEva() {
		return fechaHoraRegistroSolicitudEva;
	}


	/**
	 * @param fechaHoraRegistroSolicitudEva the fechaHoraRegistroSolicitudEva to set
	 */
	public void setFechaHoraRegistroSolicitudEva(Date fechaHoraRegistroSolicitudEva) {
		this.fechaHoraRegistroSolicitudEva = fechaHoraRegistroSolicitudEva;
	}


	/**
	 * @return the contratante
	 */
	public String getContratante() {
		return contratante;
	}


	/**
	 * @param contratante the contratante to set
	 */
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}


	/**
	 * @return the cumplePrefInstitucional
	 */
	public String getCumplePrefInstitucional() {
		return cumplePrefInstitucional;
	}


	/**
	 * @param cumplePrefInstitucional the cumplePrefInstitucional to set
	 */
	public void setCumplePrefInstitucional(String cumplePrefInstitucional) {
		this.cumplePrefInstitucional = cumplePrefInstitucional;
	}


	/**
	 * @return the indicacionesCheckListPaciente
	 */
	public String getIndicacionesCheckListPaciente() {
		return indicacionesCheckListPaciente;
	}


	/**
	 * @param indicacionesCheckListPaciente the indicacionesCheckListPaciente to set
	 */
	public void setIndicacionesCheckListPaciente(String indicacionesCheckListPaciente) {
		this.indicacionesCheckListPaciente = indicacionesCheckListPaciente;
	}


	/**
	 * @return the cumpleIndicacionesCheckListPaciente
	 */
	public String getCumpleIndicacionesCheckListPaciente() {
		return cumpleIndicacionesCheckListPaciente;
	}


	/**
	 * @param cumpleIndicacionesCheckListPaciente the cumpleIndicacionesCheckListPaciente to set
	 */
	public void setCumpleIndicacionesCheckListPaciente(String cumpleIndicacionesCheckListPaciente) {
		this.cumpleIndicacionesCheckListPaciente = cumpleIndicacionesCheckListPaciente;
	}


	/**
	 * @return the pertinencia
	 */
	public String getPertinencia() {
		return pertinencia;
	}


	/**
	 * @param pertinencia the pertinencia to set
	 */
	public void setPertinencia(String pertinencia) {
		this.pertinencia = pertinencia;
	}


	/**
	 * @return the condicionPaciente
	 */
	public String getCondicionPaciente() {
		return condicionPaciente;
	}


	/**
	 * @param condicionPaciente the condicionPaciente to set
	 */
	public void setCondicionPaciente(String condicionPaciente) {
		this.condicionPaciente = condicionPaciente;
	}


	/**
	 * @return the tiempoUso
	 */
	public Integer getTiempoUso() {
		return tiempoUso;
	}


	/**
	 * @param tiempoUso the tiempoUso to set
	 */
	public void setTiempoUso(Integer tiempoUso) {
		this.tiempoUso = tiempoUso;
	}


	/**
	 * @return the estadoSolicitudEva
	 */
	public String getEstadoSolicitudEva() {
		return estadoSolicitudEva;
	}


	/**
	 * @param estadoSolicitudEva the estadoSolicitudEva to set
	 */
	public void setEstadoSolicitudEva(String estadoSolicitudEva) {
		this.estadoSolicitudEva = estadoSolicitudEva;
	}


	/**
	 * @return the rolResponsablePendEva
	 */
	public String getRolResponsablePendEva() {
		return rolResponsablePendEva;
	}


	/**
	 * @param rolResponsablePendEva the rolResponsablePendEva to set
	 */
	public void setRolResponsablePendEva(String rolResponsablePendEva) {
		this.rolResponsablePendEva = rolResponsablePendEva;
	}


	/**
	 * @return the autorizadorPertinenciaSolicitudEva
	 */
	public String getAutorizadorPertinenciaSolicitudEva() {
		return autorizadorPertinenciaSolicitudEva;
	}


	/**
	 * @param autorizadorPertinenciaSolicitudEva the autorizadorPertinenciaSolicitudEva to set
	 */
	public void setAutorizadorPertinenciaSolicitudEva(String autorizadorPertinenciaSolicitudEva) {
		this.autorizadorPertinenciaSolicitudEva = autorizadorPertinenciaSolicitudEva;
	}


	/**
	 * @return the fechaEvaluacionAutorizadorPertinencia
	 */
	public Date getFechaEvaluacionAutorizadorPertinencia() {
		return fechaEvaluacionAutorizadorPertinencia;
	}


	/**
	 * @param fechaEvaluacionAutorizadorPertinencia the fechaEvaluacionAutorizadorPertinencia to set
	 */
	public void setFechaEvaluacionAutorizadorPertinencia(Date fechaEvaluacionAutorizadorPertinencia) {
		this.fechaEvaluacionAutorizadorPertinencia = fechaEvaluacionAutorizadorPertinencia;
	}


	/**
	 * @return the resultadoEvaluacionAutorizadorPertinencia
	 */
	public String getResultadoEvaluacionAutorizadorPertinencia() {
		return resultadoEvaluacionAutorizadorPertinencia;
	}


	/**
	 * @param resultadoEvaluacionAutorizadorPertinencia the resultadoEvaluacionAutorizadorPertinencia to set
	 */
	public void setResultadoEvaluacionAutorizadorPertinencia(String resultadoEvaluacionAutorizadorPertinencia) {
		this.resultadoEvaluacionAutorizadorPertinencia = resultadoEvaluacionAutorizadorPertinencia;
	}


	/**
	 * @return the liderTumor
	 */
	public String getLiderTumor() {
		return liderTumor;
	}


	/**
	 * @param liderTumor the liderTumor to set
	 */
	public void setLiderTumor(String liderTumor) {
		this.liderTumor = liderTumor;
	}


	/**
	 * @return the fechaEvaluacionLiderTumor
	 */
	public Date getFechaEvaluacionLiderTumor() {
		return fechaEvaluacionLiderTumor;
	}


	/**
	 * @param fechaEvaluacionLiderTumor the fechaEvaluacionLiderTumor to set
	 */
	public void setFechaEvaluacionLiderTumor(Date fechaEvaluacionLiderTumor) {
		this.fechaEvaluacionLiderTumor = fechaEvaluacionLiderTumor;
	}


	/**
	 * @return the resultadoEvaluacionLiderTumor
	 */
	public String getResultadoEvaluacionLiderTumor() {
		return resultadoEvaluacionLiderTumor;
	}


	/**
	 * @param resultadoEvaluacionLiderTumor the resultadoEvaluacionLiderTumor to set
	 */
	public void setResultadoEvaluacionLiderTumor(String resultadoEvaluacionLiderTumor) {
		this.resultadoEvaluacionLiderTumor = resultadoEvaluacionLiderTumor;
	}


	/**
	 * @return the correoEnviadoCMAC
	 */
	public String getCorreoEnviadoCMAC() {
		return correoEnviadoCMAC;
	}


	/**
	 * @param correoEnviadoCMAC the correoEnviadoCMAC to set
	 */
	public void setCorreoEnviadoCMAC(String correoEnviadoCMAC) {
		this.correoEnviadoCMAC = correoEnviadoCMAC;
	}


	/**
	 * @return the fechaReunionCMAC
	 */
	public Date getFechaReunionCMAC() {
		return fechaReunionCMAC;
	}


	/**
	 * @param fechaReunionCMAC the fechaReunionCMAC to set
	 */
	public void setFechaReunionCMAC(Date fechaReunionCMAC) {
		this.fechaReunionCMAC = fechaReunionCMAC;
	}


	/**
	 * @return the resultadoEvaluacionCMAC
	 */
	public String getResultadoEvaluacionCMAC() {
		return resultadoEvaluacionCMAC;
	}


	/**
	 * @param resultadoEvaluacionCMAC the resultadoEvaluacionCMAC to set
	 */
	public void setResultadoEvaluacionCMAC(String resultadoEvaluacionCMAC) {
		this.resultadoEvaluacionCMAC = resultadoEvaluacionCMAC;
	}


	/**
	 * @return the nroCartaGarantia
	 */
	public String getNroCartaGarantia() {
		return nroCartaGarantia;
	}


	/**
	 * @param nroCartaGarantia the nroCartaGarantia to set
	 */
	public void setNroCartaGarantia(String nroCartaGarantia) {
		this.nroCartaGarantia = nroCartaGarantia;
	}


	/**
	 * @return the tiempoAutorPertinenciaEva
	 */
	public String getTiempoAutorPertinenciaEva() {
		return tiempoAutorPertinenciaEva;
	}


	/**
	 * @param tiempoAutorPertinenciaEva the tiempoAutorPertinenciaEva to set
	 */
	public void setTiempoAutorPertinenciaEva(String tiempoAutorPertinenciaEva) {
		this.tiempoAutorPertinenciaEva = tiempoAutorPertinenciaEva;
	}


	/**
	 * @return the tiempoLiderTumor
	 */
	public String getTiempoLiderTumor() {
		return tiempoLiderTumor;
	}


	/**
	 * @param tiempoLiderTumor the tiempoLiderTumor to set
	 */
	public void setTiempoLiderTumor(String tiempoLiderTumor) {
		this.tiempoLiderTumor = tiempoLiderTumor;
	}


	/**
	 * @return the tiempoCMAC
	 */
	public String getTiempoCMAC() {
		return tiempoCMAC;
	}


	/**
	 * @param tiempoCMAC the tiempoCMAC to set
	 */
	public void setTiempoCMAC(String tiempoCMAC) {
		this.tiempoCMAC = tiempoCMAC;
	}


	/**
	 * @return the tiempoTotal
	 */
	public String getTiempoTotal() {
		return tiempoTotal;
	}


	/**
	 * @param tiempoTotal the tiempoTotal to set
	 */
	public void setTiempoTotal(String tiempoTotal) {
		this.tiempoTotal = tiempoTotal;
	}




	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ReporteSolicitudesAutorizacionesResponse [fechaMes=" + fechaMes + ", fechaAno=" + fechaAno
				+ ", codigoSolicitudEva=" + codigoSolicitudEva + ", codigoPaciente=" + codigoPaciente
				+ ", codigoAfiliado=" + codigoAfiliado + ", fechaAfiliacion=" + fechaAfiliacion + ", tipoAfiliacion="
				+ tipoAfiliacion + ", documentoIdentidad=" + documentoIdentidad + ", paciente=" + paciente + ", sexo="
				+ sexo + ", edad=" + edad + ", medicamento=" + medicamento + ", fechaReceta=" + fechaReceta + ", cie10="
				+ cie10 + ", diagnostico=" + diagnostico + ", grupoDiagnostico=" + grupoDiagnostico + ", plan=" + plan
				+ ", estadio=" + estadio + ", tnm=" + tnm + ", linea=" + linea + ", clinica=" + clinica + ", cmp=" + cmp
				+ ", medicoTratantePrescriptor=" + medicoTratantePrescriptor + ", codigoSCGSolben=" + codigoSCGSolben
				+ ", fechaRegistroSCGSolben=" + fechaRegistroSCGSolben + ", horaRegistroSCGSolben="
				+ horaRegistroSCGSolben + ", tipoSCGSolben=" + tipoSCGSolben + ", estadoSCGSolben=" + estadoSCGSolben
				+ ", codigoSolicitudPre=" + codigoSolicitudPre + ", fechaRegistroSolicitudPre="
				+ fechaRegistroSolicitudPre + ", horaRegistroSolicitudPre=" + horaRegistroSolicitudPre
				+ ", estadoSolicitudPre=" + estadoSolicitudPre + ", autorizadorPertinenciaSolicitudPre="
				+ autorizadorPertinenciaSolicitudPre + ", fechaHoraRegistroSolicitudEva="
				+ fechaHoraRegistroSolicitudEva + ", contratante=" + contratante + ", cumplePrefInstitucional="
				+ cumplePrefInstitucional + ", indicacionesCheckListPaciente=" + indicacionesCheckListPaciente
				+ ", cumpleIndicacionesCheckListPaciente=" + cumpleIndicacionesCheckListPaciente + ", pertinencia="
				+ pertinencia + ", condicionPaciente=" + condicionPaciente + ", tiempoUso=" + tiempoUso
				+ ", estadoSolicitudEva=" + estadoSolicitudEva + ", rolResponsablePendEva=" + rolResponsablePendEva
				+ ", autorizadorPertinenciaSolicitudEva=" + autorizadorPertinenciaSolicitudEva
				+ ", fechaEvaluacionAutorizadorPertinencia=" + fechaEvaluacionAutorizadorPertinencia
				+ ", resultadoEvaluacionAutorizadorPertinencia=" + resultadoEvaluacionAutorizadorPertinencia
				+ ", liderTumor=" + liderTumor + ", fechaEvaluacionLiderTumor=" + fechaEvaluacionLiderTumor
				+ ", resultadoEvaluacionLiderTumor=" + resultadoEvaluacionLiderTumor + ", correoEnviadoCMAC="
				+ correoEnviadoCMAC + ", fechaReunionCMAC=" + fechaReunionCMAC + ", resultadoEvaluacionCMAC="
				+ resultadoEvaluacionCMAC + ", nroCartaGarantia=" + nroCartaGarantia + ", tiempoAutorPertinenciaEva="
				+ tiempoAutorPertinenciaEva + ", tiempoLiderTumor=" + tiempoLiderTumor + ", tiempoCMAC=" + tiempoCMAC
				+ ", tiempoTotal=" + tiempoTotal + ", toString()=" + super.toString() + "]";
	}
	
	

}
