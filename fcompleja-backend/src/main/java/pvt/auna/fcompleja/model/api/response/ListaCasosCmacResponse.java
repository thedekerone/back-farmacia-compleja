package pvt.auna.fcompleja.model.api.response;

import java.util.List;

public class ListaCasosCmacResponse {

	List<ListaResponseCmac> listaresponse;
	private Integer codigoResultado;
	private String  mensajeResultado;
	
	public List<ListaResponseCmac> getListaresponse() {
		return listaresponse;
	}
	public void setListaresponse(List<ListaResponseCmac> listaresponse) {
		this.listaresponse = listaresponse;
	}
	public Integer getCodigoResultado() {
		return codigoResultado;
	}
	public void setCodigoResultado(Integer codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	public String getMensajeResultado() {
		return mensajeResultado;
	}
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}
	@Override
	public String toString() {
		return "ListaCasosCmacResponse [listaresponse=" + listaresponse + ", codigoResultado=" + codigoResultado
				+ ", mensajeResultado=" + mensajeResultado + "]";
	}
	
	
}
