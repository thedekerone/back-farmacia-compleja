package pvt.auna.fcompleja.model.api.response;

public class ResponseListCriterioInc {
	private Number codInclusion;
	private Number descripCriterio;
	private Number estado;
	
	public ResponseListCriterioInc() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Number getCodInclusion() {
		return codInclusion;
	}

	public void setCodInclusion(Number codInclusion) {
		this.codInclusion = codInclusion;
	}

	public Number getDescripCriterio() {
		return descripCriterio;
	}

	public void setDescripCriterio(Number descripCriterio) {
		this.descripCriterio = descripCriterio;
	}

	public Number getEstado() {
		return estado;
	}

	public void setEstado(Number estado) {
		this.estado = estado;
	}
	

	@Override
	public String toString() {
		return "ResponseListCriterioInc [codInclusion=" + codInclusion + ", descripCriterio=" + descripCriterio + 
				", estado=" + estado + "]";
	}
	
}
