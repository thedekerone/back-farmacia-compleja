package pvt.auna.fcompleja.model.api.response;

import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.response.ListSolPreeliminarResponse;

import java.util.List;

public class PreliminarFiltroResponse {

	private List<ListSolPreeliminarResponse> detallePreeliminar;
	private AudiResponse audiResponse;

	public List<ListSolPreeliminarResponse> getDetallePreeliminar() {
		return detallePreeliminar;
	}

	public void setDetallePreeliminar(List<ListSolPreeliminarResponse> detallePreeliminar) {
		this.detallePreeliminar = detallePreeliminar;
	}

	public AudiResponse getAudiResponse() {
		return audiResponse;
	}

	public void setAudiResponse(AudiResponse audiResponse) {
		this.audiResponse = audiResponse;
	}

}
