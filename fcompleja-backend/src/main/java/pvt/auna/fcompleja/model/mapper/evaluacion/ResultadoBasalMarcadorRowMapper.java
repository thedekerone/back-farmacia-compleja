package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.LinTrataReportDetalleBean;

public class ResultadoBasalMarcadorRowMapper implements RowMapper<LinTrataReportDetalleBean>{

	@Override
	public LinTrataReportDetalleBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		LinTrataReportDetalleBean resultadoBasalBean= new LinTrataReportDetalleBean();
		
		resultadoBasalBean.setDescripcion(rs.getString("descripcion"));
		resultadoBasalBean.setDescripcion(rs.getString("resultado"));
		resultadoBasalBean.setDescripcion(rs.getString("fec_resultado"));
		
		return resultadoBasalBean;
	}

	
}
