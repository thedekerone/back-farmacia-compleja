package pvt.auna.fcompleja.model.api.request;

public class RequestObtenerArchivo {

	private String usrApp;
	private String nomArchivo;
	private String ruta;
	
	public RequestObtenerArchivo() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public RequestObtenerArchivo(String usrApp, String nomArchivo, String ruta) {
		super();
		this.usrApp = usrApp;
		this.nomArchivo = nomArchivo;
		this.ruta = ruta;
	}


	public String getUsrApp() {
		return usrApp;
	}

	public void setUsrApp(String usrApp) {
		this.usrApp = usrApp;
	}

	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	@Override
	public String toString() {
		return "RequestObtenerArchivo [usrApp=" + usrApp + ", nomArchivo=" + nomArchivo + ", ruta=" + ruta + "]";
	}
}
