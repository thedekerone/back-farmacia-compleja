package pvt.auna.fcompleja.model.api;

import java.io.Serializable;

public class ListFiltroLiderTumorResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long cmpLiderTumor;
	private String nombreLiderTumor;
	
	public Long getCmpLiderTumor() {
		return cmpLiderTumor;
	}
	public void setCmpLiderTumor(Long cmpLiderTumor) {
		this.cmpLiderTumor = cmpLiderTumor;
	}
	public String getNombreLiderTumor() {
		return nombreLiderTumor;
	}
	public void setNombreLiderTumor(String nombreLiderTumor) {
		this.nombreLiderTumor = nombreLiderTumor;
	}

}
