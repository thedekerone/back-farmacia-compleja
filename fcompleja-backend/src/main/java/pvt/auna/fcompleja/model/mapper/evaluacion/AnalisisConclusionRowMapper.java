package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.ConclusionBean;

public class AnalisisConclusionRowMapper implements RowMapper<ConclusionBean> {

	@Override
	public ConclusionBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ConclusionBean bean = new ConclusionBean();
		bean.setCodAnalisisConclusion(rs.getInt("COD_SEV_ANA_CON"));
		bean.setValCumpleChkPaciente(rs.getInt("P_CUMPLE_CHKLIST_PER"));
		bean.setValCumplePreferenciaInst(rs.getInt("P_CUMPLE_PREF_INSTI"));
		bean.setCodMac(rs.getInt("COD_MAC"));
		bean.setMedicamentoPreferido(rs.getString("MEDICAMENTO_PREFERIDO"));
		bean.setValPertinencia(rs.getInt("P_PERTINENCIA"));
		bean.setValCondicionPaciente(rs.getInt("P_CONDICION_PACIENTE"));
		bean.setValTiempoUso(rs.getInt("P_TIEMPO_USO"));
		bean.setGrabar(rs.getString("GRABAR"));
		return bean;
	}
	
}
