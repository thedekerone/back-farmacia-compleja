package pvt.auna.fcompleja.model.api.response.evaluacion;

import java.io.Serializable;

public class ParticipanteResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codParticipante;
	private Long codUsuario;
	private String estadoParticipante;
	private String cmpMedico;
	private String nombreFirma;
	private Long codRol;
	private Long codArchivoFirma;
	private String correoElectronico;
	private String nombres;
	private String apellidos;
	private String codParticipanteLargo;
	private Integer pEstado;
	private Integer coordinador;

	private String codGrpDiag;
	private Integer pRangoEdad;

	public ParticipanteResponse() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodParticipante() {
		return codParticipante;
	}

	public void setCodParticipante(Long codParticipante) {
		this.codParticipante = codParticipante;
	}

	public Long getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(Long codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getEstadoParticipante() {
		return estadoParticipante;
	}

	public void setEstadoParticipante(String estadoParticipante) {
		this.estadoParticipante = estadoParticipante;
	}

	public String getCmpMedico() {
		return cmpMedico;
	}

	public void setCmpMedico(String cmpMedico) {
		this.cmpMedico = cmpMedico;
	}

	public String getNombreFirma() {
		return nombreFirma;
	}

	public void setNombreFirma(String nombreFirma) {
		this.nombreFirma = nombreFirma;
	}

	public Long getCodRol() {
		return codRol;
	}

	public void setCodRol(Long codRol) {
		this.codRol = codRol;
	}

	public Long getCodArchivoFirma() {
		return codArchivoFirma;
	}

	public void setCodArchivoFirma(Long codArchivoFirma) {
		this.codArchivoFirma = codArchivoFirma;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCodParticipanteLargo() {
		return codParticipanteLargo;
	}

	public void setCodParticipanteLargo(String codParticipanteLargo) {
		this.codParticipanteLargo = codParticipanteLargo;
	}

	public Integer getpEstado() {
		return pEstado;
	}

	public void setpEstado(Integer pEstado) {
		this.pEstado = pEstado;
	}

	public Integer getCoordinador() {
		return coordinador;
	}

	public void setCoordinador(Integer coordinador) {
		this.coordinador = coordinador;
	}

	public String getCodGrpDiag() {
		return codGrpDiag;
	}

	public void setCodGrpDiag(String codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}

	public Integer getpRangoEdad() {
		return pRangoEdad;
	}

	public void setpRangoEdad(Integer pRangoEdad) {
		this.pRangoEdad = pRangoEdad;
	}

	@Override
	public String toString() {
		return "ParticipanteResponse [codParticipante=" + codParticipante + ", codUsuario=" + codUsuario
				+ ", estadoParticipante=" + estadoParticipante + ", cmpMedico=" + cmpMedico + ", nombreFirma="
				+ nombreFirma + ", codRol=" + codRol + ", codArchivoFirma=" + codArchivoFirma + ", correoElectronico="
				+ correoElectronico + ", nombres=" + nombres + ", apellidos=" + apellidos + ", codParticipanteLargo="
				+ codParticipanteLargo + ", pEstado=" + pEstado + ", coordinador=" + coordinador + ", codGrpDiag="
				+ codGrpDiag + ", pRangoEdad=" + pRangoEdad + "]";
	}

}
