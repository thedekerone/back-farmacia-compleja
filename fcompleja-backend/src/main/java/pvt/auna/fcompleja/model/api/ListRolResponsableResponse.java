package pvt.auna.fcompleja.model.api;

import java.io.Serializable;

public class ListRolResponsableResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long codigoRolResponsable;
	private String nombreResponsable;
	
	public Long getCodigoRolResponsable() {
		return codigoRolResponsable;
	}
	public void setCodigoRolResponsable(Long codigoRolResponsable) {
		this.codigoRolResponsable = codigoRolResponsable;
	}
	public String getNombreResponsable() {
		return nombreResponsable;
	}
	public void setNombreResponsable(String nombreResponsable) {
		this.nombreResponsable = nombreResponsable;
	}

	
	
}
