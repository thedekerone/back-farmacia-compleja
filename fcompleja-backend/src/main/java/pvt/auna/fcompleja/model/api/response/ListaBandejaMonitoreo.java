package pvt.auna.fcompleja.model.api.response;

import java.io.Serializable;
import java.sql.Date;

public class ListaBandejaMonitoreo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long codigoMonitoreo;
	private String codigoDescripcionMonitoreo;
	private String codigoEvaluacion;
	private String fechAprobacion;
	private String codigoScgSolben;
	private String nroCartaGarantia;
	private String codigoAfiliado;
	private String codDiagnostico;
	private String nomDiagnostico;
	private String CodGrupoDiagnostico;
	private String nomGrupoDiagnostico;
	private String medicoTratante;
	private Integer edadPaciente;
	private String sexoPaciente;
	private Long codMedicamento;
	private String nomMedicamento;
	private String numeroLineaTratamiento;
	private String fecIniLineaTratamiento;
	private String codigoPaciente;
	private String nombrePaciente;
	private String codigoClinica;
	private String nombreClinica;
	private Integer codEstadoMonitoreo;
	private String nomEstadoMonitoreo;
	private Date fechaProximoMonitoreo;
	private Long codResponsableMonitoreo;
	private String nomResponsableMonitoreo;
	private Long codSolEvaluacion;
	private Long codHistLineaTrat;
	private long codEvolucion;

	public String getCodigoDescripcionMonitoreo() {
		return codigoDescripcionMonitoreo;
	}

	public void setCodigoDescripcionMonitoreo(String codigoDescripcionMonitoreo) {
		this.codigoDescripcionMonitoreo = codigoDescripcionMonitoreo;
	}

	public String getCodigoEvaluacion() {
		return codigoEvaluacion;
	}

	public void setCodigoEvaluacion(String codigoEvaluacion) {
		this.codigoEvaluacion = codigoEvaluacion;
	}

	public String getFechAprobacion() {
		return fechAprobacion;
	}

	public void setFechAprobacion(String fechAprobacion) {
		this.fechAprobacion = fechAprobacion;
	}

	public String getCodigoScgSolben() {
		return codigoScgSolben;
	}

	public void setCodigoScgSolben(String codigoScgSolben) {
		this.codigoScgSolben = codigoScgSolben;
	}

	public String getNroCartaGarantia() {
		return nroCartaGarantia;
	}

	public void setNroCartaGarantia(String nroCartaGarantia) {
		this.nroCartaGarantia = nroCartaGarantia;
	}

	public String getNumeroLineaTratamiento() {
		return numeroLineaTratamiento;
	}

	public void setNumeroLineaTratamiento(String numeroLineaTratamiento) {
		this.numeroLineaTratamiento = numeroLineaTratamiento;
	}

	public String getCodigoPaciente() {
		return codigoPaciente;
	}

	public void setCodigoPaciente(String codigoPaciente) {
		this.codigoPaciente = codigoPaciente;
	}

	public String getCodigoClinica() {
		return codigoClinica;
	}

	public void setCodigoClinica(String codigoClinica) {
		this.codigoClinica = codigoClinica;
	}

	public Date getFechaProximoMonitoreo() {
		return fechaProximoMonitoreo;
	}

	public void setFechaProximoMonitoreo(Date fechaProximoMonitoreo) {
		this.fechaProximoMonitoreo = fechaProximoMonitoreo;
	}

	public Long getCodigoMonitoreo() {
		return codigoMonitoreo;
	}

	public void setCodigoMonitoreo(Long codigoMonitoreo) {
		this.codigoMonitoreo = codigoMonitoreo;
	}

	public String getCodigoAfiliado() {
		return codigoAfiliado;
	}

	public void setCodigoAfiliado(String codigoAfiliado) {
		this.codigoAfiliado = codigoAfiliado;
	}

	public String getCodDiagnostico() {
		return codDiagnostico;
	}

	public void setCodDiagnostico(String codDiagnostico) {
		this.codDiagnostico = codDiagnostico;
	}

	public String getNomDiagnostico() {
		return nomDiagnostico;
	}

	public void setNomDiagnostico(String nomDiagnostico) {
		this.nomDiagnostico = nomDiagnostico;
	}

	public String getCodGrupoDiagnostico() {
		return CodGrupoDiagnostico;
	}

	public void setCodGrupoDiagnostico(String codGrupoDiagnostico) {
		CodGrupoDiagnostico = codGrupoDiagnostico;
	}

	public String getNomGrupoDiagnostico() {
		return nomGrupoDiagnostico;
	}

	public void setNomGrupoDiagnostico(String nomGrupoDiagnostico) {
		this.nomGrupoDiagnostico = nomGrupoDiagnostico;
	}

	public String getMedicoTratante() {
		return medicoTratante;
	}

	public void setMedicoTratante(String medicoTratante) {
		this.medicoTratante = medicoTratante;
	}

	public String getNombrePaciente() {
		return nombrePaciente;
	}

	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}

	public String getNombreClinica() {
		return nombreClinica;
	}

	public void setNombreClinica(String nombreClinica) {
		this.nombreClinica = nombreClinica;
	}

	public Integer getCodEstadoMonitoreo() {
		return codEstadoMonitoreo;
	}

	public void setCodEstadoMonitoreo(Integer codEstadoMonitoreo) {
		this.codEstadoMonitoreo = codEstadoMonitoreo;
	}

	public String getNomEstadoMonitoreo() {
		return nomEstadoMonitoreo;
	}

	public void setNomEstadoMonitoreo(String nomEstadoMonitoreo) {
		this.nomEstadoMonitoreo = nomEstadoMonitoreo;
	}

	public Long getCodResponsableMonitoreo() {
		return codResponsableMonitoreo;
	}

	public void setCodResponsableMonitoreo(Long codResponsableMonitoreo) {
		this.codResponsableMonitoreo = codResponsableMonitoreo;
	}

	public String getNomResponsableMonitoreo() {
		return nomResponsableMonitoreo;
	}

	public void setNomResponsableMonitoreo(String nomResponsableMonitoreo) {
		this.nomResponsableMonitoreo = nomResponsableMonitoreo;
	}

	public Long getCodMedicamento() {
		return codMedicamento;
	}

	public void setCodMedicamento(Long codMedicamento) {
		this.codMedicamento = codMedicamento;
	}

	public String getNomMedicamento() {
		return nomMedicamento;
	}

	public void setNomMedicamento(String nomMedicamento) {
		this.nomMedicamento = nomMedicamento;
	}

	public Integer getEdadPaciente() {
		return edadPaciente;
	}

	public void setEdadPaciente(Integer edadPaciente) {
		this.edadPaciente = edadPaciente;
	}

	public String getSexoPaciente() {
		return sexoPaciente;
	}

	public void setSexoPaciente(String sexoPaciente) {
		this.sexoPaciente = sexoPaciente;
	}

	public String getFecIniLineaTratamiento() {
		return fecIniLineaTratamiento;
	}

	public void setFecIniLineaTratamiento(String fecIniLineaTratamiento) {
		this.fecIniLineaTratamiento = fecIniLineaTratamiento;
	}

	public Long getCodSolEvaluacion() {
		return codSolEvaluacion;
	}

	public void setCodSolEvaluacion(Long codSolEvaluacion) {
		this.codSolEvaluacion = codSolEvaluacion;
	}

	public Long getCodHistLineaTrat() {
		return codHistLineaTrat;
	}

	public void setCodHistLineaTrat(Long codHistLineaTrat) {
		this.codHistLineaTrat = codHistLineaTrat;
	}

	public long getCodEvolucion() {
		return codEvolucion;
	}

	public void setCodEvolucion(long codEvolucion) {
		this.codEvolucion = codEvolucion;
	}

	@Override
	public String toString() {
		return "ListaBandejaMonitoreo [codigoMonitoreo=" + codigoMonitoreo + ", codigoDescripcionMonitoreo="
				+ codigoDescripcionMonitoreo + ", codigoEvaluacion=" + codigoEvaluacion + ", fechAprobacion="
				+ fechAprobacion + ", codigoScgSolben=" + codigoScgSolben + ", nroCartaGarantia=" + nroCartaGarantia
				+ ", codigoAfiliado=" + codigoAfiliado + ", codDiagnostico=" + codDiagnostico + ", nomDiagnostico="
				+ nomDiagnostico + ", CodGrupoDiagnostico=" + CodGrupoDiagnostico + ", nomGrupoDiagnostico="
				+ nomGrupoDiagnostico + ", medicoTratante=" + medicoTratante + ", edadPaciente=" + edadPaciente
				+ ", sexoPaciente=" + sexoPaciente + ", codMedicamento=" + codMedicamento + ", nomMedicamento="
				+ nomMedicamento + ", numeroLineaTratamiento=" + numeroLineaTratamiento + ", fecIniLineaTratamiento="
				+ fecIniLineaTratamiento + ", codigoPaciente=" + codigoPaciente + ", nombrePaciente=" + nombrePaciente
				+ ", codigoClinica=" + codigoClinica + ", nombreClinica=" + nombreClinica + ", codEstadoMonitoreo="
				+ codEstadoMonitoreo + ", nomEstadoMonitoreo=" + nomEstadoMonitoreo + ", fechaProximoMonitoreo="
				+ fechaProximoMonitoreo + ", codResponsableMonitoreo=" + codResponsableMonitoreo
				+ ", nomResponsableMonitoreo=" + nomResponsableMonitoreo + ", codSolEvaluacion=" + codSolEvaluacion
				+ ", codHistLineaTrat=" + codHistLineaTrat + ", codEvolucion=" + codEvolucion + "]";
	}

}
