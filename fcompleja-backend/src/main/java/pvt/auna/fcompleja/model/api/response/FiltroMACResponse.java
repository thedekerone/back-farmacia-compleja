package pvt.auna.fcompleja.model.api.response;

import java.util.ArrayList;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.bean.MACBean;

public class FiltroMACResponse {

    private AudiResponse audiResponse;
    private ArrayList<MACBean> dataList;

    public AudiResponse getAudiResponse() {
        return audiResponse;
    }

    public void setAudiResponse(AudiResponse audiResponse) {
        this.audiResponse = audiResponse;
    }

    public ArrayList<MACBean> getDataList() {
        return dataList;
    }

    public void setDataList(ArrayList<MACBean> lista) {
        this.dataList = lista;
    }
    
    
}
