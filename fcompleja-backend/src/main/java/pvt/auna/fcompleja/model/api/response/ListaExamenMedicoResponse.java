package pvt.auna.fcompleja.model.api.response;

import java.util.List;

public class ListaExamenMedicoResponse {

	private List<ListExamenesResponse>  ListaExamenes;
	private Long codigoResultado;
	private String mensajeResultado;
	
	public List<ListExamenesResponse> getListaExamenes() {
		return ListaExamenes;
	}
	public void setListaExamenes(List<ListExamenesResponse> listaExamenes) {
		ListaExamenes = listaExamenes;
	}
	public Long getCodigoResultado() {
		return codigoResultado;
	}
	public void setCodigoResultado(Long codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	public String getMensajeResultado() {
		return mensajeResultado;
	}
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}
	
}
