/**
 * 
 */
package pvt.auna.fcompleja.model.api;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ListaExamenesMedicos {
	
	private Integer codExamenMed;
	private String 	codExamenMedLargo;
	private Integer codExamenMedDet;
	private String 	descripcion;
	private Integer codTipoExamen;
	private String 	tipo;
	private Integer codEstadoExamenMed;
	private String	estadoExamenMed;
	private Integer codTipoIngresoRes;
	private String 	tipoIngresoRes;
	private String 	unidadMedida;
	private String	rango;
	private String	valorFijo;
	private Integer	codEstadoExamenMedDet;
	private String  estadoExamenMedDet;
	private Integer rangoMinimo;
	private Integer rangoMaximo;
	/**
	 * 
	 */
	public ListaExamenesMedicos() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param codExamenMed
	 * @param codExamenMedLargo
	 * @param codExamenMedDet
	 * @param descricion
	 * @param codTipoExamen
	 * @param tipo
	 * @param codEstadoExamenMed
	 * @param estadoExamenMed
	 * @param codTipoIngresoRes
	 * @param tipoIngresoRes
	 * @param unidadMedida
	 * @param rango
	 * @param valorFijo
	 * @param codEstadoExamenMedDet
	 * @param estadoExamenMedDet
	 * @param rangoMinimo
	 * @param rangoMaximo
	 */
	public ListaExamenesMedicos(Integer codExamenMed, String codExamenMedLargo, Integer codExamenMedDet,
			String descripcion, Integer codTipoExamen, String tipo, Integer codEstadoExamenMed, String estadoExamenMed,
			Integer codTipoIngresoRes, String tipoIngresoRes, String unidadMedida, String rango, String valorFijo,
			Integer codEstadoExamenMedDet, String estadoExamenMedDet, Integer rangoMinimo, Integer rangoMaximo) {
		super();
		this.codExamenMed = codExamenMed;
		this.codExamenMedLargo = codExamenMedLargo;
		this.codExamenMedDet = codExamenMedDet;
		this.descripcion = descripcion;
		this.codTipoExamen = codTipoExamen;
		this.tipo = tipo;
		this.codEstadoExamenMed = codEstadoExamenMed;
		this.estadoExamenMed = estadoExamenMed;
		this.codTipoIngresoRes = codTipoIngresoRes;
		this.tipoIngresoRes = tipoIngresoRes;
		this.unidadMedida = unidadMedida;
		this.rango = rango;
		this.valorFijo = valorFijo;
		this.codEstadoExamenMedDet = codEstadoExamenMedDet;
		this.estadoExamenMedDet = estadoExamenMedDet;
		this.rangoMinimo = rangoMinimo;
		this.rangoMaximo = rangoMaximo;
	}
	/**
	 * @return the codExamenMed
	 */
	public Integer getCodExamenMed() {
		return codExamenMed;
	}
	/**
	 * @param codExamenMed the codExamenMed to set
	 */
	public void setCodExamenMed(Integer codExamenMed) {
		this.codExamenMed = codExamenMed;
	}
	/**
	 * @return the codExamenMedLargo
	 */
	public String getCodExamenMedLargo() {
		return codExamenMedLargo;
	}
	/**
	 * @param codExamenMedLargo the codExamenMedLargo to set
	 */
	public void setCodExamenMedLargo(String codExamenMedLargo) {
		this.codExamenMedLargo = codExamenMedLargo;
	}
	/**
	 * @return the codExamenMedDet
	 */
	public Integer getCodExamenMedDet() {
		return codExamenMedDet;
	}
	/**
	 * @param codExamenMedDet the codExamenMedDet to set
	 */
	public void setCodExamenMedDet(Integer codExamenMedDet) {
		this.codExamenMedDet = codExamenMedDet;
	}
	/**
	 * @return the descricion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descricion the descricion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the codTipoExamen
	 */
	public Integer getCodTipoExamen() {
		return codTipoExamen;
	}
	/**
	 * @param codTipoExamen the codTipoExamen to set
	 */
	public void setCodTipoExamen(Integer codTipoExamen) {
		this.codTipoExamen = codTipoExamen;
	}
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the codEstadoExamenMed
	 */
	public Integer getCodEstadoExamenMed() {
		return codEstadoExamenMed;
	}
	/**
	 * @param codEstadoExamenMed the codEstadoExamenMed to set
	 */
	public void setCodEstadoExamenMed(Integer codEstadoExamenMed) {
		this.codEstadoExamenMed = codEstadoExamenMed;
	}
	/**
	 * @return the estadoExamenMed
	 */
	public String getEstadoExamenMed() {
		return estadoExamenMed;
	}
	/**
	 * @param estadoExamenMed the estadoExamenMed to set
	 */
	public void setEstadoExamenMed(String estadoExamenMed) {
		this.estadoExamenMed = estadoExamenMed;
	}
	/**
	 * @return the codTipoIngresoRes
	 */
	public Integer getCodTipoIngresoRes() {
		return codTipoIngresoRes;
	}
	/**
	 * @param codTipoIngresoRes the codTipoIngresoRes to set
	 */
	public void setCodTipoIngresoRes(Integer codTipoIngresoRes) {
		this.codTipoIngresoRes = codTipoIngresoRes;
	}
	/**
	 * @return the tipoIngresoRes
	 */
	public String getTipoIngresoRes() {
		return tipoIngresoRes;
	}
	/**
	 * @param tipoIngresoRes the tipoIngresoRes to set
	 */
	public void setTipoIngresoRes(String tipoIngresoRes) {
		this.tipoIngresoRes = tipoIngresoRes;
	}
	/**
	 * @return the unidadMedida
	 */
	public String getUnidadMedida() {
		return unidadMedida;
	}
	/**
	 * @param unidadMedida the unidadMedida to set
	 */
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	/**
	 * @return the rango
	 */
	public String getRango() {
		return rango;
	}
	/**
	 * @param rango the rango to set
	 */
	public void setRango(String rango) {
		this.rango = rango;
	}
	/**
	 * @return the valorFijo
	 */
	public String getValorFijo() {
		return valorFijo;
	}
	/**
	 * @param valorFijo the valorFijo to set
	 */
	public void setValorFijo(String valorFijo) {
		this.valorFijo = valorFijo;
	}
	/**
	 * @return the codEstadoExamenMedDet
	 */
	public Integer getCodEstadoExamenMedDet() {
		return codEstadoExamenMedDet;
	}
	/**
	 * @param codEstadoExamenMedDet the codEstadoExamenMedDet to set
	 */
	public void setCodEstadoExamenMedDet(Integer codEstadoExamenMedDet) {
		this.codEstadoExamenMedDet = codEstadoExamenMedDet;
	}
	/**
	 * @return the estadoExamenMedDet
	 */
	public String getEstadoExamenMedDet() {
		return estadoExamenMedDet;
	}
	/**
	 * @param estadoExamenMedDet the estadoExamenMedDet to set
	 */
	public void setEstadoExamenMedDet(String estadoExamenMedDet) {
		this.estadoExamenMedDet = estadoExamenMedDet;
	}
	/**
	 * @return the rangoMinimo
	 */
	public Integer getRangoMinimo() {
		return rangoMinimo;
	}
	/**
	 * @param rangoMinimo the rangoMinimo to set
	 */
	public void setRangoMinimo(Integer rangoMinimo) {
		this.rangoMinimo = rangoMinimo;
	}
	/**
	 * @return the rangoMaximo
	 */
	public Integer getRangoMaximo() {
		return rangoMaximo;
	}
	/**
	 * @param rangoMaximo the rangoMaximo to set
	 */
	public void setRangoMaximo(Integer rangoMaximo) {
		this.rangoMaximo = rangoMaximo;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListaExamenesMedicos [codExamenMed=" + codExamenMed + ", codExamenMedLargo=" + codExamenMedLargo
				+ ", codExamenMedDet=" + codExamenMedDet + ", descripcion=" + descripcion + ", codTipoExamen="
				+ codTipoExamen + ", tipo=" + tipo + ", codEstadoExamenMed=" + codEstadoExamenMed + ", estadoExamenMed="
				+ estadoExamenMed + ", codTipoIngresoRes=" + codTipoIngresoRes + ", tipoIngresoRes=" + tipoIngresoRes
				+ ", unidadMedida=" + unidadMedida + ", rango=" + rango + ", valorFijo=" + valorFijo
				+ ", codEstadoExamenMedDet=" + codEstadoExamenMedDet + ", estadoExamenMedDet=" + estadoExamenMedDet
				+ ", rangoMinimo=" + rangoMinimo + ", rangoMaximo=" + rangoMaximo + ", toString()=" + super.toString()
				+ "]";
	}
	
	
	
	
	
	
	
	
	

}
