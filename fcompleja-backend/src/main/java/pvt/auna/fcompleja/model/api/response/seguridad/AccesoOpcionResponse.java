package pvt.auna.fcompleja.model.api.response.seguridad;

public class AccesoOpcionResponse {
	private Integer codOpcion;
	private String  tipoOpcion;
	private String  nombreCorto;
	private String  flagAsignacion;
	
	public Integer getCodOpcion() {
		return codOpcion;
	}
	public void setCodOpcion(Integer codOpcion) {
		this.codOpcion = codOpcion;
	}
	public String getTipoOpcion() {
		return tipoOpcion;
	}
	public void setTipoOpcion(String tipoOpcion) {
		this.tipoOpcion = tipoOpcion;
	}
	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	public String getFlagAsignacion() {
		return flagAsignacion;
	}
	public void setFlagAsignacion(String flagAsignacion) {
		this.flagAsignacion = flagAsignacion;
	}
}
