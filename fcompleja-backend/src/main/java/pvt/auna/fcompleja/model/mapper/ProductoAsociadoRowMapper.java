/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.MarcadoresBean;
import pvt.auna.fcompleja.model.bean.ProductoAsociadoBean;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ProductoAsociadoRowMapper implements RowMapper<ProductoAsociadoBean> {

	@Override
	public ProductoAsociadoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ProductoAsociadoBean productoAsoc = new ProductoAsociadoBean();
		
		productoAsoc.setCodigoMac(rs.getInt("COD_MAC"));
		productoAsoc.setCodigoProducto(rs.getString("COD_PRODUCTO"));
		productoAsoc.setDescripcionGenerica(rs.getString("DESCRIPCION_GENERICA"));
		productoAsoc.setNombreComercial(rs.getString("NOMBRE_COMERCIAL"));
		productoAsoc.setCodigoLaboratorio(rs.getInt("COD_LABORATORIO"));
		productoAsoc.setLaboratorio(rs.getString("LABORATORIO"));
		productoAsoc.setCodigoEstado(rs.getInt("COD_ESTADO"));
		productoAsoc.setEstado(rs.getString("ESTADO"));
		
		return productoAsoc;
	}

}
