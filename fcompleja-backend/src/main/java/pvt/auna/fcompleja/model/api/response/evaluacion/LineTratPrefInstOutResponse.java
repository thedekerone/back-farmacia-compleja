package pvt.auna.fcompleja.model.api.response.evaluacion;

import java.util.List;

import pvt.auna.fcompleja.model.bean.LineaTratamientoBean;
import pvt.auna.fcompleja.model.bean.PreferenciaInstitucionalBean;

public class LineTratPrefInstOutResponse {
	
	private List<LineaTratamientoBean> lineaTratamiento;
	private List<PreferenciaInstitucionalBean> preferenciaInsti;
	
	public List<LineaTratamientoBean> getLineaTratamiento() {
		return lineaTratamiento;
	}
	public void setLineaTratamiento(List<LineaTratamientoBean> lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}
	public List<PreferenciaInstitucionalBean> getPreferenciaInsti() {
		return preferenciaInsti;
	}
	public void setPreferenciaInsti(List<PreferenciaInstitucionalBean> preferenciaInsti) {
		this.preferenciaInsti = preferenciaInsti;
	}
	@Override
	public String toString() {
		return "LineTratPrefInstOutResponse [lineaTratamiento=" + lineaTratamiento + ", preferenciaInsti="
				+ preferenciaInsti + "]";
	}
}
