package pvt.auna.fcompleja.model.api.response;

public class PreferenciaInstitucionalResponse {
	
	private String registro;
	private Integer codResultado;
	private String msgResultado;
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public Integer getCodResultado() {
		return codResultado;
	}
	public void setCodResultado(Integer codResultado) {
		this.codResultado = codResultado;
	}
	public String getMsgResultado() {
		return msgResultado;
	}
	public void setMsgResultado(String msgResultado) {
		this.msgResultado = msgResultado;
	}
	
	
}
