package pvt.auna.fcompleja.model.api.response;

public class ReporteIndicadorResponse {
	
	private Integer codigoResultado;
	private String mensajeResultado;
	/**
	 * 
	 */
	public ReporteIndicadorResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param codigoResultado
	 * @param mensajeResultado
	 */
	public ReporteIndicadorResponse(Integer codigoResultado, String mensajeResultado) {
		super();
		this.codigoResultado = codigoResultado;
		this.mensajeResultado = mensajeResultado;
	}
	/**
	 * @return the codigoResultado
	 */
	public Integer getCodigoResultado() {
		return codigoResultado;
	}
	/**
	 * @param codigoResultado the codigoResultado to set
	 */
	public void setCodigoResultado(Integer codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	/**
	 * @return the mensajeResultado
	 */
	public String getMensajeResultado() {
		return mensajeResultado;
	}
	/**
	 * @param mensajeResultado the mensajeResultado to set
	 */
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ReporteIndicadorResponse [codigoResultado=" + codigoResultado + ", mensajeResultado=" + mensajeResultado
				+ ", toString()=" + super.toString() + "]";
	}
	
	

}
