package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.CriterioInclusion;

public class ListaInclusionRowMapper implements RowMapper<CriterioInclusion> {

	@Override
	public CriterioInclusion mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		CriterioInclusion criterioInclusion = new CriterioInclusion();
		
		criterioInclusion.setCodigo(rs.getInt("COD_CRITERIO_INCLU"));
		criterioInclusion.setCodigoIndicacion(rs.getInt("COD_CHKLIST_INDI"));
		criterioInclusion.setDescripcion(rs.getString("DESCRIPCION"));
		
		return criterioInclusion;
	}

}
