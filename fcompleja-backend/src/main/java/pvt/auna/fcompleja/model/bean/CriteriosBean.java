package pvt.auna.fcompleja.model.bean;

import java.io.Serializable;

public class CriteriosBean implements Serializable {

	private static final long serialVersionUID = -867312705581290475L;
	private Integer codChkListIndi;//NO
	private Integer codMac;
	private Integer codGrpDiag;
	private Integer codCriterio;//NO
	private String codCriterioLargo;//NO
	private String descripcion;
	private Integer orden;
	private Integer codEstado;
	private String estado;

	public Integer getCodChkListIndi() {
		return codChkListIndi;
	}

	public void setCodChkListIndi(Integer codChkListIndi) {
		this.codChkListIndi = codChkListIndi;
	}

	public Integer getCodMac() {
		return codMac;
	}

	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}

	public Integer getCodGrpDiag() {
		return codGrpDiag;
	}

	public void setCodGrpDiag(Integer codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}

	public Integer getCodCriterio() {
		return codCriterio;
	}

	public void setCodCriterio(Integer codCriterio) {
		this.codCriterio = codCriterio;
	}

	public String getCodCriterioLargo() {
		return codCriterioLargo;
	}

	public void setCodCriterioLargo(String codCriterioLargo) {
		this.codCriterioLargo = codCriterioLargo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public Integer getCodEstado() {
		return codEstado;
	}

	public void setCodEstado(Integer codEstado) {
		this.codEstado = codEstado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "CriteriosBean [codChkListIndi=" + codChkListIndi + ", codMac=" + codMac + ", codGrpDiag=" + codGrpDiag
				+ ", codCriterio=" + codCriterio + ", codCriterioLargo=" + codCriterioLargo + ", descripcion="
				+ descripcion + ", orden=" + orden + ", codEstado=" + codEstado + ", estado=" + estado + "]";
	}

}
