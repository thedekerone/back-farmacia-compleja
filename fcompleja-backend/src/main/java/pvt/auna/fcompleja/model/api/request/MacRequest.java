package pvt.auna.fcompleja.model.api.request;

public class MacRequest {
	
	private Integer codMac;
	private String codMacLargo;
	
	public Integer getCodMac() {
		return codMac;
	}
	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}
	public String getCodMacLargo() {
		return codMacLargo;
	}
	public void setCodMacLargo(String codMacLargo) {
		this.codMacLargo = codMacLargo;
	}
}
