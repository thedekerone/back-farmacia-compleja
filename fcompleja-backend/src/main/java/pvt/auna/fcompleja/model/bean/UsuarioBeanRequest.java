package pvt.auna.fcompleja.model.bean;

public class UsuarioBeanRequest {
	private Integer codigoUsuario;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String correo;
	private String codigoTipoDocumento;
	private String numeroDocumento;
	private String telefono;
	private String celular;
	private String codigoAplicacion;
	private String codigoRol;
	private String usuarioOld;
	private String usuarioNew;
	private String clave;
	private String codigoEstado;
	private Integer usuario;
	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getCodigoTipoDocumento() {
		return codigoTipoDocumento;
	}
	public void setCodigoTipoDocumento(String codigoTipoDocumento) {
		this.codigoTipoDocumento = codigoTipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getCodigoAplicacion() {
		return codigoAplicacion;
	}
	public void setCodigoAplicacion(String codigoAplicacion) {
		this.codigoAplicacion = codigoAplicacion;
	}
	public String getCodigoRol() {
		return codigoRol;
	}
	public void setCodigoRol(String codigoRol) {
		this.codigoRol = codigoRol;
	}
	public String getUsuarioOld() {
		return usuarioOld;
	}
	public void setUsuarioOld(String usuarioOld) {
		this.usuarioOld = usuarioOld;
	}
	public String getUsuarioNew() {
		return usuarioNew;
	}
	public void setUsuarioNew(String usuarioNew) {
		this.usuarioNew = usuarioNew;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getCodigoEstado() {
		return codigoEstado;
	}
	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}
	public Integer getUsuario() {
		return usuario;
	}
	public void setUsuario(Integer usuario) {
		this.usuario = usuario;
	}
	@Override
	public String toString() {
		return "UsuarioBeanRequest [codigoUsuario=" + codigoUsuario + ", nombre=" + nombre + ", apellidoPaterno="
				+ apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno + ", correo=" + correo
				+ ", codigoTipoDocumento=" + codigoTipoDocumento + ", numeroDocumento=" + numeroDocumento
				+ ", telefono=" + telefono + ", celular=" + celular + ", codigoAplicacion=" + codigoAplicacion
				+ ", codigoRol=" + codigoRol + ", usuarioOld=" + usuarioOld + ", usuarioNew=" + usuarioNew + ", clave="
				+ clave + ", codigoEstado=" + codigoEstado + ", usuario=" + usuario + "]";
	}
}
