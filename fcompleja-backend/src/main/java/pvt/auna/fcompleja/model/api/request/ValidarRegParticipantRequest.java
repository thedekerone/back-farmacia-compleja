package pvt.auna.fcompleja.model.api.request;

public class ValidarRegParticipantRequest {

    private Integer codGrupoDiagnostico;

    private Integer codRangoEdad;


    public Integer getCodGrupoDiagnostico() {
        return codGrupoDiagnostico;
    }

    public void setCodGrupoDiagnostico(Integer codGrupoDiagnostico) {
        this.codGrupoDiagnostico = codGrupoDiagnostico;
    }

    public Integer getCodRangoEdad() {
        return codRangoEdad;
    }

    public void setCodRangoEdad(Integer codRangoEdad) {
        this.codRangoEdad = codRangoEdad;
    }

    @Override
    public String toString() {
        return "ValidarRegParticipantRequest{" +
                "codGrupoDiagnostico=" + codGrupoDiagnostico +
                ", codRangoEdad=" + codRangoEdad +
                '}';
    }
}
