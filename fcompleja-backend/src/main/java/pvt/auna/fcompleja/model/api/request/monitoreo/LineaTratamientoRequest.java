package pvt.auna.fcompleja.model.api.request.monitoreo;

public class LineaTratamientoRequest {
	private Long codAfiliado;

	public LineaTratamientoRequest(Long codAfiliado) {
		this.codAfiliado = codAfiliado;
	}

	public Long getCodAfiliado() {
		return codAfiliado;
	}

	public void setCodAfiliado(Long codAfiliado) {
		this.codAfiliado = codAfiliado;
	}

	@Override
	public String toString() {
		return "LineaTratamientoRequest [codAfiliado=" + codAfiliado + "]";
	}

}
