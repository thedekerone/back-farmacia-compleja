package pvt.auna.fcompleja.model.api.response;

import pvt.auna.fcompleja.model.bean.InfoSolbenBean;

import java.util.List;

public class InformacionScgResponse {

    private List<InfoSolbenBean> solbenBean;

	public List<InfoSolbenBean> getSolbenBean() {
		return solbenBean;
	}

	public void setSolbenBean(List<InfoSolbenBean> solbenBean) {
		this.solbenBean = solbenBean;
	}
}
