package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.PreferenciaInstitucionalBean;
import pvt.auna.fcompleja.model.bean.TratamientoBean;

public class TratamientoRowMapper implements RowMapper<TratamientoBean> {

	@Override
	public TratamientoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		TratamientoBean bean = new TratamientoBean();
		bean.setCodigoTratamiento(rs.getInt("COD_MAC"));
		bean.setDescripcionTratamiento(rs.getString("DESCRIPCION_MAC"));
		bean.setDescripcionTipoTratamiento(rs.getString("TIPO_TRATAMIENTO"));
		return bean;
	}
	
}
