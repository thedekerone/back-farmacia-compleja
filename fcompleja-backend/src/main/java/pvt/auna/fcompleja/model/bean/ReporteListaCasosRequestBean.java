package pvt.auna.fcompleja.model.bean;

import pvt.auna.fcompleja.model.api.response.ReporteListaCasosResponse;

import java.util.List;

public class ReporteListaCasosRequestBean {

	private Integer codSolEva;
    private Integer reporte;
    private String usuario;
    private String tipo;
    private List<ReporteListaCasosResponse> reporteListCasos;

    public Integer getCodSolEva() {
		return codSolEva;
	}

	public void setCodSolEva(Integer codSolEva) {
		this.codSolEva = codSolEva;
	}

	public Integer getReporte() {
        return reporte;
    }

    public void setReporte(Integer reporte) {
        this.reporte = reporte;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<ReporteListaCasosResponse> getReporteListCasos() {
        return reporteListCasos;
    }

    public void setReporteListCasos(List<ReporteListaCasosResponse> reporteListCasos) {
        this.reporteListCasos = reporteListCasos;
    }
}
