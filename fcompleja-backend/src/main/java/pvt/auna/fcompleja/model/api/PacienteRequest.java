package pvt.auna.fcompleja.model.api;

public class PacienteRequest {

	private String nombres;
	private String apellidoMaterno;
	private String apellidoPaterno;
	private Integer pvTibus;
	private String codigoAfiliado;
	private String tipoDocumento;
	private String numeroDocumento;

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public Integer getPvTibus() {
		return pvTibus;
	}

	public void setPvTibus(Integer i) {
		this.pvTibus = i;
	}

	public String getCodigoAfiliado() {
		return codigoAfiliado;
	}

	public void setCodigoAfiliado(String codigoAfiliado) {
		this.codigoAfiliado = codigoAfiliado;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	@Override
	public String toString() {
		return "PacienteRequest [nombres=" + nombres + ", apellidoMaterno=" + apellidoMaterno + ", apellidoPaterno="
				+ apellidoPaterno + ", pvTibus=" + pvTibus + ", codigoAfiliado=" + codigoAfiliado + ", tipoDocumento="
				+ tipoDocumento + ", numeroDocumento=" + numeroDocumento + "]";
	}

}
