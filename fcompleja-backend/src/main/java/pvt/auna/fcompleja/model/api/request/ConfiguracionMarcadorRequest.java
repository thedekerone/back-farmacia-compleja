package pvt.auna.fcompleja.model.api.request;

public class ConfiguracionMarcadorRequest {

    private static final long serialVersionUID = 1L;

    private String codMarcaConfigLargo;
    private Integer estadoMarcaConfig;
    private Integer codMac;
    private Integer tipoMarcador;
    private Integer codTipoEx;
    private Integer periocidadMin;
    private Integer periocidadMax;

    public String getCodMarcaConfigLargo() {
        return codMarcaConfigLargo;
    }

    public void setCodMarcaConfigLargo(String codMarcaConfigLargo) {
        this.codMarcaConfigLargo = codMarcaConfigLargo;
    }

    public Integer getEstadoMarcaConfig() {
        return estadoMarcaConfig;
    }

    public void setEstadoMarcaConfig(Integer estadoMarcaConfig) {
        this.estadoMarcaConfig = estadoMarcaConfig;
    }

    public Integer getCodMac() {
        return codMac;
    }

    public void setCodMac(Integer codMac) {
        this.codMac = codMac;
    }

    public Integer getTipoMarcador() {
        return tipoMarcador;
    }

    public void setTipoMarcador(Integer tipoMarcador) {
        this.tipoMarcador = tipoMarcador;
    }

    public Integer getCodTipoEx() {
        return codTipoEx;
    }

    public void setCodTipoEx(Integer codTipoEx) {
        this.codTipoEx = codTipoEx;
    }

    public Integer getPeriocidadMin() {
        return periocidadMin;
    }

    public void setPeriocidadMin(Integer periocidadMin) {
        this.periocidadMin = periocidadMin;
    }

    public Integer getPeriocidadMax() {
        return periocidadMax;
    }

    public void setPeriocidadMax(Integer periocidadMax) {
        this.periocidadMax = periocidadMax;
    }
}
