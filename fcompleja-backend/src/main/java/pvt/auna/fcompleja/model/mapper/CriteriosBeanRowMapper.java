package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.CriteriosBean;

public class CriteriosBeanRowMapper implements RowMapper<CriteriosBean> {

	@Override
	public CriteriosBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		CriteriosBean criterios = new CriteriosBean();
		criterios.setCodCriterio(rs.getInt("COD_CRITERIO"));
		criterios.setCodCriterioLargo(rs.getString("COD_CRITERIO_LARGO"));
		criterios.setCodChkListIndi(rs.getInt("COD_INDICADOR"));
		criterios.setDescripcion(rs.getString("DESCRIPCION"));
		criterios.setOrden(rs.getInt("ORDEN"));
		criterios.setCodEstado(rs.getInt("P_ESTADO"));
		criterios.setEstado(rs.getString("ESTADO"));
		return criterios;
	}
	
}
