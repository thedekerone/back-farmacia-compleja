package pvt.auna.fcompleja.model.api.response;

import java.util.List;

import pvt.auna.fcompleja.model.bean.ConclusionBean;
import pvt.auna.fcompleja.model.bean.TratamientoBean;

public class AnalisisConclusionResponse {
	
	private Integer cumplePrefInsti;
	private Integer codMac;
	private String descripcionMac;
	private List<TratamientoBean> tratamiento;
	private String descripcionIndicacion;
	private Integer cumpleCheckListPer;
	private List<ConclusionBean> analisisConcl;
	public Integer getCumplePrefInsti() {
		return cumplePrefInsti;
	}
	public void setCumplePrefInsti(Integer cumplePrefInsti) {
		this.cumplePrefInsti = cumplePrefInsti;
	}
	public Integer getCodMac() {
		return codMac;
	}
	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}
	public String getDescripcionMac() {
		return descripcionMac;
	}
	public List<TratamientoBean> getTratamiento() {
		return tratamiento;
	}
	public void setTratamiento(List<TratamientoBean> tratamiento) {
		this.tratamiento = tratamiento;
	}
	public void setDescripcionMac(String descripcionMac) {
		this.descripcionMac = descripcionMac;
	}
	public Integer getCumpleCheckListPer() {
		return cumpleCheckListPer;
	}
	public void setCumpleCheckListPer(Integer cumpleCheckListPer) {
		this.cumpleCheckListPer = cumpleCheckListPer;
	}
	public List<ConclusionBean> getAnalisisConcl() {
		return analisisConcl;
	}
	public void setAnalisisConcl(List<ConclusionBean> analisisConcl) {
		this.analisisConcl = analisisConcl;
	}
	public String getDescripcionIndicacion() {
		return descripcionIndicacion;
	}
	public void setDescripcionIndicacion(String descripcionIndicacion) {
		this.descripcionIndicacion = descripcionIndicacion;
	}
	
	
}
