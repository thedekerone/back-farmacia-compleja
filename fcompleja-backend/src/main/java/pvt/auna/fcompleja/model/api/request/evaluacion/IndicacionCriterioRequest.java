package pvt.auna.fcompleja.model.api.request.evaluacion;

import java.util.List;

import pvt.auna.fcompleja.model.api.response.CriterioExclusion;
import pvt.auna.fcompleja.model.api.response.CriterioInclusion;

public class IndicacionCriterioRequest {
	private Integer codigo;
	private String descripcion;
	private List<CriterioInclusion> listaCriterioInclusion;
	private List<CriterioExclusion> listaCriterioExclusion;
	private boolean selected;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<CriterioInclusion> getListaCriterioInclusion() {
		return listaCriterioInclusion;
	}
	public void setListaCriterioInclusion(List<CriterioInclusion> listaCriterioInclusion) {
		this.listaCriterioInclusion = listaCriterioInclusion;
	}
	public List<CriterioExclusion> getListaCriterioExclusion() {
		return listaCriterioExclusion;
	}
	public void setListaCriterioExclusion(List<CriterioExclusion> listaCriterioExclusion) {
		this.listaCriterioExclusion = listaCriterioExclusion;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	@Override
	public String toString() {
		return "IndicacionCriterioRequest [codigo=" + codigo + ", descripcion=" + descripcion
				+ ", listaCriterioInclusion=" + listaCriterioInclusion + ", listaCriterioExclusion="
				+ listaCriterioExclusion + ", selected=" + selected + "]";
	}
	
}
