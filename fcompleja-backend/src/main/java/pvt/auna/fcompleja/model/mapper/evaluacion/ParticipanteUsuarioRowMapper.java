package pvt.auna.fcompleja.model.mapper.evaluacion;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.ParticipanteResponse;

public class ParticipanteUsuarioRowMapper implements RowMapper<ParticipanteResponse>{

	@Override
	public ParticipanteResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ParticipanteResponse par = new ParticipanteResponse();
		
		par.setCodParticipanteLargo(rs.getString("COD_PARTICIPANTE_LARGO"));
		par.setCodParticipante(rs.getLong("COD_PARTICIPANTE"));
		par.setNombres(rs.getString("NOMBRES"));
		par.setApellidos(rs.getString("APELLIDOS"));
		par.setCodRol(rs.getLong("COD_ROL"));
		par.setCorreoElectronico(rs.getString("CORREO_ELECTRONICO"));
		par.setCmpMedico(rs.getString("CMP_MEDICO"));
	
		return par;
	}
}
