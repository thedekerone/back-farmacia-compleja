package pvt.auna.fcompleja.model.api.request;

public class EmailAlertaLiderTumorRequest {

    private static final long serialVersionUID = 1L;

    private String solEvaluacion;
    private String paciente;
    private String diagnostico;
    private Integer codMac;
    private String mac;
    //private String codSolEva;
    private String medicoAuditor;
    private String fileInformeEvaAuditorOrAutorizador;
    private String fileReceta;
    private String fileInformeMedico;
    private String fileGeriatrico;
    private String fileDocument;

    private String nameDocument;
    private String rutaDocument;


    public String getSolEvaluacion() {
        return solEvaluacion;
    }

    public void setSolEvaluacion(String solEvaluacion) {
        this.solEvaluacion = solEvaluacion;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public Integer getCodMac() {
        return codMac;
    }

    public void setCodMac(Integer codMac) {
        this.codMac = codMac;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    /*public String getCodSolEva() {
        return codSolEva;
    }

    public void setCodSolEva(String codSolEva) {
        this.codSolEva = codSolEva;
    }*/

    public String getMedicoAuditor() {
        return medicoAuditor;
    }

    public void setMedicoAuditor(String medicoAuditor) {
        this.medicoAuditor = medicoAuditor;
    }

    public String getFileInformeEvaAuditorOrAutorizador() {
        return fileInformeEvaAuditorOrAutorizador;
    }

    public void setFileInformeEvaAuditorOrAutorizador(String fileInformeEvaAuditorOrAutorizador) {
        this.fileInformeEvaAuditorOrAutorizador = fileInformeEvaAuditorOrAutorizador;
    }

    public String getFileReceta() {
        return fileReceta;
    }

    public void setFileReceta(String fileReceta) {
        this.fileReceta = fileReceta;
    }

    public String getFileInformeMedico() {
        return fileInformeMedico;
    }

    public void setFileInformeMedico(String fileInformeMedico) {
        this.fileInformeMedico = fileInformeMedico;
    }

    public String getFileGeriatrico() {
        return fileGeriatrico;
    }

    public void setFileGeriatrico(String fileGeriatrico) {
        this.fileGeriatrico = fileGeriatrico;
    }

    public String getFileDocument() {
        return fileDocument;
    }

    public void setFileDocument(String fileDocument) {
        this.fileDocument = fileDocument;
    }

    public String getNameDocument() {
        return nameDocument;
    }

    public void setNameDocument(String nameDocument) {
        this.nameDocument = nameDocument;
    }

    public String getRutaDocument() {
        return rutaDocument;
    }

    public void setRutaDocument(String rutaDocument) {
        this.rutaDocument = rutaDocument;
    }
}
