package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.PreferenciaInstitucionalBean;

public class PreferenciaInstiRowMapper implements RowMapper<PreferenciaInstitucionalBean>  {

	@Override
	public PreferenciaInstitucionalBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PreferenciaInstitucionalBean response = new PreferenciaInstitucionalBean();
		response.setCodPreferenciaInsti(rs.getInt("COD_PREF_INST"));
		response.setCodMac(rs.getInt("COD_MAC"));
		response.setDescripcionMac(rs.getString("DESCRIPCION_MAC"));
		response.setDetalleSubcondicion(rs.getString("DETALLE_SUBCONDICION"));
		response.setCumplePrefeInsti(rs.getInt("P_CUMPLE_PREF_INST"));
		return response;
	}
	
	
}
