/**
 * 
 */
package pvt.auna.fcompleja.model.bean;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class UsuarioSeguridadBean {
	
	private Integer codUsuario;
	private String usuario;
	private String nombres;
	private String apePate;
	private String apeMate;
	private String correo;
	private String codTipoDoc;
	private String tipoDoc;
	private String numeroDoc;
	private String telefono;
	private String celular;
	private String estado;
	private String codAplicacion;
	private String aplicacion;
	private String codRol;
	private String rol;
	public Integer getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApePate() {
		return apePate;
	}
	public void setApePate(String apePate) {
		this.apePate = apePate;
	}
	public String getApeMate() {
		return apeMate;
	}
	public void setApeMate(String apeMate) {
		this.apeMate = apeMate;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getCodTipoDoc() {
		return codTipoDoc;
	}
	public void setCodTipoDoc(String codTipoDoc) {
		this.codTipoDoc = codTipoDoc;
	}
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getNumeroDoc() {
		return numeroDoc;
	}
	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCodAplicacion() {
		return codAplicacion;
	}
	public void setCodAplicacion(String codAplicacion) {
		this.codAplicacion = codAplicacion;
	}
	public String getAplicacion() {
		return aplicacion;
	}
	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}
	public String getCodRol() {
		return codRol;
	}
	public void setCodRol(String codRol) {
		this.codRol = codRol;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	@Override
	public String toString() {
		return "UsuarioSeguridadBean [codUsuario=" + codUsuario + ", usuario=" + usuario + ", nombres=" + nombres
				+ ", apePate=" + apePate + ", apeMate=" + apeMate + ", correo=" + correo + ", codTipoDoc=" + codTipoDoc
				+ ", tipoDoc=" + tipoDoc + ", numeroDoc=" + numeroDoc + ", telefono=" + telefono + ", celular="
				+ celular + ", estado=" + estado + ", codAplicacion=" + codAplicacion + ", aplicacion=" + aplicacion
				+ ", codRol=" + codRol + ", rol=" + rol + "]";
	}
}
