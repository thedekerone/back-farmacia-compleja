package pvt.auna.fcompleja.model.api.response;

public class ReporteListaCasosResponse {

    private String codLagoSolEva= "00000001";
    private String paciente = "JESUS";
    private String diagnostico ="CANCER DE PROSTATA";
    private String codLargoMac = "00001";
    private String mac ="ABIRATERONA";


    public String getCodLagoSolEva() {
        return codLagoSolEva;
    }

    public void setCodLagoSolEva(String codLagoSolEva) {
        this.codLagoSolEva = codLagoSolEva;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getCodLargoMac() {
        return codLargoMac;
    }

    public void setCodLargoMac(String codLargoMac) {
        this.codLargoMac = codLargoMac;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
