package pvt.auna.fcompleja.model.api;

import java.io.Serializable;

public class ListFiltroParametroResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer codigoParametro;
	private String nombreParametro;
	private String codigoExterno;
	private String valor1Parametro;

	public Integer getCodigoParametro() {
		return codigoParametro;
	}

	public void setCodigoParametro(Integer codigoParametro) {
		this.codigoParametro = codigoParametro;
	}

	public String getNombreParametro() {
		return nombreParametro;
	}

	public void setNombreParametro(String nombreParametro) {
		this.nombreParametro = nombreParametro;
	}

	public String getCodigoExterno() {
		return codigoExterno;
	}

	public void setCodigoExterno(String codigoExterno) {
		this.codigoExterno = codigoExterno;
	}

	public String getValor1Parametro() {
		return valor1Parametro;
	}

	public void setValor1Parametro(String valor1Parametro) {
		this.valor1Parametro = valor1Parametro;
	}
}
