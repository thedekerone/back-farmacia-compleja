package pvt.auna.fcompleja.model.api.response;

import java.io.Serializable;

public class SolicitudPreliminarResponse implements Serializable{

    private static final long serialVersionUID = -8022474255483626723L;

    private String op_cod_scg_solben;
    private Integer op_cod_sol_pre;
    private String op_fec_sol_pre;
    private String op_hora_sol_pre;
    private Integer op_cod_resultado;
    private String op_msg_resultado;

    public String getOp_cod_scg_solben() {
        return op_cod_scg_solben;
    }

    public void setOp_cod_scg_solben(String op_cod_scg_solben) {
        this.op_cod_scg_solben = op_cod_scg_solben;
    }

    public Integer getOp_cod_sol_pre() {
        return op_cod_sol_pre;
    }

    public void setOp_cod_sol_pre(Integer op_cod_sol_pre) {
        this.op_cod_sol_pre = op_cod_sol_pre;
    }

    public String getOp_fec_sol_pre() {
        return op_fec_sol_pre;
    }

    public void setOp_fec_sol_pre(String op_fec_sol_pre) {
        this.op_fec_sol_pre = op_fec_sol_pre;
    }

    public String getOp_hora_sol_pre() {
        return op_hora_sol_pre;
    }

    public void setOp_hora_sol_pre(String op_hora_sol_pre) {
        this.op_hora_sol_pre = op_hora_sol_pre;
    }

    public Integer getOp_cod_resultado() {
        return op_cod_resultado;
    }

    public void setOp_cod_resultado(Integer op_cod_resultado) {
        this.op_cod_resultado = op_cod_resultado;
    }

    public String getOp_msg_resultado() {
        return op_msg_resultado;
    }

    public void setOp_msg_resultado(String op_msg_resultado) {
        this.op_msg_resultado = op_msg_resultado;
    }

    @Override
    public String toString() {
        return "SolicitudPreliminarResponse{" +
                "op_cod_scg_solben='" + op_cod_scg_solben + '\'' +
                ", op_cod_sol_pre=" + op_cod_sol_pre +
                ", op_fec_sol_pre='" + op_fec_sol_pre + '\'' +
                ", op_hora_sol_pre='" + op_hora_sol_pre + '\'' +
                ", op_cod_resultado=" + op_cod_resultado +
                ", op_msg_resultado='" + op_msg_resultado + '\'' +
                '}';
    }
}
