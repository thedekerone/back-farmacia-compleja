package pvt.auna.fcompleja.model.api.response.evaluacion;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ResultadoBasalDetResponse {

	private Integer codExamenMed;
	private Integer tipoIngresoResul;
	private String valorFijo;
	private Integer codConfigMarcador;
	private String descripcionExamenMed;
	private String tipoIngresoResulDescrip;
	private String unidadMedida;
	private String rango;
	private String resultado;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fecResultado;

	private Integer codResultadoBasal;
	private Integer minLength;
	private Integer maxLength;
	private String perMinima;
	private String perMaxima;

	private List<ComboResultadoBasalResponse> cmbValorFijo;

	public Integer getCodExamenMed() {
		return codExamenMed;
	}

	public void setCodExamenMed(Integer codExamenMed) {
		this.codExamenMed = codExamenMed;
	}

	public Integer getTipoIngresoResul() {
		return tipoIngresoResul;
	}

	public void setTipoIngresoResul(Integer tipoIngresoResul) {
		this.tipoIngresoResul = tipoIngresoResul;
	}

	public String getValorFijo() {
		return valorFijo;
	}

	public void setValorFijo(String valorFijo) {
		this.valorFijo = valorFijo;
	}

	public Integer getCodConfigMarcador() {
		return codConfigMarcador;
	}

	public void setCodConfigMarcador(Integer codConfigMarcador) {
		this.codConfigMarcador = codConfigMarcador;
	}

	public String getDescripcionExamenMed() {
		return descripcionExamenMed;
	}

	public void setDescripcionExamenMed(String descripcionExamenMed) {
		this.descripcionExamenMed = descripcionExamenMed;
	}

	public String getTipoIngresoResulDescrip() {
		return tipoIngresoResulDescrip;
	}

	public void setTipoIngresoResulDescrip(String tipoIngresoResulDescrip) {
		this.tipoIngresoResulDescrip = tipoIngresoResulDescrip;
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public String getRango() {
		return rango;
	}

	public void setRango(String rango) {
		this.rango = rango;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public Date getFecResultado() {
		return fecResultado;
	}

	public void setFecResultado(Date fecResultado) {
		this.fecResultado = fecResultado;
	}

	public Integer getCodResultadoBasal() {
		return codResultadoBasal;
	}

	public void setCodResultadoBasal(Integer codResultadoBasal) {
		this.codResultadoBasal = codResultadoBasal;
	}

	public Integer getMinLength() {
		return minLength;
	}

	public void setMinLength(Integer minLength) {
		this.minLength = minLength;
	}

	public Integer getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}

	public List<ComboResultadoBasalResponse> getCmbValorFijo() {
		return cmbValorFijo;
	}

	public void setCmbValorFijo(List<ComboResultadoBasalResponse> cmbValorFijo) {
		this.cmbValorFijo = cmbValorFijo;
	}

	public String getPerMinima() {
		return perMinima;
	}

	public void setPerMinima(String perMinima) {
		this.perMinima = perMinima;
	}

	public String getPerMaxima() {
		return perMaxima;
	}

	public void setPerMaxima(String perMaxima) {
		this.perMaxima = perMaxima;
	}

}
