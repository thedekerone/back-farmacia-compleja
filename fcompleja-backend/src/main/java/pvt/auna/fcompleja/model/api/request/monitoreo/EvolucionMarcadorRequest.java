package pvt.auna.fcompleja.model.api.request.monitoreo;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class EvolucionMarcadorRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codEvolucionMarcador;
	private Long codEvolucion;
	private Long codMarcador;
	private Integer codResultado;
	private String resultado;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fecResultado;
	private String tieneRegHc;
	private Integer pPerMinima;
	private Integer pPerMaxima;
	private Integer pTipoIngresoRes;
	private String descPerMinima;
	private String descPerMaxima;
	private String descripcion;
	private String unidadMedida;
	private String usuariocrea;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaCrea;
	private String usuarioModif;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaModif;

	private Long codSolEva;

	public Long getCodEvolucionMarcador() {
		return codEvolucionMarcador;
	}

	public void setCodEvolucionMarcador(Long codEvolucionMarcador) {
		this.codEvolucionMarcador = codEvolucionMarcador;
	}

	public Long getCodEvolucion() {
		return codEvolucion;
	}

	public void setCodEvolucion(Long codEvolucion) {
		this.codEvolucion = codEvolucion;
	}

	public Long getCodMarcador() {
		return codMarcador;
	}

	public void setCodMarcador(Long codMarcador) {
		this.codMarcador = codMarcador;
	}

	public Integer getCodResultado() {
		return codResultado;
	}

	public void setCodResultado(Integer codResultado) {
		this.codResultado = codResultado;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public Date getFecResultado() {
		return fecResultado;
	}

	public void setFecResultado(Date fecResultado) {
		this.fecResultado = fecResultado;
	}

	public String getTieneRegHc() {
		return tieneRegHc;
	}

	public void setTieneRegHc(String tieneRegHc) {
		this.tieneRegHc = tieneRegHc;
	}

	public Integer getpPerMinima() {
		return pPerMinima;
	}

	public void setpPerMinima(Integer pPerMinima) {
		this.pPerMinima = pPerMinima;
	}

	public Integer getpPerMaxima() {
		return pPerMaxima;
	}

	public void setpPerMaxima(Integer pPerMaxima) {
		this.pPerMaxima = pPerMaxima;
	}

	public Integer getpTipoIngresoRes() {
		return pTipoIngresoRes;
	}

	public void setpTipoIngresoRes(Integer pTipoIngresoRes) {
		this.pTipoIngresoRes = pTipoIngresoRes;
	}

	public String getDescPerMinima() {
		return descPerMinima;
	}

	public void setDescPerMinima(String descPerMinima) {
		this.descPerMinima = descPerMinima;
	}

	public String getDescPerMaxima() {
		return descPerMaxima;
	}

	public void setDescPerMaxima(String descPerMaxima) {
		this.descPerMaxima = descPerMaxima;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public String getUsuariocrea() {
		return usuariocrea;
	}

	public void setUsuariocrea(String usuariocrea) {
		this.usuariocrea = usuariocrea;
	}

	public Date getFechaCrea() {
		return fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public String getUsuarioModif() {
		return usuarioModif;
	}

	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}

	public Date getFechaModif() {
		return fechaModif;
	}

	public void setFechaModif(Date fechaModif) {
		this.fechaModif = fechaModif;
	}

	public Long getCodSolEva() {
		return codSolEva;
	}

	public void setCodSolEva(Long codSolEva) {
		this.codSolEva = codSolEva;
	}

	@Override
	public String toString() {
		return "EvolucionMarcadorRequest [codEvolucionMarcador=" + codEvolucionMarcador + ", codEvolucion="
				+ codEvolucion + ", codMarcador=" + codMarcador + ", codResultado=" + codResultado + ", resultado="
				+ resultado + ", fecResultado=" + fecResultado + ", tieneRegHc=" + tieneRegHc + ", pPerMinima="
				+ pPerMinima + ", pPerMaxima=" + pPerMaxima + ", pTipoIngresoRes=" + pTipoIngresoRes
				+ ", descPerMinima=" + descPerMinima + ", descPerMaxima=" + descPerMaxima + ", descripcion="
				+ descripcion + ", unidadMedida=" + unidadMedida + ", usuariocrea=" + usuariocrea + ", fechaCrea="
				+ fechaCrea + ", usuarioModif=" + usuarioModif + ", fechaModif=" + fechaModif + ", codSolEva="
				+ codSolEva + "]";
	}

}
