package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.EmailBean;

public class CorreoParticipanteRowMapper implements RowMapper<EmailBean> {

	@Override
	public EmailBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		EmailBean response = new EmailBean();
		response.setCodigoEnvio(rs.getString("COD_ENVIO"));
		response.setCodPlantilla(rs.getString("COD_PLANTILLA"));
		response.setCodigoPlantilla(rs.getString("COD_PLANTILLA"));
		response.setTipoEnvio(rs.getInt("TIPO_ENVIO"));
		response.setFlagAdjunto(rs.getInt("FLAG ADJUNTO"));
		response.setRuta(rs.getString("RUTA"));
		response.setUsrApp(rs.getString("USRAPP"));
		response.setAsunto(rs.getString("ASUNTO"));
		response.setCuerpo(rs.getString("CUERPO"));
		response.setDestinatario(rs.getString("DESTINATARIO"));
		
		return response;
	}

}