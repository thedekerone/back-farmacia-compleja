package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.HistPacienteResponse;

public class DocumentoLineaTratRowMapper implements RowMapper<HistPacienteResponse> {

	@Override
	public HistPacienteResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		HistPacienteResponse response = new HistPacienteResponse();
		
		response.setCodChekListReq(rs.getInt("COD_DOCUMENTO_EVALUACION"));
		response.setTipoDocumento(rs.getInt("P_TIPO_DOCUMENTO"));
		response.setNombreTipoDocumento(rs.getString("NOMBRE_TIPO_DOCUMENTO"));
		response.setDescripcionDocumento(rs.getString("DESCRIPCION_DOCUMENTO"));
		response.setEstadoDocumento((rs.getInt("P_ESTADO") != 0 ) ? rs.getInt("P_ESTADO") : 89);
		response.setDescripcionEstado(rs.getString("DESCRIPCION_ESTADO"));
		return response;
	}
	
}
