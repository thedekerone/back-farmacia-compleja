package pvt.auna.fcompleja.model.api.request;

public class RequestRegCritExclucion {
	  private Number codChkListIndi;
	  private Number codExclusion;
	  private String descripCriterio;
	  private Number estado;
	  
	public RequestRegCritExclucion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Number getCodChkListIndi() {
		return codChkListIndi;
	}

	public void setCodChkListIndi(Number codChkListIndi) {
		this.codChkListIndi = codChkListIndi;
	}

	public Number getCodExclusion() {
		return codExclusion;
	}

	public void setCodExclusion(Number codExclusion) {
		this.codExclusion = codExclusion;
	}

	public String getDescripCriterio() {
		return descripCriterio;
	}

	public void setDescripCriterio(String descripCriterio) {
		this.descripCriterio = descripCriterio;
	}

	public Number getEstado() {
		return estado;
	}

	public void setEstado(Number estado) {
		this.estado = estado;
	}
	  
	@Override
	public String toString() {
		return "RequestRegCritExclucion [codChkListIndi=" + codChkListIndi + ", codExclusion=" + codExclusion + 
				", descripCriterio=" + descripCriterio +", estado=" + estado +"]";
	}
	  
}
