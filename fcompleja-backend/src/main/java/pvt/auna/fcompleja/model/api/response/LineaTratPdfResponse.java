package pvt.auna.fcompleja.model.api.response;

import pvt.auna.fcompleja.model.bean.CondicionBasalBean;
import pvt.auna.fcompleja.model.bean.LineaMetastasisBean;
import pvt.auna.fcompleja.model.bean.LineaTratamientoBean;
import pvt.auna.fcompleja.model.bean.ResultadoBasalBean;

import java.util.List;

public class LineaTratPdfResponse {

    String status;
    String message;
    private List<LineaTratamientoBean> lineaTratamiento;
    private List<CondicionBasalBean> condicionBasal;
    private List<LineaMetastasisBean> lineaMetastasisBean;
    private List<ResultadoBasalBean> resultBasalMarcador;
    
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<LineaTratamientoBean> getLineaTratamiento() {
		return lineaTratamiento;
	}
	public void setLineaTratamiento(List<LineaTratamientoBean> lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}
	public List<CondicionBasalBean> getCondicionBasal() {
		return condicionBasal;
	}
	public void setCondicionBasal(List<CondicionBasalBean> condicionBasal) {
		this.condicionBasal = condicionBasal;
	}
	public List<LineaMetastasisBean> getLineaMetastasisBean() {
		return lineaMetastasisBean;
	}
	public void setLineaMetastasisBean(List<LineaMetastasisBean> lineaMetastasisBean) {
		this.lineaMetastasisBean = lineaMetastasisBean;
	}
	public List<ResultadoBasalBean> getResultBasalMarcador() {
		return resultBasalMarcador;
	}
	public void setResultBasalMarcador(List<ResultadoBasalBean> resultBasalMarcador) {
		this.resultBasalMarcador = resultBasalMarcador;
	}
	@Override
	public String toString() {
		return "LineaTratPdfResponse [status=" + status + ", message=" + message + ", lineaTratamiento="
				+ lineaTratamiento + ", condicionBasal=" + condicionBasal + ", lineaMetastasisBean="
				+ lineaMetastasisBean + ", resultBasalMarcador=" + resultBasalMarcador + "]";
	}

}
