package pvt.auna.fcompleja.model.api.request.seguridad;

public class CambioContrasenaRequest {

    private Integer codUsuario;

    private String nuevaContrasena;

    private Integer codAplicacion;


    public Integer getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(Integer codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getNuevaContrasena() {
        return nuevaContrasena;
    }

    public void setNuevaContrasena(String nuevaContrasena) {
        this.nuevaContrasena = nuevaContrasena;
    }

    public Integer getCodAplicacion() {
        return codAplicacion;
    }

    public void setCodAplicacion(Integer codAplicacion) {
        this.codAplicacion = codAplicacion;
    }

    @Override
    public String toString() {
        return "CambioContrasenaRequest{" +
                "codUsuario=" + codUsuario +
                ", nuevaContrasena='" + nuevaContrasena + '\'' +
                ", codAplicacion=" + codAplicacion +
                '}';
    }
}
