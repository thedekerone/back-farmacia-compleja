/**
 * 
 */
package pvt.auna.fcompleja.model.api.request;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class UsuariosPorCodigoRequest {
	
	private String codigoUsuario;

	/**
	 * 
	 */
	public UsuariosPorCodigoRequest() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the codigoUsuario
	 */
	public String getCodigoUsuario() {
		return codigoUsuario;
	}

	/**
	 * @param codigoUsuario the codigoUsuario to set
	 */
	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UsuariosPorCodigoRequest [codigoUsuario=" + codigoUsuario + ", toString()=" + super.toString() + "]";
	}

	
}
