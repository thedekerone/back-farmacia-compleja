//package pvt.auna.fcompleja.model.base;
//
//import javax.persistence.*;
//import java.util.Date;
//
//
//@MappedSuperclass
//public class AuditingEntity  implements BaseEntity{
//	
//    @Column(name = "USUARIOCREA_VCH")
//    private String usuarioCreaVch;
//
//    @Temporal(TemporalType.TIMESTAMP)
//    @Column(name = "FECHACREA_DAT")
//    private Date fechaCreaDat;
//
//    @Column(name = "USUARIOMODIF_VCH")
//    private String usuarioModifVch;
//
//    @Temporal(TemporalType.TIMESTAMP)
//    @Column(name = "FECHAMODIF_DAT")
//    private Date fechaModifDat;
//
//    @Version
//    @Column(name = "row_version")
//    private Long rowVersion;
//    
//	@Override
//	public String getUsuarioCreaVch() {
//		// TODO Auto-generated method stub
//		return usuarioCreaVch;
//	}
//
//	@Override
//	public void setUsuarioCreaVch(String usuarioCreaVch) {
//		this.usuarioCreaVch = usuarioCreaVch;
//		
//	}
//
//	@Override
//	public Date getFechaCreaDat() {
//		// TODO Auto-generated method stub
//		return fechaCreaDat;
//	}
//
//	@Override
//	public void setFechaCreaDat(Date fechaCreaDat) {
//		// TODO Auto-generated method stub
//		this.fechaCreaDat = fechaCreaDat;
//	}
//
//	@Override
//	public String getUsuarioModifVch() {
//		// TODO Auto-generated method stub
//		return usuarioModifVch;
//	}
//
//	@Override
//	public void setUsuarioModifVch(String usuarioModifVch) {
//		// TODO Auto-generated method stub
//		this.usuarioModifVch = usuarioModifVch;
//	}
//
//	@Override
//	public Date getFechaModifDat() {
//		// TODO Auto-generated method stub
//		return fechaModifDat;
//	}
//
//	@Override
//	public void setFechaModifDat(Date fechaModifDat) {
//		// TODO Auto-generated method stub
//		this.fechaModifDat = fechaModifDat;
//	}
//    
//	public Long getRowVersion() {
//        return rowVersion;
//    }
//
//    public void setRowVersion(Long rowVersion) {
//        this.rowVersion = rowVersion;
//    }
//
//    
//}
