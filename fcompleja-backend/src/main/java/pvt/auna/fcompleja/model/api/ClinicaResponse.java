package pvt.auna.fcompleja.model.api;

import java.util.List;

public class ClinicaResponse {

	private List<ListClinicaResponse> clinica;
	private Long   codigoResultado;
	private String mensageResultado;
	
	public List<ListClinicaResponse> getClinica() {
		return clinica;
	}
	public void setClinica(List<ListClinicaResponse> clinica) {
		this.clinica = clinica;
	}
	public Long getCodigoResultado() {
		return codigoResultado;
	}
	public void setCodigoResultado(Long codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	public String getMensageResultado() {
		return mensageResultado;
	}
	public void setMensageResultado(String mensageResultado) {
		this.mensageResultado = mensageResultado;
	}
	
	
	
}
