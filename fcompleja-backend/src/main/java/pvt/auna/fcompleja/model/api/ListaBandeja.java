package pvt.auna.fcompleja.model.api;

import java.io.Serializable;
import java.util.Date;
import java.util.Formatter;

public class ListaBandeja implements Serializable {

	private static final long serialVersionUID = 5235374967527692423L;

	private String numeroSolEvaluacion;
	
	private Long codigoSolPre;
	
	private String numeroScgSolben;
	private String tipoScgSolben;
	private String estadoScgSolben;
	private String descEstadoSCGSolben;
	private String numeroCartaGarantia;
	private Date fechaRegistroEvaluacion;
	private String tipoEvaluacion;
	private String estadoEvaluacion;
	private Integer codRolRespEvaluacion;
	private String rolResponsableEvaluacion;
	private String correoLiderTumor;
	private String correoCmac;
	private Date fechaCmac;
	private Integer codAutorPerte;
	private String auditorPertenencia;
	private Integer codLiderTumor;
	private String liderTumor;
	private String codSolEvaluacion;
	private String codMac;
	private String codigoPaciente;
	private String codigoClinica;
	private String codigoDiagnostico;
	private String descripcionCmac;
	private String nombrePaciente;
	private String descClinica;
	private String nombreDiagnostico;
	private String horaCmac;
	private String codigoP;
	private String codigoEstadoEvaluacion;
	private String codigoResultado;
	/** codigo de estado cmac */
	private String descripcionResultado;
	private String observacion;
	private String codActa;
	private String codigoGrabado;
	private String observacionCMAC;
	private String resultadoCMAC;
	/** Variables de correo cmac y lider tumor **/
	private Integer estadoCorreoEnvCmac;
	private Integer estadoCorreoEnvLiderTumor;
	private Long codigoEnvioEnvMac;
	private Long codigoEnvioEnvLiderTumor;
	private Long codigoEnvioEnvAlerMonit;
	private Integer pTipoEvaluacion;
	
	private String numeroCodPre;
	
	private String tipoDoc;
	private String numDoc;
	
	

	public String getNumeroCodPre() {
		Formatter fmt = new Formatter();
		return String.valueOf(fmt.format("%010d", this.codigoSolPre));
	}

	public void setNumeroCodPre(String numeroCodPre) {
		this.numeroCodPre = numeroCodPre;
	}

	/* Auxiliares */
	private String paciente;
	private String diagnostico;

	public String getPaciente() {
		return paciente;
	}

	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getNumeroSolEvaluacion() {
		return numeroSolEvaluacion;
	}

	public void setNumeroSolEvaluacion(String numeroSolEvaluacion) {
		this.numeroSolEvaluacion = numeroSolEvaluacion;
	}
	
	public Long getCodigoSolPre() {
		return codigoSolPre;
	}

	public void setCodigoSolPre(Long codigoSolPre) {
		this.codigoSolPre = codigoSolPre;
	}

	public String getNumeroScgSolben() {
		return numeroScgSolben;
	}

	public void setNumeroScgSolben(String numeroScgSolben) {
		this.numeroScgSolben = numeroScgSolben;
	}

	public String getTipoScgSolben() {
		return tipoScgSolben;
	}

	public void setTipoScgSolben(String tipoScgSolben) {
		this.tipoScgSolben = tipoScgSolben;
	}

	public String getEstadoScgSolben() {
		return estadoScgSolben;
	}

	public void setEstadoScgSolben(String estadoScgSolben) {
		this.estadoScgSolben = estadoScgSolben;
	}

	public String getDescEstadoSCGSolben() {
		return descEstadoSCGSolben;
	}

	public void setDescEstadoSCGSolben(String descEstadoSCGSolben) {
		this.descEstadoSCGSolben = descEstadoSCGSolben;
	}

	public String getNumeroCartaGarantia() {
		return numeroCartaGarantia;
	}

	public void setNumeroCartaGarantia(String numeroCartaGarantia) {
		this.numeroCartaGarantia = numeroCartaGarantia;
	}

	public Date getFechaRegistroEvaluacion() {
		return fechaRegistroEvaluacion;
	}

	public void setFechaRegistroEvaluacion(Date fechaRegistroEvaluacion) {
		this.fechaRegistroEvaluacion = fechaRegistroEvaluacion;
	}

	public String getTipoEvaluacion() {
		return tipoEvaluacion;
	}

	public void setTipoEvaluacion(String tipoEvaluacion) {
		this.tipoEvaluacion = tipoEvaluacion;
	}

	public String getEstadoEvaluacion() {
		return estadoEvaluacion;
	}

	public void setEstadoEvaluacion(String estadoEvaluacion) {
		this.estadoEvaluacion = estadoEvaluacion;
	}

	public String getRolResponsableEvaluacion() {
		return rolResponsableEvaluacion;
	}

	public void setRolResponsableEvaluacion(String rolResponsableEvaluacion) {
		this.rolResponsableEvaluacion = rolResponsableEvaluacion;
	}

	public String getCorreoLiderTumor() {
		return correoLiderTumor;
	}

	public void setCorreoLiderTumor(String correoLiderTumor) {
		this.correoLiderTumor = correoLiderTumor;
	}

	public String getCorreoCmac() {
		return correoCmac;
	}

	public void setCorreoCmac(String correoCmac) {
		this.correoCmac = correoCmac;
	}

	public Date getFechaCmac() {
		return fechaCmac;
	}

	public void setFechaCmac(Date fechaCmac) {
		this.fechaCmac = fechaCmac;
	}

	public String getAuditorPertenencia() {
		return auditorPertenencia;
	}

	public void setAuditorPertenencia(String auditorPertenencia) {
		this.auditorPertenencia = auditorPertenencia;
	}

	public String getLiderTumor() {
		return liderTumor;
	}

	public void setLiderTumor(String liderTumor) {
		this.liderTumor = liderTumor;
	}

	public String getCodSolEvaluacion() {
		return codSolEvaluacion;
	}

	public void setCodSolEvaluacion(String codSolEvaluacion) {
		this.codSolEvaluacion = codSolEvaluacion;
	}

	public String getCodMac() {
		return codMac;
	}

	public void setCodMac(String codMac) {
		this.codMac = codMac;
	}

	public String getCodigoPaciente() {
		return codigoPaciente;
	}

	public void setCodigoPaciente(String codigoPaciente) {
		this.codigoPaciente = codigoPaciente;
	}

	public String getCodigoClinica() {
		return codigoClinica;
	}

	public void setCodigoClinica(String codigoClinica) {
		this.codigoClinica = codigoClinica;
	}

	public String getCodigoDiagnostico() {
		return codigoDiagnostico;
	}

	public void setCodigoDiagnostico(String codigoDiagnostico) {
		this.codigoDiagnostico = codigoDiagnostico;
	}

	public String getDescripcionCmac() {
		return descripcionCmac;
	}

	public void setDescripcionCmac(String descripcionCmac) {
		this.descripcionCmac = descripcionCmac;
	}

	public String getNombrePaciente() {
		return nombrePaciente;
	}

	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}

	public String getDescClinica() {
		return descClinica;
	}

	public void setDescClinica(String descClinica) {
		this.descClinica = descClinica;
	}

	public String getNombreDiagnostico() {
		return nombreDiagnostico;
	}

	public void setNombreDiagnostico(String nombreDiagnostico) {
		this.nombreDiagnostico = nombreDiagnostico;
	}

	public String getHoraCmac() {
		return horaCmac;
	}

	public void setHoraCmac(String horaCmac) {
		this.horaCmac = horaCmac;
	}

	public String getCodigoP() {
		return codigoP;
	}

	public void setCodigoP(String codigoP) {
		this.codigoP = codigoP;
	}

	public String getCodigoEstadoEvaluacion() {
		return codigoEstadoEvaluacion;
	}

	public void setCodigoEstadoEvaluacion(String codigoEstadoEvaluacion) {
		this.codigoEstadoEvaluacion = codigoEstadoEvaluacion;
	}

	public String getCodigoResultado() {
		return codigoResultado;
	}

	public void setCodigoResultado(String codigoResultado) {
		this.codigoResultado = codigoResultado;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public void setDescripcionResultado(String descripcionResultado) {
		this.descripcionResultado = descripcionResultado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getCodActa() {
		return codActa;
	}

	public void setCodActa(String codActa) {
		this.codActa = codActa;
	}

	public String getCodigoGrabado() {
		return codigoGrabado;
	}

	public void setCodigoGrabado(String codigoGrabado) {
		this.codigoGrabado = codigoGrabado;
	}

	public String getObservacionCMAC() {
		return observacionCMAC;
	}

	public void setObservacionCMAC(String observacionCMAC) {
		this.observacionCMAC = observacionCMAC;
	}

	public String getResultadoCMAC() {
		return resultadoCMAC;
	}

	public void setResultadoCMAC(String resultadoCMAC) {
		this.resultadoCMAC = resultadoCMAC;
	}

	public Integer getEstadoCorreoEnvCmac() {
		return estadoCorreoEnvCmac;
	}

	public void setEstadoCorreoEnvCmac(Integer estadoCorreoEnvCmac) {
		this.estadoCorreoEnvCmac = estadoCorreoEnvCmac;
	}

	public Integer getEstadoCorreoEnvLiderTumor() {
		return estadoCorreoEnvLiderTumor;
	}

	public void setEstadoCorreoEnvLiderTumor(Integer estadoCorreoEnvLiderTumor) {
		this.estadoCorreoEnvLiderTumor = estadoCorreoEnvLiderTumor;
	}

	public Long getCodigoEnvioEnvMac() {
		return codigoEnvioEnvMac;
	}

	public void setCodigoEnvioEnvMac(Long codigoEnvioEnvMac) {
		this.codigoEnvioEnvMac = codigoEnvioEnvMac;
	}

	public Long getCodigoEnvioEnvLiderTumor() {
		return codigoEnvioEnvLiderTumor;
	}

	public void setCodigoEnvioEnvLiderTumor(Long codigoEnvioEnvLiderTumor) {
		this.codigoEnvioEnvLiderTumor = codigoEnvioEnvLiderTumor;
	}

	public Long getCodigoEnvioEnvAlerMonit() {
		return codigoEnvioEnvAlerMonit;
	}

	public void setCodigoEnvioEnvAlerMonit(Long codigoEnvioEnvAlerMonit) {
		this.codigoEnvioEnvAlerMonit = codigoEnvioEnvAlerMonit;
	}

	public Integer getCodRolRespEvaluacion() {
		return codRolRespEvaluacion;
	}

	public void setCodRolRespEvaluacion(Integer codRolRespEvaluacion) {
		this.codRolRespEvaluacion = codRolRespEvaluacion;
	}

	public Integer getCodAutorPerte() {
		return codAutorPerte;
	}

	public void setCodAutorPerte(Integer codAutorPerte) {
		this.codAutorPerte = codAutorPerte;
	}

	public Integer getCodLiderTumor() {
		return codLiderTumor;
	}

	public void setCodLiderTumor(Integer codLiderTumor) {
		this.codLiderTumor = codLiderTumor;
	}

	public Integer getpTipoEvaluacion() {
		return pTipoEvaluacion;
	}

	public void setpTipoEvaluacion(Integer pTipoEvaluacion) {
		this.pTipoEvaluacion = pTipoEvaluacion;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getNumDoc() {
		return numDoc;
	}

	public void setNumDoc(String numDoc) {
		this.numDoc = numDoc;
	}

	@Override
	public String toString() {
		return "ListaBandeja [numeroSolEvaluacion=" + numeroSolEvaluacion + ", codigoSolicitudPreliminar="+codigoSolPre+", numeroScgSolben=" + numeroScgSolben
				+ ", tipoScgSolben=" + tipoScgSolben + ", estadoScgSolben=" + estadoScgSolben + ", descEstadoSCGSolben="
				+ descEstadoSCGSolben + ", numeroCartaGarantia=" + numeroCartaGarantia + ", fechaRegistroEvaluacion="
				+ fechaRegistroEvaluacion + ", tipoEvaluacion=" + tipoEvaluacion + ", estadoEvaluacion="
				+ estadoEvaluacion + ", codRolRespEvaluacion=" + codRolRespEvaluacion + ", rolResponsableEvaluacion="
				+ rolResponsableEvaluacion + ", correoLiderTumor=" + correoLiderTumor + ", correoCmac=" + correoCmac
				+ ", fechaCmac=" + fechaCmac + ", codAutorPerte=" + codAutorPerte + ", auditorPertenencia="
				+ auditorPertenencia + ", codLiderTumor=" + codLiderTumor + ", liderTumor=" + liderTumor
				+ ", codSolEvaluacion=" + codSolEvaluacion + ", codMac=" + codMac + ", codigoPaciente=" + codigoPaciente
				+ ", codigoClinica=" + codigoClinica + ", codigoDiagnostico=" + codigoDiagnostico + ", descripcionCmac="
				+ descripcionCmac + ", nombrePaciente=" + nombrePaciente + ", descClinica=" + descClinica
				+ ", nombreDiagnostico=" + nombreDiagnostico + ", horaCmac=" + horaCmac + ", codigoP=" + codigoP
				+ ", codigoEstadoEvaluacion=" + codigoEstadoEvaluacion + ", codigoResultado=" + codigoResultado
				+ ", descripcionResultado=" + descripcionResultado + ", observacion=" + observacion + ", codActa="
				+ codActa + ", codigoGrabado=" + codigoGrabado + ", observacionCMAC=" + observacionCMAC
				+ ", resultadoCMAC=" + resultadoCMAC + ", estadoCorreoEnvCmac=" + estadoCorreoEnvCmac
				+ ", estadoCorreoEnvLiderTumor=" + estadoCorreoEnvLiderTumor + ", codigoEnvioEnvMac="
				+ codigoEnvioEnvMac + ", codigoEnvioEnvLiderTumor=" + codigoEnvioEnvLiderTumor
				+ ", codigoEnvioEnvAlerMonit=" + codigoEnvioEnvAlerMonit + ", pTipoEvaluacion=" + pTipoEvaluacion
				+ ", paciente=" + paciente + ", diagnostico=" + diagnostico + ", tipoDocumento=" + tipoDoc + ", numeroDocumento"+ numDoc+ "]";
	}

}
