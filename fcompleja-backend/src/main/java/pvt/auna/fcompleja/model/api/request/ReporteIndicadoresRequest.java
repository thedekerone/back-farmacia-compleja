package pvt.auna.fcompleja.model.api.request;

public class ReporteIndicadoresRequest {
	
	private Integer mes;
	private Integer ano;
	private Integer tipo;
	/**
	 * 
	 */
	public ReporteIndicadoresRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param mes
	 * @param ano
	 */
	public ReporteIndicadoresRequest(Integer mes, Integer ano, Integer tipo) {
		super();
		this.mes = mes;
		this.ano = ano;
		this.tipo = tipo;
	}
	/**
	 * @return the mes
	 */
	public Integer getMes() {
		return mes;
	}
	/**
	 * @param mes the mes to set
	 */
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	/**
	 * @return the ano
	 */
	public Integer getAno() {
		return ano;
	}
	/**
	 * @param ano the ano to set
	 */
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "ReporteIndicadoresRequest [mes=" + mes + ", ano=" + ano + ", tipo=" + tipo + "]";
	}
	
	
	
	
}
