/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.request.ParticipanteBeanRequest;
import pvt.auna.fcompleja.model.bean.CasosEvaCmacBean;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ParticipanteRequestRowMapper implements RowMapper<ParticipanteBeanRequest>  {


    @Override
    public ParticipanteBeanRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
    	ParticipanteBeanRequest participante = new ParticipanteBeanRequest();
    	participante.setCodParticipante(rs.getInt("COD_PARTICIPANTE"));
    	participante.setCodParticipanteLargo(rs.getString("COD_PARTICIPANTE_LARGO"));
    	participante.setNombres(rs.getString("NOMBRES"));
    	participante.setApellidos(rs.getString("APELLIDOS"));
    	participante.setCodRol(rs.getInt("COD_ROL"));
    	participante.setCorreoElectronico(rs.getString("CORREO_ELECTRONICO"));
    	participante.setpEstado(rs.getInt("CODIGO_ESTADO"));
    	participante.setEstadoParticipante(rs.getString("ESTADO"));
    	participante.setCodigoArchivoFirma(rs.getInt("CODIGO_ARCHIVO_FIRMA"));
    	participante.setCodUsuario(rs.getInt("COD_USUARIO"));
    	participante.setCoordinador(rs.getInt("COORDINADOR"));
    	participante.setCmpMedico(rs.getString("CMP_MEDICO"));
    	
        return participante;
    }

}
