/**
 * 
 */
package pvt.auna.fcompleja.model.api.response;

/**
 * @author Jose.Luis/MDP
 *
 */
public class IndicadorConsumoPorGrupoMacLineaResponse extends IndicadorConsumoPorGrupoMacResponse{
	
	private Integer lineaTratamiento;

	/**
	 * 
	 */
	public IndicadorConsumoPorGrupoMacLineaResponse() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the lineaTratamiento
	 */
	public Integer getLineaTratamiento() {
		return lineaTratamiento;
	}

	/**
	 * @param lineaTratamiento the lineaTratamiento to set
	 */
	public void setLineaTratamiento(Integer lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IndicadorConsumoPorGrupoMacLineaResponse [lineaTratamiento=" + lineaTratamiento + ", toString()="
				+ super.toString() + "]";
	}
	
	

}
