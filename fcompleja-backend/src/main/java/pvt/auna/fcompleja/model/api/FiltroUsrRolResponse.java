package pvt.auna.fcompleja.model.api;

import java.util.List;

public class FiltroUsrRolResponse {

	private List<ListFiltroRolResponse>  ListaRol;
	private Long codigoResultado;
	private String mensajeResultado;

	public List<ListFiltroRolResponse> getListaRol() {
		return ListaRol;
	}
	public void setListaRol(List<ListFiltroRolResponse> listaRol) {
		ListaRol = listaRol;
	}
	public Long getCodigoResultado() {
		return codigoResultado;
	}
	public void setCodigoResultado(Long codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	public String getMensajeResultado() {
		return mensajeResultado;
	}
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}
	
	
}
