package pvt.auna.fcompleja.model.api.request;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class SolicitudPreliminarRequest implements Serializable {

	private static final long serialVersionUID = -8022474255483626723L;

	public String cod_scg_solben;
	private String cod_clinica;
	private String cod_afi_paciente;
	private Integer edad_paciente;
	private String des_contratante;
	private String des_plan;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date fec_afiliacion;

	private String cod_diagnostico;
	private String cmp_medico;
	private String medico_tratante_prescriptor;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date fec_receta;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date fec_quimio;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date fec_hosp_inicio;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date fec_hosp_fin;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date fec_scg_solben;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
	private String hora_scg_solben;

	private String tipo_scg_solben;
	private String estado_scg;
	private String desc_medicamento;
	private String desc_esquema;
	private Double total_presupuesto;
	private String desc_procedimiento;
	private String person_contacto;
	private String obs_clinica;
	private Integer ind_valida;	
	private String tx_dato_adic1;
	private String tx_dato_adic2;
	private String tx_dato_adic3;
	private String codGrpDiag;

	public String getCod_scg_solben() {
		return cod_scg_solben;
	}

	public void setCod_scg_solben(String cod_scg_solben) {
		this.cod_scg_solben = cod_scg_solben;
	}

	public String getCod_clinica() {
		return cod_clinica;
	}

	public void setCod_clinica(String cod_clinica) {
		this.cod_clinica = cod_clinica;
	}

	public String getCod_afi_paciente() {
		return cod_afi_paciente;
	}

	public void setCod_afi_paciente(String cod_afi_paciente) {
		this.cod_afi_paciente = cod_afi_paciente;
	}

	public Integer getEdad_paciente() {
		return edad_paciente;
	}

	public void setEdad_paciente(Integer edad_paciente) {
		this.edad_paciente = edad_paciente;
	}

	public String getDes_contratante() {
		return des_contratante;
	}

	public void setDes_contratante(String des_contratante) {
		this.des_contratante = des_contratante;
	}

	public String getDes_plan() {
		return des_plan;
	}

	public void setDes_plan(String des_plan) {
		this.des_plan = des_plan;
	}

	public String getCod_diagnostico() {
		return cod_diagnostico;
	}

	public void setCod_diagnostico(String cod_diagnostico) {
		this.cod_diagnostico = cod_diagnostico;
	}

	public String getCmp_medico() {
		return cmp_medico;
	}

	public void setCmp_medico(String cmp_medico) {
		this.cmp_medico = cmp_medico;
	}

	public Date getFec_quimio() {
		return fec_quimio;
	}

	public void setFec_quimio(Date fec_quimio) {
		this.fec_quimio = fec_quimio;
	}

	public Date getFec_hosp_inicio() {
		return fec_hosp_inicio;
	}

	public void setFec_hosp_inicio(Date fec_hosp_inicio) {
		this.fec_hosp_inicio = fec_hosp_inicio;
	}

	public Date getFec_hosp_fin() {
		return fec_hosp_fin;
	}

	public void setFec_hosp_fin(Date fec_hosp_fin) {
		this.fec_hosp_fin = fec_hosp_fin;
	}

	public String getTipo_scg_solben() {
		return tipo_scg_solben;
	}

	public void setTipo_scg_solben(String tipo_scg_solben) {
		this.tipo_scg_solben = tipo_scg_solben;
	}

	public String getEstado_scg() {
		return estado_scg;
	}

	public void setEstado_scg(String estado_scg) {
		this.estado_scg = estado_scg;
	}

	public String getDesc_medicamento() {
		return desc_medicamento;
	}

	public void setDesc_medicamento(String desc_medicamento) {
		this.desc_medicamento = desc_medicamento;
	}

	public String getDesc_esquema() {
		return desc_esquema;
	}

	public void setDesc_esquema(String desc_esquema) {
		this.desc_esquema = desc_esquema;
	}

	public Double getTotal_presupuesto() {
		return total_presupuesto;
	}

	public void setTotal_presupuesto(Double total_presupuesto) {
		this.total_presupuesto = total_presupuesto;
	}

	public String getDesc_procedimiento() {
		return desc_procedimiento;
	}

	public void setDesc_procedimiento(String desc_procedimiento) {
		this.desc_procedimiento = desc_procedimiento;
	}

	public String getPerson_contacto() {
		return person_contacto;
	}

	public void setPerson_contacto(String person_contacto) {
		this.person_contacto = person_contacto;
	}

	public String getObs_clinica() {
		return obs_clinica;
	}

	public void setObs_clinica(String obs_clinica) {
		this.obs_clinica = obs_clinica;
	}

	public Integer getInd_valida() {
		return ind_valida;
	}

	public void setInd_valida(Integer ind_valida) {
		this.ind_valida = ind_valida;
	}

	public Date getFec_scg_solben() {
		return fec_scg_solben;
	}

	public void setFec_scg_solben(Date fec_scg_solben) {
		this.fec_scg_solben = fec_scg_solben;
	}

	public String getHora_scg_solben() {
		return hora_scg_solben;
	}

	public void setHora_scg_solben(String hora_scg_solben) {
		this.hora_scg_solben = hora_scg_solben;
	}

	public String getTx_dato_adic1() {
		return tx_dato_adic1;
	}

	public void setTx_dato_adic1(String tx_dato_adic1) {
		this.tx_dato_adic1 = tx_dato_adic1;
	}

	public String getTx_dato_adic2() {
		return tx_dato_adic2;
	}

	public void setTx_dato_adic2(String tx_dato_adic2) {
		this.tx_dato_adic2 = tx_dato_adic2;
	}

	public String getTx_dato_adic3() {
		return tx_dato_adic3;
	}

	public void setTx_dato_adic3(String tx_dato_adic3) {
		this.tx_dato_adic3 = tx_dato_adic3;
	}

	public Date getFec_receta() {
		return fec_receta;
	}

	public void setFec_receta(Date fec_receta) {
		this.fec_receta = fec_receta;
	}

	public String getMedico_tratante_prescriptor() {
		return medico_tratante_prescriptor;
	}

	public void setMedico_tratante_prescriptor(String medico_tratante_prescriptor) {
		this.medico_tratante_prescriptor = medico_tratante_prescriptor;
	}

	public Date getFec_afiliacion() {
		return fec_afiliacion;
	}

	public void setFec_afiliacion(Date fec_afiliacion) {
		this.fec_afiliacion = fec_afiliacion;
	}

	public String getCodGrpDiag() {
		return codGrpDiag;
	}

	public void setCodGrpDiag(String codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}

	@Override
	public String toString() {
		return "SolicitudPreliminarRequest [cod_scg_solben=" + cod_scg_solben + ", cod_clinica=" + cod_clinica
				+ ", cod_afi_paciente=" + cod_afi_paciente + ", edad_paciente=" + edad_paciente + ", des_contratante="
				+ des_contratante + ", des_plan=" + des_plan + ", cod_diagnostico=" + cod_diagnostico + ", cmp_medico="
				+ cmp_medico + ", fec_quimio=" + fec_quimio + ", fec_hosp_inicio=" + fec_hosp_inicio + ", fec_hosp_fin="
				+ fec_hosp_fin + ", tipo_scg_solben=" + tipo_scg_solben + ", estado_scg=" + estado_scg
				+ ", desc_medicamento=" + desc_medicamento + ", desc_esquema=" + desc_esquema + ", total_presupuesto="
				+ total_presupuesto + ", desc_procedimiento=" + desc_procedimiento + ", person_contacto="
				+ person_contacto + ", obs_clinica=" + obs_clinica + ", ind_valida=" + ind_valida + ", fec_scg_solben="
				+ fec_scg_solben + ", hora_scg_solben=" + hora_scg_solben + ", tx_dato_adic1=" + tx_dato_adic1
				+ ", tx_dato_adic2=" + tx_dato_adic2 + ", tx_dato_adic3=" + tx_dato_adic3 + ", fec_receta=" + fec_receta
				+ ", medico_tratante_prescriptor=" + medico_tratante_prescriptor + ", fec_afiliacion=" + fec_afiliacion
				+ ", codGrpDiag=" + codGrpDiag + "]";
	}
}
