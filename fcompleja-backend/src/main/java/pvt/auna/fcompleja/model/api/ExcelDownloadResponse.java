package pvt.auna.fcompleja.model.api;

public class ExcelDownloadResponse {

    private String name;
    private String mime;
    private long length;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ExcelDonloadResponse {" + "name=" + name + ", mime=" + mime + ", length=" + length + ", url=" + url + '}';
    }

	public void setHeader(String string, String string2) {
		// TODO Auto-generated method stub
		
	}

}