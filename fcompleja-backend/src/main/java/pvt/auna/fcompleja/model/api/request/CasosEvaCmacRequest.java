package pvt.auna.fcompleja.model.api.request;

public class CasosEvaCmacRequest {

    private String codLargoSolEvaluacion;
    private String paciente;
    private String diagnostico;
    private String codLargoMac;
    private String mac;

    public String getCodLargoSolEvaluacion() {
        return codLargoSolEvaluacion;
    }

    public void setCodLargoSolEvaluacion(String codLargoSolEvaluacion) {
        this.codLargoSolEvaluacion = codLargoSolEvaluacion;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getCodLargoMac() {
        return codLargoMac;
    }

    public void setCodLargoMac(String codLargoMac) {
        this.codLargoMac = codLargoMac;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
