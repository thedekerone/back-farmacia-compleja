package pvt.auna.fcompleja.model.api;

import java.util.ArrayList;
import java.util.List;


public class ApiOutResponse {


	private Integer codResultado;
    private String msgResultado;
    private Integer total;
    private Integer consumoExiste;
    private Integer tratamientoExiste;
    private List<ListFiltroParametroResponse> extra;
    private Object response;
    private List <Object> ListResponse;

	private ArrayList<?> dataList;    


	public List<ListFiltroParametroResponse> getExtra() {
		return extra;
	}

	public void setExtra(List<ListFiltroParametroResponse> extra) {
		this.extra = extra;
	}	
	
	public Integer getCodResultado() {
        return codResultado;
    }

    public void setCodResultado(Integer codResultado) {
        this.codResultado = codResultado;
    }

    public String getMsgResultado() {
        return msgResultado;
    }

    public void setMsgResultado(String msgResultado) {
        this.msgResultado = msgResultado;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getConsumoExiste() {
        return consumoExiste;
    }

    public void setConsumoExiste(Integer consumoExiste) {
        this.consumoExiste = consumoExiste;
    }

    public Integer getTratamientoExiste() {
        return tratamientoExiste;
    }

    public void setTratamientoExiste(Integer tratamientoExiste) {
        this.tratamientoExiste = tratamientoExiste;
    }

    @Override
    public String toString() {
        return "ApiOutResponse [codResultado=" + codResultado + ", msgResultado=" + msgResultado + ", response="
                + response + "]";
    }

    public List<Object> getListResponse() {
  		return ListResponse;
  	}

  	public void setListResponse(List<Object> listResponse) {
  		ListResponse = listResponse;
  	}

	public ArrayList<?> getDataList() {
		return dataList;
	}

	public void setDataList(ArrayList<?> dataList) {
		this.dataList = dataList;
	}
}
