package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.PersonaRolResponse;

public class PersonaRolRowMapper implements RowMapper<PersonaRolResponse>{

	@Override
	public PersonaRolResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PersonaRolResponse rolPersona = new PersonaRolResponse();
		rolPersona.setNombres(rs.getString("NOMBRES"));
		rolPersona.setApePaterno(rs.getString("APE_PAT"));
		rolPersona.setApeMaterno(rs.getString("APE_MAT"));
		rolPersona.setCodRol(rs.getInt("COD_ROL"));
		rolPersona.setRolDescripcion(rs.getString("DESCRIPCION"));
		return rolPersona;
	}

}
