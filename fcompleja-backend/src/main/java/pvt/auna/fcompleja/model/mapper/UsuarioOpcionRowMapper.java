package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.MenuOpcionBean;

public class UsuarioOpcionRowMapper implements RowMapper<MenuOpcionBean> {

	@Override
	public MenuOpcionBean mapRow(ResultSet rs, int rowNum) throws SQLException {

		MenuOpcionBean response = new MenuOpcionBean();
		response.setCodOpcion(rs.getInt("COD_OPCION"));
		response.setCodMenu(rs.getInt("COD_MENU"));
		response.setTipoOpcion(rs.getString("TIPO_OPCION"));
		response.setNombreCorto(rs.getString("NOM_CORTO"));
		response.setFlagAsignacion(rs.getString("FLAG_ASIGNACION"));
		return response;
	}

}
