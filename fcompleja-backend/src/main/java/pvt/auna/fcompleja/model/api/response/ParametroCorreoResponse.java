package pvt.auna.fcompleja.model.api.response;

import java.util.List;

import pvt.auna.fcompleja.model.bean.ParticipanteCorreoBean;

public class ParametroCorreoResponse {
	
	private List<ParticipanteCorreoBean> lista;
	private Integer codigo;
	private String mensaje;
	public List<ParticipanteCorreoBean> getLista() {
		return lista;
	}
	public void setLista(List<ParticipanteCorreoBean> lista) {
		this.lista = lista;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	@Override
	public String toString() {
		return "ParametroCorreoResponse [lista=" + lista + ", codigo=" + codigo + ", mensaje=" + mensaje + "]";
	}
	
	

}
