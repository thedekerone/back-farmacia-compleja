package pvt.auna.fcompleja.model.api;

import java.util.List;

public class FiltroLiderTumorResponse {

	private List<ListFiltroLiderTumorResponse> filtroLiderTumor;
	private Long   codigoResultado;
	private String mensageResultado;
	
	public List<ListFiltroLiderTumorResponse> getFiltroLiderTumor() {
	return filtroLiderTumor;
	}
	public void setFiltroLiderTumor(List<ListFiltroLiderTumorResponse> filtroLiderTumor) {
		this.filtroLiderTumor = filtroLiderTumor;
	}
	public Long getCodigoResultado() {
		return codigoResultado;
	}
	public void setCodigoResultado(Long codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	public String getMensageResultado() {
		return mensageResultado;
	}
	public void setMensageResultado(String mensageResultado) {
		this.mensageResultado = mensageResultado;
	}

}
