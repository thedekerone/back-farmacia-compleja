/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.ReporteSolicitudesAutorizacionesResponse;
import pvt.auna.fcompleja.model.api.response.ReporteSolicitudesMonitoreoResponse;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ReporteSolicitudesMonitoreoRowMapper implements RowMapper<ReporteSolicitudesMonitoreoResponse>{

	@Override
	public ReporteSolicitudesMonitoreoResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReporteSolicitudesMonitoreoResponse reporteSolicitudMonitoreo = new ReporteSolicitudesMonitoreoResponse();		
		
		reporteSolicitudMonitoreo.setFechaMes(rs.getInt("FEC_MES"));
		reporteSolicitudMonitoreo.setFechaAno(rs.getInt("FEC_ANO"));
		reporteSolicitudMonitoreo.setCodigoSolicitudEva(rs.getString("COD_SOL_EVA"));
		reporteSolicitudMonitoreo.setCodigoPaciente(rs.getString("COD_PACIENTE"));
		reporteSolicitudMonitoreo.setCodigoAfiliado(rs.getString("COD_AFILIADO"));
		reporteSolicitudMonitoreo.setFechaAfiliacion(rs.getDate("FECHA_AFILIACION"));
		reporteSolicitudMonitoreo.setTipoAfiliacion(rs.getString("TIPO_AFILIACION"));
		reporteSolicitudMonitoreo.setDocumentoIdentidad(rs.getString("DOC_IDENTIDAD"));
		reporteSolicitudMonitoreo.setPaciente(rs.getString("PACIENTE"));
		reporteSolicitudMonitoreo.setSexo(rs.getString("SEXO"));
		reporteSolicitudMonitoreo.setEdad(rs.getInt("EDAD"));
		reporteSolicitudMonitoreo.setMedicamento(rs.getString("MEDICAMENTO"));
		reporteSolicitudMonitoreo.setCie10(rs.getString("CIE10"));
		reporteSolicitudMonitoreo.setDiagnostico(rs.getString("DIAGNOSTICO"));
		reporteSolicitudMonitoreo.setGrupoDiagnostico(rs.getString("GRUPO_DIAGNOSTICO"));
		reporteSolicitudMonitoreo.setEstadio(rs.getString("ESTADIO"));
		reporteSolicitudMonitoreo.setTnm(rs.getString("TNM"));
		reporteSolicitudMonitoreo.setLinea(rs.getString("LINEA"));
		reporteSolicitudMonitoreo.setClinica(rs.getString("CLINICA"));
		reporteSolicitudMonitoreo.setCmp(rs.getString("CMP"));
		reporteSolicitudMonitoreo.setMedicoTratantePrescriptor(rs.getString("MEC_TRATANTE_PRESC"));
		reporteSolicitudMonitoreo.setCodigoSCGSolben(rs.getString("COD_SCG_SOLBEN"));
		reporteSolicitudMonitoreo.setEstadoSolicitudEva(rs.getString("ESTADO_SOL_EVA"));
		reporteSolicitudMonitoreo.setFechaAprobSolicitudEva(rs.getDate("FEC_APROB_SOL_EVA"));
		
		reporteSolicitudMonitoreo.setAutorizadorPertinenciaSolicitudEva(rs.getString("AUTOR_PERTINENCIA_SOL_EVA"));
		reporteSolicitudMonitoreo.setNroCartaGarantia(rs.getString("NRO_CARTA_GARANTIA"));
		reporteSolicitudMonitoreo.setFechaIniTratamiento(rs.getDate("FEC_INI_TRAT"));
		reporteSolicitudMonitoreo.setNroEvolucion(rs.getString("NRO_EVOLUCION"));
		reporteSolicitudMonitoreo.setCodigoResponsableMonitoreo(rs.getInt("COD_RESP_MONITOREO"));
		reporteSolicitudMonitoreo.setFechaProgMonitoreo(rs.getDate("FEC_PROG_MONITOREO"));
		reporteSolicitudMonitoreo.setFechaRealMonitoreo(rs.getDate("FEC_REAL_MONITOREO"));
		reporteSolicitudMonitoreo.setEstadoMonitoreo(rs.getString("ESTADO_MONITOREO"));
		reporteSolicitudMonitoreo.setTolerancia(rs.getString("TOLERANCIA"));
		reporteSolicitudMonitoreo.setToxicidad(rs.getString("TOXICIDAD"));
		reporteSolicitudMonitoreo.setRespuestaClinica(rs.getString("RESPUESTA_CLINICA"));
		reporteSolicitudMonitoreo.setAtencionAlertas(rs.getString("ATENCION_ALERTAS"));
		reporteSolicitudMonitoreo.setCantidadUltimoConsumo(rs.getInt("CANT_ULTIMO_CONSUMO"));
		reporteSolicitudMonitoreo.setFechaUltimoConsumo(rs.getDate("FEC_ULTIMO_CONSUMO"));
		reporteSolicitudMonitoreo.setResultadoEvolucion(rs.getString("RESULTADO_EVOLUCION"));
		reporteSolicitudMonitoreo.setFechaInactivacion(rs.getDate("FEC_INACTIVACION"));
		reporteSolicitudMonitoreo.setMotivoInactivacion(rs.getString("MOTIVO_INACTIVACION"));
		reporteSolicitudMonitoreo.setEjecutivoMonitoreo(rs.getString("EJECUTIVO_MONITOREO"));
		reporteSolicitudMonitoreo.setEstadoSeguimiento(rs.getString("ESTADO_SEGUIMIENTO"));
		reporteSolicitudMonitoreo.setFechaRegistroSE(rs.getDate("FECHA_REGISTRO_SE"));
		reporteSolicitudMonitoreo.setCodigoTareaMonitoreo(rs.getString("CODIGO_TAREA_MONITOREO"));
	
		return reporteSolicitudMonitoreo;
	}

}

