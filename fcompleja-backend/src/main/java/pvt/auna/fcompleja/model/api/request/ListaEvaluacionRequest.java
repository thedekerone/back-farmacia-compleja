package pvt.auna.fcompleja.model.api.request;

public class ListaEvaluacionRequest {

    private String numeroSolEvaluacion;
    private String numeroScgSolben;
    private String tipoScgSolben;
    private String estadoScgSolben;
    private String numeroCartaGarantia;
    private String fechaRegistroEvaluacion;
    private String tipoEvaluacion;
    private String estadoEvaluacion;
    private String correoLiderTumor;
    private String correoCmac;
    private String fechaCmac;
    private String auditorPertenencia;
    private String liderTumor;
    private String codSolEvaluacion;
    private String codMac;
    private String codigoPaciente;
    private String codigoDiagnostico;
    private String descripcionCmac;
    private String nombreDiagnostico;
    private String nombrePaciente;
    
	public String getNumeroSolEvaluacion() {
		return numeroSolEvaluacion;
	}
	public void setNumeroSolEvaluacion(String numeroSolEvaluacion) {
		this.numeroSolEvaluacion = numeroSolEvaluacion;
	}
	public String getNumeroScgSolben() {
		return numeroScgSolben;
	}
	public void setNumeroScgSolben(String numeroScgSolben) {
		this.numeroScgSolben = numeroScgSolben;
	}
	public String getTipoScgSolben() {
		return tipoScgSolben;
	}
	public void setTipoScgSolben(String tipoScgSolben) {
		this.tipoScgSolben = tipoScgSolben;
	}
	public String getEstadoScgSolben() {
		return estadoScgSolben;
	}
	public void setEstadoScgSolben(String estadoScgSolben) {
		this.estadoScgSolben = estadoScgSolben;
	}
	public String getNumeroCartaGarantia() {
		return numeroCartaGarantia;
	}
	public void setNumeroCartaGarantia(String numeroCartaGarantia) {
		this.numeroCartaGarantia = numeroCartaGarantia;
	}
	public String getFechaRegistroEvaluacion() {
		return fechaRegistroEvaluacion;
	}
	public void setFechaRegistroEvaluacion(String fechaRegistroEvaluacion) {
		this.fechaRegistroEvaluacion = fechaRegistroEvaluacion;
	}
	public String getTipoEvaluacion() {
		return tipoEvaluacion;
	}
	public void setTipoEvaluacion(String tipoEvaluacion) {
		this.tipoEvaluacion = tipoEvaluacion;
	}
	public String getEstadoEvaluacion() {
		return estadoEvaluacion;
	}
	public void setEstadoEvaluacion(String estadoEvaluacion) {
		this.estadoEvaluacion = estadoEvaluacion;
	}
	public String getCorreoLiderTumor() {
		return correoLiderTumor;
	}
	public void setCorreoLiderTumor(String correoLiderTumor) {
		this.correoLiderTumor = correoLiderTumor;
	}
	public String getCorreoCmac() {
		return correoCmac;
	}
	public void setCorreoCmac(String correoCmac) {
		this.correoCmac = correoCmac;
	}
	public String getFechaCmac() {
		return fechaCmac;
	}
	public void setFechaCmac(String fechaCmac) {
		this.fechaCmac = fechaCmac;
	}
	public String getAuditorPertenencia() {
		return auditorPertenencia;
	}
	public void setAuditorPertenencia(String auditorPertenencia) {
		this.auditorPertenencia = auditorPertenencia;
	}
	public String getLiderTumor() {
		return liderTumor;
	}
	public void setLiderTumor(String liderTumor) {
		this.liderTumor = liderTumor;
	}
	public String getCodSolEvaluacion() {
		return codSolEvaluacion;
	}
	public void setCodSolEvaluacion(String codSolEvaluacion) {
		this.codSolEvaluacion = codSolEvaluacion;
	}
	public String getCodMac() {
		return codMac;
	}
	public void setCodMac(String codMac) {
		this.codMac = codMac;
	}
	public String getCodigoPaciente() {
		return codigoPaciente;
	}
	public void setCodigoPaciente(String codigoPaciente) {
		this.codigoPaciente = codigoPaciente;
	}
	public String getCodigoDiagnostico() {
		return codigoDiagnostico;
	}
	public void setCodigoDiagnostico(String codigoDiagnostico) {
		this.codigoDiagnostico = codigoDiagnostico;
	}
	public String getDescripcionCmac() {
		return descripcionCmac;
	}
	public void setDescripcionCmac(String descripcionCmac) {
		this.descripcionCmac = descripcionCmac;
	}
	public String getNombreDiagnostico() {
		return nombreDiagnostico;
	}
	public void setNombreDiagnostico(String nombreDiagnostico) {
		this.nombreDiagnostico = nombreDiagnostico;
	}
	public String getNombrePaciente() {
		return nombrePaciente;
	}
	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}
	@Override
	public String toString() {
		return "ListaEvaluacionRequest [numeroSolEvaluacion=" + numeroSolEvaluacion + ", numeroScgSolben="
				+ numeroScgSolben + ", tipoScgSolben=" + tipoScgSolben + ", estadoScgSolben=" + estadoScgSolben
				+ ", numeroCartaGarantia=" + numeroCartaGarantia + ", fechaRegistroEvaluacion="
				+ fechaRegistroEvaluacion + ", tipoEvaluacion=" + tipoEvaluacion + ", estadoEvaluacion="
				+ estadoEvaluacion + ", correoLiderTumor=" + correoLiderTumor + ", correoCmac=" + correoCmac
				+ ", fechaCmac=" + fechaCmac + ", auditorPertenencia=" + auditorPertenencia + ", liderTumor="
				+ liderTumor + ", codSolEvaluacion=" + codSolEvaluacion + ", codMac=" + codMac + ", codigoPaciente="
				+ codigoPaciente + ", codigoDiagnostico=" + codigoDiagnostico + ", descripcionCmac=" + descripcionCmac
				+ ", nombreDiagnostico=" + nombreDiagnostico + ", nombrePaciente=" + nombrePaciente + "]";
	}
    

}
