package pvt.auna.fcompleja.model.api.response;

import java.util.List;


public class ListaMonitoreoResponse {

	private List<ListaBandejaMonitoreo> listaBandejaMonitoreo;
	private Integer codigoResultado;
	private String mensajeResultado;
	
	public List<ListaBandejaMonitoreo> getListaBandejaMonitoreo() {
		return listaBandejaMonitoreo;
	}
	public void setListaBandejaMonitoreo(List<ListaBandejaMonitoreo> listaBandejaMonitoreo) {
		this.listaBandejaMonitoreo = listaBandejaMonitoreo;
	}
	public Integer getCodigoResultado() {
		return codigoResultado;
	}
	public void setCodigoResultado(Integer codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	public String getMensajeResultado() {
		return mensajeResultado;
	}
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}
	
	
}
