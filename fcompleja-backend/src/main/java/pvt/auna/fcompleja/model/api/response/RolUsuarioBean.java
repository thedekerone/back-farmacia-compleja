package pvt.auna.fcompleja.model.api.response;



public class RolUsuarioBean {
	private String  codSeguimiento;
	private String  codRolResp;
	private String  descRolResp;
	private String  codUsrResp;
	private String  descUsrResp;
	private String  codRolRespReg;
	private String  descRolRespReg;
	private String  codUsrRespReg;
	private String  descUsrRespReg;
	
	public RolUsuarioBean(String codSeguimiento, String codRolResp, String descRolResp, String codUsrResp,
			String descUsrResp, String codRolRespReg, String descRolRespReg, String codUsrRespReg,
			String descUsrRespReg) {
		super();
		this.codSeguimiento = codSeguimiento;
		this.codRolResp = codRolResp;
		this.descRolResp = descRolResp;
		this.codUsrResp = codUsrResp;
		this.descUsrResp = descUsrResp;
		this.codRolRespReg = codRolRespReg;
		this.descRolRespReg = descRolRespReg;
		this.codUsrRespReg = codUsrRespReg;
		this.descUsrRespReg = descUsrRespReg;
	}

	public RolUsuarioBean() {
		super();
	}

	public String getCodSeguimiento() {
		return codSeguimiento;
	}

	public void setCodSeguimiento(String codSeguimiento) {
		this.codSeguimiento = codSeguimiento;
	}

	public String getCodRolResp() {
		return codRolResp;
	}

	public void setCodRolResp(String codRolResp) {
		this.codRolResp = codRolResp;
	}

	public String getDescRolResp() {
		return descRolResp;
	}

	public void setDescRolResp(String descRolResp) {
		this.descRolResp = descRolResp;
	}

	public String getCodUsrResp() {
		return codUsrResp;
	}

	public void setCodUsrResp(String codUsrResp) {
		this.codUsrResp = codUsrResp;
	}

	public String getDescUsrResp() {
		return descUsrResp;
	}

	public void setDescUsrResp(String descUsrResp) {
		this.descUsrResp = descUsrResp;
	}

	public String getCodRolRespReg() {
		return codRolRespReg;
	}

	public void setCodRolRespReg(String codRolRespReg) {
		this.codRolRespReg = codRolRespReg;
	}

	public String getDescRolRespReg() {
		return descRolRespReg;
	}

	public void setDescRolRespReg(String descRolRespReg) {
		this.descRolRespReg = descRolRespReg;
	}

	public String getCodUsrRespReg() {
		return codUsrRespReg;
	}

	public void setCodUsrRespReg(String codUsrRespReg) {
		this.codUsrRespReg = codUsrRespReg;
	}

	public String getDescUsrRespReg() {
		return descUsrRespReg;
	}

	public void setDescUsrRespReg(String descUsrRespReg) {
		this.descUsrRespReg = descUsrRespReg;
	}
	
	
}

