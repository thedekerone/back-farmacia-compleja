package pvt.auna.fcompleja.model.bean;

import java.sql.Date;

public class HistorialCargaGastosBean {
	
	private Integer CodigoControlGasto;
	private Date fechaInicio;
	private Date fechaFinal;
	//-------------------------------------------
	private String  fechaCarga;
	private Integer  responsableCarga;
	private Integer  estadoCarga;
	private String   desEstadoCarga;
	private Integer  registroTotal;
	private Integer  registroCargado;
	private Integer  registroError;
	private Integer  verLog;
	private Integer  descarga;
	private Integer  codArchivoLog;
	private Integer  codArchivoDescarga;	
	
	private Integer usuarioCreacion;
	private Date fechaCreacion;
	private Integer usuarioModificacion;	
	private Date fechaModificacion;
	
	private String   nombreUsuario;
	private String   horaCarga;
	
	private String archivoNombre;
	private String archivoRuta;
	
	
	
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public String getFechaCarga() {
		return fechaCarga;
	}
	public void setFechaCarga(String fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
	public Integer getResponsableCarga() {
		return responsableCarga;
	}
	public void setResponsableCarga(Integer responsableCarga) {
		this.responsableCarga = responsableCarga;
	}
	public Integer getEstadoCarga() {
		return estadoCarga;
	}
	public void setEstadoCarga(Integer estadoCarga) {
		this.estadoCarga = estadoCarga;
	}
	public Integer getRegistroTotal() {
		return registroTotal;
	}
	public void setRegistroTotal(Integer registroTotal) {
		this.registroTotal = registroTotal;
	}
	public Integer getRegistroCargado() {
		return registroCargado;
	}
	public void setRegistroCargado(Integer registroCargado) {
		this.registroCargado = registroCargado;
	}
	public Integer getRegistroError() {
		return registroError;
	}
	public void setRegistroError(Integer registroError) {
		this.registroError = registroError;
	}
	public Integer getVerLog() {
		return verLog;
	}
	public void setVerLog(Integer verLog) {
		this.verLog = verLog;
	}
	public Integer getDescarga() {
		return descarga;
	}
	public void setDescarga(Integer descarga) {
		this.descarga = descarga;
	}
	public Integer getCodArchivoLog() {
		return codArchivoLog;
	}
	public void setCodArchivoLog(Integer codArchivoLog) {
		this.codArchivoLog = codArchivoLog;
	}
	public Integer getCodArchivoDescarga() {
		return codArchivoDescarga;
	}
	public void setCodArchivoDescarga(Integer codArchivoDescarga) {
		this.codArchivoDescarga = codArchivoDescarga;
	}
	public Integer getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(Integer usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Integer getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setUsuarioModificacion(Integer usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public Integer getCodigoControlGasto() {
		return CodigoControlGasto;
	}
	public void setCodigoControlGasto(Integer codigoControlGasto) {
		CodigoControlGasto = codigoControlGasto;
	}
	public String getDesEstadoCarga() {
		return desEstadoCarga;
	}
	public void setDesEstadoCarga(String desEstadoCarga) {
		this.desEstadoCarga = desEstadoCarga;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getHoraCarga() {
		return horaCarga;
	}
	public void setHoraCarga(String horaCarga) {
		this.horaCarga = horaCarga;
	}
	public String getArchivoNombre() {
		return archivoNombre;
	}
	public void setArchivoNombre(String archivoNombre) {
		this.archivoNombre = archivoNombre;
	}
	public String getArchivoRuta() {
		return archivoRuta;
	}
	public void setArchivoRuta(String archivoRuta) {
		this.archivoRuta = archivoRuta;
	}


	
	
}
