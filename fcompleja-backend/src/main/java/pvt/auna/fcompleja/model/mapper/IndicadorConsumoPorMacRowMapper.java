/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorMacResponse;
import pvt.auna.fcompleja.model.api.response.IndicadoresProcesoResponse;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class IndicadorConsumoPorMacRowMapper implements RowMapper<IndicadorConsumoPorMacResponse>{

	@Override
	public IndicadorConsumoPorMacResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		IndicadorConsumoPorMacResponse indicadorConsumoPorMacResponse = new IndicadorConsumoPorMacResponse();		
		
		indicadorConsumoPorMacResponse.setCodigoMac(rs.getInt("COD_MAC"));
		indicadorConsumoPorMacResponse.setDescripcion(rs.getString("DESCRIPCION"));
		indicadorConsumoPorMacResponse.setGastoTotal(rs.getDouble("GASTO_TOTAL"));
		indicadorConsumoPorMacResponse.setGastoPaciente(rs.getDouble("G_PAC"));
		indicadorConsumoPorMacResponse.setNroPacientes(rs.getInt("NRO_PACIENTES"));
		indicadorConsumoPorMacResponse.setNroNuevos(rs.getInt("NRO_NUEVOS"));
		indicadorConsumoPorMacResponse.setNroContinuadores(rs.getInt("NRO_CONTINUADOR"));
		
		return indicadorConsumoPorMacResponse;
	}

}

