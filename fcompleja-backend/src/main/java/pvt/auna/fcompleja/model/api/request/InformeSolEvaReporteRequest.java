package pvt.auna.fcompleja.model.api.request;

public class InformeSolEvaReporteRequest {

    private Integer codSolEva;
    private String  codAfiliado;
    private String  codDiagnostico;
    //private List<DetSolEvaResponse> detSolEva;
    //private List<LineaTratResponse> lineaTrat;
    //private List<AnalisiConclusionResponse> analisisConclusion;
    //private List<MedicoAuditorResponse> medicoAuditor;


    public Integer getCodSolEva() {
        return codSolEva;
    }

    public void setCodSolEva(Integer codSolEva) {
        this.codSolEva = codSolEva;
    }
	public String getCodAfiliado() {
		return codAfiliado;
	}

	public void setCodAfiliado(String codAfiliado) {
		this.codAfiliado = codAfiliado;
	}

	public String getCodDiagnostico() {
		return codDiagnostico;
	}

	public void setCodDiagnostico(String codDiagnostico) {
		this.codDiagnostico = codDiagnostico;
	}

	@Override
	public String toString() {
		return "InformeSolEvaReporteRequest [codSolEva=" + codSolEva + ", codAfiliado=" + codAfiliado
				+ ", codDiagnostico=" + codDiagnostico + "]";
	}
}
