package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.ChecklistRequisitoResponse;

public class ChecklistRequisitoMapper implements RowMapper<ChecklistRequisitoResponse>{

	@Override
	public ChecklistRequisitoResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		ChecklistRequisitoResponse checklist = new ChecklistRequisitoResponse();
		
		checklist.setCodArchivo(rs.getLong("COD_ARCHIVO"));
		checklist.setCodSolEva(rs.getLong("COD_SOL_EVA"));
		return checklist;
	}
}
