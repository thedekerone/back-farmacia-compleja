package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.DetalleReportBean;

public class DetalleEvaluacionRepoRowMapper implements RowMapper<DetalleReportBean>{

	@Override
	public DetalleReportBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		DetalleReportBean detalle = new DetalleReportBean();
		
		detalle.setCodigoEvaluacion(rs.getInt("COD_SOL_EVA"));
		detalle.setNombres(rs.getString("Nombre_Apellido"));
		detalle.setSexo(rs.getString("sexo"));
		detalle.setPlan(rs.getString("des_plan"));
		detalle.setCodigoDiagnostico(rs.getString("cod_diagnostico"));
		detalle.setDescripcionDiagnostico(rs.getString("DescripcionDiagnostico"));
		detalle.setDescripcionMedicamento(rs.getString("DESCRIPCION"));
		detalle.setFecha(rs.getString("FEC_SOL_PRE"));
		detalle.setCodigoScgSolben(rs.getString("COD_SCG_SOLBEN"));
		detalle.setCodigoAfiliadoPaciente(rs.getString("cod_afi_paciente"));
		detalle.setEdadPaciente(rs.getInt("edad_paciente"));
		detalle.setMedicoTratante(rs.getString("medico_tratante_prescriptor"));
		detalle.setEstadioClinico(rs.getString("EstadioClinico"));
		detalle.setNumeroLinea(rs.getInt("NRO_LINEA_TRA"));
		
		return detalle;
		
	}

}
