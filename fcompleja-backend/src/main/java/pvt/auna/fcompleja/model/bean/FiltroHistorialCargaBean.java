package pvt.auna.fcompleja.model.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class FiltroHistorialCargaBean {
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaInicio;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")	
	private Date fechaFinal;
	
	private Integer index;
	private Integer size;
	private Integer CodigoControlGasto;
	
	
	
	
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getCodigoControlGasto() {
		return CodigoControlGasto;
	}
	public void setCodigoControlGasto(Integer codigoControlGasto) {
		CodigoControlGasto = codigoControlGasto;
	}

}
