package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.ListaBandeja;

public class ListaEvaluacionRowMapper implements RowMapper<ListaBandeja>{

	@Override
	public ListaBandeja mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		ListaBandeja listaBandeja = new ListaBandeja();
		
		listaBandeja.setNumeroSolEvaluacion(rs.getString("COD_DESC_SOL_EVA"));
		listaBandeja.setCodigoSolPre(rs.getLong("COD_SOL_PRE"));
		listaBandeja.setNumeroScgSolben(rs.getString("COD_SCG_SOLBEN"));
		listaBandeja.setTipoScgSolben(rs.getString("TIPO_SCG_SOLBEN"));
		listaBandeja.setEstadoScgSolben(rs.getString("ESTADO_SCG"));
		listaBandeja.setDescEstadoSCGSolben((rs.getNString("ESTADOSGCSOLBEN") != null)? rs.getNString("ESTADOSGCSOLBEN"): "");
		listaBandeja.setNumeroCartaGarantia((rs.getString("NRO_CG") != null) ? rs.getString("NRO_CG") : "");
		listaBandeja.setFechaRegistroEvaluacion(rs.getTimestamp("FECHAEVALUACION"));
		listaBandeja.setTipoEvaluacion((rs.getString("TIPOEVALUACION") != null) ? rs.getString("TIPOEVALUACION") : "");
		listaBandeja.setEstadoEvaluacion(rs.getString("ESTADOEVALUACION"));
		listaBandeja.setCorreoLiderTumor((rs.getString("CORREOLIDERTUMOR") != null) ? rs.getString("CORREOLIDERTUMOR") : "");
		listaBandeja.setCorreoCmac((rs.getString("CORREOCMAC") != null) ? rs.getString("CORREOCMAC") : "");
		listaBandeja.setFechaCmac(rs.getDate("FECHACMAC"));
		listaBandeja.setCodRolRespEvaluacion((rs.getString("ROL_RESPONSABLE") != null) ? rs.getInt("ROL_RESPONSABLE") : null);
		listaBandeja.setRolResponsableEvaluacion((rs.getString("ROL_RESPONSABLE") != null) ? rs.getString("ROL_RESPONSABLE") : "");
		listaBandeja.setCodAutorPerte((rs.getObject("AUTORIZADOR_PERTENENCIA") != null) ? rs.getInt("AUTORIZADOR_PERTENENCIA") : null);
		listaBandeja.setAuditorPertenencia("");
		listaBandeja.setCodLiderTumor((rs.getObject("COD_LIDER_TUMOR") != null) ? rs.getInt("COD_LIDER_TUMOR") : null);
		listaBandeja.setLiderTumor((rs.getObject("LIDER_TUMOR") != null) ? rs.getString("LIDER_TUMOR") : "");
		listaBandeja.setCodSolEvaluacion(rs.getString("COD_SOL_EVA"));
		listaBandeja.setCodMac(rs.getString("COD_MAC"));
		listaBandeja.setCodigoPaciente(rs.getString("COD_AFI_PACIENTE"));
		listaBandeja.setNombrePaciente("");
		listaBandeja.setCodigoClinica(rs.getString("COD_CLINICA"));
		listaBandeja.setDescClinica("");
		listaBandeja.setCodigoDiagnostico(rs.getString("COD_DIAGNOSTICO"));
		listaBandeja.setNombreDiagnostico("");
		listaBandeja.setDescripcionCmac(rs.getString("DESCRIPCION"));
		listaBandeja.setCodigoP(rs.getString("CODIGOP"));
		listaBandeja.setCodigoEstadoEvaluacion(rs.getString("P_ESTADO_SOL_EVA"));
		
		listaBandeja.setEstadoCorreoEnvCmac(rs.getInt("ESTADO_CORREO_ENV_CMAC"));
		listaBandeja.setEstadoCorreoEnvLiderTumor(rs.getInt("ESTADO_CORREO_ENV_LIDER_TUMOR"));
		listaBandeja.setCodigoEnvioEnvMac(rs.getLong("CODIGO_ENVIO_ENV_MAC"));
		listaBandeja.setCodigoEnvioEnvLiderTumor(rs.getLong("CODIGO_ENVIO_ENV_LIDER_TUMOR"));
		listaBandeja.setCodigoEnvioEnvAlerMonit(rs.getLong("CODIGO_ENVIO_ENV_ALER_MONIT"));
		listaBandeja.setCodigoEnvioEnvAlerMonit(rs.getLong("CODIGO_ENVIO_ENV_ALER_MONIT"));
		
		listaBandeja.setpTipoEvaluacion(rs.getInt("P_TIPO_EVA"));
		listaBandeja.setTipoDoc(rs.getString("TIPO_DOC"));
		listaBandeja.setNumDoc(rs.getString("NUM_DOC"));

		return listaBandeja;
	}

}
