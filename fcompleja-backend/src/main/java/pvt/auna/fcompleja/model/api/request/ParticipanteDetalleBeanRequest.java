/**
 * 
 */
package pvt.auna.fcompleja.model.api.request;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ParticipanteDetalleBeanRequest {
	
	private Integer codParticipante;
	private String  codGrupoDiagnostico;
	private Integer pRangoEdad;
	
	

	/**
	 * 
	 */
	public ParticipanteDetalleBeanRequest() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * @param codParticipante
	 * @param codGrupoDiagnostico
	 * @param pRangoEdad
	 */
	public ParticipanteDetalleBeanRequest(Integer codParticipante, String codGrupoDiagnostico, Integer pRangoEdad) {
		super();
		this.codParticipante = codParticipante;
		this.codGrupoDiagnostico = codGrupoDiagnostico;
		this.pRangoEdad = pRangoEdad;
	}



	/**
	 * @return the codParticipante
	 */
	public Integer getCodParticipante() {
		return codParticipante;
	}



	/**
	 * @param codParticipante the codParticipante to set
	 */
	public void setCodParticipante(Integer codParticipante) {
		this.codParticipante = codParticipante;
	}



	/**
	 * @return the codGrupoDiagnostico
	 */
	public String getCodGrupoDiagnostico() {
		return codGrupoDiagnostico;
	}



	/**
	 * @param codGrupoDiagnostico the codGrupoDiagnostico to set
	 */
	public void setCodGrupoDiagnostico(String codGrupoDiagnostico) {
		this.codGrupoDiagnostico = codGrupoDiagnostico;
	}



	/**
	 * @return the pRangoEdad
	 */
	public Integer getpRangoEdad() {
		return pRangoEdad;
	}



	/**
	 * @param pRangoEdad the pRangoEdad to set
	 */
	public void setpRangoEdad(Integer pRangoEdad) {
		this.pRangoEdad = pRangoEdad;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParticipanteDetalleBeanRequest [codParticipante=" + codParticipante + ", codGrupoDiagnostico="
				+ codGrupoDiagnostico + ", pRangoEdad=" + pRangoEdad + ", toString()=" + super.toString() + "]";
	}
	
	

}
