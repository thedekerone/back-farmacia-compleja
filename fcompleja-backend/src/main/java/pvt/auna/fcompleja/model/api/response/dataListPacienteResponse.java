package pvt.auna.fcompleja.model.api.response;

public class dataListPacienteResponse {

	private Integer fecnac;
	private String codafir;
	private String tipafi;
	private String espaci;
	private String numdoc;
	private String edaafi;
	private String fecbaj;
	private String fecafi;
	private String nombr1;
	private String apepat;
	private String nombr2;
	private String tipdoc;
	private String apemat;
	private String fecren;
	private String rn;

	public Integer getFecnac() {
		return fecnac;
	}

	public void setFecnac(Integer fecnac) {
		this.fecnac = fecnac;
	}

	public String getCodafir() {
		return codafir;
	}

	public void setCodafir(String codafir) {
		this.codafir = codafir;
	}

	public String getTipafi() {
		return tipafi;
	}

	public void setTipafi(String tipafi) {
		this.tipafi = tipafi;
	}

	public String getEspaci() {
		return espaci;
	}

	public void setEspaci(String espaci) {
		this.espaci = espaci;
	}

	public String getNumdoc() {
		return numdoc;
	}

	public void setNumdoc(String numdoc) {
		this.numdoc = numdoc;
	}

	public String getEdaafi() {
		return edaafi;
	}

	public void setEdaafi(String edaafi) {
		this.edaafi = edaafi;
	}

	public String getFecbaj() {
		return fecbaj;
	}

	public void setFecbaj(String fecbaj) {
		this.fecbaj = fecbaj;
	}

	public String getFecafi() {
		return fecafi;
	}

	public void setFecafi(String fecafi) {
		this.fecafi = fecafi;
	}

	public String getNombr1() {
		return nombr1;
	}

	public void setNombr1(String nombr1) {
		this.nombr1 = nombr1;
	}

	public String getApepat() {
		return apepat;
	}

	public void setApepat(String apepat) {
		this.apepat = apepat;
	}

	public String getNombr2() {
		return nombr2;
	}

	public void setNombr2(String nombr2) {
		this.nombr2 = nombr2;
	}

	public String getTipdoc() {
		return tipdoc;
	}

	public void setTipdoc(String tipdoc) {
		this.tipdoc = tipdoc;
	}

	public String getApemat() {
		return apemat;
	}

	public void setApemat(String apemat) {
		this.apemat = apemat;
	}

	public String getFecren() {
		return fecren;
	}

	public void setFecren(String fecren) {
		this.fecren = fecren;
	}

	public String getRn() {
		return rn;
	}

	public void setRn(String rn) {
		this.rn = rn;
	}

	@Override
	public String toString() {
		return "dataListPacienteResponse [fecnac=" + fecnac + ", codafir=" + codafir + ", tipafi=" + tipafi
				+ ", espaci=" + espaci + ", numdoc=" + numdoc + ", edaafi=" + edaafi + ", fecbaj=" + fecbaj
				+ ", fecafi=" + fecafi + ", nombr1=" + nombr1 + ", apepat=" + apepat + ", nombr2=" + nombr2
				+ ", tipdoc=" + tipdoc + ", apemat=" + apemat + ", fecren=" + fecren + ", rn=" + rn + "]";
	}

}
