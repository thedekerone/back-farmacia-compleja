package pvt.auna.fcompleja.model.api;

public class AccesoMenuResponse {
	
	private Integer codMenu;
	private String nombreCorto;
	private String nombreLargo;
	public Integer getCodMenu() {
		return codMenu;
	}
	public void setCodMenu(Integer codMenu) {
		this.codMenu = codMenu;
	}
	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	public String getNombreLargo() {
		return nombreLargo;
	}
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}
}
