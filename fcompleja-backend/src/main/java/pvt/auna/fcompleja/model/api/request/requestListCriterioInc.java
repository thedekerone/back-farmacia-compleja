package pvt.auna.fcompleja.model.api.request;

public class requestListCriterioInc {
	
	private Number codChkListIndi;

	public requestListCriterioInc() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Number getCodChkListIndi() {
		return codChkListIndi;
	}

	public void setCodChkListIndi(Number codChkListIndi) {
		this.codChkListIndi = codChkListIndi;
	}
	
	@Override
	public String toString() {
		return "requestListCriterioInc [codChkListIndi=" + codChkListIndi + "]";
	}

}
