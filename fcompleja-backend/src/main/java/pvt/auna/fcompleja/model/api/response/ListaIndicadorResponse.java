package pvt.auna.fcompleja.model.api.response;

import java.util.List;

public class ListaIndicadorResponse {

	private List<Indicador> listaIndicador;
	private List<CriterioInclusion> listaCriterioInclusion;
	private List<CriterioExclusion> listaCriterioExclusion;
	private Long codigoResultado;
	private String mensajeResultado;
	
	public List<Indicador> getListaIndicador() {
		return listaIndicador;
	}
	public void setListaIndicador(List<Indicador> listaIndicador) {
		this.listaIndicador = listaIndicador;
	}
	public List<CriterioInclusion> getListaCriterioInclusion() {
		return listaCriterioInclusion;
	}
	public void setListaCriterioInclusion(List<CriterioInclusion> listaCriterioInclusion) {
		this.listaCriterioInclusion = listaCriterioInclusion;
	}
	public List<CriterioExclusion> getListaCriterioExclusion() {
		return listaCriterioExclusion;
	}
	public void setListaCriterioExclusion(List<CriterioExclusion> listaCriterioExclusion) {
		this.listaCriterioExclusion = listaCriterioExclusion;
	}
	public Long getCodigoResultado() {
		return codigoResultado;
	}
	public void setCodigoResultado(Long codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	public String getMensajeResultado() {
		return mensajeResultado;
	}
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	} 
	
	
}
