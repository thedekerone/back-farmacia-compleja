package pvt.auna.fcompleja.model.api.request.evaluacion;

public class AntecedenteOncoPersDet {
	
	Boolean aplica;
	String diagnostco;
	String fecha;
	
	
	public Boolean getAplica() {
		return aplica;
	}
	public void setAplica(Boolean aplica) {
		this.aplica = aplica;
	}
	public String getDiagnostco() {
		return diagnostco;
	}
	public void setDiagnostco(String diagnostco) {
		this.diagnostco = diagnostco;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	

}
