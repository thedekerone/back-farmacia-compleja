package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import pvt.auna.fcompleja.model.bean.PersonaBean;

public class PersonaMapper extends PersonaBean implements SQLData  {
	
    private static final long serialVersionUID = 1L;

    public static final String typeName = "ONTPFC.TO_PERSONA";
    public static final String arrayTypeName = "ONTPFC.TT_PERSONA";

	@Override
	public String getSQLTypeName() throws SQLException {
		return typeName;
	}

	@Override
	public void readSQL(SQLInput sqlInput, String string) throws SQLException {
		setTN_COD_PERSONA(sqlInput.readInt());
		setTV_NOMBRE(sqlInput.readString());
		setTN_EDAD(sqlInput.readInt());		
	}

	@Override
	public void writeSQL(SQLOutput sqlOutput) throws SQLException {
		sqlOutput.writeInt(getTN_COD_PERSONA());
        sqlOutput.writeString(getTV_NOMBRE());
        sqlOutput.writeInt(getTN_EDAD());
		
	}
	
}
