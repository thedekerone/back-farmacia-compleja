package pvt.auna.fcompleja.model.api;

public class ActaMac {
private String solEvaluacion;
private String paciente;
private String diagnostico;
private String codMedicamento;
private String medicamentoSolicitado;
private String resultadoEvaluacion;
private String observaciones;


	public String getSolEvaluacion() {
		return solEvaluacion;
	}
	
	public void setSolEvaluacion(String solEvaluacion) {
		this.solEvaluacion = solEvaluacion;
	}
	
	public String getPaciente() {
		return paciente;
	}
	
	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}
	
	public String getDiagnostico() {
		return diagnostico;
	}
	
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	
	public String getCodMedicamento() {
		return codMedicamento;
	}
	
	public void setCodMedicamento(String codMedicamento) {
		this.codMedicamento = codMedicamento;
	}
	
	public String getMedicamentoSolicitado() {
		return medicamentoSolicitado;
	}
	
	public void setMedicamentoSolicitado(String medicamentoSolicitado) {
		this.medicamentoSolicitado = medicamentoSolicitado;
	}
	
	public String getResultadoEvaluacion() {
		return resultadoEvaluacion;
	}
	
	public void setResultadoEvaluacion(String resultadoEvaluacion) {
		this.resultadoEvaluacion = resultadoEvaluacion;
	}
	
	public String getObservaciones() {
		return observaciones;
	}
	
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

}
