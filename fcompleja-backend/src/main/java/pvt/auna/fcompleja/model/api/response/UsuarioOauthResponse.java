package pvt.auna.fcompleja.model.api.response;

import pvt.auna.fcompleja.model.api.AudiOauthResponse;

public class UsuarioOauthResponse {

    private Integer codUsuario;

    private String usuario;

    private AudiOauthResponse audiResponse;


    public Integer getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(Integer codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public AudiOauthResponse getAudiResponse() {
        return audiResponse;
    }

    public void setAudiResponse(AudiOauthResponse audiResponse) {
        this.audiResponse = audiResponse;
    }

    @Override
    public String toString() {
        return "UsuarioOauthResponse{" +
                "codUsuario=" + codUsuario +
                ", usuario='" + usuario + '\'' +
                '}';
    }
}
