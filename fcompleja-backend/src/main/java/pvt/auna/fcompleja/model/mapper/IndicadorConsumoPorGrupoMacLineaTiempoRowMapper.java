/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorGrupoMacLineaResponse;
import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorGrupoMacLineaTiempoResponse;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class IndicadorConsumoPorGrupoMacLineaTiempoRowMapper implements RowMapper<IndicadorConsumoPorGrupoMacLineaTiempoResponse>{

	@Override
	public IndicadorConsumoPorGrupoMacLineaTiempoResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		IndicadorConsumoPorGrupoMacLineaTiempoResponse indicadorConsumoPorGrupoMacLineaTiempoResponse = new IndicadorConsumoPorGrupoMacLineaTiempoResponse();		
		
		indicadorConsumoPorGrupoMacLineaTiempoResponse.setLineaTratamiento(rs.getInt("LINEA_TRATAMIENTO"));
		indicadorConsumoPorGrupoMacLineaTiempoResponse.setCodigoGrupoDiagnostico(rs.getInt("COD_GRP_DIAG"));
		indicadorConsumoPorGrupoMacLineaTiempoResponse.setCodigoMac(rs.getInt("COD_MAC"));
		indicadorConsumoPorGrupoMacLineaTiempoResponse.setDescripcion(rs.getString("DESCRIPCION"));
		indicadorConsumoPorGrupoMacLineaTiempoResponse.setGastoTotal(rs.getDouble("GASTO_TOTAL"));
		indicadorConsumoPorGrupoMacLineaTiempoResponse.setGastoPaciente(rs.getDouble("G_PAC"));
		indicadorConsumoPorGrupoMacLineaTiempoResponse.setNroPacientes(rs.getInt("NRO_PACIENTES"));
		indicadorConsumoPorGrupoMacLineaTiempoResponse.setNroNuevos(rs.getInt("NRO_NUEVOS"));
		indicadorConsumoPorGrupoMacLineaTiempoResponse.setNroContinuadores(rs.getInt("NRO_CONTINUADOR"));
		indicadorConsumoPorGrupoMacLineaTiempoResponse.setTiempoUso(rs.getString("TIEMPO_USO"));
		
		return indicadorConsumoPorGrupoMacLineaTiempoResponse;
	}

}