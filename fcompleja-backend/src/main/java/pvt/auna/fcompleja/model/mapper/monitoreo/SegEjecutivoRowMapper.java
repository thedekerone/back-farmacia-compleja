package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.monitoreo.SegEjecutivoResponse;

public class SegEjecutivoRowMapper implements RowMapper<SegEjecutivoResponse> {

	@Override
	public SegEjecutivoResponse mapRow(ResultSet rs, int rowNum) throws SQLException {

		SegEjecutivoResponse response = new SegEjecutivoResponse();

		response.setCodSegEjecutivo(rs.getLong("COD_SEG_EJECUTIVO"));
		response.setCodMonitoreo(rs.getLong("COD_MONITOREO"));
		response.setCodEjecutivoMonitoreo(rs.getLong("COD_EJECUTIVO_MONITOREO"));
		response.setNomEjecutivoMonitoreo(rs.getString("NOM_EJECUTIVO_MONITOREO"));
		response.setFechaRegistro(rs.getTimestamp("FECHA_REGISTRO"));
		response.setpEstadoSeguimiento(rs.getInt("P_ESTADO_SEGUIMIENTO"));
		response.setDescEstadoSeguimiento(rs.getString("DESC_ESTADO_SEGUIMIENTO"));
		response.setDetalleEvento(rs.getString("DETALLE_EVENTO"));
		response.setVistoRespMonitoreo(rs.getInt("VISTO_RESP_MONITOREO"));

		return response;
	}

}
