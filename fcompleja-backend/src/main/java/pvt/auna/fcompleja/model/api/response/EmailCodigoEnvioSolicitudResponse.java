package pvt.auna.fcompleja.model.api.response;

import pvt.auna.fcompleja.model.bean.CodigoEnvioSolicitudBean;;

public class EmailCodigoEnvioSolicitudResponse {
	
	private CodigoEnvioSolicitudBean objCodigoEnvioSolicitudBean;
	private Integer codigo;
	private String mensaje;
	
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public CodigoEnvioSolicitudBean getObjCodigoEnvioSolicitudBean() {
		return objCodigoEnvioSolicitudBean;
	}
	public void setObjCodigoEnvioSolicitudBean(CodigoEnvioSolicitudBean objCodigoEnvioSolicitudBean) {
		this.objCodigoEnvioSolicitudBean = objCodigoEnvioSolicitudBean;
	}
	
	@Override
	public String toString() {
		return "EmailCodigoEnvioSolicitudResponse [objCodigoEnvioSolicitudBean=" + objCodigoEnvioSolicitudBean
				+ ", codigo=" + codigo + ", mensaje=" + mensaje + "]";
	}
	
	
	
	

}
