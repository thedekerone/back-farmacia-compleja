package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.LinTrataReportDetalleBean;

public class LineaMetastasisRowMapper implements RowMapper<LinTrataReportDetalleBean>{

	@Override
	public LinTrataReportDetalleBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		LinTrataReportDetalleBean lineaMetastasisBean = new LinTrataReportDetalleBean();
		
		lineaMetastasisBean.setLineaMetastasis(rs.getString("linea_metastasis"));
		lineaMetastasisBean.setLineaMetastasis(rs.getString("lugar_metastasis"));
		
		return lineaMetastasisBean;
	}

}
