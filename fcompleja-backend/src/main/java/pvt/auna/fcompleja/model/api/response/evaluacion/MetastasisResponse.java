package pvt.auna.fcompleja.model.api.response.evaluacion;

public class MetastasisResponse {
	private Integer codMetastasis;
	private Integer	lineaMetastasis;
	private String descripcionLineaMetastasis;
	private Integer lugarMetastasis;
	private String descripcionLugarMetastasis;
	private String estado;
	
	public Integer getCodMetastasis() {
		return codMetastasis;
	}
	public void setCodMetastasis(Integer codMetastasis) {
		this.codMetastasis = codMetastasis;
	}
	public Integer getLineaMetastasis() {
		return lineaMetastasis;
	}
	public void setLineaMetastasis(Integer lineaMetastasis) {
		this.lineaMetastasis = lineaMetastasis;
	}
	public String getDescripcionLineaMetastasis() {
		return descripcionLineaMetastasis;
	}
	public void setDescripcionLineaMetastasis(String descripcionLineaMetastasis) {
		this.descripcionLineaMetastasis = descripcionLineaMetastasis;
	}
	public Integer getLugarMetastasis() {
		return lugarMetastasis;
	}
	public void setLugarMetastasis(Integer lugarMetastasis) {
		this.lugarMetastasis = lugarMetastasis;
	}
	public String getDescripcionLugarMetastasis() {
		return descripcionLugarMetastasis;
	}
	public void setDescripcionLugarMetastasis(String descripcionLugarMetastasis) {
		this.descripcionLugarMetastasis = descripcionLugarMetastasis;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	@Override
	public String toString() {
		return "MetastasisResponse [codMetastasis=" + codMetastasis + ", lineaMetastasis=" + lineaMetastasis
				+ ", descripcionLineaMetastasis=" + descripcionLineaMetastasis + ", lugarMetastasis=" + lugarMetastasis
				+ ", descripcionLugarMetastasis=" + descripcionLugarMetastasis + ", estado=" + estado + "]";
	}
}
