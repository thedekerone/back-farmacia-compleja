package pvt.auna.fcompleja.model.api.response;

public class RptaBasalMarcadorResponse {

    private String codConfigMarca;
    private String codConfigMarcaLargo ;
    private String codExamenMed ;
    private String codExamenMedLargo;
    private String descripcion ;
    private String rango ;
    private String unidadMedida;
    private String nombreTipoExMed ;
    private String nombreTipoIngrRes ;
    private String nombrePosibleValor;
    private String grupoDiagnostico;

    public String getCodConfigMarca() {
        return codConfigMarca;
    }

    public void setCodConfigMarca(String codConfigMarca) {
        this.codConfigMarca = codConfigMarca;
    }

    public String getCodConfigMarcaLargo() {
        return codConfigMarcaLargo;
    }

    public void setCodConfigMarcaLargo(String codConfigMarcaLargo) {
        this.codConfigMarcaLargo = codConfigMarcaLargo;
    }

    public String getCodExamenMed() {
        return codExamenMed;
    }

    public void setCodExamenMed(String codExamenMed) {
        this.codExamenMed = codExamenMed;
    }

    public String getCodExamenMedLargo() {
        return codExamenMedLargo;
    }

    public void setCodExamenMedLargo(String codExamenMedLargo) {
        this.codExamenMedLargo = codExamenMedLargo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRango() {
        return rango;
    }

    public void setRango(String rango) {
        this.rango = rango;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getNombreTipoExMed() {
        return nombreTipoExMed;
    }

    public void setNombreTipoExMed(String nombreTipoExMed) {
        this.nombreTipoExMed = nombreTipoExMed;
    }

    public String getNombreTipoIngrRes() {
        return nombreTipoIngrRes;
    }

    public void setNombreTipoIngrRes(String nombreTipoIngrRes) {
        this.nombreTipoIngrRes = nombreTipoIngrRes;
    }

    public String getNombrePosibleValor() {
        return nombrePosibleValor;
    }

    public void setNombrePosibleValor(String nombrePosibleValor) {
        this.nombrePosibleValor = nombrePosibleValor;
    }

    public String getGrupoDiagnostico() {
        return grupoDiagnostico;
    }

    public void setGrupoDiagnostico(String grupoDiagnostico) {
        this.grupoDiagnostico = grupoDiagnostico;
    }
}
