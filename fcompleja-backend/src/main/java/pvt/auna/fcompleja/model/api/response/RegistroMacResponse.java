package pvt.auna.fcompleja.model.api.response;

import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.bean.MACBean;

public class RegistroMacResponse {

	private AudiResponse audiResponse;
	private MACBean mac;

	public AudiResponse getAudiResponse() {
		return audiResponse;
	}

	public void setAudiResponse(AudiResponse audiResponse) {
		this.audiResponse = audiResponse;
	}

	public MACBean getMac() {
		return mac;
	}

	public void setMac(MACBean mac) {
		this.mac = mac;
	}

}
