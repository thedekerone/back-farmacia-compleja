package pvt.auna.fcompleja.model.api.response.seguridad;

import java.util.List;

public class AccesoMenuResponse {
	
	private Integer codMenu;
	private String nombreCorto;
	private String nombreLargo;
	private String url;
	private String rutaImg;
	private List<AccesoOpcionResponse> opcionResponse;
	
	public Integer getCodMenu() {
		return codMenu;
	}
	public void setCodMenu(Integer codMenu) {
		this.codMenu = codMenu;
	}
	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	public String getNombreLargo() {
		return nombreLargo;
	}
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getRutaImg() {
		return rutaImg;
	}
	public void setRutaImg(String rutaImg) {
		this.rutaImg = rutaImg;
	}
	public List<AccesoOpcionResponse> getOpcionResponse() {
		return opcionResponse;
	}
	public void setOpcionResponse(List<AccesoOpcionResponse> opcionResponse) {
		this.opcionResponse = opcionResponse;
	}
}
