package pvt.auna.fcompleja.model.api.response.evaluacion;

import pvt.auna.fcompleja.model.api.ListFiltroRolResponse;

import java.sql.Date;
import java.util.ArrayList;

public class ListaEvaluacionXFecha {

	private String codActa;
	private String codigoGrabado;
	private String horaCmac;

	private Integer codProgramacionCmac;
	private Integer codActaFtp;
	private Date fecReunion;
	private String reporteActaEscaneada;

	ArrayList<ProgramacionCmacDetResponse> ListaBandeja;
	ArrayList<ListFiltroRolResponse> ListFiltroRolResponse;

	public String getCodActa() {
		return codActa;
	}

	public void setCodActa(String codActa) {
		this.codActa = codActa;
	}

	public String getCodigoGrabado() {
		return codigoGrabado;
	}

	public void setCodigoGrabado(String codigoGrabado) {
		this.codigoGrabado = codigoGrabado;
	}

	public String getHoraCmac() {
		return horaCmac;
	}

	public void setHoraCmac(String horaCmac) {
		this.horaCmac = horaCmac;
	}

	public ArrayList<ProgramacionCmacDetResponse> getListaBandeja() {
		return ListaBandeja;
	}

	public void setListaBandeja(ArrayList<ProgramacionCmacDetResponse> listaBandeja) {
		ListaBandeja = listaBandeja;
	}

	public ArrayList<ListFiltroRolResponse> getListFiltroRolResponse() {
		return ListFiltroRolResponse;
	}

	public void setListFiltroRolResponse(ArrayList<ListFiltroRolResponse> listFiltroRolResponse) {
		ListFiltroRolResponse = listFiltroRolResponse;
	}

	public Integer getCodProgramacionCmac() {
		return codProgramacionCmac;
	}

	public void setCodProgramacionCmac(Integer codProgramacionCmac) {
		this.codProgramacionCmac = codProgramacionCmac;
	}

	public Integer getCodActaFtp() {
		return codActaFtp;
	}

	public void setCodActaFtp(Integer codActaFtp) {
		this.codActaFtp = codActaFtp;
	}

	public Date getFecReunion() {
		return fecReunion;
	}

	public void setFecReunion(Date fecReunion) {
		this.fecReunion = fecReunion;
	}

	public String getReporteActaEscaneada() {
		return reporteActaEscaneada;
	}

	public void setReporteActaEscaneada(String reporteActaEscaneada) {
		this.reporteActaEscaneada = reporteActaEscaneada;
	}

	@Override
	public String toString() {
		return "ListaEvaluacionXFecha [codActa=" + codActa + ", codigoGrabado=" + codigoGrabado + ", horaCmac="
				+ horaCmac + ", codProgramacionCmac=" + codProgramacionCmac + ", codActaFtp=" + codActaFtp
				+ ", fecReunion=" + fecReunion + ", reporteActaEscaneada=" + reporteActaEscaneada + ", ListaBandeja="
				+ ListaBandeja + ", ListFiltroRolResponse=" + ListFiltroRolResponse + "]";
	}

	

}
