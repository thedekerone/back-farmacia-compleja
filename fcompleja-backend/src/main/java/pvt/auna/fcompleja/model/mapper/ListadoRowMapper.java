package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.ListSolPreeliminarResponse;

public class ListadoRowMapper implements RowMapper<ListSolPreeliminarResponse>{

	@Override
	public ListSolPreeliminarResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		ListSolPreeliminarResponse ListFiltroBean = new ListSolPreeliminarResponse();		
		
		ListFiltroBean.setCodPre(rs.getLong("COD_SOL_PRE"));
		ListFiltroBean.setFechaPre(rs.getDate("fecha"));
		ListFiltroBean.setHoraPre(rs.getString("hora"));
		ListFiltroBean.setCodScgSolben(rs.getString("COD_SCG_SOLBEN"));
		ListFiltroBean.setFechaScgSolben(rs.getDate("FEC_SCG_SOLBEN"));
		ListFiltroBean.setHoraScgSolben(rs.getString("HORA_SCG_SOLBEN"));
		ListFiltroBean.setTipoScgSolben(rs.getString("TIPO_SCG_SOLBEN"));
		ListFiltroBean.setEstadoSol(rs.getString("ESTADO_SOL_PRE"));
		ListFiltroBean.setMedicoTratante(rs.getString("MEDICO_TRATANTE_PRESCRIPTOR"));
		ListFiltroBean.setCodAfiliado(rs.getString("COD_PACIENTE"));
		ListFiltroBean.setCodClinica(rs.getString("COD_CLINICA"));
		ListFiltroBean.setAutorizador(rs.getString("TX_DATO_ADIC1"));
		ListFiltroBean.setTipdoc(rs.getString("TIPO_DOC"));
		ListFiltroBean.setNumdoc(rs.getString("NUM_DOC"));
		
		return ListFiltroBean;
	}

}
