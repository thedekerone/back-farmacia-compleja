package pvt.auna.fcompleja.model.api.response;

import java.util.List;

import pvt.auna.fcompleja.model.api.AudiResponse;

public class Response {

	public Response() {
		super();
	}

	private AudiResponse audiResponse;

	private List<RolUsuarioBean> dataList;



	public Response(AudiResponse audiResponse, List<RolUsuarioBean> dataList) {
		super();
		this.audiResponse = audiResponse;
		this.dataList = dataList;
	}

	public List<RolUsuarioBean> getDataList() {
		return dataList;
	}

	public void setDataList(List<RolUsuarioBean> dataList) {
		this.dataList = dataList;
	}

	public AudiResponse getAudiResponse() {
		return audiResponse;
	}

	public void setAudiResponse(AudiResponse audiResponse) {
		this.audiResponse = audiResponse;
	}




	@Override
	public String toString() {
		return "Response [audiResponse=" + audiResponse + ", dataList=" + dataList + "]";
	}

}
