package pvt.auna.fcompleja.model.api.response;

public class SolbenWsResponse {
	private String cod_scg_solben;
	private String cod_sol_eval;
	private Integer cod_rpta;
	private String msg_rpta;
	private String txt_dato_adic1;
	private String txt_dato_adic2;
	private String txt_dato_adic3;
	
	public String getCod_scg_solben() {
		return cod_scg_solben;
	}
	public void setCod_scg_solben(String cod_scg_solben) {
		this.cod_scg_solben = cod_scg_solben;
	}
	public String getCod_sol_eval() {
		return cod_sol_eval;
	}
	public void setCod_sol_eval(String cod_sol_eval) {
		this.cod_sol_eval = cod_sol_eval;
	}
	public Integer getCod_rpta() {
		return cod_rpta;
	}
	public void setCod_rpta(Integer cod_rpta) {
		this.cod_rpta = cod_rpta;
	}
	public String getMsg_rpta() {
		return msg_rpta;
	}
	public void setMsg_rpta(String msg_rpta) {
		this.msg_rpta = msg_rpta;
	}
	public String getTxt_dato_adic1() {
		return txt_dato_adic1;
	}
	public void setTxt_dato_adic1(String txt_dato_adic1) {
		this.txt_dato_adic1 = txt_dato_adic1;
	}
	public String getTxt_dato_adic2() {
		return txt_dato_adic2;
	}
	public void setTxt_dato_adic2(String txt_dato_adic2) {
		this.txt_dato_adic2 = txt_dato_adic2;
	}
	public String getTxt_dato_adic3() {
		return txt_dato_adic3;
	}
	public void setTxt_dato_adic3(String txt_dato_adic3) {
		this.txt_dato_adic3 = txt_dato_adic3;
	}
}
