package pvt.auna.fcompleja.model.api.request;

public class EmailAlertaMonitoreoRequest {

    private static final long serialVersionUID = 1L;

    private String paciente;
    private String mac;
    private String scgSolben;
    private String solEvaluacion;
    private String medicoAuditor;

    //private int grupoDiagnostico;


    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getScgSolben() {
        return scgSolben;
    }

    public void setScgSolben(String scgSolben) {
        this.scgSolben = scgSolben;
    }

    public String getSolEvaluacion() {
        return solEvaluacion;
    }

    public void setSolEvaluacion(String solEvaluacion) {
        this.solEvaluacion = solEvaluacion;
    }

    public String getMedicoAuditor() {
        return medicoAuditor;
    }

    public void setMedicoAuditor(String medicoAuditor) {
        this.medicoAuditor = medicoAuditor;
    }


    @Override
    public String toString() {
        return "EmailAlertaMonitoreoRequest{" +
                "paciente='" + paciente + '\'' +
                ", mac='" + mac + '\'' +
                ", scgSolben='" + scgSolben + '\'' +
                ", solEvaluacion='" + solEvaluacion + '\'' +
                ", medicoAuditor='" + medicoAuditor + '\'' +
                '}';
    }
}
