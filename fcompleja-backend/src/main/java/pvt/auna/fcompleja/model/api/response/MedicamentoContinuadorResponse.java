package pvt.auna.fcompleja.model.api.response;

import java.util.Date;
import java.util.List;

public class MedicamentoContinuadorResponse {
	
	private Integer codContinuador;
	private Integer resultadoAutorizador;
	private String nombreResultadoAutorizador;
	private Integer codResponsableMonit;
	private String comentario;
	private Integer resultadoMonitoreo;
	private String nombreResultadoMonitoreo;
	private String nroTareaMonitoreo;
	private String fechaMonitoreo;
	private List<DocumentoContinuadorResponse> documentoContinuador;
	private Integer codResultado;
	private String msgResultado;
	public Integer getCodContinuador() {
		return codContinuador;
	}
	public void setCodContinuador(Integer codContinuador) {
		this.codContinuador = codContinuador;
	}
	public Integer getResultadoAutorizador() {
		return resultadoAutorizador;
	}
	public void setResultadoAutorizador(Integer resultadoAutorizador) {
		this.resultadoAutorizador = resultadoAutorizador;
	}
	public Integer getCodResponsableMonit() {
		return codResponsableMonit;
	}
	public void setCodResponsableMonit(Integer codResponsableMonit) {
		this.codResponsableMonit = codResponsableMonit;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Integer getResultadoMonitoreo() {
		return resultadoMonitoreo;
	}
	public void setResultadoMonitoreo(Integer resultadoMonitoreo) {
		this.resultadoMonitoreo = resultadoMonitoreo;
	}
	public String getNroTareaMonitoreo() {
		return nroTareaMonitoreo;
	}
	public void setNroTareaMonitoreo(String nroTareaMonitoreo) {
		this.nroTareaMonitoreo = nroTareaMonitoreo;
	}
	
	public String getNombreResultadoAutorizador() {
		return nombreResultadoAutorizador;
	}
	public void setNombreResultadoAutorizador(String nombreResultadoAutorizador) {
		this.nombreResultadoAutorizador = nombreResultadoAutorizador;
	}
	public String getNombreResultadoMonitoreo() {
		return nombreResultadoMonitoreo;
	}
	public void setNombreResultadoMonitoreo(String nombreResultadoMonitoreo) {
		this.nombreResultadoMonitoreo = nombreResultadoMonitoreo;
	}
	public String getFechaMonitoreo() {
		return fechaMonitoreo;
	}
	public void setFechaMonitoreo(String fechaMonitoreo) {
		this.fechaMonitoreo = fechaMonitoreo;
	}
	public List<DocumentoContinuadorResponse> getDocumentoContinuador() {
		return documentoContinuador;
	}
	public void setDocumentoContinuador(List<DocumentoContinuadorResponse> documentoContinuador) {
		this.documentoContinuador = documentoContinuador;
	}
	public Integer getCodResultado() {
		return codResultado;
	}
	public void setCodResultado(Integer codResultado) {
		this.codResultado = codResultado;
	}
	public String getMsgResultado() {
		return msgResultado;
	}
	public void setMsgResultado(String msgResultado) {
		this.msgResultado = msgResultado;
	}
	
}
