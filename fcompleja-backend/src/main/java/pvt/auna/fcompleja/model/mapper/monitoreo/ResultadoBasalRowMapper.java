package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.ResultadoBasalDetResponse;

public class ResultadoBasalRowMapper implements RowMapper<ResultadoBasalDetResponse>{
	
	@Override
	public ResultadoBasalDetResponse mapRow(ResultSet rs, int rowNum) throws SQLException {		

		ResultadoBasalDetResponse response = new ResultadoBasalDetResponse();
		response.setCodExamenMed(rs.getInt("COD_EXAMEN_MED"));
		response.setCodResultadoBasal(rs.getInt("COD_RESULTADO_MARCADOR"));
		response.setCodConfigMarcador(rs.getInt("COD_CONFIG_MARCA"));
		response.setDescripcionExamenMed(rs.getString("DESCRIPCION"));
		response.setTipoIngresoResul(rs.getInt("P_TIPO_INGRESO_RES"));
		response.setTipoIngresoResulDescrip(rs.getString("TIPO_INGRESO_RES"));
		response.setUnidadMedida(rs.getString("UNIDAD_MEDIDA"));
		response.setRango(rs.getString("RANGO"));
		response.setResultado(rs.getString("RESULTADO"));
		response.setFecResultado(rs.getDate("FEC_RESULTADO"));
		response.setMinLength(rs.getInt("RANGO_MINIMO"));
		response.setMaxLength(rs.getInt("RANGO_MAXIMO"));
		
		response.setPerMinima(rs.getString("PER_MINIMA"));
		response.setPerMaxima(rs.getString("PER_MAXIMA"));
		return response;
	}

}
