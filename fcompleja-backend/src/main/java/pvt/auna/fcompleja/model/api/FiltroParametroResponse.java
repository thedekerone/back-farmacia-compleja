package pvt.auna.fcompleja.model.api;

import java.util.List;

public class FiltroParametroResponse {

	private List<ListFiltroParametroResponse> filtroParametro;
	private Integer codigoResultado;
	private String mensageResultado;

	public List<ListFiltroParametroResponse> getFiltroParametro() {
		return filtroParametro;
	}

	public void setFiltroParametro(List<ListFiltroParametroResponse> filtroParametro) {
		this.filtroParametro = filtroParametro;
	}

	public Integer getCodigoResultado() {
		return codigoResultado;
	}

	public void setCodigoResultado(Integer codigoResultado) {
		this.codigoResultado = codigoResultado;
	}

	public String getMensageResultado() {
		return mensageResultado;
	}

	public void setMensageResultado(String mensageResultado) {
		this.mensageResultado = mensageResultado;
	}

}
