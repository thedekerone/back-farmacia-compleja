package pvt.auna.fcompleja.model.api.request.evaluacion;

import java.io.Serializable;

public class AntecedentePersGinecologico implements Serializable {

	private static final long serialVersionUID = 1L;
	
	String menarquia;
	Integer gestaciones;
	Integer nro_hijos;
	String fur;
	Integer abortos;
	String anticoceptivo;
	String observaciones;
	Boolean aplica;
	
	public String getMenarquia() {
		return menarquia;
	}
	public void setMenarquia(String menarquia) {
		this.menarquia = menarquia;
	}
	public Integer getGestaciones() {
		return gestaciones;
	}
	public void setGestaciones(Integer gestaciones) {
		this.gestaciones = gestaciones;
	}
	public Integer getNro_hijos() {
		return nro_hijos;
	}
	public void setNro_hijos(Integer nro_hijos) {
		this.nro_hijos = nro_hijos;
	}
	public String getFur() {
		return fur;
	}
	public void setFur(String fur) {
		this.fur = fur;
	}
	public Integer getAbortos() {
		return abortos;
	}
	public void setAbortos(Integer abortos) {
		this.abortos = abortos;
	}
	public String getAnticoceptivo() {
		return anticoceptivo;
	}
	public void setAnticoceptivo(String anticoceptivo) {
		this.anticoceptivo = anticoceptivo;
	}
	public Boolean getAplica() {
		return aplica;
	}
	public void setAplica(Boolean aplica) {
		this.aplica = aplica;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

}