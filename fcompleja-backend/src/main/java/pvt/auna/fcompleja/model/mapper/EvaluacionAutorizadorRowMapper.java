package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.EvaluacionAutorizadorResponse;

public class EvaluacionAutorizadorRowMapper implements RowMapper<EvaluacionAutorizadorResponse> {

	@Override
	public EvaluacionAutorizadorResponse mapRow(ResultSet rs, int rowNum) throws SQLException {

		EvaluacionAutorizadorResponse response = new EvaluacionAutorizadorResponse();
		response.setCodHistLineaTrat(rs.getInt("COD_HIST_LINEA_TRAT"));
		response.setCodMac(rs.getInt("COD_MAC"));
		response.setNroLineaTratamiento(rs.getInt("LINEA_TRAT"));
		response.setEstadoMonitoreoMedic(rs.getInt("P_ESTADO"));
		response.setFecInicio(rs.getString("FEC_INICIO"));
		response.setFecFin(rs.getString("FEC_FIN"));
		response.setFecSolEva(rs.getString("FEC_SOL_EVA"));
		response.setCodGrpDiag(rs.getString("COD_GRP_DIAG"));
		return response;
	}

}
