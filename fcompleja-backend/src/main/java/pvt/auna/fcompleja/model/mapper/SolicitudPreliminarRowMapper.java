package pvt.auna.fcompleja.model.mapper;

import org.springframework.jdbc.core.RowMapper;
import pvt.auna.fcompleja.model.bean.SolicitudPreliminar;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SolicitudPreliminarRowMapper implements RowMapper<SolicitudPreliminar> {


    @Override
    public SolicitudPreliminar mapRow(ResultSet rs, int rowNum) throws SQLException {

        SolicitudPreliminar solicitudPreliminar = new SolicitudPreliminar();

        solicitudPreliminar.setCod_sol_pre(rs.getInt("COD_SOL_PRE"));
        solicitudPreliminar.setEstado_sol_pre(rs.getString("ESTADO_SOL_PRE"));
        solicitudPreliminar.setId_scg_solben(rs.getInt("ID_SCG_SOLBEN"));
        solicitudPreliminar.setFec_sol_pre(rs.getDate("FEC_SOL_PRE"));
        solicitudPreliminar.setId_mac(rs.getInt("ID_MAC"));

        return solicitudPreliminar;
    }
}
