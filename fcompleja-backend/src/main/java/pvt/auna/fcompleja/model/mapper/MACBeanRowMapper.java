package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import pvt.auna.fcompleja.model.bean.MACBean;

public class MACBeanRowMapper implements RowMapper<MACBean>{

    @Override
    public MACBean mapRow(ResultSet rs, int i) throws SQLException {
        MACBean o = new MACBean();
        o.setCodigo(rs.getInt("COD_MAC"));
        o.setCodigoLargo(rs.getString("COD_MAC_LARGO"));
        o.setDescripcion(rs.getString("DESCRIPCION"));
        o.setTipoMac(rs.getInt("P_TIPO_MAC"));
        o.setTipo(rs.getString("D_TIPO_MAC"));
        o.setFechaInscripcion(rs.getDate("FEC_INSCRIPCION"));
        o.setFechaInicioVigencia(rs.getDate("FEC_INICIO_VIG"));
        o.setFechaFinVigencia(rs.getDate("FEC_FIN_VIG"));
        o.setFechaCreacion(rs.getDate("FEC_CREACION"));
        o.setUsuarioCreacion(rs.getString("USR_CREACION"));
        o.setFechaModificacion(rs.getDate("FEC_MODIFICACION"));
        o.setUsuarioModificacion(rs.getString("USR_MODIFICACION"));
        o.setEstadoMac(rs.getInt("P_ESTADO_MAC"));
        o.setEstado(rs.getString("D_ESTADO_MAC"));
        return o;
    }
    
}
