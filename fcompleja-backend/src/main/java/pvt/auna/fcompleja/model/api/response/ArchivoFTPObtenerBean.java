package pvt.auna.fcompleja.model.api.response;

public class ArchivoFTPObtenerBean {

	 private String nomArchivo;
	   private String tamano;
	   private byte[] archivo;
	   
	public ArchivoFTPObtenerBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ArchivoFTPObtenerBean(String nomArchivo, String tamano, byte[] archivo) {
		super();
		this.nomArchivo = nomArchivo;
		this.tamano = tamano;
		this.archivo = archivo;
	}

	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public String getTamano() {
		return tamano;
	}

	public void setTamano(String tamano) {
		this.tamano = tamano;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}


	@Override
	public String toString() {
		return "ArchivoFTPObtenerBean [nomArchivo=" + nomArchivo + ", archivo=" + archivo +  ", tamano=" + tamano + "]";
	}
	   
}
