package pvt.auna.fcompleja.model.api.request.evaluacion;

import java.util.List;

/*
 * Clase que representa una respuesta del servicio. 
 * @author: Enrique Paiva
 * @date: 08/03/2019
 */
public class ResponseAfiliado {

	public ResponseAfiliado() {
		super();
		// TODO Auto-generated constructor stub
	}

	private AudiResponse audiResponse;

	private List<Object> dataList;

	public ResponseAfiliado(AudiResponse audiResponse, List<Object> dataList) {
		super();
		this.audiResponse = audiResponse;
		this.dataList = dataList;
	}

	public AudiResponse getAudiResponse() {
		return audiResponse;
	}

	public void setAudiResponse(AudiResponse audiResponse) {
		this.audiResponse = audiResponse;
	}

	public List<Object> getDataList() {
		return dataList;
	}

	public void setDataList(List<Object> dataList) {
		this.dataList = dataList;
	}

	@Override
	public String toString() {
		return "Response [audiResponse=" + audiResponse + ", dataList=" + dataList + "]";
	}

}
