package pvt.auna.fcompleja.model.api.response.evaluacion;

import java.io.Serializable;
import java.sql.Date;

public class ProgramacionCmacDetResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String numeroSolEvaluacion;
	private String codMac;
	private String codigoPaciente;
	private String paciente;
	private String codigoDiagnostico;
	private String diagnostico;
	private String descripcionCmac;
	private String horaCmac;
	private String codSolEvaluacion;
	private String codigoGrabado;
	private String codActa;
	private String observacion;
	private String descripcionResultado;
	private String codigoResultado;
	private Integer codProgramacionCmac;
	private Integer codActaFtp;
	private Date fecReunion;
	private String reporteActaEscaneada;
	

	public String getNumeroSolEvaluacion() {
		return numeroSolEvaluacion;
	}

	public void setNumeroSolEvaluacion(String numeroSolEvaluacion) {
		this.numeroSolEvaluacion = numeroSolEvaluacion;
	}

	public String getCodMac() {
		return codMac;
	}

	public void setCodMac(String codMac) {
		this.codMac = codMac;
	}

	public String getCodigoPaciente() {
		return codigoPaciente;
	}

	public void setCodigoPaciente(String codigoPaciente) {
		this.codigoPaciente = codigoPaciente;
	}

	public String getPaciente() {
		return paciente;
	}

	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}

	public String getCodigoDiagnostico() {
		return codigoDiagnostico;
	}

	public void setCodigoDiagnostico(String codigoDiagnostico) {
		this.codigoDiagnostico = codigoDiagnostico;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getDescripcionCmac() {
		return descripcionCmac;
	}

	public void setDescripcionCmac(String descripcionCmac) {
		this.descripcionCmac = descripcionCmac;
	}

	public String getHoraCmac() {
		return horaCmac;
	}

	public void setHoraCmac(String horaCmac) {
		this.horaCmac = horaCmac;
	}

	public String getCodSolEvaluacion() {
		return codSolEvaluacion;
	}

	public void setCodSolEvaluacion(String codSolEvaluacion) {
		this.codSolEvaluacion = codSolEvaluacion;
	}

	public String getCodigoGrabado() {
		return codigoGrabado;
	}

	public void setCodigoGrabado(String codigoGrabado) {
		this.codigoGrabado = codigoGrabado;
	}

	public String getCodActa() {
		return codActa;
	}

	public void setCodActa(String codActa) {
		this.codActa = codActa;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public void setDescripcionResultado(String descripcionResultado) {
		this.descripcionResultado = descripcionResultado;
	}

	public String getCodigoResultado() {
		return codigoResultado;
	}

	public void setCodigoResultado(String codigoResultado) {
		this.codigoResultado = codigoResultado;
	}

	public Integer getCodProgramacionCmac() {
		return codProgramacionCmac;
	}

	public void setCodProgramacionCmac(Integer codProgramacionCmac) {
		this.codProgramacionCmac = codProgramacionCmac;
	}

	public Integer getCodActaFtp() {
		return codActaFtp;
	}

	public void setCodActaFtp(Integer codActaFtp) {
		this.codActaFtp = codActaFtp;
	}

	public Date getFecReunion() {
		return fecReunion;
	}

	public void setFecReunion(Date fecReunion) {
		this.fecReunion = fecReunion;
	}

	public String getReporteActaEscaneada() {
		return reporteActaEscaneada;
	}

	public void setReporteActaEscaneada(String reporteActaEscaneada) {
		this.reporteActaEscaneada = reporteActaEscaneada;
	}
	
	

}
