package pvt.auna.fcompleja.model.api.response.monitoreo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class SegEjecutivoResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codSegEjecutivo;
	private Long codMonitoreo;
	private Long codEjecutivoMonitoreo;
	private String nomEjecutivoMonitoreo;
	private Timestamp fechaRegistro;
	private String horaRegistro;
	private Integer pEstadoSeguimiento;
	private String descEstadoSeguimiento;
	private String detalleEvento;
	private Integer vistoRespMonitoreo;
	private String usuariocrea;
	private Date fechacrea;
	private String usuarioModif;
	private Date fechaModif;
	private Integer pEstadoMonitoreo;

	public SegEjecutivoResponse() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodSegEjecutivo() {
		return codSegEjecutivo;
	}

	public void setCodSegEjecutivo(Long codSegEjecutivo) {
		this.codSegEjecutivo = codSegEjecutivo;
	}

	public Long getCodMonitoreo() {
		return codMonitoreo;
	}

	public void setCodMonitoreo(Long codMonitoreo) {
		this.codMonitoreo = codMonitoreo;
	}

	public Long getCodEjecutivoMonitoreo() {
		return codEjecutivoMonitoreo;
	}

	public void setCodEjecutivoMonitoreo(Long codEjecutivoMonitoreo) {
		this.codEjecutivoMonitoreo = codEjecutivoMonitoreo;
	}

	public String getNomEjecutivoMonitoreo() {
		return nomEjecutivoMonitoreo;
	}

	public void setNomEjecutivoMonitoreo(String nomEjecutivoMonitoreo) {
		this.nomEjecutivoMonitoreo = nomEjecutivoMonitoreo;
	}

	public Timestamp getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getHoraRegistro() {
		return horaRegistro;
	}

	public void setHoraRegistro(String horaRegistro) {
		this.horaRegistro = horaRegistro;
	}

	public Integer getpEstadoSeguimiento() {
		return pEstadoSeguimiento;
	}

	public void setpEstadoSeguimiento(Integer pEstadoSeguimiento) {
		this.pEstadoSeguimiento = pEstadoSeguimiento;
	}

	public String getDescEstadoSeguimiento() {
		return descEstadoSeguimiento;
	}

	public void setDescEstadoSeguimiento(String descEstadoSeguimiento) {
		this.descEstadoSeguimiento = descEstadoSeguimiento;
	}

	public String getDetalleEvento() {
		return detalleEvento;
	}

	public void setDetalleEvento(String detalleEvento) {
		this.detalleEvento = detalleEvento;
	}

	public Integer getVistoRespMonitoreo() {
		return vistoRespMonitoreo;
	}

	public void setVistoRespMonitoreo(Integer vistoRespMonitoreo) {
		this.vistoRespMonitoreo = vistoRespMonitoreo;
	}

	public String getUsuariocrea() {
		return usuariocrea;
	}

	public void setUsuariocrea(String usuariocrea) {
		this.usuariocrea = usuariocrea;
	}

	public Date getFechacrea() {
		return fechacrea;
	}

	public void setFechacrea(Date fechacrea) {
		this.fechacrea = fechacrea;
	}

	public String getUsuarioModif() {
		return usuarioModif;
	}

	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}

	public Date getFechaModif() {
		return fechaModif;
	}

	public void setFechaModif(Date fechaModif) {
		this.fechaModif = fechaModif;
	}

	public Integer getpEstadoMonitoreo() {
		return pEstadoMonitoreo;
	}

	public void setpEstadoMonitoreo(Integer pEstadoMonitoreo) {
		this.pEstadoMonitoreo = pEstadoMonitoreo;
	}

	@Override
	public String toString() {
		return "SegEjecutivoResponse [codSegEjecutivo=" + codSegEjecutivo + ", codMonitoreo=" + codMonitoreo
				+ ", codEjecutivoMonitoreo=" + codEjecutivoMonitoreo + ", nomEjecutivoMonitoreo="
				+ nomEjecutivoMonitoreo + ", fechaRegistro=" + fechaRegistro + ", horaRegistro=" + horaRegistro
				+ ", pEstadoSeguimiento=" + pEstadoSeguimiento + ", descEstadoSeguimiento=" + descEstadoSeguimiento
				+ ", detalleEvento=" + detalleEvento + ", vistoRespMonitoreo=" + vistoRespMonitoreo + ", usuariocrea="
				+ usuariocrea + ", fechacrea=" + fechacrea + ", usuarioModif=" + usuarioModif + ", fechaModif="
				+ fechaModif + ", pEstadoMonitoreo=" + pEstadoMonitoreo + "]";
	}

}
