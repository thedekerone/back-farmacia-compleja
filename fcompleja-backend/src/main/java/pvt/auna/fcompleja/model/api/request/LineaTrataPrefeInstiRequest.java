package pvt.auna.fcompleja.model.api.request;

public class LineaTrataPrefeInstiRequest {
	
	private Integer codSolEva;
	private Integer nroLineaTrata;
	private Integer nroCurso;
	private Integer tipoTumor;
	private Integer lugarProgresion;
	private Integer respAlcanzada;
	private Integer codPrefeInsti;
	private Integer condicionCancer;
	private Integer codMac;
	private Integer descripcionMac;
	private Integer cumplePrefeInsti;
	private String  observacion;
	private String  codGrupoDiag;
	private Integer lineaTratamiento;
	private Integer codigoRolUsuario;
	private String  fechaEstado;
	private Integer codigoUsuario;
	
	public Integer getCodSolEva() {
		return codSolEva;
	}
	public void setCodSolEva(Integer codSolEva) {
		this.codSolEva = codSolEva;
	}
	public Integer getNroLineaTrata() {
		return nroLineaTrata;
	}
	public void setNroLineaTrata(Integer nroLineaTrata) {
		this.nroLineaTrata = nroLineaTrata;
	}
	public Integer getNroCurso() {
		return nroCurso;
	}
	public void setNroCurso(Integer nroCurso) {
		this.nroCurso = nroCurso;
	}
	public Integer getTipoTumor() {
		return tipoTumor;
	}
	public void setTipoTumor(Integer tipoTumor) {
		this.tipoTumor = tipoTumor;
	}
	public Integer getLugarProgresion() {
		return lugarProgresion;
	}
	public void setLugarProgresion(Integer lugarProgresion) {
		this.lugarProgresion = lugarProgresion;
	}
	public Integer getRespAlcanzada() {
		return respAlcanzada;
	}
	public void setRespAlcanzada(Integer respAlcanzada) {
		this.respAlcanzada = respAlcanzada;
	}
	public Integer getCodPrefeInsti() {
		return codPrefeInsti;
	}
	public void setCodPrefeInsti(Integer codPrefeInsti) {
		this.codPrefeInsti = codPrefeInsti;
	}
	public Integer getCondicionCancer() {
		return condicionCancer;
	}
	public void setCondicionCancer(Integer condicionCancer) {
		this.condicionCancer = condicionCancer;
	}
	public Integer getCodMac() {
		return codMac;
	}
	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}
	public Integer getDescripcionMac() {
		return descripcionMac;
	}
	public void setDescripcionMac(Integer descripcionMac) {
		this.descripcionMac = descripcionMac;
	}
	public Integer getCumplePrefeInsti() {
		return cumplePrefeInsti;
	}
	public void setCumplePrefeInsti(Integer cumplePrefeInsti) {
		this.cumplePrefeInsti = cumplePrefeInsti;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getCodGrupoDiag() {
		return codGrupoDiag;
	}
	public void setCodGrupoDiag(String codGrupoDiag) {
		this.codGrupoDiag = codGrupoDiag;
	}
	public Integer getLineaTratamiento() {
		return lineaTratamiento;
	}
	public void setLineaTratamiento(Integer lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}
	public Integer getCodigoRolUsuario() {
		return codigoRolUsuario;
	}
	public void setCodigoRolUsuario(Integer codigoRolUsuario) {
		this.codigoRolUsuario = codigoRolUsuario;
	}
	public String getFechaEstado() {
		return fechaEstado;
	}
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}
	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	
	@Override
	public String toString() {
		return "LineaTrataPrefeInstiRequest [codSolEva=" + codSolEva + ", nroLineaTrata=" + nroLineaTrata
				+ ", nroCurso=" + nroCurso + ", tipoTumor=" + tipoTumor + ", lugarProgresion=" + lugarProgresion
				+ ", respAlcanzada=" + respAlcanzada + ", codPrefeInsti=" + codPrefeInsti + ", condicionCancer="
				+ condicionCancer + ", codMac=" + codMac + ", descripcionMac=" + descripcionMac + ", cumplePrefeInsti="
				+ cumplePrefeInsti + ", observacion=" + observacion + ", codGrupoDiag=" + codGrupoDiag
				+ ", lineaTratamiento=" + lineaTratamiento + ", codigoRolUsuario=" + codigoRolUsuario + ", fechaEstado="
				+ fechaEstado + ", codigoUsuario=" + codigoUsuario + "]";
	}
}
