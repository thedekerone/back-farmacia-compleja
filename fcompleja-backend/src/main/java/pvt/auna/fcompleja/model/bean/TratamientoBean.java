package pvt.auna.fcompleja.model.bean;

public class TratamientoBean {
	
	private Integer codigoTratamiento;
	private String  descripcionTratamiento;
	private String  descripcionTipoTratamiento;
	
	public Integer getCodigoTratamiento() {
		return codigoTratamiento;
	}
	public void setCodigoTratamiento(Integer codigoTratamiento) {
		this.codigoTratamiento = codigoTratamiento;
	}
	public String getDescripcionTratamiento() {
		return descripcionTratamiento;
	}
	public void setDescripcionTratamiento(String descripcionTratamiento) {
		this.descripcionTratamiento = descripcionTratamiento;
	}
	public String getDescripcionTipoTratamiento() {
		return descripcionTipoTratamiento;
	}
	public void setDescripcionTipoTratamiento(String descripcionTipoTratamiento) {
		this.descripcionTipoTratamiento = descripcionTipoTratamiento;
	}
}
