package pvt.auna.fcompleja.model.api.response;

import java.util.List;

import pvt.auna.fcompleja.model.api.ListaIndicador;

public class IndicadorResponse {

	private List<ListaIndicador> listaIndicador;
	private List<CriterioInclusion> inclusionPrecarga;
	private List<CriterioExclusion> exclusionPrecarga;
	private Integer codigoIndicacionPre;
	private Integer valCumpleChkListPer;
	private String  comentario;
	private String  grabar;
	
	public List<ListaIndicador> getListaIndicador() {
		return listaIndicador;
	}
	public void setListaIndicador(List<ListaIndicador> listaIndicador) {
		this.listaIndicador = listaIndicador;
	}
	public List<CriterioInclusion> getInclusionPrecarga() {
		return inclusionPrecarga;
	}
	public void setInclusionPrecarga(List<CriterioInclusion> inclusionPrecarga) {
		this.inclusionPrecarga = inclusionPrecarga;
	}
	public List<CriterioExclusion> getExclusionPrecarga() {
		return exclusionPrecarga;
	}
	public void setExclusionPrecarga(List<CriterioExclusion> exclusionPrecarga) {
		this.exclusionPrecarga = exclusionPrecarga;
	}
	
	public Integer getCodigoIndicacionPre() {
		return codigoIndicacionPre;
	}
	public void setCodigoIndicacionPre(Integer codigoIndicacionPre) {
		this.codigoIndicacionPre = codigoIndicacionPre;
	}
	public Integer getValCumpleChkListPer() {
		return valCumpleChkListPer;
	}
	public void setValCumpleChkListPer(Integer valCumpleChkListPer) {
		this.valCumpleChkListPer = valCumpleChkListPer;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	
}
