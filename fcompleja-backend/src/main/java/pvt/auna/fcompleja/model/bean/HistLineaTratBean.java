package pvt.auna.fcompleja.model.bean;

import java.io.Serializable;
import java.util.Date;

public class HistLineaTratBean implements Serializable {

	private static final long serialVersionUID = -8022474255483626723L;

	private String codLineaTratamiento;
	private String lineaTratamiento;
	private String macSolicitado;
	private String codEvaluacion;
	private Date fechaAprobacion;
	private Date fechaInicio;
	private Date fechaFin;
	private String nroCurso;
	private String tipoTumor;
	private String respAlcansada;
	private String estado;
	private String motivoInactivacion;
	private String medicoTratantePrescriptor;
	private String montoAutorizado;
	private String nroScgSolben;
	private Date fecScgSolben;
	private String nroInforme;
	private Date fecEmision;
	private String nomAuditorEvaluacion;
	private String nroCgSolben;
	private String fecCgSolben;
	private Integer pEstado;

	public String getCodLineaTratamiento() {
		return codLineaTratamiento;
	}

	public void setCodLineaTratamiento(String codLineaTratamiento) {
		this.codLineaTratamiento = codLineaTratamiento;
	}

	public String getLineaTratamiento() {
		return lineaTratamiento;
	}

	public void setLineaTratamiento(String lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}

	public String getMacSolicitado() {
		return macSolicitado;
	}

	public void setMacSolicitado(String macSolicitado) {
		this.macSolicitado = macSolicitado;
	}

	public String getCodEvaluacion() {
		return codEvaluacion;
	}

	public void setCodEvaluacion(String codEvaluacion) {
		this.codEvaluacion = codEvaluacion;
	}

	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}

	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getNroCurso() {
		return nroCurso;
	}

	public void setNroCurso(String nroCurso) {
		this.nroCurso = nroCurso;
	}

	public String getTipoTumor() {
		return tipoTumor;
	}

	public void setTipoTumor(String tipoTumor) {
		this.tipoTumor = tipoTumor;
	}

	public String getRespAlcansada() {
		return respAlcansada;
	}

	public void setRespAlcansada(String respAlcansada) {
		this.respAlcansada = respAlcansada;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMotivoInactivacion() {
		return motivoInactivacion;
	}

	public void setMotivoInactivacion(String motivoInactivacion) {
		this.motivoInactivacion = motivoInactivacion;
	}

	public String getMedicoTratantePrescriptor() {
		return medicoTratantePrescriptor;
	}

	public void setMedicoTratantePrescriptor(String medicoTratantePrescriptor) {
		this.medicoTratantePrescriptor = medicoTratantePrescriptor;
	}

	public String getMontoAutorizado() {
		return montoAutorizado;
	}

	public void setMontoAutorizado(String montoAutorizado) {
		this.montoAutorizado = montoAutorizado;
	}

	public String getNroScgSolben() {
		return nroScgSolben;
	}

	public void setNroScgSolben(String nroScgSolben) {
		this.nroScgSolben = nroScgSolben;
	}

	public Date getFecScgSolben() {
		return fecScgSolben;
	}

	public void setFecScgSolben(Date fecScgSolben) {
		this.fecScgSolben = fecScgSolben;
	}

	public String getNroInforme() {
		return nroInforme;
	}

	public void setNroInforme(String nroInforme) {
		this.nroInforme = nroInforme;
	}

	public Date getFecEmision() {
		return fecEmision;
	}

	public void setFecEmision(Date fecEmision) {
		this.fecEmision = fecEmision;
	}

	public String getNomAuditorEvaluacion() {
		return nomAuditorEvaluacion;
	}

	public void setNomAuditorEvaluacion(String nomAuditorEvaluacion) {
		this.nomAuditorEvaluacion = nomAuditorEvaluacion;
	}

	public String getNroCgSolben() {
		return nroCgSolben;
	}

	public void setNroCgSolben(String nroCgSolben) {
		this.nroCgSolben = nroCgSolben;
	}

	public String getFecCgSolben() {
		return fecCgSolben;
	}

	public void setFecCgSolben(String fecCgSolben) {
		this.fecCgSolben = fecCgSolben;
	}

	public Integer getpEstado() {
		return pEstado;
	}

	public void setpEstado(Integer pEstado) {
		this.pEstado = pEstado;
	}

	@Override
	public String toString() {
		return "HistLineaTratBean [codLineaTratamiento=" + codLineaTratamiento + ", lineaTratamiento="
				+ lineaTratamiento + ", macSolicitado=" + macSolicitado + ", codEvaluacion=" + codEvaluacion
				+ ", fechaAprobacion=" + fechaAprobacion + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin
				+ ", nroCurso=" + nroCurso + ", tipoTumor=" + tipoTumor + ", respAlcansada=" + respAlcansada
				+ ", estado=" + estado + ", motivoInactivacion=" + motivoInactivacion + ", medicoTratantePrescriptor="
				+ medicoTratantePrescriptor + ", montoAutorizado=" + montoAutorizado + ", nroScgSolben=" + nroScgSolben
				+ ", fecScgSolben=" + fecScgSolben + ", nroInforme=" + nroInforme + ", fecEmision=" + fecEmision
				+ ", nomAuditorEvaluacion=" + nomAuditorEvaluacion + ", nroCgSolben=" + nroCgSolben + ", fecCgSolben="
				+ fecCgSolben + ", pEstado=" + pEstado + "]";
	}

}
