package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.SeguimientoResponse;

public class SeguimientoRowMapper implements RowMapper<SeguimientoResponse>{

	@Override
	public SeguimientoResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		SeguimientoResponse response = new SeguimientoResponse();
		
		response.setCodSeguimiento(rs.getInt("COD_SEGUIMIENTO"));
		response.setDescEstadoEvaluacion(rs.getString("ESTADOEVALUACION"));
		response.setFechaEvaluacion(rs.getString("FEC_ESTADO"));
		response.setCodRolRespEstado(rs.getString("ROL_RESP_ESTADO"));
		response.setCodUsrRespEstado(rs.getString("USR_RESP_ESTADO"));
		response.setCodRolRespRegistroEstado(rs.getString("ROL_RESP_REG_ESTADO"));
		response.setCodUsrRespRegistroEstado(rs.getString("USR_RESP_REG_ESTADO"));
		
		return response;
	}

}
