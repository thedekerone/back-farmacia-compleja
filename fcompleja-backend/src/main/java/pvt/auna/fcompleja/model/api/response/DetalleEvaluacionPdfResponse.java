package pvt.auna.fcompleja.model.api.response;

import java.util.List;

import pvt.auna.fcompleja.model.bean.DetalleReportBean;

public class DetalleEvaluacionPdfResponse {

	private List<DetalleReportBean> datosGenerales;
	private Integer codigoResultado;
	private String mensajeResultado;
	
	public List<DetalleReportBean> getDatosGenerales() {
		return datosGenerales;
	}
	public void setDatosGenerales(List<DetalleReportBean> datosGenerales) {
		this.datosGenerales = datosGenerales;
	}
	public Integer getCodigoResultado() {
		return codigoResultado;
	}
	public void setCodigoResultado(Integer codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	public String getMensajeResultado() {
		return mensajeResultado;
	}
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}
	
	@Override
	public String toString() {
		return "DetalleEvaluacionPdfResponse [datosGenerales=" + datosGenerales + ", codigoResultado=" + codigoResultado
				+ ", mensajeResultado=" + mensajeResultado + "]";
	}
	
	
	
}

