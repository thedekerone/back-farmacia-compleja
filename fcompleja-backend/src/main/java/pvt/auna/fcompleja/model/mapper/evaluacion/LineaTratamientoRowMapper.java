package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.LineaTratamientoBean;

public class LineaTratamientoRowMapper implements RowMapper<LineaTratamientoBean> {

	@Override
	public LineaTratamientoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		LineaTratamientoBean bean = new LineaTratamientoBean();
		bean.setNroCurso(rs.getInt("P_NRO_CURSO"));
		bean.setTipoTumor(rs.getInt("P_TIPO_TUMOR"));
		bean.setLugarProgresion(rs.getInt("P_LUGAR_PROGRESION"));
		bean.setRespAlcanzada(rs.getInt("P_RESP_ALCANZADA"));
		bean.setCumplePrefeInsti(rs.getInt("P_CUMPLE_PREF_INST"));
		bean.setDescripcion(rs.getString("DESCRIPCION"));
		bean.setGrabar(rs.getString("GRABAR"));
		return bean;
	}

}
