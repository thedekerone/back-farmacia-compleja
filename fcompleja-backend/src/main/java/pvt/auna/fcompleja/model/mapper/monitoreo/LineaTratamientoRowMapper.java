package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.monitoreo.LineaTratamientoResponse;;

public class LineaTratamientoRowMapper implements RowMapper<LineaTratamientoResponse> {

	@Override
	public LineaTratamientoResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		LineaTratamientoResponse lineaTrat = new LineaTratamientoResponse();

		lineaTrat.setCodLineaTratamiento(rs.getLong("COD_LINEA_TRATAMIENTO"));
		lineaTrat.setLineaTratamiento(rs.getString("LINEA_TRAT"));
		lineaTrat.setMacSolicitado(rs.getString("MEDICAMENTOSOLICITADO"));
		lineaTrat.setCodEvaluacion(rs.getString("COD_DESC_SOL_EVA"));
		lineaTrat.setFecAprobacion(rs.getDate("FEC_APROBACION"));
		lineaTrat.setFecInicio(rs.getDate("FEC_INICIO"));
		lineaTrat.setFecFin(rs.getDate("FEC_FIN"));
		lineaTrat.setNroCurso(rs.getString("P_CURSO"));
		lineaTrat.setTipoTumor(rs.getString("P_TIPO_TUMOR"));
		lineaTrat.setRespAlcansada(rs.getString("P_RESP_ALCANZADA"));
		lineaTrat.setEstado(rs.getString("P_ESTADO"));
		lineaTrat.setMotivoInactivacion(rs.getString("P_MOTIVO_INACTIVACION"));
		lineaTrat.setMedicoTratantePrescriptor(rs.getString("MEDICO_TRATANTE_PRESCRIPTOR"));
		lineaTrat.setMontoAutorizado(rs.getLong("MONTO_AUTORIZADO"));
		lineaTrat.setNroScgSolben(rs.getString("NRO_SCG_SOLBEN"));
		lineaTrat.setFecScgSolben(rs.getDate("FEC_SCG_SOLBEN"));
		lineaTrat.setNroInforme(rs.getString("NRO_INFORME"));
		lineaTrat.setFecEmision(rs.getDate("FEC_EMISION"));
		lineaTrat.setCodAuditorEvaluacion(rs.getInt("COD_AUDITOR_EVALUACION"));
		lineaTrat.setNroCgSolben(rs.getString("NRO_CG_SOLBEN"));
		lineaTrat.setFecCgSolben(rs.getString("FEC_CG_SOLBEN"));
		lineaTrat.setCodInformeAuto(rs.getInt("COD_INFORME_AUTO"));
		

		return lineaTrat;
	}
}
