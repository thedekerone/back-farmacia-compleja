package pvt.auna.fcompleja.model.bean;

import java.io.Serializable;

public class ParticipanteCorreoBean implements Serializable {

	private static final long serialVersionUID = -8022474255483626723L;
	
	private Integer codParticipante;
	private String codParticipanteLargo;
	private Integer codRol;
	private String nombres;
	private String apellidos;
	private String correoElectronico;
	private String cmpMedico;
	
	public Integer getCodParticipante() {
		return codParticipante;
	}
	public void setCodParticipante(Integer codParticipante) {
		this.codParticipante = codParticipante;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public String getCodParticipanteLargo() {
		return codParticipanteLargo;
	}
	public void setCodParticipanteLargo(String codParticipanteLargo) {
		this.codParticipanteLargo = codParticipanteLargo;
	}
	public Integer getCodRol() {
		return codRol;
	}
	public void setCodRol(Integer codRol) {
		this.codRol = codRol;
	}
	public String getCmpMedico() {
		return cmpMedico;
	}
	public void setCmpMedico(String cmpMedico) {
		this.cmpMedico = cmpMedico;
	}
	
	@Override
	public String toString() {
		return "ParticipanteCorreoBean [codParticipante=" + codParticipante + ", codParticipanteLargo="
				+ codParticipanteLargo + ", codRol=" + codRol + ", nombres=" + nombres + ", apellidos=" + apellidos
				+ ", correoElectronico=" + correoElectronico + ", cmpMedico=" + cmpMedico + "]";
	}
	
	
	
	
}
