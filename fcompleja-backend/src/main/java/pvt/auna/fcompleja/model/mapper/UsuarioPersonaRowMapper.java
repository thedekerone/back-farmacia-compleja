package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.UsuarioPersonaResponse;

public class UsuarioPersonaRowMapper implements RowMapper<UsuarioPersonaResponse> {
	
	@Override
	public UsuarioPersonaResponse mapRow(ResultSet rs, int rowNum) throws SQLException {

		UsuarioPersonaResponse usuarioPersona = new UsuarioPersonaResponse();
		usuarioPersona.setCodUsuario(rs.getInt("COD_USUARIO"));
		usuarioPersona.setNombre(rs.getString("NOMBRES"));
		usuarioPersona.setApePaterno(rs.getString("APE_PAT"));
		usuarioPersona.setApeMaterno(rs.getString("APE_MAT"));
		
		return usuarioPersona;
	}
	
}
