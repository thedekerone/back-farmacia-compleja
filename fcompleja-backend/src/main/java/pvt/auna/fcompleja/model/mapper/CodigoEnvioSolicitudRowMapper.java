package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import pvt.auna.fcompleja.model.bean.CodigoEnvioSolicitudBean;

public class CodigoEnvioSolicitudRowMapper implements RowMapper<CodigoEnvioSolicitudBean>{
	
	
	@Override
	public CodigoEnvioSolicitudBean mapRow(ResultSet rs, int rowNum) throws SQLException {				
		CodigoEnvioSolicitudBean codigoEnvioSolicitudBean = new CodigoEnvioSolicitudBean();
		codigoEnvioSolicitudBean.setCodSolEvaluacion(rs.getInt("COD_SOL_EVA"));
		codigoEnvioSolicitudBean.setCodEnvioSolEvaluacion(rs.getString("CODIGO_ENVIO_ENV_ALER_MONIT"));
		codigoEnvioSolicitudBean.setEstadoSolEvaluacion(rs.getString("COD_SOL_EVA"));	

		return codigoEnvioSolicitudBean;
	}

}
