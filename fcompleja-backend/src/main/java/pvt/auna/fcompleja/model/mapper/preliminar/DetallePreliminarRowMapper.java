package pvt.auna.fcompleja.model.mapper.preliminar;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.InfoSolbenBean;
import pvt.auna.fcompleja.util.GenericUtil;

public class DetallePreliminarRowMapper implements RowMapper<InfoSolbenBean>{

	@Override
	public InfoSolbenBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		InfoSolbenBean infoSolben = new InfoSolbenBean();
		
		infoSolben.setCodSolben(rs.getString("COD_SOLBEN"));
		infoSolben.setNroSCGSolben(rs.getString("NRO_SOLBEN"));
		infoSolben.setCodEstadoSolben(Integer.parseInt(rs.getString("COD_EST_SOLBEN")));
		infoSolben.setEstadoSCGSolben(rs.getString("ESTADO_SOLBEN"));
		infoSolben.setFecSCGSolben(rs.getDate("FEC_SCG_SOLBEN"));
		infoSolben.setHoraSCGSolben(rs.getString("HORA_SCG_SOLBEN"));
		infoSolben.setCodTipoSolben(rs.getInt("COD_TIPO_SOLBEN"));
		infoSolben.setTipoSolben(rs.getString("TIPO_SOLBEN"));
		infoSolben.setCodClinica(rs.getString("COD_CLINICA"));
		infoSolben.setClinica(null);
		infoSolben.setCmpMedico(rs.getString("CMP_MEDICO"));
		infoSolben.setMedicoTratante(rs.getString("MEDICO_TRATANTE_PRESCRIPTOR"));
		infoSolben.setFechaReceta(rs.getDate("FEC_RECETA"));
		infoSolben.setFechaQuimio(rs.getDate("FEC_QUIMIO"));
		infoSolben.setHospitalDesde(rs.getDate("FEC_HOSP_INICIO"));
		infoSolben.setHospitalHasta(rs.getDate("FEC_HOSP_FIN"));
		infoSolben.setMedicamentos(rs.getString("DESC_MEDICAMENTO"));
		infoSolben.setEsquemaQuimio(rs.getString("DESC_ESQUEMA"));
		infoSolben.setPersonaContacto(rs.getString("PERSON_CONTACTO"));
		infoSolben.setTotalPresupuesto(rs.getDouble("TOTAL_PRESUPUESTO"));
		infoSolben.setCodAfiliado(rs.getString("COD_AFI_PACIENTE"));
		infoSolben.setPaciente(null);
		infoSolben.setFechaAfiliado(rs.getDate("FEC_AFILIACION"));
		infoSolben.setEdad(rs.getInt("EDAD_PACIENTE"));
		infoSolben.setCodGrupoDiagnostico(null);
		infoSolben.setGrupoDiagnostico(null);
		infoSolben.setCodDiagnostico(rs.getString("COD_DIAGNOSTICO"));
		infoSolben.setDiagnostico(null);
		infoSolben.setCie10(rs.getString("COD_DIAGNOSTICO"));
		infoSolben.setContratante(rs.getString("DES_CONTRATANTE"));
		infoSolben.setPlan(rs.getString("DES_PLAN"));
		infoSolben.setEstadoClinico(null);
		infoSolben.setTnm(null);
		infoSolben.setObservacion(rs.getString("OBS_CLINICA"));		
		infoSolben.setCodEstadoPre(rs.getInt("COD_ESTADO_PRE"));
		infoSolben.setEstadoSolPre(rs.getString("ESTADO_PRELIMINAR"));
		infoSolben.setCodHis(rs.getString("COD_HIS"));
		infoSolben.setDescHis(rs.getString("DESC_HIS"));		
		return infoSolben;
	}
	
}
