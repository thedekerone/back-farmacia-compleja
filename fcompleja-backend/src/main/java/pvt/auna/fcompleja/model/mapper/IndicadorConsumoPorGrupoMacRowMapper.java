/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorGrupoMacResponse;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class IndicadorConsumoPorGrupoMacRowMapper implements RowMapper<IndicadorConsumoPorGrupoMacResponse>{

	@Override
	public IndicadorConsumoPorGrupoMacResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		IndicadorConsumoPorGrupoMacResponse indicadorConsumoPorGrupoMacResponse = new IndicadorConsumoPorGrupoMacResponse();		
		
		indicadorConsumoPorGrupoMacResponse.setCodigoGrupoDiagnostico(rs.getInt("COD_GRP_DIAG"));
		indicadorConsumoPorGrupoMacResponse.setCodigoMac(rs.getInt("COD_MAC"));
		indicadorConsumoPorGrupoMacResponse.setDescripcion(rs.getString("DESCRIPCION"));
		indicadorConsumoPorGrupoMacResponse.setGastoTotal(rs.getDouble("GASTO_TOTAL"));
		indicadorConsumoPorGrupoMacResponse.setGastoPaciente(rs.getDouble("G_PAC"));
		indicadorConsumoPorGrupoMacResponse.setNroPacientes(rs.getInt("NRO_PACIENTES"));
		indicadorConsumoPorGrupoMacResponse.setNroNuevos(rs.getInt("NRO_NUEVOS"));
		indicadorConsumoPorGrupoMacResponse.setNroContinuadores(rs.getInt("NRO_CONTINUADOR"));
		
		return indicadorConsumoPorGrupoMacResponse;
	}

}

