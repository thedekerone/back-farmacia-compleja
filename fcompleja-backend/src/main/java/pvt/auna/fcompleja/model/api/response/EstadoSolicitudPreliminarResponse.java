package pvt.auna.fcompleja.model.api.response;

public class EstadoSolicitudPreliminarResponse {

    private Integer codResultado;

    private String msgResultado;


    public Integer getCodResultado() {
        return codResultado;
    }

    public void setCodResultado(Integer codResultado) {
        this.codResultado = codResultado;
    }

    public String getMsgResultado() {
        return msgResultado;
    }

    public void setMsgResultado(String msgResultado) {
        this.msgResultado = msgResultado;
    }


    @Override
    public String toString() {
        return "EstadoSolicitudPreliminarResponse{" +
                "codResultado=" + codResultado +
                ", msgResultado='" + msgResultado + '\'' +
                '}';
    }
}
