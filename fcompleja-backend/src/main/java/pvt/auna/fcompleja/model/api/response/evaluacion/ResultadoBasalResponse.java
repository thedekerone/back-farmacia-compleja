package pvt.auna.fcompleja.model.api.response.evaluacion;

import java.util.List;

public class ResultadoBasalResponse {
	private String antecedente;
	private Integer ecog;
	private Integer existeToxicidad;
	private Integer tipoToxicidad;
	private String  grabar;
	private List<ResultadoBasalDetResponse> resultadoBasal;
	private List<MetastasisResponse> metastasis;
	
	public String getAntecedente() {
		return antecedente;
	}
	public void setAntecedente(String antecedente) {
		this.antecedente = antecedente;
	}
	public Integer getEcog() {
		return ecog;
	}
	public void setEcog(Integer ecog) {
		this.ecog = ecog;
	}
	public Integer getExisteToxicidad() {
		return existeToxicidad;
	}
	public void setExisteToxicidad(Integer existeToxicidad) {
		this.existeToxicidad = existeToxicidad;
	}
	public Integer getTipoToxicidad() {
		return tipoToxicidad;
	}
	public void setTipoToxicidad(Integer tipoToxicidad) {
		this.tipoToxicidad = tipoToxicidad;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public List<ResultadoBasalDetResponse> getResultadoBasal() {
		return resultadoBasal;
	}
	public void setResultadoBasal(List<ResultadoBasalDetResponse> resultadoBasal) {
		this.resultadoBasal = resultadoBasal;
	}
	public List<MetastasisResponse> getMetastasis() {
		return metastasis;
	}
	public void setMetastasis(List<MetastasisResponse> metastasis) {
		this.metastasis = metastasis;
	}
}
