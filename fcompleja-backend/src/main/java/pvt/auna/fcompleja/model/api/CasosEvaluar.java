package pvt.auna.fcompleja.model.api;

public class CasosEvaluar {

	private Long codSolEva;
	private String numSolicitudEvaluacion;
	private String codAfipaciente;
	private String paciente;
	private String codDiagnostico;
	private String diagnostico;
	private String codigoMedicamento;
	private String medicamentoSolicitado;
	private String fechaMac;

	public CasosEvaluar() {
		// TODO Auto-generated constructor stub
	}

	public CasosEvaluar(String numSolicitudEvaluacion, String paciente, String diagnostico, String codigoMedicamento, String medicamentoSolicitado, String fechaMac) {
		this.numSolicitudEvaluacion = numSolicitudEvaluacion;
		this.paciente = paciente;
		this.diagnostico = diagnostico;
		this.codigoMedicamento = codigoMedicamento;
		this.medicamentoSolicitado = medicamentoSolicitado;
		this.fechaMac = fechaMac;
	}

	public String getNumSolicitudEvaluacion() {
		return numSolicitudEvaluacion;
	}

	public void setNumSolicitudEvaluacion(String numSolicitudEvaluacion) {
		this.numSolicitudEvaluacion = numSolicitudEvaluacion;
	}

	public String getPaciente() {
		return paciente;
	}

	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getCodigoMedicamento() {
		return codigoMedicamento;
	}

	public void setCodigoMedicamento(String codigoMedicamento) {
		this.codigoMedicamento = codigoMedicamento;
	}

	public String getMedicamentoSolicitado() {
		return medicamentoSolicitado;
	}

	public void setMedicamentoSolicitado(String medicamentoSolicitado) {
		this.medicamentoSolicitado = medicamentoSolicitado;
	}

	public String getFechaMac() {
		return fechaMac;
	}

	public void setFechaMac(String fechaMac) {
		this.fechaMac = fechaMac;
	}

	public String getCodDiagnostico() {
		return codDiagnostico;
	}

	public void setCodDiagnostico(String codDiagnostico) {
		this.codDiagnostico = codDiagnostico;
	}

	public String getCodAfipaciente() {
		return codAfipaciente;
	}

	public void setCodAfipaciente(String codAfipaciente) {
		this.codAfipaciente = codAfipaciente;
	}

	public Long getCodSolEva() {
		return codSolEva;
	}

	public void setCodSolEva(Long codSolEva) {
		this.codSolEva = codSolEva;
	}

	@Override
	public String toString() {
		return "CasosEvaluar [codSolEva=" + codSolEva + ", numSolicitudEvaluacion=" + numSolicitudEvaluacion
				+ ", codAfipaciente=" + codAfipaciente + ", paciente=" + paciente + ", codDiagnostico=" + codDiagnostico
				+ ", diagnostico=" + diagnostico + ", codigoMedicamento=" + codigoMedicamento
				+ ", medicamentoSolicitado=" + medicamentoSolicitado + ", fechaMac=" + fechaMac + "]";
	}

}
