package pvt.auna.fcompleja.model.bean;

public class ParametroBean {
	private Integer codParametro;
	private Integer codGrupoParametro;
	private String denominacion;
	private String abreviatura;
	private String estado;
	public int getCodParametro() {
		return codParametro;
	}
	public void setCodParametro(int codParametro) {
		this.codParametro = codParametro;
	}
	public int getCodGrupoParametro() {
		return codGrupoParametro;
	}
	public void setCodGrupoParametro(int codGrupoParametro) {
		this.codGrupoParametro = codGrupoParametro;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	public String getAbreviatura() {
		return abreviatura;
	}
	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	@Override
	public String toString() {
		return "ParametroBean [codParametro=" + codParametro + ", codGrupoParametro=" + codGrupoParametro
				+ ", denominacion=" + denominacion + ", abreviatura=" + abreviatura + ", estado=" + estado + "]";
	}
}
