package pvt.auna.fcompleja.model.api.request;

public class RequestImputReportEvaluacion {

	private Integer codSolicitudEvaluacion;
	private String codDiagnostico;
	private String descripcionDiagnostico;
	private String codAfiliado;
	private String nombrePaciente;
	private String sexo;
	private String nombreMedicoAuditor;
	private Integer codArchivo;
	private Integer codMac;
	private Integer codGrupopDiagnostico;
	private Integer codUsuario;
	
	public Integer getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}
	
	/**
	 * 
	 */
	public RequestImputReportEvaluacion() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param codSolicitudEvaluacion
	 * @param codDiagnostico
	 * @param descripcionDiagnostico
	 * @param codAfiliado
	 * @param nombrePaciente
	 * @param sexo
	 * @param nombreMedicoAuditor
	 * @param codArchivo
	 * @param codMac
	 * @param codGrupopDiagnostico
	 */
	public RequestImputReportEvaluacion(Integer codSolicitudEvaluacion, String codDiagnostico,
			String descripcionDiagnostico, String codAfiliado, String nombrePaciente, String sexo,
			String nombreMedicoAuditor, Integer codArchivo, Integer codMac, Integer codGrupopDiagnostico) {
		super();
		this.codSolicitudEvaluacion = codSolicitudEvaluacion;
		this.codDiagnostico = codDiagnostico;
		this.descripcionDiagnostico = descripcionDiagnostico;
		this.codAfiliado = codAfiliado;
		this.nombrePaciente = nombrePaciente;
		this.sexo = sexo;
		this.nombreMedicoAuditor = nombreMedicoAuditor;
		this.codArchivo = codArchivo;
		this.codMac = codMac;
		this.codGrupopDiagnostico = codGrupopDiagnostico;
	}
	/**
	 * @return the codSolicitudEvaluacion
	 */
	public Integer getCodSolicitudEvaluacion() {
		return codSolicitudEvaluacion;
	}
	/**
	 * @param codSolicitudEvaluacion the codSolicitudEvaluacion to set
	 */
	public void setCodSolicitudEvaluacion(Integer codSolicitudEvaluacion) {
		this.codSolicitudEvaluacion = codSolicitudEvaluacion;
	}
	/**
	 * @return the codDiagnostico
	 */
	public String getCodDiagnostico() {
		return codDiagnostico;
	}
	/**
	 * @param codDiagnostico the codDiagnostico to set
	 */
	public void setCodDiagnostico(String codDiagnostico) {
		this.codDiagnostico = codDiagnostico;
	}
	/**
	 * @return the descripcionDiagnostico
	 */
	public String getDescripcionDiagnostico() {
		return descripcionDiagnostico;
	}
	/**
	 * @param descripcionDiagnostico the descripcionDiagnostico to set
	 */
	public void setDescripcionDiagnostico(String descripcionDiagnostico) {
		this.descripcionDiagnostico = descripcionDiagnostico;
	}
	/**
	 * @return the codAfiliado
	 */
	public String getCodAfiliado() {
		return codAfiliado;
	}
	/**
	 * @param codAfiliado the codAfiliado to set
	 */
	public void setCodAfiliado(String codAfiliado) {
		this.codAfiliado = codAfiliado;
	}
	/**
	 * @return the nombrePaciente
	 */
	public String getNombrePaciente() {
		return nombrePaciente;
	}
	/**
	 * @param nombrePaciente the nombrePaciente to set
	 */
	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}
	/**
	 * @return the sexo
	 */
	public String getSexo() {
		return sexo;
	}
	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	/**
	 * @return the nombreMedicoAuditor
	 */
	public String getNombreMedicoAuditor() {
		return nombreMedicoAuditor;
	}
	/**
	 * @param nombreMedicoAuditor the nombreMedicoAuditor to set
	 */
	public void setNombreMedicoAuditor(String nombreMedicoAuditor) {
		this.nombreMedicoAuditor = nombreMedicoAuditor;
	}
	/**
	 * @return the codArchivo
	 */
	public Integer getCodArchivo() {
		return codArchivo;
	}
	/**
	 * @param codArchivo the codArchivo to set
	 */
	public void setCodArchivo(Integer codArchivo) {
		this.codArchivo = codArchivo;
	}
	/**
	 * @return the codMac
	 */
	public Integer getCodMac() {
		return codMac;
	}
	/**
	 * @param codMac the codMac to set
	 */
	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}
	/**
	 * @return the codGrupopDiagnostico
	 */
	public Integer getCodGrupopDiagnostico() {
		return codGrupopDiagnostico;
	}
	/**
	 * @param codGrupopDiagnostico the codGrupopDiagnostico to set
	 */
	public void setCodGrupopDiagnostico(Integer codGrupopDiagnostico) {
		this.codGrupopDiagnostico = codGrupopDiagnostico;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RequestImputReportEvaluacion [codSolicitudEvaluacion=" + codSolicitudEvaluacion + ", codDiagnostico="
				+ codDiagnostico + ", descripcionDiagnostico=" + descripcionDiagnostico + ", codAfiliado=" + codAfiliado
				+ ", nombrePaciente=" + nombrePaciente + ", sexo=" + sexo + ", nombreMedicoAuditor="
				+ nombreMedicoAuditor + ", codArchivo=" + codArchivo + ", codMac=" + codMac + ", codGrupopDiagnostico="
				+ codGrupopDiagnostico + ", toString()=" + super.toString() + "]";
	}

	

}
