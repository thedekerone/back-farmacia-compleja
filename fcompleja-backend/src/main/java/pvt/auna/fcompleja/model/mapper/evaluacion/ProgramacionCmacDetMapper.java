package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.ProgramacionCmacDetResponse;


public class ProgramacionCmacDetMapper implements RowMapper<ProgramacionCmacDetResponse>{

	@Override
	public ProgramacionCmacDetResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ProgramacionCmacDetResponse progCmacDet = new ProgramacionCmacDetResponse();
		
		progCmacDet.setNumeroSolEvaluacion(rs.getString("COD_DESC_SOL_EVA"));
		progCmacDet.setCodMac(rs.getString("COD_MAC")); 
		progCmacDet.setCodigoPaciente(rs.getString("COD_AFI_PACIENTE"));
		progCmacDet.setCodigoDiagnostico(rs.getString("COD_DIAGNOSTICO"));
		progCmacDet.setDescripcionCmac(rs.getString("DESCRIPCION"));
		progCmacDet.setHoraCmac(rs.getString("HORA_REUNION"));
		progCmacDet.setCodSolEvaluacion(rs.getString("COD_SOL_EVA"));
		progCmacDet.setCodigoGrabado(rs.getString("COD_GRABADO"));
		progCmacDet.setCodActa(rs.getString("COD_ACTA"));
		progCmacDet.setObservacion(rs.getString("OBSERVACION"));
		progCmacDet.setDescripcionResultado(rs.getString("DESC_EVALUACION"));
		progCmacDet.setCodigoResultado(rs.getString("COD_RES_EVALUACION"));
		
		progCmacDet.setCodProgramacionCmac(rs.getInt("COD_PROGRAMACION_CMAC"));
		progCmacDet.setCodActaFtp(rs.getInt("COD_ACTA_FTP"));
		progCmacDet.setFecReunion(rs.getDate("FEC_REUNION"));
		
		progCmacDet.setReporteActaEscaneada(rs.getString("REPORTE_ACTA_ESCANEADA"));
	
		
		return progCmacDet;
	}
	
	
}
