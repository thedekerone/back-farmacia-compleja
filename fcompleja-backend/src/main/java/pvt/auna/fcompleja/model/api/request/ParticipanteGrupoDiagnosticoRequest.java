package pvt.auna.fcompleja.model.api.request;

import java.io.Serializable;

public class ParticipanteGrupoDiagnosticoRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String   codigoGrupoDiagnostico;
	private Integer  edadPaciente;
	
	
	public String getCodigoGrupoDiagnostico() {
		return codigoGrupoDiagnostico;
	}
	public void setCodigoGrupoDiagnostico(String codigoGrupoDiagnostico) {
		this.codigoGrupoDiagnostico = codigoGrupoDiagnostico;
	}
	public Integer getEdadPaciente() {
		return edadPaciente;
	}
	public void setEdadPaciente(Integer edadPaciente) {
		this.edadPaciente = edadPaciente;
	}
	
	@Override
	public String toString() {
		return "ParticipanteGrupoDiagnosticoRequest [codigoGrupoDiagnostico=" + codigoGrupoDiagnostico
				+ ", edadPaciente=" + edadPaciente + "]";
	}

	
	

}
