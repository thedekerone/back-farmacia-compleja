package pvt.auna.fcompleja.model.api;

public class FiltroUsrRolRequest {

	private String codigoRol;

	public FiltroUsrRolRequest() {
	}

	public FiltroUsrRolRequest(String codigoRol) {
		this.codigoRol = codigoRol;
	}

	public String getCodigoRol() {
		return codigoRol;
	}

	public void setCodigoRol(String codigoRol) {
		this.codigoRol = codigoRol;
	}

}
