package pvt.auna.fcompleja.model.bean;

import org.springframework.web.multipart.MultipartFile;

public class FileControlGasto {

		
	Integer codUsuario;
	MultipartFile file;
	
	public Integer getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}


	
}
