package pvt.auna.fcompleja.model.bean;

public class DetalleReportBean {

	private Integer codigoEvaluacion;
	private String  nombres;
	private String  sexo;
	private String  plan;
	private String  codigoDiagnostico;
	private String  descripcionDiagnostico;
	private String  descripcionMedicamento;
	private String  fecha;
	private String  codigoScgSolben;
	private String  codigoAfiliadoPaciente;
	private Integer edadPaciente;
	private String medicoTratante;
	private String estadioClinico;
	private Integer numeroLinea;
	
	public Integer getCodigoEvaluacion() {
		return codigoEvaluacion;
	}
	public void setCodigoEvaluacion(Integer codigoEvaluacion) {
		this.codigoEvaluacion = codigoEvaluacion;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getCodigoDiagnostico() {
		return codigoDiagnostico;
	}
	public void setCodigoDiagnostico(String codigoDiagnostico) {
		this.codigoDiagnostico = codigoDiagnostico;
	}
	public String getDescripcionDiagnostico() {
		return descripcionDiagnostico;
	}
	public void setDescripcionDiagnostico(String descripcionDiagnostico) {
		this.descripcionDiagnostico = descripcionDiagnostico;
	}
	public String getDescripcionMedicamento() {
		return descripcionMedicamento;
	}
	public void setDescripcionMedicamento(String descripcionMedicamento) {
		this.descripcionMedicamento = descripcionMedicamento;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getCodigoScgSolben() {
		return codigoScgSolben;
	}
	public void setCodigoScgSolben(String codigoScgSolben) {
		this.codigoScgSolben = codigoScgSolben;
	}
	public String getCodigoAfiliadoPaciente() {
		return codigoAfiliadoPaciente;
	}
	public void setCodigoAfiliadoPaciente(String codigoAfiliadoPaciente) {
		this.codigoAfiliadoPaciente = codigoAfiliadoPaciente;
	}
	public Integer getEdadPaciente() {
		return edadPaciente;
	}
	public void setEdadPaciente(Integer edadPaciente) {
		this.edadPaciente = edadPaciente;
	}
	public String getMedicoTratante() {
		return medicoTratante;
	}
	public void setMedicoTratante(String medicoTratante) {
		this.medicoTratante = medicoTratante;
	}
	public String getEstadioClinico() {
		return estadioClinico;
	}
	public void setEstadioClinico(String estadioClinico) {
		this.estadioClinico = estadioClinico;
	}
	public Integer getNumeroLinea() {
		return numeroLinea;
	}
	public void setNumeroLinea(Integer numeroLinea) {
		this.numeroLinea = numeroLinea;
	}
	
	@Override
	public String toString() {
		return "DetalleReportBean [codigoEvaluacion=" + codigoEvaluacion + ", nombres=" + nombres + ", sexo=" + sexo
				+ ", plan=" + plan + ", codigoDiagnostico=" + codigoDiagnostico + ", descripcionDiagnostico="
				+ descripcionDiagnostico + ", descripcionMedicamento=" + descripcionMedicamento + ", fecha=" + fecha
				+ ", codigoScgSolben=" + codigoScgSolben + ", codigoAfiliadoPaciente=" + codigoAfiliadoPaciente
				+ ", edadPaciente=" + edadPaciente + ", medicoTratante=" + medicoTratante + ", estadioClinico="
				+ estadioClinico + ", numeroLinea=" + numeroLinea + "]";
	}
	
	
	
}
