package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.monitoreo.EvolucionResponse;

public class EvolucionRowMapper implements RowMapper<EvolucionResponse> {

	@Override
	public EvolucionResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		EvolucionResponse evolucionResponse = new EvolucionResponse();
		
		evolucionResponse.setCodEvolucion(rs.getLong("COD_EVOLUCION"));
		evolucionResponse.setNroDescEvolucion(rs.getString("NRO_DESC_EVOLUCION"));
		evolucionResponse.setCodMonitoreo(rs.getLong("COD_MONITOREO"));
		evolucionResponse.setCodDescMonitoreo(rs.getString("COD_DESC_MONITOREO"));
		evolucionResponse.setCodMac(rs.getLong("COD_MAC"));
		evolucionResponse.setpResEvolucion(rs.getInt("P_RES_EVOLUCION"));
		evolucionResponse.setCodHistLineaTrat(rs.getLong("COD_HIST_LINEA_TRAT"));
		evolucionResponse.setFecMonitoreo(rs.getDate("FEC_MONITOREO"));
		evolucionResponse.setFecProxMonitoreo(rs.getDate("FEC_PROX_MONITOREO"));
		evolucionResponse.setpMotivoInactivacion(rs.getInt("P_MOTIVO_INACTIVACION"));
		evolucionResponse.setFecInactivacion(rs.getDate("FEC_INACTIVACION"));
		evolucionResponse.setObservacion(rs.getString("OBSERVACION"));
		evolucionResponse.setpTolerancia(rs.getInt("P_TOLERANCIA"));
		evolucionResponse.setDescTolerancia(rs.getString("DESC_TOLERANCIA"));
		evolucionResponse.setpToxicidad(rs.getInt("P_TOXICIDAD"));
		evolucionResponse.setDescToxicidad(rs.getString("DESC_TOXICIDAD"));
		evolucionResponse.setpGrado(rs.getInt("P_GRADO"));
		evolucionResponse.setDescGrado(rs.getString("DESC_GRADO"));
		evolucionResponse.setpRespClinica(rs.getInt("P_RESP_CLINICA"));
		evolucionResponse.setDescRespClinica(rs.getString("DESC_RESP_CLINICA"));
		evolucionResponse.setpAtenAlerta(rs.getInt("P_ATEN_ALERTA"));
		evolucionResponse.setDescAtenAlerta(rs.getString("DESC_ATEN_ALERTA"));
		evolucionResponse.setExisteToxicidad(rs.getString("EXISTE_TOXICIDAD"));
		evolucionResponse.setfUltimoConsumo(rs.getDate("F_ULTIMO_CONSUMO"));
		evolucionResponse.setUltimaCantConsumida(rs.getInt("ULTIMA_CANT_CONSUMIDA"));
		evolucionResponse.setpEstadoMonitoreo(rs.getInt("P_ESTADO_MONITOREO"));
		
		
		return evolucionResponse;
	}
}
