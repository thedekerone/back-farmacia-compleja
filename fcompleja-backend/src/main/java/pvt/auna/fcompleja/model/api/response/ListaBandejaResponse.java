package pvt.auna.fcompleja.model.api.response;

import java.util.ArrayList;

import pvt.auna.fcompleja.model.api.ListaBandeja;

public class ListaBandejaResponse {

	private ArrayList<ListaBandeja> listabandeja;
	private Integer codigoResultado;
	private String mensajeResultado;

	public ArrayList<ListaBandeja> getListabandeja() {
		return listabandeja;
	}

	public void setListabandeja(ArrayList<ListaBandeja> listabandeja) {
		this.listabandeja = listabandeja;
	}

	public Integer getCodigoResultado() {
		return codigoResultado;
	}

	public void setCodigoResultado(Integer codigoResultado) {
		this.codigoResultado = codigoResultado;
	}

	public String getMensajeResultado() {
		return mensajeResultado;
	}

	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}

	@Override
	public String toString() {
		return "ListaBandejaResponse [listabandeja=" + listabandeja + ", codigoResultado=" + codigoResultado
				+ ", mensajeResultado=" + mensajeResultado + "]";
	}

}
