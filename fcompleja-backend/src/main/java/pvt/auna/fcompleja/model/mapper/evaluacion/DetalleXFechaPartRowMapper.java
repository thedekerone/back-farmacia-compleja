package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.ListFiltroRolResponse;

public class DetalleXFechaPartRowMapper implements RowMapper<ListFiltroRolResponse>{

	@Override
	public ListFiltroRolResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ListFiltroRolResponse response = new ListFiltroRolResponse();
		response.setCodigoRol(rs.getInt("COD_USUARIO_CMAC"));
		return response;
	}

}
