package pvt.auna.fcompleja.model.api.request;

import java.util.List;

import pvt.auna.fcompleja.model.api.FiltroUsrRolResponse;
import pvt.auna.fcompleja.model.api.ListFiltroRolResponse;
//import pvt.auna.fcompleja.model.api.ListRolResponsableResponse;
import pvt.auna.fcompleja.model.api.ListaBandeja;

public class registrarEvaluacionCmac {

	private String evaluacion;
	private String participanteCmac;
	private String codigoScan;
	private String fechaProgramada;
	private String horaProgramada;
	private String fechaEstado;
	private Integer codigoRolResponsableCmac;
	private Integer codigoUsrResponsableCmac;
	private Long codActaFtp;
	List<ListaBandeja> listaEvaluacion;
	// List<ListRolResponsableResponse> listaParticipante;

	List<ListFiltroRolResponse> listaParticipante;

	public String getEvaluacion() {
		return evaluacion;
	}

	public void setEvaluacion(String evaluacion) {
		this.evaluacion = evaluacion;
	}

	public String getParticipanteCmac() {
		return participanteCmac;
	}

	public void setParticipanteCmac(String participanteCmac) {
		this.participanteCmac = participanteCmac;
	}

	public String getCodigoScan() {
		return codigoScan;
	}

	public void setCodigoScan(String codigoScan) {
		this.codigoScan = codigoScan;
	}

	public String getFechaProgramada() {
		return fechaProgramada;
	}

	public void setFechaProgramada(String fechaProgramada) {
		this.fechaProgramada = fechaProgramada;
	}

	public String getHoraProgramada() {
		return horaProgramada;
	}

	public void setHoraProgramada(String horaProgramada) {
		this.horaProgramada = horaProgramada;
	}

	public String getFechaEstado() {
		return fechaEstado;
	}

	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

	public Integer getCodigoRolResponsableCmac() {
		return codigoRolResponsableCmac;
	}

	public void setCodigoRolResponsableCmac(Integer codigoRolResponsableCmac) {
		this.codigoRolResponsableCmac = codigoRolResponsableCmac;
	}

	public Integer getCodigoUsrResponsableCmac() {
		return codigoUsrResponsableCmac;
	}

	public void setCodigoUsrResponsableCmac(Integer codigoUsrResponsableCmac) {
		this.codigoUsrResponsableCmac = codigoUsrResponsableCmac;
	}

	public List<ListaBandeja> getListaEvaluacion() {
		return listaEvaluacion;
	}

	public void setListaEvaluacion(List<ListaBandeja> listaEvaluacion) {
		this.listaEvaluacion = listaEvaluacion;
	}

	public List<ListFiltroRolResponse> getListaParticipante() {
		return listaParticipante;
	}

	public void setListaParticipante(List<ListFiltroRolResponse> listaParticipante) {
		this.listaParticipante = listaParticipante;
	}

	public Long getCodActaFtp() {
		return codActaFtp;
	}

	public void setCodActaFtp(Long codActaFtp) {
		this.codActaFtp = codActaFtp;
	}

	@Override
	public String toString() {
		return "registrarEvaluacionCmac [evaluacion=" + evaluacion + ", participanteCmac=" + participanteCmac
				+ ", codigoScan=" + codigoScan + ", fechaProgramada=" + fechaProgramada + ", horaProgramada="
				+ horaProgramada + ", fechaEstado=" + fechaEstado + ", codigoRolResponsableCmac="
				+ codigoRolResponsableCmac + ", codigoUsrResponsableCmac=" + codigoUsrResponsableCmac + ", codActaFtp="
				+ codActaFtp + ", listaEvaluacion=" + listaEvaluacion + ", listaParticipante=" + listaParticipante
				+ "]";
	}

}
