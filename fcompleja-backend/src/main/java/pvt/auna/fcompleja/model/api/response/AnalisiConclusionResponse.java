package pvt.auna.fcompleja.model.api.response;

import java.util.List;

import pvt.auna.fcompleja.model.bean.ConclusionBean;

public class AnalisiConclusionResponse {

	private List< ConclusionBean > conclusion;
	private String nombreCompleto;
	private Integer codigoRespuesta;
	private String  mensajeRespuesta;
	
	public List<ConclusionBean> getConclusion() {
		return conclusion;
	}
	public void setConclusion(List<ConclusionBean> conclusion) {
		this.conclusion = conclusion;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public Integer getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(Integer codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	@Override
	public String toString() {
		return "AnalisiConclusionResponse [conclusion=" + conclusion + ", nombreCompleto=" + nombreCompleto
				+ ", codigoRespuesta=" + codigoRespuesta + ", mensajeRespuesta=" + mensajeRespuesta + "]";
	}
	
	
	
}
