package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.CasosEvaluar;

public class CasosEvaluarRowMapper implements RowMapper<CasosEvaluar> {

	@Override
	public CasosEvaluar mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		CasosEvaluar response = new CasosEvaluar();
		
		response.setCodSolEva(rs.getLong("COD_SOL_EVA"));
		response.setNumSolicitudEvaluacion(rs.getString("COD_DESC_SOL_EVA"));
		response.setCodAfipaciente(rs.getString("COD_AFI_PACIENTE"));
		response.setCodDiagnostico(rs.getString("COD_DIAGNOSTICO"));
		response.setCodigoMedicamento(rs.getString("COD_MAC_LARGO"));
		response.setMedicamentoSolicitado(rs.getString("DESC_MAC"));
		response.setFechaMac(rs.getString("FEC_REUNION"));
		
		return response;
	}

}