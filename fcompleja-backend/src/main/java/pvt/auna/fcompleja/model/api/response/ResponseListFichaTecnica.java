package pvt.auna.fcompleja.model.api.response;

public class ResponseListFichaTecnica {
private Number codMac;

public ResponseListFichaTecnica() {
	super();
	// TODO Auto-generated constructor stub
}

public Number getCodMac() {
	return codMac;
}

public void setCodMac(Number codMac) {
	this.codMac = codMac;
}

@Override
public String toString() {
	return "ResponseListFichaTecnica [codMac=" + codMac + "]";
}

}
