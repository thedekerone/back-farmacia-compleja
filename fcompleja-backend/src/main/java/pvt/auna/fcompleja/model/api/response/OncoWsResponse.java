package pvt.auna.fcompleja.model.api.response;

import pvt.auna.fcompleja.model.api.AudiResponse;

public class OncoWsResponse {

	private AudiResponse audiResponse;

	private Object dataList;

	public OncoWsResponse() {
	}

	public OncoWsResponse(AudiResponse audiResponse, Object dataList) {
		super();
		this.audiResponse = audiResponse;
		this.dataList = dataList;
	}

	public AudiResponse getAudiResponse() {
		return audiResponse;
	}

	public void setAudiResponse(AudiResponse audiResponse) {
		this.audiResponse = audiResponse;
	}

	public Object getDataList() {
		return dataList;
	}

	public void setDataList(Object dataList) {
		this.dataList = dataList;
	}

	@Override
	public String toString() {
		return "OncoWsResponse [audiResponse=" + audiResponse + ", dataList=" + dataList + "]";
	}

}
