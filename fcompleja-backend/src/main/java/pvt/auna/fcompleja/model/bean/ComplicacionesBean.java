/**
 * 
 */
package pvt.auna.fcompleja.model.bean;

import java.util.Date;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ComplicacionesBean {
	
	private Integer codArchivoComp;
	private String codVersion;
	private String nombreArchivo;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private Integer codEstado;
	private String estado;
	private Integer codMac;
	private Integer codigoArchivoComp;
	/**
	 * 
	 */
	public ComplicacionesBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * @param codArchivoComp
	 * @param codVersion
	 * @param nombreArchivo
	 * @param fechaInicioVigencia
	 * @param fechaFinVigencia
	 * @param codEstado
	 * @param estado
	 * @param codMac
	 * @param codFileUpload
	 */
	public ComplicacionesBean(Integer codArchivoComp, String codVersion, String nombreArchivo, Date fechaInicioVigencia,
			Date fechaFinVigencia, Integer codEstado, String estado, Integer codMac, Integer codigoArchivoComp) {
		super();
		this.codArchivoComp = codArchivoComp;
		this.codVersion = codVersion;
		this.nombreArchivo = nombreArchivo;
		this.fechaInicioVigencia = fechaInicioVigencia;
		this.fechaFinVigencia = fechaFinVigencia;
		this.codEstado = codEstado;
		this.estado = estado;
		this.codMac = codMac;
		this.codigoArchivoComp = codigoArchivoComp;
	}


	/**
	 * @return the codArchivoComp
	 */
	public Integer getCodArchivoComp() {
		return codArchivoComp;
	}
	/**
	 * @param codArchivoComp the codArchivoComp to set
	 */
	public void setCodArchivoComp(Integer codArchivoComp) {
		this.codArchivoComp = codArchivoComp;
	}
	/**
	 * @return the codVersion
	 */
	public String getCodVersion() {
		return codVersion;
	}
	/**
	 * @param codVersion the codVersion to set
	 */
	public void setCodVersion(String codVersion) {
		this.codVersion = codVersion;
	}
	/**
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * @return the fechaInicioVigencia
	 */
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	/**
	 * @param fechaInicioVigencia the fechaInicioVigencia to set
	 */
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	/**
	 * @return the fechaFinVigencia
	 */
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	/**
	 * @param fechaFinVigencia the fechaFinVigencia to set
	 */
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	
	/**
	 * @return the codEstado
	 */
	public Integer getCodEstado() {
		return codEstado;
	}


	/**
	 * @param codEstado the codEstado to set
	 */
	public void setCodEstado(Integer codEstado) {
		this.codEstado = codEstado;
	}


	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}


	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}


	/**
	 * @return the codMac
	 */
	public Integer getCodMac() {
		return codMac;
	}
	/**
	 * @param codMac the codMac to set
	 */
	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}
	/**
	 * @return the codFileUpload
	 */
	public Integer getCodigoArchivoComp() {
		return codigoArchivoComp;
	}
	/**
	 * @param codFileUpload the codFileUpload to set
	 */
	public void setCodigoArchivoComp(Integer codigoArchivoComp) {
		this.codigoArchivoComp = codigoArchivoComp;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ComplicacionesBean [codArchivoComp=" + codArchivoComp + ", codVersion=" + codVersion
				+ ", nombreArchivo=" + nombreArchivo + ", fechaInicioVigencia=" + fechaInicioVigencia
				+ ", fechaFinVigencia=" + fechaFinVigencia + ", codEstado=" + codEstado + ", estado=" + estado
				+ ", codMac=" + codMac + ", codigoArchivoComp=" + codigoArchivoComp + ", toString()=" + super.toString() + "]";
	}
	
	
	
	
	

}
