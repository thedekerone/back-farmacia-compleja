/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.MarcadoresBean;


/**
 * @author Jose.Reyes/MDP
 *
 */
public class ListaMarcadoresRowMapper implements RowMapper<MarcadoresBean> {

	@Override
	public MarcadoresBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		MarcadoresBean marcadores = new MarcadoresBean();
		
		marcadores.setCodigoConfigMarca(rs.getInt("COD_CONFIG_MARCA"));
		marcadores.setCodigoConfigMarcaLargo(rs.getString("COD_CONFIG_MARCA_LARGO"));
		marcadores.setCodigoExamenMed(rs.getInt("COD_EXAMEN_MED"));
		marcadores.setDescripcionExamenMed(rs.getString("DESCRIPCION_EXAMEN"));
		marcadores.setCodigoMac(rs.getInt("COD_MAC"));
		marcadores.setDescripcionMac(rs.getString("DESCRIPCION_MAC"));
		marcadores.setCodigoGrupoDiag(rs.getInt("COD_GRP_DIAG"));
		marcadores.setDescripcionGrupoDiag(rs.getString("DESCRIPCION_GRUPO_DIAG"));
		marcadores.setCodigoTipoMarcador(rs.getInt("P_TIPO_MARCADOR"));
		marcadores.setDescripcionMarcador(rs.getString("MARCADOR"));
		marcadores.setCodigoPerMaxima(rs.getInt("COD_PER_MAXIMA"));
		marcadores.setDescripcionPerMaxima(rs.getString("DESCRIPCION_PER_MAXIMA"));
		marcadores.setCodigoPerMinima(rs.getInt("COD_PER_MINIMA"));
		marcadores.setDescripcionPerMinima(rs.getString("DESCRIPCION_PER_MINIMA"));
		marcadores.setCodigoEstado(rs.getInt("COD_ESTADO"));
		marcadores.setDescripcionEstado(rs.getString("ESTADO"));
		
		return marcadores;
	}

}
