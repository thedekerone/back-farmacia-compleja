package pvt.auna.fcompleja.model.api;

public class AccesoOpcionResponse {
	private Integer codOpcion;
	private Integer codRol;
	private String flagAsignacion;
	public Integer getCodOpcion() {
		return codOpcion;
	}
	public void setCodOpcion(Integer codOpcion) {
		this.codOpcion = codOpcion;
	}
	public Integer getCodRol() {
		return codRol;
	}
	public void setCodRol(Integer codRol) {
		this.codRol = codRol;
	}
	public String getFlagAsignacion() {
		return flagAsignacion;
	}
	public void setFlagAsignacion(String flagAsignacion) {
		this.flagAsignacion = flagAsignacion;
	}
	
}
