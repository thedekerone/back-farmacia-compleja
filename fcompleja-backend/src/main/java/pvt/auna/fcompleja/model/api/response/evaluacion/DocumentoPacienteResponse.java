package pvt.auna.fcompleja.model.api.response.evaluacion;

import java.util.List;

import pvt.auna.fcompleja.model.api.response.HistPacienteResponse;

public class DocumentoPacienteResponse {
	
	private List<HistPacienteResponse> documentoPaciente;

	public List<HistPacienteResponse> getDocumentoPaciente() {
		return documentoPaciente;
	}

	public void setDocumentoPaciente(List<HistPacienteResponse> documentoPaciente) {
		this.documentoPaciente = documentoPaciente;
	}
	
	
}
