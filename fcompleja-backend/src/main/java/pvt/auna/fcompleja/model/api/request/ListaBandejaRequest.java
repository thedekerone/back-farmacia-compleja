package pvt.auna.fcompleja.model.api.request;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ListaBandejaRequest implements Serializable {

	private static final long serialVersionUID = 4529468696001493524L;

	private String codigoEvaluacion;
	private String codigoClinica;
	private String codigoPaciente;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
	private Date fechaInicio;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
	private Date fechaFin;

	private String numeroScgSolben;
	private String estadoSolicitudEvaluacion;
	private String tipoScgSolben;
	private Integer rolResponsable;
	private String estadoScgSolben;
	private String correoLiderTumor;
	private String numeroCartaGarantia;
	private String correoCmac;
	private Integer autorizadorPertenencia;
	private String tipoEvaluacion;
	private Integer liderTumor;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
	private Date reunionCmac;

	private String horaCmac;
	private String flagFiltro;
	private String flagClinica;

	private Integer codInformeAuto;
	private Integer codActaCmacFtp;
	private String codRepActaScan;
	
	private String nombre;
	private String apePaterno;
	private String apeMaterno;
	private String tipoDoc;
	private String nroDoc;

	public String getCodigoEvaluacion() {
		return codigoEvaluacion;
	}

	public void setCodigoEvaluacion(String codigoEvaluacion) {
		this.codigoEvaluacion = codigoEvaluacion;
	}

	public String getCodigoClinica() {
		return codigoClinica;
	}

	public void setCodigoClinica(String codigoClinica) {
		this.codigoClinica = codigoClinica;
	}

	public String getCodigoPaciente() {
		return codigoPaciente;
	}

	public void setCodigoPaciente(String codigoPaciente) {
		this.codigoPaciente = codigoPaciente;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getNumeroScgSolben() {
		return numeroScgSolben;
	}

	public void setNumeroScgSolben(String numeroScgSolben) {
		this.numeroScgSolben = numeroScgSolben;
	}

	public String getEstadoSolicitudEvaluacion() {
		return estadoSolicitudEvaluacion;
	}

	public void setEstadoSolicitudEvaluacion(String estadoSolicitudEvaluacion) {
		this.estadoSolicitudEvaluacion = estadoSolicitudEvaluacion;
	}

	public String getTipoScgSolben() {
		return tipoScgSolben;
	}

	public void setTipoScgSolben(String tipoScgSolben) {
		this.tipoScgSolben = tipoScgSolben;
	}

	public Integer getRolResponsable() {
		return rolResponsable;
	}

	public void setRolResponsable(Integer rolResponsable) {
		this.rolResponsable = rolResponsable;
	}

	public String getEstadoScgSolben() {
		return estadoScgSolben;
	}

	public void setEstadoScgSolben(String estadoScgSolben) {
		this.estadoScgSolben = estadoScgSolben;
	}

	public String getCorreoLiderTumor() {
		return correoLiderTumor;
	}

	public void setCorreoLiderTumor(String correoLiderTumor) {
		this.correoLiderTumor = correoLiderTumor;
	}

	public String getNumeroCartaGarantia() {
		return numeroCartaGarantia;
	}

	public void setNumeroCartaGarantia(String numeroCartaGarantia) {
		this.numeroCartaGarantia = numeroCartaGarantia;
	}

	public String getCorreoCmac() {
		return correoCmac;
	}

	public void setCorreoCmac(String correoCmac) {
		this.correoCmac = correoCmac;
	}

	public Integer getAutorizadorPertenencia() {
		return autorizadorPertenencia;
	}

	public void setAutorizadorPertenencia(Integer autorizadorPertenencia) {
		this.autorizadorPertenencia = autorizadorPertenencia;
	}

	public String getTipoEvaluacion() {
		return tipoEvaluacion;
	}

	public void setTipoEvaluacion(String tipoEvaluacion) {
		this.tipoEvaluacion = tipoEvaluacion;
	}

	public Integer getLiderTumor() {
		return liderTumor;
	}

	public void setLiderTumor(Integer liderTumor) {
		this.liderTumor = liderTumor;
	}

	public Date getReunionCmac() {
		return reunionCmac;
	}

	public void setReunionCmac(Date reunionCmac) {
		this.reunionCmac = reunionCmac;
	}

	public String getHoraCmac() {
		return horaCmac;
	}

	public void setHoraCmac(String horaCmac) {
		this.horaCmac = horaCmac;
	}

	public String getFlagFiltro() {
		return flagFiltro;
	}

	public void setFlagFiltro(String flagFiltro) {
		this.flagFiltro = flagFiltro;
	}

	public String getFlagClinica() {
		return flagClinica;
	}

	public void setFlagClinica(String flagClinica) {
		this.flagClinica = flagClinica;
	}

	public Integer getCodInformeAuto() {
		return codInformeAuto;
	}

	public void setCodInformeAuto(Integer codInformeAuto) {
		this.codInformeAuto = codInformeAuto;
	}

	public Integer getCodActaCmacFtp() {
		return codActaCmacFtp;
	}

	public void setCodActaCmacFtp(Integer codActaCmacFtp) {
		this.codActaCmacFtp = codActaCmacFtp;
	}

	public String getCodRepActaScan() {
		return codRepActaScan;
	}

	public void setCodRepActaScan(String codRepActaScan) {
		this.codRepActaScan = codRepActaScan;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApePaterno() {
		return apePaterno;
	}

	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	public String getApeMaterno() {
		return apeMaterno;
	}

	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDocumento) {
		this.tipoDoc = tipoDocumento;
	}

	public String getNroDoc() {
		return nroDoc;
	}

	public void setNroDoc(String nroDoc) {
		this.nroDoc = nroDoc;
	}

	@Override
	public String toString() {
		return "ListaBandejaRequest [codigoEvaluacion=" + codigoEvaluacion + ", codigoClinica=" + codigoClinica
				+ ", codigoPaciente=" + codigoPaciente + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin
				+ ", numeroScgSolben=" + numeroScgSolben + ", estadoSolicitudEvaluacion=" + estadoSolicitudEvaluacion
				+ ", tipoScgSolben=" + tipoScgSolben + ", rolResponsable=" + rolResponsable + ", estadoScgSolben="
				+ estadoScgSolben + ", correoLiderTumor=" + correoLiderTumor + ", numeroCartaGarantia="
				+ numeroCartaGarantia + ", correoCmac=" + correoCmac + ", autorizadorPertenencia="
				+ autorizadorPertenencia + ", tipoEvaluacion=" + tipoEvaluacion + ", liderTumor=" + liderTumor
				+ ", reunionCmac=" + reunionCmac + ", horaCmac=" + horaCmac + ", flagFiltro=" + flagFiltro
				+ ", flagClinica=" + flagClinica + ", codInformeAuto=" + codInformeAuto + ", codActaCmacFtp="
				+ codActaCmacFtp + ", codRepActaScan=" + codRepActaScan + ", nombre=" + nombre +  ", apellidoPaterno="
				+ apePaterno +  ", apellidoMaterno=" + apeMaterno +  "tipoDocuemnto=" + tipoDoc +", numeroDocumento="+ nroDoc+"]";
	}

}
