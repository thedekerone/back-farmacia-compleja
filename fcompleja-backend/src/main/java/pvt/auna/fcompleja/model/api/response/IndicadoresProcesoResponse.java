package pvt.auna.fcompleja.model.api.response;

public class IndicadoresProcesoResponse {
	
	private String indicador;
	private Double cantidad;
	private String numerador;
	private String denominador;
	private String quiebre;
	private Integer fecAno;
	private Integer fecMes;
	/**
	 * 
	 */
	public IndicadoresProcesoResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * @param indicador
	 * @param cantidad
	 * @param numerador
	 * @param denominador
	 * @param quiebre
	 * @param fecAno
	 * @param fecMes
	 */
	public IndicadoresProcesoResponse(String indicador, Double cantidad, String numerador, String denominador,
			String quiebre, Integer fecAno, Integer fecMes) {
		super();
		this.indicador = indicador;
		this.cantidad = cantidad;
		this.numerador = numerador;
		this.denominador = denominador;
		this.quiebre = quiebre;
		this.fecAno = fecAno;
		this.fecMes = fecMes;
	}


	/**
	 * @return the indicador
	 */
	public String getIndicador() {
		return indicador;
	}
	/**
	 * @param indicador the indicador to set
	 */
	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}
	/**
	 * @return the cantidad
	 */
	public Double getCantidad() {
		return cantidad;
	}
	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	/**
	 * @return the quiebre
	 */
	public String getQuiebre() {
		return quiebre;
	}
	/**
	 * @param quiebre the quiebre to set
	 */
	public void setQuiebre(String quiebre) {
		this.quiebre = quiebre;
	}
	/**
	 * @return the fecAno
	 */
	public Integer getFecAno() {
		return fecAno;
	}
	/**
	 * @param fecAno the fecAno to set
	 */
	public void setFecAno(Integer fecAno) {
		this.fecAno = fecAno;
	}
	/**
	 * @return the fecMes
	 */
	public Integer getFecMes() {
		return fecMes;
	}
	/**
	 * @param fecMes the fecMes to set
	 */
	public void setFecMes(Integer fecMes) {
		this.fecMes = fecMes;
	}
	
	
	
	/**
	 * @return the numerador
	 */
	public String getNumerador() {
		return numerador;
	}


	/**
	 * @param numerador the numerador to set
	 */
	public void setNumerador(String numerador) {
		this.numerador = numerador;
	}


	/**
	 * @return the denominador
	 */
	public String getDenominador() {
		return denominador;
	}


	/**
	 * @param denominador the denominador to set
	 */
	public void setDenominador(String denominador) {
		this.denominador = denominador;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IndicadoresProcesoResponse [indicador=" + indicador + ", cantidad=" + cantidad + ", numerador="
				+ numerador + ", denominador=" + denominador + ", quiebre=" + quiebre + ", fecAno=" + fecAno
				+ ", fecMes=" + fecMes + ", toString()=" + super.toString() + "]";
	}


	
	
	

}
