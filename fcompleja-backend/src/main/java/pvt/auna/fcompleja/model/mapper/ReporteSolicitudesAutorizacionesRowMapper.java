/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import pvt.auna.fcompleja.model.api.response.ReporteSolicitudesAutorizacionesResponse;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ReporteSolicitudesAutorizacionesRowMapper implements RowMapper<ReporteSolicitudesAutorizacionesResponse>{

	@Override
	public ReporteSolicitudesAutorizacionesResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReporteSolicitudesAutorizacionesResponse reporteSolicitudAutorizaciones = new ReporteSolicitudesAutorizacionesResponse();		
		
		reporteSolicitudAutorizaciones.setFechaMes(rs.getInt("FEC_MES"));
		reporteSolicitudAutorizaciones.setFechaAno(rs.getInt("FEC_ANO"));
		reporteSolicitudAutorizaciones.setCodigoSolicitudEva(rs.getString("COD_SOL_EVA"));
		reporteSolicitudAutorizaciones.setCodigoPaciente(rs.getString("COD_PACIENTE"));
		reporteSolicitudAutorizaciones.setCodigoAfiliado(rs.getString("COD_AFILIADO"));
		reporteSolicitudAutorizaciones.setFechaAfiliacion(rs.getDate("FECHA_AFILIACION"));
		reporteSolicitudAutorizaciones.setTipoAfiliacion(rs.getString("TIPO_AFILIACION"));
		reporteSolicitudAutorizaciones.setDocumentoIdentidad(rs.getString("DOC_IDENTIDAD"));
		reporteSolicitudAutorizaciones.setPaciente(rs.getString("PACIENTE"));
		reporteSolicitudAutorizaciones.setSexo(rs.getString("SEXO"));
		reporteSolicitudAutorizaciones.setEdad(rs.getInt("EDAD"));
		reporteSolicitudAutorizaciones.setMedicamento(rs.getString("MEDICAMENTO"));
		reporteSolicitudAutorizaciones.setFechaReceta(rs.getDate("FECHA_RECETA"));
		reporteSolicitudAutorizaciones.setCie10(rs.getString("CIE10"));
		reporteSolicitudAutorizaciones.setDiagnostico(rs.getString("DIAGNOSTICO"));
		reporteSolicitudAutorizaciones.setGrupoDiagnostico(rs.getString("GRUPO_DIAGNOSTICO"));
		reporteSolicitudAutorizaciones.setEstadio(rs.getString("ESTADIO"));
		reporteSolicitudAutorizaciones.setTnm(rs.getString("TNM"));
		reporteSolicitudAutorizaciones.setLinea(rs.getString("LINEA"));
		reporteSolicitudAutorizaciones.setClinica(rs.getString("CLINICA"));
		reporteSolicitudAutorizaciones.setCmp(rs.getString("CMP"));
		reporteSolicitudAutorizaciones.setMedicoTratantePrescriptor(rs.getString("MEC_TRATANTE_PRESC"));
		reporteSolicitudAutorizaciones.setCodigoSCGSolben(rs.getString("COD_SCG_SOLBEN"));
		reporteSolicitudAutorizaciones.setFechaRegistroSCGSolben(rs.getDate("FEC_REG_SCG_SOLBEN"));
		reporteSolicitudAutorizaciones.setHoraRegistroSCGSolben(rs.getString("HOR_REG_SCG_SOLBEN"));
		reporteSolicitudAutorizaciones.setTipoSCGSolben(rs.getString("TIPO_SCG_SOLBEN"));
		reporteSolicitudAutorizaciones.setEstadoSCGSolben(rs.getString("ESTADO_SCG_SOLBEN"));
		reporteSolicitudAutorizaciones.setCodigoSolicitudPre(rs.getString("COD_SOL_PRE"));
		reporteSolicitudAutorizaciones.setFechaRegistroSolicitudPre(rs.getDate("FEC_REG_SOL_PRE"));
		reporteSolicitudAutorizaciones.setHoraRegistroSolicitudPre(rs.getString("HOR_REG_SOL_PRE"));
		reporteSolicitudAutorizaciones.setEstadoSolicitudPre(rs.getString("ESTADO_SOL_PRE"));
		reporteSolicitudAutorizaciones.setAutorizadorPertinenciaSolicitudPre(rs.getString("AUTOR_PERTINENCIA_SOL_PRE"));
		reporteSolicitudAutorizaciones.setFechaHoraRegistroSolicitudEva(rs.getTimestamp("FEC_HOR_REG_SOL_EVA"));
		reporteSolicitudAutorizaciones.setContratante(rs.getString("CONTRATANTE"));
		reporteSolicitudAutorizaciones.setCumplePrefInstitucional(rs.getString("CUMPLE_PREF_INST"));
		reporteSolicitudAutorizaciones.setIndicacionesCheckListPaciente(rs.getString("INDIC_CHECK_LIST_PAC"));
		reporteSolicitudAutorizaciones.setCumpleIndicacionesCheckListPaciente(rs.getString("CUMPLE_INDIC_CHECK_LIST_PAC"));
		reporteSolicitudAutorizaciones.setPertinencia(rs.getString("PERTINENCIA"));
		reporteSolicitudAutorizaciones.setCondicionPaciente(rs.getString("COND_PACIENTE"));
		reporteSolicitudAutorizaciones.setTiempoUso(rs.getInt("TIEMPO_USO"));
		reporteSolicitudAutorizaciones.setEstadoSolicitudEva(rs.getString("ESTADO_SOL_EVA"));
		reporteSolicitudAutorizaciones.setAutorizadorPertinenciaSolicitudEva(rs.getString("AUTOR_PERTINENCIA_SOL_EVA"));
		reporteSolicitudAutorizaciones.setFechaEvaluacionAutorizadorPertinencia(rs.getDate("FEC_EVA_AUTOR_PERTI"));
		reporteSolicitudAutorizaciones.setResultadoEvaluacionAutorizadorPertinencia(rs.getString("RESULTADO_EVA_AUTOR_PERTI"));
		reporteSolicitudAutorizaciones.setLiderTumor(rs.getString("LIDER_TUMOR"));
		reporteSolicitudAutorizaciones.setFechaEvaluacionLiderTumor(rs.getDate("FEC_EVA_LIDER_TUMOR"));
		reporteSolicitudAutorizaciones.setResultadoEvaluacionLiderTumor(rs.getString("RESULTADO_EVA_LIDER_TUMOR"));
		reporteSolicitudAutorizaciones.setCorreoEnviadoCMAC(rs.getString("CORREO_ENV_CMAC"));
		reporteSolicitudAutorizaciones.setFechaReunionCMAC(rs.getDate("FEC_REUNION_CMAC"));
		reporteSolicitudAutorizaciones.setResultadoEvaluacionCMAC(rs.getString("RESULTADO_EVA_CMAC"));
		reporteSolicitudAutorizaciones.setNroCartaGarantia(rs.getString("NRO_CARTA_GARANTIA"));
		reporteSolicitudAutorizaciones.setTiempoAutorPertinenciaEva(rs.getString("TIEMPO_AUTOR_PERTINENCIA_EVA"));
		reporteSolicitudAutorizaciones.setTiempoLiderTumor(rs.getString("TIEMPO_LIDER_TUMOR"));
		reporteSolicitudAutorizaciones.setTiempoCMAC(rs.getString("TIEMPO_CMAC"));
		reporteSolicitudAutorizaciones.setTiempoTotal(rs.getString("TIEMPO_TOTAL"));
		
		return reporteSolicitudAutorizaciones;
	}

}

