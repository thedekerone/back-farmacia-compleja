package pvt.auna.fcompleja.model.api.request;

import java.io.Serializable;
import java.util.ArrayList;

import pvt.auna.fcompleja.model.api.request.evaluacion.AntecedenteOncoFam;
import pvt.auna.fcompleja.model.api.request.evaluacion.AntecedenteOncoPers;
import pvt.auna.fcompleja.model.api.request.evaluacion.AntecedentePatoPersonal;
import pvt.auna.fcompleja.model.api.request.evaluacion.AntecedentePersGinecologico;
import pvt.auna.fcompleja.model.api.request.evaluacion.UsuarioAntecedente;

public class RegistrarAntecedenteRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	String codAntecedente;
	
	private UsuarioAntecedente codigos;	
	
	private AntecedentePersGinecologico ant_per_gine;
	
	private AntecedentePatoPersonal ant_pat_per;
	
	private ArrayList<AntecedenteOncoPers> ant_onc_per;
	
	private AntecedenteOncoFam ant_onc_fam;
	
	


	public String getCodAntecedente() {
		return codAntecedente;
	}

	public void setCodAntecedente(String codAntecedente) {
		this.codAntecedente = codAntecedente;
	}

	public UsuarioAntecedente getCodigos() {
		return codigos;
	}

	public void setCodigos(UsuarioAntecedente codigos) {
		this.codigos = codigos;
	}

	public AntecedentePersGinecologico getAnt_per_gine() {
		return ant_per_gine;
	}

	public void setAnt_per_gine(AntecedentePersGinecologico ant_per_gine) {
		this.ant_per_gine = ant_per_gine;
	}

	public AntecedentePatoPersonal getAnt_pat_per() {
		return ant_pat_per;
	}

	public void setAnt_pat_per(AntecedentePatoPersonal ant_pat_per) {
		this.ant_pat_per = ant_pat_per;
	}

	public ArrayList<AntecedenteOncoPers> getAnt_onc_per() {
		return ant_onc_per;
	}

	public void setAnt_onc_per(ArrayList<AntecedenteOncoPers> ant_onc_per) {
		this.ant_onc_per = ant_onc_per;
	}

	public AntecedenteOncoFam getAnt_onc_fam() {
		return ant_onc_fam;
	}

	public void setAnt_onc_fam(AntecedenteOncoFam ant_onc_fam) {
		this.ant_onc_fam = ant_onc_fam;
	}
	

}
