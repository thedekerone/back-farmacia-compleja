package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoResponse;

public class MonitoreoRowMapper implements RowMapper<MonitoreoResponse> {

	@Override
	public MonitoreoResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		MonitoreoResponse monitoreo = new MonitoreoResponse();

		monitoreo.setCodigoMonitoreo(rs.getLong("COD_MONITOREO"));
		monitoreo.setCodigoDescripcionMonitoreo(rs.getString("COD_DESC_MONITOREO"));
		monitoreo.setCodigoEvaluacion(rs.getString("COD_DESC_SOL_EVA"));
		monitoreo.setFechAprobacion(rs.getDate("FECHAAPROBACION"));
		monitoreo.setCodigoScgSolben(rs.getString("COD_SCG_SOLBEN"));
		monitoreo.setNroCartaGarantia(rs.getString("NRO_CG"));
		monitoreo.setCodDiagnostico(rs.getString("COD_DIAGNOSTICO"));
		monitoreo.setCodGrupoDiagnostico(rs.getString("COD_GRUPO_DIAGNOSTICO"));
		monitoreo.setMedicoTratante(rs.getString("MEDICO_TRATANTE"));
		monitoreo.setEdadPaciente(rs.getInt("EDAD_PACIENTE"));
		monitoreo.setCodMedicamento(rs.getLong("COD_MEDICAMENTO"));
		monitoreo.setNomMedicamento(rs.getString("NOM_MEDICAMENTO"));
		monitoreo.setNumeroLineaTratamiento(rs.getString("LINEA_TRAT"));
		monitoreo.setFecIniLineaTratamiento(rs.getDate("FEC_INI_LINEA_TRAT"));
		monitoreo.setCodigoPaciente(rs.getString("COD_PACIENTE"));
		monitoreo.setCodigoClinica(rs.getString("COD_CLINICA"));
		monitoreo.setCodEstadoMonitoreo(rs.getInt("COD_ESTADOMONITOREO"));
		monitoreo.setNomEstadoMonitoreo(rs.getString("DESC_ESTADOMONITOREO"));
		monitoreo.setFechaProximoMonitoreo(rs.getDate("FECHAPROXIMOMONITOREO"));
		monitoreo.setCodResponsableMonitoreo(rs.getLong("COD_RESPONSABLEMONITOREO"));
		monitoreo.setNomResponsableMonitoreo(rs.getString("NOM_RESPONSABLEMONITOREO"));
		monitoreo.setCodigoAfiliado(rs.getString("COD_AFILIADO"));
		monitoreo.setCodSolEvaluacion(rs.getLong("COD_SOL_EVA"));
		monitoreo.setCodHistLineaTrat(rs.getLong("COD_HIST_LINEA_TRAT"));
		monitoreo.setCodEvolucion(rs.getLong("COD_EVOLUCION"));
		monitoreo.setCodInformeAuto(rs.getLong("COD_INFORME_AUTO"));
		/*monitoreo.setTipDoc(rs.getString("TIPO_DOC"));
		monitoreo.setNumDoc(rs.getString("NUM_DOC"));*/

		return monitoreo;
	}

}
