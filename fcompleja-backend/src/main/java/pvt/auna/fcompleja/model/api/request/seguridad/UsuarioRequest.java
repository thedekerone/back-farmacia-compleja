package pvt.auna.fcompleja.model.api.request.seguridad;

public class UsuarioRequest {

    private Integer codUsuario;
    private String usuario;
    private Integer codAplicacion;
    private Integer codRol;
    private Integer estado;

    private String tokenTemporal;

    public String getTokenTemporal() {
        return tokenTemporal;
    }

    public void setTokenTemporal(String tokenTemporal) {
        this.tokenTemporal = tokenTemporal;
    }

    public Integer getCodAplicacion() {
        return codAplicacion;
    }

    public void setCodAplicacion(Integer codAplicacion) {
        this.codAplicacion = codAplicacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(Integer codUsuario) {
        this.codUsuario = codUsuario;
    }

    public Integer getCodRol() {
        return codRol;
    }

    public void setCodRol(Integer codRol) {
        this.codRol = codRol;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "UsuarioRequest [codUsuario=" + codUsuario + ", usuario=" + usuario + ", codAplicacion=" + codAplicacion
                + ", codRol=" + codRol + "]";
    }

}
