/**
 * 
 */
package pvt.auna.fcompleja.model.api.request;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class UsuarioSeguridadRequest {
	
	private String codAplicacion;
	private String usuario;
	private String nombres;
	private String apePate;
	private String apeMate;
	private String codRol;
	
	public String getCodRol() {
		return codRol;
	}



	public void setCodRol(String codRol) {
		this.codRol = codRol;
	}



	
	
	

	/**
	 * 
	 */
	public UsuarioSeguridadRequest() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * @return the codAplicacion
	 */
	public String getCodAplicacion() {
		return codAplicacion;
	}



	/**
	 * @param codAplicacion the codAplicacion to set
	 */
	public void setCodAplicacion(String codAplicacion) {
		this.codAplicacion = codAplicacion;
	}



	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}



	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}



	/**
	 * @return the nombres
	 */
	public String getNombres() {
		return nombres;
	}



	/**
	 * @param nombres the nombres to set
	 */
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}



	/**
	 * @return the apePate
	 */
	public String getApePate() {
		return apePate;
	}



	/**
	 * @param apePate the apePate to set
	 */
	public void setApePate(String apePate) {
		this.apePate = apePate;
	}


	/**
	 * @return the apeMate
	 */
	public String getApeMate() {
		return apeMate;
	}
	

	/**
	 * @param apeMate the apeMate to set
	 */
	public void setApeMate(String apeMate) {
		this.apeMate = apeMate;
	}


	@Override
	public String toString() {
		return "UsuarioSeguridadRequest [codAplicacion=" + codAplicacion + ", usuario=" + usuario + ", nombres="
				+ nombres + ", apePate=" + apePate + ", apeMate=" + apeMate + ", codRol=" + codRol + "]";
	}
	
	
}
