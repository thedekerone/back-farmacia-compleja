package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.UsuarioFarmaciaBean;

public class UsuarioRowMapper implements RowMapper<UsuarioFarmaciaBean> {

    @Override
    public UsuarioFarmaciaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
    	UsuarioFarmaciaBean bean = new UsuarioFarmaciaBean();
        bean.setCodUsuario(rs.getInt("COD_PARTICIPANTE"));
        bean.setNombreApellido(rs.getString("NOMBRE_APELLIDO"));
        bean.setCorreo(rs.getString("CORREO_ELECTRONICO"));
        return bean;
    }
}
