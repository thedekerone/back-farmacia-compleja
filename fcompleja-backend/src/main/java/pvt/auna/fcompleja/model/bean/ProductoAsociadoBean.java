/**
 * 
 */
package pvt.auna.fcompleja.model.bean;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ProductoAsociadoBean {
	
	private Integer codigoMac;
	private String codigoProducto;
	private String  descripcionGenerica;
	private String  nombreComercial;
	private Integer codigoLaboratorio;
	private String  laboratorio;
	private Integer codigoEstado;
	private String  estado;
	/**
	 * 
	 */
	public ProductoAsociadoBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param codigoMac
	 * @param codigoProducto
	 * @param descripcionGenerica
	 * @param nombreComercial
	 * @param codigoLaboratorio
	 * @param laboratorio
	 * @param codigoEstado
	 * @param estado
	 */
	public ProductoAsociadoBean(Integer codigoMac, String codigoProducto, String descripcionGenerica,
			String nombreComercial, Integer codigoLaboratorio, String laboratorio, Integer codigoEstado,
			String estado) {
		super();
		this.codigoMac = codigoMac;
		this.codigoProducto = codigoProducto;
		this.descripcionGenerica = descripcionGenerica;
		this.nombreComercial = nombreComercial;
		this.codigoLaboratorio = codigoLaboratorio;
		this.laboratorio = laboratorio;
		this.codigoEstado = codigoEstado;
		this.estado = estado;
	}
	/**
	 * @return the codigoMac
	 */
	public Integer getCodigoMac() {
		return codigoMac;
	}
	/**
	 * @param codigoMac the codigoMac to set
	 */
	public void setCodigoMac(Integer codigoMac) {
		this.codigoMac = codigoMac;
	}
	/**
	 * @return the codigoProducto
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * @param codigoProducto the codigoProducto to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * @return the descripcionGenerica
	 */
	public String getDescripcionGenerica() {
		return descripcionGenerica;
	}
	/**
	 * @param descripcionGenerica the descripcionGenerica to set
	 */
	public void setDescripcionGenerica(String descripcionGenerica) {
		this.descripcionGenerica = descripcionGenerica;
	}
	/**
	 * @return the nombreComercial
	 */
	public String getNombreComercial() {
		return nombreComercial;
	}
	/**
	 * @param nombreComercial the nombreComercial to set
	 */
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	/**
	 * @return the codigoLaboratorio
	 */
	public Integer getCodigoLaboratorio() {
		return codigoLaboratorio;
	}
	/**
	 * @param codigoLaboratorio the codigoLaboratorio to set
	 */
	public void setCodigoLaboratorio(Integer codigoLaboratorio) {
		this.codigoLaboratorio = codigoLaboratorio;
	}
	/**
	 * @return the laboratorio
	 */
	public String getLaboratorio() {
		return laboratorio;
	}
	/**
	 * @param laboratorio the laboratorio to set
	 */
	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}
	/**
	 * @return the codigoEstado
	 */
	public Integer getCodigoEstado() {
		return codigoEstado;
	}
	/**
	 * @param codigoEstado the codigoEstado to set
	 */
	public void setCodigoEstado(Integer codigoEstado) {
		this.codigoEstado = codigoEstado;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductoAsociadoBean [codigoMac=" + codigoMac + ", codigoProducto=" + codigoProducto
				+ ", descripcionGenerica=" + descripcionGenerica + ", nombreComercial=" + nombreComercial
				+ ", codigoLaboratorio=" + codigoLaboratorio + ", laboratorio=" + laboratorio + ", codigoEstado="
				+ codigoEstado + ", estado=" + estado + ", toString()=" + super.toString() + "]";
	}
	
	

}
