package pvt.auna.fcompleja.model.api.request;

import pvt.auna.fcompleja.model.api.request.evaluacion.IndicacionCriterioRequest;

public class ListaIndicadorRequest {

	private Integer codSolEvaluacion;
	private Integer codMac;
	private Integer codGrpDiag;
	private Integer codigoMedico;
	private Integer codigoIndicacion;
	private String inclusion;
	private String exclusion;
	private String comentario;
	private Integer valCumpleChkListPer;
	private IndicacionCriterioRequest indicacionCriterio;
	private Integer codigoRolUsuario;
	private String fechaEstado;
	private Integer codigoUsuario;

	public Integer getCodSolEvaluacion() {
		return codSolEvaluacion;
	}

	public void setCodSolEvaluacion(Integer codSolEvaluacion) {
		this.codSolEvaluacion = codSolEvaluacion;
	}

	public Integer getCodMac() {
		return codMac;
	}

	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}

	public Integer getCodGrpDiag() {
		return codGrpDiag;
	}

	public void setCodGrpDiag(Integer codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}

	public Integer getCodigoMedico() {
		return codigoMedico;
	}

	public void setCodigoMedico(Integer codigoMedico) {
		this.codigoMedico = codigoMedico;
	}

	public Integer getCodigoIndicacion() {
		return codigoIndicacion;
	}

	public void setCodigoIndicacion(Integer codigoIndicacion) {
		this.codigoIndicacion = codigoIndicacion;
	}

	public String getInclusion() {
		return inclusion;
	}

	public void setInclusion(String inclusion) {
		this.inclusion = inclusion;
	}

	public String getExclusion() {
		return exclusion;
	}

	public void setExclusion(String exclusion) {
		this.exclusion = exclusion;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Integer getValCumpleChkListPer() {
		return valCumpleChkListPer;
	}

	public void setValCumpleChkListPer(Integer valCumpleChkListPer) {
		this.valCumpleChkListPer = valCumpleChkListPer;
	}

	public IndicacionCriterioRequest getIndicacionCriterio() {
		return indicacionCriterio;
	}

	public void setIndicacionCriterio(IndicacionCriterioRequest indicacionCriterio) {
		this.indicacionCriterio = indicacionCriterio;
	}

	public Integer getCodigoRolUsuario() {
		return codigoRolUsuario;
	}

	public void setCodigoRolUsuario(Integer codigoRolUsuario) {
		this.codigoRolUsuario = codigoRolUsuario;
	}

	public String getFechaEstado() {
		return fechaEstado;
	}

	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	@Override
	public String toString() {
		return "ListaIndicadorRequest [codSolEvaluacion=" + codSolEvaluacion + ", codMac=" + codMac + ", codGrpDiag="
				+ codGrpDiag + ", codigoMedico=" + codigoMedico + ", codigoIndicacion=" + codigoIndicacion
				+ ", inclusion=" + inclusion + ", exclusion=" + exclusion + ", comentario=" + comentario
				+ ", valCumpleChkListPer=" + valCumpleChkListPer + ", indicacionCriterio=" + indicacionCriterio
				+ ", codigoRolUsuario=" + codigoRolUsuario + ", fechaEstado=" + fechaEstado + ", codigoUsuario="
				+ codigoUsuario + "]";
	}

}
