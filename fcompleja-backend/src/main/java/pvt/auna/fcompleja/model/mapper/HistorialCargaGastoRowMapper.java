package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.HistorialCargaGastosBean;


public class HistorialCargaGastoRowMapper implements RowMapper<HistorialCargaGastosBean>{
	
    @Override
    public HistorialCargaGastosBean mapRow(ResultSet rs, int i) throws SQLException {
    	HistorialCargaGastosBean o = new HistorialCargaGastosBean();
        o.setCodigoControlGasto(rs.getInt("COD_HIST_CONTROL_GASTO"));
    	o.setFechaCarga(rs.getString("FECHA_HORA_CARGA"));
    	o.setHoraCarga(rs.getString("HORA_CARGA"));
    	o.setResponsableCarga(rs.getInt("RESPONSABLE_CARGA"));
        o.setEstadoCarga(rs.getInt("ESTADO_CARGA"));
        o.setDesEstadoCarga(rs.getString("DES_ESTADO_CARGA"));
        o.setRegistroTotal(rs.getInt("REGISTRO_TOTAL"));
        o.setRegistroCargado(rs.getInt("REGISTRO_CARGADO"));
        o.setRegistroError(rs.getInt("REGISTRO_ERROR"));
        o.setVerLog(rs.getInt("VER_LOG"));
        o.setDescarga(rs.getInt("DESCARGA"));
        o.setCodArchivoLog(rs.getInt("COD_ARCHIVO_LOG"));
        o.setCodArchivoDescarga(rs.getInt("COD_ARCHIVO_DESCARGA"));
        return o;
    }	

}



