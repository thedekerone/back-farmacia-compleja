package pvt.auna.fcompleja.model.api.request.evaluacion;

import java.io.Serializable;
import java.util.ArrayList;

public class AntecedenteOncoPers implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/*ArrayList<AntecedenteOncoPersDet> items;
	
	public ArrayList<AntecedenteOncoPersDet> getItems() {
		return items;
	}
	public void setItems(ArrayList<AntecedenteOncoPersDet> items) {
		this.items = items;
	}*/
	
	Boolean aplica;
	String diagnostico;
	String fecha;
	public Boolean getAplica() {
		return aplica;
	}
	public void setAplica(Boolean aplica) {
		this.aplica = aplica;
	}
	public String getDiagnostico() {
		return diagnostico;
	}
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	

}
