package pvt.auna.fcompleja.model.api.request.monitoreo;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class EvolucionRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codEvolucion;
	private Integer nroEvolucion;
	private String nroDescEvolucion;
	private Long codMonitoreo;
	private Long codMac;
	private Integer pResEvolucion;
	private Long codHistLineaTrat;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fecMonitoreo;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fecProxMonitoreo;
	private Integer pMotivoInactivacion;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fecInactivacion;
	private String observacion;
	private Integer pTolerancia;
	private Integer pToxicidad;
	private Integer pGrado;
	private Integer pRespClinica;
	private Integer pAtenAlerta;
	private char existeToxicidad;
	private String cadenaMarcadores;
	private List<EvolucionMarcadorRequest> listaMarcadores;
	private Integer estado;
	private String usuarioCrea;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaCrea;
	private String usuarioModif;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaModif;
	private Integer pEstadoMonitoreo;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fecUltimoConsumo;
	private Long codSolEvaluacion;

	private String DescCodSolEvaluacion;

	public EvolucionRequest() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodEvolucion() {
		return codEvolucion;
	}

	public void setCodEvolucion(Long codEvolucion) {
		this.codEvolucion = codEvolucion;
	}

	public Integer getNroEvolucion() {
		return nroEvolucion;
	}

	public void setNroEvolucion(Integer nroEvolucion) {
		this.nroEvolucion = nroEvolucion;
	}

	public String getNroDescEvolucion() {
		return nroDescEvolucion;
	}

	public void setNroDescEvolucion(String nroDescEvolucion) {
		this.nroDescEvolucion = nroDescEvolucion;
	}

	public Long getCodMonitoreo() {
		return codMonitoreo;
	}

	public void setCodMonitoreo(Long codMonitoreo) {
		this.codMonitoreo = codMonitoreo;
	}

	public Long getCodMac() {
		return codMac;
	}

	public void setCodMac(Long codMac) {
		this.codMac = codMac;
	}

	public Integer getpResEvolucion() {
		return pResEvolucion;
	}

	public void setpResEvolucion(Integer pResEvolucion) {
		this.pResEvolucion = pResEvolucion;
	}

	public Long getCodHistLineaTrat() {
		return codHistLineaTrat;
	}

	public void setCodHistLineaTrat(Long codHistLineaTrat) {
		this.codHistLineaTrat = codHistLineaTrat;
	}

	public Date getFecMonitoreo() {
		return fecMonitoreo;
	}

	public void setFecMonitoreo(Date fecMonitoreo) {
		this.fecMonitoreo = fecMonitoreo;
	}

	public Date getFecProxMonitoreo() {
		return fecProxMonitoreo;
	}

	public void setFecProxMonitoreo(Date fecProxMonitoreo) {
		this.fecProxMonitoreo = fecProxMonitoreo;
	}

	public Integer getpMotivoInactivacion() {
		return pMotivoInactivacion;
	}

	public void setpMotivoInactivacion(Integer pMotivoInactivacion) {
		this.pMotivoInactivacion = pMotivoInactivacion;
	}

	public Date getFecInactivacion() {
		return fecInactivacion;
	}

	public void setFecInactivacion(Date fecInactivacion) {
		this.fecInactivacion = fecInactivacion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Integer getpTolerancia() {
		return pTolerancia;
	}

	public void setpTolerancia(Integer pTolerancia) {
		this.pTolerancia = pTolerancia;
	}

	public Integer getpToxicidad() {
		return pToxicidad;
	}

	public void setpToxicidad(Integer pToxicidad) {
		this.pToxicidad = pToxicidad;
	}

	public Integer getpGrado() {
		return pGrado;
	}

	public void setpGrado(Integer pGrado) {
		this.pGrado = pGrado;
	}

	public Integer getpRespClinica() {
		return pRespClinica;
	}

	public void setpRespClinica(Integer pRespClinica) {
		this.pRespClinica = pRespClinica;
	}

	public Integer getpAtenAlerta() {
		return pAtenAlerta;
	}

	public void setpAtenAlerta(Integer pAtenAlerta) {
		this.pAtenAlerta = pAtenAlerta;
	}

	public char getExisteToxicidad() {
		return existeToxicidad;
	}

	public void setExisteToxicidad(char existeToxicidad) {
		this.existeToxicidad = existeToxicidad;
	}

	public List<EvolucionMarcadorRequest> getListaMarcadores() {
		return listaMarcadores;
	}

	public void setListaMarcadores(List<EvolucionMarcadorRequest> listaMarcadores) {
		this.listaMarcadores = listaMarcadores;
	}

	public String getCadenaMarcadores() {
		return cadenaMarcadores;
	}

	public void setCadenaMarcadores(String cadenaMarcadores) {
		this.cadenaMarcadores = cadenaMarcadores;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getUsuarioCrea() {
		return usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public Date getFechaCrea() {
		return fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public String getUsuarioModif() {
		return usuarioModif;
	}

	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}

	public Date getFechaModif() {
		return fechaModif;
	}

	public void setFechaModif(Date fechaModif) {
		this.fechaModif = fechaModif;
	}

	public Integer getpEstadoMonitoreo() {
		return pEstadoMonitoreo;
	}

	public void setpEstadoMonitoreo(Integer pEstadoMonitoreo) {
		this.pEstadoMonitoreo = pEstadoMonitoreo;
	}

	public Date getFecUltimoConsumo() {
		return fecUltimoConsumo;
	}

	public void setFecUltimoConsumo(Date fecUltimoConsumo) {
		this.fecUltimoConsumo = fecUltimoConsumo;
	}

	public Long getCodSolEvaluacion() {
		return codSolEvaluacion;
	}

	public void setCodSolEvaluacion(Long codSolEvaluacion) {
		this.codSolEvaluacion = codSolEvaluacion;
	}

	public String getDescCodSolEvaluacion() {
		return DescCodSolEvaluacion;
	}

	public void setDescCodSolEvaluacion(String descCodSolEvaluacion) {
		DescCodSolEvaluacion = descCodSolEvaluacion;
	}

	@Override
	public String toString() {
		return "EvolucionRequest [codEvolucion=" + codEvolucion + ", nroEvolucion=" + nroEvolucion
				+ ", nroDescEvolucion=" + nroDescEvolucion + ", codMonitoreo=" + codMonitoreo + ", codMac=" + codMac
				+ ", pResEvolucion=" + pResEvolucion + ", codHistLineaTrat=" + codHistLineaTrat + ", fecMonitoreo="
				+ fecMonitoreo + ", fecProxMonitoreo=" + fecProxMonitoreo + ", pMotivoInactivacion="
				+ pMotivoInactivacion + ", fecInactivacion=" + fecInactivacion + ", observacion=" + observacion
				+ ", pTolerancia=" + pTolerancia + ", pToxicidad=" + pToxicidad + ", pGrado=" + pGrado
				+ ", pRespClinica=" + pRespClinica + ", pAtenAlerta=" + pAtenAlerta + ", existeToxicidad="
				+ existeToxicidad + ", cadenaMarcadores=" + cadenaMarcadores + ", listaMarcadores=" + listaMarcadores
				+ ", estado=" + estado + ", usuarioCrea=" + usuarioCrea + ", fechaCrea=" + fechaCrea + ", usuarioModif="
				+ usuarioModif + ", fechaModif=" + fechaModif + ", pEstadoMonitoreo=" + pEstadoMonitoreo
				+ ", fecUltimoConsumo=" + fecUltimoConsumo + ", codSolEvaluacion=" + codSolEvaluacion
				+ ", DescCodSolEvaluacion=" + DescCodSolEvaluacion + "]";
	}

}
