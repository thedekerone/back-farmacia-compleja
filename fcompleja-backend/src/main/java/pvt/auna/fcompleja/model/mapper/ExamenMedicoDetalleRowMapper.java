/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.ExamenMedicoBean;
import pvt.auna.fcompleja.model.bean.ExamenMedicoDetalleBean;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ExamenMedicoDetalleRowMapper implements RowMapper<ExamenMedicoDetalleBean>{

	@Override
	public ExamenMedicoDetalleBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ExamenMedicoDetalleBean examenMedicoDetalle = new ExamenMedicoDetalleBean();
		examenMedicoDetalle.setCodExamenMed(rs.getInt("cod_examen_med"));
		examenMedicoDetalle.setCodExamenMedDet(rs.getInt("cod_examen_med_det"));
		examenMedicoDetalle.setCodigoTipoIngresoResultado(rs.getInt("cod_tipo_ingreso_res"));
		examenMedicoDetalle.setTipoIngresoResultado(rs.getString("tipo_ingreso_res"));
		examenMedicoDetalle.setUnidadMedida(rs.getString("unidad_medida"));
		examenMedicoDetalle.setRango(rs.getString("rango"));
		examenMedicoDetalle.setCodigoEstado(rs.getInt("cod_estado_exam_med_det"));
		examenMedicoDetalle.setEstado(rs.getString("estado_exam_med_det"));
		examenMedicoDetalle.setRangoMinimo(rs.getInt("rango_minimo"));
		examenMedicoDetalle.setRangoMaximo(rs.getInt("rango_maximo"));
		examenMedicoDetalle.setValorFijo(rs.getString("valor_fijo"));
		
		return examenMedicoDetalle;
	}

}
