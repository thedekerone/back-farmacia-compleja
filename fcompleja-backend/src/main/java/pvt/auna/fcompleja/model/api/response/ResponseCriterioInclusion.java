package pvt.auna.fcompleja.model.api.response;

public class ResponseCriterioInclusion {
	private Number NUCOD_CRITERIO_INCLU;
	private Number COD_CHKLIST_INDI;
	private String COD_CRITERIO_INCLU_LARGO;
	private String DESCRIPCION;
	private Number ORDEN; 
	private Number P_ESTADO;
	
	public ResponseCriterioInclusion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Number getNUCOD_CRITERIO_INCLU() {
		return NUCOD_CRITERIO_INCLU;
	}

	public void setNUCOD_CRITERIO_INCLU(Number nUCOD_CRITERIO_INCLU) {
		NUCOD_CRITERIO_INCLU = nUCOD_CRITERIO_INCLU;
	}

	public Number getCOD_CHKLIST_INDI() {
		return COD_CHKLIST_INDI;
	}

	public void setCOD_CHKLIST_INDI(Number cOD_CHKLIST_INDI) {
		COD_CHKLIST_INDI = cOD_CHKLIST_INDI;
	}

	public String getCOD_CRITERIO_INCLU_LARGO() {
		return COD_CRITERIO_INCLU_LARGO;
	}

	public void setCOD_CRITERIO_INCLU_LARGO(String cOD_CRITERIO_INCLU_LARGO) {
		COD_CRITERIO_INCLU_LARGO = cOD_CRITERIO_INCLU_LARGO;
	}

	public String getDESCRIPCION() {
		return DESCRIPCION;
	}

	public void setDESCRIPCION(String dESCRIPCION) {
		DESCRIPCION = dESCRIPCION;
	}

	public Number getORDEN() {
		return ORDEN;
	}

	public void setORDEN(Number oRDEN) {
		ORDEN = oRDEN;
	}

	public Number getP_ESTADO() {
		return P_ESTADO;
	}

	public void setP_ESTADO(Number p_ESTADO) {
		P_ESTADO = p_ESTADO;
	}

	@Override
	public String toString() {
		return "ResponseCriterioInclusion [NUCOD_CRITERIO_INCLU=" + NUCOD_CRITERIO_INCLU + ", COD_CRITERIO_INCLU_LARGO=" + COD_CRITERIO_INCLU_LARGO +
				", DESCRIPCION=" + DESCRIPCION + ", ORDEN=" + ORDEN + 
				", P_ESTADO=" + P_ESTADO + "]";
	};
}
