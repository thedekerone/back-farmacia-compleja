package pvt.auna.fcompleja.model.bean;

public class UsuarioBean {

	private Integer codAplicacion;
	private Integer codSolben;
	private Integer codRol;
	private String nombreRol;
	private String codUsuarioCodRol;
	private Integer codUsuario;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombre;
	private String correo;
	private String numDocumento;
	private String usuario;
	private String nombreApellido;

	public Integer getCodAplicacion() {
		return codAplicacion;
	}

	public void setCodAplicacion(Integer codAplicacion) {
		this.codAplicacion = codAplicacion;
	}

	public Integer getCodSolben() {
		return codSolben;
	}

	public void setCodSolben(Integer codSolben) {
		this.codSolben = codSolben;
	}

	public Integer getCodRol() {
		return codRol;
	}

	public void setCodRol(Integer codRol) {
		this.codRol = codRol;
	}

	public String getNombreRol() {
		return nombreRol;
	}

	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}

	public String getCodUsuarioCodRol() {
		return codUsuarioCodRol;
	}

	public void setCodUsuarioCodRol(String codUsuarioCodRol) {
		this.codUsuarioCodRol = codUsuarioCodRol;
	}

	public Integer getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNumDocumento() {
		return numDocumento;
	}

	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNombreApellido() {
		return this.apellidoPaterno + " " + this.apellidoMaterno + ", " + this.nombre;
	}

	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}

	@Override
	public String toString() {
		return "UsuarioBean [codAplicacion=" + codAplicacion + ", codSolben=" + codSolben + ", codRol=" + codRol
				+ ", nombreRol=" + nombreRol + ", codUsuarioCodRol=" + codUsuarioCodRol + ", codUsuario=" + codUsuario
				+ ", apellidoPaterno=" + apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno + ", nombre=" + nombre
				+ ", correo=" + correo + ", numDocumento=" + numDocumento + ", usuario=" + usuario + ", nombreApellido="
				+ nombreApellido + "]";
	}

}
