package pvt.auna.fcompleja.model.api.request.monitoreo;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class SegEjecutivoRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codSegEjecutivo;
	private Long codMonitoreo;
	private Long codEjecutivoMonitoreo;
	private String nomEjecutivoMonitoreo;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaRegistro;
	private Integer pEstadoSeguimiento;
	private String descEstadoSeguimiento;
	private String detalleEvento;
	private Integer vistoRespMonitoreo;
	private String usuariocrea;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechacrea;
	private String usuarioModif;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaModif;
	private Integer pEstadoMonitoreo;

	public SegEjecutivoRequest() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodSegEjecutivo() {
		return codSegEjecutivo;
	}

	public void setCodSegEjecutivo(Long codSegEjecutivo) {
		this.codSegEjecutivo = codSegEjecutivo;
	}

	public Long getCodMonitoreo() {
		return codMonitoreo;
	}

	public void setCodMonitoreo(Long codMonitoreo) {
		this.codMonitoreo = codMonitoreo;
	}

	public Long getCodEjecutivoMonitoreo() {
		return codEjecutivoMonitoreo;
	}

	public void setCodEjecutivoMonitoreo(Long codEjecutivoMonitoreo) {
		this.codEjecutivoMonitoreo = codEjecutivoMonitoreo;
	}

	public String getNomEjecutivoMonitoreo() {
		return nomEjecutivoMonitoreo;
	}

	public void setNomEjecutivoMonitoreo(String nomEjecutivoMonitoreo) {
		this.nomEjecutivoMonitoreo = nomEjecutivoMonitoreo;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Integer getpEstadoSeguimiento() {
		return pEstadoSeguimiento;
	}

	public void setpEstadoSeguimiento(Integer pEstadoSeguimiento) {
		this.pEstadoSeguimiento = pEstadoSeguimiento;
	}

	public String getDescEstadoSeguimiento() {
		return descEstadoSeguimiento;
	}

	public void setDescEstadoSeguimiento(String descEstadoSeguimiento) {
		this.descEstadoSeguimiento = descEstadoSeguimiento;
	}

	public String getDetalleEvento() {
		return detalleEvento;
	}

	public void setDetalleEvento(String detalleEvento) {
		this.detalleEvento = detalleEvento;
	}

	public Integer getVistoRespMonitoreo() {
		return vistoRespMonitoreo;
	}

	public void setVistoRespMonitoreo(Integer vistoRespMonitoreo) {
		this.vistoRespMonitoreo = vistoRespMonitoreo;
	}

	public String getUsuariocrea() {
		return usuariocrea;
	}

	public void setUsuariocrea(String usuariocrea) {
		this.usuariocrea = usuariocrea;
	}

	public Date getFechacrea() {
		return fechacrea;
	}

	public void setFechacrea(Date fechacrea) {
		this.fechacrea = fechacrea;
	}

	public String getUsuarioModif() {
		return usuarioModif;
	}

	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}

	public Date getFechaModif() {
		return fechaModif;
	}

	public void setFechaModif(Date fechaModif) {
		this.fechaModif = fechaModif;
	}

	public Integer getpEstadoMonitoreo() {
		return pEstadoMonitoreo;
	}

	public void setpEstadoMonitoreo(Integer pEstadoMonitoreo) {
		this.pEstadoMonitoreo = pEstadoMonitoreo;
	}

	@Override
	public String toString() {
		return "SegEjecutivoRequest [codSegEjecutivo=" + codSegEjecutivo + ", codMonitoreo=" + codMonitoreo
				+ ", codEjecutivoMonitoreo=" + codEjecutivoMonitoreo + ", nomEjecutivoMonitoreo="
				+ nomEjecutivoMonitoreo + ", fechaRegistro=" + fechaRegistro + ", pEstadoSeguimiento="
				+ pEstadoSeguimiento + ", descEstadoSeguimiento=" + descEstadoSeguimiento + ", detalleEvento="
				+ detalleEvento + ", vistoRespMonitoreo=" + vistoRespMonitoreo + ", usuariocrea=" + usuariocrea
				+ ", fechacrea=" + fechacrea + ", usuarioModif=" + usuarioModif + ", fechaModif=" + fechaModif
				+ ", pEstadoMonitoreo=" + pEstadoMonitoreo + "]";
	}

}
