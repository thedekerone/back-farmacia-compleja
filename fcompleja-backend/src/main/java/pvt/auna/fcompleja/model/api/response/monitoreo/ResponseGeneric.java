package pvt.auna.fcompleja.model.api.response.monitoreo;

import java.util.List;

import pvt.auna.fcompleja.model.api.AudiResponse;

public class ResponseGeneric {
	private AudiResponse audiResponse;
	private List<DetCorreoParticipanteResponse> dataList;

	public AudiResponse getAudiResponse() {
		return audiResponse;
	}

	public void setAudiResponse(AudiResponse audiResponse) {
		this.audiResponse = audiResponse;
	}

	public List<DetCorreoParticipanteResponse> getDataList() {
		return dataList;
	}

	public void setDataList(List<DetCorreoParticipanteResponse> dataList) {
		this.dataList = dataList;
	}

	@Override
	public String toString() {
		return "ResponseGeneric [audiResponse=" + audiResponse + ", dataList=" + dataList + "]";
	}

}
