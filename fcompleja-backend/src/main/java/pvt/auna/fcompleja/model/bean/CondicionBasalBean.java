package pvt.auna.fcompleja.model.bean;

public class CondicionBasalBean {

	private String antecedentesImp;
	private String ecog;
	private String existeToxicidad;
	private String tipoToxicidad;
	
	public String getAntecedentesImp() {
		return antecedentesImp;
	}
	public void setAntecedentesImp(String antecedentesImp) {
		this.antecedentesImp = antecedentesImp;
	}
	public String getEcog() {
		return ecog;
	}
	public void setEcog(String ecog) {
		this.ecog = ecog;
	}
	public String getExisteToxicidad() {
		return existeToxicidad;
	}
	public void setExisteToxicidad(String existeToxicidad) {
		this.existeToxicidad = existeToxicidad;
	}
	public String getTipoToxicidad() {
		return tipoToxicidad;
	}
	public void setTipoToxicidad(String tipoToxicidad) {
		this.tipoToxicidad = tipoToxicidad;
	}
	@Override
	public String toString() {
		return "CondicionBasalBean [antecedentesImp=" + antecedentesImp + ", ecog=" + ecog + ", existeToxicidad="
				+ existeToxicidad + ", tipoToxicidad=" + tipoToxicidad + "]";
	}
	
	

}
