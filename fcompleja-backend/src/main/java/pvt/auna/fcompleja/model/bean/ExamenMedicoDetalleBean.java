/**
 * 
 */
package pvt.auna.fcompleja.model.bean;

import java.util.Date;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ExamenMedicoDetalleBean {
	
	private Integer codExamenMedDet;
	private Integer codExamenMed;
	private Integer codigoTipoIngresoResultado;
	private String  tipoIngresoResultado;
	private String	unidadMedida;
	private String 	rango;
	private String 	valorFijo;
	private Integer codigoEstado;
	private String	estado;
	private Integer	rangoMinimo;
	private	Integer	rangoMaximo;
	private String 	usuarioCrea;
	private	Date	fechaCrea;
	private	String 	usuarioModif;
	private	Date	fechaModif;
	/**
	 * 
	 */
	public ExamenMedicoDetalleBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param codExamenMedDet
	 * @param codExamenMed
	 * @param codigoTipoIngresoResultado
	 * @param tipoIngresoResultado
	 * @param unidadMedida
	 * @param rango
	 * @param valorFijo
	 * @param codigoEstado
	 * @param estado
	 * @param rangoMinimo
	 * @param rangoMaximo
	 * @param usuarioCrea
	 * @param fechaCrea
	 * @param usuarioModif
	 * @param fechaModif
	 */
	public ExamenMedicoDetalleBean(Integer codExamenMedDet, Integer codExamenMed, Integer codigoTipoIngresoResultado,
			String tipoIngresoResultado, String unidadMedida, String rango, String valorFijo, Integer codigoEstado,
			String estado, Integer rangoMinimo, Integer rangoMaximo, String usuarioCrea, Date fechaCrea,
			String usuarioModif, Date fechaModif) {
		super();
		this.codExamenMedDet = codExamenMedDet;
		this.codExamenMed = codExamenMed;
		this.codigoTipoIngresoResultado = codigoTipoIngresoResultado;
		this.tipoIngresoResultado = tipoIngresoResultado;
		this.unidadMedida = unidadMedida;
		this.rango = rango;
		this.valorFijo = valorFijo;
		this.codigoEstado = codigoEstado;
		this.estado = estado;
		this.rangoMinimo = rangoMinimo;
		this.rangoMaximo = rangoMaximo;
		this.usuarioCrea = usuarioCrea;
		this.fechaCrea = fechaCrea;
		this.usuarioModif = usuarioModif;
		this.fechaModif = fechaModif;
	}
	/**
	 * @return the codExamenMedDet
	 */
	public Integer getCodExamenMedDet() {
		return codExamenMedDet;
	}
	/**
	 * @param codExamenMedDet the codExamenMedDet to set
	 */
	public void setCodExamenMedDet(Integer codExamenMedDet) {
		this.codExamenMedDet = codExamenMedDet;
	}
	/**
	 * @return the codExamenMed
	 */
	public Integer getCodExamenMed() {
		return codExamenMed;
	}
	/**
	 * @param codExamenMed the codExamenMed to set
	 */
	public void setCodExamenMed(Integer codExamenMed) {
		this.codExamenMed = codExamenMed;
	}
	/**
	 * @return the codigoTipoIngresoResultado
	 */
	public Integer getCodigoTipoIngresoResultado() {
		return codigoTipoIngresoResultado;
	}
	/**
	 * @param codigoTipoIngresoResultado the codigoTipoIngresoResultado to set
	 */
	public void setCodigoTipoIngresoResultado(Integer codigoTipoIngresoResultado) {
		this.codigoTipoIngresoResultado = codigoTipoIngresoResultado;
	}
	/**
	 * @return the tipoIngresoResultado
	 */
	public String getTipoIngresoResultado() {
		return tipoIngresoResultado;
	}
	/**
	 * @param tipoIngresoResultado the tipoIngresoResultado to set
	 */
	public void setTipoIngresoResultado(String tipoIngresoResultado) {
		this.tipoIngresoResultado = tipoIngresoResultado;
	}
	/**
	 * @return the unidadMedida
	 */
	public String getUnidadMedida() {
		return unidadMedida;
	}
	/**
	 * @param unidadMedida the unidadMedida to set
	 */
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	/**
	 * @return the rango
	 */
	public String getRango() {
		return rango;
	}
	/**
	 * @param rango the rango to set
	 */
	public void setRango(String rango) {
		this.rango = rango;
	}
	/**
	 * @return the valorFijo
	 */
	public String getValorFijo() {
		return valorFijo;
	}
	/**
	 * @param valorFijo the valorFijo to set
	 */
	public void setValorFijo(String valorFijo) {
		this.valorFijo = valorFijo;
	}
	/**
	 * @return the codigoEstado
	 */
	public Integer getCodigoEstado() {
		return codigoEstado;
	}
	/**
	 * @param codigoEstado the codigoEstado to set
	 */
	public void setCodigoEstado(Integer codigoEstado) {
		this.codigoEstado = codigoEstado;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the rangoMinimo
	 */
	public Integer getRangoMinimo() {
		return rangoMinimo;
	}
	/**
	 * @param rangoMinimo the rangoMinimo to set
	 */
	public void setRangoMinimo(Integer rangoMinimo) {
		this.rangoMinimo = rangoMinimo;
	}
	/**
	 * @return the rangoMaximo
	 */
	public Integer getRangoMaximo() {
		return rangoMaximo;
	}
	/**
	 * @param rangoMaximo the rangoMaximo to set
	 */
	public void setRangoMaximo(Integer rangoMaximo) {
		this.rangoMaximo = rangoMaximo;
	}
	/**
	 * @return the usuarioCrea
	 */
	public String getUsuarioCrea() {
		return usuarioCrea;
	}
	/**
	 * @param usuarioCrea the usuarioCrea to set
	 */
	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}
	/**
	 * @return the fechaCrea
	 */
	public Date getFechaCrea() {
		return fechaCrea;
	}
	/**
	 * @param fechaCrea the fechaCrea to set
	 */
	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}
	/**
	 * @return the usuarioModif
	 */
	public String getUsuarioModif() {
		return usuarioModif;
	}
	/**
	 * @param usuarioModif the usuarioModif to set
	 */
	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}
	/**
	 * @return the fechaModif
	 */
	public Date getFechaModif() {
		return fechaModif;
	}
	/**
	 * @param fechaModif the fechaModif to set
	 */
	public void setFechaModif(Date fechaModif) {
		this.fechaModif = fechaModif;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ExamenMedicoDetalleBean [codExamenMedDet=" + codExamenMedDet + ", codExamenMed=" + codExamenMed
				+ ", codigoTipoIngresoResultado=" + codigoTipoIngresoResultado + ", tipoIngresoResultado="
				+ tipoIngresoResultado + ", unidadMedida=" + unidadMedida + ", rango=" + rango + ", valorFijo="
				+ valorFijo + ", codigoEstado=" + codigoEstado + ", estado=" + estado + ", rangoMinimo=" + rangoMinimo
				+ ", rangoMaximo=" + rangoMaximo + ", usuarioCrea=" + usuarioCrea + ", fechaCrea=" + fechaCrea
				+ ", usuarioModif=" + usuarioModif + ", fechaModif=" + fechaModif + ", toString()=" + super.toString()
				+ "]";
	}
	
	

}
