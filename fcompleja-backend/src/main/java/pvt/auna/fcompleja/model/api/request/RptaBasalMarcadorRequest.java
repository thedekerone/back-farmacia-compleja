package pvt.auna.fcompleja.model.api.request;

public class RptaBasalMarcadorRequest {

    private Integer codMac;
    private String grupoDiagnostico ;


    public Integer getCodMac() {
        return codMac;
    }

    public void setCodMac(Integer codMac) {
        this.codMac = codMac;
    }

    public String getGrupoDiagnostico() {
        return grupoDiagnostico;
    }

    public void setGrupoDiagnostico(String grupoDiagnostico) {
        this.grupoDiagnostico = grupoDiagnostico;
    }
}
