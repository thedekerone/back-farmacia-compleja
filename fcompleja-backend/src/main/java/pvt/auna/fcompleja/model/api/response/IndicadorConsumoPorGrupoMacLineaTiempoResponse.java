/**
 * 
 */
package pvt.auna.fcompleja.model.api.response;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class IndicadorConsumoPorGrupoMacLineaTiempoResponse extends IndicadorConsumoPorGrupoMacLineaResponse{

	private String tiempoUso;
	
	/**
	 * 
	 */
	public IndicadorConsumoPorGrupoMacLineaTiempoResponse() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the tiempoUso
	 */
	public String getTiempoUso() {
		return tiempoUso;
	}

	/**
	 * @param tiempoUso the tiempoUso to set
	 */
	public void setTiempoUso(String tiempoUso) {
		this.tiempoUso = tiempoUso;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IndicadorConsumoPorGrupoMacLineaTiempoResponse [tiempoUso=" + tiempoUso + ", toString()="
				+ super.toString() + "]";
	}

	
	
}
