package pvt.auna.fcompleja.model.api;

import java.util.List;

public class RolResponsableResponse {

	private List<ListRolResponsableResponse> RolResponsable;
	private Long   codigoResultado;
	private String mensageResultado;
	
	public List<ListRolResponsableResponse> getRolResponsable() {
		return RolResponsable;
	}
	public void setRolResponsable(List<ListRolResponsableResponse> rolResponsable) {
		RolResponsable = rolResponsable;
	}
	public Long getCodigoResultado() {
		return codigoResultado;
	}
	public void setCodigoResultado(Long codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	public String getMensageResultado() {
		return mensageResultado;
	}
	public void setMensageResultado(String mensageResultado) {
		this.mensageResultado = mensageResultado;
	} 
	
	
}
