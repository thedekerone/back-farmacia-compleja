package pvt.auna.fcompleja.model.api.response;

import java.io.Serializable;
import java.util.Date;
import java.util.Formatter;

public class ListSolPreeliminarResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long codPre;
	private Date fechaPre;
	private String horaPre;
	private String codScgSolben;
	private Date fechaScgSolben;
	private String horaScgSolben;
	private String tipoScgSolben;
	private String estadoSol;
	private String medicoTratante;
	private String autorizador;

	private String codClinica;
	private String descClinica;
	private String codAfiliado;
	private String nombrePaciente;
	private String numeroCodPre;
	
	private String tipdoc;
	private String numdoc;
	
	public String getNumeroCodPre() {
		Formatter fmt = new Formatter();
		return String.valueOf(fmt.format("%010d", this.codPre));
	}

	public Long getCodPre() {
		return codPre;
	}

	public void setCodPre(Long codPre) {
		this.codPre = codPre;
	}

	public Date getFechaPre() {
		return fechaPre;
	}

	public void setFechaPre(Date fechaPre) {
		this.fechaPre = fechaPre;
	}

	public String getHoraPre() {
		return horaPre;
	}

	public void setHoraPre(String horaPre) {
		this.horaPre = horaPre;
	}

	public String getCodScgSolben() {
		return codScgSolben;
	}

	public void setCodScgSolben(String codScgSolben) {
		this.codScgSolben = codScgSolben;
	}

	public Date getFechaScgSolben() {
		return fechaScgSolben;
	}

	public void setFechaScgSolben(Date fechaScgSolben) {
		this.fechaScgSolben = fechaScgSolben;
	}

	public String getHoraScgSolben() {
		return horaScgSolben;
	}

	public void setHoraScgSolben(String horaScgSolben) {
		this.horaScgSolben = horaScgSolben;
	}

	public String getTipoScgSolben() {
		return tipoScgSolben;
	}

	public void setTipoScgSolben(String tipoScgSolben) {
		this.tipoScgSolben = tipoScgSolben;
	}

	public String getEstadoSol() {
		return estadoSol;
	}

	public void setEstadoSol(String estadoSol) {
		this.estadoSol = estadoSol;
	}

	public String getMedicoTratante() {
		return medicoTratante;
	}

	public void setMedicoTratante(String medicoTratante) {
		this.medicoTratante = medicoTratante;
	}

	public String getAutorizador() {
		return autorizador;
	}

	public void setAutorizador(String autorizador) {
		this.autorizador = autorizador;
	}

	public String getCodClinica() {
		return codClinica;
	}

	public void setCodClinica(String codClinica) {
		this.codClinica = codClinica;
	}

	public String getDescClinica() {
		if (descClinica != null) {
			return descClinica;
		} else {
			return "";
		}
	}

	public void setDescClinica(String descClinica) {
		if (descClinica != null) {
			this.descClinica = descClinica;
		} else {
			this.descClinica = "";
		}
	}

	public String getCodAfiliado() {
		return codAfiliado;
	}

	public void setCodAfiliado(String codAfiliado) {
		this.codAfiliado = codAfiliado;
	}

	public String getNombrePaciente() {
		if (nombrePaciente != null) {
			return nombrePaciente;
		} else {
			return "";
		}
	}

	public void setNombrePaciente(String nombrePaciente) {
		if (nombrePaciente != null) {
			this.nombrePaciente = nombrePaciente;
		} else {
			this.nombrePaciente = "";
		}
	}

	public String getTipdoc() {
		return tipdoc;
	}

	public void setTipdoc(String tipdoc) {
		this.tipdoc = tipdoc;
	}

	public String getNumdoc() {
		return numdoc;
	}

	public void setNumdoc(String numdoc) {
		this.numdoc = numdoc;
	}
	
	

}
