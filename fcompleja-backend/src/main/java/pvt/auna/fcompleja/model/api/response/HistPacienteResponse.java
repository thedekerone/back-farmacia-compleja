package pvt.auna.fcompleja.model.api.response;

public class HistPacienteResponse {

	private Integer codChekListReq;
	private Integer codContinuadorDoc;
	private Integer lineaTratamiento;
	private String descripcionMac;
	private Integer tipoDocumento;
	private String nombreTipoDocumento;
	private String descripcionDocumento;
	private String urlDescarga;
	private String fechaCarga;
	private Integer estadoDocumento;
	private String descripcionEstado;
	private Integer codArchivo;

	public Integer getCodChekListReq() {
		return codChekListReq;
	}

	public void setCodChekListReq(Integer codChekListReq) {
		this.codChekListReq = codChekListReq;
	}

	public Integer getCodContinuadorDoc() {
		return codContinuadorDoc;
	}

	public void setCodContinuadorDoc(Integer codContinuadorDoc) {
		this.codContinuadorDoc = codContinuadorDoc;
	}

	public Integer getLineaTratamiento() {
		return lineaTratamiento;
	}

	public void setLineaTratamiento(Integer lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}

	public String getDescripcionMac() {
		return descripcionMac;
	}

	public void setDescripcionMac(String descripcionMac) {
		this.descripcionMac = descripcionMac;
	}

	public Integer getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(Integer tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDescripcionDocumento() {
		return descripcionDocumento;
	}

	public void setDescripcionDocumento(String descripcionDocumento) {
		this.descripcionDocumento = descripcionDocumento;
	}

	public String getUrlDescarga() {
		return urlDescarga;
	}

	public void setUrlDescarga(String urlDescarga) {
		this.urlDescarga = urlDescarga;
	}

	public String getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(String fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public String getNombreTipoDocumento() {
		return nombreTipoDocumento;
	}

	public void setNombreTipoDocumento(String nombreTipoDocumento) {
		this.nombreTipoDocumento = nombreTipoDocumento;
	}

	public Integer getEstadoDocumento() {
		return estadoDocumento;
	}

	public void setEstadoDocumento(Integer estadoDocumento) {
		this.estadoDocumento = estadoDocumento;
	}

	public String getDescripcionEstado() {
		return descripcionEstado;
	}

	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}

	public Integer getCodArchivo() {
		return codArchivo;
	}

	public void setCodArchivo(Integer codArchivo) {
		this.codArchivo = codArchivo;
	}

}
