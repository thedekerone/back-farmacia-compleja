package pvt.auna.fcompleja.model.bean;

public class DetalleEvaluacionBean {
	private InfoSolbenBean solbenBean;
	private Integer flagVerInforme;
	private String  reportePdf;
	private Integer flagVerActaCmac;
	private String  reporteActaCmac;
	private Integer codRolLiderTum;
	private Integer codUsrLiderTum;
	private String  usrLiderTum;
	private Integer codArchFichaTec;
	private Integer codArchCompMed;
	
	public Integer getCodArchComplMed() {
		return codArchCompMed;
	}
	public void setCodArchComplMed(Integer codArchComplMed) {
		this.codArchCompMed = codArchComplMed;
	}
	public InfoSolbenBean getSolbenBean() {
		return solbenBean;
	}
	public void setSolbenBean(InfoSolbenBean solbenBean) {
		this.solbenBean = solbenBean;
	}
	public Integer getFlagVerInforme() {
		return flagVerInforme;
	}
	public void setFlagVerInforme(Integer flagVerInforme) {
		this.flagVerInforme = flagVerInforme;
	}
	public String getReportePdf() {
		return reportePdf;
	}
	public void setReportePdf(String reportePdf) {
		this.reportePdf = reportePdf;
	}
	public Integer getFlagVerActaCmac() {
		return flagVerActaCmac;
	}
	public void setFlagVerActaCmac(Integer flagVerActaCmac) {
		this.flagVerActaCmac = flagVerActaCmac;
	}
	public String getReporteActaCmac() {
		return reporteActaCmac;
	}
	public void setReporteActaCmac(String reporteActaCmac) {
		this.reporteActaCmac = reporteActaCmac;
	}
	public Integer getCodRolLiderTum() {
		return codRolLiderTum;
	}
	public void setCodRolLiderTum(Integer codRolLiderTum) {
		this.codRolLiderTum = codRolLiderTum;
	}
	public Integer getCodUsrLiderTum() {
		return codUsrLiderTum;
	}
	public void setCodUsrLiderTum(Integer codUsrLiderTum) {
		this.codUsrLiderTum = codUsrLiderTum;
	}
	public String getUsrLiderTum() {
		return usrLiderTum;
	}
	public void setUsrLiderTum(String usrLiderTum) {
		this.usrLiderTum = usrLiderTum;
	}
	public Integer getCodArchFichaTec() {
		return codArchFichaTec;
	}
	public void setCodArchFichaTec(Integer codArchFichaTec) {
		this.codArchFichaTec = codArchFichaTec;
	}
	
	@Override
	public String toString() {
		return "DetalleEvaluacionBean [solbenBean=" + solbenBean + ", flagVerInforme=" + flagVerInforme
				+ ", reportePdf=" + reportePdf + ", flagVerActaCmac=" + flagVerActaCmac + ", reporteActaCmac="
				+ reporteActaCmac + ", codRolLiderTum=" + codRolLiderTum + ", codUsrLiderTum=" + codUsrLiderTum
				+ ", usrLiderTum=" + usrLiderTum + ", codArchFichaTec=" + codArchFichaTec + ", codArchCompMed="
				+ codArchCompMed + "]";
	}
}
