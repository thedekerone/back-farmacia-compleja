package pvt.auna.fcompleja.model.bean;

public class LineaTratamientoBean {
	
	private Integer codPrefeInsti;
	private Integer nroCurso;
	private Integer tipoTumor;
	private Integer lugarProgresion;
	private Integer respAlcanzada;
	private Integer cumplePrefeInsti;
	private String  grabar;
	private String  descripcion;
	
	private String lineaTratamiento;
	private String fechaInicio;
	private String fechaFin;
	private String curso;
	private String respuestaAlcanzada;
	private String descLugarProgresion;
	private String motivoInactivacion;
	
	public Integer getCodPrefeInsti() {
		return codPrefeInsti;
	}
	public void setCodPrefeInsti(Integer codPrefeInsti) {
		this.codPrefeInsti = codPrefeInsti;
	}
	public Integer getNroCurso() {
		return nroCurso;
	}
	public void setNroCurso(Integer nroCurso) {
		this.nroCurso = nroCurso;
	}
	public Integer getTipoTumor() {
		return tipoTumor;
	}
	public void setTipoTumor(Integer tipoTumor) {
		this.tipoTumor = tipoTumor;
	}
	public Integer getLugarProgresion() {
		return lugarProgresion;
	}
	public void setLugarProgresion(Integer lugarProgresion) {
		this.lugarProgresion = lugarProgresion;
	}
	public Integer getRespAlcanzada() {
		return respAlcanzada;
	}
	public void setRespAlcanzada(Integer respAlcanzada) {
		this.respAlcanzada = respAlcanzada;
	}
	public Integer getCumplePrefeInsti() {
		return cumplePrefeInsti;
	}
	public void setCumplePrefeInsti(Integer cumplePrefeInsti) {
		this.cumplePrefeInsti = cumplePrefeInsti;
	}
	public String getGrabar() {
		return grabar;
	}
	public void setGrabar(String grabar) {
		this.grabar = grabar;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getLineaTratamiento() {
		return lineaTratamiento;
	}
	public void setLineaTratamiento(String lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getRespuestaAlcanzada() {
		return respuestaAlcanzada;
	}
	public void setRespuestaAlcanzada(String respuestaAlcanzada) {
		this.respuestaAlcanzada = respuestaAlcanzada;
	}
	public String getDescLugarProgresion() {
		return descLugarProgresion;
	}
	public void setDescLugarProgresion(String descLugarProgresion) {
		this.descLugarProgresion = descLugarProgresion;
	}
	public String getMotivoInactivacion() {
		return motivoInactivacion;
	}
	public void setMotivoInactivacion(String motivoInactivacion) {
		this.motivoInactivacion = motivoInactivacion;
	}
	
	
	@Override
	public String toString() {
		return "LineaTratamientoBean [lineaTratamiento=" + lineaTratamiento + ", fechaInicio=" + fechaInicio
				+ ", fechaFin=" + fechaFin + ", curso=" + curso + ", respuestaAlcanzada=" + respuestaAlcanzada
				+ ", descLugarProgresion=" + descLugarProgresion + ", motivoInactivacion=" + motivoInactivacion + "]";
	}
	
	
	
}
