package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.ParticipanteResponse;

public class ParticipanteGrupoDiagRowMapper implements RowMapper<ParticipanteResponse>{
	
	@Override
	public ParticipanteResponse mapRow(ResultSet rs, int rowNum) throws SQLException {

		ParticipanteResponse participanteBean = new ParticipanteResponse();
		participanteBean.setCodParticipante(rs.getLong("COD_PARTICIPANTE"));
		participanteBean.setCmpMedico(rs.getString("CMP_MEDICO"));
		participanteBean.setCodUsuario(rs.getLong("COD_USUARIO"));
		
		return participanteBean;
	}

}
