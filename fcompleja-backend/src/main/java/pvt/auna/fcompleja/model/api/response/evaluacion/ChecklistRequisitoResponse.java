package pvt.auna.fcompleja.model.api.response.evaluacion;

import java.io.Serializable;
import java.sql.Date;

public class ChecklistRequisitoResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codChecklistReq;
	private Long lineaTratamiento;
	private Long codSevChkReqPac;
	private Long codMac;
	private Integer pTipoDocumento;
	private String descripcionDocumento;
	private String flagCarga;
	private Date fecCarga;
	private String urlDescarga;
	private String estado;
	private Integer pEstado;
	private Long codArchivo;

	private Long codSolEva;

	public ChecklistRequisitoResponse(Long codArchivo, Long codSolEva) {
		this.codArchivo = codArchivo;
		this.codSolEva = codSolEva;
	}

	public ChecklistRequisitoResponse() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodChecklistReq() {
		return codChecklistReq;
	}

	public void setCodChecklistReq(Long codChecklistReq) {
		this.codChecklistReq = codChecklistReq;
	}

	public Long getLineaTratamiento() {
		return lineaTratamiento;
	}

	public void setLineaTratamiento(Long lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}

	public Long getCodSevChkReqPac() {
		return codSevChkReqPac;
	}

	public void setCodSevChkReqPac(Long codSevChkReqPac) {
		this.codSevChkReqPac = codSevChkReqPac;
	}

	public Long getCodMac() {
		return codMac;
	}

	public void setCodMac(Long codMac) {
		this.codMac = codMac;
	}

	public Integer getpTipoDocumento() {
		return pTipoDocumento;
	}

	public void setpTipoDocumento(Integer pTipoDocumento) {
		this.pTipoDocumento = pTipoDocumento;
	}

	public String getDescripcionDocumento() {
		return descripcionDocumento;
	}

	public void setDescripcionDocumento(String descripcionDocumento) {
		this.descripcionDocumento = descripcionDocumento;
	}

	public String getFlagCarga() {
		return flagCarga;
	}

	public void setFlagCarga(String flagCarga) {
		this.flagCarga = flagCarga;
	}

	public Date getFecCarga() {
		return fecCarga;
	}

	public void setFecCarga(Date fecCarga) {
		this.fecCarga = fecCarga;
	}

	public String getUrlDescarga() {
		return urlDescarga;
	}

	public void setUrlDescarga(String urlDescarga) {
		this.urlDescarga = urlDescarga;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getpEstado() {
		return pEstado;
	}

	public void setpEstado(Integer pEstado) {
		this.pEstado = pEstado;
	}

	public Long getCodArchivo() {
		return codArchivo;
	}

	public void setCodArchivo(Long codArchivo) {
		this.codArchivo = codArchivo;
	}

	public Long getCodSolEva() {
		return codSolEva;
	}

	public void setCodSolEva(Long codSolEva) {
		this.codSolEva = codSolEva;
	}

	@Override
	public String toString() {
		return "ChecklistRequisitoResponse [codChecklistReq=" + codChecklistReq + ", lineaTratamiento="
				+ lineaTratamiento + ", codSevChkReqPac=" + codSevChkReqPac + ", codMac=" + codMac + ", pTipoDocumento="
				+ pTipoDocumento + ", descripcionDocumento=" + descripcionDocumento + ", flagCarga=" + flagCarga
				+ ", fecCarga=" + fecCarga + ", urlDescarga=" + urlDescarga + ", estado=" + estado + ", pEstado="
				+ pEstado + ", codArchivo=" + codArchivo + ", codSolEva=" + codSolEva + "]";
	}

}
