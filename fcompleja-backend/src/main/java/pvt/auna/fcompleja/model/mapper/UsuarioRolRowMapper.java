package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.UsuarioBean;

public class UsuarioRolRowMapper implements RowMapper<UsuarioBean>{

	@Override
	public UsuarioBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		UsuarioBean usuarioBean = new UsuarioBean();
		usuarioBean.setCodUsuario(rs.getInt("COD_USUARIO"));
		usuarioBean.setUsuario(rs.getString("USUARIO"));
		return usuarioBean;
	}

}
