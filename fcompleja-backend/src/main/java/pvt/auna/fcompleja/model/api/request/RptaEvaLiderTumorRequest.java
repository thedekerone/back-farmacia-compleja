package pvt.auna.fcompleja.model.api.request;

public class RptaEvaLiderTumorRequest {

    private static final long serialVersionUID = 1L;

    private Integer codSolEva;
    private String fechaSolEvaRpta;
    private Integer rptaSolEva;
    private String comentarioSolEva;
    private Integer codigoLiderTumor;
    private Integer codigoRolLiderTumor; // seguimiento
    private Integer codigoRolUsuario;
	private String  fechaEstado;
	private Integer codigoUsuario;
	
	public Integer getCodSolEva() {
		return codSolEva;
	}
	public void setCodSolEva(Integer codSolEva) {
		this.codSolEva = codSolEva;
	}
	public String getFechaSolEvaRpta() {
		return fechaSolEvaRpta;
	}
	public void setFechaSolEvaRpta(String fechaSolEvaRpta) {
		this.fechaSolEvaRpta = fechaSolEvaRpta;
	}
	public Integer getRptaSolEva() {
		return rptaSolEva;
	}
	public void setRptaSolEva(Integer rptaSolEva) {
		this.rptaSolEva = rptaSolEva;
	}
	public String getComentarioSolEva() {
		return comentarioSolEva;
	}
	public void setComentarioSolEva(String comentarioSolEva) {
		this.comentarioSolEva = comentarioSolEva;
	}
	public Integer getCodigoLiderTumor() {
		return codigoLiderTumor;
	}
	public void setCodigoLiderTumor(Integer codigoLiderTumor) {
		this.codigoLiderTumor = codigoLiderTumor;
	}
	public Integer getCodigoRolLiderTumor() {
		return codigoRolLiderTumor;
	}
	public void setCodigoRolLiderTumor(Integer codigoRolLiderTumor) {
		this.codigoRolLiderTumor = codigoRolLiderTumor;
	}
	public Integer getCodigoRolUsuario() {
		return codigoRolUsuario;
	}
	public void setCodigoRolUsuario(Integer codigoRolUsuario) {
		this.codigoRolUsuario = codigoRolUsuario;
	}
	public String getFechaEstado() {
		return fechaEstado;
	}
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}
	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	
	@Override
	public String toString() {
		return "RptaEvaLiderTumorRequest [codSolEva=" + codSolEva + ", fechaSolEvaRpta=" + fechaSolEvaRpta
				+ ", rptaSolEva=" + rptaSolEva + ", comentarioSolEva=" + comentarioSolEva + ", codigoLiderTumor="
				+ codigoLiderTumor + ", codigoRolLiderTumor=" + codigoRolLiderTumor + ", codigoRolUsuario="
				+ codigoRolUsuario + ", fechaEstado=" + fechaEstado + ", codigoUsuario=" + codigoUsuario + "]";
	}	
}
