package pvt.auna.fcompleja.model.api.request;

public class ObtenerMedicoRequest {
	
	private String codigoAfiliado;
	
	

	public ObtenerMedicoRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public ObtenerMedicoRequest(String codigoAfiliado) {
		super();
		this.codigoAfiliado = codigoAfiliado;
	}


	public String getCodigoAfiliado() {
		return codigoAfiliado;
	}

	public void setCodigoAfiliado(String codigoAfiliado) {
		this.codigoAfiliado = codigoAfiliado;
	}


	@Override
	public String toString() {
		return "ObtenerMedicoRequest [codigoAfiliado=" + codigoAfiliado + "]";
	}
	
	

}
