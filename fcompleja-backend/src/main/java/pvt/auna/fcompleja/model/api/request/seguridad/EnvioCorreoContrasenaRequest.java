package pvt.auna.fcompleja.model.api.request.seguridad;

public class EnvioCorreoContrasenaRequest {

    private String correo;

    private Integer codAplicacion;

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Integer getCodAplicacion() {
        return codAplicacion;
    }

    public void setCodAplicacion(Integer codAplicacion) {
        this.codAplicacion = codAplicacion;
    }
}

