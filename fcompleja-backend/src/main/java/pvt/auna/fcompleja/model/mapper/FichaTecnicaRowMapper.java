package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.FichaTecnicaBean;

public class FichaTecnicaRowMapper implements RowMapper<FichaTecnicaBean>{

	@Override
	public FichaTecnicaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		FichaTecnicaBean ficha = new FichaTecnicaBean();
		ficha.setCodMac(rs.getInt("cod_mac"));
		ficha.setCodFichaTecnica(rs.getInt("cod_ficha"));
		ficha.setCodVersion(rs.getString("cod_version"));
		ficha.setNombreArchivo(rs.getString("nombre_archivo"));
		ficha.setFechaIniVigencia(rs.getDate("fecha_inicio"));
		ficha.setFechaFinVigencia(rs.getDate("fecha_fin"));
		ficha.setCodEstado(rs.getInt("cod_estado"));
		ficha.setEstado(rs.getString("estado"));
		return ficha;
	}

}
