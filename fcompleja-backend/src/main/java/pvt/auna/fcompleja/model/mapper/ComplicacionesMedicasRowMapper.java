/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.ComplicacionesBean;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ComplicacionesMedicasRowMapper implements RowMapper<ComplicacionesBean>{

	@Override
	public ComplicacionesBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ComplicacionesBean complicaciones = new ComplicacionesBean();
		complicaciones.setCodMac(rs.getInt("cod_mac"));
		complicaciones.setCodArchivoComp(rs.getInt("cod_archivo_comp"));
		complicaciones.setCodVersion(rs.getString("cod_version"));
		complicaciones.setNombreArchivo(rs.getString("nombre_archivo"));
		complicaciones.setFechaInicioVigencia(rs.getDate("fecha_inicio"));
		complicaciones.setFechaFinVigencia(rs.getDate("fecha_fin"));
		complicaciones.setCodEstado(rs.getInt("cod_estado"));
		complicaciones.setEstado(rs.getString("estado"));
		complicaciones.setCodigoArchivoComp(rs.getInt("codigo_archivo_comp"));
		return complicaciones;
	}

}

