package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.ParticipanteBean;

public class ParticipanteRolRowMapper implements RowMapper<ParticipanteBean>{
	
	
	@Override
	public ParticipanteBean mapRow(ResultSet rs, int rowNum) throws SQLException {
			
		ParticipanteBean participanteBean = new ParticipanteBean();
		participanteBean.setCodParticipante(rs.getInt("COD_PARTICIPANTE"));
		participanteBean.setCodUsuario(rs.getInt("COD_USUARIO"));
		participanteBean.setEstadoParticipante(rs.getInt("ESTADO_PARTICIPANTE"));
		participanteBean.setCmpMedico(rs.getString("CMP_MEDICO"));
		participanteBean.setNombreFirma(rs.getString("NOMBRE_FIRMA"));
		participanteBean.setCodGrpDiag(rs.getString("COD_GRP_DIAG"));
		return participanteBean;
	}

	
}
