package pvt.auna.fcompleja.model.api.response;

import java.util.List;

import pvt.auna.fcompleja.model.api.AuditResponse;

public class ResponseFTPObtener {

	private AuditResponse auditResponse;

	private List<ArchivoFTPObtenerBean> dataList;

	
	
	public ResponseFTPObtener() {
		super();
		// TODO Auto-generated constructor stub
	}


	public ResponseFTPObtener(AuditResponse audiResponse, List<ArchivoFTPObtenerBean> dataList) {
		super();
		this.auditResponse = audiResponse;
		this.dataList = dataList;
	}
	

	public AuditResponse getAuditResponse() {
		return auditResponse;
	}

	public void setAudiResponse(AuditResponse auditResponse) {
		this.auditResponse = auditResponse;
	}

	public List<ArchivoFTPObtenerBean> getDataList() {
		return dataList;
	}

	public void setDataList(List<ArchivoFTPObtenerBean> dataList) {
		this.dataList = dataList;
	}
	
	@Override
	public String toString() {
		return "ResponseFTP [auditResponse=" + auditResponse + ", dataList=" + dataList + "]";
	}
	
}
