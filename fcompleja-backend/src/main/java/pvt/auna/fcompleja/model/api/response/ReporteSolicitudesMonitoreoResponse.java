/**
 * 
 */
package pvt.auna.fcompleja.model.api.response;

import java.util.Date;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ReporteSolicitudesMonitoreoResponse {
	
	
	private Integer fechaMes;
	private Integer fechaAno;
	private String codigoSolicitudEva;
	private String codigoPaciente;
	private String  codigoAfiliado;
	private Date    fechaAfiliacion;
	private String  tipoAfiliacion;
	private String documentoIdentidad;
	private String  paciente;
	private String  sexo;
	private Integer edad;
	private String  medicamento;
	private String  cie10;
	private String 	diagnostico;
	private String  grupoDiagnostico;
	private String 	plan;
	private String	estadio;
	private String 	tnm;
	private String 	linea;
	private String	clinica;
	private	String	cmp;
	private String 	medicoTratantePrescriptor;
	private String	codigoSCGSolben;
	private String	estadoSolicitudEva;
	private Date 	fechaAprobSolicitudEva;
	private String 	autorizadorPertinenciaSolicitudEva;
	private String	nroCartaGarantia;
	private Date 	fechaIniTratamiento;
	private String nroEvolucion;
	private Integer codigoResponsableMonitoreo;
	private Date	fechaProgMonitoreo;
	private Date	fechaRealMonitoreo;
	private String	estadoMonitoreo;
	private	String	tolerancia;
	private	String	toxicidad;
	private	String	respuestaClinica;
	private String	atencionAlertas;
	private Date	fechaUltimoConsumo;
	private Integer	cantidadUltimoConsumo;
	private String	resultadoEvolucion;
	private	Date	fechaInactivacion;
	private String 	motivoInactivacion;
	private	String	ejecutivoMonitoreo;
	private	String	estadoSeguimiento;
	private Date	fechaRegistroSE;
	private	String	codigoTareaMonitoreo;
	

	/**
	 * 
	 */
	public ReporteSolicitudesMonitoreoResponse() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the fechaMes
	 */
	public Integer getFechaMes() {
		return fechaMes;
	}


	/**
	 * @param fechaMes the fechaMes to set
	 */
	public void setFechaMes(Integer fechaMes) {
		this.fechaMes = fechaMes;
	}


	/**
	 * @return the fechaAno
	 */
	public Integer getFechaAno() {
		return fechaAno;
	}


	/**
	 * @param fechaAno the fechaAno to set
	 */
	public void setFechaAno(Integer fechaAno) {
		this.fechaAno = fechaAno;
	}


	/**
	 * @return the codigoSolicitudEva
	 */
	public String getCodigoSolicitudEva() {
		return codigoSolicitudEva;
	}


	/**
	 * @param codigoSolicitudEva the codigoSolicitudEva to set
	 */
	public void setCodigoSolicitudEva(String codigoSolicitudEva) {
		this.codigoSolicitudEva = codigoSolicitudEva;
	}


	/**
	 * @return the codigoPaciente
	 */
	public String getCodigoPaciente() {
		return codigoPaciente;
	}


	/**
	 * @param codigoPaciente the codigoPaciente to set
	 */
	public void setCodigoPaciente(String codigoPaciente) {
		this.codigoPaciente = codigoPaciente;
	}


	/**
	 * @return the codigoAfiliado
	 */
	public String getCodigoAfiliado() {
		return codigoAfiliado;
	}


	/**
	 * @param codigoAfiliado the codigoAfiliado to set
	 */
	public void setCodigoAfiliado(String codigoAfiliado) {
		this.codigoAfiliado = codigoAfiliado;
	}


	/**
	 * @return the fechaAfiliacion
	 */
	public Date getFechaAfiliacion() {
		return fechaAfiliacion;
	}


	/**
	 * @param fechaAfiliacion the fechaAfiliacion to set
	 */
	public void setFechaAfiliacion(Date fechaAfiliacion) {
		this.fechaAfiliacion = fechaAfiliacion;
	}


	/**
	 * @return the tipoAfiliacion
	 */
	public String getTipoAfiliacion() {
		return tipoAfiliacion;
	}


	/**
	 * @param tipoAfiliacion the tipoAfiliacion to set
	 */
	public void setTipoAfiliacion(String tipoAfiliacion) {
		this.tipoAfiliacion = tipoAfiliacion;
	}


	/**
	 * @return the documentoIdentidad
	 */
	public String getDocumentoIdentidad() {
		return documentoIdentidad;
	}


	/**
	 * @param documentoIdentidad the documentoIdentidad to set
	 */
	public void setDocumentoIdentidad(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}


	/**
	 * @return the paciente
	 */
	public String getPaciente() {
		return paciente;
	}


	/**
	 * @param paciente the paciente to set
	 */
	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}


	/**
	 * @return the sexo
	 */
	public String getSexo() {
		return sexo;
	}


	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}


	/**
	 * @return the edad
	 */
	public Integer getEdad() {
		return edad;
	}


	/**
	 * @param edad the edad to set
	 */
	public void setEdad(Integer edad) {
		this.edad = edad;
	}


	/**
	 * @return the medicamento
	 */
	public String getMedicamento() {
		return medicamento;
	}


	/**
	 * @param medicamento the medicamento to set
	 */
	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}


	/**
	 * @return the cie10
	 */
	public String getCie10() {
		return cie10;
	}


	/**
	 * @param cie10 the cie10 to set
	 */
	public void setCie10(String cie10) {
		this.cie10 = cie10;
	}


	/**
	 * @return the diagnostico
	 */
	public String getDiagnostico() {
		return diagnostico;
	}


	/**
	 * @param diagnostico the diagnostico to set
	 */
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}


	/**
	 * @return the grupoDiagnostico
	 */
	public String getGrupoDiagnostico() {
		return grupoDiagnostico;
	}


	/**
	 * @param grupoDiagnostico the grupoDiagnostico to set
	 */
	public void setGrupoDiagnostico(String grupoDiagnostico) {
		this.grupoDiagnostico = grupoDiagnostico;
	}


	/**
	 * @return the plan
	 */
	public String getPlan() {
		return plan;
	}


	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan) {
		this.plan = plan;
	}


	/**
	 * @return the estadio
	 */
	public String getEstadio() {
		return estadio;
	}


	/**
	 * @param estadio the estadio to set
	 */
	public void setEstadio(String estadio) {
		this.estadio = estadio;
	}


	/**
	 * @return the tnm
	 */
	public String getTnm() {
		return tnm;
	}


	/**
	 * @param tnm the tnm to set
	 */
	public void setTnm(String tnm) {
		this.tnm = tnm;
	}


	/**
	 * @return the linea
	 */
	public String getLinea() {
		return linea;
	}


	/**
	 * @param linea the linea to set
	 */
	public void setLinea(String linea) {
		this.linea = linea;
	}


	/**
	 * @return the clinica
	 */
	public String getClinica() {
		return clinica;
	}


	/**
	 * @param clinica the clinica to set
	 */
	public void setClinica(String clinica) {
		this.clinica = clinica;
	}


	/**
	 * @return the cmp
	 */
	public String getCmp() {
		return cmp;
	}


	/**
	 * @param cmp the cmp to set
	 */
	public void setCmp(String cmp) {
		this.cmp = cmp;
	}


	/**
	 * @return the medicoTratantePrescriptor
	 */
	public String getMedicoTratantePrescriptor() {
		return medicoTratantePrescriptor;
	}


	/**
	 * @param medicoTratantePrescriptor the medicoTratantePrescriptor to set
	 */
	public void setMedicoTratantePrescriptor(String medicoTratantePrescriptor) {
		this.medicoTratantePrescriptor = medicoTratantePrescriptor;
	}


	/**
	 * @return the codigoSCGSolben
	 */
	public String getCodigoSCGSolben() {
		return codigoSCGSolben;
	}


	/**
	 * @param codigoSCGSolben the codigoSCGSolben to set
	 */
	public void setCodigoSCGSolben(String codigoSCGSolben) {
		this.codigoSCGSolben = codigoSCGSolben;
	}


	/**
	 * @return the estadoSolicitudEva
	 */
	public String getEstadoSolicitudEva() {
		return estadoSolicitudEva;
	}


	/**
	 * @param estadoSolicitudEva the estadoSolicitudEva to set
	 */
	public void setEstadoSolicitudEva(String estadoSolicitudEva) {
		this.estadoSolicitudEva = estadoSolicitudEva;
	}


	/**
	 * @return the fechaAprobSolicitudEva
	 */
	public Date getFechaAprobSolicitudEva() {
		return fechaAprobSolicitudEva;
	}


	/**
	 * @param fechaAprobSolicitudEva the fechaAprobSolicitudEva to set
	 */
	public void setFechaAprobSolicitudEva(Date fechaAprobSolicitudEva) {
		this.fechaAprobSolicitudEva = fechaAprobSolicitudEva;
	}


	/**
	 * @return the autorizadorPertinenciaSolicitudEva
	 */
	public String getAutorizadorPertinenciaSolicitudEva() {
		return autorizadorPertinenciaSolicitudEva;
	}


	/**
	 * @param autorizadorPertinenciaSolicitudEva the autorizadorPertinenciaSolicitudEva to set
	 */
	public void setAutorizadorPertinenciaSolicitudEva(String autorizadorPertinenciaSolicitudEva) {
		this.autorizadorPertinenciaSolicitudEva = autorizadorPertinenciaSolicitudEva;
	}


	/**
	 * @return the nroCartaGarantia
	 */
	public String getNroCartaGarantia() {
		return nroCartaGarantia;
	}


	/**
	 * @param nroCartaGarantia the nroCartaGarantia to set
	 */
	public void setNroCartaGarantia(String nroCartaGarantia) {
		this.nroCartaGarantia = nroCartaGarantia;
	}


	/**
	 * @return the fechaIniTratamiento
	 */
	public Date getFechaIniTratamiento() {
		return fechaIniTratamiento;
	}


	/**
	 * @param fechaIniTratamiento the fechaIniTratamiento to set
	 */
	public void setFechaIniTratamiento(Date fechaIniTratamiento) {
		this.fechaIniTratamiento = fechaIniTratamiento;
	}


	/**
	 * @return the nroEvolucion
	 */
	public String getNroEvolucion() {
		return nroEvolucion;
	}


	/**
	 * @param nroEvolucion the nroEvolucion to set
	 */
	public void setNroEvolucion(String nroEvolucion) {
		this.nroEvolucion = nroEvolucion;
	}


	/**
	 * @return the codigoResponsableMonitoreo
	 */
	public Integer getCodigoResponsableMonitoreo() {
		return codigoResponsableMonitoreo;
	}


	/**
	 * @param codigoResponsableMonitoreo the codigoResponsableMonitoreo to set
	 */
	public void setCodigoResponsableMonitoreo(Integer codigoResponsableMonitoreo) {
		this.codigoResponsableMonitoreo = codigoResponsableMonitoreo;
	}


	/**
	 * @return the fechaProgMonitoreo
	 */
	public Date getFechaProgMonitoreo() {
		return fechaProgMonitoreo;
	}


	/**
	 * @param fechaProgMonitoreo the fechaProgMonitoreo to set
	 */
	public void setFechaProgMonitoreo(Date fechaProgMonitoreo) {
		this.fechaProgMonitoreo = fechaProgMonitoreo;
	}


	/**
	 * @return the fechaRealMonitoreo
	 */
	public Date getFechaRealMonitoreo() {
		return fechaRealMonitoreo;
	}


	/**
	 * @param fechaRealMonitoreo the fechaRealMonitoreo to set
	 */
	public void setFechaRealMonitoreo(Date fechaRealMonitoreo) {
		this.fechaRealMonitoreo = fechaRealMonitoreo;
	}


	/**
	 * @return the estadoMonitoreo
	 */
	public String getEstadoMonitoreo() {
		return estadoMonitoreo;
	}


	/**
	 * @param estadoMonitoreo the estadoMonitoreo to set
	 */
	public void setEstadoMonitoreo(String estadoMonitoreo) {
		this.estadoMonitoreo = estadoMonitoreo;
	}


	/**
	 * @return the tolerancia
	 */
	public String getTolerancia() {
		return tolerancia;
	}


	/**
	 * @param tolerancia the tolerancia to set
	 */
	public void setTolerancia(String tolerancia) {
		this.tolerancia = tolerancia;
	}


	/**
	 * @return the toxicidad
	 */
	public String getToxicidad() {
		return toxicidad;
	}


	/**
	 * @param toxicidad the toxicidad to set
	 */
	public void setToxicidad(String toxicidad) {
		this.toxicidad = toxicidad;
	}


	/**
	 * @return the respuestaClinica
	 */
	public String getRespuestaClinica() {
		return respuestaClinica;
	}


	/**
	 * @param respuestaClinica the respuestaClinica to set
	 */
	public void setRespuestaClinica(String respuestaClinica) {
		this.respuestaClinica = respuestaClinica;
	}


	/**
	 * @return the atencionAlertas
	 */
	public String getAtencionAlertas() {
		return atencionAlertas;
	}


	/**
	 * @param atencionAlertas the atencionAlertas to set
	 */
	public void setAtencionAlertas(String atencionAlertas) {
		this.atencionAlertas = atencionAlertas;
	}


	/**
	 * @return the fechaUltimoConsumo
	 */
	public Date getFechaUltimoConsumo() {
		return fechaUltimoConsumo;
	}


	/**
	 * @param fechaUltimoConsumo the fechaUltimoConsumo to set
	 */
	public void setFechaUltimoConsumo(Date fechaUltimoConsumo) {
		this.fechaUltimoConsumo = fechaUltimoConsumo;
	}


	/**
	 * @return the cantidadUltimoConsumo
	 */
	public Integer getCantidadUltimoConsumo() {
		return cantidadUltimoConsumo;
	}


	/**
	 * @param cantidadUltimoConsumo the cantidadUltimoConsumo to set
	 */
	public void setCantidadUltimoConsumo(Integer cantidadUltimoConsumo) {
		this.cantidadUltimoConsumo = cantidadUltimoConsumo;
	}


	/**
	 * @return the resultadoEvolucion
	 */
	public String getResultadoEvolucion() {
		return resultadoEvolucion;
	}


	/**
	 * @param resultadoEvolucion the resultadoEvolucion to set
	 */
	public void setResultadoEvolucion(String resultadoEvolucion) {
		this.resultadoEvolucion = resultadoEvolucion;
	}


	/**
	 * @return the fechaInactivacion
	 */
	public Date getFechaInactivacion() {
		return fechaInactivacion;
	}


	/**
	 * @param fechaInactivacion the fechaInactivacion to set
	 */
	public void setFechaInactivacion(Date fechaInactivacion) {
		this.fechaInactivacion = fechaInactivacion;
	}


	/**
	 * @return the motivoInactivacion
	 */
	public String getMotivoInactivacion() {
		return motivoInactivacion;
	}


	/**
	 * @param motivoInactivacion the motivoInactivacion to set
	 */
	public void setMotivoInactivacion(String motivoInactivacion) {
		this.motivoInactivacion = motivoInactivacion;
	}


	/**
	 * @return the ejecutivoMonitoreo
	 */
	public String getEjecutivoMonitoreo() {
		return ejecutivoMonitoreo;
	}


	/**
	 * @param ejecutivoMonitoreo the ejecutivoMonitoreo to set
	 */
	public void setEjecutivoMonitoreo(String ejecutivoMonitoreo) {
		this.ejecutivoMonitoreo = ejecutivoMonitoreo;
	}


	/**
	 * @return the estadoSeguimiento
	 */
	public String getEstadoSeguimiento() {
		return estadoSeguimiento;
	}


	/**
	 * @param estadoSeguimiento the estadoSeguimiento to set
	 */
	public void setEstadoSeguimiento(String estadoSeguimiento) {
		this.estadoSeguimiento = estadoSeguimiento;
	}


	/**
	 * @return the fechaRegistroSE
	 */
	public Date getFechaRegistroSE() {
		return fechaRegistroSE;
	}


	/**
	 * @param fechaRegistroSE the fechaRegistroSE to set
	 */
	public void setFechaRegistroSE(Date fechaRegistroSE) {
		this.fechaRegistroSE = fechaRegistroSE;
	}


	/**
	 * @return the codigoTareaMonitoreo
	 */
	public String getCodigoTareaMonitoreo() {
		return codigoTareaMonitoreo;
	}


	/**
	 * @param codigoTareaMonitoreo the codigoTareaMonitoreo to set
	 */
	public void setCodigoTareaMonitoreo(String codigoTareaMonitoreo) {
		this.codigoTareaMonitoreo = codigoTareaMonitoreo;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ReporteSolicitudesMonitoreoResponse [fechaMes=" + fechaMes + ", fechaAno=" + fechaAno
				+ ", codigoSolicitudEva=" + codigoSolicitudEva + ", codigoPaciente=" + codigoPaciente
				+ ", codigoAfiliado=" + codigoAfiliado + ", fechaAfiliacion=" + fechaAfiliacion + ", tipoAfiliacion="
				+ tipoAfiliacion + ", documentoIdentidad=" + documentoIdentidad + ", paciente=" + paciente + ", sexo="
				+ sexo + ", edad=" + edad + ", medicamento=" + medicamento + ", cie10=" + cie10 + ", diagnostico="
				+ diagnostico + ", grupoDiagnostico=" + grupoDiagnostico + ", plan=" + plan + ", estadio=" + estadio
				+ ", tnm=" + tnm + ", linea=" + linea + ", clinica=" + clinica + ", cmp=" + cmp
				+ ", medicoTratantePrescriptor=" + medicoTratantePrescriptor + ", codigoSCGSolben=" + codigoSCGSolben
				+ ", estadoSolicitudEva=" + estadoSolicitudEva + ", fechaAprobSolicitudEva=" + fechaAprobSolicitudEva
				+ ", autorizadorPertinenciaSolicitudEva=" + autorizadorPertinenciaSolicitudEva + ", nroCartaGarantia="
				+ nroCartaGarantia + ", fechaIniTratamiento=" + fechaIniTratamiento + ", nroEvolucion=" + nroEvolucion
				+ ", codigoResponsableMonitoreo=" + codigoResponsableMonitoreo + ", fechaProgMonitoreo="
				+ fechaProgMonitoreo + ", fechaRealMonitoreo=" + fechaRealMonitoreo + ", estadoMonitoreo="
				+ estadoMonitoreo + ", tolerancia=" + tolerancia + ", toxicidad=" + toxicidad + ", respuestaClinica="
				+ respuestaClinica + ", atencionAlertas=" + atencionAlertas + ", fechaUltimoConsumo="
				+ fechaUltimoConsumo + ", cantidadUltimoConsumo=" + cantidadUltimoConsumo + ", resultadoEvolucion="
				+ resultadoEvolucion + ", fechaInactivacion=" + fechaInactivacion + ", motivoInactivacion="
				+ motivoInactivacion + ", ejecutivoMonitoreo=" + ejecutivoMonitoreo + ", estadoSeguimiento="
				+ estadoSeguimiento + ", fechaRegistroSE=" + fechaRegistroSE + ", codigoTareaMonitoreo="
				+ codigoTareaMonitoreo + ", toString()=" + super.toString() + "]";
	}
	
	
	

}
