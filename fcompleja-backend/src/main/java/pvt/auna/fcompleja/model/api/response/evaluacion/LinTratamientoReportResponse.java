package pvt.auna.fcompleja.model.api.response.evaluacion;

import java.util.List;

import pvt.auna.fcompleja.model.bean.CondicionBasalBean;

public class LinTratamientoReportResponse {

    private List<LinTrataReportDetalleBean> lineaTratamiento;
    private List<LinTrataReportDetalleBean> lineaMetastasis;
    private List<LinTrataReportDetalleBean> resultadoBasal;
    private List<CondicionBasalBean> condicionBasalBean;
	private Integer codigoRespuesta;
	private String  mensajeRespuesta;
	
	
	public List<CondicionBasalBean> getCondicionBasalBean() {
		return condicionBasalBean;
	}
	public void setCondicionBasalBean(List<CondicionBasalBean> condicionBasalBean) {
		this.condicionBasalBean = condicionBasalBean;
	}
	public Integer getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(Integer codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	public List<LinTrataReportDetalleBean> getLineaTratamiento() {
		return lineaTratamiento;
	}
	public void setLineaTratamiento(List<LinTrataReportDetalleBean> lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}
	public List<LinTrataReportDetalleBean> getLineaMetastasis() {
		return lineaMetastasis;
	}
	public void setLineaMetastasis(List<LinTrataReportDetalleBean> lineaMetastasis) {
		this.lineaMetastasis = lineaMetastasis;
	}
	public List<LinTrataReportDetalleBean> getResultadoBasal() {
		return resultadoBasal;
	}
	public void setResultadoBasal(List<LinTrataReportDetalleBean> resultadoBasal) {
		this.resultadoBasal = resultadoBasal;
	}
	@Override
	public String toString() {
		return "pruebaResponse [lineaTratamiento=" + lineaTratamiento + ", lineaMetastasis=" + lineaMetastasis
				+ ", resultadoBasal=" + resultadoBasal + ", condicionBasalBean=" + condicionBasalBean
				+ ", codigoRespuesta=" + codigoRespuesta + ", mensajeRespuesta=" + mensajeRespuesta + "]";
	}
	
	

}
