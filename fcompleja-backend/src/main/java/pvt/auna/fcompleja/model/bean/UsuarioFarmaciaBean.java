package pvt.auna.fcompleja.model.bean;

public class UsuarioFarmaciaBean {
	private Integer codUsuario;
	private String nombreApellido;
	private String correo;

	public Integer getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getNombreApellido() {
		return nombreApellido;
	}

	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	@Override
	public String toString() {
		return "UsuarioFarmaciaBean [codUsuario=" + codUsuario + ", nombreApellido=" + nombreApellido + ", correo="
				+ correo + "]";
	}

}
