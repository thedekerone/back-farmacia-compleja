package pvt.auna.fcompleja.model.api.response;

public class ResponseCodInclusion {
	
private Number codInclusion;

public ResponseCodInclusion() {
	super();
	// TODO Auto-generated constructor stub
}

public Number getCodInclusion() {
	return codInclusion;
}

public void setCodInclusion(Number codInclusion) {
	this.codInclusion = codInclusion;
}

@Override
public String toString() {
	return "ResponseCodInclusion [codInclusion=" + codInclusion + "]"; }

}
