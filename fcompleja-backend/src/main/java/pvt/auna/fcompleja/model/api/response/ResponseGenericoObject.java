package pvt.auna.fcompleja.model.api.response;

import pvt.auna.fcompleja.model.api.AudiResponse;

public class ResponseGenericoObject {

	private AudiResponse audiResponse;

	private Object data;

	public ResponseGenericoObject() {
	}

	public ResponseGenericoObject(AudiResponse audiResponse, Object data) {
		super();
		this.audiResponse = audiResponse;
		this.data = data;
	}

	public AudiResponse getAudiResponse() {
		return audiResponse;
	}

	public void setAudiResponse(AudiResponse audiResponse) {
		this.audiResponse = audiResponse;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "OncoWsResponse [audiResponse=" + audiResponse + ", dataList=" + data + "]";
	}

}
