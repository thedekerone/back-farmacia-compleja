/**
 * 
 */
package pvt.auna.fcompleja.model.bean;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class MarcadoresBean {
	
	private Integer 	codigoConfigMarca;
	private String 		codigoConfigMarcaLargo;
	private Integer 	codigoExamenMed;
	private String 		descripcionExamenMed;
	private Integer 	codigoMac;
	private String 		descripcionMac;
	private Integer 	codigoGrupoDiag;
	private String		descripcionGrupoDiag;
	private Integer 	codigoTipoMarcador;
	private String		descripcionMarcador;
	private Integer		codigoPerMaxima;
	private String 		descripcionPerMaxima;
	private Integer		codigoPerMinima;
	private String		descripcionPerMinima;
	private Integer		codigoEstado;
	private String		descripcionEstado;
	/**
	 * 
	 */
	public MarcadoresBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param codigoConfigMarca
	 * @param codigoConfigMarcaLargo
	 * @param codigoExamenMed
	 * @param descripcionExamenMed
	 * @param codigoMac
	 * @param descripcionMac
	 * @param codigoGrupoDiag
	 * @param descripcionGrupoDiag
	 * @param codigoTipoMarcador
	 * @param descripcionMarcador
	 * @param codigoPerMaxima
	 * @param descripcionPerMaxima
	 * @param codigoPerMinima
	 * @param descripcionPerMinima
	 * @param codigoEstado
	 * @param descripcionEstado
	 */
	public MarcadoresBean(Integer codigoConfigMarca, String codigoConfigMarcaLargo, Integer codigoExamenMed,
			String descripcionExamenMed, Integer codigoMac, String descripcionMac, Integer codigoGrupoDiag,
			String descripcionGrupoDiag, Integer codigoTipoMarcador, String descripcionMarcador,
			Integer codigoPerMaxima, String descripcionPerMaxima, Integer codigoPerMinima, String descripcionPerMinima,
			Integer codigoEstado, String descripcionEstado) {
		super();
		this.codigoConfigMarca = codigoConfigMarca;
		this.codigoConfigMarcaLargo = codigoConfigMarcaLargo;
		this.codigoExamenMed = codigoExamenMed;
		this.descripcionExamenMed = descripcionExamenMed;
		this.codigoMac = codigoMac;
		this.descripcionMac = descripcionMac;
		this.codigoGrupoDiag = codigoGrupoDiag;
		this.descripcionGrupoDiag = descripcionGrupoDiag;
		this.codigoTipoMarcador = codigoTipoMarcador;
		this.descripcionMarcador = descripcionMarcador;
		this.codigoPerMaxima = codigoPerMaxima;
		this.descripcionPerMaxima = descripcionPerMaxima;
		this.codigoPerMinima = codigoPerMinima;
		this.descripcionPerMinima = descripcionPerMinima;
		this.codigoEstado = codigoEstado;
		this.descripcionEstado = descripcionEstado;
	}
	/**
	 * @return the codigoConfigMarca
	 */
	public Integer getCodigoConfigMarca() {
		return codigoConfigMarca;
	}
	/**
	 * @param codigoConfigMarca the codigoConfigMarca to set
	 */
	public void setCodigoConfigMarca(Integer codigoConfigMarca) {
		this.codigoConfigMarca = codigoConfigMarca;
	}
	/**
	 * @return the codigoConfigMarcaLargo
	 */
	public String getCodigoConfigMarcaLargo() {
		return codigoConfigMarcaLargo;
	}
	/**
	 * @param codigoConfigMarcaLargo the codigoConfigMarcaLargo to set
	 */
	public void setCodigoConfigMarcaLargo(String codigoConfigMarcaLargo) {
		this.codigoConfigMarcaLargo = codigoConfigMarcaLargo;
	}
	/**
	 * @return the codigoExamenMed
	 */
	public Integer getCodigoExamenMed() {
		return codigoExamenMed;
	}
	/**
	 * @param codigoExamenMed the codigoExamenMed to set
	 */
	public void setCodigoExamenMed(Integer codigoExamenMed) {
		this.codigoExamenMed = codigoExamenMed;
	}
	/**
	 * @return the descripcionExamenMed
	 */
	public String getDescripcionExamenMed() {
		return descripcionExamenMed;
	}
	/**
	 * @param descripcionExamenMed the descripcionExamenMed to set
	 */
	public void setDescripcionExamenMed(String descripcionExamenMed) {
		this.descripcionExamenMed = descripcionExamenMed;
	}
	/**
	 * @return the codigoMac
	 */
	public Integer getCodigoMac() {
		return codigoMac;
	}
	/**
	 * @param codigoMac the codigoMac to set
	 */
	public void setCodigoMac(Integer codigoMac) {
		this.codigoMac = codigoMac;
	}
	/**
	 * @return the descripcionMac
	 */
	public String getDescripcionMac() {
		return descripcionMac;
	}
	/**
	 * @param descripcionMac the descripcionMac to set
	 */
	public void setDescripcionMac(String descripcionMac) {
		this.descripcionMac = descripcionMac;
	}
	/**
	 * @return the codigoGrupoDiag
	 */
	public Integer getCodigoGrupoDiag() {
		return codigoGrupoDiag;
	}
	/**
	 * @param codigoGrupoDiag the codigoGrupoDiag to set
	 */
	public void setCodigoGrupoDiag(Integer codigoGrupoDiag) {
		this.codigoGrupoDiag = codigoGrupoDiag;
	}
	/**
	 * @return the descripcionGrupoDiag
	 */
	public String getDescripcionGrupoDiag() {
		return descripcionGrupoDiag;
	}
	/**
	 * @param descripcionGrupoDiag the descripcionGrupoDiag to set
	 */
	public void setDescripcionGrupoDiag(String descripcionGrupoDiag) {
		this.descripcionGrupoDiag = descripcionGrupoDiag;
	}
	/**
	 * @return the codigoTipoMarcador
	 */
	public Integer getCodigoTipoMarcador() {
		return codigoTipoMarcador;
	}
	/**
	 * @param codigoTipoMarcador the codigoTipoMarcador to set
	 */
	public void setCodigoTipoMarcador(Integer codigoTipoMarcador) {
		this.codigoTipoMarcador = codigoTipoMarcador;
	}
	/**
	 * @return the descripcionMarcador
	 */
	public String getDescripcionMarcador() {
		return descripcionMarcador;
	}
	/**
	 * @param descripcionMarcador the descripcionMarcador to set
	 */
	public void setDescripcionMarcador(String descripcionMarcador) {
		this.descripcionMarcador = descripcionMarcador;
	}
	/**
	 * @return the codigoPerMaxima
	 */
	public Integer getCodigoPerMaxima() {
		return codigoPerMaxima;
	}
	/**
	 * @param codigoPerMaxima the codigoPerMaxima to set
	 */
	public void setCodigoPerMaxima(Integer codigoPerMaxima) {
		this.codigoPerMaxima = codigoPerMaxima;
	}
	/**
	 * @return the descripcionPerMaxima
	 */
	public String getDescripcionPerMaxima() {
		return descripcionPerMaxima;
	}
	/**
	 * @param descripcionPerMaxima the descripcionPerMaxima to set
	 */
	public void setDescripcionPerMaxima(String descripcionPerMaxima) {
		this.descripcionPerMaxima = descripcionPerMaxima;
	}
	/**
	 * @return the codigoPerMinima
	 */
	public Integer getCodigoPerMinima() {
		return codigoPerMinima;
	}
	/**
	 * @param codigoPerMinima the codigoPerMinima to set
	 */
	public void setCodigoPerMinima(Integer codigoPerMinima) {
		this.codigoPerMinima = codigoPerMinima;
	}
	/**
	 * @return the descripcionPerMinima
	 */
	public String getDescripcionPerMinima() {
		return descripcionPerMinima;
	}
	/**
	 * @param descripcionPerMinima the descripcionPerMinima to set
	 */
	public void setDescripcionPerMinima(String descripcionPerMinima) {
		this.descripcionPerMinima = descripcionPerMinima;
	}
	/**
	 * @return the codigoEstado
	 */
	public Integer getCodigoEstado() {
		return codigoEstado;
	}
	/**
	 * @param codigoEstado the codigoEstado to set
	 */
	public void setCodigoEstado(Integer codigoEstado) {
		this.codigoEstado = codigoEstado;
	}
	/**
	 * @return the descripcionEstado
	 */
	public String getDescripcionEstado() {
		return descripcionEstado;
	}
	/**
	 * @param descripcionEstado the descripcionEstado to set
	 */
	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MarcadoresBean [codigoConfigMarca=" + codigoConfigMarca + ", codigoConfigMarcaLargo="
				+ codigoConfigMarcaLargo + ", codigoExamenMed=" + codigoExamenMed + ", descripcionExamenMed="
				+ descripcionExamenMed + ", codigoMac=" + codigoMac + ", descripcionMac=" + descripcionMac
				+ ", codigoGrupoDiag=" + codigoGrupoDiag + ", descripcionGrupoDiag=" + descripcionGrupoDiag
				+ ", codigoTipoMarcador=" + codigoTipoMarcador + ", descripcionMarcador=" + descripcionMarcador
				+ ", codigoPerMaxima=" + codigoPerMaxima + ", descripcionPerMaxima=" + descripcionPerMaxima
				+ ", codigoPerMinima=" + codigoPerMinima + ", descripcionPerMinima=" + descripcionPerMinima
				+ ", codigoEstado=" + codigoEstado + ", descripcionEstado=" + descripcionEstado + ", toString()="
				+ super.toString() + "]";
	}
	
	
	
	
}
