package pvt.auna.fcompleja.model.api.response;

import java.io.Serializable;

public class CriterioInclusion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer codigo;
	private Integer codigoIndicacion;
	private String descripcion;
	private Integer criterioInclusion;
	private Integer parametro;
	private boolean selected;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getCodigoIndicacion() {
		return codigoIndicacion;
	}
	public void setCodigoIndicacion(Integer codigoIndicacion) {
		this.codigoIndicacion = codigoIndicacion;
	}
	public Integer getCriterioInclusion() {
		return criterioInclusion;
	}
	public void setCriterioInclusion(Integer criterioInclusion) {
		this.criterioInclusion = criterioInclusion;
	}
	public Integer getParametro() {
		return parametro;
	}
	public void setParametro(Integer parametro) {
		this.parametro = parametro;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	@Override
	public String toString() {
		return "CriterioInclusion [codigo=" + codigo + ", codigoIndicacion=" + codigoIndicacion + ", descripcion="
				+ descripcion + ", criterioInclusion=" + criterioInclusion + ", parametro=" + parametro + ", selected="
				+ selected + "]";
	}
}
