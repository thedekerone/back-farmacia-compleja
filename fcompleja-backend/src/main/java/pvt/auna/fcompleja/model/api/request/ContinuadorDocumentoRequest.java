package pvt.auna.fcompleja.model.api.request;

public class ContinuadorDocumentoRequest {
	
	private Integer codSolicitudEvaluacion;
	private Integer tipoMedicamento;
	private String descripcionMedicamento;
	private Integer estado;
	
	public Integer getCodSolicitudEvaluacion() {
		return codSolicitudEvaluacion;
	}
	public void setCodSolicitudEvaluacion(Integer codSolicitudEvaluacion) {
		this.codSolicitudEvaluacion = codSolicitudEvaluacion;
	}
	public Integer getTipoMedicamento() {
		return tipoMedicamento;
	}
	public void setTipoMedicamento(Integer tipoMedicamento) {
		this.tipoMedicamento = tipoMedicamento;
	}
	public String getDescripcionMedicamento() {
		return descripcionMedicamento;
	}
	public void setDescripcionMedicamento(String descripcionMedicamento) {
		this.descripcionMedicamento = descripcionMedicamento;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	
	
}
