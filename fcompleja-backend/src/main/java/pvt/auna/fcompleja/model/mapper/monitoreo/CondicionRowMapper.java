package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.ListFiltroParametroResponse;

public class CondicionRowMapper implements RowMapper<ListFiltroParametroResponse> {

	@Override
	public ListFiltroParametroResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ListFiltroParametroResponse response = new ListFiltroParametroResponse();
		
		response.setCodigoParametro(rs.getInt("COD_PARAMETRO"));
		response.setNombreParametro(rs.getString("NOMBRE"));
		
		return response;
	}

}
