package pvt.auna.fcompleja.model.api.response;

import java.util.ArrayList;
import java.util.List;

import pvt.auna.fcompleja.model.api.ArchivoFTPBean;
import pvt.auna.fcompleja.model.api.AuditResponse;



public class ResponseFTP {
	
	private AuditResponse auditResponse;
	private ArrayList<ArchivoFTPBean> dataList;

	public ResponseFTP() {
	}
	
	public ResponseFTP(AuditResponse audiResponse, ArrayList<ArchivoFTPBean> dataList) {
		this.auditResponse = audiResponse;
		this.dataList = dataList;
	}
	

	public AuditResponse getAuditResponse() {
		return auditResponse;
	}

	public void setAudiResponse(AuditResponse auditResponse) {
		this.auditResponse = auditResponse;
	}

	public ArrayList<ArchivoFTPBean> getDataList() {
		return dataList;
	}

	public void setDataList(ArrayList<ArchivoFTPBean> dataList) {
		this.dataList = dataList;
	}
	
	@Override
	public String toString() {
		return "ResponseFTP [auditResponse=" + auditResponse + ", dataList=" + dataList + "]";
	}
	
	
}
