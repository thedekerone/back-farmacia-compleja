package pvt.auna.fcompleja.model.api;

public class TipoCorreoResponse {

    private static final long serialVersionUID = 1L;

    private Long codTipoCorreo;
    private String descripTipoCorreo;
    private String asunto;
    private String cabecera;
    private String cuerpo;
    private String firma;
    private String estado;

    public Long getCodTipoCorreo() {
        return codTipoCorreo;
    }

    public void setCodTipoCorreo(Long codTipoCorreo) {
        this.codTipoCorreo = codTipoCorreo;
    }

    public String getDescripTipoCorreo() {
        return descripTipoCorreo;
    }

    public void setDescripTipoCorreo(String descripTipoCorreo) {
        this.descripTipoCorreo = descripTipoCorreo;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCabecera() {
        return cabecera;
    }

    public void setCabecera(String cabecera) {
        this.cabecera = cabecera;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
