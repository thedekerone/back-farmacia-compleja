package pvt.auna.fcompleja.model.api;

public class FiltroParametroRequest {

	private String codigoGrupo;
	private Integer codigoParametro;
	
	private String codigoParam;
	private String valueParam;
	
	public String getCodigoGrupo() {
		return codigoGrupo;
	}
	public void setCodigoGrupo(String codigoGrupo) {
		this.codigoGrupo = codigoGrupo;
	}
	public Integer getCodigoParametro() {
		return codigoParametro;
	}
	public void setCodigoParametro(Integer codigoParametro) {
		this.codigoParametro = codigoParametro;
	}
	public String getCodigoParam() {
		return codigoParam;
	}
	public void setCodigoParam(String codigoParam) {
		this.codigoParam = codigoParam;
	}
	public String getValueParam() {
		return valueParam;
	}
	public void setValueParam(String valueParam) {
		this.valueParam = valueParam;
	}
	
	@Override
	public String toString() {
		return "FiltroParametroRequest [codigoGrupo=" + codigoGrupo + ", codigoParametro=" + codigoParametro
				+ ", codigoParam=" + codigoParam + ", valueParam=" + valueParam + "]";
	}
	
	
}
