package pvt.auna.fcompleja.model.api.request;

public class EmailAlertaCmacRequest {

	private static final long serialVersionUID = 1L;

	private String fechaCmac;
	private String codLagoSolEva;
	private String paciente;
	private String diagnostico;
	private String codLargoMac;
	private String mac;

	public String getFechaCmac() {
		return fechaCmac;
	}

	public void setFechaCmac(String fechaCmac) {
		this.fechaCmac = fechaCmac;
	}

	public String getCodLagoSolEva() {
		return codLagoSolEva;
	}

	public void setCodLagoSolEva(String codLagoSolEva) {
		this.codLagoSolEva = codLagoSolEva;
	}

	public String getPaciente() {
		return paciente;
	}

	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getCodLargoMac() {
		return codLargoMac;
	}

	public void setCodLargoMac(String codLargoMac) {
		this.codLargoMac = codLargoMac;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	@Override
	public String toString() {
		return "EmailAlertaCmacRequest [fechaCmac=" + fechaCmac + ", codLagoSolEva=" + codLagoSolEva + ", paciente="
				+ paciente + ", diagnostico=" + diagnostico + ", codLargoMac=" + codLargoMac + ", mac=" + mac + "]";
	}

}
