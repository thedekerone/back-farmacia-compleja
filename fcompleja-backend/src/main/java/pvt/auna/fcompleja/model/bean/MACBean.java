package pvt.auna.fcompleja.model.bean;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MACBean implements Serializable {

	private static final long serialVersionUID = 4064670568497501327L;
	private Integer codigo;
	private String codigoLargo;
	private String descripcion;
	private String nombreComercial;
	private Integer tipoMac;
	private String tipo;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaInscripcion;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaInicioVigencia;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaFinVigencia;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaCreacion;
	private Integer codUsuario;
	private String usuarioCreacion;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaModificacion;
	private Integer CodUsuarioMod;
	private String usuarioModificacion;
	private Integer estadoMac;
	private String estado;
	private String busqueda;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getCodigoLargo() {
		return codigoLargo;
	}

	public void setCodigoLargo(String codigoLargo) {
		this.codigoLargo = codigoLargo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public Integer getTipoMac() {
		return tipoMac;
	}

	public void setTipoMac(Integer tipoMac) {
		this.tipoMac = tipoMac;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(Date fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Integer getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Integer getCodUsuarioMod() {
		return CodUsuarioMod;
	}

	public void setCodUsuarioMod(Integer codUsuarioMod) {
		CodUsuarioMod = codUsuarioMod;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Integer getEstadoMac() {
		return estadoMac;
	}

	public void setEstadoMac(Integer estadoMac) {
		this.estadoMac = estadoMac;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}	

	public String getBusqueda() {
		return busqueda;
	}

	public void setBusqueda(String busqueda) {
		this.busqueda = busqueda;
	}

	@Override
	public String toString() {
		return "MACBean [codigo=" + codigo + ", codigoLargo=" + codigoLargo + ", descripcion=" + descripcion
				+ ", nombreComercial=" + nombreComercial + ", tipoMac=" + tipoMac + ", tipo=" + tipo
				+ ", fechaInscripcion=" + fechaInscripcion + ", fechaInicioVigencia=" + fechaInicioVigencia
				+ ", fechaFinVigencia=" + fechaFinVigencia + ", fechaCreacion=" + fechaCreacion + ", codUsuario="
				+ codUsuario + ", usuarioCreacion=" + usuarioCreacion + ", fechaModificacion=" + fechaModificacion
				+ ", CodUsuarioMod=" + CodUsuarioMod + ", usuarioModificacion=" + usuarioModificacion + ", estadoMac="
				+ estadoMac + ", estado=" + estado + "]";
	}

}
