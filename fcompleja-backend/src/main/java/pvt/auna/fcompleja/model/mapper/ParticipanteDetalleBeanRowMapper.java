/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.request.ParticipanteBeanRequest;
import pvt.auna.fcompleja.model.api.request.ParticipanteDetalleBeanRequest;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ParticipanteDetalleBeanRowMapper implements RowMapper<ParticipanteDetalleBeanRequest>  {


    @Override
    public ParticipanteDetalleBeanRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
    	ParticipanteDetalleBeanRequest participante = new ParticipanteDetalleBeanRequest();
    	participante.setCodParticipante(rs.getInt("COD_PARTICIPANTE"));
    	participante.setCodGrupoDiagnostico(rs.getString("COD_GRP_DIAG"));
    	participante.setpRangoEdad(rs.getInt("P_RANGO_EDAD"));
    	
    	
        return participante;
    }

}
