package pvt.auna.fcompleja.model.api.request.monitoreo;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MonitoreoEvolucionRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer codLineaTrat;
	//@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private String fecha;
	private String codGrpDiag;

	public MonitoreoEvolucionRequest() {
		// TODO Auto-generated constructor stub
	}

	
	public Integer getCodLineaTrat() {
		return codLineaTrat;
	}


	public void setCodLineaTrat(Integer codLineaTrat) {
		this.codLineaTrat = codLineaTrat;
	}

	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getCodGrpDiag() {
		return codGrpDiag;
	}

	public void setCodGrpDiag(String codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}

	@Override
	public String toString() {
		return "MonitoreoEvolucionRequest [codLineaTrat=" + codLineaTrat + ", fecha=" + fecha + ", codGrpDiag="
				+ codGrpDiag + "]";
	}

}
