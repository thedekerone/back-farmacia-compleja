package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.ConclusionBean;

public class AnalisisConclusionRepRowMapper implements RowMapper<ConclusionBean>{

	@Override
	public ConclusionBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ConclusionBean conclusionBean = new ConclusionBean();
		
		conclusionBean.setMedicamentoEvaluado(rs.getString("medicamento_evaluado"));
		conclusionBean.setCumpleChkPaciente(rs.getString("p_cumple_chklist_per"));
		conclusionBean.setCumplePreferenciaInst(rs.getString("p_cumple_pref_insti"));
		conclusionBean.setDescripcion(rs.getString("descripcion"));
		conclusionBean.setMedicamentoPreferido(rs.getString("medicamento_preferido"));
		conclusionBean.setPertinencia(rs.getString("pertinencia"));
		conclusionBean.setTiempoUso(rs.getString("tiempo_uso"));
		conclusionBean.setCondicionPaciente(rs.getString("condicion_paciente"));
		conclusionBean.setResultadoAutorizador(rs.getString("resultado_autorizador"));
		conclusionBean.setComentario(rs.getString("Comentario"));
		conclusionBean.setNroLineaTratamiento(rs.getInt("nro_linea_tra"));
		
		return conclusionBean;
		
	}

}
