package pvt.auna.fcompleja.model.api;

public class AccesoAplicacionResponse {
	
	private Integer codAplicacion;
	private String nombreCorto;
	private String nombreLargo;
	public Integer getCodAplicacion() {
		return codAplicacion;
	}
	public void setCodAplicacion(Integer codAplicacion) {
		this.codAplicacion = codAplicacion;
	}
	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	public String getNombreLargo() {
		return nombreLargo;
	}
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}
}
