package pvt.auna.fcompleja.model.api.request;

public class FiltroMACRequest {
	private String codigoMac;
	private String descripcion;
	private String nombreComercial;

	public String getCodigoMac() {
		return codigoMac;
	}

	public void setCodigoMac(String codigoMac) {
		this.codigoMac = codigoMac;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	@Override
	public String toString() {
		return "FiltroMACRequest {" + "codigoMac=" + codigoMac + ", descripcion=" + descripcion + ", nombreComercial=" + nombreComercial + '}';
	}

}
