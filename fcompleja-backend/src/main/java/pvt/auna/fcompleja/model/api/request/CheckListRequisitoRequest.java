package pvt.auna.fcompleja.model.api.request;

public class CheckListRequisitoRequest {

	private Integer codSolEva;
	private Integer codCheckListRequisito;
	private Integer codContinuadorDoc;
	private Integer codMac;
	private Integer tipoDocumento;
	private String descripcionDocumento;
	private Integer estadoDocumento;
	private String urlDescarga;
	private Integer edad;
	private Integer tipoEvaluacion;
	private Integer codigoRolUsuario;
	private String fechaEstado;
	private Integer codigoUsuario;
	private Integer codArchivo;

	public Integer getCodSolEva() {
		return codSolEva;
	}

	public void setCodSolEva(Integer codSolEva) {
		this.codSolEva = codSolEva;
	}

	public Integer getCodCheckListRequisito() {
		return codCheckListRequisito;
	}

	public void setCodCheckListRequisito(Integer codCheckListRequisito) {
		this.codCheckListRequisito = codCheckListRequisito;
	}

	public Integer getCodContinuadorDoc() {
		return codContinuadorDoc;
	}

	public void setCodContinuadorDoc(Integer codContinuadorDoc) {
		this.codContinuadorDoc = codContinuadorDoc;
	}

	public Integer getCodMac() {
		return codMac;
	}

	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}

	public Integer getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(Integer tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDescripcionDocumento() {
		return descripcionDocumento;
	}

	public void setDescripcionDocumento(String descripcionDocumento) {
		this.descripcionDocumento = descripcionDocumento;
	}

	public Integer getEstadoDocumento() {
		return estadoDocumento;
	}

	public void setEstadoDocumento(Integer estadoDocumento) {
		this.estadoDocumento = estadoDocumento;
	}

	public String getUrlDescarga() {
		return urlDescarga;
	}

	public void setUrlDescarga(String urlDescarga) {
		this.urlDescarga = urlDescarga;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Integer getTipoEvaluacion() {
		return tipoEvaluacion;
	}

	public void setTipoEvaluacion(Integer tipoEvaluacion) {
		this.tipoEvaluacion = tipoEvaluacion;
	}

	public Integer getCodigoRolUsuario() {
		return codigoRolUsuario;
	}

	public void setCodigoRolUsuario(Integer codigoRolUsuario) {
		this.codigoRolUsuario = codigoRolUsuario;
	}

	public String getFechaEstado() {
		return fechaEstado;
	}

	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public Integer getCodArchivo() {
		return codArchivo;
	}

	public void setCodArchivo(Integer codArchivo) {
		this.codArchivo = codArchivo;
	}

	@Override
	public String toString() {
		return "CheckListRequisitoRequest [codSolEva=" + codSolEva + ", codCheckListRequisito=" + codCheckListRequisito
				+ ", codContinuadorDoc=" + codContinuadorDoc + ", codMac=" + codMac + ", tipoDocumento=" + tipoDocumento
				+ ", descripcionDocumento=" + descripcionDocumento + ", estadoDocumento=" + estadoDocumento
				+ ", UrlDescarga=" + urlDescarga + ", edad=" + edad + ", tipoEvaluacion=" + tipoEvaluacion
				+ ", codigoRolUsuario=" + codigoRolUsuario + ", fechaEstado=" + fechaEstado + ", codigoUsuario="
				+ codigoUsuario + ", codArchivo=" + codArchivo + "]";
	}

}
