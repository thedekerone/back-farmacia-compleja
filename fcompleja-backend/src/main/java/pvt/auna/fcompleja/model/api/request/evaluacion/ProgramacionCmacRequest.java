package pvt.auna.fcompleja.model.api.request.evaluacion;

import java.util.ArrayList;

import pvt.auna.fcompleja.model.api.ListaBandeja;

public class ProgramacionCmacRequest {

	private String fecha;
	private String hora;
	private String codEvaluacion;
	private Integer codArchivo;
	private String codReporteActaEscaneada;
	private ArrayList<ListaBandeja> listaEvaluacion;

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getCodEvaluacion() {
		return codEvaluacion;
	}

	public void setCodEvaluacion(String codEvaluacion) {
		this.codEvaluacion = codEvaluacion;
	}

	public ArrayList<ListaBandeja> getListaEvaluacion() {
		return listaEvaluacion;
	}

	public void setListaEvaluacion(ArrayList<ListaBandeja> listaEvaluacion) {
		this.listaEvaluacion = listaEvaluacion;
	}

	public Integer getCodArchivo() {
		return codArchivo;
	}

	public void setCodArchivo(Integer codArchivo) {
		this.codArchivo = codArchivo;
	}

	public String getCodReporteActaEscaneada() {
		return codReporteActaEscaneada;
	}

	public void setCodReporteActaEscaneada(String codReporteActaEscaneada) {
		this.codReporteActaEscaneada = codReporteActaEscaneada;
	}

	@Override
	public String toString() {
		return "ProgramacionCmacRequest [fecha=" + fecha + ", hora=" + hora + ", codEvaluacion=" + codEvaluacion
				+ ", codArchivo=" + codArchivo + ", codReporteActaEscaneada=" + codReporteActaEscaneada
				+ ", listaEvaluacion=" + listaEvaluacion + "]";
	}

}
