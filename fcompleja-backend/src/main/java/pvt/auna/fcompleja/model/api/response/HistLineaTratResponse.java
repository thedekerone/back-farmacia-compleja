package pvt.auna.fcompleja.model.api.response;

import java.util.List;

import pvt.auna.fcompleja.model.bean.HistLineaTratBean;

public class HistLineaTratResponse {
		
	private List<HistLineaTratBean> lista;
	private Integer codigo;
	private String mensaje;
	
	public List<HistLineaTratBean> getLista() {
		return lista;
	}
	public void setLista(List<HistLineaTratBean> lista) {
		this.lista = lista;
	}
	public Integer getCodigo() {
		return codigo;
	}	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


}
