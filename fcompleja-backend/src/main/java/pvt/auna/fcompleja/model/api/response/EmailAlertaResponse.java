package pvt.auna.fcompleja.model.api.response;

public class EmailAlertaResponse {

    private static final long serialVersionUID = -8022474255483626723L;

    private int codUsuario;
    private String usuario;    
    private boolean envioCorreo;
    private String errorCorreo;
    //private int codGrupDiag;
    //private int codRol;


    public int getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(int codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public boolean isEnvioCorreo() {
        return envioCorreo;
    }

    public void setEnvioCorreo(boolean envioCorreo) {
        this.envioCorreo = envioCorreo;
    }

    public String getErrorCorreo() {
        return errorCorreo;
    }

    public void setErrorCorreo(String errorCorreo) {
        this.errorCorreo = errorCorreo;
    }

	    
    
    
}
