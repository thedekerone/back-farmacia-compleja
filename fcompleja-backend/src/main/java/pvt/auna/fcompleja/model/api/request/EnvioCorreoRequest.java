package pvt.auna.fcompleja.model.api.request;

public class EnvioCorreoRequest {
	
	private String usrApp;
	private String codigoEnvio;
	private Integer codSolicitudEvaluacion;
	private Integer estadoSolicitudEvaluacion;
	
	public String getUsrApp() {
		return usrApp;
	}
	public void setUsrApp(String usrApp) {
		this.usrApp = usrApp;
	}
	public String getCodigoEnvio() {
		return codigoEnvio;
	}
	public void setCodigoEnvio(String codigoEnvio) {
		this.codigoEnvio = codigoEnvio;
	}
	public Integer getCodSolicitudEvaluacion() {
		return codSolicitudEvaluacion;
	}
	public void setCodSolicitudEvaluacion(Integer codSolicitudEvaluacion) {
		this.codSolicitudEvaluacion = codSolicitudEvaluacion;
	}
	public Integer getEstadoSolicitudEvaluacion() {
		return estadoSolicitudEvaluacion;
	}
	public void setEstadoSolicitudEvaluacion(Integer estadoSolicitudEvaluacion) {
		this.estadoSolicitudEvaluacion = estadoSolicitudEvaluacion;
	}
	
	
	
}
