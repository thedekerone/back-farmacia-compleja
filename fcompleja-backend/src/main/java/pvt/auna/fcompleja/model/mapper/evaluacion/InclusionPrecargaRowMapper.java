package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.CriterioInclusion;

public class InclusionPrecargaRowMapper implements RowMapper<CriterioInclusion> {

	@Override
	public CriterioInclusion mapRow(ResultSet rs, int rowNum) throws SQLException {
		CriterioInclusion criterio = new CriterioInclusion();
		criterio.setCodigo(rs.getInt("COD_CRITERIO_INCLU"));
		criterio.setCriterioInclusion(rs.getInt("P_CRITERIO_INCLU"));
		return criterio;
	}

}
