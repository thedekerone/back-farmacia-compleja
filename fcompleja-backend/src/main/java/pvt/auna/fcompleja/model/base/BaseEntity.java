package pvt.auna.fcompleja.model.base;

import java.util.Date;

public interface BaseEntity {


    public String getUsuarioCreaVch() ;

	public void setUsuarioCreaVch(String usuarioCreaVch) ;

	public Date getFechaCreaDat();

	public void setFechaCreaDat(Date fechaCreaDat) ;

	public String getUsuarioModifVch() ;

	public void setUsuarioModifVch(String usuarioModifVch) ;

	public Date getFechaModifDat() ;

	public void setFechaModifDat(Date fechaModifDat) ;


}
