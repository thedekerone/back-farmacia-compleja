/**
 * 
 */
package pvt.auna.fcompleja.model.api.request;

import java.util.List;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ParticipanteBeanRequest {
	
	private Integer codParticipante;
	private Integer codUsuario;
	private String estadoParticipante;
	private String cmpMedico;
	private String nombreFirma;
	private Integer codRol;
	private String  descripcionRol;
	private Integer codigoArchivoFirma;
	private String correoElectronico;
	private String nombres;
	private String apellidos;
	private String codParticipanteLargo;
	private Integer pEstado;
	private Integer coordinador;
	
    private List<ParticipanteDetalleBeanRequest> gruposDiagnosticos;
	/**
	 * 
	 */
	public ParticipanteBeanRequest() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param codParticipante
	 * @param codUsuario
	 * @param estadoParticipante
	 * @param cmpMedico
	 * @param nombreFirma
	 * @param codRol
	 * @param codigoArchivoFrima
	 * @param correoElectronico
	 * @param nombres
	 * @param apellidos
	 * @param codParticipanteLargo
	 * @param pEstado
	 * @param coordinador
	 * @param gruposDiagnosticos
	 */
	public ParticipanteBeanRequest(Integer codParticipante, Integer codUsuario, String estadoParticipante,
			String cmpMedico, String nombreFirma, Integer codRol, Integer codigoArchivoFirma, String correoElectronico,
			String nombres, String apellidos, String codParticipanteLargo, Integer pEstado, Integer coordinador,
			List<ParticipanteDetalleBeanRequest> gruposDiagnosticos) {
		super();
		this.codParticipante = codParticipante;
		this.codUsuario = codUsuario;
		this.estadoParticipante = estadoParticipante;
		this.cmpMedico = cmpMedico;
		this.nombreFirma = nombreFirma;
		this.codRol = codRol;
		this.codigoArchivoFirma = codigoArchivoFirma;
		this.correoElectronico = correoElectronico;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.codParticipanteLargo = codParticipanteLargo;
		this.pEstado = pEstado;
		this.coordinador = coordinador;
		this.gruposDiagnosticos = gruposDiagnosticos;
	}
	/**
	 * @return the codParticipante
	 */
	public Integer getCodParticipante() {
		return codParticipante;
	}
	/**
	 * @param codParticipante the codParticipante to set
	 */
	public void setCodParticipante(Integer codParticipante) {
		this.codParticipante = codParticipante;
	}
	/**
	 * @return the codUsuario
	 */
	public Integer getCodUsuario() {
		return codUsuario;
	}
	/**
	 * @param codUsuario the codUsuario to set
	 */
	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}
	/**
	 * @return the estadoParticipante
	 */
	public String getEstadoParticipante() {
		return estadoParticipante;
	}
	/**
	 * @param estadoParticipante the estadoParticipante to set
	 */
	public void setEstadoParticipante(String estadoParticipante) {
		this.estadoParticipante = estadoParticipante;
	}
	/**
	 * @return the cmpMedico
	 */
	public String getCmpMedico() {
		return cmpMedico;
	}
	/**
	 * @param cmpMedico the cmpMedico to set
	 */
	public void setCmpMedico(String cmpMedico) {
		this.cmpMedico = cmpMedico;
	}
	/**
	 * @return the nombreFirma
	 */
	public String getNombreFirma() {
		return nombreFirma;
	}
	/**
	 * @param nombreFirma the nombreFirma to set
	 */
	public void setNombreFirma(String nombreFirma) {
		this.nombreFirma = nombreFirma;
	}
	/**
	 * @return the codRol
	 */
	public Integer getCodRol() {
		return codRol;
	}
	/**
	 * @param codRol the codRol to set
	 */
	public void setCodRol(Integer codRol) {
		this.codRol = codRol;
	}
	/**
	 * @return the codigoArchivoFrima
	 */
	public Integer getCodigoArchivoFirma() {
		return codigoArchivoFirma;
	}
	/**
	 * @param codigoArchivoFrima the codigoArchivoFrima to set
	 */
	public void setCodigoArchivoFirma(Integer codigoArchivoFrima) {
		this.codigoArchivoFirma = codigoArchivoFrima;
	}
	/**
	 * @return the correoElectronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	/**
	 * @param correoElectronico the correoElectronico to set
	 */
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	/**
	 * @return the nombres
	 */
	public String getNombres() {
		return nombres;
	}
	/**
	 * @param nombres the nombres to set
	 */
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	/**
	 * @return the apellidos
	 */
	public String getApellidos() {
		return apellidos;
	}
	/**
	 * @param apellidos the apellidos to set
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	/**
	 * @return the codParticipanteLargo
	 */
	public String getCodParticipanteLargo() {
		return codParticipanteLargo;
	}
	/**
	 * @param codParticipanteLargo the codParticipanteLargo to set
	 */
	public void setCodParticipanteLargo(String codParticipanteLargo) {
		this.codParticipanteLargo = codParticipanteLargo;
	}
	/**
	 * @return the pEstado
	 */
	public Integer getpEstado() {
		return pEstado;
	}
	/**
	 * @param pEstado the pEstado to set
	 */
	public void setpEstado(Integer pEstado) {
		this.pEstado = pEstado;
	}
	/**
	 * @return the coordinador
	 */
	public Integer getCoordinador() {
		return coordinador;
	}
	/**
	 * @param coordinador the coordinador to set
	 */
	public void setCoordinador(Integer coordinador) {
		this.coordinador = coordinador;
	}
	/**
	 * @return the gruposDiagnosticos
	 */
	public List<ParticipanteDetalleBeanRequest> getGruposDiagnosticos() {
		return gruposDiagnosticos;
	}
	/**
	 * @param gruposDiagnosticos the gruposDiagnosticos to set
	 */
	public void setGruposDiagnosticos(List<ParticipanteDetalleBeanRequest> gruposDiagnosticos) {
		this.gruposDiagnosticos = gruposDiagnosticos;
	}
	
	
	/**
	 * @return the descripcionRol
	 */
	public String getDescripcionRol() {
		return descripcionRol;
	}
	/**
	 * @param descripcionRol the descripcionRol to set
	 */
	public void setDescripcionRol(String descripcionRol) {
		this.descripcionRol = descripcionRol;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParticipanteBeanRequest [codParticipante=" + codParticipante + ", codUsuario=" + codUsuario
				+ ", estadoParticipante=" + estadoParticipante + ", cmpMedico=" + cmpMedico + ", nombreFirma="
				+ nombreFirma + ", codRol=" + codRol + ", codigoArchivoFirma=" + codigoArchivoFirma
				+ ", correoElectronico=" + correoElectronico + ", nombres=" + nombres + ", apellidos=" + apellidos
				+ ", codParticipanteLargo=" + codParticipanteLargo + ", pEstado=" + pEstado + ", coordinador="
				+ coordinador + ", gruposDiagnosticos=" + gruposDiagnosticos + ", toString()=" + super.toString() + "]";
	}
	
	

}
