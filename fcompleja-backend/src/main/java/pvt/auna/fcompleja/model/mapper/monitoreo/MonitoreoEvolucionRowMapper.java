package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoEvolucionResponse;

public class MonitoreoEvolucionRowMapper implements RowMapper<MonitoreoEvolucionResponse> {
	@Override
	public MonitoreoEvolucionResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		MonitoreoEvolucionResponse evo = new MonitoreoEvolucionResponse();

		evo.setCodigoMonitoreo(rs.getLong("COD_MONITOREO"));
		evo.setDescCodigoMonitoreo(rs.getString("DESC_COD_MONITOREO"));
		evo.setEdadPaciente(rs.getInt("EDAD_PACIENTE"));
		evo.setNomMedicamento(rs.getString("NOM_MEDICAMENTO"));
		evo.setCodMedicamento(rs.getLong("COD_MEDICAMENTO"));
		evo.setCodEstadoMonitoreo(rs.getInt("COD_ESTADOMONITOREO"));
		evo.setNomEstadoMonitoreo(rs.getString("DESC_ESTADOMONITOREO"));
		evo.setFechaMonitoreo(rs.getString("FEC_MONITOREO"));
		evo.setFechaProxMonitoreo(rs.getString("FEC_PROX_MONITOREO"));
		evo.setNomResponsableMonitoreo(rs.getString("NOM_RESPONSABLEMONITOREO"));
		evo.setCodResponsableMonitoreo(rs.getLong("COD_RESPONSABLEMONITOREO"));
		evo.setCodHistLineaTrat(rs.getLong("COD_HIST_LINEA_TRAT"));
		evo.setCodEvolucion(rs.getLong("COD_EVOLUCION"));
		evo.setCodResEvolucion(rs.getInt("P_RES_EVOLUCION"));
		evo.setDescResEvolucion(rs.getString("DESC_RES_EVOLUCION"));

		return evo;
	}

}
