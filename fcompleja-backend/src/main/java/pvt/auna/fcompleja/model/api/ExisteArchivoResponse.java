package pvt.auna.fcompleja.model.api;

public class ExisteArchivoResponse {
    private Integer existe;
    private String mensaje;

    public Integer getExiste() {
        return existe;
    }

    public void setExiste(Integer existe) {
        this.existe = existe;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "ExisteArchivoResponse{" +
                "existe=" + existe +
                ", mensaje='" + mensaje + '\'' +
                '}';
    }
}