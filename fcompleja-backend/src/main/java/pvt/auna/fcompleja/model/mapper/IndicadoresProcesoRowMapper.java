package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.IndicadoresProcesoResponse;


public class IndicadoresProcesoRowMapper implements RowMapper<IndicadoresProcesoResponse>{

	@Override
	public IndicadoresProcesoResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		IndicadoresProcesoResponse indicadoresProcesoResponse = new IndicadoresProcesoResponse();		
		
		indicadoresProcesoResponse.setIndicador(rs.getString("INDICADOR"));
		indicadoresProcesoResponse.setCantidad(rs.getDouble("CANTIDAD"));
		indicadoresProcesoResponse.setNumerador(rs.getString("NUMERADOR"));
		indicadoresProcesoResponse.setDenominador(rs.getString("DENOMINADOR"));
		indicadoresProcesoResponse.setQuiebre(rs.getString("QUIEBRE"));
		indicadoresProcesoResponse.setFecMes(rs.getInt("FEC_MES"));
		indicadoresProcesoResponse.setFecAno(rs.getInt("FEC_ANO"));
		
		return indicadoresProcesoResponse;
	}

}

