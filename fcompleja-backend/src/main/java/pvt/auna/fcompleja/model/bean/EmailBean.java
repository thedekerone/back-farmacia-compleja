package pvt.auna.fcompleja.model.bean;
import java.io.Serializable;
import java.util.List;

import pvt.auna.fcompleja.model.api.CasosEvaluar;

public class EmailBean implements Serializable {

	private static final long serialVersionUID = 5657315446492872476L;

	private String usrApp;
	private Integer flagAdjunto;
	private Integer tipoEnvio;
	private String asunto;
	private String cuerpo;
	private String ruta;
	private String destinatario;
	private String fechaProgramada;
	private String codigoPlantilla;
	private String codigoEnvio;
	private String codPlantilla;
	private String   codigoGrupoDiagnostico;
	private Integer  edadPaciente;
	private Integer codSolicitudEvaluacion;
	private Integer estadoSolicitudEvaluacion;
	private Integer codRol;	
	private List<CasosEvaluar> listaCasosEvaluar;
	
	
	
	

	public Integer getCodSolicitudEvaluacion() {
		return codSolicitudEvaluacion;
	}

	public void setCodSolicitudEvaluacion(Integer codSolicitudEvaluacion) {
		this.codSolicitudEvaluacion = codSolicitudEvaluacion;
	}

	public String getCodPlantilla() {
		return codPlantilla;
	}

	public void setCodPlantilla(String codPlantilla) {
		this.codPlantilla = codPlantilla;
	}

	public String getCodigoPlantilla() {
		return codigoPlantilla;
	}

	public void setCodigoPlantilla(String codigoPlantilla) {
		this.codigoPlantilla = codigoPlantilla;
	}

	public String getCodigoEnvio() {
		return codigoEnvio;
	}

	public void setCodigoEnvio(String codigoEnvio) {
		this.codigoEnvio = codigoEnvio;
	}

	public String getUsrApp() {
		return usrApp;
	}

	public void setUsrApp(String usrApp) {
		this.usrApp = usrApp;
	}

	public Integer getFlagAdjunto() {
		return flagAdjunto;
	}

	public void setFlagAdjunto(Integer flagAdjunto) {
		this.flagAdjunto = flagAdjunto;
	}

	public Integer getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(Integer tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getFechaProgramada() {
		return fechaProgramada;
	}

	public void setFechaProgramada(String fechaProgramada) {
		this.fechaProgramada = fechaProgramada;
	}

	public String getCodigoGrupoDiagnostico() {
		return codigoGrupoDiagnostico;
	}

	public void setCodigoGrupoDiagnostico(String codigoGrupoDiagnostico) {
		this.codigoGrupoDiagnostico = codigoGrupoDiagnostico;
	}

	public Integer getEdadPaciente() {
		return edadPaciente;
	}

	public void setEdadPaciente(Integer edadPaciente) {
		this.edadPaciente = edadPaciente;
	}
	
	public Integer getEstadoSolicitudEvaluacion() {
		return estadoSolicitudEvaluacion;
	}

	public void setEstadoSolicitudEvaluacion(Integer estadoSolicitudEvaluacion) {
		this.estadoSolicitudEvaluacion = estadoSolicitudEvaluacion;
	}

	
	public Integer getCodRol() {
		return codRol;
	}

	public void setCodRol(Integer codRol) {
		this.codRol = codRol;
	}

	public List<CasosEvaluar> getListaCasosEvaluar() {
		return listaCasosEvaluar;
	}

	public void setListaCasosEvaluar(List<CasosEvaluar> listaCasosEvaluar) {
		this.listaCasosEvaluar = listaCasosEvaluar;
	}

	@Override
	public String toString() {
		return "EmailBean [usrApp=" + usrApp + ", flagAdjunto=" + flagAdjunto + ", tipoEnvio=" + tipoEnvio + ", asunto="
				+ asunto + ", cuerpo=" + cuerpo + ", ruta=" + ruta + ", destinatario=" + destinatario
				+ ", fechaProgramada=" + fechaProgramada + ", codigoPlantilla=" + codigoPlantilla + ", codigoEnvio="
				+ codigoEnvio + ", codPlantilla=" + codPlantilla + ", codigoGrupoDiagnostico=" + codigoGrupoDiagnostico
				+ ", edadPaciente=" + edadPaciente + ", codSolicitudEvaluacion=" + codSolicitudEvaluacion
				+ ", estadoSolicitudEvaluacion=" + estadoSolicitudEvaluacion + ", codRol=" + codRol
				+ ", listaCasosEvaluar=" + listaCasosEvaluar + "]";
	}
	
	

}
