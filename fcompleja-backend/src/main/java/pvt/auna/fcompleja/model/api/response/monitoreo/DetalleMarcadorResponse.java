package pvt.auna.fcompleja.model.api.response.monitoreo;

import java.io.Serializable;

public class DetalleMarcadorResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codDetMarcador;
	private Long codMarcador;
	private String valorFijo;

	public DetalleMarcadorResponse() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodDetMarcador() {
		return codDetMarcador;
	}

	public void setCodDetMarcador(Long codDetMarcador) {
		this.codDetMarcador = codDetMarcador;
	}

	public Long getCodMarcador() {
		return codMarcador;
	}

	public void setCodMarcador(Long codMarcador) {
		this.codMarcador = codMarcador;
	}

	public String getValorFijo() {
		return valorFijo;
	}

	public void setValorFijo(String valorFijo) {
		this.valorFijo = valorFijo;
	}

	@Override
	public String toString() {
		return "DetalleMarcadorResponse [codDetMarcador=" + codDetMarcador + ", codMarcador=" + codMarcador
				+ ", valorFijo=" + valorFijo + "]";
	}

}
