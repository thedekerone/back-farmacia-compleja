package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.AccesoAplicacionResponse;

public class UsuarioAplicacionRowMapper implements RowMapper<AccesoAplicacionResponse> {

	@Override
	public AccesoAplicacionResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		AccesoAplicacionResponse aplicacion = new AccesoAplicacionResponse();
		
		aplicacion.setCodAplicacion(rs.getInt("COD_APLICACION"));
		aplicacion.setNombreCorto(rs.getString("NOM_CORTO"));
		aplicacion.setNombreLargo(rs.getString("NOM_LARGO"));

		return aplicacion;
	}

}
