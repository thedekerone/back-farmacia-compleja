/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.ListaExamenesMedicos;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ListaExamenesMedicosRowMapper implements RowMapper<ListaExamenesMedicos>{

	@Override
	public ListaExamenesMedicos mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		ListaExamenesMedicos listaExamenesMedicos = new ListaExamenesMedicos();
		
		listaExamenesMedicos.setCodExamenMed(rs.getInt("COD_EXAMEN_MED"));
		listaExamenesMedicos.setCodExamenMedLargo(rs.getString("COD_EXAMEN_MED_LARGO"));
		listaExamenesMedicos.setDescripcion(rs.getString("DESCRIPCION"));
		listaExamenesMedicos.setCodTipoExamen(rs.getInt("COD_TIPO_EXAMEN"));
		listaExamenesMedicos.setTipo(rs.getString("TIPO"));
		listaExamenesMedicos.setCodEstadoExamenMed(rs.getInt("COD_ESTADO_EXAM_MED"));
		listaExamenesMedicos.setEstadoExamenMed(rs.getString("ESTADO_EXAM_MED"));
		listaExamenesMedicos.setCodExamenMedDet(rs.getInt("COD_EXAMEN_MED_DET"));
		listaExamenesMedicos.setCodTipoIngresoRes(rs.getInt("COD_TIPO_INGRESO_RES"));
		listaExamenesMedicos.setTipoIngresoRes(rs.getString("TIPO_INGRESO_RES"));
		listaExamenesMedicos.setUnidadMedida((rs.getString("UNIDAD_MEDIDA") != null) ? rs.getString("UNIDAD_MEDIDA") : "");
		listaExamenesMedicos.setRango((rs.getString("RANGO") != null) ? rs.getString("RANGO") : "");
		listaExamenesMedicos.setValorFijo((rs.getString("VALOR_FIJO") != null) ? rs.getString("VALOR_FIJO") : "");
		listaExamenesMedicos.setCodEstadoExamenMedDet(rs.getInt("COD_ESTADO_EXAM_MED_DET"));
		listaExamenesMedicos.setEstadoExamenMedDet(rs.getString("ESTADO_EXAM_MED_DET"));
		listaExamenesMedicos.setRangoMinimo( rs.getInt("RANGO_MINIMO") );
		listaExamenesMedicos.setRangoMaximo( rs.getInt("RANGO_MAXIMO") );
		
		

		return listaExamenesMedicos;
	}

}
