package pvt.auna.fcompleja.model.api.request.evaluacion;

import java.io.Serializable;
import java.util.ArrayList;

public class AntecedenteOncoFam implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	Boolean aplica;
	ArrayList<AntecedenteOncoFamDet> familiar_1er_grado;
	ArrayList<AntecedenteOncoFamDet> familiar_2do_grado;
	//String otros;
	
	public Boolean getAplica() {
		return aplica;
	}
	public void setAplica(Boolean aplica) {
		this.aplica = aplica;
	}
	public ArrayList<AntecedenteOncoFamDet> getFamiliar_1er_grado() {
		return familiar_1er_grado;
	}
	public void setFamiliar_1er_grado(ArrayList<AntecedenteOncoFamDet> familiar_1er_grado) {
		this.familiar_1er_grado = familiar_1er_grado;
	}
	public ArrayList<AntecedenteOncoFamDet> getFamiliar_2do_grado() {
		return familiar_2do_grado;
	}
	public void setFamiliar_2do_grado(ArrayList<AntecedenteOncoFamDet> familiar_2do_grado) {
		this.familiar_2do_grado = familiar_2do_grado;
	}
	/*public String getOtros() {
		return otros;
	}
	public void setOtros(String otros) {
		this.otros = otros;
	}*/
	
	

}
