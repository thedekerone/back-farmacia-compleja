/**
 * 
 */
package pvt.auna.fcompleja.model.bean;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class UsuarioRolBean {
	
	private String codUsuario;
	private String nomUsuario;
	private String nombres;
	private String apellidos;
	private Integer codRol;
	private String nomRol;
	/**
	 * 
	 */
	public UsuarioRolBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param codUsuario
	 * @param nomUsuario
	 * @param nombres
	 * @param apellidos
	 * @param codRol
	 * @param nomRol
	 
	 */
	public UsuarioRolBean(String codUsuario, String nomUsuario,String nombres,String apellidos, Integer codRol, String nomRol) {
		super();
		this.codUsuario = codUsuario;
		this.nomUsuario = nomUsuario;
		this.codRol = codRol;
		this.nomRol = nomRol;
		this.nombres = nombres;
		this.apellidos = apellidos;
	}
	/**
	 * @return the codUsuario
	 */
	public String getCodUsuario() {
		return codUsuario;
	}
	/**
	 * @param codUsuario the codUsuario to set
	 */
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	/**
	 * @return the nomUsuario
	 */
	public String getNomUsuario() {
		return nomUsuario;
	}
	/**
	 * @param nomUsuario the nomUsuario to set
	 */
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}
	/**
	 * @return the codRol
	 */
	public Integer getCodRol() {
		return codRol;
	}
	/**
	 * @param codRol the codRol to set
	 */
	public void setCodRol(Integer codRol) {
		this.codRol = codRol;
	}
	/**
	 * @return the nomRol
	 */
	public String getNomRol() {
		return nomRol;
	}
	/**
	 * @param nomRol the nomRol to set
	 */
	public void setNomRol(String nomRol) {
		this.nomRol = nomRol;
	}
	
	
	
	/**
	 * @return the nombres
	 */
	public String getNombres() {
		return nombres;
	}
	/**
	 * @param nombres the nombres to set
	 */
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	/**
	 * @return the apellidos
	 */
	public String getApellidos() {
		return apellidos;
	}
	/**
	 * @param apellidos the apellidos to set
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UsuarioRolBean [codUsuario=" + codUsuario + ", nomUsuario=" + nomUsuario + ", nombres=" + nombres
				+ ", apellidos=" + apellidos + ", codRol=" + codRol + ", nomRol=" + nomRol + ", toString()="
				+ super.toString() + "]";
	}
	
	


}
