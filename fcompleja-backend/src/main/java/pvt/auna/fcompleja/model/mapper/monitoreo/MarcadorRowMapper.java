package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.monitoreo.MarcadorResponse;

public class MarcadorRowMapper implements RowMapper<MarcadorResponse>{
	@Override
	public MarcadorResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		MarcadorResponse marcador = new MarcadorResponse();

		marcador.setCodConfigMarca(rs.getLong("COD_CONFIG_MARCA"));
		marcador.setCodMarcador(rs.getLong("COD_MARCADOR"));
		marcador.setDescripcion(rs.getString("DESCRIPCION"));
		marcador.setRangoMinimo(rs.getInt("RANGO_MINIMO"));
		marcador.setRangoMaximo(rs.getInt("RANGO_MAXIMO"));
		marcador.setpPerMinima(rs.getInt("P_PER_MINIMA"));
		marcador.setpPerMaxima(rs.getInt("P_PER_MAXIMA"));
		marcador.setValPerMinima(Integer.parseInt(rs.getString("VAL_PER_MINIMA")));
		marcador.setValPerMaxima(Integer.parseInt(rs.getString("VAL_PER_MAXIMA")));
		marcador.setDescPerMinima(rs.getString("DESC_PER_MINIMA"));
		marcador.setDescPerMaxima(rs.getString("DESC_PER_MAXIMA"));
		marcador.setRango(rs.getString("RANGO"));
		marcador.setpTipoIngresoRes(rs.getInt("P_TIPO_INGRESO_RES"));
		marcador.setDescTipoIngresoRes(rs.getString("DESC_TIPO_INGRESO_RES"));
		marcador.setUnidadMedida(rs.getString("UNIDAD_MEDIDA"));

		return marcador;
	}
}
