/**
 * 
 */
package pvt.auna.fcompleja.model.bean;

import java.util.Date;
import java.util.List;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class ExamenMedicoBean {
	
	private Integer codExamenMed;
	private String  codExamenMedLargo;
	private String  descripcion;
	private Integer tipoExamen;
	private String  examen;
	private Integer codEstado;
	private String  estado;
	private String  usuarioCrea;
	private Date    fechaCrea;
	private String  usuarioModif;
	private Date	fechaModif;
	private List<ExamenMedicoDetalleBean> lExamenMedicoDetalle;
	
	/**
	 * 
	 */
	public ExamenMedicoBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param codExamenMed
	 * @param codExamenMedLargo
	 * @param descripcion
	 * @param tipoExamen
	 * @param examen
	 * @param codEstado
	 * @param estado
	 * @param usuarioCrea
	 * @param fechaCrea
	 * @param usuarioModif
	 * @param fechaModif
	 */
	public ExamenMedicoBean(Integer codExamenMed, String codExamenMedLargo, String descripcion, Integer tipoExamen,
			String examen, Integer codEstado, String estado, String usuarioCrea, Date fechaCrea, String usuarioModif,
			Date fechaModif) {
		super();
		this.codExamenMed = codExamenMed;
		this.codExamenMedLargo = codExamenMedLargo;
		this.descripcion = descripcion;
		this.tipoExamen = tipoExamen;
		this.examen = examen;
		this.codEstado = codEstado;
		this.estado = estado;
		this.usuarioCrea = usuarioCrea;
		this.fechaCrea = fechaCrea;
		this.usuarioModif = usuarioModif;
		this.fechaModif = fechaModif;
	}
	/**
	 * @return the codExamenMed
	 */
	public Integer getCodExamenMed() {
		return codExamenMed;
	}
	/**
	 * @param codExamenMed the codExamenMed to set
	 */
	public void setCodExamenMed(Integer codExamenMed) {
		this.codExamenMed = codExamenMed;
	}
	/**
	 * @return the codExamenMedLargo
	 */
	public String getCodExamenMedLargo() {
		return codExamenMedLargo;
	}
	/**
	 * @param codExamenMedLargo the codExamenMedLargo to set
	 */
	public void setCodExamenMedLargo(String codExamenMedLargo) {
		this.codExamenMedLargo = codExamenMedLargo;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the tipoExamen
	 */
	public Integer getTipoExamen() {
		return tipoExamen;
	}
	/**
	 * @param tipoExamen the tipoExamen to set
	 */
	public void setTipoExamen(Integer tipoExamen) {
		this.tipoExamen = tipoExamen;
	}
	/**
	 * @return the examen
	 */
	public String getExamen() {
		return examen;
	}
	/**
	 * @param examen the examen to set
	 */
	public void setExamen(String examen) {
		this.examen = examen;
	}
	/**
	 * @return the codEstado
	 */
	public Integer getCodEstado() {
		return codEstado;
	}
	/**
	 * @param codEstado the codEstado to set
	 */
	public void setCodEstado(Integer codEstado) {
		this.codEstado = codEstado;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the usuarioCrea
	 */
	public String getUsuarioCrea() {
		return usuarioCrea;
	}
	/**
	 * @param usuarioCrea the usuarioCrea to set
	 */
	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}
	/**
	 * @return the fechaCrea
	 */
	public Date getFechaCrea() {
		return fechaCrea;
	}
	/**
	 * @param fechaCrea the fechaCrea to set
	 */
	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}
	/**
	 * @return the usuarioModif
	 */
	public String getUsuarioModif() {
		return usuarioModif;
	}
	/**
	 * @param usuarioModif the usuarioModif to set
	 */
	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}
	/**
	 * @return the fechaModif
	 */
	public Date getFechaModif() {
		return fechaModif;
	}
	/**
	 * @param fechaModif the fechaModif to set
	 */
	public void setFechaModif(Date fechaModif) {
		this.fechaModif = fechaModif;
	}
	
	
	
	/**
	 * @return the lExamenMedicoDetalle
	 */
	public List<ExamenMedicoDetalleBean> getlExamenMedicoDetalle() {
		return lExamenMedicoDetalle;
	}
	/**
	 * @param lExamenMedicoDetalle the lExamenMedicoDetalle to set
	 */
	public void setlExamenMedicoDetalle(List<ExamenMedicoDetalleBean> lExamenMedicoDetalle) {
		this.lExamenMedicoDetalle = lExamenMedicoDetalle;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ExamenMedicoBean [codExamenMed=" + codExamenMed + ", codExamenMedLargo=" + codExamenMedLargo
				+ ", descripcion=" + descripcion + ", tipoExamen=" + tipoExamen + ", examen=" + examen + ", codEstado="
				+ codEstado + ", estado=" + estado + ", usuarioCrea=" + usuarioCrea + ", fechaCrea=" + fechaCrea
				+ ", usuarioModif=" + usuarioModif + ", fechaModif=" + fechaModif + ", lExamenMedicoDetalle="
				+ lExamenMedicoDetalle + ", toString()=" + super.toString() + "]";
	}
	
	
	
	

}
