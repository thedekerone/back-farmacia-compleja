/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorGrupoMacLineaResponse;
import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorGrupoMacResponse;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class IndicadorConsumoPorGrupoMacLineaRowMapper implements RowMapper<IndicadorConsumoPorGrupoMacLineaResponse>{

	@Override
	public IndicadorConsumoPorGrupoMacLineaResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		IndicadorConsumoPorGrupoMacLineaResponse indicadorConsumoPorGrupoMacLineaResponse = new IndicadorConsumoPorGrupoMacLineaResponse();		
		
		indicadorConsumoPorGrupoMacLineaResponse.setLineaTratamiento(rs.getInt("LINEA_TRATAMIENTO"));
		indicadorConsumoPorGrupoMacLineaResponse.setCodigoGrupoDiagnostico(rs.getInt("COD_GRP_DIAG"));
		indicadorConsumoPorGrupoMacLineaResponse.setCodigoMac(rs.getInt("COD_MAC"));
		indicadorConsumoPorGrupoMacLineaResponse.setDescripcion(rs.getString("DESCRIPCION"));
		indicadorConsumoPorGrupoMacLineaResponse.setGastoTotal(rs.getDouble("GASTO_TOTAL"));
		indicadorConsumoPorGrupoMacLineaResponse.setGastoPaciente(rs.getDouble("G_PAC"));
		indicadorConsumoPorGrupoMacLineaResponse.setNroPacientes(rs.getInt("NRO_PACIENTES"));
		indicadorConsumoPorGrupoMacLineaResponse.setNroNuevos(rs.getInt("NRO_NUEVOS"));
		indicadorConsumoPorGrupoMacLineaResponse.setNroContinuadores(rs.getInt("NRO_CONTINUADOR"));
		
		return indicadorConsumoPorGrupoMacLineaResponse;
	}

}