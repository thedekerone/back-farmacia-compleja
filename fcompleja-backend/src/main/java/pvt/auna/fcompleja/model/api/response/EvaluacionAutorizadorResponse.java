package pvt.auna.fcompleja.model.api.response;

public class EvaluacionAutorizadorResponse {
	
	private Integer codHistLineaTrat;
	private Integer codMac;
	private Integer nroLineaTratamiento;
	private Integer estadoMonitoreoMedic;
	private String  fecInicio;
	private String  fecFin;
	private String 	fecSolEva;
	private String 	codGrpDiag;
	
	
	public Integer getCodHistLineaTrat() {
		return codHistLineaTrat;
	}
	public void setCodHistLineaTrat(Integer codHistLineaTrat) {
		this.codHistLineaTrat = codHistLineaTrat;
	}
	public Integer getCodMac() {
		return codMac;
	}
	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}
	public Integer getNroLineaTratamiento() {
		return nroLineaTratamiento;
	}
	public void setNroLineaTratamiento(Integer nroLineaTratamiento) {
		this.nroLineaTratamiento = nroLineaTratamiento;
	}
	public Integer getEstadoMonitoreoMedic() {
		return estadoMonitoreoMedic;
	}
	public void setEstadoMonitoreoMedic(Integer estadoMonitoreoMedic) {
		this.estadoMonitoreoMedic = estadoMonitoreoMedic;
	}
	public String getFecInicio() {
		return fecInicio;
	}
	public void setFecInicio(String fecInicio) {
		this.fecInicio = fecInicio;
	}
	public String getFecSolEva() {
		return fecSolEva;
	}
	public void setFecSolEva(String fecSolEva) {
		this.fecSolEva = fecSolEva;
	}
	public String getFecFin() {
		return fecFin;
	}
	public void setFecFin(String fecFin) {
		this.fecFin = fecFin;
	}
	public String getCodGrpDiag() {
		return codGrpDiag;
	}
	public void setCodGrpDiag(String codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}
	
}
