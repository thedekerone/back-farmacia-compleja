package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.HistPacienteResponse;

public class HistorialLineaTratRowMapper implements RowMapper<HistPacienteResponse> {

	@Override
	public HistPacienteResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		HistPacienteResponse response = new HistPacienteResponse();
		
		response.setCodChekListReq(rs.getInt("COD_DOCUMENTO_EVALUACION"));
		response.setLineaTratamiento(rs.getInt("LINEA_TRATAMIENTO"));
		response.setDescripcionMac(rs.getString("DESCRIPCION_MAC"));
		response.setTipoDocumento(rs.getInt("P_TIPO_DOCUMENTO"));
		response.setNombreTipoDocumento(rs.getString("NOMBRE_TIPO_DOCUMENTO"));
		response.setDescripcionDocumento(rs.getString("DESCRIPCION_DOCUMENTO"));
		response.setUrlDescarga(null);
		response.setFechaCarga(rs.getString("FEC_CARGA"));
		response.setCodArchivo(rs.getInt("COD_ARCHIVO"));
		
		return response;
	}

}
