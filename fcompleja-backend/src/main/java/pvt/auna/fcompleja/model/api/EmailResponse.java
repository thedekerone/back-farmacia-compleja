package pvt.auna.fcompleja.model.api;

public class EmailResponse {

    private String from;
    private String to;
    private String subject;
    private String content;
    private String tipoDocumento;

    public EmailResponse() {
    }

    public EmailResponse(String from, String to, String subject, String content,String tipoDocumento) {
        super();
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.content = content;
        this.tipoDocumento=tipoDocumento;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Override
    public String toString() {
        return "Mail [from=" + from + ", to=" + to + ", subject=" + subject + ", content=" + content
                + ", tipoDocumento=" + tipoDocumento + "]";
    }

}
