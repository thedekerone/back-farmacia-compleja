package pvt.auna.fcompleja.model.api.request;

public class ParametroBeanRequest {
	private String codGrupoParametro;

	public String getCodGrupoParametro() {
		return codGrupoParametro;
	}

	public void setCodGrupoParametro(String codGrupoParametro) {
		this.codGrupoParametro = codGrupoParametro;
	}

	@Override
	public String toString() {
		return "ParametroBeanRequest [codGrupoParametro=" + codGrupoParametro + "]";
	}
}
