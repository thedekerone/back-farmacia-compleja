package pvt.auna.fcompleja.model.api.response;

import java.io.Serializable;

public class ListExamenesResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codigoLarge;
	private String descripcion;
	private String tipoExamen;
	private String tipoIngreso;
	private String unidadMedida;
	private String rango;
	private String estado;
	
	public String getCodigoLarge() {
		return codigoLarge;
	}
	public void setCodigoLarge(String codigoLarge) {
		this.codigoLarge = codigoLarge;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTipoExamen() {
		return tipoExamen;
	}
	public void setTipoExamen(String tipoExamen) {
		this.tipoExamen = tipoExamen;
	}
	public String getTipoIngreso() {
		return tipoIngreso;
	}
	public void setTipoIngreso(String tipoIngreso) {
		this.tipoIngreso = tipoIngreso;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getRango() {
		return rango;
	}
	public void setRango(String rango) {
		this.rango = rango;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	
}
