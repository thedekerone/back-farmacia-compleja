package pvt.auna.fcompleja.model.api.response;

public class ResponseRegCheckListConfig {
	private Number codChklistIndi;

	public ResponseRegCheckListConfig() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Number getCodChklistIndi() {
		return codChklistIndi;
	}

	public void setCodChklistIndi(Number codChklistIndi) {
		this.codChklistIndi = codChklistIndi;
	}
	
	@Override
	public String toString() {
		return "ResponseRegCheckListConfig [codChklistIndi=" + codChklistIndi + "]"; }
}
