package pvt.auna.fcompleja.model.api.request;

import java.io.Serializable;
import java.util.Date;

public class RegistroMACRequest implements Serializable {

	private static final long serialVersionUID = -1400042369102145235L;
	private Integer codMac;
	private String descripcion;
	private String tipoMac;
	private String estadoMac;
	private Date fechaInscripcion;
	private Date fechaCreacion;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private Date fechaModificacion;
	private Integer codUsuario;
	private Integer codUsuarioMod;

	public Integer getCodMac() {
		return codMac;
	}

	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipoMac() {
		return tipoMac;
	}

	public void setTipoMac(String tipoMac) {
		this.tipoMac = tipoMac;
	}

	public String getEstadoMac() {
		return estadoMac;
	}

	public void setEstadoMac(String estadoMac) {
		this.estadoMac = estadoMac;
	}

	public Date getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(Date fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Integer getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}

	public Integer getCodUsuarioMod() {
		return codUsuarioMod;
	}

	public void setCodUsuarioMod(Integer codUsuarioMod) {
		this.codUsuarioMod = codUsuarioMod;
	}

	@Override
	public String toString() {
		return "RegistroMACRequest [codMac=" + codMac + ", descripcion=" + descripcion + ", tipoMac=" + tipoMac
				+ ", estadoMac=" + estadoMac + ", fechaInscripcion=" + fechaInscripcion + ", fechaCreacion="
				+ fechaCreacion + ", fechaInicioVigencia=" + fechaInicioVigencia + ", fechaFinVigencia="
				+ fechaFinVigencia + ", fechaModificacion=" + fechaModificacion + ", codUsuario=" + codUsuario
				+ ", codUsuarioMod=" + codUsuarioMod + "]";
	}

}
