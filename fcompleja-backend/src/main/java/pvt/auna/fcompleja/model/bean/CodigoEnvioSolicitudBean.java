package pvt.auna.fcompleja.model.bean;

import java.io.Serializable;

public class CodigoEnvioSolicitudBean  implements Serializable {

	private static final long serialVersionUID = -8022474255483626723L;

		private Integer codSolEvaluacion;
		private String  estadoSolEvaluacion;
		private String  codEnvioSolEvaluacion;
		
		public Integer getCodSolEvaluacion() {
			return codSolEvaluacion;
		}
		public void setCodSolEvaluacion(Integer codSolEvaluacion) {
			this.codSolEvaluacion = codSolEvaluacion;
		}
		public String getEstadoSolEvaluacion() {
			return estadoSolEvaluacion;
		}
		public void setEstadoSolEvaluacion(String estadoSolEvaluacion) {
			this.estadoSolEvaluacion = estadoSolEvaluacion;
		}
		public String getCodEnvioSolEvaluacion() {
			return codEnvioSolEvaluacion;
		}
		public void setCodEnvioSolEvaluacion(String codEnvioSolEvaluacion) {
			this.codEnvioSolEvaluacion = codEnvioSolEvaluacion;
		}
		
		@Override
		public String toString() {
			return "CodigoEnvioSolicitudBean [codSolEvaluacion=" + codSolEvaluacion + ", estadoSolEvaluacion="
					+ estadoSolEvaluacion + ", codEnvioSolEvaluacion=" + codEnvioSolEvaluacion + "]";
		}

		

}
