package pvt.auna.fcompleja.model.api;

import java.io.Serializable;
import java.util.List;

import pvt.auna.fcompleja.model.api.response.CriterioExclusion;
import pvt.auna.fcompleja.model.api.response.CriterioInclusion;

public class ListaIndicador implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer codigo;
	private String descripcion;
	private List<CriterioInclusion> listaCriterioInclusion;
	private List<CriterioExclusion> listaCriterioExclusion;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<CriterioInclusion> getListaCriterioInclusion() {
		return listaCriterioInclusion;
	}
	public void setListaCriterioInclusion(List<CriterioInclusion> listaCriterioInclusion) {
		this.listaCriterioInclusion = listaCriterioInclusion;
	}
	public List<CriterioExclusion> getListaCriterioExclusion() {
		return listaCriterioExclusion;
	}
	public void setListaCriterioExclusion(List<CriterioExclusion> listaCriterioExclusion) {
		this.listaCriterioExclusion = listaCriterioExclusion;
	}
	
	
}
