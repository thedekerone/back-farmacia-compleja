package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.MedicamentoMacResponse;

public class BuscarMacRowMapper implements RowMapper<MedicamentoMacResponse> {
	
	@Override
	public MedicamentoMacResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		MedicamentoMacResponse macmedicamento = new MedicamentoMacResponse();
		macmedicamento.setCodMac(rs.getLong("COD_MAC"));
		macmedicamento.setCodMacVch(rs.getString("COD_MAC_LARGO"));
		macmedicamento.setDescripcion(rs.getString("DESCRIPCION"));
		return macmedicamento;
	}

}
