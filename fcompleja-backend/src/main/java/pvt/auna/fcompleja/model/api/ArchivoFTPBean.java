package pvt.auna.fcompleja.model.api;

import java.io.Serializable;

public class ArchivoFTPBean implements Serializable {

	private static final long serialVersionUID = 5671704606216878621L;

	private Integer codArchivo;
	private String ip;
	private String usrApp;
	private String nomServ;
	private String nomArchivo;
	private String ruta;
	private String estado;
	private String extension;
	private String tamano;
	private byte[] archivo;
	private String archivoBase64;

	public Integer getCodArchivo() {
		return codArchivo;
	}

	public void setCodArchivo(Integer codArchivo) {
		this.codArchivo = codArchivo;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUsrApp() {
		return usrApp;
	}

	public void setUsrApp(String usrApp) {
		this.usrApp = usrApp;
	}

	public String getNomServ() {
		return nomServ;
	}

	public void setNomServ(String nomServ) {
		this.nomServ = nomServ;
	}

	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getTamano() {
		return tamano;
	}

	public void setTamano(String tamano) {
		this.tamano = tamano;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	public String getArchivoBase64() {
		return archivoBase64;
	}

	public void setArchivoBase64(String archivoBase64) {
		this.archivoBase64 = archivoBase64;
	}

	@Override
	public String toString() {
		return "ArchivoFTPBean [codArchivo=" + codArchivo + ", ip=" + ip + ", usrApp=" + usrApp + ", nomServ=" + nomServ
				+ ", nomArchivo=" + nomArchivo + ", ruta=" + ruta + ", estado=" + estado + ", extension=" + extension
				+ ", tamano=" + tamano + ", archivo=" + archivo + "]";
	}

};
