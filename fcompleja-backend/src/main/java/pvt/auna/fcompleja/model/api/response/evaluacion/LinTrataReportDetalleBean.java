package pvt.auna.fcompleja.model.api.response.evaluacion;

public class LinTrataReportDetalleBean {
	
	private String lineaTratamiento="";
	private String fechaInicio="";
	private String fechaFin="";
	private String curso="";
	private String respuestaAlcanzada="";
	private String lugarProgresion="";
	private String motivoInactivacion="";
	private String lineaMetastasis="";
	private String lugarMetastasis="";
	private String descripcion="";
	private String resultado="";
	private String fecha="";
	
	public String getLineaTratamiento() {
		return lineaTratamiento;
	}
	public void setLineaTratamiento(String lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getRespuestaAlcanzada() {
		return respuestaAlcanzada;
	}
	public void setRespuestaAlcanzada(String respuestaAlcanzada) {
		this.respuestaAlcanzada = respuestaAlcanzada;
	}
	public String getLugarProgresion() {
		return lugarProgresion;
	}
	public void setLugarProgresion(String lugarProgresion) {
		this.lugarProgresion = lugarProgresion;
	}
	public String getMotivoInactivacion() {
		return motivoInactivacion;
	}
	public void setMotivoInactivacion(String motivoInactivacion) {
		this.motivoInactivacion = motivoInactivacion;
	}
	public String getLineaMetastasis() {
		return lineaMetastasis;
	}
	public void setLineaMetastasis(String lineaMetastasis) {
		this.lineaMetastasis = lineaMetastasis;
	}
	public String getLugarMetastasis() {
		return lugarMetastasis;
	}
	public void setLugarMetastasis(String lugarMetastasis) {
		this.lugarMetastasis = lugarMetastasis;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	@Override
	public String toString() {
		return "pruebaDetalleBean [lineaTratamiento=" + lineaTratamiento + ", fechaInicio=" + fechaInicio
				+ ", fechaFin=" + fechaFin + ", curso=" + curso + ", respuestaAlcanzada=" + respuestaAlcanzada
				+ ", lugarProgresion=" + lugarProgresion + ", motivoInactivacion=" + motivoInactivacion
				+ ", lineaMetastasis=" + lineaMetastasis + ", lugarMetastasis=" + lugarMetastasis + ", descripcion="
				+ descripcion + ", resultado=" + resultado + ", fecha=" + fecha + "]";
	}
	
	
	
	
	
}
