package pvt.auna.fcompleja.model.api.request;

public class CodigoCheckList {

	private Number codGrpDiag;
	private Number codMac;

	public CodigoCheckList() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Number getCodMac() {
		return codMac;
	}


	public void setCodMac(Number codMac) {
		this.codMac = codMac;
	}


	public Number getCodGrpDiag() {
		return codGrpDiag;
	}

	public void setCodGrpDiag(Number codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}
	
	@Override
	public String toString() {
		return "CodigoCheckList [codGrpDiag=" + codGrpDiag + ", codMac=" + codMac + "]";
	}
}
