/**
 * 
 */
package pvt.auna.fcompleja.model.api.response;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class IndicadorConsumoPorMacResponse {
	
	private Integer codigoMac;
	private String descripcion;
	private Double gastoTotal;
	private Integer nroPacientes;
	private Double gastoPaciente;
	private Integer nroNuevos;
	private Integer nroContinuadores;
	

	/**
	 * 
	 */
	public IndicadorConsumoPorMacResponse() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @param codigoMac
	 * @param descripcion
	 * @param gastoTotal
	 * @param nroPacientes
	 * @param gastoPaciente
	 * @param nroNuevos
	 * @param nroContinuadores
	 */
	public IndicadorConsumoPorMacResponse(Integer codigoMac, String descripcion, Double gastoTotal,
			Integer nroPacientes, Double gastoPaciente, Integer nroNuevos, Integer nroContinuadores) {
		super();
		this.codigoMac = codigoMac;
		this.descripcion = descripcion;
		this.gastoTotal = gastoTotal;
		this.nroPacientes = nroPacientes;
		this.gastoPaciente = gastoPaciente;
		this.nroNuevos = nroNuevos;
		this.nroContinuadores = nroContinuadores;
	}


	/**
	 * @return the codigoMac
	 */
	public Integer getCodigoMac() {
		return codigoMac;
	}


	/**
	 * @param codigoMac the codigoMac to set
	 */
	public void setCodigoMac(Integer codigoMac) {
		this.codigoMac = codigoMac;
	}


	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}


	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	/**
	 * @return the gastoTotal
	 */
	public Double getGastoTotal() {
		return gastoTotal;
	}


	/**
	 * @param gastoTotal the gastoTotal to set
	 */
	public void setGastoTotal(Double gastoTotal) {
		this.gastoTotal = gastoTotal;
	}


	/**
	 * @return the nroPacientes
	 */
	public Integer getNroPacientes() {
		return nroPacientes;
	}


	/**
	 * @param nroPacientes the nroPacientes to set
	 */
	public void setNroPacientes(Integer nroPacientes) {
		this.nroPacientes = nroPacientes;
	}


	/**
	 * @return the gastoPaciente
	 */
	public Double getGastoPaciente() {
		return gastoPaciente;
	}


	/**
	 * @param gastoPaciente the gastoPaciente to set
	 */
	public void setGastoPaciente(Double gastoPaciente) {
		this.gastoPaciente = gastoPaciente;
	}


	/**
	 * @return the nroNuevos
	 */
	public Integer getNroNuevos() {
		return nroNuevos;
	}


	/**
	 * @param nroNuevos the nroNuevos to set
	 */
	public void setNroNuevos(Integer nroNuevos) {
		this.nroNuevos = nroNuevos;
	}


	/**
	 * @return the nroContinuadores
	 */
	public Integer getNroContinuadores() {
		return nroContinuadores;
	}


	/**
	 * @param nroContinuadores the nroContinuadores to set
	 */
	public void setNroContinuadores(Integer nroContinuadores) {
		this.nroContinuadores = nroContinuadores;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IndicadorConsumoPorMacResponse [codigoMac=" + codigoMac + ", descripcion=" + descripcion
				+ ", gastoTotal=" + gastoTotal + ", nroPacientes=" + nroPacientes + ", gastoPaciente=" + gastoPaciente
				+ ", nroNuevos=" + nroNuevos + ", nroContinuadores=" + nroContinuadores + ", toString()="
				+ super.toString() + "]";
	}

	
	
}
