package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.ListExamenesResponse;

public class consultarExamenMedicoRowMapper implements RowMapper<ListExamenesResponse> {

	@Override
	public ListExamenesResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ListExamenesResponse listExamenesResponse = new ListExamenesResponse();
		listExamenesResponse.setCodigoLarge(rs.getString("codigo"));
		listExamenesResponse.setDescripcion(rs.getString("descripcion"));
		listExamenesResponse.setTipoExamen(rs.getString("tipoExamen"));
		listExamenesResponse.setTipoIngreso(rs.getString("tipoIngreso"));
		listExamenesResponse.setUnidadMedida(rs.getString("unidadMedida"));
		listExamenesResponse.setRango(rs.getString("rango"));
		listExamenesResponse.setEstado(rs.getString("estado"));
		return listExamenesResponse;
	}


}
