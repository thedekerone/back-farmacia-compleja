/**
 * 
 */
package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.ExamenMedicoBean;



/**
 * @author Jose.Reyes/MDP
 *
 */
public class ExamenMedicoRowMapper implements RowMapper<ExamenMedicoBean>{

	@Override
	public ExamenMedicoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ExamenMedicoBean examenMedico = new ExamenMedicoBean();
		examenMedico.setCodExamenMed(rs.getInt("cod_examen_med"));
		examenMedico.setCodExamenMedLargo(rs.getString("cod_examen_med_largo"));
		examenMedico.setDescripcion(rs.getString("descripcion"));
		examenMedico.setExamen(rs.getString("tipo"));
		examenMedico.setTipoExamen(rs.getInt("cod_tipo_examen"));
		examenMedico.setCodEstado(rs.getInt("cod_estado_exam_med"));
		examenMedico.setEstado(rs.getString("estado_exam_med"));
		
		return examenMedico;
	}

}

