package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.MetastasisResponse;


public class MetastasisRowMapper implements RowMapper<MetastasisResponse> {

	@Override
	public MetastasisResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		MetastasisResponse response = new MetastasisResponse();
		response.setCodMetastasis(rs.getInt("COD_METASTASIS"));
		response.setLineaMetastasis(rs.getInt("P_LINEA_METASTASIS"));
		response.setDescripcionLineaMetastasis(rs.getString("LINEA_METASTASIS"));
		response.setLugarMetastasis(rs.getInt("P_LUGAR_METASTASIS"));
		response.setDescripcionLugarMetastasis(rs.getString("LUGAR_METASTASIS"));
		response.setEstado(rs.getString("ESTADO"));
		return response;
	}

}
