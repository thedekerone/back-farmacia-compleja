/**
 * 
 */
package pvt.auna.fcompleja.model.api.request;

/**
 * @author Jose.Reyes/MDP
 *
 */
public class UsuarioRolRequest {
	
	private String codUsuarioCodRol;
	private Integer codAplicacion;
	

	/**
	 * 
	 */
	public UsuarioRolRequest() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @param codUsuarioCodRol
	 * @param codAplicacion
	 */
	public UsuarioRolRequest(String codUsuarioCodRol, Integer codAplicacion) {
		super();
		this.codUsuarioCodRol = codUsuarioCodRol;
		this.codAplicacion = codAplicacion;
	}


	/**
	 * @return the codUsuarioCodRol
	 */
	public String getCodUsuarioCodRol() {
		return codUsuarioCodRol;
	}


	/**
	 * @param codUsuarioCodRol the codUsuarioCodRol to set
	 */
	public void setCodUsuarioCodRol(String codUsuarioCodRol) {
		this.codUsuarioCodRol = codUsuarioCodRol;
	}


	/**
	 * @return the codAplicacion
	 */
	public Integer getCodAplicacion() {
		return codAplicacion;
	}


	/**
	 * @param codAplicacion the codAplicacion to set
	 */
	public void setCodAplicacion(Integer codAplicacion) {
		this.codAplicacion = codAplicacion;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UsuarioRolRequest [codUsuarioCodRol=" + codUsuarioCodRol + ", codAplicacion=" + codAplicacion
				+ ", toString()=" + super.toString() + "]";
	}

	
	
}
