package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.ListFiltroParametroResponse;

public class ControlFilaParametroRowMapper implements RowMapper<ListFiltroParametroResponse>{
	
	@Override
	public ListFiltroParametroResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ListFiltroParametroResponse response = new ListFiltroParametroResponse();
		
		response.setCodigoParametro(rs.getInt("COD_PARAMETRO"));
		response.setNombreParametro(rs.getString("NOMBRE"));
		response.setValor1Parametro(rs.getString("VALOR1"));
		
		return response;
	}
}
