package pvt.auna.fcompleja.model.mapper;

import org.springframework.jdbc.core.RowMapper;
import pvt.auna.fcompleja.model.bean.CasosEvaCmacBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CasosEvaCmacMapper implements RowMapper<CasosEvaCmacBean>  {


        @Override
        public CasosEvaCmacBean mapRow(ResultSet rs, int rowNum) throws SQLException {
            CasosEvaCmacBean casosEvaCmacBean = new CasosEvaCmacBean();
            casosEvaCmacBean.setCodLagoSolEva(rs.getString("COD_DESC_SOL_EVA"));
            casosEvaCmacBean.setPaciente(rs.getString("COD_AFI_PACIENTE"));
            casosEvaCmacBean.setDiagnostico(rs.getString("COD_DIAGNOSTICO"));
            casosEvaCmacBean.setCodLargoMac(rs.getString("COD_MAC_VCH"));
            casosEvaCmacBean.setMac(rs.getString("DESCRIPCION"));
            casosEvaCmacBean.setFechaCmac(rs.getString("FECHA_REUNION"));
            return casosEvaCmacBean;
        }

}
