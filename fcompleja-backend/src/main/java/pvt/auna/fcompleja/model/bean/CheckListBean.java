package pvt.auna.fcompleja.model.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CheckListBean implements Serializable {

	private static final long serialVersionUID = 4246604379342194489L;
	private Integer codMac;
	private Integer codGrpDiag;
	private Integer codChkListIndi;
	private String codIndicadorLargo;
	private String descripcion;
	private Integer codEstado;
	private String estado;
	private Date fechaIniVigencia;
	private Date fechaFinVigencia;
	private List<CriteriosBean> listaCriterioInclusion;
	private List<CriteriosBean> listaCriterioExclusion;
	private String lCriterioInclusion;
	private String lCriterioExclusion;

	public Integer getCodMac() {
		return codMac;
	}

	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}

	public Integer getCodGrpDiag() {
		return codGrpDiag;
	}

	public void setCodGrpDiag(Integer codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}

	public Integer getCodChkListIndi() {
		return codChkListIndi;
	}

	public void setCodChkListIndi(Integer codChkListIndi) {
		this.codChkListIndi = codChkListIndi;
	}

	public String getCodIndicadorLargo() {
		return codIndicadorLargo;
	}

	public void setCodIndicadorLargo(String codIndicadorLargo) {
		this.codIndicadorLargo = codIndicadorLargo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getCodEstado() {
		return codEstado;
	}

	public void setCodEstado(Integer codEstado) {
		this.codEstado = codEstado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaIniVigencia() {
		return fechaIniVigencia;
	}

	public void setFechaIniVigencia(Date fechaIniVigencia) {
		this.fechaIniVigencia = fechaIniVigencia;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public List<CriteriosBean> getListaCriterioInclusion() {
		return listaCriterioInclusion;
	}

	public void setListaCriterioInclusion(List<CriteriosBean> listaCriterioInclusion) {
		this.listaCriterioInclusion = listaCriterioInclusion;
	}

	public List<CriteriosBean> getListaCriterioExclusion() {
		return listaCriterioExclusion;
	}

	public void setListaCriterioExclusion(List<CriteriosBean> listaCriterioExclusion) {
		this.listaCriterioExclusion = listaCriterioExclusion;
	}

	public String getlCriterioInclusion() {
		return lCriterioInclusion;
	}

	public void setlCriterioInclusion(String lCriterioInclusion) {
		this.lCriterioInclusion = lCriterioInclusion;
	}

	public String getlCriterioExclusion() {
		return lCriterioExclusion;
	}

	public void setlCriterioExclusion(String lCriterioExclusion) {
		this.lCriterioExclusion = lCriterioExclusion;
	}

	@Override
	public String toString() {
		return "CheckListBean [codMac=" + codMac + ", codGrpDiag=" + codGrpDiag + ", codChkListIndi=" + codChkListIndi
				+ ", codIndicadorLargo=" + codIndicadorLargo + ", descripcion=" + descripcion + ", codEstado="
				+ codEstado + ", estado=" + estado + ", fechaIniVigencia=" + fechaIniVigencia + ", fechaFinVigencia="
				+ fechaFinVigencia + ", listaCriterioInclusion=" + listaCriterioInclusion + ", listaCriterioExclusion="
				+ listaCriterioExclusion + ", lCriterioInclusion=" + lCriterioInclusion + ", lCriterioExclusion="
				+ lCriterioExclusion + "]";
	}

}
