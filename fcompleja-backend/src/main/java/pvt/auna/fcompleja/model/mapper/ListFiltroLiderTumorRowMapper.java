package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.ListFiltroLiderTumorResponse;

public class ListFiltroLiderTumorRowMapper implements RowMapper<ListFiltroLiderTumorResponse> {

	@Override
	public ListFiltroLiderTumorResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		ListFiltroLiderTumorResponse ListFiltroLiderTumorResponse = new ListFiltroLiderTumorResponse();
		
		ListFiltroLiderTumorResponse.setCmpLiderTumor(rs.getLong("CMP_MEDICO"));
		ListFiltroLiderTumorResponse.setNombreLiderTumor(rs.getString("Nombre"));
		
		return ListFiltroLiderTumorResponse;
		
	}
}
