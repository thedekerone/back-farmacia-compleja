package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.CheckListBean;

public class CheckListBeanRowMapper implements RowMapper<CheckListBean> {
	@Override
	public CheckListBean mapRow(ResultSet rs, int i) throws SQLException {
		CheckListBean o = new CheckListBean();
		o.setCodMac(rs.getInt("cod_mac"));
		o.setCodGrpDiag(rs.getInt("cod_grp_diag"));
		o.setCodChkListIndi(rs.getInt("COD_INDICADOR"));
		o.setCodIndicadorLargo(rs.getString("COD_INDICADOR_LARGO"));
		o.setDescripcion(rs.getString("descripcion"));
		o.setCodEstado(rs.getInt("COD_ESTADO"));
		o.setEstado(rs.getString("ESTADO"));
		o.setFechaIniVigencia(rs.getDate("fecha_ini_vigencia"));
		o.setFechaFinVigencia(rs.getDate("fecha_fin_vigencia"));
		return o;
	}
}
