package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.LinTrataReportDetalleBean;

public class ListaLineaTratamientRowMapper implements RowMapper<LinTrataReportDetalleBean>{

	@Override
	public LinTrataReportDetalleBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		LinTrataReportDetalleBean lineaTratamientoBean = new LinTrataReportDetalleBean();
		
		lineaTratamientoBean.setLineaTratamiento(rs.getString("linea_trat"));
		lineaTratamientoBean.setFechaInicio(rs.getString("fec_inicio"));
		lineaTratamientoBean.setFechaFin(rs.getString("fec_fin"));
		lineaTratamientoBean.setCurso(rs.getString("p_curso"));
		lineaTratamientoBean.setRespuestaAlcanzada(rs.getString("p_resp_alcanzada"));
		lineaTratamientoBean.setLugarProgresion(rs.getString("p_lugar_progresion"));
		lineaTratamientoBean.setMotivoInactivacion(rs.getString("p_motivo_inactivacion"));

		return lineaTratamientoBean;
	}

}
