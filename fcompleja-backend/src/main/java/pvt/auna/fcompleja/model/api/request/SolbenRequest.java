package pvt.auna.fcompleja.model.api.request;

public class SolbenRequest {
	
	private String codAfiliado;

	public String getCodAfiliado() {
		return codAfiliado;
	}

	public void setCodAfiliado(String codAfiliado) {
		this.codAfiliado = codAfiliado;
	}
}
