package pvt.auna.fcompleja.model.api;

import java.io.Serializable;

public class ListClinicaResponse  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long itemCli;
	private long codigoCli;
	private String nombreCli;
	
	public long getItemCli() {
		return itemCli;
	}
	public void setItemCli(long itemCli) {
		this.itemCli = itemCli;
	}
	public long getCodigoCli() {
		return codigoCli;
	}
	public void setCodigoCli(long codigoCli) {
		this.codigoCli = codigoCli;
	}
	public String getNombreCli() {
		return nombreCli;
	}
	public void setNombreCli(String nombreCli) {
		this.nombreCli = nombreCli;
	}
	
	
}
