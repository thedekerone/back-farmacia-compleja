package pvt.auna.fcompleja.model.api.response;

import java.util.Map;

import pvt.auna.fcompleja.model.api.AudiResponse;

public class AunaResponse {
	
	private AudiResponse audiResponse;

	private Map<String, String> dataList;
	
	private String estadoEnvio;

	public AunaResponse() {	}
	
	public AunaResponse(AudiResponse audiResponse, Map<String, String> dataList) {
		this.audiResponse = audiResponse;
		this.dataList = dataList;
	}

	public AudiResponse getAudiResponse() {
		return audiResponse;
	}

	public void setAudiResponse(AudiResponse audiResponse) {
		this.audiResponse = audiResponse;
	}

	public Map<String, String> getDataList() {
		return dataList;
	}

	public void setDataList(Map<String, String> dataList) {
		this.dataList = dataList;
	}

	public String getEstadoEnvio() {
		return estadoEnvio;
	}

	public void setEstadoEnvio(String estadoEnvio) {
		this.estadoEnvio = estadoEnvio;
	}

	
	

	


}