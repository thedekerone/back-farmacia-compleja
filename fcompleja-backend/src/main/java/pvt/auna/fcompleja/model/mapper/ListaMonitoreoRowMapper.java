package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.ListaBandejaMonitoreo;

public class ListaMonitoreoRowMapper implements RowMapper<ListaBandejaMonitoreo> {

	@Override
	public ListaBandejaMonitoreo mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ListaBandejaMonitoreo listaBandejaMonitoreo = new ListaBandejaMonitoreo();
		
		listaBandejaMonitoreo.setCodigoMonitoreo(rs.getLong("COD_MONITOREO"));
		listaBandejaMonitoreo.setCodigoDescripcionMonitoreo(rs.getString("COD_DESC_MONITOREO"));
		listaBandejaMonitoreo.setCodigoEvaluacion(rs.getString("COD_DESC_SOL_EVA"));
		listaBandejaMonitoreo.setFechAprobacion(rs.getString("FECHAAPROBACION"));
		listaBandejaMonitoreo.setCodigoScgSolben(rs.getString("COD_SCG_SOLBEN"));
		listaBandejaMonitoreo.setNroCartaGarantia(rs.getString("NRO_CG"));
		listaBandejaMonitoreo.setCodDiagnostico(rs.getString("COD_DIAGNOSTICO"));
		listaBandejaMonitoreo.setCodGrupoDiagnostico(rs.getString("COD_GRUPO_DIAGNOSTICO"));
		listaBandejaMonitoreo.setMedicoTratante(rs.getString("MEDICO_TRATANTE"));
		listaBandejaMonitoreo.setEdadPaciente(rs.getInt("EDAD_PACIENTE"));
		listaBandejaMonitoreo.setCodMedicamento(rs.getLong("COD_MEDICAMENTO"));
		listaBandejaMonitoreo.setNomMedicamento(rs.getString("NOM_MEDICAMENTO"));
		listaBandejaMonitoreo.setNumeroLineaTratamiento(rs.getString("LINEA_TRAT"));
		listaBandejaMonitoreo.setFecIniLineaTratamiento(rs.getString("FEC_INI_LINEA_TRAT"));
		listaBandejaMonitoreo.setCodigoPaciente(rs.getString("COD_PACIENTE"));
		listaBandejaMonitoreo.setCodigoClinica(rs.getString("COD_CLINICA"));
		listaBandejaMonitoreo.setCodEstadoMonitoreo(rs.getInt("COD_ESTADOMONITOREO"));
		listaBandejaMonitoreo.setNomEstadoMonitoreo(rs.getString("DESC_ESTADOMONITOREO"));
		listaBandejaMonitoreo.setFechaProximoMonitoreo(rs.getDate("FECHAPROXIMOMONITOREO"));
		listaBandejaMonitoreo.setCodResponsableMonitoreo(rs.getLong("COD_RESPONSABLEMONITOREO"));
		listaBandejaMonitoreo.setNomResponsableMonitoreo(rs.getString("NOM_RESPONSABLEMONITOREO"));
		listaBandejaMonitoreo.setCodigoAfiliado(rs.getString("COD_AFILIADO"));
		listaBandejaMonitoreo.setCodSolEvaluacion(rs.getLong("COD_SOL_EVA"));
		listaBandejaMonitoreo.setCodHistLineaTrat(rs.getLong("COD_HIST_LINEA_TRAT"));
		listaBandejaMonitoreo.setCodEvolucion(rs.getLong("COD_EVOLUCION"));
		
		return listaBandejaMonitoreo;
	}

}
