package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.monitoreo.DetalleMarcadorResponse;

public class DetalleMarcadorRowMapper implements RowMapper<DetalleMarcadorResponse>{
	@Override
	public DetalleMarcadorResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		DetalleMarcadorResponse detMarcador = new DetalleMarcadorResponse();

		detMarcador.setCodDetMarcador(rs.getLong("COD_DET_MARCADOR"));
		detMarcador.setCodMarcador(rs.getLong("COD_MARCADOR"));
		detMarcador.setValorFijo(rs.getString("VALOR_FIJO"));

		return detMarcador;
	}

}
