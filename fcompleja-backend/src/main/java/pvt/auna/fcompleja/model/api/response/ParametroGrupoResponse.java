package pvt.auna.fcompleja.model.api.response;

import java.util.List;

import pvt.auna.fcompleja.model.api.response.evaluacion.ParticipanteResponse;
import pvt.auna.fcompleja.model.bean.ParticipanteGrupoBean;

public class ParametroGrupoResponse {
	

	private List<ParticipanteGrupoBean> lista;
	private List<ParticipanteResponse> listaParticipante;
	private Integer codigo;
	private String mensaje;
	
	public List<ParticipanteGrupoBean> getLista() {
		return lista;
	}
	public void setLista(List<ParticipanteGrupoBean> lista) {
		this.lista = lista;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public List<ParticipanteResponse> getListaParticipante() {
		return listaParticipante;
	}
	public void setListaParticipante(List<ParticipanteResponse> listaParticipante) {
		this.listaParticipante = listaParticipante;
	}
	
	@Override
	public String toString() {
		return "ParametroGrupoResponse [lista=" + lista + ", listaParticipante=" + listaParticipante + ", codigo="
				+ codigo + ", mensaje=" + mensaje + "]";
	}
}
