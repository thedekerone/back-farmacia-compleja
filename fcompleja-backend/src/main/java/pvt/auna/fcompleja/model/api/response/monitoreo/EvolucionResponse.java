package pvt.auna.fcompleja.model.api.response.monitoreo;

import java.io.Serializable;
import java.sql.Date;

public class EvolucionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codEvolucion;
	private String nroDescEvolucion;
	private Long codMonitoreo;
	private String codDescMonitoreo;
	private Long codMac;
	private Integer pResEvolucion;
	private String descResEvolucion;
	private Long codHistLineaTrat;
	private Date fecMonitoreo;
	private Date fecProxMonitoreo;
	private Integer pMotivoInactivacion;
	private Date fecInactivacion;
	private String observacion;
	private Integer pTolerancia;
	private String descTolerancia;
	private Integer pToxicidad;
	private String descToxicidad;
	private Integer pGrado;
	private String descGrado;
	private Integer pRespClinica;
	private String descRespClinica;
	private Integer pAtenAlerta;
	private String descAtenAlerta;
	private String existeToxicidad;
	private Date fUltimoConsumo;
	private Integer ultimaCantConsumida;
	private Integer estado;
	private String usuarioCrea;
	private Date fechaCrea;
	private String usuarioModif;
	private Date fechaModif;
	private Integer pEstadoMonitoreo;

	public EvolucionResponse() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodEvolucion() {
		return codEvolucion;
	}

	public void setCodEvolucion(Long codEvolucion) {
		this.codEvolucion = codEvolucion;
	}

	public String getNroDescEvolucion() {
		return nroDescEvolucion;
	}

	public void setNroDescEvolucion(String nroDescEvolucion) {
		this.nroDescEvolucion = nroDescEvolucion;
	}

	public Long getCodMonitoreo() {
		return codMonitoreo;
	}

	public void setCodMonitoreo(Long codMonitoreo) {
		this.codMonitoreo = codMonitoreo;
	}

	public String getCodDescMonitoreo() {
		return codDescMonitoreo;
	}

	public void setCodDescMonitoreo(String codDescMonitoreo) {
		this.codDescMonitoreo = codDescMonitoreo;
	}

	public Long getCodMac() {
		return codMac;
	}

	public void setCodMac(Long codMac) {
		this.codMac = codMac;
	}

	public Integer getpResEvolucion() {
		return pResEvolucion;
	}

	public void setpResEvolucion(Integer pResEvolucion) {
		this.pResEvolucion = pResEvolucion;
	}

	public String getDescResEvolucion() {
		return descResEvolucion;
	}

	public void setDescResEvolucion(String descResEvolucion) {
		this.descResEvolucion = descResEvolucion;
	}

	public Long getCodHistLineaTrat() {
		return codHistLineaTrat;
	}

	public void setCodHistLineaTrat(Long codHistLineaTrat) {
		this.codHistLineaTrat = codHistLineaTrat;
	}

	public Date getFecMonitoreo() {
		return fecMonitoreo;
	}

	public void setFecMonitoreo(Date fecMonitoreo) {
		this.fecMonitoreo = fecMonitoreo;
	}

	public Date getFecProxMonitoreo() {
		return fecProxMonitoreo;
	}

	public void setFecProxMonitoreo(Date fecProxMonitoreo) {
		this.fecProxMonitoreo = fecProxMonitoreo;
	}

	public Integer getpMotivoInactivacion() {
		return pMotivoInactivacion;
	}

	public void setpMotivoInactivacion(Integer pMotivoInactivacion) {
		this.pMotivoInactivacion = pMotivoInactivacion;
	}

	public Date getFecInactivacion() {
		return fecInactivacion;
	}

	public void setFecInactivacion(Date fecInactivacion) {
		this.fecInactivacion = fecInactivacion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Integer getpTolerancia() {
		return pTolerancia;
	}

	public void setpTolerancia(Integer pTolerancia) {
		this.pTolerancia = pTolerancia;
	}

	public String getDescTolerancia() {
		return descTolerancia;
	}

	public void setDescTolerancia(String descTolerancia) {
		this.descTolerancia = descTolerancia;
	}

	public Integer getpToxicidad() {
		return pToxicidad;
	}

	public void setpToxicidad(Integer pToxicidad) {
		this.pToxicidad = pToxicidad;
	}

	public String getDescToxicidad() {
		return descToxicidad;
	}

	public void setDescToxicidad(String descToxicidad) {
		this.descToxicidad = descToxicidad;
	}

	public Integer getpGrado() {
		return pGrado;
	}

	public void setpGrado(Integer pGrado) {
		this.pGrado = pGrado;
	}

	public String getDescGrado() {
		return descGrado;
	}

	public void setDescGrado(String descGrado) {
		this.descGrado = descGrado;
	}

	public Integer getpRespClinica() {
		return pRespClinica;
	}

	public void setpRespClinica(Integer pRespClinica) {
		this.pRespClinica = pRespClinica;
	}

	public String getDescRespClinica() {
		return descRespClinica;
	}

	public void setDescRespClinica(String descRespClinica) {
		this.descRespClinica = descRespClinica;
	}

	public Integer getpAtenAlerta() {
		return pAtenAlerta;
	}

	public void setpAtenAlerta(Integer pAtenAlerta) {
		this.pAtenAlerta = pAtenAlerta;
	}

	public String getDescAtenAlerta() {
		return descAtenAlerta;
	}

	public void setDescAtenAlerta(String descAtenAlerta) {
		this.descAtenAlerta = descAtenAlerta;
	}

	public String getExisteToxicidad() {
		return existeToxicidad;
	}

	public void setExisteToxicidad(String existeToxicidad) {
		this.existeToxicidad = existeToxicidad;
	}

	public Date getfUltimoConsumo() {
		return fUltimoConsumo;
	}

	public void setfUltimoConsumo(Date fUltimoConsumo) {
		this.fUltimoConsumo = fUltimoConsumo;
	}

	public Integer getUltimaCantConsumida() {
		return ultimaCantConsumida;
	}

	public void setUltimaCantConsumida(Integer ultimaCantConsumida) {
		this.ultimaCantConsumida = ultimaCantConsumida;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getUsuarioCrea() {
		return usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public Date getFechaCrea() {
		return fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public String getUsuarioModif() {
		return usuarioModif;
	}

	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}

	public Date getFechaModif() {
		return fechaModif;
	}

	public void setFechaModif(Date fechaModif) {
		this.fechaModif = fechaModif;
	}

	public Integer getpEstadoMonitoreo() {
		return pEstadoMonitoreo;
	}

	public void setpEstadoMonitoreo(Integer pEstadoMonitoreo) {
		this.pEstadoMonitoreo = pEstadoMonitoreo;
	}

	@Override
	public String toString() {
		return "EvolucionResponse [codEvolucion=" + codEvolucion + ", nroDescEvolucion=" + nroDescEvolucion
				+ ", codMonitoreo=" + codMonitoreo + ", codDescMonitoreo=" + codDescMonitoreo + ", codMac=" + codMac
				+ ", pResEvolucion=" + pResEvolucion + ", descResEvolucion=" + descResEvolucion + ", codHistLineaTrat="
				+ codHistLineaTrat + ", fecMonitoreo=" + fecMonitoreo + ", fecProxMonitoreo=" + fecProxMonitoreo
				+ ", pMotivoInactivacion=" + pMotivoInactivacion + ", fecInactivacion=" + fecInactivacion
				+ ", observacion=" + observacion + ", pTolerancia=" + pTolerancia + ", descTolerancia=" + descTolerancia
				+ ", pToxicidad=" + pToxicidad + ", descToxicidad=" + descToxicidad + ", pGrado=" + pGrado
				+ ", descGrado=" + descGrado + ", pRespClinica=" + pRespClinica + ", descRespClinica=" + descRespClinica
				+ ", pAtenAlerta=" + pAtenAlerta + ", descAtenAlerta=" + descAtenAlerta + ", existeToxicidad="
				+ existeToxicidad + ", fUltimoConsumo=" + fUltimoConsumo + ", ultimaCantConsumida="
				+ ultimaCantConsumida + ", estado=" + estado + ", usuarioCrea=" + usuarioCrea + ", fechaCrea="
				+ fechaCrea + ", usuarioModif=" + usuarioModif + ", fechaModif=" + fechaModif + ", pEstadoMonitoreo="
				+ pEstadoMonitoreo + "]";
	}

}
