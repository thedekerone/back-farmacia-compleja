package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.CriterioExclusion;

public class ListaExclusionRowMapper implements RowMapper<CriterioExclusion>{

	@Override
	public CriterioExclusion mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		CriterioExclusion criterioExclusion = new CriterioExclusion();
		
		criterioExclusion.setCodigo(rs.getInt("COD_CRITERIO_EXCLU"));
		criterioExclusion.setCodigoIndicacion(rs.getInt("COD_CHKLIST_INDI"));
		criterioExclusion.setDescripcion(rs.getString("DESCRIPCION"));
		return criterioExclusion;
	}

}
