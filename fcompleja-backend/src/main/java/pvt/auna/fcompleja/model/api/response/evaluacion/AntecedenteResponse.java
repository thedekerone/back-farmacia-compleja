package pvt.auna.fcompleja.model.api.response.evaluacion;


public class AntecedenteResponse {

	Integer codResultado;
    String msgResultado;
    String codAntecedente;
	
	public Integer getCodResultado() {
		return codResultado;
	}
	public void setCodResultado(Integer codResultado) {
		this.codResultado = codResultado;
	}
	public String getMsgResultado() {
		return msgResultado;
	}
	public void setMsgResultado(String msgResultado) {
		this.msgResultado = msgResultado;
	}
	public String getCodAntecedente() {
		return codAntecedente;
	}
	public void setCodAntecedente(String codAntecedente) {
		this.codAntecedente = codAntecedente;
	}
	
}
