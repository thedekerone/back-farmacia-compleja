package pvt.auna.fcompleja.model.base;

import java.io.Serializable;

public class GenericResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3306587173441534864L;
	private String codResponse;
	private String mensajeResponse;
	
	public String getCodResponse() {
		return codResponse;
	}
	public void setCodResponse(String codResponse) {
		this.codResponse = codResponse;
	}
	public String getMensajeResponse() {
		return mensajeResponse;
	}
	public void setMensajeResponse(String mensajeResponse) {
		this.mensajeResponse = mensajeResponse;
	}
	
	

}
