package pvt.auna.fcompleja.model.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GastoConsumoBean {


	private Integer codHistControlGasto		; 
	private String  codMac                  ; 
	private String  tipoEncuentro           ; 
	private Long 	encuentro               ; 
	private String  codigoSap               ; 
	private String  descripActivoMolecula   ; 
	private String  descripPresentGenerico  ; 
	private String  prestacion              ; 
	private String  codAfiliado             ; 
	private String  codPaciente             ; 
	private String  nombrePaciente          ; 
	private String  clinica                 ; 
	private Date    fechaConsumo            ; 
	private String  descripGrpDiag          ; 
	private String  codDiagnostico          ; 
	private String  descripDiagnostico      ; 
	private Long 	lineaTratamiento        ;
	private String  medicoTratante          ; 
	private String  plan                    ; 
	private Long cantidadConsumo         ; 
	private Double  montoConsumo            ; 
	private Integer codConsumoMedicamento   ; 
	//--------------------------------------  
	private Date 	fechaCreacion       	; 
	private Integer usuarioCreacion         ; 
	private Date 	fechaModificacion   	; 
	private Integer usuarioModificacion     ; 	
	
	//---------------------------------------
	
	private Date  	fechaCarga;  	
	private Integer  responsableCarga;
	private Integer  estadoCarga;
	private Integer  registroTotal;
	private Integer  registroCargado;
	private Integer  registroError;
	private Integer  verLog;
	private Integer  descarga;
	private Integer  codArchivoLog;
	private Integer  codArchivoDescarga;		
	
	private Integer  indCampoObligatorio;
	private Integer  indCampoClave;
	
	//-------------------------------------------
	
	List<String>  obsDetalle = new ArrayList<String>();
	
	
	public Integer getCodHistControlGasto() {
		return codHistControlGasto;
	}
	public void setCodHistControlGasto(Integer codHistControlGasto) {
		this.codHistControlGasto = codHistControlGasto;
	}
	public String getCodMac() {
		return codMac;
	}
	public void setCodMac(String codMac) {
		this.codMac = codMac;
	}
	public String getTipoEncuentro() {
		return tipoEncuentro;
	}
	public void setTipoEncuentro(String tipoEncuentro) {
		this.tipoEncuentro = tipoEncuentro;
	}
	public Long getEncuentro() {
		return encuentro;
	}
	public void setEncuentro(Long encuentro) {
		this.encuentro = encuentro;
	}
	public String getCodigoSap() {
		return codigoSap;
	}
	public void setCodigoSap(String codigoSap) {
		this.codigoSap = codigoSap;
	}
	public String getDescripActivoMolecula() {
		return descripActivoMolecula;
	}
	public void setDescripActivoMolecula(String descripActivoMolecula) {
		this.descripActivoMolecula = descripActivoMolecula;
	}
	public String getDescripPresentGenerico() {
		return descripPresentGenerico;
	}
	public void setDescripPresentGenerico(String descripPresentGenerico) {
		this.descripPresentGenerico = descripPresentGenerico;
	}
	public String getPrestacion() {
		return prestacion;
	}
	public void setPrestacion(String prestacion) {
		this.prestacion = prestacion;
	}
	public String getCodAfiliado() {
		return codAfiliado;
	}
	public void setCodAfiliado(String codAfiliado) {
		this.codAfiliado = codAfiliado;
	}
	public String getCodPaciente() {
		return codPaciente;
	}
	public void setCodPaciente(String codPaciente) {
		this.codPaciente = codPaciente;
	}
	public String getNombrePaciente() {
		return nombrePaciente;
	}
	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}
	public String getClinica() {
		return clinica;
	}
	public void setClinica(String clinica) {
		this.clinica = clinica;
	}
	public Date getFechaConsumo() {
		return fechaConsumo;
	}
	public void setFechaConsumo(Date fechaConsumo) {
		this.fechaConsumo = fechaConsumo;
	}
	public String getDescripGrpDiag() {
		return descripGrpDiag;
	}
	public void setDescripGrpDiag(String descripGrpDiag) {
		this.descripGrpDiag = descripGrpDiag;
	}
	public String getCodDiagnostico() {
		return codDiagnostico;
	}

	public void setCodDiagnostico(String codDiagnostico) {
		this.codDiagnostico = codDiagnostico;
	}
	public String getDescripDiagnostico() {
		return descripDiagnostico;
	}
	public void setDescripDiagnostico(String descripDiagnostico) {
		this.descripDiagnostico = descripDiagnostico;
	}
	public Long getLineaTratamiento() {
		return lineaTratamiento;
	}
	public void setLineaTratamiento(Long lineaTratamiento) {
		this.lineaTratamiento = lineaTratamiento;
	}
	public String getMedicoTratante() {
		return medicoTratante;
	}
	public void setMedicoTratante(String medicoTratante) {
		this.medicoTratante = medicoTratante;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public Long getCantidadConsumo() {
		return cantidadConsumo;
	}
	public void setCantidadConsumo(Long cantidadConsumo) {
		this.cantidadConsumo = cantidadConsumo;
	}
	public Double getMontoConsumo() {
		return montoConsumo;
	}
	public void setMontoConsumo(Double montoConsumo) {
		this.montoConsumo = montoConsumo;
	}
	public Integer getCodConsumoMedicamento() {
		return codConsumoMedicamento;
	}
	public void setCodConsumoMedicamento(Integer codConsumoMedicamento) {
		this.codConsumoMedicamento = codConsumoMedicamento;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Integer getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(Integer usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public Integer getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setUsuarioModificacion(Integer usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	public Date getFechaCarga() {
		return fechaCarga;
	}
	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
	public Integer getResponsableCarga() {
		return responsableCarga;
	}
	public void setResponsableCarga(Integer responsableCarga) {
		this.responsableCarga = responsableCarga;
	}
	public Integer getEstadoCarga() {
		return estadoCarga;
	}
	public void setEstadoCarga(Integer estadoCarga) {
		this.estadoCarga = estadoCarga;
	}
	public Integer getRegistroTotal() {
		return registroTotal;
	}
	public void setRegistroTotal(Integer registroTotal) {
		this.registroTotal = registroTotal;
	}
	public Integer getRegistroCargado() {
		return registroCargado;
	}
	public void setRegistroCargado(Integer registroCargado) {
		this.registroCargado = registroCargado;
	}
	public Integer getRegistroError() {
		return registroError;
	}
	public void setRegistroError(Integer registroError) {
		this.registroError = registroError;
	}
	public Integer getVerLog() {
		return verLog;
	}
	public void setVerLog(Integer verLog) {
		this.verLog = verLog;
	}
	public Integer getDescarga() {
		return descarga;
	}
	public void setDescarga(Integer descarga) {
		this.descarga = descarga;
	}
	public Integer getCodArchivoLog() {
		return codArchivoLog;
	}
	public void setCodArchivoLog(Integer codArchivoLog) {
		this.codArchivoLog = codArchivoLog;
	}
	public Integer getCodArchivoDescarga() {
		return codArchivoDescarga;
	}
	public void setCodArchivoDescarga(Integer codArchivoDescarga) {
		this.codArchivoDescarga = codArchivoDescarga;
	}

	public List<String> getObsDetalle() {
		return obsDetalle;
	}
	public void setObsDetalle(List<String> obsDetalle) {
		this.obsDetalle = obsDetalle;
	}
	
	public Integer getIndCampoObligatorio() {
		return indCampoObligatorio;
	}
	public void setIndCampoObligatorio(Integer indCampoObligatorio) {
		this.indCampoObligatorio = indCampoObligatorio;
	}
	public Integer getIndCampoClave() {
		return indCampoClave;
	}
	public void setIndCampoClave(Integer indCampoClave) {
		this.indCampoClave = indCampoClave;
	}
	

}
