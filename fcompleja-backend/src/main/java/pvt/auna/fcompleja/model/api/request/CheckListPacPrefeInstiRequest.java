package pvt.auna.fcompleja.model.api.request;

public class CheckListPacPrefeInstiRequest {
	private Integer codSolicitudEvaluacion;
	private Integer cumpleCheckListPerfilPac;
	private Integer cumplePrefeInsti;
	private Integer codMac;
	private Integer pertinencia;
	private Integer condicionPaciente;
	private Integer tiempoUso;
	private Integer resultadoAutorizador;
	private String comentario;
	private Integer codigoRolUsuario;
	private String  fechaEstado;
	private Integer codigoUsuario;
	private Integer flagGrabar;
	private Integer codReporte;
	
	
	public Integer getCodReporte() {
		return codReporte;
	}
	public void setCodReporte(Integer codReporte) {
		this.codReporte = codReporte;
	}
	public Integer getCodSolicitudEvaluacion() {
		return codSolicitudEvaluacion;
	}
	public void setCodSolicitudEvaluacion(Integer codSolicitudEvaluacion) {
		this.codSolicitudEvaluacion = codSolicitudEvaluacion;
	}
	public Integer getCumpleCheckListPerfilPac() {
		return cumpleCheckListPerfilPac;
	}
	public void setCumpleCheckListPerfilPac(Integer cumpleCheckListPerfilPac) {
		this.cumpleCheckListPerfilPac = cumpleCheckListPerfilPac;
	}
	public Integer getCumplePrefeInsti() {
		return cumplePrefeInsti;
	}
	public void setCumplePrefeInsti(Integer cumplePrefeInsti) {
		this.cumplePrefeInsti = cumplePrefeInsti;
	}
	public Integer getCodMac() {
		return codMac;
	}
	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}
	public Integer getPertinencia() {
		return pertinencia;
	}
	public void setPertinencia(Integer pertinencia) {
		this.pertinencia = pertinencia;
	}
	public Integer getCondicionPaciente() {
		return condicionPaciente;
	}
	public void setCondicionPaciente(Integer condicionPaciente) {
		this.condicionPaciente = condicionPaciente;
	}
	public Integer getTiempoUso() {
		return tiempoUso;
	}
	public void setTiempoUso(Integer tiempoUso) {
		this.tiempoUso = tiempoUso;
	}
	public Integer getResultadoAutorizador() {
		return resultadoAutorizador;
	}
	public void setResultadoAutorizador(Integer resultadoAutorizador) {
		this.resultadoAutorizador = resultadoAutorizador;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Integer getCodigoRolUsuario() {
		return codigoRolUsuario;
	}
	public void setCodigoRolUsuario(Integer codigoRolUsuario) {
		this.codigoRolUsuario = codigoRolUsuario;
	}
	public String getFechaEstado() {
		return fechaEstado;
	}
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}
	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	public Integer getFlagGrabar() {
		return flagGrabar;
	}
	public void setFlagGrabar(Integer flagGrabar) {
		this.flagGrabar = flagGrabar;
	}
	@Override
	public String toString() {
		return "CheckListPacPrefeInstiRequest [codSolicitudEvaluacion=" + codSolicitudEvaluacion
				+ ", cumpleCheckListPerfilPac=" + cumpleCheckListPerfilPac + ", cumplePrefeInsti=" + cumplePrefeInsti
				+ ", codMac=" + codMac + ", pertinencia=" + pertinencia + ", condicionPaciente=" + condicionPaciente
				+ ", tiempoUso=" + tiempoUso + ", resultadoAutorizador=" + resultadoAutorizador + ", comentario="
				+ comentario + ", codigoRolUsuario=" + codigoRolUsuario + ", fechaEstado=" + fechaEstado
				+ ", codigoUsuario=" + codigoUsuario + ", flagGrabar=" + flagGrabar + ", codReporte=" + codReporte +"]";
	}

}
