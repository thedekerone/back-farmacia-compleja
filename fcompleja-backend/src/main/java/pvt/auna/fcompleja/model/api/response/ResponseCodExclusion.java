package pvt.auna.fcompleja.model.api.response;

public class ResponseCodExclusion {
	
private Number codExclusion;

public ResponseCodExclusion() {
	super();
	// TODO Auto-generated constructor stub
}

public Number getCodExclusion() {
	return codExclusion;
}

public void setCodExclusion(Number codExclusion) {
	this.codExclusion = codExclusion;
}

@Override
public String toString() {
	return "ResponseCodExclusion [codExclusion=" + codExclusion + "]"; }

}
