package pvt.auna.fcompleja.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.seguridad.AccesoMenuResponse;

public class UsuarioMenuRowMapper implements RowMapper<AccesoMenuResponse> {

	@Override
	public AccesoMenuResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		AccesoMenuResponse menu = new AccesoMenuResponse();
		menu.setCodMenu(rs.getInt("COD_MENU"));
		menu.setNombreCorto(rs.getString("NOM_CORTO"));
		menu.setNombreLargo(rs.getString("NOM_LARGO"));
		menu.setUrl(rs.getString("URL"));
		menu.setRutaImg(rs.getString("RUTA_IMG"));
		
		return menu;
	}

}
