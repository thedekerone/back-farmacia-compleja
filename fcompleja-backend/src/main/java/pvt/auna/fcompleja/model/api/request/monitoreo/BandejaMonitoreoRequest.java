package pvt.auna.fcompleja.model.api.request.monitoreo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class BandejaMonitoreoRequest {

	private String codigoPaciente;
	private int estadoMonitoreo;
	private String codigoClinica;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaMonitoreo;
	private int codigoResponsableMonitoreo;
	/*private String nombre;
	private String apePaterno;
	private String apeMaterno;
	private String tipoDoc;
	private String nroDoc;*/
	public BandejaMonitoreoRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getCodigoPaciente() {
		return codigoPaciente;
	}

	public void setCodigoPaciente(String codigoPaciente) {
		this.codigoPaciente = codigoPaciente;
	}

	public int getEstadoMonitoreo() {
		return estadoMonitoreo;
	}

	public void setEstadoMonitoreo(int estadoMonitoreo) {
		this.estadoMonitoreo = estadoMonitoreo;
	}

	public String getCodigoClinica() {
		return codigoClinica;
	}

	public void setCodigoClinica(String codigoClinica) {
		this.codigoClinica = codigoClinica;
	}

	public Date getFechaMonitoreo() {
		return fechaMonitoreo;
	}

	public void setFechaMonitoreo(Date fechaMonitoreo) {
		this.fechaMonitoreo = fechaMonitoreo;
	}

	public int getCodigoResponsableMonitoreo() {
		return codigoResponsableMonitoreo;
	}

	public void setCodigoResponsableMonitoreo(int codigoResponsableMonitoreo) {
		this.codigoResponsableMonitoreo = codigoResponsableMonitoreo;
	}
	
	/*public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApePaterno() {
		return apePaterno;
	}

	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	public String getApeMaterno() {
		return apeMaterno;
	}

	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getNroDoc() {
		return nroDoc;
	}

	public void setNroDoc(String nroDoc) {
		this.nroDoc = nroDoc;
	}*/

	@Override
	public String toString() {
		return "BandejaMonitoreoRequest [codigoPaciente=" + codigoPaciente + ", estadoMonitoreo=" + estadoMonitoreo
				+ ", codigoClinica=" + codigoClinica + ", fechaMonitoreo=" + fechaMonitoreo
				+ ", codigoResponsableMonitoreo=" + codigoResponsableMonitoreo  + "]";
	}
/*+  ", nombre=" + nombre +  ", apellidoPaterno="
				+ apePaterno +  ", apellidoMaterno=" + apeMaterno + ", tipoDocumento"+ tipoDoc
				+ ", numeroDocuemnto" + nroDoc*/
}
