package pvt.auna.fcompleja.model.api.request.monitoreo;

import java.io.Serializable;

public class MarcadorRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long codMac;
	private Long codGrpDiag;
	private Long codSolEva;
	private Integer pEstado;

	public MarcadorRequest() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodMac() {
		return codMac;
	}

	public void setCodMac(Long codMac) {
		this.codMac = codMac;
	}

	public Long getCodGrpDiag() {
		return codGrpDiag;
	}

	public void setCodGrpDiag(Long codGrpDiag) {
		this.codGrpDiag = codGrpDiag;
	}

	public Long getCodSolEva() {
		return codSolEva;
	}

	public void setCodSolEva(Long codSolEva) {
		this.codSolEva = codSolEva;
	}

	public Integer getpEstado() {
		return pEstado;
	}

	public void setpEstado(Integer pEstado) {
		this.pEstado = pEstado;
	}

	@Override
	public String toString() {
		return "MarcadorRequest [codMac=" + codMac + ", codGrpDiag=" + codGrpDiag + ", codSolEva=" + codSolEva
				+ ", pEstado=" + pEstado + "]";
	}

}
