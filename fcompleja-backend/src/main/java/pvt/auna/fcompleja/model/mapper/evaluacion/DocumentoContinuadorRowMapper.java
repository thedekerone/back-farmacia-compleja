package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.HistPacienteResponse;

public class DocumentoContinuadorRowMapper implements RowMapper<HistPacienteResponse> {

	@Override
	public HistPacienteResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		HistPacienteResponse response = new HistPacienteResponse();
		response.setCodContinuadorDoc(rs.getInt("COD_CONTINUADOR_DOC"));
		response.setTipoDocumento(rs.getInt("P_TIPO_DOCUMENTO"));
		response.setNombreTipoDocumento(rs.getString("NOMBRE_TIPO_DOCUMENTO"));
		response.setDescripcionDocumento(rs.getString("DESCRIPCION_DOCUMENTO"));
		response.setEstadoDocumento(rs.getInt("P_ESTADO"));
		response.setDescripcionEstado(rs.getString("DESCRIPCION_ESTADO"));
		return response;
	}

}
