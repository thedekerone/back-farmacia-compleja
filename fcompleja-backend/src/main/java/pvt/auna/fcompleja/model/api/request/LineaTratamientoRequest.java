package pvt.auna.fcompleja.model.api.request;

import java.io.Serializable;

public class LineaTratamientoRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long   codigoHistLineaTrat;
	private String codigoAfiliado;
	private Long   codigoMac;

	public LineaTratamientoRequest() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodigoHistLineaTrat() {
		return codigoHistLineaTrat;
	}

	public void setCodigoHistLineaTrat(Long codigoHistLineaTrat) {
		this.codigoHistLineaTrat = codigoHistLineaTrat;
	}

	public String getCodigoAfiliado() {
		return codigoAfiliado;
	}

	public void setCodigoAfiliado(String codigoAfiliado) {
		this.codigoAfiliado = codigoAfiliado;
	}

	public Long getCodigoMac() {
		return codigoMac;
	}

	public void setCodigoMac(Long codigoMac) {
		this.codigoMac = codigoMac;
	}

	@Override
	public String toString() {
		return "LineaTratamientoRequest [codigoHistLineaTrat=" + codigoHistLineaTrat + ", codigoAfiliado="
				+ codigoAfiliado + ", codigoMac=" + codigoMac + "]";
	}

}
