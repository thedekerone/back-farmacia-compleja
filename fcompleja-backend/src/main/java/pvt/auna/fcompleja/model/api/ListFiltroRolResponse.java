package pvt.auna.fcompleja.model.api;

import java.io.Serializable;

public class ListFiltroRolResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer codigoUsuario;
	private Integer codigoRol;
	private String  NombreUsuarioRol;
	private Integer codUsuario;
	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}
	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	public Integer getCodigoRol() {
		return codigoRol;
	}
	public void setCodigoRol(Integer codigoRol) {
		this.codigoRol = codigoRol;
	}
	public String getNombreUsuarioRol() {
		return NombreUsuarioRol;
	}
	public void setNombreUsuarioRol(String nombreUsuarioRol) {
		NombreUsuarioRol = nombreUsuarioRol;
	}
	public Integer getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}
}
