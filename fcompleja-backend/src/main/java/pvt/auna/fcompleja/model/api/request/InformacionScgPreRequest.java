package pvt.auna.fcompleja.model.api.request;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class InformacionScgPreRequest {

	private Integer codSol;
	private Integer estadoSolPre;
	private String estadoSol;
	private Integer codMac;
	private Integer codUsuario;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
	private Date fechaActual;

	public Integer getCodSol() {
		return codSol;
	}

	public void setCodSol(Integer codSol) {
		this.codSol = codSol;
	}

	public Integer getEstadoSolPre() {
		return estadoSolPre;
	}

	public void setEstadoSolPre(Integer estadoSolPre) {
		this.estadoSolPre = estadoSolPre;
	}

	public String getEstadoSol() {
		return estadoSol;
	}

	public void setEstadoSol(String estadoSol) {
		this.estadoSol = estadoSol;
	}

	public Integer getCodMac() {
		return codMac;
	}

	public void setCodMac(Integer codMac) {
		this.codMac = codMac;
	}

	public Integer getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	@Override
	public String toString() {
		return "InformacionScgPreRequest [codSol=" + codSol + ", estadoSolPre=" + estadoSolPre + ", estadoSol="
				+ estadoSol + ", codMac=" + codMac + ", codUsuario=" + codUsuario + ", fechaActual=" + fechaActual
				+ "]";
	}

}
