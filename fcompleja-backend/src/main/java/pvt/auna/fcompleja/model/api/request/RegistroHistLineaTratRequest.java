package pvt.auna.fcompleja.model.api.request;

public class RegistroHistLineaTratRequest {
	
		private String  codAfiliado;
		private Integer codSolicitudEvaluacion;
		private Integer codMac;
		
		private String fechaInicio;
		private String fechaFin;
		
		private Integer motivoInactivacion;
		
		private String fechaInactivacion;
		private Integer numeroCurso;
		private Integer tipoTumor;
		private Integer respuestaAlcanzada;
		private Integer lugarProgresion;
		private String codigoMedicoTratante;
		private String medicoTratante;
		
		public String getCodAfiliado() {
			return codAfiliado;
		}
		public void setCodAfiliado(String codAfiliado) {
			this.codAfiliado = codAfiliado;
		}
		public Integer getCodSolicitudEvaluacion() {
			return codSolicitudEvaluacion;
		}
		public void setCodSolicitudEvaluacion(Integer codSolicitudEvaluacion) {
			this.codSolicitudEvaluacion = codSolicitudEvaluacion;
		}
		public Integer getCodMac() {
			return codMac;
		}
		public void setCodMac(Integer codMac) {
			this.codMac = codMac;
		}
		public String getFechaInicio() {
			return fechaInicio;
		}
		public void setFechaInicio(String fechaInicio) {
			this.fechaInicio = fechaInicio;
		}
		public String getFechaFin() {
			return fechaFin;
		}
		public void setFechaFin(String fechaFin) {
			this.fechaFin = fechaFin;
		}
		public Integer getMotivoInactivacion() {
			return motivoInactivacion;
		}
		public void setMotivoInactivacion(Integer motivoInactivacion) {
			this.motivoInactivacion = motivoInactivacion;
		}
		public String getFechaInactivacion() {
			return fechaInactivacion;
		}
		public void setFechaInactivacion(String fechaInactivacion) {
			this.fechaInactivacion = fechaInactivacion;
		}
		public Integer getNumeroCurso() {
			return numeroCurso;
		}
		public void setNumeroCurso(Integer numeroCurso) {
			this.numeroCurso = numeroCurso;
		}
		public Integer getTipoTumor() {
			return tipoTumor;
		}
		public void setTipoTumor(Integer tipoTumor) {
			this.tipoTumor = tipoTumor;
		}
		public Integer getRespuestaAlcanzada() {
			return respuestaAlcanzada;
		}
		public void setRespuestaAlcanzada(Integer respuestaAlcanzada) {
			this.respuestaAlcanzada = respuestaAlcanzada;
		}
		public Integer getLugarProgresion() {
			return lugarProgresion;
		}
		public void setLugarProgresion(Integer lugarProgresion) {
			this.lugarProgresion = lugarProgresion;
		}
		public String getCodigoMedicoTratante() {
			return codigoMedicoTratante;
		}
		public void setCodigoMedicoTratante(String codigoMedicoTratante) {
			this.codigoMedicoTratante = codigoMedicoTratante;
		}
		public String getMedicoTratante() {
			return medicoTratante;
		}
		public void setMedicoTratante(String medicoTratante) {
			this.medicoTratante = medicoTratante;
		}
		@Override
		public String toString() {
			return "RegistroHistLineaTratRequest [codAfiliado=" + codAfiliado + ", codSolicitudEvaluacion="
					+ codSolicitudEvaluacion + ", codMac=" + codMac + ", fechaInicio=" + fechaInicio + ", fechaFin="
					+ fechaFin + ", motivoInactivacion=" + motivoInactivacion + ", fechaInactivacion="
					+ fechaInactivacion + ", numeroCurso=" + numeroCurso + ", tipoTumor=" + tipoTumor
					+ ", respuestaAlcanzada=" + respuestaAlcanzada + ", lugarProgresion=" + lugarProgresion
					+ ", codigoMedicoTratante=" + codigoMedicoTratante + ", medicoTratante=" + medicoTratante + "]";
		}		
		
	
}
