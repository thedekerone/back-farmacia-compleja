package pvt.auna.fcompleja.model.api.response;

import java.io.Serializable;

public class ListaResponseCmac implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String  codigoEvaluacionL;
	private Integer codigoEvaluacionC;
	private String  nombrePaciente;
	private Integer codigoPaciente;
	private String  nombreDiagnostico;
	private String codigoDiagnostico;
	private String nombreMedicamento;
	
	public String getCodigoEvaluacionL() {
		return codigoEvaluacionL;
	}
	public void setCodigoEvaluacionL(String codigoEvaluacionL) {
		this.codigoEvaluacionL = codigoEvaluacionL;
	}
	public Integer getCodigoEvaluacionC() {
		return codigoEvaluacionC;
	}
	public void setCodigoEvaluacionC(Integer codigoEvaluacionC) {
		this.codigoEvaluacionC = codigoEvaluacionC;
	}
	public String getNombrePaciente() {
		return nombrePaciente;
	}
	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}
	public Integer getCodigoPaciente() {
		return codigoPaciente;
	}
	public void setCodigoPaciente(Integer codigoPaciente) {
		this.codigoPaciente = codigoPaciente;
	}
	public String getNombreDiagnostico() {
		return nombreDiagnostico;
	}
	public void setNombreDiagnostico(String nombreDiagnostico) {
		this.nombreDiagnostico = nombreDiagnostico;
	}
	public String getCodigoDiagnostico() {
		return codigoDiagnostico;
	}
	public void setCodigoDiagnostico(String codigoDiagnostico) {
		this.codigoDiagnostico = codigoDiagnostico;
	}
	public String getNombreMedicamento() {
		return nombreMedicamento;
	}
	public void setNombreMedicamento(String nombreMedicamento) {
		this.nombreMedicamento = nombreMedicamento;
	}
	@Override
	public String toString() {
		return "ListaResponseCmac [codigoEvaluacionL=" + codigoEvaluacionL + ", codigoEvaluacionC=" + codigoEvaluacionC
				+ ", nombrePaciente=" + nombrePaciente + ", codigoPaciente=" + codigoPaciente + ", nombreDiagnostico="
				+ nombreDiagnostico + ", codigoDiagnostico=" + codigoDiagnostico + ", nombreMedicamento="
				+ nombreMedicamento + "]";
	}

}
