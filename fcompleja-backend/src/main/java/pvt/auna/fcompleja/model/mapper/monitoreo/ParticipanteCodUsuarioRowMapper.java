package pvt.auna.fcompleja.model.mapper.monitoreo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.bean.ParticipanteBean;

public class ParticipanteCodUsuarioRowMapper implements RowMapper<ParticipanteBean> {

	@Override
	public ParticipanteBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ParticipanteBean part = new ParticipanteBean();
		part.setCodUsuario(rs.getInt("COD_USUARIO"));

		return part;
	}

}