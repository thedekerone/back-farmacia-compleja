package pvt.auna.fcompleja.model.api.request;

public class RequestRegCritInclucion {
	 private Integer codChkListIndi;
	  private Integer codInclusion;
	  private String descripCriterio;
	  private Integer estado;
	  
	public RequestRegCritInclucion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getCodChkListIndi() {
		return codChkListIndi;
	}

	public void setCodChkListIndi(Integer codChkListIndi) {
		this.codChkListIndi = codChkListIndi;
	}

	public Integer getCodInclusion() {
		return codInclusion;
	}

	public void setCodExclusion(Integer codInclusion) {
		this.codInclusion = codInclusion;
	}

	public String getDescripCriterio() {
		return descripCriterio;
	}

	public void setDescripCriterio(String descripCriterio) {
		this.descripCriterio = descripCriterio;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	  
	@Override
	public String toString() {
		return "RequestRegCritInclucion [codChkListIndi=" + codChkListIndi + ", codInclusion=" + codInclusion + 
				", descripCriterio=" + descripCriterio +", estado=" + estado +"]";
	}
	  
}
