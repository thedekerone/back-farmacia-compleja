package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.evaluacion.ComboResultadoBasalResponse;
import pvt.auna.fcompleja.model.api.response.evaluacion.ResultadoBasalDetResponse;

public class ValorFijoRowMapper implements RowMapper<ComboResultadoBasalResponse>{

	@Override
	public ComboResultadoBasalResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
		ComboResultadoBasalResponse response = new ComboResultadoBasalResponse();
		response.setCodExamenMedDetalle(rs.getInt("COD_EXAMEN_MED_DET"));
		response.setCodExamenMed(rs.getInt("COD_EXAMEN_MED"));
		response.setTipoIngresoResulDescrip(rs.getString("TIPO_INGRESO_RES"));
		response.setValorFijo(rs.getString("VALOR_FIJO"));
		return response;
	}
		
}
