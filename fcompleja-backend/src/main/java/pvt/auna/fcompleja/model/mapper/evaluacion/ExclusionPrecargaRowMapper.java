package pvt.auna.fcompleja.model.mapper.evaluacion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pvt.auna.fcompleja.model.api.response.CriterioExclusion;

public class ExclusionPrecargaRowMapper implements RowMapper<CriterioExclusion> {

	@Override
	public CriterioExclusion mapRow(ResultSet rs, int rowNum) throws SQLException {
		CriterioExclusion criterio = new CriterioExclusion();
		criterio.setCodigo(rs.getInt("COD_CRITERIO_EXCLU"));
		criterio.setCriterioExclusion(rs.getInt("P_CRITERIO_EXCLU"));
		return criterio;
	}

}
