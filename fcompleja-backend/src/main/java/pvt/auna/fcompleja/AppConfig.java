package pvt.auna.fcompleja;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
public class AppConfig {
	
	private static final Logger log = LoggerFactory.getLogger(AppConfig.class);

	@Bean
	public Boolean disableSSLValidation() throws Exception {
		final SSLContext sslContext = SSLContext.getInstance("TLS");

		sslContext.init(null, new TrustManager[] { new X509TrustManager() {
			@Override
			public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}
		} }, null);

		HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		});

		return true;
	}

	@PostConstruct
	public void init() {
		try {
			TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
			log.info("Timezone configurada es: " + TimeZone.getDefault().getDisplayName());
			log.info("Fecha Actual: " + new Date().toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@EventListener(AppConfig.class)
	public void doSomethingAfterStartup() {
		try {
			/*
			 * pcmMaestroService.listarDatosMaestro();
			 * pcmWebServiceService.listarWebServices();
			 * consultaSiteDsService.listarIafasActivas(null);
			 * sedeService.getListaSedesAll(null);
			 */
		} catch (Exception e) {

		}

	}

}
