package pvt.auna.fcompleja.test;

//@RunWith(value=SpringRunner.class)
//@SpringBootTest(classes={Application.class})
//public class ModuloConfiguracionTest {
//    @Autowired
//    MacService macService;
//    private final Logger LOGGER = Logger.getLogger(this.getClass());
//
//    @Test
//    public void registrarChkListIndicacion() {
//    	
//        CheckListBean checkListBean = new CheckListBean();
//        ApiOutResponse apiOutResponse = new ApiOutResponse();
//        checkListBean.setCodEstado(Integer.valueOf(8));
//        checkListBean.setCodGrpDiag(Integer.valueOf(10));
//        checkListBean.setDescripcion("NCT00638690 - ABIRATERONE IN CASTRATION-RESISTANT PROSTATE CANCER PREVIOUSLY TREATED WITH DOCETAXEL-BASED CHEMOTHERAPY");
//        checkListBean.setFechaIniVigencia(new Date());
//        checkListBean.setCodMac(Integer.valueOf(1));
//        //checkListBean.setCodChkListIndi(Integer.valueOf(1));
//        apiOutResponse = this.macService.registrarCheckListConfiguracion(checkListBean);
//        this.LOGGER.info((Object)("Registro de CheckList : " + apiOutResponse.getMsgResultado()));
//        Assertions.assertThat((Integer)apiOutResponse.getCodResultado()).isEqualTo(0);
//    }
//
//    @Test
//    public void registrarRegistroInclusion() {
//        CriteriosBean criterioBean = new CriteriosBean();
//        ApiOutResponse apiOutResponse = new ApiOutResponse();
//        criterioBean.setCodChkListIndi(Integer.valueOf(1));
//        criterioBean.setCodMac(Integer.valueOf(1));
//        criterioBean.setCodGrpDiag(Integer.valueOf(1));
//        criterioBean.setCodEstado(Integer.valueOf(8));
//        criterioBean.setDescripcion("NCT00638690 - Abiraterone in Castration-Resistant Prostate Cancer Previously Treated With Docetaxel-Based Chemotherapy");
//        criterioBean.setOrden(Integer.valueOf(1));
//        criterioBean.setCodCriterio(Integer.valueOf(2));
//        apiOutResponse = this.macService.registroCriterioInclusion(criterioBean);
//        this.LOGGER.info((Object)("Registro de Inclusion : " + apiOutResponse.getMsgResultado()));
//        Assertions.assertThat((Integer)apiOutResponse.getCodResultado()).isEqualTo(0);
//    }
//
//    @Test
//    public void registrarRegistroExclusion() {
//        CriteriosBean criterioBean = new CriteriosBean();
//        ApiOutResponse apiOutResponse = new ApiOutResponse();
//        criterioBean.setCodChkListIndi(Integer.valueOf(1));
//        criterioBean.setCodMac(Integer.valueOf(1));
//        criterioBean.setCodGrpDiag(Integer.valueOf(1));
//        criterioBean.setCodEstado(Integer.valueOf(8));
//        criterioBean.setDescripcion("NCT00638690 - Abiraterone in Castration-Resistant Prostate Cancer Previously Treated With Docetaxel-Based Chemotherapy");
//        criterioBean.setOrden(Integer.valueOf(1));
//        apiOutResponse = this.macService.registroCriterioExclusion(criterioBean);
//        this.LOGGER.info((Object)("Registro de Exclusion : " + apiOutResponse.getMsgResultado()));
//        Assertions.assertThat((Integer)apiOutResponse.getCodResultado()).isEqualTo(0);
//    }
//
//    @SuppressWarnings("unchecked")
//	@Test
//    public void listarCheckListConf() {
//        CheckListBean checkListBean = new CheckListBean();
//        ApiOutResponse apiOutResponse = new ApiOutResponse();
//        checkListBean.setCodMac(Integer.valueOf(1));
//        checkListBean.setCodGrpDiag(Integer.valueOf(1));
//        apiOutResponse = this.macService.listaCheckListConfiguracion(checkListBean);
//        this.LOGGER.info((Object)("Listar Check List Configuracion : " + apiOutResponse.getMsgResultado()));
//        if (apiOutResponse.getCodResultado() == 0) {
//            ((ArrayList<CheckListBean>)apiOutResponse.getResponse()).forEach((final CheckListBean lista) -> 
//            System.out.println( lista.getCodChkListIndi() + "|" +  lista.getCodMac() + "|" +  lista.getDescripcion()));
//        }
//        Assertions.assertThat((Integer)apiOutResponse.getCodResultado()).isEqualTo(0);
//    }
//}
// 
// 
