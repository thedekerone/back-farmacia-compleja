package pvt.auna.fcompleja.repository;

import java.util.ArrayList;

import pvt.auna.fcompleja.model.bean.GrupoDiagnosticoBean;

public interface GrupoDiagnosticoRepository {
	public ArrayList<GrupoDiagnosticoBean> getListarGrpDiagnostico();
}
