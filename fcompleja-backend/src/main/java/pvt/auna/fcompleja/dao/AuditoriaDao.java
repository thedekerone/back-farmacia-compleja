package pvt.auna.fcompleja.dao;

public interface AuditoriaDao {
	
	/**
	 * Metodo que permite registrar  Errores DML que se sucitan en pago de citas  mediente un hilo para no afectar el proceso
	 * nbormal en primer plano
	 * @param codProcess
	 * @param idNumEvento
	 * @param idNumLog
	 * @param nombreSp
	 * @param lmensajeError
	 * @param tipoProcess
	 * @return
	 */
	public Integer registrarErrorDmlLogerThread(Integer codProcess,Integer idNumEvento, Integer idNumLog, String nombreSp, String lmensajeError,String tipoProcess);
	
}
