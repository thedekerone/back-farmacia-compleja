package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.dao.MonitoreoDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.LineaTratamientoRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.BandejaMonitoreoRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionMarcadorRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.MarcadorRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.MonitoreoEvolucionRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.SegEjecutivoRequest;
import pvt.auna.fcompleja.model.api.response.monitoreo.DetalleMarcadorResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.EvolucionResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.LineaTratamientoResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.MarcadorResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoEvolucionResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.SegEjecutivoResponse;
import pvt.auna.fcompleja.model.mapper.monitoreo.DetalleMarcadorRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.EvolucionMarcadorRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.EvolucionMarcadoresRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.EvolucionRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.LineaTratamientoRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.MarcadorRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.MonitoreoEvolucionRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.MonitoreoRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.SegEjecutivoRowMapper;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;

@Repository
public class MonitoreoDaoImpl implements MonitoreoDao {

	private final Logger logger = Logger.getLogger(getClass());

	@Autowired
	DataSource dataSource;
	SimpleDateFormat sdf;

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse listaMonitoreo(BandejaMonitoreoRequest bandejaMonitoreo) {
		// TODO Auto-generated method stub
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<MonitoreoResponse> lista = new ArrayList<>();

		try {
			
			System.out.println(bandejaMonitoreo );
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_BANDEJA_MONITOREO")
					.returningResultSet("AC_LISTAMONITOREO", new MonitoreoRowMapper());

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PV_COD_PACIENTE", bandejaMonitoreo.getCodigoPaciente() == null ? "000" : bandejaMonitoreo.getCodigoPaciente(), Types.VARCHAR);
			in.addValue("PN_ESTADO_MONITOREO", bandejaMonitoreo.getEstadoMonitoreo(), Types.NUMERIC);
			in.addValue("PV_COD_CLINICA", bandejaMonitoreo.getCodigoClinica() == null ? "000" : bandejaMonitoreo.getCodigoClinica(), Types.VARCHAR);
			in.addValue("PV_FECHA_MONITOREO", DateUtils.getDateToStringDDMMYYYY(bandejaMonitoreo.getFechaMonitoreo()), Types.VARCHAR);
			in.addValue("PN_COD_RESP_MONITOREO", bandejaMonitoreo.getCodigoResponsableMonitoreo(), Types.NUMERIC);
			/*in.addValue("PV_NOMBRE_PAC", bandejaMonitoreo.getNombre(), Types.VARCHAR);
			in.addValue("PV_APE_PAT_PAC", bandejaMonitoreo.getApePaterno(), Types.VARCHAR);
			in.addValue("PV_APE_MAT_PAC", bandejaMonitoreo.getApeMaterno(), Types.VARCHAR);
			in.addValue("PV_TIPO_DOC", bandejaMonitoreo.getTipoDoc(), Types.VARCHAR);
			in.addValue("PV_NUM_DOC", bandejaMonitoreo.getNroDoc(), Types.VARCHAR);*/

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			lista = (List<MonitoreoResponse>) out.get("AC_LISTAMONITOREO");
			outResponse.setResponse(lista);

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return outResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse historialLineaTratamiento(LineaTratamientoRequest request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<LineaTratamientoResponse> Lista = new ArrayList<>();

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_HIST_LIN_TRATAMIENTO")
					.returningResultSet("AC_LISTA", new LineaTratamientoRowMapper());

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_AFILIADO", request.getCodigoAfiliado(), Types.VARCHAR);
			in.addValue("PN_COD_HIST_LINEA_TRAT", null, Types.NUMERIC);
			in.addValue("PN_COD_MAC", null, Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			Lista = (List<LineaTratamientoResponse>) out.get("AC_LISTA");
			outResponse.setResponse(Lista);

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return outResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse getUltimoLineaTratamiento(LineaTratamientoRequest request) {
		// TODO Auto-generated method stub
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_HIST_LIN_TRATAMIENTO")
					.returningResultSet("AC_LISTA", new LineaTratamientoRowMapper());

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_HIST_LINEA_TRAT", request.getCodigoHistLineaTrat(), Types.NUMERIC);
			in.addValue("PN_COD_AFILIADO", null, Types.VARCHAR);
			in.addValue("PN_COD_MAC", null, Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			List<LineaTratamientoResponse> Lista = (List<LineaTratamientoResponse>) out.get("AC_LISTA");
			outResponse.setResponse(Lista.get(0));

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return outResponse;
	}

	@Override
	public ApiOutResponse regDatosEvolucion(EvolucionRequest request) {
		Map<String, Object> out = new HashMap<>();

		ApiOutResponse response = new ApiOutResponse();
		EvolucionRequest res = request;

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("sp_pfc_i_datos_evolucion");

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PV_NRO_DESC_EVOLUCION", request.getNroDescEvolucion(), Types.VARCHAR);
			in.addValue("PN_COD_MONITOREO", request.getCodMonitoreo(), Types.NUMERIC);
			in.addValue("PN_COD_MAC", request.getCodMac(), Types.NUMERIC);
			in.addValue("PN_P_RES_EVOLUCION", request.getpResEvolucion(), Types.NUMERIC);
			in.addValue("PN_COD_HIST_LINEA_TRAT", request.getCodHistLineaTrat(), Types.NUMERIC);
			in.addValue("PV_FEC_MONITOREO", DateUtils.getDateToStringDDMMYYYY(request.getFecMonitoreo()), Types.VARCHAR);
			in.addValue("PN_P_TOLERANCIA", request.getpTolerancia(), Types.NUMERIC);
			in.addValue("PN_P_TOXICIDAD", request.getpToxicidad(), Types.NUMERIC);
			in.addValue("PN_P_GRADO", request.getpGrado(), Types.NUMERIC);
			in.addValue("PN_P_RESP_CLINICA", request.getpRespClinica(), Types.NUMERIC);
			in.addValue("PN_P_ATEN_ALERTA", request.getpAtenAlerta(), Types.NUMERIC);
			in.addValue("PV_EXISTE_TOXICIDAD", request.getExisteToxicidad(), Types.VARCHAR);
			in.addValue("PN_NRO_EVOLUCION", request.getNroEvolucion(), Types.NUMERIC);
			in.addValue("PV_MARCADORES", request.getCadenaMarcadores(), Types.VARCHAR);
			in.addValue("PN_ESTADO", request.getEstado(), Types.NUMERIC);
			in.addValue("PV_USUARIO_CREA", request.getUsuarioCrea(), Types.VARCHAR);
			in.addValue("PN_P_ESTADO_MONITOREO", request.getpEstadoMonitoreo(), Types.NUMERIC);

			// INSERTAR MAS PARAMETROS

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			response.setResponse(res);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return response;
	}

	@Override
	public ApiOutResponse actDatosEvolucion(EvolucionRequest request) {
		Map<String, Object> out = new HashMap<>();

		ApiOutResponse response = new ApiOutResponse();
		EvolucionRequest res = request;

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("sp_pfc_u_datos_evolucion");

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_EVOLUCION", request.getCodEvolucion(), Types.NUMERIC);
			in.addValue("PN_COD_MONITOREO", request.getCodMonitoreo(), Types.NUMERIC);
			in.addValue("PV_FEC_MONITOREO", DateUtils.getDateToStringDDMMYYYY(request.getFecMonitoreo()), Types.VARCHAR);
			in.addValue("PN_P_TOLERANCIA", request.getpTolerancia(), Types.NUMERIC);
			in.addValue("PN_P_TOXICIDAD", request.getpToxicidad(), Types.NUMERIC);
			in.addValue("PN_P_GRADO", request.getpGrado(), Types.NUMERIC);
			in.addValue("PN_P_RESP_CLINICA", request.getpRespClinica(), Types.NUMERIC);
			in.addValue("PN_P_ATEN_ALERTA", request.getpAtenAlerta(), Types.NUMERIC);
			in.addValue("PV_EXISTE_TOXICIDAD", request.getExisteToxicidad(), Types.VARCHAR);
			in.addValue("PV_MARCADORES", request.getCadenaMarcadores(), Types.VARCHAR);
			in.addValue("PN_ESTADO", request.getEstado(), Types.NUMERIC);
			in.addValue("PV_USUARIO_MODIF", request.getUsuarioModif(), Types.VARCHAR);

			// INSERTAR MAS PARAMETROS

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			response.setResponse(res);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse consultarEvoluciones(EvolucionRequest request) {

		Map<String, Object> out = new HashMap<>();

		ApiOutResponse response = new ApiOutResponse();
		List<EvolucionResponse> lista = new ArrayList<>();

		try {
			/** Call store procedure */
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_LISTAR_EVOLUCION")
					.returningResultSet("AC_LISTA", new EvolucionRowMapper());

			logger.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_SOL_EVA", request.getCodSolEvaluacion(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			lista = (List<EvolucionResponse>) out.get("AC_LISTA");
			response.setResponse(lista);

			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return response;
	}

	@Override
	public ApiOutResponse regResultadoEvolucion(EvolucionRequest request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();

		try {
			/** Call store procedure */
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("sp_pfc_i_resultado_evolucion");

			logger.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_EVOLUCION", request.getCodEvolucion(), Types.NUMERIC);
			in.addValue("PN_P_RES_EVOLUCION", request.getpResEvolucion(), Types.NUMERIC);
			in.addValue("PN_COD_SOL_EVALUACION", request.getCodSolEvaluacion(), Types.NUMERIC);
			in.addValue("PV_DESC_COD_SOL_EVALUACION", request.getDescCodSolEvaluacion(), Types.VARCHAR);
			in.addValue("PN_COD_MONITOREO", request.getCodMonitoreo(), Types.NUMERIC);
			in.addValue("PN_P_ESTADO_MONITOREO", request.getpEstadoMonitoreo(), Types.NUMERIC);
			in.addValue("PV_FEC_MONITOREO", DateUtils.getDateToStringDDMMYYYY(request.getFecMonitoreo()), Types.VARCHAR);
			in.addValue("PV_FEC_PROX_MONITOREO", DateUtils.getDateToStringDDMMYYYY(request.getFecProxMonitoreo()), Types.VARCHAR);
			in.addValue("PN_P_MOTIVO_INACTIVACION", request.getpMotivoInactivacion(), Types.NUMERIC);
			in.addValue("PV_FEC_INACTIVACION", DateUtils.getDateToStringDDMMYYYY(request.getFecInactivacion()), Types.VARCHAR);
			in.addValue("PV_OBSERVACION", request.getObservacion(), Types.VARCHAR);
			in.addValue("PV_USUARIO_MODIF", request.getUsuarioModif(), Types.VARCHAR);
			in.addValue("PN_ESTADO", request.getEstado(), Types.NUMERIC);
			in.addValue("PN_COD_HIST_LINEA_TRAT", request.getCodHistLineaTrat(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			response.setResponse(request);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) != 0) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			logger.info("Error al listar : ", e);
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse listarMarcadores(MarcadorRequest request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<MarcadorResponse> lista = new ArrayList<>();
		List<DetalleMarcadorResponse> lDetalle = new ArrayList<>();

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("sp_pfc_s_listar_marcador")
					.returningResultSet("ac_lista", new MarcadorRowMapper())
					.returningResultSet("ac_lista_detalle", new DetalleMarcadorRowMapper());

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_MAC", request.getCodMac(), Types.NUMERIC);
			in.addValue("PN_COD_GRP_DIAG", request.getCodGrpDiag(), Types.NUMERIC);
			in.addValue("PN_P_ESTADO", request.getpEstado(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			lista = (List<MarcadorResponse>) out.get("ac_lista");
			lDetalle = (List<DetalleMarcadorResponse>) out.get("ac_lista_detalle");

			for (MarcadorResponse mar : lista) {
				List<DetalleMarcadorResponse> l = new ArrayList<>();
				for (DetalleMarcadorResponse dm : lDetalle) {
					if (dm.getCodMarcador() == mar.getCodMarcador()) {
						l.add(dm);
					}
				}
				mar.setListaDetalleMarcador(l);
			}

			outResponse.setResponse(lista);

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			logger.info("Error al listar : ", e);
		}
		return outResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse consultarDetalleEvolucion(EvolucionMarcadorRequest request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<EvolucionMarcadorRequest> lista = new ArrayList<>();

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_DETALLE_EVOLUCION")
					.returningResultSet("AC_LISTA", new EvolucionMarcadorRowMapper());

			logger.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_EVOLUCION", request.getCodEvolucion(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			lista = (List<EvolucionMarcadorRequest>) out.get("AC_LISTA");
			outResponse.setResponse(lista);

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			logger.info("Error al listar : ", e);
		}
		return outResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse listarHistorialMarcadores(MarcadorRequest request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<EvolucionMarcadorRequest> lista = new ArrayList<>();

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_HISTORIAL_MARCADORES")
					.returningResultSet("AC_LISTA", new EvolucionMarcadoresRowMapper());

			logger.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_SOL_EVA", request.getCodSolEva(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			lista = (List<EvolucionMarcadorRequest>) out.get("AC_LISTA");
			outResponse.setResponse(lista);

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			logger.info("Error al listar : ", e);
		}
		return outResponse;
	}

	@Override
	public ApiOutResponse actualizarMonitoreoPendInfo(EvolucionRequest request) {
		Map<String, Object> out = new HashMap<>();

		ApiOutResponse response = new ApiOutResponse();
		EvolucionRequest res = request;

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("sp_pfc_u_estado_monitoreo");

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_MONITOREO", request.getCodMonitoreo(), Types.NUMERIC);
			in.addValue("PN_P_ESTADO_MONITOREO", request.getpEstadoMonitoreo(), Types.NUMERIC);

			// INSERTAR MAS PARAMETROS

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			response.setResponse(res);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return response;
	}

	@Override
	public ApiOutResponse regSeguimientoEjecutivo(SegEjecutivoRequest request) {
		Map<String, Object> out = new HashMap<>();

		ApiOutResponse response = new ApiOutResponse();
		SegEjecutivoRequest res = request;

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("sp_pfc_i_seg_ejecutivo");

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_MONITOREO", request.getCodMonitoreo(), Types.NUMERIC);
			in.addValue("PN_COD_EJECUTIVO_MONITOREO", request.getCodEjecutivoMonitoreo(), Types.NUMERIC);
			in.addValue("PN_NOM_EJECUTIVO_MONITOREO", request.getNomEjecutivoMonitoreo(), Types.VARCHAR);
			in.addValue("PN_P_ESTADO_SEGUIMIENTO", request.getpEstadoSeguimiento(), Types.NUMERIC);
			in.addValue("PV_DETALLE_EVENTO", request.getDetalleEvento(), Types.VARCHAR);
			in.addValue("PN_VISTO_RESP_MONITOREO", request.getVistoRespMonitoreo(), Types.NUMERIC);
			in.addValue("PV_FECHA_REGISTRO", DateUtils.getDateToStringDDMMYYYHHMMSS(new Date()), Types.VARCHAR);
			in.addValue("PV_USUARIO_CREA", request.getUsuariocrea(), Types.VARCHAR);
			in.addValue("PN_P_ESTADO_MONITOREO", request.getpEstadoMonitoreo(), Types.NUMERIC);

			// INSERTAR MAS PARAMETROS
			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			if (res.getpEstadoMonitoreo() == null) {// REGISTRO NUEVO
				res.setCodSegEjecutivo(Long.parseLong(out.get("PN_COD_SEG_EJECUTIVO").toString()));
			}
			response.setResponse(res);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse listarSeguimientoEjecutivo(SegEjecutivoRequest request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<SegEjecutivoResponse> lista = new ArrayList<>();

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_SEG_EJECUTIVO")
					.returningResultSet("AC_LISTA", new SegEjecutivoRowMapper());

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_MONITOREO", request.getCodMonitoreo(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			lista = (List<SegEjecutivoResponse>) out.get("AC_LISTA");
			outResponse.setResponse(lista);

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return outResponse;
	}

	@Override
	public ApiOutResponse getSeguimientosPendientes(SegEjecutivoRequest request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		Map<String,Integer> mapa = new HashMap<>();

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("sp_pfc_s_seg_pendientes");

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_MONITOREO", request.getCodMonitoreo(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			mapa.put("pendientes", Integer.parseInt(out.get("PN_PENDIENTES").toString()));
			mapa.put("seguimiento", Integer.parseInt(out.get("PN_SEGUIMIENTO").toString()));
			outResponse.setResponse(mapa);

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return outResponse;
	}

	@Override
	public ApiOutResponse actEstadoSegPendientes(SegEjecutivoRequest request) {
		Map<String, Object> out = new HashMap<>();

		ApiOutResponse response = new ApiOutResponse();
		SegEjecutivoRequest res = request;

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("sp_pfc_u_seg_ejecutivo");

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_MONITOREO", request.getCodMonitoreo(), Types.NUMERIC);

			// INSERTAR MAS PARAMETROS

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			response.setResponse(res);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Error al listar : ", e);
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse getUltimoRegistroMarcador(EvolucionMarcadorRequest request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<EvolucionMarcadorRequest> lista = new ArrayList<>();

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO).withProcedureName("sp_pfc_s_ult_marcador")
					.returningResultSet("AC_LISTA", new EvolucionMarcadoresRowMapper());

			logger.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_MARCADOR", request.getCodMarcador(), Types.NUMERIC);
			in.addValue("PN_COD_SOL_EVA", request.getCodSolEva(), Types.NUMERIC);
			in.addValue("PN_COD_EVOLUCION", request.getCodEvolucion(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			lista = (List<EvolucionMarcadorRequest>) out.get("AC_LISTA");
			if (lista.size() > 0) {
				outResponse.setResponse(lista.get(0));
				outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
				outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());
			}else {
				outResponse.setCodResultado(1);
				outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());
			}
			
			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			logger.info("Error al listar : ", e);
		}
		return outResponse;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse getUltimoMonitoreo(MonitoreoEvolucionRequest request) {
		// TODO Auto-generated method stub
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<MonitoreoEvolucionResponse> lista = new ArrayList<>();

		try {
			logger.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_ULT_MONITOREO")
					.returningResultSet("AC_LISTAMONITOREO", new MonitoreoEvolucionRowMapper());

			logger.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_LINEA_TRAT", request.getCodLineaTrat(), Types.NUMERIC);
			in.addValue("PV_FECHA", request.getFecha(), Types.VARCHAR);
			in.addValue("PV_COD_GRP_DIAG", request.getCodGrpDiag(), Types.VARCHAR);

			out = simpleJdbcCall.execute(in);
			logger.info("Respuesta del sp");

			lista = (List<MonitoreoEvolucionResponse>) out.get("AC_LISTAMONITOREO");
			outResponse.setResponse(lista);
			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if(lista.size()<=0) {
				outResponse.setCodResultado(-1);
				outResponse.setMsgResultado("No se obtuvo ningun monitoreo");
			}
			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			logger.info("Error al listar : ", e);
		}
		return outResponse;
	}

}
