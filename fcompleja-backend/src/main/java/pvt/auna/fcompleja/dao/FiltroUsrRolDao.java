package pvt.auna.fcompleja.dao;

import pvt.auna.fcompleja.model.api.FiltroUsrRolRequest;
import pvt.auna.fcompleja.model.api.FiltroUsrRolResponse;

public interface FiltroUsrRolDao {

	FiltroUsrRolResponse FiltroUsuarioRol(FiltroUsrRolRequest ListaUsuario) throws Exception;
}
