package pvt.auna.fcompleja.dao;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.SolicitudesPreeliminarFiltroRequest;

public interface BandejaSolicitudPreliminarDao {

	ApiOutResponse listarSolicitudes(SolicitudesPreeliminarFiltroRequest objA_preeliminar);

}
