package pvt.auna.fcompleja.dao;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.FiltroParametroRequest;
import pvt.auna.fcompleja.model.api.FiltroParametroResponse;

public interface FiltroParametroDao {

	public FiltroParametroResponse FiltroParametro(FiltroParametroRequest request) throws Exception;
	public FiltroParametroResponse ControlFilaParametro(FiltroParametroRequest request) throws Exception;
	public ApiOutResponse ConsultaParametroValue(FiltroParametroRequest request) throws Exception;
	
}
