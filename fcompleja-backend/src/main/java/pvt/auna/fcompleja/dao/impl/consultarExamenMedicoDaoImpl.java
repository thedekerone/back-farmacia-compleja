package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pvt.auna.fcompleja.dao.consultarExamenMedicoDao;
import pvt.auna.fcompleja.model.api.request.ListaExamenMedicoRequest;
import pvt.auna.fcompleja.model.api.response.ListExamenesResponse;
import pvt.auna.fcompleja.model.api.response.ListaExamenMedicoResponse;
import pvt.auna.fcompleja.model.mapper.consultarExamenMedicoRowMapper;
import pvt.auna.fcompleja.util.ConstanteUtil;

@Repository
public class consultarExamenMedicoDaoImpl implements consultarExamenMedicoDao {

	private final Logger LOGGER = Logger.getLogger(getClass());
	
	@Autowired
	DataSource dataSource;
	
	@SuppressWarnings("unchecked")
	@Override
	public ListaExamenMedicoResponse consultarExamenMedico(ListaExamenMedicoRequest listaExamenMed) throws Exception {
		// TODO Auto-generated method stub
		ListaExamenMedicoResponse ListaExamenMedicoResponse = new ListaExamenMedicoResponse();
		List<ListExamenesResponse> ListExamenesResponse =  new ArrayList<>();
		Map<String ,Object> out = new HashMap<>();
		
		try {
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA).withCatalogName(ConstanteUtil.PAQUETE_MAESTRO)
					.withProcedureName("SP_PFC_S_EXAMENMEDICO")
					.returningResultSet("AC_LISTA_EXAMENES", new consultarExamenMedicoRowMapper());
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_DESCRIPCION", Types.VARCHAR));
			in.addValue("PV_DESCRIPCION",listaExamenMed.getDescripcion());
			
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("AC_LISTA_EXAMENES", OracleTypes.CURSOR,
					new BeanPropertyRowMapper<ListaExamenMedicoResponse>(ListaExamenMedicoResponse.class)));
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PN_COD_RESULTADO", Types.NUMERIC));
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PV_MSG_RESULTADO", Types.VARCHAR));
			
			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			ListExamenesResponse 	= (List<ListExamenesResponse>)out.get("AC_LISTA_EXAMENES");
			
			ListaExamenMedicoResponse.setListaExamenes(ListExamenesResponse);

			LOGGER.info(out.get("PN_COD_RESULTADO"));
			 LOGGER.info(out.get("PV_MSG_RESULTADO"));
			 
			 if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1){
	                throw new Exception(out.get("PV_MSG_RESULTADO").toString());
	            }

			 LOGGER.info(ListExamenesResponse.size());
			 LOGGER.info("TRAE DATA " + ListExamenesResponse.size());
			
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("Error al consultar ---> listar los Examenes Medico", e);
		}
		
		return ListaExamenMedicoResponse;
	}

}
