package pvt.auna.fcompleja.dao.impl;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

//import org.apache.tomcat.util.bcel.classfile.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.dao.AccesoAsignadoDao;
import pvt.auna.fcompleja.model.api.request.seguridad.UsuarioRequest;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;

@Repository
public class AccesoAsignadoDaoImpl implements AccesoAsignadoDao {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private AseguramientoPropiedades propiedades;

	private static final Logger log = LoggerFactory.getLogger(AccesoAsignadoDaoImpl.class);

	@Autowired
	DataSource dataSource;

	@Override
	public OncoWsResponse consultarMenu(UsuarioRequest request, HttpHeaders headers) throws Exception {

		OncoWsResponse response = new OncoWsResponse();
		String UrlVchar = propiedades.getAseguramiento() + "/seguridad/api/menu";

		try {
			HttpEntity<UsuarioRequest> rqtUsuario = new HttpEntity<UsuarioRequest>(request, headers);
			response = restTemplate.postForObject(UrlVchar, rqtUsuario, OncoWsResponse.class);

		} catch (Exception e) {
			log.error("ERROR SySTEMA" + e.getMessage());
			log.error("Error No muestra Menu");
		}

		return response;
	}

	@Override
	public OncoWsResponse consultarUsuarioRol(UsuarioRequest request) throws Exception {

		OncoWsResponse response = new OncoWsResponse();

		String UrlVchar = propiedades.getAseguramiento() + "/farmacia-compleja/api/persona/rol";
		// Post Data
		Map<String, Object> params = new HashMap<String, Object>();

		System.out.println("codUsuario" + request.getCodUsuario());
		params.put("codUsuario", request.getCodUsuario());

		try {

			response = restTemplate.postForObject(UrlVchar, params, OncoWsResponse.class);

		} catch (Exception e) {
			log.error("ERROR SySTEMA" + e.getMessage());
			log.error("Error No muestra Opcion");
		}

		return response;
	}

	@Override
	public OncoWsResponse consultarUsuario(UsuarioRequest request) throws Exception {

		OncoWsResponse response = new OncoWsResponse();

		String UrlVchar = propiedades.getAseguramiento() + "/farmacia-compleja/api/usuario";
		
		try {
			response = restTemplate.postForObject(UrlVchar, request, OncoWsResponse.class);
			
		} catch (Exception e) {
			log.error("ERROR SySTEMA" , e.getMessage());
			log.error("Error No muestra Opcion");
		}

		return response;
	}

    @Override 
    public OncoWsResponse consultarUsuariosPorRol(UsuarioRequest request, HttpHeaders headers) throws Exception { 
        OncoWsResponse response = new OncoWsResponse(); 
 
        String UrlVchar = propiedades.getAseguramiento() + "/seguridad/usuario/listarPorRol"; 
        HttpEntity<UsuarioRequest> rqPcte = new HttpEntity<UsuarioRequest>(request, headers); 
 
        try { 
            response = restTemplate.postForObject(UrlVchar, rqPcte, OncoWsResponse.class); 
 
        } catch (Exception e) { 
            log.error("ERROR SySTEMA" + e.getMessage()); 
            log.error("Error No muestra Opcion"); 
        } 
 
        return response; 
    }
    
    @Override 
    public OncoWsResponse obtenerPorUsuarioId(UsuarioRequest request, HttpHeaders headers) { 
        OncoWsResponse response = new OncoWsResponse(); 
 
        String UrlVchar = propiedades.getAseguramiento() + "/seguridad/usuario/buscar"; 
        HttpEntity<UsuarioRequest> rqPcte = new HttpEntity<UsuarioRequest>(request, headers); 
 
        try { 
            response = restTemplate.postForObject(UrlVchar, rqPcte, OncoWsResponse.class); 
 
        } catch (Exception e) { 
            log.error("ERROR SySTEMA" + e.getMessage()); 
            log.error("Error No muestra Opcion"); 
        } 
 
        return response; 
    }
}
