package pvt.auna.fcompleja.dao.impl;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.dao.DetalleSolicitudPreliminarDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ApiResponse;
import pvt.auna.fcompleja.model.bean.InfoSolbenBean;
import pvt.auna.fcompleja.model.mapper.preliminar.DetallePreliminarRowMapper;
import pvt.auna.fcompleja.model.api.request.InformacionScgPreRequest;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Repository
public class DetalleSolicitudPreliminarDaoImpl implements DetalleSolicitudPreliminarDao {

	private static final org.slf4j.Logger log = LoggerFactory.getLogger(DetalleSolicitudPreliminarDaoImpl.class);

	@Qualifier("dataSource")
	@Autowired
	private DataSource dataSource;

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse consultarInformacionScgPre(InformacionScgPreRequest request) {

		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();

		try {
			/** Call store procedure */
			log.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_PRELIMINAR).withProcedureName("SP_PFC_S_DET_PRELIMINAR_SCG")
					.returningResultSet("AC_INFOSOLBEN", new DetallePreliminarRowMapper());

			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();
			
			in.addValue("PN_COD_SOL_PRE", request.getCodSol(), Types.NUMERIC);
			out = simpleJdbcCall.execute(in);

			
			ArrayList<InfoSolbenBean> lista = (ArrayList<InfoSolbenBean>) out.get("AC_INFOSOLBEN");

			if (lista != null && lista.size() == 1) {
				outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
				outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());
				outResponse.setResponse((InfoSolbenBean) lista.get(0));
			} else {
				log.warn(this.getClass().getName() + ".consultarInformacionScgPre", "No existen datos");
				outResponse.setCodResultado(10);
				outResponse.setMsgResultado("No existen datos");
				outResponse.setResponse(null);
			}

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error(this.getClass().getName() + ".consultarInformacionScgPre", e);
			outResponse.setCodResultado(99);
			outResponse.setMsgResultado(e.getMessage());
			outResponse.setResponse(null);
		}
		return outResponse;
	}

	@Override
	public ApiResponse updateEstadoSolicitudPreliminar(InformacionScgPreRequest request) {

		Map<String, Object> out = new HashMap<>();

		ApiResponse response = new ApiResponse();

		try {
			/** Call store procedure */
			log.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_PRELIMINAR).withProcedureName("SP_PFC_U_DETALLE_PRELIMINAR");

			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_SOL_PRE", request.getCodSol(), Types.NUMERIC);
			in.addValue("PN_P_ESTADO_SOL_PRE", request.getEstadoSolPre(), Types.NUMERIC);
			in.addValue("PN_COD_MAC", request.getCodMac(), Types.NUMERIC);
			in.addValue("PV_FECHA_ACTUAL", DateUtils.getDateToStringDDMMYYYHHMMSS(new Date()), Types.VARCHAR);
			in.addValue("PN_USUARIO", request.getCodUsuario(), Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);
			String estadoSolicitudPreliminar = (String) out.get("PV_ESTADO_SOL_PRE");
			if (estadoSolicitudPreliminar == null) {
				response.setResponse(null);
			} else {
				response.setResponse(out.get("PV_ESTADO_SOL_PRE").toString());
			}

			response.setStatus(out.get("PN_COD_RESULTADO").toString());
			response.setMessage(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("Error al listar : ", e);
		}

		return response;
	}

	@Override
	public HashMap<String, Object> actualizarEstadoPreliminar(InformacionScgPreRequest request) {

		Map<String, Object> out = new HashMap<>();
		HashMap<String, Object> hshDataMap = new HashMap<>();

		try {
			/** Call store procedure */
			log.info("Inicia llamada al procedimiento [" + ConstanteUtil.SP_ESTADO_PRELIMINAR + "]");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_PRELIMINAR)
					.withProcedureName(ConstanteUtil.SP_ESTADO_PRELIMINAR);
			// ConstanteUtil.SP_ESTADO_PRELIMINAR
			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_SOL_PRE", request.getCodSol(), Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			hshDataMap.put("status", out.get("PN_COD_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			} else if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == 0) {

			}
			hshDataMap.put("message", out.get("PV_MSG_RESULTADO").toString());

		} catch (Exception e) {
			log.error("Error [DetalleSolicitudPreliminarDaoImpl][actualizarEstadoPreliminar] : ", e);
			hshDataMap.put("message", ConstanteUtil.MENSAJE_SIN_RESULTADO);
		}
		return hshDataMap;
	}

	@Override
	public ApiResponse updateEstadoPendInscriptionMAC(InformacionScgPreRequest request) {
		Map<String, Object> out = new HashMap<>();
		ApiResponse response = new ApiResponse();

		try {
			/** Call store procedure */
			log.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_PRELIMINAR).withProcedureName("SP_PFC_U_SOLIC_PRELIMINAR");

			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_SOL_PRE", request.getCodSol(), Types.NUMERIC);
			in.addValue("PN_P_ESTADO_SOL_PRE", request.getEstadoSolPre(), Types.NUMERIC);
			in.addValue("PV_FECHA_ACTUAL", DateUtils.getDateToStringDDMMYYYY(new Date()), Types.VARCHAR);
			in.addValue("PN_USUARIO", request.getCodUsuario(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			response.setStatus(out.get("PN_COD_RESULTADO").toString());
			response.setMessage(out.get("PV_MSG_RESULTADO").toString());
			response.setResponse(request);

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("Error al listar : ", e);
		}

		return response;
	}

}
