/**
 * 
 */
package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.config.ConfigFTPProp;
import pvt.auna.fcompleja.dao.MaestrosDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.bean.CriteriosBean;
import pvt.auna.fcompleja.model.bean.ExamenMedicoBean;
import pvt.auna.fcompleja.model.bean.ExamenMedicoDetalleBean;
import pvt.auna.fcompleja.model.bean.MACBean;
import pvt.auna.fcompleja.model.bean.MarcadoresBean;
import pvt.auna.fcompleja.model.bean.ProductoAsociadoBean;
import pvt.auna.fcompleja.model.mapper.CriteriosBeanRowMapper;
import pvt.auna.fcompleja.model.mapper.ExamenMedicoDetalleRowMapper;
import pvt.auna.fcompleja.model.mapper.ExamenMedicoRowMapper;
import pvt.auna.fcompleja.model.mapper.ListaMarcadoresRowMapper;
import pvt.auna.fcompleja.model.mapper.ProductoAsociadoRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.MarcadorRowMapper;
import pvt.auna.fcompleja.service.GenericoService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;

/**
 * @author Jose.Reyes/MDP
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class MaestrosDaoImpl implements MaestrosDao {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	DataSource dataSource;

	@Autowired
	GenericoService genericoService;

	@Autowired
	ConfigFTPProp configFTPProp;

	@Override
	public ApiOutResponse registroExamenesMedicos(ExamenMedicoBean examenMedicoBean) {
		// TODO Auto-generated method stub
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_I_REG_EXAM_MED_MARCA");

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_EXAMEN_MED", examenMedicoBean.getCodExamenMed(), Types.NUMERIC);
			in.addValue("PV_DESCRIPCION", examenMedicoBean.getDescripcion(), Types.VARCHAR);
			in.addValue("PN_COD_TIPO_EXAMEN", examenMedicoBean.getTipoExamen(), Types.NUMERIC);
			in.addValue("PN_COD_ESTADO", examenMedicoBean.getCodEstado(), Types.NUMERIC);
			in.addValue("PD_FECHA_CREA", examenMedicoBean.getFechaCrea(), Types.DATE);
			in.addValue("PV_USUARIO_CREA", examenMedicoBean.getUsuarioCrea(), Types.VARCHAR);
			in.addValue("PD_FECHA_MODIF", examenMedicoBean.getFechaModif(), Types.DATE);
			in.addValue("PV_USUARIO_MODIF", examenMedicoBean.getUsuarioModif(), Types.VARCHAR);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			examenMedicoBean.setCodExamenMed(Integer.parseInt(out.get("PN_COD_EXAMEN_MEDICO").toString()));

			apiOut.setResponse(examenMedicoBean);
			
			/*Elimina Examen Medico Detalle */
			eliminarExamenMedicoDetalle(examenMedicoBean.getCodExamenMed());

			/* Guardar Detalle Examen Medico */

			guardarExamenesMedicoDetalle(examenMedicoBean);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	private void guardarExamenesMedicoDetalle(ExamenMedicoBean examenMedicoBean) {

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_I_REG_EXAM_MED_DET");

		try {

			for (ExamenMedicoDetalleBean examenDetalle : examenMedicoBean.getlExamenMedicoDetalle()) {

				MapSqlParameterSource in = new MapSqlParameterSource();
				in.addValue("PN_COD_EXAMEN_MED", examenMedicoBean.getCodExamenMed(), Types.NUMERIC);
				in.addValue("PN_COD_EXAMEN_MED_DET", examenDetalle.getCodExamenMedDet(), Types.NUMERIC);
				in.addValue("PN_COD_TIPO_INGRESO_RES", examenDetalle.getCodigoTipoIngresoResultado(), Types.NUMERIC);
				in.addValue("PV_UNIDAD_MEDIDA", examenDetalle.getUnidadMedida(), Types.VARCHAR);
				in.addValue("PV_RANGO", examenDetalle.getRango(), Types.VARCHAR);
				in.addValue("PV_VALOR_FIJO", examenDetalle.getValorFijo(), Types.VARCHAR);
				in.addValue("PN_COD_ESTADO", examenDetalle.getCodigoEstado(), Types.NUMERIC);
				in.addValue("PN_RANGO_MINIMO", examenDetalle.getRangoMinimo(), Types.NUMERIC);
				in.addValue("PN_RANGO_MAXIMO", examenDetalle.getRangoMaximo(), Types.NUMERIC);

				in.addValue("PD_FECHA_CREA", examenMedicoBean.getFechaCrea(), Types.DATE);
				in.addValue("PV_USUARIO_CREA", examenMedicoBean.getUsuarioCrea(), Types.VARCHAR);
				in.addValue("PD_FECHA_MODIF", examenMedicoBean.getFechaModif(), Types.DATE);
				in.addValue("PV_USUARIO_MODIF", examenMedicoBean.getUsuarioModif(), Types.VARCHAR);

				out = simpleJdbcCall.execute(in);

				if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) != 0) {
					LOGGER.error((String) out.get("PV_MSG_RESULTADO"));
					throw new Exception((String) out.get("PV_MSG_RESULTADO"));
				}

				LOGGER.info((String) out.get("PV_MSG_RESULTADO"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());

		}

	}
	
	private void eliminarExamenMedicoDetalle ( Integer codExamenMedico) {
		
		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_D_EXAMEN_MED_DET");

		try {
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_EXAMEN_MED", codExamenMedico, Types.NUMERIC);
			
			out = simpleJdbcCall.execute(in);

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) != 0) {
				LOGGER.error((String) out.get("PV_MSG_RESULTADO"));
				throw new Exception((String) out.get("PV_MSG_RESULTADO"));
			}

			LOGGER.info((String) out.get("PV_MSG_RESULTADO"));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());

		}
		
	}

	@Override
	public ApiOutResponse listarExamenesMedicos(ExamenMedicoBean examenesMedicos) {
		// TODO Auto-generated method stub
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_s_listar_exams_med")
				.returningResultSet("TC_LIST", new ExamenMedicoRowMapper())
				.returningResultSet("TC_LIST_DETALLE", new ExamenMedicoDetalleRowMapper());

		try {
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PV_DESCRIPCION", examenesMedicos.getDescripcion(), Types.VARCHAR);
			
			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			ArrayList<ExamenMedicoBean> examenesMedicosBean = new ArrayList<ExamenMedicoBean>();
			examenesMedicosBean = (ArrayList<ExamenMedicoBean>) out.get("TC_LIST");
			ArrayList<ExamenMedicoDetalleBean> examenesMedicosDetalle = new ArrayList<ExamenMedicoDetalleBean>();
			examenesMedicosDetalle = (ArrayList<ExamenMedicoDetalleBean>) out.get("TC_LIST_DETALLE");

			for (ExamenMedicoBean em : examenesMedicosBean) {
				ArrayList<ExamenMedicoDetalleBean> emDetalle = new ArrayList<ExamenMedicoDetalleBean>();
				for (ExamenMedicoDetalleBean emd : examenesMedicosDetalle) {
					if (em.getCodExamenMed() == emd.getCodExamenMed()) {
						emDetalle.add(emd);
					}
				}
				em.setlExamenMedicoDetalle(emDetalle);
				;
			}

			apiOut.setResponse(examenesMedicosBean);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	@Override
	public ApiOutResponse registroMarcadores(MarcadoresBean marcadores) {
		// TODO Auto-generated method stub

		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_I_REGISTRO_MARCA");

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_CONFIG_MARCA", marcadores.getCodigoConfigMarca(), Types.NUMERIC);
			in.addValue("PV_COD_CONFIG_MARCA_LARGO", marcadores.getCodigoConfigMarcaLargo(), Types.VARCHAR);
			in.addValue("PN_COD_EXAMEN_MED", marcadores.getCodigoExamenMed(), Types.NUMERIC);
			in.addValue("PN_COD_MAC", marcadores.getCodigoMac(), Types.NUMERIC);
			in.addValue("PN_COD_GRP_DIAG", marcadores.getCodigoGrupoDiag(), Types.NUMERIC);
			in.addValue("PN_TIPO_MARCADOR", marcadores.getCodigoTipoMarcador(), Types.NUMERIC);
			in.addValue("PN_PER_MAXIMA", marcadores.getCodigoPerMaxima(), Types.NUMERIC);
			in.addValue("PN_PER_MINIMA", marcadores.getCodigoPerMinima(), Types.NUMERIC);
			in.addValue("PN_COD_ESTADO", marcadores.getCodigoEstado(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			apiOut.setResponse(null);

		
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;

	}

	@Override
	public ApiOutResponse listarMarcaodres(MarcadoresBean marcadores) {
		// TODO Auto-generated method stub
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_s_listar_marcadores")
				.returningResultSet("TC_LIST", new ListaMarcadoresRowMapper());

		try {
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_MAC", marcadores.getCodigoMac(), Types.NUMERIC);
			in.addValue("PN_COD_GRP_DIAG", marcadores.getCodigoGrupoDiag(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			
			apiOut.setResponse((ArrayList<MarcadoresBean>) out.get("TC_LIST"));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	@Override
	public ApiOutResponse registroProductoAsociado(ProductoAsociadoBean productoAsociado) {
		// TODO Auto-generated method stub
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_I_REGISTRO_PROD_ASOC");

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_CODMAC", productoAsociado.getCodigoMac(), Types.NUMERIC);
			in.addValue("PV_COD_PRODUCTO", productoAsociado.getCodigoProducto(), Types.VARCHAR);
			in.addValue("PV_DESCRIPCION", productoAsociado.getDescripcionGenerica(), Types.VARCHAR);
			in.addValue("PV_NOMBRE_COMERCIAL", productoAsociado.getNombreComercial(), Types.VARCHAR);
			in.addValue("PN_COD_ESTADO", productoAsociado.getCodigoEstado(), Types.NUMERIC);
			in.addValue("PN_COD_LABORATORIO", productoAsociado.getCodigoLaboratorio(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			apiOut.setResponse(null);

		
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

	@Override
	public ApiOutResponse listarProductoAsociado(ProductoAsociadoBean productoAsociado) {
		// TODO Auto-generated method stub
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("sp_pfc_s_listar_prod_asoc")
				.returningResultSet("TC_LIST", new ProductoAsociadoRowMapper());

		try {
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_CODMAC", productoAsociado.getCodigoMac(), Types.NUMERIC);
			

			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			
			apiOut.setResponse((ArrayList<ProductoAsociadoBean>) out.get("TC_LIST"));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}

}
