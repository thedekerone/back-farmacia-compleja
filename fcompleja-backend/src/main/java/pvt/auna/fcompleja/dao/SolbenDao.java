package pvt.auna.fcompleja.dao;

import pvt.auna.fcompleja.model.api.request.SolbenWsRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudPreliminarRequest;
import pvt.auna.fcompleja.model.api.response.SolbenWsResponse;
import pvt.auna.fcompleja.model.api.response.SolicitudPreliminarResponse;

public interface SolbenDao {
	SolicitudPreliminarResponse getScgSolbenAndInsertSolPreliminar(SolicitudPreliminarRequest request);
	public SolbenWsResponse actualizarSolben(SolbenWsRequest request);
}
