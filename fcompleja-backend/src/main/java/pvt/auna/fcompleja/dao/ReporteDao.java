package pvt.auna.fcompleja.dao;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ExcelDownloadResponse;
import pvt.auna.fcompleja.model.api.request.ReporteIndicadoresRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.response.AnalisiConclusionResponse;
import pvt.auna.fcompleja.model.api.response.DetalleEvaluacionPdfResponse;
import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorGrupoMacResponse;
import pvt.auna.fcompleja.model.api.response.ReporteIndicadorResponse;
import pvt.auna.fcompleja.model.api.response.evaluacion.LinTratamientoReportResponse;

public interface ReporteDao {

	public DetalleEvaluacionPdfResponse detSolEva(SolicitudEvaluacionRequest request) throws Exception;

	public LinTratamientoReportResponse lineaTrat(SolicitudEvaluacionRequest request) throws Exception;
    public ApiOutResponse generarIndicadores(ReporteIndicadoresRequest request)throws Exception;
    public ApiOutResponse generarIndicadoresExcel(ReporteIndicadoresRequest requestRequest, HttpServletRequest request)throws Exception;
	public AnalisiConclusionResponse analisisConclusion(SolicitudEvaluacionRequest request) throws Exception;
	public AnalisiConclusionResponse medicoAuditor(SolicitudEvaluacionRequest request) throws Exception;
	public ApiOutResponse generarIndicadoresConsumoPorMacExcel(ReporteIndicadoresRequest requestRequest)throws Exception;
	public List<IndicadorConsumoPorGrupoMacResponse> generarIndicadoresConsumoPorGrupoMacExcel(ReporteIndicadoresRequest requestRequest)throws Exception;
	public ApiOutResponse generarIndicadoresConsumoPorGrupoMacLineaExcel(ReporteIndicadoresRequest requestRequest)throws Exception;
	public ApiOutResponse generarIndicadoresConsumoPorGrupoMacLineaTiempoExcel(ReporteIndicadoresRequest requestRequest)throws Exception;
	public ApiOutResponse generarReporteSolicitudesAutorizaciones(ReporteIndicadoresRequest requestRequest)throws Exception;
	public ApiOutResponse generarReporteGenerales(ReporteIndicadoresRequest requestRequest)throws Exception;
	public ApiOutResponse generarReporteSolicitudesMonitoreo(ReporteIndicadoresRequest requestRequest)throws Exception;
	public ApiOutResponse generarReporteGeneralesMonitoresDatos(ReporteIndicadoresRequest requestRequest)throws Exception;
	
	// public List<IndicadorConsumoPorGrupoMacResponse> listaIndicadoresConsumoPorGrupoMac (Integer fecMes , Integer fecAno);
}
