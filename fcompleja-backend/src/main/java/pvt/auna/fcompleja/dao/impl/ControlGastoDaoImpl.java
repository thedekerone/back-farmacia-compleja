package pvt.auna.fcompleja.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.config.ConfigFTPProp;
import pvt.auna.fcompleja.dao.ControlGastoDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ConsumoMedicamento;
import pvt.auna.fcompleja.model.api.ExisteArchivoResponse;
import pvt.auna.fcompleja.model.bean.ConsumoBean;
import pvt.auna.fcompleja.model.bean.FiltroHistorialCargaBean;
import pvt.auna.fcompleja.model.bean.GastoConsumoBean;
import pvt.auna.fcompleja.model.bean.HistorialCargaGastosBean;
import pvt.auna.fcompleja.model.mapper.HistorialCargaGastoRowMapper;
import pvt.auna.fcompleja.service.GenericoService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

@Repository
public class ControlGastoDaoImpl implements ControlGastoDao {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	DataSource dataSource;
	
	@Autowired
	GenericoService genericoService;
	
	@Autowired 
	ConfigFTPProp configFTPProp;

   @Override
   public ApiOutResponse registrarConsumos(List<GastoConsumoBean> consumoMedicamentos) {
      return null;
   }
    
	@Override
	public ApiOutResponse listArchivoCargadoGastos(FiltroHistorialCargaBean filtro) {
		ApiOutResponse apiOut = new ApiOutResponse();
		ArrayList<HistorialCargaGastosBean> lista = new ArrayList<>();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CARGA_CONTROL_GASTO").withProcedureName("SP_PFC_S_HIST_CONTROL_GASTO_PG")
				.returningResultSet("TC_CARGA_ARCHIVO", new HistorialCargaGastoRowMapper());

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PV_FECHA_INICIO", filtro.getFechaInicio(), Types.DATE);
			in.addValue("PV_FECHA_FIN", filtro.getFechaFinal(), Types.DATE);
			in.addValue("PN_INDEX", filtro.getIndex(), Types.NUMERIC);
			in.addValue("PN_LONGITUD", filtro.getSize(), Types.NUMERIC);			
			
			out = simpleJdbcCall.execute(in);
			
			lista = (ArrayList<HistorialCargaGastosBean>) out.get("TC_CARGA_ARCHIVO");
			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));
			apiOut.setResponse(lista);

			if (apiOut.getCodResultado() == -1) {
				throw new Exception(apiOut.getMsgResultado());
			}

		} catch (Exception e) {
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
			LOGGER.error(e.getMessage());
			e.printStackTrace();
		}

		return apiOut;
	}

	@Override
	public ApiOutResponse InsertaRegistroGastosCab(HistorialCargaGastosBean objRegistro) {

		ApiOutResponse apiOut = new ApiOutResponse();
		

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CARGA_CONTROL_GASTO").withProcedureName("SP_PFC_I_CONTROL_GASTO_CAB");

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();
			
						in.addValue("PD_FECHA_HORA_CARGA", objRegistro.getFechaCarga(), Types.DATE);           		//IN   DATE   , 
						in.addValue("PN_RESPONSABLE_CARGA", objRegistro.getResponsableCarga(), Types.NUMERIC);          //IN   NUMBER , 
						in.addValue("PN_P_ESTADO_CARGA", objRegistro.getEstadoCarga(), Types.NUMERIC);           	//IN   NUMBER , 
						in.addValue("PN_REGISTRO_TOTAL", objRegistro.getRegistroTotal(), Types.NUMERIC);           	//IN   NUMBER , 
						in.addValue("PN_REGISTRO_CARGADO", objRegistro.getRegistroCargado(), Types.NUMERIC);           //IN   NUMBER , 
						in.addValue("PN_REGISTRO_ERROR", objRegistro.getRegistroError(), Types.NUMERIC);           	//IN   NUMBER , 
						in.addValue("PN_VER_LOG", objRegistro.getVerLog(), Types.NUMERIC);           					//IN   NUMBER , 
						in.addValue("PN_COD_ARCHIVO_LOG", objRegistro.getCodArchivoLog(), Types.NUMERIC);           	//IN   NUMBER , 
						in.addValue("PN_DESCARGA", objRegistro.getDescarga(), Types.NUMERIC);           				//IN   NUMBER , 
						in.addValue("PN_COD_ARCHIVO_DESCARGA", objRegistro.getCodArchivoDescarga(), Types.NUMERIC);       //IN   NUMBER ,
						//----------------------------------------------------------------------------------------------------------
						in.addValue("PD_FECHA_CREACION", DateUtils.getDateToStringDDMMYYYHHMMSS(objRegistro.getFechaCreacion()),Types.VARCHAR);
						in.addValue("PN_USUARIO_CREACION", objRegistro.getUsuarioCreacion(), Types.NUMERIC);   //IN NUMBER,  
						in.addValue("PD_FECHA_MODIFICACION", DateUtils.getDateToStringDDMMYYYHHMMSS(objRegistro.getFechaModificacion()),Types.VARCHAR);

						in.addValue("PN_USUARIO_MODIFICACION", objRegistro.getUsuarioModificacion(), Types.NUMERIC);   //IN NUMBER,                         
			
			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));
			objRegistro.setCodigoControlGasto(Integer.parseInt(out.get("PN_COD_HIST_CONTROL_GASTO").toString()));
			objRegistro.setCodArchivoDescarga(Integer.parseInt(out.get("PN_COD_HIST_CONTROL_GASTO").toString()));
			apiOut.setResponse(objRegistro);
			
			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}
	
	@Override                                                                                                                                     
	public ApiOutResponse InsertaRegistroGastosDet(GastoConsumoBean objRegistro) {                                                        
                                                                                                                                                
		ApiOutResponse apiOut = new ApiOutResponse();                                                                                               
		GastoConsumoBean mac = new GastoConsumoBean();                                                                              
                                                                                                                                                
		Map<String, Object> out = new HashMap<>();
                                                                                                                                                
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)                                        
				.withCatalogName("PCK_PFC_CARGA_CONTROL_GASTO").withProcedureName("SP_PFC_I_CONSUMOS");
                                                                                                                                                
		try {                                                                                                                                       
			MapSqlParameterSource in = new MapSqlParameterSource();                                                                                   
			                                                                 
			in.addValue("PD_FECHA_HORA_CARGA", objRegistro.getFechaCarga(), Types.DATE);           		//IN   DATE   , 
			in.addValue("PN_RESPONSABLE_CARGA", objRegistro.getResponsableCarga(), Types.NUMERIC);          //IN   NUMBER , 
			in.addValue("PN_P_ESTADO_CARGA", objRegistro.getEstadoCarga(), Types.NUMERIC);           	//IN   NUMBER , 
			in.addValue("PN_REGISTRO_TOTAL", objRegistro.getRegistroTotal(), Types.NUMERIC);           	//IN   NUMBER , 
			in.addValue("PN_REGISTRO_CARGADO", objRegistro.getRegistroCargado(), Types.NUMERIC);           //IN   NUMBER , 
			in.addValue("PN_REGISTRO_ERROR", objRegistro.getRegistroError(), Types.NUMERIC);           	//IN   NUMBER , 
			in.addValue("PN_VER_LOG", objRegistro.getVerLog(), Types.NUMERIC);           					//IN   NUMBER , 
			in.addValue("PN_COD_ARCHIVO_LOG", objRegistro.getCodArchivoLog(), Types.NUMERIC);           	//IN   NUMBER , 
			in.addValue("PN_DESCARGA", objRegistro.getDescarga(), Types.NUMERIC);           				//IN   NUMBER , 
			in.addValue("PN_COD_ARCHIVO_DESCARGA", objRegistro.getCodArchivoDescarga(), Types.NUMERIC);       //IN   NUMBER ,
			//----------------------------------------------------------------------------------------------------------			
			
			in.addValue("PN_COD_HIST_CONTROL_GASTO", objRegistro.getCodHistControlGasto(), Types.INTEGER);	  			//IN   NUMBER ,   
			in.addValue("PV_COD_MAC", objRegistro.getCodMac(), Types.VARCHAR);												//IN VARCHAR2 ,   
			in.addValue("PV_TIPO_ENCUENTRO", objRegistro.getTipoEncuentro(), Types.VARCHAR);									//IN VARCHAR2 ,   
			in.addValue("PN_ENCUENTRO", objRegistro.getEncuentro(), Types.INTEGER);											//IN NUMBER   ,   
			in.addValue("PV_CODIGO_SAP", objRegistro.getCodigoSap(), Types.VARCHAR);											//IN VARCHAR2 ,   
			in.addValue("PV_DESCRIP_ACTIVO_MOLECULA", objRegistro.getDescripActivoMolecula(), Types.VARCHAR);				//IN VARCHAR2 ,   
			in.addValue("PV_DESCRIP_PRESENT_GENERICO", objRegistro.getDescripPresentGenerico(), Types.VARCHAR);				//IN VARCHAR2 ,   
			in.addValue("PV_PRESTACION", objRegistro.getPrestacion(), Types.VARCHAR);											//IN VARCHAR2 ,   
			in.addValue("PV_COD_AFILIADO", objRegistro.getCodAfiliado(), Types.VARCHAR);										//IN VARCHAR2 ,   
			in.addValue("PV_COD_PACIENTE", objRegistro.getCodPaciente(), Types.VARCHAR);										//IN VARCHAR2 ,   
			in.addValue("PV_NOMBRE_PACIENTE", objRegistro.getNombrePaciente(), Types.VARCHAR);								//IN VARCHAR2 ,   
			in.addValue("PV_CLINICA", objRegistro.getClinica(), Types.VARCHAR);												//IN VARCHAR2 ,   
			in.addValue("PD_FECHA_CONSUMO", objRegistro.getFechaConsumo(), Types.DATE);									//IN DATE     ,   
			in.addValue("PV_DESCRIP_GRP_DIAG", objRegistro.getDescripGrpDiag(), Types.VARCHAR);								//IN VARCHAR2 ,   
			in.addValue("PV_COD_DIAGNOSTICO", objRegistro.getCodDiagnostico(), Types.VARCHAR);								//IN VARCHAR2 ,   
			in.addValue("PV_DESCRIP_DIAGNOSTICO", objRegistro.getDescripDiagnostico(), Types.VARCHAR);						//IN VARCHAR2 ,   
			in.addValue("PV_LINEA_TRATAMIENTO", objRegistro.getLineaTratamiento(), Types.VARCHAR);							//IN NUMBER   ,   
			in.addValue("PV_MEDICO_TRATANTE", objRegistro.getMedicoTratante(), Types.VARCHAR);								//IN VARCHAR2 ,   
			in.addValue("PV_PLAN", objRegistro.getPlan(), Types.VARCHAR);														//IN VARCHAR2 ,   
			in.addValue("PN_CANTIDAD_CONSUMO", objRegistro.getCantidadConsumo(), Types.INTEGER);								//IN NUMBER   ,   
			in.addValue("PN_MONTO_CONSUMO", objRegistro.getMontoConsumo(), Types.DOUBLE);									//IN NUMBER   ,   
//------------------------------------------------------------------------------------------------
			in.addValue("PD_FECHA_CREACION", objRegistro.getFechaCreacion(), Types.VARCHAR);    							//IN DATE
			in.addValue("PN_USUARIO_CREACION", objRegistro.getUsuarioCreacion(), Types.VARCHAR);    						//IN NUMBER,      
			in.addValue("PD_FECHA_MODIFICACION", objRegistro.getFechaModificacion(), Types.VARCHAR);    					//IN DATE,        
			in.addValue("PN_USUARIO_MODIFICACION", objRegistro.getUsuarioModificacion(), Types.VARCHAR);    				//IN NUMBER,      			                                                                                                                                          
			
			out = simpleJdbcCall.execute(in);                                                                                                         
                                                                                                                                                                                                                                                                            
			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));                                                         
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));    
			objRegistro.setCodConsumoMedicamento(Integer.parseInt(out.get("PN_COD_CONSUMO_MEDICAMENTO").toString()));
			apiOut.setResponse(objRegistro);

                                                                                                                                                
			if (apiOut.getCodResultado() != 0) {                                                                                                      
				LOGGER.error(apiOut.getMsgResultado());                                                                                                 
				throw new Exception(apiOut.getMsgResultado());                                                                                          
			}                                                                                                                                                                                                                                                                                         
                                                                                                                                                
		} catch (Exception e) {                                                                                                                     
			e.printStackTrace();                                                                                                                      
			LOGGER.error(e.getMessage());                                                                                                             
			apiOut.setCodResultado(99);                                                                                                               
			apiOut.setMsgResultado(e.getMessage());                                                                                                   
			apiOut.setResponse(null);                                                                                                                 
		}                                                                                                                                           
                                                                                                                                                
		return apiOut;                                                                                                                              
	}                                                                                                                                             	
	
	
	
	@Override                                                                                                                                     
	public ApiOutResponse ActualizaRegistroGastosCab(GastoConsumoBean objRegistro) {                                                        
                                                                                                                                                
		ApiOutResponse apiOut = new ApiOutResponse();                                                                                               
		GastoConsumoBean mac = new GastoConsumoBean();                                                                              
                                                                                                                                                
		Map<String, Object> out = new HashMap<>();
                                                                                                                                                
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)                                        
				.withCatalogName("PCK_PFC_CARGA_CONTROL_GASTO").withProcedureName("SP_PFC_U_CONTROL_GASTO_CAB");
                                                                                                                                                
		try {                                                                                                                                       
			MapSqlParameterSource in = new MapSqlParameterSource();                                                                                   
			in.addValue("PN_COD_HIST_CONTROL_GASTO", objRegistro.getCodHistControlGasto(), Types.INTEGER);	  			//IN   NUMBER ,   			                                                                 
			in.addValue("PD_FECHA_HORA_CARGA", objRegistro.getFechaCarga(), Types.DATE);           		//IN   DATE   , 
			in.addValue("PN_RESPONSABLE_CARGA", objRegistro.getResponsableCarga(), Types.NUMERIC);          //IN   NUMBER , 
			in.addValue("PN_P_ESTADO_CARGA", objRegistro.getEstadoCarga(), Types.NUMERIC);           	//IN   NUMBER , 
			in.addValue("PN_REGISTRO_TOTAL", objRegistro.getRegistroTotal(), Types.NUMERIC);           	//IN   NUMBER , 
			in.addValue("PN_REGISTRO_CARGADO", objRegistro.getRegistroCargado(), Types.NUMERIC);           //IN   NUMBER , 
			in.addValue("PN_REGISTRO_ERROR", objRegistro.getRegistroError(), Types.NUMERIC);           	//IN   NUMBER , 
			in.addValue("PN_VER_LOG", objRegistro.getVerLog(), Types.NUMERIC);           					//IN   NUMBER , 
			in.addValue("PN_COD_ARCHIVO_LOG", objRegistro.getCodArchivoLog(), Types.NUMERIC);           	//IN   NUMBER , 
			in.addValue("PN_DESCARGA", objRegistro.getDescarga(), Types.NUMERIC);           				//IN   NUMBER , 
			in.addValue("PN_COD_ARCHIVO_DESCARGA", objRegistro.getCodArchivoDescarga(), Types.NUMERIC);       //IN   NUMBER ,
			//----------------------------------------------------------------------------------------------------------			
			
			in.addValue("PD_FECHA_CREACION", objRegistro.getFechaCreacion(), Types.VARCHAR);    							//IN DATE
			in.addValue("PN_USUARIO_CREACION", objRegistro.getUsuarioCreacion(), Types.VARCHAR);    						//IN NUMBER,      
			in.addValue("PD_FECHA_MODIFICACION", objRegistro.getFechaModificacion(), Types.VARCHAR);    					//IN DATE,        
			in.addValue("PN_USUARIO_MODIFICACION", objRegistro.getUsuarioModificacion(), Types.VARCHAR);    				//IN NUMBER,      			                                                                                                                                          
			
			out = simpleJdbcCall.execute(in);                                                                                                         
                                                                                                                                                                                                                                                                            
			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));                                                         
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));    
			apiOut.setResponse(objRegistro);

                                                                                                                                                
			if (apiOut.getCodResultado() != 0) {                                                                                                      
				LOGGER.error(apiOut.getMsgResultado());                                                                                                 
				throw new Exception(apiOut.getMsgResultado());                                                                                          
			}                                                                                                                                                                                                                                                                                         
                                                                                                                                                
		} catch (Exception e) {                                                                                                                     
			e.printStackTrace();                                                                                                                      
			LOGGER.error(e.getMessage());                                                                                                             
			apiOut.setCodResultado(99);                                                                                                               
			apiOut.setMsgResultado(e.getMessage());                                                                                                   
			apiOut.setResponse(null);                                                                                                                 
		}                                                                                                                                           
                                                                                                                                                
		return apiOut;                                                                                                                              
	}

	@Override
	public ApiOutResponse validarTratamiento(GastoConsumoBean objRegistro) {

		ApiOutResponse apiOut = new ApiOutResponse();
		Map<String, Object> out;

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CARGA_CONTROL_GASTO").withProcedureName("SP_PFC_S_VALIDAR_TRATAMIENTO");

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PV_COD_AFI_PACIENTE", objRegistro.getCodAfiliado(), Types.VARCHAR);
			in.addValue("PV_COD_DIAGNOSTICO", objRegistro.getCodDiagnostico(), Types.VARCHAR);
			in.addValue("PN_COD_MAC", objRegistro.getCodMac(), Types.VARCHAR);
			in.addValue("PN_LINEA_TRA", objRegistro.getLineaTratamiento(), Types.INTEGER);

			out = simpleJdbcCall.execute(in);

			/*apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));*/
			apiOut.setTratamientoExiste(Integer.parseInt(out.get("PN_TOTAL").toString()));

		/*	if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}*/

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			/*apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);*/
		}

		return apiOut;
	}

	@Override
	public void actualizarMonitoreo(ConsumoBean consumoBean) {

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CARGA_CONTROL_GASTO").withProcedureName("SP_PFC_U_ACTUALIZAR_CONSUMO");

		MapSqlParameterSource in = new MapSqlParameterSource();
		in.addValue("PV_COD_AFI_PACIENTE", consumoBean.getCodAfiliado(), Types.VARCHAR);
		in.addValue("PV_COD_DIAGNOSTICO", consumoBean.getCodDiagnostico(), Types.VARCHAR);
		in.addValue("PN_COD_MAC", consumoBean.getCodMac(), Types.VARCHAR);
		in.addValue("PN_CANT_ULTIMO_CONSUMO", consumoBean.getCantidadConsumo(), Types.INTEGER);
		in.addValue("PN_GASTO_ULTIMO_CONSUMO", consumoBean.getMontoConsumo(), Types.DOUBLE);
		in.addValue("PD_FEC_ULTIMO_CONSUMO", consumoBean.getFechaConsumo(), Types.DATE);

		simpleJdbcCall.execute(in);

	}

	@Override
	public ExisteArchivoResponse validarExisteArchivo() {

		ExisteArchivoResponse existeArchivoResponse = new ExisteArchivoResponse();
		Map<String, Object> out;

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName("PCK_PFC_CARGA_CONTROL_GASTO").withProcedureName("SP_PFC_S_VALIDAR_CARGA");

			out = simpleJdbcCall.execute();
			existeArchivoResponse.setExiste(Integer.parseInt(out.get("PN_EXISTE_ARCHIVO").toString()));
			existeArchivoResponse.setMensaje((String) out.get("PV_MENSAJE"));

			LOGGER.error(existeArchivoResponse.getMensaje());

			if (existeArchivoResponse.getExiste() == -1) {
				LOGGER.error(existeArchivoResponse.getMensaje());
				throw new Exception(existeArchivoResponse.getMensaje());
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			existeArchivoResponse.setMensaje(e.getMessage());
		}

		return existeArchivoResponse;

	}

	@Override
	public void actualizarEstado() {

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName("PCK_PFC_CARGA_CONTROL_GASTO").withProcedureName("SP_PFC_U_ACTUALIZAR_ESTADO");

			simpleJdbcCall.execute();

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	}
		
	
}
