package pvt.auna.fcompleja.dao.impl;

import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.util.ResourceUtils;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.config.ConfigFTPProp;
import pvt.auna.fcompleja.dao.ReporteDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ArchivoFTPBean;
import pvt.auna.fcompleja.model.api.ClinicaRequest;
import pvt.auna.fcompleja.model.api.ExcelDownloadResponse;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.ReporteIndicadoresRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.response.AnalisiConclusionResponse;
import pvt.auna.fcompleja.model.api.response.DetalleEvaluacionPdfResponse;
import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorGrupoMacLineaResponse;
import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorGrupoMacLineaTiempoResponse;
import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorGrupoMacResponse;
import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorMacResponse;
import pvt.auna.fcompleja.model.api.response.IndicadoresProcesoResponse;
import pvt.auna.fcompleja.model.api.response.ListSolPreeliminarResponse;
import pvt.auna.fcompleja.model.api.response.ReporteIndicadorResponse;
import pvt.auna.fcompleja.model.api.response.ReporteSolicitudesAutorizacionesResponse;
import pvt.auna.fcompleja.model.api.response.ReporteSolicitudesMonitoreoResponse;
import pvt.auna.fcompleja.model.api.response.evaluacion.LinTrataReportDetalleBean;
import pvt.auna.fcompleja.model.api.response.evaluacion.LinTratamientoReportResponse;
import pvt.auna.fcompleja.model.bean.ClinicaBean;
import pvt.auna.fcompleja.model.bean.ConclusionBean;
import pvt.auna.fcompleja.model.bean.CondicionBasalBean;
import pvt.auna.fcompleja.model.bean.DetalleReportBean;
import pvt.auna.fcompleja.model.bean.DiagnosticoBean;
import pvt.auna.fcompleja.model.bean.GrupoDiagnosticoBean;
import pvt.auna.fcompleja.model.bean.PacienteBean;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.model.mapper.IndicadorConsumoPorGrupoMacLineaRowMapper;
import pvt.auna.fcompleja.model.mapper.IndicadorConsumoPorGrupoMacLineaTiempoRowMapper;
import pvt.auna.fcompleja.model.mapper.IndicadorConsumoPorGrupoMacRowMapper;
import pvt.auna.fcompleja.model.mapper.IndicadorConsumoPorMacRowMapper;
import pvt.auna.fcompleja.model.mapper.IndicadoresProcesoRowMapper;
import pvt.auna.fcompleja.model.mapper.ListadoRowMapper;
import pvt.auna.fcompleja.model.mapper.ReporteSolicitudesAutorizacionesRowMapper;
import pvt.auna.fcompleja.model.mapper.ReporteSolicitudesMonitoreoRowMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.AnalisisConclusionRepRowMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.DescripcionLineaRowMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.DetalleEvaluacionRepoRowMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.LineaMetastasisRowMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.ListaLineaTratamientRowMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.ResultadoBasalMarcadorRowMapper;
import pvt.auna.fcompleja.service.OncoClinicaService;
import pvt.auna.fcompleja.service.OncoDiagnosticoService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@Repository
public class ReporteDaoImpl implements ReporteDao {

	private static final org.slf4j.Logger log = LoggerFactory.getLogger(DetalleSolicitudEvaluacionDaoImpl.class);

	@Qualifier("dataSource")
	@Autowired
	DataSource dataSource;
	
	
	@Autowired ConfigFTPProp configFTPProp;
	
	
	@Autowired
	OncoDiagnosticoService diagnosticoService;
	
	
	@Autowired
	OncoClinicaService clinicaService;
	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	AseguramientoPropiedades portalProp;
	
	@Autowired
	OncoPacienteService pacienteService;
	
	
	
	
	@Override
	public DetalleEvaluacionPdfResponse detSolEva(SolicitudEvaluacionRequest request) throws Exception {
		// ApiResponse response = new ApiResponse();
		Map<String, Object> out = new HashMap<>();
		DetalleEvaluacionPdfResponse response = new DetalleEvaluacionPdfResponse();
		List<DetalleReportBean> DetalleReportBean = new ArrayList<>();

		try {
			/** Call store procedure */
			log.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_EVALUACION)
					.withProcedureName("SP_PFC_S_DET_EVALUACION_REPORT")
                    .returningResultSet("OP_OBJCURSOR", new DetalleEvaluacionRepoRowMapper());

			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_SOL_EVA", request.getCodSolicitudEvaluacion(), Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			DetalleReportBean = (List<DetalleReportBean>) out.get("OP_OBJCURSOR");
			response.setDatosGenerales(DetalleReportBean);
			response.setCodigoResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMensajeResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("Error al listar : ", e);
		}

		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public LinTratamientoReportResponse lineaTrat(SolicitudEvaluacionRequest request) throws Exception {

		Map<String, Object> out = new HashMap<>();
		LinTratamientoReportResponse responseLineaTrat = new LinTratamientoReportResponse();
		List<CondicionBasalBean> condicionBasalBean = new ArrayList<>();
		List<LinTrataReportDetalleBean> lineaTratamientoBean = new ArrayList<>();
		List<LinTrataReportDetalleBean> lineaMetastasisBean = new ArrayList<>();
		List<LinTrataReportDetalleBean> resultadoBasalBean = new ArrayList<>();

		try {
			/** Call store procedure */
			log.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_EVALUACION)
					.withProcedureName("SP_PFC_S_LINEA_TRAT_PDF")
					.returningResultSet("tc_list_lt", new ListaLineaTratamientRowMapper())
					.returningResultSet("tc_list_ms", new DescripcionLineaRowMapper())
					.returningResultSet("tc_list_lm", new LineaMetastasisRowMapper())
					.returningResultSet("tc_list_cb", new ResultadoBasalMarcadorRowMapper());

			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_SOL_EVA", request.getCodSolicitudEvaluacion(), Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			lineaTratamientoBean = (List<LinTrataReportDetalleBean>) out.get("tc_list_lt");
			condicionBasalBean = (List<CondicionBasalBean>) out.get("tc_list_ms");
			lineaMetastasisBean = (List<LinTrataReportDetalleBean>) out.get("tc_list_lm");
			resultadoBasalBean = (List<LinTrataReportDetalleBean>) out.get("tc_list_cb");

			responseLineaTrat.setLineaTratamiento(lineaTratamientoBean);
			responseLineaTrat.setLineaMetastasis(lineaMetastasisBean);
			responseLineaTrat.setResultadoBasal(resultadoBasalBean);
			responseLineaTrat.setCondicionBasalBean(condicionBasalBean);

			responseLineaTrat.setCodigoRespuesta(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			responseLineaTrat.setMensajeRespuesta(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("Error al listar : ", e);
		}

		return responseLineaTrat;
	}

	@Override
	public AnalisiConclusionResponse analisisConclusion(SolicitudEvaluacionRequest request) throws Exception {

		Map<String, Object> out = new HashMap<>();
		AnalisiConclusionResponse response = new AnalisiConclusionResponse();
		List<ConclusionBean> conclusion = new ArrayList<>();
		try {
			/** Call store procedure */
			log.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_EVALUACION)
					.withProcedureName("SP_PFC_S_PDF_ANALISIS_CONCLU")
					.returningResultSet("ac_list", new AnalisisConclusionRepRowMapper());

			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("pn_cod_sol_eva", request.getCodSolicitudEvaluacion(), Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			conclusion = (List<ConclusionBean>) out.get("ac_list");
			response.setConclusion(conclusion);

			response.setCodigoRespuesta(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMensajeRespuesta(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("Error al listar : ", e);
		}

		return response;
	}

	@Override
	public AnalisiConclusionResponse medicoAuditor(SolicitudEvaluacionRequest request) throws Exception {

		Map<String, Object> out = new HashMap<>();
		// ApiResponse response = new ApiResponse();
		AnalisiConclusionResponse responseMedico = new AnalisiConclusionResponse();

		try {
			/** Call store procedure */
			log.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_EVALUACION)
					.withProcedureName("SP_PFC_S_PDF_MEDICO_AUDITOR");

			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_SOL_EVA", request.getCodSolicitudEvaluacion(), Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			responseMedico.setNombreCompleto(out.get("V_NOMBRE_COMPLETO").toString());
			responseMedico.setCodigoRespuesta(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			responseMedico.setMensajeRespuesta(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("Error al listar : ", e);
		}

		return responseMedico;
	}

	@Override
	public ApiOutResponse generarIndicadores(ReporteIndicadoresRequest request) throws Exception {
		// TODO Auto-generated method stub

		ApiOutResponse reporteIndicadorResponse = new ApiOutResponse();

		Calendar cal = Calendar.getInstance();
		String sFechaInicio = "01/" + request.getMes().toString() + "/" + request.getAno();
		Date dFechaInicio = new SimpleDateFormat("dd/MM/yyyy").parse(sFechaInicio);

		cal.setTime(dFechaInicio);
		cal.add(Calendar.MONTH, 1);

		for (int i = 0; i < 12; i++) {
			cal.add(Calendar.MONTH, -1);
			log.info(" Fecha : " + cal.getTime());
			// cal.setTime(cal.getTime());

			log.info("Generando Indicador  = >  MES : " + (cal.get(Calendar.MONTH) + 1) + " | AÑO : "
					+ cal.get(Calendar.YEAR));
			if (!generadoIndicador((cal.get(Calendar.MONTH) + 1), cal.get(Calendar.YEAR))) {

				if (generarIndicador((cal.get(Calendar.MONTH) + 1), cal.get(Calendar.YEAR))) {
					log.info("Generado Indicador  = >  MES : " + (cal.get(Calendar.MONTH) + 1) + " | AÑO : "
							+ cal.get(Calendar.YEAR));
				} else {
					log.info(" [ NO ] Generado Indicador  = >  MES : " + (cal.get(Calendar.MONTH) + 1) + " | AÑO : "
							+ cal.get(Calendar.YEAR));

				}

			} else {

				log.info("Indicador Existe   = >  MES : " + (cal.get(Calendar.MONTH) + 1) + " | AÑO : "
						+ cal.get(Calendar.YEAR));
			}
		}

		reporteIndicadorResponse.setCodResultado(0);
		reporteIndicadorResponse.setMsgResultado("Indicador Generado ");

		log.info(" \t - FIN - [" + "generarIndicador" + "] ");
		return reporteIndicadorResponse;
	}

	private Boolean generadoIndicador(Integer mes, Integer ano) {

		Boolean generado = false;

		Map<String, Object> out = new HashMap<>();

		try {
			/** Call store procedure */
			log.info("Call store procedure : SP_PFC_S_VALIDAR_INDICADORES ");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES)
					.withProcedureName("SP_PFC_S_VALIDAR_INDICADORES");

			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_FEC_MES", mes, Types.NUMERIC);
			in.addValue("PN_FEC_ANO", ano, Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			if (Integer.parseInt(out.get("PN_CAN_REGISTROS").toString()) != 0)
				generado = true;

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("Error al listar : ", e);
		}

		return generado;
	}

	private Boolean generarIndicador(Integer mes, Integer ano) {

		Boolean generado = false;

		Map<String, Object> out = new HashMap<>();

		try {
			/** Call store procedure */
			log.info("Call store procedure : SP_PFC_S_INDICADORES_PROCESO ");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES)
					.withProcedureName("SP_PFC_S_INDICADORES_PROCESO");

			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_FEC_MES", mes, Types.NUMERIC);
			in.addValue("PN_FEC_ANO", ano, Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == 0)
				generado = true;

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("Error al Generar Indicador : ", e);
		}

		return generado;
	}

	@Override
	public ApiOutResponse generarIndicadoresExcel(ReporteIndicadoresRequest requestRequest,HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		List<IndicadoresProcesoResponse> ListIndicadoresProcesoBean = new ArrayList<>();
		ListIndicadoresProcesoBean = listaIndicadoresProceso(requestRequest.getMes(),requestRequest.getAno());
		ApiOutResponse responseBody = new ApiOutResponse();
        /* 
         * Test Generar Archivo en PATH Local 
         * */
	    //String pathFile = "C:\\app\\fcompleja\\indicador" + File.separator + "IndicadoresDeProceso" + ".xlsx";
	    
	    /*  */
	    //String pathFile = ensureDir(request.getRealPath("descargarIndicadorExcel")) + File.separator + "IndicadoresDeProceso" + ".xlsx";
	    String pathFile = configFTPProp.getRutaTemp()+File.separator + "IndicadoresDeProceso" + ".xlsx";
	     
        File file = new File(pathFile);
        Workbook workbook = new XSSFWorkbook();
        //responseBody.setMime("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

        Sheet sheet = workbook.createSheet("Detalle Indicador");
        
        sheet.setDefaultColumnWidth(30);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
        
        /* Header de Años */
        Row headerAnos = sheet.createRow(0);
        
        Calendar cal = Calendar.getInstance();
		String sFechaInicio = "01/" + requestRequest.getMes().toString() + "/" + requestRequest.getAno();
		Date dFechaInicio = new SimpleDateFormat("dd/MM/yyyy").parse(sFechaInicio);

		cal.setTime(dFechaInicio);
		// cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.MONTH, -12);
		
		int cHeader = 3;
		
		int cellAnoActual = 0;
		int cellAnoHasta=0;
		boolean b= false;
		
		cellAnoActual = cal.get(Calendar.YEAR);
		
		for (int i = 0; i < 12; i++) {
			// cal.add(Calendar.MONTH, -1);
			cal.add(Calendar.MONTH, 1);
			cHeader++;
			headerAnos.createCell(cHeader).setCellValue(cal.get(Calendar.YEAR));
			headerAnos.getCell(cHeader).setCellStyle(style);
			sheet.setColumnWidth(cHeader, 2000);
			if ( cellAnoActual != cal.get(Calendar.YEAR) && !b) {
				cellAnoHasta = cHeader;
				b= true;
			}
			
			
		}
        
        
		CellRangeAddress cellMergeAnoIni = new CellRangeAddress(0, 0, 4, cellAnoHasta-1);
		if ( (cellAnoHasta-1 ) != 4 )
	         sheet.addMergedRegion(cellMergeAnoIni);
	    CellRangeAddress cellMergeAnoFin = new CellRangeAddress(0, 0, cellAnoHasta, 15);
	    sheet.addMergedRegion(cellMergeAnoFin);
        
        Row header = sheet.createRow(1);
        header.createCell(0).setCellValue("Código");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Descripción Indicador");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("NUMERADOR");
        header.getCell(2).setCellStyle(style);
		header.createCell(3).setCellValue("DENOMINADOR");
		header.getCell(3).setCellStyle(style);
		
		
		 cal = Calendar.getInstance();
		 sFechaInicio = "01/" + requestRequest.getMes().toString() + "/" + requestRequest.getAno();
		 dFechaInicio = new SimpleDateFormat("dd/MM/yyyy").parse(sFechaInicio);

		cal.setTime(dFechaInicio);
		// cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.MONTH, -12);

		cHeader = 3;
		
		for (int i = 0; i < 12; i++) {
			// cal.add(Calendar.MONTH, -1);
			cal.add(Calendar.MONTH, 1);
			cHeader++;
			header.createCell(cHeader).setCellValue( inicialMesLetra( (cal.get(Calendar.MONTH) + 1) ) );
			header.getCell(cHeader).setCellStyle(style);
			sheet.setColumnWidth(cHeader, 2000);
		}
		
		
		
		

        int rowCount = 2;

        for (IndicadoresProcesoResponse preeliminar : ListIndicadoresProcesoBean) {
            Row userRow = sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(preeliminar.getIndicador());
            userRow.createCell(1).setCellValue(preeliminar.getQuiebre());
            userRow.createCell(2).setCellValue(preeliminar.getNumerador());
            userRow.createCell(3).setCellValue(preeliminar.getDenominador());
            
            
            cHeader = 3;
            for (int i = 0; i < 12; i++) {
            	cHeader++;
            	userRow.createCell(cHeader).setCellValue(0);	
    		}
        }
        
        
        cHeader = 3;
        
        cal.setTime(dFechaInicio);
        // cal.add(Calendar.MONTH, 1);
        cal.add(Calendar.MONTH, -12);
		
		for (int i = 0; i < 12; i++) {
			cal.add(Calendar.MONTH, 1);
			cHeader++;
			ListIndicadoresProcesoBean = listaIndicadoresProceso(cal.get(Calendar.MONTH) + 1,cal.get(Calendar.YEAR));
			
			for ( IndicadoresProcesoResponse lista:  ListIndicadoresProcesoBean) {
				
				
				   for(Row row : sheet) {        
					   
					   if ( row.getRowNum() != 0 ){
			        		
						   if ( row.getCell(0).getStringCellValue().equals(lista.getIndicador()) && 
		        					row.getCell(1).getStringCellValue().equals(lista.getQuiebre())	) {
							   row.getCell(cHeader).setCellValue(lista.getCantidad());
							   break;
						   }
		                		    
			        	}
				   }
				
				
				
			}
        	
			
		}
        
        
        
        
        
      

        try {
        	
        	ByteArrayOutputStream baos = new ByteArrayOutputStream();
        	workbook.write(baos);
        	byte[] xls = baos.toByteArray();
        	String pathFileByte = configFTPProp.getRutaTemp()+File.separator  + "IndicadoresDeProcesoByte" + ".xlsx";
            File fileByte = new File(pathFileByte);
        	 OutputStream   os  = new FileOutputStream(fileByte); 

	         // Starts writing the bytes in it 
	         os.write(xls); 
        	 os.close();
        	 
            FileOutputStream outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            workbook.close();

            responseBody.setResponse(xls);
            responseBody.setCodResultado(0);
            responseBody.setMsgResultado("Indicador en Excel Generado");
            
            //responseBody.setName(file.getName());
            //responseBody.setLength(file.length());
            //responseBody.setUrl(configFTPProp.getRutaTemp()+File.separator  + file.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        //responseBody.setUrl(pathFile);
		
		return responseBody;
	}
	
	
	private String inicialMesLetra ( int mes) {
		
		String mesString;
		switch (mes) {
		        case 1:  mesString = "ENE";
		                 break;
		        case 2:  mesString  = "FEB";
		                 break;
		        case 3:  mesString = "MAR";
		                 break;
		        case 4:  mesString = "ABR";
		                 break;
		        case 5:  mesString = "MAY";
		                 break;
		        case 6:  mesString = "JUN";
		                 break;
		        case 7:  mesString = "JUL";
		                 break;
		        case 8:  mesString = "AGO";
		                 break;
		        case 9:  mesString = "SEP";
		                 break;
		        case 10: mesString = "OCT";
		                 break;
		        case 11: mesString = "NOV";
		                 break;
		        case 12: mesString = "DIC";
		                 break;
		        default: mesString = "NO VALIDO";
		                 break;
		        }
		        
	 return mesString;	        
		
	}
	
	private List<IndicadoresProcesoResponse> listaIndicadoresProceso (Integer fecMes , Integer fecAno) {
		
		List<IndicadoresProcesoResponse> ListIndicadoresProcesoBean = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();

		MapSqlParameterSource in = new MapSqlParameterSource();

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES).withProcedureName("SP_PFC_S_INDICADORES_PROC_XLS")
					.returningResultSet("OP_OBJCURSOR", new IndicadoresProcesoRowMapper());

			in.addValue("PN_FEC_MES", fecMes, Types.NUMERIC);
			in.addValue("PN_FEC_ANO", fecAno, Types.NUMERIC);
			

			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			ListIndicadoresProcesoBean = (List<IndicadoresProcesoResponse>) out.get("OP_OBJCURSOR");

			
			

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			
			log.info((String)out.get("PV_MSG_RESULTADO"));

		} catch (Exception e) {
			outResponse.setCodResultado(99);
			outResponse.setMsgResultado(this.getClass().getName() + ".listaIndicadoresProceso: " + e.getMessage());
			log.error(outResponse.getMsgResultado());
		}
		
		return ListIndicadoresProcesoBean;
	}
	
  private List<IndicadorConsumoPorMacResponse> listaIndicadoresConsumoPorMac (Integer fecMes , Integer fecAno) {
		
		List<IndicadorConsumoPorMacResponse> ListIndicadoresConsumoPorMacBean = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();

		MapSqlParameterSource in = new MapSqlParameterSource();

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES).withProcedureName("SP_PFC_S_INDICADORES_CONS_XLS")
					.returningResultSet("OP_OBJCURSOR", new IndicadorConsumoPorMacRowMapper());

			in.addValue("PN_FEC_MES", fecMes, Types.NUMERIC);
			in.addValue("PN_FEC_ANO", fecAno, Types.NUMERIC);
			in.addValue("PN_TIPO", 1, Types.NUMERIC);
			

			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			ListIndicadoresConsumoPorMacBean = (List<IndicadorConsumoPorMacResponse>) out.get("OP_OBJCURSOR");

			
			

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			
			log.info((String)out.get("PV_MSG_RESULTADO"));

		} catch (Exception e) {
			outResponse.setCodResultado(99);
			outResponse.setMsgResultado(this.getClass().getName() + ".listaIndicadoresProceso: " + e.getMessage());
			log.error(outResponse.getMsgResultado());
		}
		
		return ListIndicadoresConsumoPorMacBean;
	}
  
  
  private List<ReporteSolicitudesAutorizacionesResponse> reporteSolicitudesAutorizaciones (Integer fecMes , Integer fecAno) {
		
		List<ReporteSolicitudesAutorizacionesResponse> reporteSolicitudesAutorizacionesBean = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();

		MapSqlParameterSource in = new MapSqlParameterSource();
		
		log.info("Inicio Metodo :reporteSolicitudesAutorizaciones -- SP SP_PFC_S_REP_SOL_AUTOR:");
		log.info("SP fecMes: "+fecMes);
		log.info("SP fecAno: "+fecAno);

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES).withProcedureName("SP_PFC_S_REP_SOL_AUTOR")
					.returningResultSet("OP_OBJCURSOR", new ReporteSolicitudesAutorizacionesRowMapper());

			in.addValue("PN_FEC_MES", fecMes, Types.NUMERIC);
			in.addValue("PN_FEC_ANO", fecAno, Types.NUMERIC);
			
			

			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			reporteSolicitudesAutorizacionesBean = (List<ReporteSolicitudesAutorizacionesResponse>) out.get("OP_OBJCURSOR");

			

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			
			log.info((String)out.get("PV_MSG_RESULTADO"));
			log.info("PN_COD_RESULTADO: "+out.get("PN_COD_RESULTADO").toString());
			log.info("PV_MSG_RESULTADO: "+out.get("PV_MSG_RESULTADO").toString());

		} catch (Exception e) {
			outResponse.setCodResultado(99);
			outResponse.setMsgResultado(this.getClass().getName() + ".reporteSolicitudesAutorizaciones: " + e.getMessage());
			log.error("Error codigo de resultado: " + outResponse.getCodResultado());
			log.error("Error mensaje: "+outResponse.getMsgResultado());
		}
		
		return reporteSolicitudesAutorizacionesBean;
	}
  
  private List<ReporteSolicitudesMonitoreoResponse> reporteSolicitudesMonitoreo (Integer fecMes , Integer fecAno) {
		
		List<ReporteSolicitudesMonitoreoResponse> reporteSolicitudesMonitoreoBean = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();

		MapSqlParameterSource in = new MapSqlParameterSource();
		
		log.info("Inicio Metodo :reporteSolicitudesMonitoreo -- SP SP_PFC_S_REP_SOL_MONITOR:");
		log.info("SP fecMes: "+fecMes);
		log.info("SP fecAno: "+fecAno);

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES).withProcedureName("SP_PFC_S_REP_SOL_MONITOR")
					.returningResultSet("OP_OBJCURSOR", new ReporteSolicitudesMonitoreoRowMapper());

			in.addValue("PN_FEC_MES", fecMes, Types.NUMERIC);
			in.addValue("PN_FEC_ANO", fecAno, Types.NUMERIC);
			
			

			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			reporteSolicitudesMonitoreoBean = (List<ReporteSolicitudesMonitoreoResponse>) out.get("OP_OBJCURSOR");

			
			

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			
			log.info((String)out.get("PV_MSG_RESULTADO"));
			log.info("PN_COD_RESULTADO: "+out.get("PN_COD_RESULTADO").toString());
			log.info("PV_MSG_RESULTADO: "+out.get("PV_MSG_RESULTADO").toString());

		} catch (Exception e) {
			outResponse.setCodResultado(99);
			outResponse.setMsgResultado(this.getClass().getName() + ".reporteSolicitudesMonitoreo: " + e.getMessage());
			log.error("Error codigo de resultado: " + outResponse.getCodResultado());
			log.error("Error mensaje: "+outResponse.getMsgResultado());
		}
		
		return reporteSolicitudesMonitoreoBean;
	}
  
  private List<IndicadorConsumoPorGrupoMacResponse> listaIndicadoresConsumoPorGrupoMac (Integer fecMes , Integer fecAno) {
		
		List<IndicadorConsumoPorGrupoMacResponse> ListIndicadoresConsumoPorGrupoMacBean = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();

		MapSqlParameterSource in = new MapSqlParameterSource();

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES).withProcedureName("SP_PFC_S_INDICADORES_CONS_XLS")
					.returningResultSet("OP_OBJCURSOR", new IndicadorConsumoPorGrupoMacRowMapper());

			in.addValue("PN_FEC_MES", fecMes, Types.NUMERIC);
			in.addValue("PN_FEC_ANO", fecAno, Types.NUMERIC);
			in.addValue("PN_TIPO", 2, Types.NUMERIC);
			

			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			ListIndicadoresConsumoPorGrupoMacBean = (List<IndicadorConsumoPorGrupoMacResponse>) out.get("OP_OBJCURSOR");

			
			

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			
			log.info((String)out.get("PV_MSG_RESULTADO"));

		} catch (Exception e) {
			outResponse.setCodResultado(99);
			outResponse.setMsgResultado(this.getClass().getName() + ".listaIndicadoresConsumoPorGrupoMac: " + e.getMessage());
			log.error(outResponse.getMsgResultado());
		}
		
		return ListIndicadoresConsumoPorGrupoMacBean;
	}
  
  private List<IndicadorConsumoPorGrupoMacLineaResponse> listaIndicadoresConsumoPorGrupoMacLinea (Integer fecMes , Integer fecAno) {
		
		List<IndicadorConsumoPorGrupoMacLineaResponse> ListIndicadoresConsumoPorGrupoMacLineaBean = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();

		MapSqlParameterSource in = new MapSqlParameterSource();

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES).withProcedureName("SP_PFC_S_INDICADORES_CONS_XLS")
					.returningResultSet("OP_OBJCURSOR", new IndicadorConsumoPorGrupoMacLineaRowMapper());

			in.addValue("PN_FEC_MES", fecMes, Types.NUMERIC);
			in.addValue("PN_FEC_ANO", fecAno, Types.NUMERIC);
			in.addValue("PN_TIPO", 3, Types.NUMERIC);
			

			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			ListIndicadoresConsumoPorGrupoMacLineaBean = (List<IndicadorConsumoPorGrupoMacLineaResponse>) out.get("OP_OBJCURSOR");

			
			

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			
			log.info((String)out.get("PV_MSG_RESULTADO"));

		} catch (Exception e) {
			outResponse.setCodResultado(99);
			outResponse.setMsgResultado(this.getClass().getName() + ".listaIndicadoresConsumoPorGrupoMacLinea: " + e.getMessage());
			log.error(outResponse.getMsgResultado());
		}
		
		return ListIndicadoresConsumoPorGrupoMacLineaBean;
	}
  
  private List<IndicadorConsumoPorGrupoMacLineaTiempoResponse> listaIndicadoresConsumoPorGrupoMacLineaTiempo (Integer fecMes , Integer fecAno) {
		
		List<IndicadorConsumoPorGrupoMacLineaTiempoResponse> ListIndicadoresConsumoPorGrupoMacLineaTiempoBean = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();

		MapSqlParameterSource in = new MapSqlParameterSource();

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES).withProcedureName("SP_PFC_S_INDICADORES_CONS_XLS")
					.returningResultSet("OP_OBJCURSOR", new IndicadorConsumoPorGrupoMacLineaTiempoRowMapper());

			in.addValue("PN_FEC_MES", fecMes, Types.NUMERIC);
			in.addValue("PN_FEC_ANO", fecAno, Types.NUMERIC);
			in.addValue("PN_TIPO", 4, Types.NUMERIC);
			

			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			ListIndicadoresConsumoPorGrupoMacLineaTiempoBean = (List<IndicadorConsumoPorGrupoMacLineaTiempoResponse>) out.get("OP_OBJCURSOR");

			
			

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}
			
			log.info((String)out.get("PV_MSG_RESULTADO"));

		} catch (Exception e) {
			outResponse.setCodResultado(99);
			outResponse.setMsgResultado(this.getClass().getName() + ".listaIndicadoresConsumoPorGrupoMacLineaTiempo: " + e.getMessage());
			log.error(outResponse.getMsgResultado());
		}
		
		return ListIndicadoresConsumoPorGrupoMacLineaTiempoBean;
	}

	public static String ensureDir(String path) {
        File f_out = new File(path);
        if (!f_out.exists()) {
            f_out.mkdirs();
        }
        return f_out.getAbsolutePath();
    }

	@Override
	public ApiOutResponse generarIndicadoresConsumoPorMacExcel(ReporteIndicadoresRequest requestRequest) throws Exception {
		// TODO Auto-generated method stub
		List<IndicadorConsumoPorMacResponse> ListIndicadoresConsumoPorMacBean = new ArrayList<>();
		ListIndicadoresConsumoPorMacBean = listaIndicadoresConsumoPorMac(requestRequest.getMes(),requestRequest.getAno());
		ApiOutResponse responseBody = new ApiOutResponse();
        /* 
         * Test Generar Archivo en PATH Local 
         * */
	    //String pathFile = "C:\\app\\fcompleja\\indicador" + File.separator + "IndicadoresDeProceso" + ".xlsx";
	    
	    /*  */
	    //String pathFile = ensureDir(request.getRealPath("descargarIndicadorExcel")) + File.separator + "IndicadoresDeProceso" + ".xlsx";
	    String pathFile = configFTPProp.getRutaTemp()+File.separator + "IndicadoresDeConsumoPorMac" + ".xlsx";
	     
        File file = new File(pathFile);
        Workbook workbook = new XSSFWorkbook();
        //responseBody.setMime("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

        Sheet sheet = workbook.createSheet("Detalle Indicador");
        
        sheet.setDefaultColumnWidth(30);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        /* Cabecera de Titulos Reporte*/
    	
        CellStyle  styleTitulos = workbook.createCellStyle();
        Font fontTitulos = workbook.createFont();
        fontTitulos.setFontName("Arial");
        styleTitulos.setFillForegroundColor(HSSFColor.WHITE.index);
        styleTitulos.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleTitulos.setAlignment(HorizontalAlignment.CENTER);
        fontTitulos.setBold(true);
        fontTitulos.setColor(HSSFColor.BLACK.index);
        styleTitulos.setFont(fontTitulos);
        
    	Row headerTitulo = sheet.createRow(0);
    	headerTitulo.createCell(2).setCellValue("Indicadores Por Medicamento MAC");
    	headerTitulo.getCell(2).setCellStyle(styleTitulos);
    	String mesEnLetras = mesLetra(  requestRequest.getMes()  );
    	Row headerFiltro = sheet.createRow(1);
    	headerFiltro.createCell(2).setCellValue("MES: " + mesEnLetras + "  AÑO: "+requestRequest.getAno());
    	headerFiltro.getCell(2).setCellStyle(styleTitulos);
    	
    	
		
		
       

        int rowCount = 2;
        
        
        if ( ListIndicadoresConsumoPorMacBean == null || ListIndicadoresConsumoPorMacBean.isEmpty()) {
			/*Imprime Encabezados */
        	/* Cabecera de Grupo*/
        	
        	Row headerMac = sheet.createRow(rowCount);
            headerMac.createCell(0).setCellValue("Medicamento MAC: ");
            headerMac.createCell(1).setCellValue("");
            headerMac.getCell(0).setCellStyle(styleTitulos);
            headerMac.getCell(1).setCellStyle(styleTitulos);
            rowCount = rowCount + 2;
            
            Row header = sheet.createRow(rowCount);
            header.createCell(0).setCellValue("Gasto Total");
            header.getCell(0).setCellStyle(style);
            header.createCell(1).setCellValue("Nro Pacientes");
            header.getCell(1).setCellStyle(style);
            header.createCell(2).setCellValue("G. Paciente");
            header.getCell(2).setCellStyle(style);
    		header.createCell(3).setCellValue("Nro. Nuevos");
    		header.getCell(3).setCellStyle(style);
    		header.createCell(4).setCellValue("Nro. Continuador");
    		header.getCell(4).setCellStyle(style);
			
		}
        

        for (IndicadorConsumoPorMacResponse preeliminar : ListIndicadoresConsumoPorMacBean) {
            
        	/* Cabecera de Grupo*/
        	
        	Row headerMac = sheet.createRow(rowCount);
            headerMac.createCell(0).setCellValue("Medicamento MAC: ");
            headerMac.createCell(1).setCellValue(preeliminar.getDescripcion());
            headerMac.getCell(0).setCellStyle(styleTitulos);
            headerMac.getCell(1).setCellStyle(styleTitulos);
            rowCount = rowCount + 2;
            
            Row header = sheet.createRow(rowCount);
            header.createCell(0).setCellValue("Gasto Total");
            header.getCell(0).setCellStyle(style);
            header.createCell(1).setCellValue("Nro Pacientes");
            header.getCell(1).setCellStyle(style);
            header.createCell(2).setCellValue("G. Paciente");
            header.getCell(2).setCellStyle(style);
    		header.createCell(3).setCellValue("Nro. Nuevos");
    		header.getCell(3).setCellStyle(style);
    		header.createCell(4).setCellValue("Nro. Continuador");
    		header.getCell(4).setCellStyle(style);
        	
    		rowCount = rowCount + 1;
        	Row userRow = sheet.createRow(rowCount);
            userRow.createCell(0).setCellValue(preeliminar.getGastoTotal());
            userRow.createCell(1).setCellValue(preeliminar.getNroPacientes());
            userRow.createCell(2).setCellValue(preeliminar.getGastoPaciente());
            userRow.createCell(3).setCellValue(preeliminar.getNroNuevos());
            userRow.createCell(4).setCellValue(preeliminar.getNroContinuadores());
            
            rowCount++;
            rowCount++;
        }
        
        
     

        try {
        	
        	ByteArrayOutputStream baos = new ByteArrayOutputStream();
        	workbook.write(baos);
        	byte[] xls = baos.toByteArray();
        	String pathFileByte = configFTPProp.getRutaTemp()+File.separator  + "IndicadoresDeConsumoPorMac" + ".xlsx";
            File fileByte = new File(pathFileByte);
        	 OutputStream   os  = new FileOutputStream(fileByte); 

	         // Starts writing the bytes in it 
	         os.write(xls); 
        	 os.close();
        	 
            FileOutputStream outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            workbook.close();

            responseBody.setResponse(xls);
            responseBody.setCodResultado(0);
            responseBody.setMsgResultado("Indicador en Excel Generado");
            
            //responseBody.setName(file.getName());
            //responseBody.setLength(file.length());
            //responseBody.setUrl(configFTPProp.getRutaTemp()+File.separator  + file.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        //responseBody.setUrl(pathFile);
		
		return responseBody;
	}

	
	private String mesLetra ( Integer mes) {
		
		String mesString;
		switch (mes) {
		        case 1:  mesString = "Enero";
		                 break;
		        case 2:  mesString  = "Febrero";
		                 break;
		        case 3:  mesString = "Marzo";
		                 break;
		        case 4:  mesString = "Abril";
		                 break;
		        case 5:  mesString = "Mayo";
		                 break;
		        case 6:  mesString = "Junio";
		                 break;
		        case 7:  mesString = "Julio";
		                 break;
		        case 8:  mesString = "Agosto";
		                 break;
		        case 9:  mesString = "Septiembre";
		                 break;
		        case 10: mesString = "Octubre";
		                 break;
		        case 11: mesString = "Noviembre";
		                 break;
		        case 12: mesString = "Diciembre";
		                 break;
		        default: mesString = "Mes NO Válido";
		                 break;
		        }
		
		
		return mesString;
		
		
	}
	
	private String grupoDiagnosticoLetra ( Integer codGrupo ,ArrayList<GrupoDiagnosticoBean> listaGrupo) {
		
		String descripcion = "";

			for (GrupoDiagnosticoBean gd: listaGrupo ) {
				
				if ( codGrupo.toString().equals(String.valueOf(Integer.parseInt(gd.getCodigo())))) {
					descripcion = gd.getDescripcion();
					break;
				}
			}
		
		
		return descripcion;
		
		
	}

	@Override
	public List<IndicadorConsumoPorGrupoMacResponse> generarIndicadoresConsumoPorGrupoMacExcel(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				List<IndicadorConsumoPorGrupoMacResponse> ListIndicadoresConsumoPorGrupoMacBean = new ArrayList<>();
				ListIndicadoresConsumoPorGrupoMacBean = listaIndicadoresConsumoPorGrupoMac(requestRequest.getMes(),requestRequest.getAno());
				ApiOutResponse responseBody = new ApiOutResponse();
				
				String idTransaccion = GenericUtil.getUniqueID();
				String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
				HttpHeaders headers = new HttpHeaders();
				headers.add("Content-Type", "application/json");
				headers.add("idTransaccion", idTransaccion);
				headers.add("fechaTransaccion", fechaTransaccion);
				
				/* Llamada a Servicio de Roles de Farmacia Compleja */
				ResponseOnc response = new ResponseOnc();
				ArrayList<GrupoDiagnosticoBean> listaGrupoDiagnostico;
				
				response = diagnosticoService.getListarGrupoDiagnostico(headers);
				listaGrupoDiagnostico = (ArrayList<GrupoDiagnosticoBean>) response.getDataList();

			    /*String pathFile = configFTPProp.getRutaTemp()+File.separator + "IndicadoresDeConsumoPorGrupoMac" + ".xlsx";
			     
		        File file = new File(pathFile);
		        Workbook workbook = new XSSFWorkbook();

		        Sheet sheet = workbook.createSheet("Detalle Indicador");
		        
		        sheet.setDefaultColumnWidth(30);

		        CellStyle style = workbook.createCellStyle();
		        Font font = workbook.createFont();
		        font.setFontName("Arial");
		        style.setFillForegroundColor(HSSFColor.BLUE.index);
		        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		        font.setBold(true);
		        font.setColor(HSSFColor.WHITE.index);
		        style.setFont(font);

		        // Cabecera de Titulos Reporte
		    	
		        CellStyle  styleTitulos = workbook.createCellStyle();
		        Font fontTitulos = workbook.createFont();
		        fontTitulos.setFontName("Arial");
		        styleTitulos.setFillForegroundColor(HSSFColor.WHITE.index);
		        styleTitulos.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		        styleTitulos.setAlignment(HorizontalAlignment.CENTER);
		        fontTitulos.setBold(true);
		        fontTitulos.setColor(HSSFColor.BLACK.index);
		        styleTitulos.setFont(fontTitulos);
		        
		    	Row headerTitulo = sheet.createRow(0);
		    	headerTitulo.createCell(2).setCellValue("Indicadores Por Medicamento Grupo Diagnostico y MAC");
		    	headerTitulo.getCell(2).setCellStyle(styleTitulos);
		    	String mesEnLetras = mesLetra(  requestRequest.getMes()  );
		    	Row headerFiltro = sheet.createRow(1);
		    	headerFiltro.createCell(2).setCellValue("MES: " + mesEnLetras + "  AÑO: "+requestRequest.getAno());
		    	headerFiltro.getCell(2).setCellStyle(styleTitulos);
		    	
		    	
				
				
		       

		        int rowCount = 2;
		        
		        if ( ListIndicadoresConsumoPorGrupoMacBean == null || ListIndicadoresConsumoPorGrupoMacBean.isEmpty()) {
//		        	Se Imprimen los Encabezados
//		        	Cabecera de Grupo
		        	
		        	Row headerGrupo = sheet.createRow(rowCount);
		        	headerGrupo.createCell(0).setCellValue("Grupo Diagonostico: ");
		        	headerGrupo.createCell(1).setCellValue("");
		        	headerGrupo.getCell(0).setCellStyle(styleTitulos);
		        	headerGrupo.getCell(1).setCellStyle(styleTitulos);
		            rowCount = rowCount + 1;
		        	
		        	Row headerMac = sheet.createRow(rowCount);
		            headerMac.createCell(0).setCellValue("Medicamento MAC: ");
		            headerMac.createCell(1).setCellValue("");
		            headerMac.getCell(0).setCellStyle(styleTitulos);
		            headerMac.getCell(1).setCellStyle(styleTitulos);
		            rowCount = rowCount + 2;
		            
		            Row header = sheet.createRow(rowCount);
		            header.createCell(0).setCellValue("Gasto Total");
		            header.getCell(0).setCellStyle(style);
		            header.createCell(1).setCellValue("Nro Pacientes");
		            header.getCell(1).setCellStyle(style);
		            header.createCell(2).setCellValue("G. Paciente");
		            header.getCell(2).setCellStyle(style);
		    		header.createCell(3).setCellValue("Nro. Nuevos");
		    		header.getCell(3).setCellStyle(style);
		    		header.createCell(4).setCellValue("Nro. Continuador");
		    		header.getCell(4).setCellStyle(style);
		        	
		        }*/
		        
		        

		        for (IndicadorConsumoPorGrupoMacResponse preeliminar : ListIndicadoresConsumoPorGrupoMacBean) {
		            preeliminar.setDescripcionGrupoDiagnostico(grupoDiagnosticoLetra(preeliminar.getCodigoGrupoDiagnostico(),listaGrupoDiagnostico));
		        	/* 
		        	 // Cabecera de Grupo
		        	
		        	Row headerGrupo = sheet.createRow(rowCount);
		        	headerGrupo.createCell(0).setCellValue("Grupo Diagonostico: ");
		        	headerGrupo.createCell(1).setCellValue(preeliminar.getCodigoGrupoDiagnostico().toString() + "-"+ grupoDiagnosticoLetra(preeliminar.getCodigoGrupoDiagnostico(),listaGrupoDiagnostico));
		        	headerGrupo.getCell(0).setCellStyle(styleTitulos);
		        	headerGrupo.getCell(1).setCellStyle(styleTitulos);
		            rowCount = rowCount + 1;
		        	
		        	Row headerMac = sheet.createRow(rowCount);
		            headerMac.createCell(0).setCellValue("Medicamento MAC: ");
		            headerMac.createCell(1).setCellValue(preeliminar.getDescripcion());
		            headerMac.getCell(0).setCellStyle(styleTitulos);
		            headerMac.getCell(1).setCellStyle(styleTitulos);
		            rowCount = rowCount + 2;
		            
		            Row header = sheet.createRow(rowCount);
		            header.createCell(0).setCellValue("Gasto Total");
		            header.getCell(0).setCellStyle(style);
		            header.createCell(1).setCellValue("Nro Pacientes");
		            header.getCell(1).setCellStyle(style);
		            header.createCell(2).setCellValue("G. Paciente");
		            header.getCell(2).setCellStyle(style);
		    		header.createCell(3).setCellValue("Nro. Nuevos");
		    		header.getCell(3).setCellStyle(style);
		    		header.createCell(4).setCellValue("Nro. Continuador");
		    		header.getCell(4).setCellStyle(style);
		        	
		    		rowCount = rowCount + 1;
		        	Row userRow = sheet.createRow(rowCount);
		            userRow.createCell(0).setCellValue(preeliminar.getGastoTotal());
		            userRow.createCell(1).setCellValue(preeliminar.getNroPacientes());
		            userRow.createCell(2).setCellValue(preeliminar.getGastoPaciente());
		            userRow.createCell(3).setCellValue(preeliminar.getNroNuevos());
		            userRow.createCell(4).setCellValue(preeliminar.getNroContinuadores());
		            
		            rowCount++;
		            rowCount++;
		            */
		        }
		        
		        
		     
/*
		        try {
		        	
		        	ByteArrayOutputStream baos = new ByteArrayOutputStream();
		        	workbook.write(baos);
		        	byte[] xls = baos.toByteArray();
		        	String pathFileByte = configFTPProp.getRutaTemp()+File.separator  + "IndicadoresDeConsumoPorGrupoMac" + ".xlsx";
		            File fileByte = new File(pathFileByte);
		        	 OutputStream   os  = new FileOutputStream(fileByte); 

			         // Starts writing the bytes in it 
			         os.write(xls); 
		        	 os.close();
		        	 
		            FileOutputStream outputStream = new FileOutputStream(file);
		            workbook.write(outputStream);
		            workbook.close();

		            responseBody.setResponse(xls);
		            responseBody.setCodResultado(0);
		            responseBody.setMsgResultado("Indicador en Excel Generado");
		            
		            //responseBody.setName(file.getName());
		            //responseBody.setLength(file.length());
		            //responseBody.setUrl(configFTPProp.getRutaTemp()+File.separator  + file.getName());

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		        
		        //responseBody.setUrl(pathFile);
				*/
		        return ListIndicadoresConsumoPorGrupoMacBean;
	}

	@Override
	public ApiOutResponse generarIndicadoresConsumoPorGrupoMacLineaExcel(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
				// TODO Auto-generated method stub
						List<IndicadorConsumoPorGrupoMacLineaResponse> ListIndicadoresConsumoPorGrupoMacLineaBean = new ArrayList<>();
						ListIndicadoresConsumoPorGrupoMacLineaBean = listaIndicadoresConsumoPorGrupoMacLinea(requestRequest.getMes(),requestRequest.getAno());
						ApiOutResponse responseBody = new ApiOutResponse();
						
						String idTransaccion = GenericUtil.getUniqueID();
						String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
						HttpHeaders headers = new HttpHeaders();
						headers.add("Content-Type", "application/json");
						headers.add("idTransaccion", idTransaccion);
						headers.add("fechaTransaccion", fechaTransaccion);
						
						/* Llamada a Servicio de Roles de Farmacia Compleja */
						ResponseOnc response = new ResponseOnc();
						ArrayList<GrupoDiagnosticoBean> listaGrupoDiagnostico;
						
						response = diagnosticoService.getListarGrupoDiagnostico(headers);
						listaGrupoDiagnostico = (ArrayList<GrupoDiagnosticoBean>) response.getDataList();						
				        /* 
				         * Test Generar Archivo en PATH Local 
				         * */
					    //String pathFile = "C:\\app\\fcompleja\\indicador" + File.separator + "IndicadoresDeProceso" + ".xlsx";
					    
					    /*  */
					    //String pathFile = ensureDir(request.getRealPath("descargarIndicadorExcel")) + File.separator + "IndicadoresDeProceso" + ".xlsx";
					    String pathFile = configFTPProp.getRutaTemp()+File.separator + "IndicadoresDeConsumoPorGrupoMacLinea" + ".xlsx";
					     
				        File file = new File(pathFile);
				        Workbook workbook = new XSSFWorkbook();
				        //responseBody.setMime("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

				        Sheet sheet = workbook.createSheet("Detalle Indicador");
				        
				        sheet.setDefaultColumnWidth(30);

				        CellStyle style = workbook.createCellStyle();
				        Font font = workbook.createFont();
				        font.setFontName("Arial");
				        style.setFillForegroundColor(HSSFColor.BLUE.index);
				        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				        font.setBold(true);
				        font.setColor(HSSFColor.WHITE.index);
				        style.setFont(font);

				        /* Cabecera de Titulos Reporte*/
				    	
				        CellStyle  styleTitulos = workbook.createCellStyle();
				        Font fontTitulos = workbook.createFont();
				        fontTitulos.setFontName("Arial");
				        styleTitulos.setFillForegroundColor(HSSFColor.WHITE.index);
				        styleTitulos.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				        styleTitulos.setAlignment(HorizontalAlignment.CENTER);
				        fontTitulos.setBold(true);
				        fontTitulos.setColor(HSSFColor.BLACK.index);
				        styleTitulos.setFont(fontTitulos);
				        
				    	Row headerTitulo = sheet.createRow(0);
				    	headerTitulo.createCell(2).setCellValue("Indicadores Por Medicamento Grupo Diagnostico, MAC y Linea de Tratamiento");
				    	headerTitulo.getCell(2).setCellStyle(styleTitulos);
				    	String mesEnLetras = mesLetra(  requestRequest.getMes()  );
				    	Row headerFiltro = sheet.createRow(1);
				    	headerFiltro.createCell(2).setCellValue("MES: " + mesEnLetras + "  AÑO: "+requestRequest.getAno());
				    	headerFiltro.getCell(2).setCellStyle(styleTitulos);
				    	
				    	
						
						
				       

				        int rowCount = 2;
				        
				        if (ListIndicadoresConsumoPorGrupoMacLineaBean == null || ListIndicadoresConsumoPorGrupoMacLineaBean.isEmpty()) {
				        	
				        	/*Se Imprimen los Encabezados*/
				        	/* Cabecera de Grupo*/
				        	
				        	Row headerGrupo = sheet.createRow(rowCount);
				        	headerGrupo.createCell(0).setCellValue("Grupo Diagonostico: ");
				        	headerGrupo.createCell(1).setCellValue("");
				        	headerGrupo.getCell(0).setCellStyle(styleTitulos);
				        	headerGrupo.getCell(1).setCellStyle(styleTitulos);
				            rowCount = rowCount + 1;
				        	
				        	Row headerMac = sheet.createRow(rowCount);
				            headerMac.createCell(0).setCellValue("Medicamento MAC: ");
				            headerMac.createCell(1).setCellValue("");
				            headerMac.getCell(0).setCellStyle(styleTitulos);
				            headerMac.getCell(1).setCellStyle(styleTitulos);
				            rowCount = rowCount + 1;
				            
				            Row headerLinea = sheet.createRow(rowCount);
				            headerLinea.createCell(0).setCellValue("Linea de Tratamiento: ");
				            headerLinea.createCell(1).setCellValue("");
				            headerLinea.getCell(0).setCellStyle(styleTitulos);
				            headerLinea.getCell(1).setCellStyle(styleTitulos);
				            rowCount = rowCount + 2;
				            
				            Row header = sheet.createRow(rowCount);
				            header.createCell(0).setCellValue("Gasto Total");
				            header.getCell(0).setCellStyle(style);
				            header.createCell(1).setCellValue("Nro Pacientes");
				            header.getCell(1).setCellStyle(style);
				            header.createCell(2).setCellValue("G. Paciente");
				            header.getCell(2).setCellStyle(style);
				    		header.createCell(3).setCellValue("Nro. Nuevos");
				    		header.getCell(3).setCellStyle(style);
				    		header.createCell(4).setCellValue("Nro. Continuador");
				    		header.getCell(4).setCellStyle(style);
				        }
				        
				        

				        for (IndicadorConsumoPorGrupoMacLineaResponse preeliminar : ListIndicadoresConsumoPorGrupoMacLineaBean) {
				            
				        	/* Cabecera de Grupo*/
				        	
				        	Row headerGrupo = sheet.createRow(rowCount);
				        	headerGrupo.createCell(0).setCellValue("Grupo Diagonostico: ");
				        	headerGrupo.createCell(1).setCellValue(preeliminar.getCodigoGrupoDiagnostico().toString() + "-"+ grupoDiagnosticoLetra(preeliminar.getCodigoGrupoDiagnostico(), listaGrupoDiagnostico));
				        	headerGrupo.getCell(0).setCellStyle(styleTitulos);
				        	headerGrupo.getCell(1).setCellStyle(styleTitulos);
				            rowCount = rowCount + 1;
				        	
				        	Row headerMac = sheet.createRow(rowCount);
				            headerMac.createCell(0).setCellValue("Medicamento MAC: ");
				            headerMac.createCell(1).setCellValue(preeliminar.getDescripcion());
				            headerMac.getCell(0).setCellStyle(styleTitulos);
				            headerMac.getCell(1).setCellStyle(styleTitulos);
				            rowCount = rowCount + 1;
				            
				            Row headerLinea = sheet.createRow(rowCount);
				            headerLinea.createCell(0).setCellValue("Linea de Tratamiento: ");
				            headerLinea.createCell(1).setCellValue(preeliminar.getLineaTratamiento());
				            headerLinea.getCell(0).setCellStyle(styleTitulos);
				            headerLinea.getCell(1).setCellStyle(styleTitulos);
				            rowCount = rowCount + 2;
				            
				            Row header = sheet.createRow(rowCount);
				            header.createCell(0).setCellValue("Gasto Total");
				            header.getCell(0).setCellStyle(style);
				            header.createCell(1).setCellValue("Nro Pacientes");
				            header.getCell(1).setCellStyle(style);
				            header.createCell(2).setCellValue("G. Paciente");
				            header.getCell(2).setCellStyle(style);
				    		header.createCell(3).setCellValue("Nro. Nuevos");
				    		header.getCell(3).setCellStyle(style);
				    		header.createCell(4).setCellValue("Nro. Continuador");
				    		header.getCell(4).setCellStyle(style);
				        	
				    		rowCount = rowCount + 1;
				        	Row userRow = sheet.createRow(rowCount);
				            userRow.createCell(0).setCellValue(preeliminar.getGastoTotal());
				            userRow.createCell(1).setCellValue(preeliminar.getNroPacientes());
				            userRow.createCell(2).setCellValue(preeliminar.getGastoPaciente());
				            userRow.createCell(3).setCellValue(preeliminar.getNroNuevos());
				            userRow.createCell(4).setCellValue(preeliminar.getNroContinuadores());
				            
				            rowCount++;
				            rowCount++;
				        }
				        
				        
				     

				        try {
				        	
				        	ByteArrayOutputStream baos = new ByteArrayOutputStream();
				        	workbook.write(baos);
				        	byte[] xls = baos.toByteArray();
				        	String pathFileByte = configFTPProp.getRutaTemp()+File.separator  + "IndicadoresDeConsumoPorGrupoMacLinea" + ".xlsx";
				            File fileByte = new File(pathFileByte);
				        	 OutputStream   os  = new FileOutputStream(fileByte); 

					         // Starts writing the bytes in it 
					         os.write(xls); 
				        	 os.close();
				        	 
				            FileOutputStream outputStream = new FileOutputStream(file);
				            workbook.write(outputStream);
				            workbook.close();

				            responseBody.setResponse(xls);
				            responseBody.setCodResultado(0);
				            responseBody.setMsgResultado("Indicador en Excel Generado");
				            
				            //responseBody.setName(file.getName());
				            //responseBody.setLength(file.length());
				            //responseBody.setUrl(configFTPProp.getRutaTemp()+File.separator  + file.getName());

				        } catch (Exception e) {
				            e.printStackTrace();
				        }
				        
				        //responseBody.setUrl(pathFile);
						
			return responseBody;
	}

	@Override
	public ApiOutResponse generarIndicadoresConsumoPorGrupoMacLineaTiempoExcel(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		List<IndicadorConsumoPorGrupoMacLineaTiempoResponse> ListIndicadoresConsumoPorGrupoMacLineatiempoBean = new ArrayList<>();
		ListIndicadoresConsumoPorGrupoMacLineatiempoBean = listaIndicadoresConsumoPorGrupoMacLineaTiempo(requestRequest.getMes(),requestRequest.getAno());
		ApiOutResponse responseBody = new ApiOutResponse();
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		
		/* Llamada a Servicio de Roles de Farmacia Compleja */
		ResponseOnc response = new ResponseOnc();
		ArrayList<GrupoDiagnosticoBean> listaGrupoDiagnostico;
		
		response = diagnosticoService.getListarGrupoDiagnostico(headers);
		listaGrupoDiagnostico = (ArrayList<GrupoDiagnosticoBean>) response.getDataList();						
        /* 
         * Test Generar Archivo en PATH Local 
         * */
	    //String pathFile = "C:\\app\\fcompleja\\indicador" + File.separator + "IndicadoresDeProceso" + ".xlsx";
	    
	    /*  */
	    //String pathFile = ensureDir(request.getRealPath("descargarIndicadorExcel")) + File.separator + "IndicadoresDeProceso" + ".xlsx";
	    String pathFile = configFTPProp.getRutaTemp()+File.separator + "IndicadoresDeConsumoPorGrupoMacLineaTiempo" + ".xlsx";
	     
        File file = new File(pathFile);
        Workbook workbook = new XSSFWorkbook();
        //responseBody.setMime("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

        Sheet sheet = workbook.createSheet("Detalle Indicador");
        
        sheet.setDefaultColumnWidth(30);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        /* Cabecera de Titulos Reporte*/
    	
        CellStyle  styleTitulos = workbook.createCellStyle();
        Font fontTitulos = workbook.createFont();
        fontTitulos.setFontName("Arial");
        styleTitulos.setFillForegroundColor(HSSFColor.WHITE.index);
        styleTitulos.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleTitulos.setAlignment(HorizontalAlignment.CENTER);
        fontTitulos.setBold(true);
        fontTitulos.setColor(HSSFColor.BLACK.index);
        styleTitulos.setFont(fontTitulos);
        
    	Row headerTitulo = sheet.createRow(0);
    	headerTitulo.createCell(2).setCellValue("Indicadores Por Medicamento Grupo Diagnostico, MAC ,  Linea de Tratamiento y Tiempo de Uso");
    	headerTitulo.getCell(2).setCellStyle(styleTitulos);
    	String mesEnLetras = mesLetra(  requestRequest.getMes()  );
    	Row headerFiltro = sheet.createRow(1);
    	headerFiltro.createCell(2).setCellValue("MES: " + mesEnLetras + "  AÑO: "+requestRequest.getAno());
    	headerFiltro.getCell(2).setCellStyle(styleTitulos);
    	
    	
		
		
       

        int rowCount = 2;
        
        if ( ListIndicadoresConsumoPorGrupoMacLineatiempoBean == null || ListIndicadoresConsumoPorGrupoMacLineatiempoBean.isEmpty()) {
        	/*Se Imprimen los Encabezados*/
        	/* Cabecera de Grupo*/
        	
        	Row headerGrupo = sheet.createRow(rowCount);
        	headerGrupo.createCell(0).setCellValue("Grupo Diagonostico: ");
        	headerGrupo.createCell(1).setCellValue("");
        	headerGrupo.getCell(0).setCellStyle(styleTitulos);
        	headerGrupo.getCell(1).setCellStyle(styleTitulos);
            rowCount = rowCount + 1;
        	
        	Row headerMac = sheet.createRow(rowCount);
            headerMac.createCell(0).setCellValue("Medicamento MAC: ");
            headerMac.createCell(1).setCellValue("");
            headerMac.getCell(0).setCellStyle(styleTitulos);
            headerMac.getCell(1).setCellStyle(styleTitulos);
            rowCount = rowCount + 1;
            
            Row headerLinea = sheet.createRow(rowCount);
            headerLinea.createCell(0).setCellValue("Linea de Tratamiento: ");
            headerLinea.createCell(1).setCellValue("");
            headerLinea.getCell(0).setCellStyle(styleTitulos);
            headerLinea.getCell(1).setCellStyle(styleTitulos);
            rowCount = rowCount + 1;
            
            Row headerTiempo = sheet.createRow(rowCount);
            headerTiempo.createCell(0).setCellValue("Tiempo de Uso: ");
            headerTiempo.createCell(1).setCellValue("");
            headerTiempo.getCell(0).setCellStyle(styleTitulos);
            headerTiempo.getCell(1).setCellStyle(styleTitulos);
            rowCount = rowCount + 2;
            
            Row header = sheet.createRow(rowCount);
            header.createCell(0).setCellValue("Gasto Total");
            header.getCell(0).setCellStyle(style);
            header.createCell(1).setCellValue("Nro Pacientes");
            header.getCell(1).setCellStyle(style);
            header.createCell(2).setCellValue("G. Paciente");
            header.getCell(2).setCellStyle(style);
    		header.createCell(3).setCellValue("Nro. Nuevos");
    		header.getCell(3).setCellStyle(style);
    		header.createCell(4).setCellValue("Nro. Continuador");
    		header.getCell(4).setCellStyle(style);
        	
        }
        

        for (IndicadorConsumoPorGrupoMacLineaTiempoResponse preeliminar : ListIndicadoresConsumoPorGrupoMacLineatiempoBean) {
            
        	/* Cabecera de Grupo*/
        	
        	Row headerGrupo = sheet.createRow(rowCount);
        	headerGrupo.createCell(0).setCellValue("Grupo Diagonostico: ");
        	headerGrupo.createCell(1).setCellValue(preeliminar.getCodigoGrupoDiagnostico().toString() + "-"+ grupoDiagnosticoLetra(preeliminar.getCodigoGrupoDiagnostico(), listaGrupoDiagnostico));
        	headerGrupo.getCell(0).setCellStyle(styleTitulos);
        	headerGrupo.getCell(1).setCellStyle(styleTitulos);
            rowCount = rowCount + 1;
        	
        	Row headerMac = sheet.createRow(rowCount);
            headerMac.createCell(0).setCellValue("Medicamento MAC: ");
            headerMac.createCell(1).setCellValue(preeliminar.getDescripcion());
            headerMac.getCell(0).setCellStyle(styleTitulos);
            headerMac.getCell(1).setCellStyle(styleTitulos);
            rowCount = rowCount + 1;
            
            Row headerLinea = sheet.createRow(rowCount);
            headerLinea.createCell(0).setCellValue("Linea de Tratamiento: ");
            headerLinea.createCell(1).setCellValue(preeliminar.getLineaTratamiento());
            headerLinea.getCell(0).setCellStyle(styleTitulos);
            headerLinea.getCell(1).setCellStyle(styleTitulos);
            rowCount = rowCount + 1;
            
            Row headerTiempo = sheet.createRow(rowCount);
            headerTiempo.createCell(0).setCellValue("Tiempo de Uso: ");
            headerTiempo.createCell(1).setCellValue(preeliminar.getTiempoUso());
            headerTiempo.getCell(0).setCellStyle(styleTitulos);
            headerTiempo.getCell(1).setCellStyle(styleTitulos);
            rowCount = rowCount + 2;
            
            Row header = sheet.createRow(rowCount);
            header.createCell(0).setCellValue("Gasto Total");
            header.getCell(0).setCellStyle(style);
            header.createCell(1).setCellValue("Nro Pacientes");
            header.getCell(1).setCellStyle(style);
            header.createCell(2).setCellValue("G. Paciente");
            header.getCell(2).setCellStyle(style);
    		header.createCell(3).setCellValue("Nro. Nuevos");
    		header.getCell(3).setCellStyle(style);
    		header.createCell(4).setCellValue("Nro. Continuador");
    		header.getCell(4).setCellStyle(style);
        	
    		rowCount = rowCount + 1;
        	Row userRow = sheet.createRow(rowCount);
            userRow.createCell(0).setCellValue(preeliminar.getGastoTotal());
            userRow.createCell(1).setCellValue(preeliminar.getNroPacientes());
            userRow.createCell(2).setCellValue(preeliminar.getGastoPaciente());
            userRow.createCell(3).setCellValue(preeliminar.getNroNuevos());
            userRow.createCell(4).setCellValue(preeliminar.getNroContinuadores());
            
            rowCount++;
            rowCount++;
        }
        
        
     

        try {
        	
        	ByteArrayOutputStream baos = new ByteArrayOutputStream();
        	workbook.write(baos);
        	byte[] xls = baos.toByteArray();
        	String pathFileByte = configFTPProp.getRutaTemp()+File.separator  + "IndicadoresDeConsumoPorGrupoMacLineaTiempo" + ".xlsx";
            File fileByte = new File(pathFileByte);
        	 OutputStream   os  = new FileOutputStream(fileByte); 

	         // Starts writing the bytes in it 
	         os.write(xls); 
        	 os.close();
        	 
            FileOutputStream outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            workbook.close();

            responseBody.setResponse(xls);
            responseBody.setCodResultado(0);
            responseBody.setMsgResultado("Indicador en Excel Generado");
            
            //responseBody.setName(file.getName());
            //responseBody.setLength(file.length());
            //responseBody.setUrl(configFTPProp.getRutaTemp()+File.separator  + file.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
        
       
		
        return responseBody;
	}

	@Override
	public ApiOutResponse generarReporteSolicitudesAutorizaciones(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		List<ReporteSolicitudesAutorizacionesResponse> reporteSolicitudesAutorizacionesBean = new ArrayList<>();
		reporteSolicitudesAutorizacionesBean = reporteSolicitudesAutorizaciones(requestRequest.getMes(),requestRequest.getAno());
		ApiOutResponse responseBody = new ApiOutResponse();

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
        
		String pathFile = configFTPProp.getRutaTemp()+File.separator + "ReporteSolicitudesAutorizaciones" + ".xlsx";
	     
        File file = new File(pathFile);
        Workbook workbook = new XSSFWorkbook();
        //responseBody.setMime("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        
        /* Servicio Listar Clinicas */
        ClinicaRequest requestClinica = new ClinicaRequest ();
        
        requestClinica.setIni(1);
        requestClinica.setFin(100);
        requestClinica.setTipoBus(1);
        
        ArrayList<ClinicaBean> listaClinicas = ( ArrayList<ClinicaBean>) clinicaService.obtenerListaClinica(requestClinica, headers).getDataList();
        
        /* Servicio Listar Grupo Diagnostico */
        
        ArrayList<GrupoDiagnosticoBean> listaGrupoDiagnostico = (ArrayList<GrupoDiagnosticoBean>) diagnosticoService.getListarGrupoDiagnostico(headers).getDataList();

        
        /* Servicio Listar Consulta de Afiliados */
        
        AfiliadosRequest requestAfiliado = new AfiliadosRequest();
        requestAfiliado.setFin(100);
    	requestAfiliado.setIni(1);
    	String sCodigosDiagnosticos = "";
    	String sCodigosAfiliados = "";
        for ( ReporteSolicitudesAutorizacionesResponse rp:  reporteSolicitudesAutorizacionesBean) {
        	
        	if (sCodigosAfiliados.indexOf(rp.getCodigoAfiliado().toString()) == -1 )
        	    sCodigosAfiliados = sCodigosAfiliados + "|" + rp.getCodigoAfiliado();
        	
        	if (sCodigosDiagnosticos.indexOf(rp.getCie10().toString()+ "|") == -1 )
        		sCodigosDiagnosticos = sCodigosDiagnosticos + rp.getCie10() + "|";

        }
        requestAfiliado.setCodafir( sCodigosAfiliados);
        List<PacienteBean> listaPaciente = (ArrayList<PacienteBean>) pacienteService.obtenerListaPacientexCodigos(requestAfiliado, headers).getDataList();
       
        /* Servicio Listar Usuarios*/
        UsuarioBean usuario = new UsuarioBean();
        
        usuario.setCodAplicacion(portalProp.getAplicacion());
        
        ArrayList<UsuarioBean> listaUsuarios = (ArrayList<UsuarioBean>) usuarioService.getListarUsuarios(usuario, headers).getResponse();
        
        /*Servicio Listar Diagnosticos por codigo */
        
        ArrayList<DiagnosticoBean> listaDiagnostico =  (ArrayList<DiagnosticoBean>) diagnosticoService.getListarDiagnosticoXcodigos(sCodigosDiagnosticos, 0, headers).getDataList();
        
        
        Sheet sheet = workbook.createSheet("Reporte Solicitudes Autorizaciones");
        
        sheet.setDefaultColumnWidth(30);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        /* Cabecera de Titulos Reporte*/
    	
        CellStyle  styleTitulos = workbook.createCellStyle();
        Font fontTitulos = workbook.createFont();
        fontTitulos.setFontName("Arial");
        styleTitulos.setFillForegroundColor(HSSFColor.WHITE.index);
        styleTitulos.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleTitulos.setAlignment(HorizontalAlignment.CENTER);
        fontTitulos.setBold(true);
        fontTitulos.setColor(HSSFColor.BLACK.index);
        styleTitulos.setFont(fontTitulos);
        
    	Row headerTitulo = sheet.createRow(0);
    	headerTitulo.createCell(2).setCellValue("Reporte Solicitudes Autorizaciones");
    	headerTitulo.getCell(2).setCellStyle(styleTitulos);
    	String mesEnLetras = mesLetra(  requestRequest.getMes()  );
    	Row headerFiltro = sheet.createRow(1);
    	headerFiltro.createCell(2).setCellValue("MES: " + mesEnLetras + "  AÑO: "+requestRequest.getAno());
    	headerFiltro.getCell(2).setCellStyle(styleTitulos);
    	
    	
		
    	/* Cabecera de Grupo*/
    	int rowCount = 2;
    	rowCount = rowCount + 1;
        
        Row header = sheet.createRow(rowCount);
        header.createCell(0).setCellValue("N° SOLICITUD DE EVALUACIÓN");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("CÓDIGO DE PACIENTE");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("CÓDIGO DE AFILIADO");
        header.getCell(2).setCellStyle(style);
		header.createCell(3).setCellValue("FECHA DE AFILIACIÓN");
		header.getCell(3).setCellStyle(style);
		header.createCell(4).setCellValue("TIPO AFILIACIÓN");
		header.getCell(4).setCellStyle(style);
		header.createCell(5).setCellValue("DOCUMENTO IDENTIDAD");
		header.getCell(5).setCellStyle(style);
		header.createCell(6).setCellValue("PACIENTE");
		header.getCell(6).setCellStyle(style);
		header.createCell(7).setCellValue("SEXO");
		header.getCell(7).setCellStyle(style);
		header.createCell(8).setCellValue("EDAD");
		header.getCell(8).setCellStyle(style);
		header.createCell(9).setCellValue("MEDICAMENTO");
		header.getCell(9).setCellStyle(style);
		header.createCell(10).setCellValue("FECHA RECETA");
		header.getCell(10).setCellStyle(style);
		header.createCell(11).setCellValue("CIE10");
		header.getCell(11).setCellStyle(style);
		
		header.createCell(12).setCellValue("DIAGNÓSTICO");
		header.getCell(12).setCellStyle(style);
       
		header.createCell(13).setCellValue("GRUPO DIAGNÓSTICO");
		header.getCell(13).setCellStyle(style);
		
		header.createCell(14).setCellValue("PLAN");
		header.getCell(14).setCellStyle(style);
		
		header.createCell(15).setCellValue("ESTADIO");
		header.getCell(15).setCellStyle(style);
		
		header.createCell(16).setCellValue("TNM");
		header.getCell(16).setCellStyle(style);
		
		header.createCell(17).setCellValue("LINEA");
		header.getCell(17).setCellStyle(style);
		
		header.createCell(18).setCellValue("CLINICA");
		header.getCell(18).setCellStyle(style);
		
		header.createCell(19).setCellValue("CMP");
		header.getCell(19).setCellStyle(style);
		
		header.createCell(20).setCellValue("MEDICO TRATANTE/PRESCRIPTOR");
		header.getCell(20).setCellStyle(style);
		
		header.createCell(21).setCellValue("N° SCG SOLBEN");
		header.getCell(21).setCellStyle(style);
		
		header.createCell(22).setCellValue("FECHA REGISTRO SCG SOLBEN");
		header.getCell(22).setCellStyle(style);
		
		header.createCell(23).setCellValue("HORA REGISTRO SCG SOLBEN");
		header.getCell(23).setCellStyle(style);
		
		header.createCell(24).setCellValue("TIPO SCG SOLBEN");
		header.getCell(24).setCellStyle(style);
		
		header.createCell(25).setCellValue("ESTADO SCG SOLBEN");
		header.getCell(25).setCellStyle(style);
		
		header.createCell(26).setCellValue("N° SOLICITUD PRELIMINAR");
		header.getCell(26).setCellStyle(style);
		 
		header.createCell(27).setCellValue("FECHA REGISTRO SOLICITUD PRELIMINAR");
		header.getCell(27).setCellStyle(style);
		
		header.createCell(28).setCellValue("HORA  REGISTRO SOLICITUD PRELIMINAR");
		header.getCell(28).setCellStyle(style);
		
		header.createCell(29).setCellValue("ESTADO SOLICITUD PRELIMINAR");
		header.getCell(29).setCellStyle(style);
		
		header.createCell(30).setCellValue("AUTORIZADOR DE PERTINENCIAS");
		header.getCell(30).setCellStyle(style);
		
		header.createCell(31).setCellValue("FECHA Y HORA  REGISTRO  SOLICITUD DE EVALUACIÓN");
		header.getCell(31).setCellStyle(style);
		
		header.createCell(32).setCellValue("CONTRATANTE");
		header.getCell(32).setCellStyle(style);
		
		header.createCell(33).setCellValue("CUMPLE PREFERENCIA INTITUCIONAL");
		header.getCell(33).setCellStyle(style);
		
		header.createCell(34).setCellValue("INDICACIÓN-CHECK LIST DE PERFIL DE PACIENTE");
		header.getCell(34).setCellStyle(style);
		
		header.createCell(35).setCellValue("CUMPLE CHECK LIST DE PERFIL DE PACIENTE");
		header.getCell(35).setCellStyle(style);
		
		int nCell = 35;
		
		nCell++;
		header.createCell(nCell).setCellValue("PERTINENCIA");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("CONDICIÓN DEL PACIENTE");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("TIEMPO DE USO");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("ESTADO DE SOLICITUD DE EVALUACIÓN");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("ROL RESPONSABLE PENDIENTE DE EVALUACIÓN");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("AUTORIZADOR DE PERTINENCIAS");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("FECHA DE EVALUACIÓN  AUTORIZADOR DE PERTINENCIAS");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("RESULTADO DE EVALUACIÓN AUTORIZADOR DE PERTINENCIAS");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("LÍDER DE TUMOR");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("FECHA DE EVALUACIÓN  DEL LÍDER DE TUMOR");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("RESULTADO DE EVALUACIÓN DEL LÍDER DE TUMOR");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("CORREO ENVIADO A CMAC");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("FECHA DE REUNIÓN CMAC");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("RESULTADO DE EVALUACIÓN DEL CMAC");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("N° CARTA DE GARANTÍA");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("TIEMPO AUTORIZADOR DE PERTINENCIAS");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("TIEMPO LIDER DE TUMOR");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("TIEMPO DE CMAC");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("TIEMPO TOTAL");
		header.getCell(nCell).setCellStyle(style);
		
		rowCount = rowCount + 1;
		
		try {

        for (ReporteSolicitudesAutorizacionesResponse preeliminar : reporteSolicitudesAutorizacionesBean) {
            
        	
        	Row userRow = sheet.createRow(rowCount);
            userRow.createCell(0).setCellValue(preeliminar.getCodigoSolicitudEva());
            ActualizarLongitudColumna (0,  preeliminar.getCodigoSolicitudEva() != null ?  preeliminar.getCodigoSolicitudEva().toString().length() : null,sheet);

            PacienteBean pacienteBean = listaPaciente.stream().filter(x -> x.getCodafir().equals(preeliminar.getCodigoAfiliado())).findFirst().orElse(null);
            
            userRow.createCell(1).setCellValue(preeliminar.getCodigoPaciente());
            ActualizarLongitudColumna (1, preeliminar.getCodigoPaciente() != null ? preeliminar.getCodigoPaciente().toString().length() : 0 ,sheet);
            
            userRow.createCell(2).setCellValue(preeliminar.getCodigoAfiliado());
            ActualizarLongitudColumna (2, preeliminar.getCodigoAfiliado() != null ? preeliminar.getCodigoAfiliado().toString().length() : 0,sheet);
            
            userRow.createCell(3).setCellValue(pacienteBean != null ? DateUtils.getDateToStringDDMMYYYY(pacienteBean.getFecafi()) : DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaAfiliacion()));
            ActualizarLongitudColumna (3, preeliminar.getFechaAfiliacion() != null ? preeliminar.getFechaAfiliacion().toString().length() : 0,sheet);
            
            userRow.createCell(4).setCellValue(pacienteBean != null ? pacienteBean.getTipafi() :preeliminar.getTipoAfiliacion());
            ActualizarLongitudColumna (4, preeliminar.getTipoAfiliacion() != null ? preeliminar.getTipoAfiliacion().toString().length() : 0,sheet);
            
            userRow.createCell(5).setCellValue(pacienteBean != null ? pacienteBean.getNumdoc() : preeliminar.getDocumentoIdentidad());
            ActualizarLongitudColumna (5, preeliminar.getDocumentoIdentidad() != null ? preeliminar.getDocumentoIdentidad().toString().length() : 0,sheet);
            
            userRow.createCell(6).setCellValue(pacienteBean != null ? pacienteBean.getApelNomb() : preeliminar.getPaciente() );
            ActualizarLongitudColumna (6, pacienteBean != null ? pacienteBean.getApelNomb().toString().length() : 0,sheet);
            
            userRow.createCell(7).setCellValue(pacienteBean.getSexafi());
            ActualizarLongitudColumna (7, preeliminar.getSexo() != null ? preeliminar.getSexo().toString().length() : 0,sheet);
            
            userRow.createCell(8).setCellValue(pacienteBean.getEdaafi());
            ActualizarLongitudColumna (8, preeliminar.getEdad() != null ? preeliminar.getEdad().toString().length() : 0,sheet);
            
                      
            userRow.createCell(9).setCellValue(preeliminar.getMedicamento());
            ActualizarLongitudColumna (9, preeliminar.getMedicamento() != null ? preeliminar.getMedicamento().toString().length() : 0,sheet);
            
            userRow.createCell(10).setCellValue( DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaReceta()));
            ActualizarLongitudColumna (10, preeliminar.getFechaReceta() != null ? preeliminar.getFechaReceta().toString().length(): 0,sheet);
            
            userRow.createCell(11).setCellValue(preeliminar.getCie10());
            ActualizarLongitudColumna (11, preeliminar.getCie10() != null ?  preeliminar.getCie10().toString().length(): 0,sheet);
            
            DiagnosticoBean diagnosticoBean = listaDiagnostico.stream().filter(x -> x.getCodigo().toString().equals(preeliminar.getCie10())).findFirst()
                    .orElse(null);
            
            
            userRow.createCell(12).setCellValue(diagnosticoBean != null ? diagnosticoBean.getDiagnostico() : preeliminar.getCie10());
            ActualizarLongitudColumna (12, diagnosticoBean != null ? diagnosticoBean.getDiagnostico().toString().length(): 0,sheet);
            
            String nombreGrupoDiagnostico = buscarGrupoDiagnostico(listaGrupoDiagnostico, preeliminar.getGrupoDiagnostico());
            userRow.createCell(13).setCellValue(nombreGrupoDiagnostico);
            ActualizarLongitudColumna (13,  preeliminar.getGrupoDiagnostico() != null ? nombreGrupoDiagnostico.length(): 0,sheet);
            
            userRow.createCell(14).setCellValue(pacienteBean.getNomPlan());
            ActualizarLongitudColumna (14, preeliminar.getPlan() != null ?  preeliminar.getPlan().toString().length():0,sheet);
            
            userRow.createCell(15).setCellValue(preeliminar.getEstadio());
            ActualizarLongitudColumna (15, preeliminar.getEstadio() != null ?  preeliminar.getEstadio().toString().length():0,sheet);
            
            userRow.createCell(16).setCellValue(preeliminar.getTnm());
            ActualizarLongitudColumna (16, preeliminar.getTnm() != null ?  preeliminar.getTnm().toString().length():0,sheet);
            
            userRow.createCell(17).setCellValue(preeliminar.getLinea());
            ActualizarLongitudColumna (17, preeliminar.getLinea() != null ?  preeliminar.getLinea().toString().length():0,sheet);
            
            String nombreClinica = buscarNombreClinica (listaClinicas,preeliminar.getClinica());
            userRow.createCell(18).setCellValue(nombreClinica);
            ActualizarLongitudColumna (18, preeliminar.getClinica() != null ?  nombreClinica.length():0,sheet);
            
            userRow.createCell(19).setCellValue(preeliminar.getCmp());
            ActualizarLongitudColumna (19, preeliminar.getCmp() != null ?  preeliminar.getCmp().toString().length():0,sheet);
            
            userRow.createCell(20).setCellValue(preeliminar.getMedicoTratantePrescriptor());
            ActualizarLongitudColumna (20, preeliminar.getMedicoTratantePrescriptor() != null ?  preeliminar.getMedicoTratantePrescriptor().toString().length():0,sheet);
            
            userRow.createCell(21).setCellValue(preeliminar.getCodigoSCGSolben());
            ActualizarLongitudColumna (21, preeliminar.getCodigoSCGSolben() != null ?  preeliminar.getCodigoSCGSolben().toString().length():0,sheet);
            
            userRow.createCell(22).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaRegistroSCGSolben()));
            ActualizarLongitudColumna (22, preeliminar.getFechaRegistroSCGSolben() != null ?  preeliminar.getFechaRegistroSCGSolben().toString().length():0,sheet);
            
            userRow.createCell(23).setCellValue(preeliminar.getHoraRegistroSCGSolben());
            ActualizarLongitudColumna (23, preeliminar.getHoraRegistroSCGSolben() != null ?  preeliminar.getHoraRegistroSCGSolben().toString().length():0,sheet);
            
            userRow.createCell(24).setCellValue(preeliminar.getTipoSCGSolben());
            ActualizarLongitudColumna (24, preeliminar.getTipoSCGSolben() != null ?  preeliminar.getTipoSCGSolben().toString().length():0,sheet);
            
            userRow.createCell(25).setCellValue(preeliminar.getEstadoSCGSolben());
            ActualizarLongitudColumna (25, preeliminar.getEstadoSCGSolben() != null ?  preeliminar.getEstadoSCGSolben().toString().length():0,sheet);
            
            userRow.createCell(26).setCellValue(preeliminar.getCodigoSolicitudPre());
            ActualizarLongitudColumna (26, preeliminar.getCodigoSolicitudPre() != null ?  preeliminar.getCodigoSolicitudPre().toString().length():null,sheet);
            
            userRow.createCell(27).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaRegistroSolicitudPre()));
            ActualizarLongitudColumna (27, preeliminar.getFechaRegistroSolicitudPre() != null ?  preeliminar.getFechaRegistroSolicitudPre().toString().length():0,sheet);
            
            userRow.createCell(28).setCellValue(preeliminar.getHoraRegistroSolicitudPre());
            ActualizarLongitudColumna (28, preeliminar.getHoraRegistroSolicitudPre() != null ?  preeliminar.getHoraRegistroSolicitudPre().toString().length():0,sheet);
            
            userRow.createCell(29).setCellValue(preeliminar.getEstadoSolicitudPre());
            ActualizarLongitudColumna (29, preeliminar.getEstadoSolicitudPre() != null ?  preeliminar.getEstadoSolicitudPre().toString().length():0,sheet);
            
            
            String usuarioSolben = preeliminar.getAutorizadorPertinenciaSolicitudPre() != null ? buscarUsuarioSolben( Integer.parseInt(preeliminar.getAutorizadorPertinenciaSolicitudPre()),headers) : "";
            userRow.createCell(30).setCellValue(usuarioSolben);
            ActualizarLongitudColumna (30, preeliminar.getAutorizadorPertinenciaSolicitudPre() != null ?  usuarioSolben.length():0,sheet);
            
            userRow.createCell(31).setCellValue(DateUtils.getDateToStringDDMMYYYHHMMSS(preeliminar.getFechaHoraRegistroSolicitudEva()));
            ActualizarLongitudColumna (31, preeliminar.getFechaHoraRegistroSolicitudEva() != null ?  preeliminar.getFechaHoraRegistroSolicitudEva().toString().length():0,sheet);
            
            userRow.createCell(32).setCellValue(preeliminar.getContratante());
            ActualizarLongitudColumna (32, preeliminar.getContratante() != null ?  preeliminar.getContratante().toString().length():0,sheet);
            
            userRow.createCell(33).setCellValue(preeliminar.getCumplePrefInstitucional());
            ActualizarLongitudColumna (33, preeliminar.getCumplePrefInstitucional() != null ?  preeliminar.getCumplePrefInstitucional().toString().length():0,sheet);
            
            userRow.createCell(34).setCellValue(preeliminar.getIndicacionesCheckListPaciente());
            ActualizarLongitudColumna (34, preeliminar.getIndicacionesCheckListPaciente() != null ?  preeliminar.getIndicacionesCheckListPaciente().toString().length():0,sheet);
            
            userRow.createCell(35).setCellValue(preeliminar.getCumpleIndicacionesCheckListPaciente());
            ActualizarLongitudColumna (35, preeliminar.getCumpleIndicacionesCheckListPaciente() != null ?  preeliminar.getCumpleIndicacionesCheckListPaciente().toString().length():0,sheet);
            
            userRow.createCell(36).setCellValue(preeliminar.getPertinencia());
            ActualizarLongitudColumna (36, preeliminar.getPertinencia() != null ?  preeliminar.getPertinencia().toString().length():0,sheet);
            
            userRow.createCell(37).setCellValue(preeliminar.getCondicionPaciente());
            ActualizarLongitudColumna (37, preeliminar.getCondicionPaciente() != null ?  preeliminar.getCondicionPaciente().toString().length():0,sheet);
            
            userRow.createCell(38).setCellValue(preeliminar.getTiempoUso());
            ActualizarLongitudColumna (38, preeliminar.getTiempoUso() != null ?  preeliminar.getTiempoUso().toString().length():0,sheet);
            
            userRow.createCell(39).setCellValue(preeliminar.getEstadoSolicitudEva());
            ActualizarLongitudColumna (39, preeliminar.getEstadoSolicitudEva() != null ?  preeliminar.getEstadoSolicitudEva().toString().length():0,sheet);
            
            userRow.createCell(40).setCellValue(preeliminar.getRolResponsablePendEva());
            ActualizarLongitudColumna (40, preeliminar.getRolResponsablePendEva() != null ?  preeliminar.getRolResponsablePendEva().toString().length():0,sheet);
            
            UsuarioBean usuarioBean = listaUsuarios.stream().filter(x -> x.getCodUsuario().toString().equals(preeliminar.getAutorizadorPertinenciaSolicitudEva())).findFirst()
                    .orElse(null);
            
            
            
            userRow.createCell(41).setCellValue(usuarioBean != null ? usuarioBean.getNombreApellido(): preeliminar.getAutorizadorPertinenciaSolicitudEva());
            ActualizarLongitudColumna (41, preeliminar.getAutorizadorPertinenciaSolicitudEva() != null ?  preeliminar.getAutorizadorPertinenciaSolicitudEva().toString().length():0,sheet);
            
            userRow.createCell(42).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaEvaluacionAutorizadorPertinencia()));
            ActualizarLongitudColumna (42, preeliminar.getFechaEvaluacionAutorizadorPertinencia() != null ?  preeliminar.getFechaEvaluacionAutorizadorPertinencia().toString().length():0,sheet);
            
            userRow.createCell(43).setCellValue(preeliminar.getResultadoEvaluacionAutorizadorPertinencia());
            ActualizarLongitudColumna (43, preeliminar.getResultadoEvaluacionAutorizadorPertinencia() != null ?  preeliminar.getResultadoEvaluacionAutorizadorPertinencia().toString().length():0,sheet);
            
            userRow.createCell(44).setCellValue(preeliminar.getLiderTumor());
            ActualizarLongitudColumna (44, preeliminar.getLiderTumor() != null ?  preeliminar.getLiderTumor().toString().length():0,sheet);
            
            userRow.createCell(45).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaEvaluacionLiderTumor()));
            ActualizarLongitudColumna (45, preeliminar.getFechaEvaluacionLiderTumor() != null ?  preeliminar.getFechaEvaluacionLiderTumor().toString().length():0,sheet);
            
            userRow.createCell(46).setCellValue(preeliminar.getResultadoEvaluacionLiderTumor());
            ActualizarLongitudColumna (46, preeliminar.getResultadoEvaluacionLiderTumor() != null ?  preeliminar.getResultadoEvaluacionLiderTumor().toString().length():0,sheet);
            
            userRow.createCell(47).setCellValue(preeliminar.getCorreoEnviadoCMAC());
            ActualizarLongitudColumna (47, preeliminar.getCorreoEnviadoCMAC() != null ?  preeliminar.getCorreoEnviadoCMAC().toString().length():0,sheet);
            
            userRow.createCell(48).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaReunionCMAC()));
            ActualizarLongitudColumna (48, preeliminar.getFechaReunionCMAC() != null ?  preeliminar.getFechaReunionCMAC().toString().length():0,sheet);
            
            userRow.createCell(49).setCellValue(preeliminar.getResultadoEvaluacionCMAC());
            ActualizarLongitudColumna (49, preeliminar.getResultadoEvaluacionCMAC() != null ?  preeliminar.getResultadoEvaluacionCMAC().toString().length():0,sheet);
            
            userRow.createCell(50).setCellValue( preeliminar.getNroCartaGarantia());
            ActualizarLongitudColumna (50, preeliminar.getNroCartaGarantia() != null ?  preeliminar.getNroCartaGarantia().toString().length():null,sheet);
            
            userRow.createCell(51).setCellValue(preeliminar.getTiempoAutorPertinenciaEva());
            ActualizarLongitudColumna (51, preeliminar.getTiempoAutorPertinenciaEva() != null ?  preeliminar.getTiempoAutorPertinenciaEva().toString().length():0,sheet);
            
            userRow.createCell(52).setCellValue(preeliminar.getTiempoLiderTumor());
            ActualizarLongitudColumna (52, preeliminar.getTiempoLiderTumor() != null ?  preeliminar.getTiempoLiderTumor().toString().length():0,sheet);
           
            userRow.createCell(53).setCellValue(preeliminar.getTiempoCMAC());
            ActualizarLongitudColumna (53, preeliminar.getTiempoCMAC() != null ?  preeliminar.getTiempoCMAC().toString().length():0,sheet);
            
            userRow.createCell(54).setCellValue(preeliminar.getTiempoTotal());
            ActualizarLongitudColumna (54, preeliminar.getTiempoTotal() != null ?  preeliminar.getTiempoTotal().toString().length():0,sheet);
            
            rowCount++;
            
        }
        
        
        	
        	ByteArrayOutputStream baos = new ByteArrayOutputStream();
        	workbook.write(baos);
        	byte[] xls = baos.toByteArray();
        	String pathFileByte = configFTPProp.getRutaTemp()+File.separator  + "ReporteSolicitudesAutorizaciones" + ".xlsx";
            File fileByte = new File(pathFileByte);
        	 OutputStream   os  = new FileOutputStream(fileByte); 

	         // Starts writing the bytes in it 
	         os.write(xls); 
        	 os.close();
        	 
            FileOutputStream outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            workbook.close();

            responseBody.setResponse(xls);
            responseBody.setCodResultado(0);
            responseBody.setMsgResultado("Indicador en Excel Generado");
            
            //responseBody.setName(file.getName());
            //responseBody.setLength(file.length());
            //responseBody.setUrl(configFTPProp.getRutaTemp()+File.separator  + file.getName());

        } catch (Exception e) {
            e.printStackTrace();
            responseBody.setCodResultado(-3);
            responseBody.setMsgResultado("Error en la Generacion del Reporte, volver a intentar.");
        }
        
        //responseBody.setUrl(pathFile);
		
		return responseBody;
	}
	
	private void ActualizarLongitudColumna ( int columna , Integer longitud, Sheet sheet) {
		
		if (longitud == null ) 
			longitud = 0;
		
//		System.out.println("Long Columna [" + columna + "] : " + sheet.getDefaultColumnWidth());
//		 if (sheet.getDefaultColumnWidth() < longitud) {
//			 sheet.setColumnWidth(columna, longitud * 300);
//		 }
		
	}

	@Override
	public ApiOutResponse generarReporteGenerales(ReporteIndicadoresRequest requestRequest) throws Exception {
		// TODO Auto-generated method stub
		
		ApiOutResponse responseBody = new ApiOutResponse();
		Map<String, Object> out = new HashMap<>();

		try {
			/** Call store procedure */
			log.info("Call store procedure : SP_PFC_I_REP_SOL_AUTOR ");
			log.info("Parametros de entrada: "+requestRequest.toString());
			log.info("Mes: "+requestRequest.getMes());
			log.info("Anyo: "+requestRequest.getAno());
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES)
					.withProcedureName("SP_PFC_I_REP_SOL_AUTOR");

			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_FEC_MES", requestRequest.getMes(), Types.NUMERIC);
			in.addValue("PN_FEC_ANO", requestRequest.getAno(), Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			
			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			} else {
				responseBody.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
				responseBody.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());
				/*Se generan Datos de Monitoreo*/
				log.info("Se generan Datos de Monitoreo");
				log.info("Parametros de salida: ");
				
				ApiOutResponse responseBodyMonitoreo = generarReporteGeneralesMonitoresDatos(requestRequest);
				
				
			}
			
			log.info("PN_COD_RESULTADO: "+out.get("PN_COD_RESULTADO").toString());
			log.info("PV_MSG_RESULTADO: "+out.get("PV_MSG_RESULTADO").toString());
			

		} catch (Exception e) {
			log.error("ERROR DE REPORTES GENERALES");
			log.error("PN_COD_RESULTADO: "+out.get("PN_COD_RESULTADO").toString());
			log.error("PV_MSG_RESULTADO: "+out.get("PV_MSG_RESULTADO").toString());
			log.error("Error al listar : generarReporteGenerales ", e);
		}
		
		
		return responseBody;
		
	}
	
	private String buscarNombreClinica (ArrayList<ClinicaBean> listaClinicas , String codigo ) {
		
		String descripcionClinica = "NO REGISTRADO";
		
		for ( ClinicaBean cb:listaClinicas) {
			if ( cb.getCodcli().equals(codigo)) {
				descripcionClinica = cb.getNomcli();
				break;
			}
			
		}
		
		return descripcionClinica;
		
	}
	
	private String buscarGrupoDiagnostico (ArrayList<GrupoDiagnosticoBean> listaGrupoDiagnostico , String codigo) {
		String descripcionGrupo = "NO REGISTRADO";
		
		for ( GrupoDiagnosticoBean gd:listaGrupoDiagnostico) {
			if ( gd.getCodigo().equals(codigo)) {
				descripcionGrupo = gd.getDescripcion();
				break;
			}
			
		}
		
		return descripcionGrupo;
	}
	private String buscarUsuarioSolben ( Integer codigoSoben , HttpHeaders headers) {
		
		/* Servicio de Consulta Usuario Por User Solben*/
        
        UsuarioBean usuario = new UsuarioBean();
        
        usuario.setCodSolben(codigoSoben);
        usuario.setCodAplicacion(portalProp.getAplicacion());
        
        ArrayList<UsuarioBean> listaUsuarios = ( ArrayList<UsuarioBean> ) usuarioService.getListarUsuarioSolben(usuario, headers).getResponse();
        
		
		String nombreUsuario = "NO REGISTRADO";
		if ( listaUsuarios != null)
			for ( UsuarioBean ub:listaUsuarios) {
				
				nombreUsuario = ub.getNombre() + " " + ub.getApellidoMaterno() + " " + ub.getApellidoPaterno();
				
				
			}
		
		return nombreUsuario;
	}
	
	
	private PacienteBean buscarDatosAfiliado (String codigo , ArrayList<PacienteBean> listaPcte) {
		
		PacienteBean paciente = null;
		
		for ( PacienteBean afi: listaPcte) {
			if (afi.getCodafir().equals(codigo)) {
				paciente = afi;
				break;
			}
		}
		return paciente;
	}

	@Override
	public ApiOutResponse generarReporteSolicitudesMonitoreo(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		List<ReporteSolicitudesMonitoreoResponse> reporteSolicitudesMonitoreoBean = new ArrayList<>();
		reporteSolicitudesMonitoreoBean = reporteSolicitudesMonitoreo(requestRequest.getMes(),requestRequest.getAno());
		ApiOutResponse responseBody = new ApiOutResponse();
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
        
		String pathFile = configFTPProp.getRutaTemp()+File.separator + "ReporteSolicitudesMonitoreo" + ".xlsx";
	     
        File file = new File(pathFile);
        Workbook workbook = new XSSFWorkbook();
        //responseBody.setMime("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        
        /* Servicio Listar Clinicas */
        ClinicaRequest requestClinica = new ClinicaRequest ();
        
        requestClinica.setIni(1);
        requestClinica.setFin(100);
        requestClinica.setTipoBus(1);
        
		ArrayList<ClinicaBean> listaClinicas = ( ArrayList<ClinicaBean>) clinicaService.obtenerListaClinica(requestClinica, headers).getDataList();
        
        /* Servicio Listar Grupo Diagnostico */
        
        ArrayList<GrupoDiagnosticoBean> listaGrupoDiagnostico = (ArrayList<GrupoDiagnosticoBean>) diagnosticoService.getListarGrupoDiagnostico(headers).getDataList();

        
        /* Servicio Listar Consulta de Afiliados */
        
        AfiliadosRequest requestAfiliado = new AfiliadosRequest();
        requestAfiliado.setFin(100);
    	requestAfiliado.setIni(1);
    	String sCodigosDiagnosticos = "";
    	String sCodigosAfiliados = "";
        
        for ( ReporteSolicitudesMonitoreoResponse rp:  reporteSolicitudesMonitoreoBean) {
        	
        	if (sCodigosAfiliados.indexOf(rp.getCodigoAfiliado().toString()) == -1 )
        	    sCodigosAfiliados = sCodigosAfiliados + "|" + rp.getCodigoAfiliado();
        	
        	if (sCodigosDiagnosticos.indexOf(rp.getCie10().toString()+ "|") == -1 )
        		sCodigosDiagnosticos = sCodigosDiagnosticos + rp.getCie10() + "|";
        }
        requestAfiliado.setCodafir( sCodigosAfiliados);
        List<PacienteBean> listaPaciente = (ArrayList<PacienteBean>) pacienteService.obtenerListaPacientexCodigos(requestAfiliado, headers).getDataList();
       
        /* Servicio Listar Usuarios*/
        UsuarioBean usuario = new UsuarioBean();
        
        usuario.setCodAplicacion(portalProp.getAplicacion());
        
        ArrayList<UsuarioBean> listaUsuarios = (ArrayList<UsuarioBean>) usuarioService.getListarUsuarios(usuario, headers).getResponse();
        
        /*Servicio Listar Diagnosticos por codigo */
        
        ArrayList<DiagnosticoBean> listaDiagnostico =  (ArrayList<DiagnosticoBean>) diagnosticoService.getListarDiagnosticoXcodigos(sCodigosDiagnosticos, 0, headers).getDataList();
        
        
        Sheet sheet = workbook.createSheet("Reporte Solicitudes Monitoreo");
        
        sheet.setDefaultColumnWidth(30);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        /* Cabecera de Titulos Reporte*/
    	
        CellStyle  styleTitulos = workbook.createCellStyle();
        Font fontTitulos = workbook.createFont();
        fontTitulos.setFontName("Arial");
        styleTitulos.setFillForegroundColor(HSSFColor.WHITE.index);
        styleTitulos.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleTitulos.setAlignment(HorizontalAlignment.CENTER);
        fontTitulos.setBold(true);
        fontTitulos.setColor(HSSFColor.BLACK.index);
        styleTitulos.setFont(fontTitulos);
        
    	Row headerTitulo = sheet.createRow(0);
    	headerTitulo.createCell(2).setCellValue("Reporte Solicitudes Monitoreo");
    	headerTitulo.getCell(2).setCellStyle(styleTitulos);
    	String mesEnLetras = mesLetra(  requestRequest.getMes()  );
    	Row headerFiltro = sheet.createRow(1);
    	headerFiltro.createCell(2).setCellValue("MES: " + mesEnLetras + "  AÑO: "+requestRequest.getAno());
    	headerFiltro.getCell(2).setCellStyle(styleTitulos);
    	
    	
		
    	/* Cabecera de Grupo*/
    	int rowCount = 2;
    	rowCount = rowCount + 1;
        
        Row header = sheet.createRow(rowCount);
        header.createCell(0).setCellValue("CÓDIGO DE TAREA DE MONITOREO");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("CÓDIGO DE PACIENTE");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("CÓDIGO DE AFILIADO");
        header.getCell(2).setCellStyle(style);
		header.createCell(3).setCellValue("FECHA DE AFILIACIÓN");
		header.getCell(3).setCellStyle(style);
		header.createCell(4).setCellValue("TIPO AFILIACIÓN");
		header.getCell(4).setCellStyle(style);
		header.createCell(5).setCellValue("DOCUMENTO IDENTIDAD");
		header.getCell(5).setCellStyle(style);
		header.createCell(6).setCellValue("PACIENTE");
		header.getCell(6).setCellStyle(style);
		header.createCell(7).setCellValue("SEXO");
		header.getCell(7).setCellStyle(style);
		header.createCell(8).setCellValue("EDAD");
		header.getCell(8).setCellStyle(style);
		header.createCell(9).setCellValue("MEDICAMENTO");
		header.getCell(9).setCellStyle(style);
		header.createCell(10).setCellValue("CIE10");
		header.getCell(10).setCellStyle(style);
		header.createCell(11).setCellValue("DIAGNÓSTICO");
		header.getCell(11).setCellStyle(style);
		
		header.createCell(12).setCellValue("GRUPO DIAGNÓSTICO");
		header.getCell(12).setCellStyle(style);
       
		header.createCell(13).setCellValue("PLAN");
		header.getCell(13).setCellStyle(style);
		
		header.createCell(14).setCellValue("ESTADÍO");
		header.getCell(14).setCellStyle(style);
		
		header.createCell(15).setCellValue("TNM");
		header.getCell(15).setCellStyle(style);
		
		header.createCell(16).setCellValue("LÍNEA TRATAMIENTO ACTUAL");
		header.getCell(16).setCellStyle(style);
		
		header.createCell(17).setCellValue("CLÍNICA");
		header.getCell(17).setCellStyle(style);
		
		header.createCell(18).setCellValue("CMP");
		header.getCell(18).setCellStyle(style);
		
		header.createCell(19).setCellValue("MEDICO TRATANTE/PRESCRIPTOR");
		header.getCell(19).setCellStyle(style);
		
		header.createCell(20).setCellValue("N° SOLICITUD DE EVALUACIÓN");
		header.getCell(20).setCellStyle(style);
		
		header.createCell(21).setCellValue("FECHA DE APROBACIÓN SOLICITUD DE EVALUACIÓN");
		header.getCell(21).setCellStyle(style);
		
		header.createCell(22).setCellValue("ESTADO DE SOLICITUD DE EVALUACIÓN");
		header.getCell(22).setCellStyle(style);
		
		header.createCell(23).setCellValue("AUTORIZADOR DE PERTINENCIAS");
		header.getCell(23).setCellStyle(style);
		
		header.createCell(24).setCellValue("N° SCG SOLBEN");
		header.getCell(24).setCellStyle(style);
		
		header.createCell(25).setCellValue("N° CG SOLBEN");
		header.getCell(25).setCellStyle(style);
		
		header.createCell(26).setCellValue("FECHA DE INICIO DE TRATAMIENTO");
		header.getCell(26).setCellStyle(style);
		 
		header.createCell(27).setCellValue("N° EVOLUCIÓN");
		header.getCell(27).setCellStyle(style);
		
		header.createCell(28).setCellValue("RESPONSABLE DE MONITOREO");
		header.getCell(28).setCellStyle(style);
		
		header.createCell(29).setCellValue("FECHA PROGRAMADA DE MONITOREO");
		header.getCell(29).setCellStyle(style);
		
		header.createCell(30).setCellValue("FECHA REAL DE MONITOREO");
		header.getCell(30).setCellStyle(style);
		
		header.createCell(31).setCellValue("ESTADO TAREA  DE MONITOREO");
		header.getCell(31).setCellStyle(style);
		
		header.createCell(32).setCellValue("FECHA DE PRÓXIMO MONITOREO");
		header.getCell(32).setCellStyle(style);
		
		header.createCell(33).setCellValue("TOLERANCIA ");
		header.getCell(33).setCellStyle(style);
		
		header.createCell(34).setCellValue("TOXICIDAD");
		header.getCell(34).setCellStyle(style);
		
		header.createCell(35).setCellValue("RESPUESTA CLÍNICA");
		header.getCell(35).setCellStyle(style);
		
		int nCell = 35;
		
		nCell++;
		header.createCell(nCell).setCellValue("ATENCIÓN DE ALERTAS");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("FECHA DE ÚLTIMO CONSUMO");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("ÚLTIMA CANTIDAD CONSUMIDA");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("RESULTADO DE LA EVOLUCIÓN");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("FECHA DE INACTIVACIÓN");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("MOTIVO DE INACTIVACIÓN");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("EJECUTIVO DE MONITOREO");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("ESTADO DE SEGUIMIENTO");
		header.getCell(nCell).setCellStyle(style);
		
		nCell++;
		header.createCell(nCell).setCellValue("FECHA DE REGISTRO");
		header.getCell(nCell).setCellStyle(style);
		
		
		
		
		
		rowCount = rowCount + 1;

        for (ReporteSolicitudesMonitoreoResponse preeliminar : reporteSolicitudesMonitoreoBean) {
            
        	
        	
    		
        	Row userRow = sheet.createRow(rowCount);
            userRow.createCell(0).setCellValue(preeliminar.getCodigoTareaMonitoreo());
            ActualizarLongitudColumna (0,  preeliminar.getCodigoTareaMonitoreo() != null ?  preeliminar.getCodigoTareaMonitoreo().toString().length() : 0,sheet);

            PacienteBean pacienteBean = null;
            if ( listaPaciente != null )
                 	pacienteBean= listaPaciente.stream().filter(x -> x.getCodafir().equals(preeliminar.getCodigoAfiliado())).findFirst()
                    .orElse(null);
            
            userRow.createCell(1).setCellValue(preeliminar.getCodigoPaciente());
            ActualizarLongitudColumna (1, preeliminar.getCodigoPaciente() != null ? preeliminar.getCodigoPaciente().toString().length() : 0 ,sheet);
            
            userRow.createCell(2).setCellValue(preeliminar.getCodigoAfiliado());
            ActualizarLongitudColumna (2, preeliminar.getCodigoAfiliado() != null ? preeliminar.getCodigoAfiliado().toString().length() : 0,sheet);
            
            userRow.createCell(3).setCellValue(pacienteBean != null ? DateUtils.getDateToStringDDMMYYYY(pacienteBean.getFecafi()) : DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaAfiliacion()));
            ActualizarLongitudColumna (3, preeliminar.getFechaAfiliacion() != null ? preeliminar.getFechaAfiliacion().toString().length() : 0,sheet);
            
            userRow.createCell(4).setCellValue(pacienteBean != null ? pacienteBean.getTipafi() :preeliminar.getTipoAfiliacion());
            ActualizarLongitudColumna (4, preeliminar.getTipoAfiliacion() != null ? preeliminar.getTipoAfiliacion().toString().length() : 0,sheet);
            
            userRow.createCell(5).setCellValue(pacienteBean != null ? pacienteBean.getNumdoc() : preeliminar.getDocumentoIdentidad());
            ActualizarLongitudColumna (5, preeliminar.getDocumentoIdentidad() != null ? preeliminar.getDocumentoIdentidad().toString().length() : 0,sheet);
            
            userRow.createCell(6).setCellValue(pacienteBean != null ? pacienteBean.getApelNomb() : preeliminar.getPaciente() );
            ActualizarLongitudColumna (6, pacienteBean != null ? pacienteBean.getApelNomb().toString().length() : 0,sheet);
            
            userRow.createCell(7).setCellValue(pacienteBean!= null ? pacienteBean.getSexafi():preeliminar.getSexo());
            ActualizarLongitudColumna (7, preeliminar.getSexo() != null ? preeliminar.getSexo().toString().length() : 0,sheet);
            
            userRow.createCell(8).setCellValue(pacienteBean!= null ? pacienteBean.getEdaafi():preeliminar.getEdad());
            ActualizarLongitudColumna (8, preeliminar.getEdad() != null ? preeliminar.getEdad().toString().length() : 0,sheet);
            
                      
            userRow.createCell(9).setCellValue(preeliminar.getMedicamento());
            ActualizarLongitudColumna (9, preeliminar.getMedicamento() != null ? preeliminar.getMedicamento().toString().length() : 0,sheet);
            
            userRow.createCell(10).setCellValue(preeliminar.getCie10());
            ActualizarLongitudColumna (10, preeliminar.getCie10() != null ?  preeliminar.getCie10().toString().length(): 0,sheet);
            
            DiagnosticoBean diagnosticoBean = null;
            if (listaDiagnostico != null)
            	diagnosticoBean= listaDiagnostico.stream().filter(x -> x.getCodigo().toString().equals(preeliminar.getCie10())).findFirst().orElse(null);
            
            userRow.createCell(11).setCellValue(diagnosticoBean != null ? diagnosticoBean.getDiagnostico() : preeliminar.getCie10());
            ActualizarLongitudColumna (11, diagnosticoBean != null ? diagnosticoBean.getDiagnostico().toString().length(): 0,sheet);
            
            String nombreGrupoDiagnostico = buscarGrupoDiagnostico(listaGrupoDiagnostico, preeliminar.getGrupoDiagnostico());
            userRow.createCell(12).setCellValue(nombreGrupoDiagnostico);
            ActualizarLongitudColumna (12,  preeliminar.getGrupoDiagnostico() != null ? nombreGrupoDiagnostico.length(): 0,sheet);
            
            userRow.createCell(13).setCellValue(pacienteBean != null ? pacienteBean.getNomPlan(): preeliminar.getPlan() );
            ActualizarLongitudColumna (13, preeliminar.getPlan() != null ?  preeliminar.getPlan().toString().length():0,sheet);
            
            userRow.createCell(14).setCellValue(preeliminar.getEstadio());
            ActualizarLongitudColumna (14, preeliminar.getEstadio() != null ?  preeliminar.getEstadio().toString().length():0,sheet);
            
            userRow.createCell(15).setCellValue(preeliminar.getTnm());
            ActualizarLongitudColumna (15, preeliminar.getTnm() != null ?  preeliminar.getTnm().toString().length():0,sheet);
            
            userRow.createCell(16).setCellValue(preeliminar.getLinea());
            ActualizarLongitudColumna (16, preeliminar.getLinea() != null ?  preeliminar.getLinea().toString().length():0,sheet);
            
            String nombreClinica = buscarNombreClinica (listaClinicas,preeliminar.getClinica());
            userRow.createCell(17).setCellValue(nombreClinica);
            ActualizarLongitudColumna (17, preeliminar.getClinica() != null ?  nombreClinica.length():0,sheet);
            
            userRow.createCell(18).setCellValue(preeliminar.getCmp());
            ActualizarLongitudColumna (18, preeliminar.getCmp() != null ?  preeliminar.getCmp().toString().length():0,sheet);
            
            userRow.createCell(19).setCellValue(preeliminar.getMedicoTratantePrescriptor());
            ActualizarLongitudColumna (19, preeliminar.getMedicoTratantePrescriptor() != null ?  preeliminar.getMedicoTratantePrescriptor().toString().length():0,sheet);
            
            userRow.createCell(20).setCellValue(preeliminar.getCodigoSolicitudEva());
            ActualizarLongitudColumna (20, preeliminar.getCodigoSolicitudEva() != null ?  preeliminar.getCodigoSolicitudEva().toString().length():null,sheet);
            
            userRow.createCell(21).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaAprobSolicitudEva()));
            ActualizarLongitudColumna (21, preeliminar.getFechaAprobSolicitudEva() != null ?  preeliminar.getFechaAprobSolicitudEva().toString().length():0,sheet);
            
            userRow.createCell(22).setCellValue(preeliminar.getEstadoSolicitudEva());
            ActualizarLongitudColumna (22, preeliminar.getEstadoSolicitudEva() != null ?  preeliminar.getEstadoSolicitudEva().toString().length():0,sheet);
            
            UsuarioBean usuarioBean = null; 
            if ( listaUsuarios != null)
            	usuarioBean=listaUsuarios.stream().filter(y -> y.getCodUsuario().toString().equals(preeliminar.getAutorizadorPertinenciaSolicitudEva())).findFirst()
                    .orElse(null);
            
            //userRow.createCell(23).setCellValue(preeliminar.getAutorizadorPertinenciaSolicitudEva());
            //ActualizarLongitudColumna (23, preeliminar.getAutorizadorPertinenciaSolicitudEva() != null ?  preeliminar.getAutorizadorPertinenciaSolicitudEva().toString().length():0,sheet);
            
            userRow.createCell(23).setCellValue(usuarioBean != null ? usuarioBean.getNombreApellido() :preeliminar.getAutorizadorPertinenciaSolicitudEva().toString());
            ActualizarLongitudColumna (23, preeliminar.getAutorizadorPertinenciaSolicitudEva() != null ?  preeliminar.getAutorizadorPertinenciaSolicitudEva().toString().length():0, sheet);
            
            userRow.createCell(24).setCellValue(preeliminar.getCodigoSCGSolben());
            ActualizarLongitudColumna (24, preeliminar.getCodigoSCGSolben() != null ?  preeliminar.getCodigoSCGSolben().toString().length():null,sheet);
            
            userRow.createCell(25).setCellValue(preeliminar.getNroCartaGarantia());
            ActualizarLongitudColumna (25, preeliminar.getNroCartaGarantia() != null ?  preeliminar.getNroCartaGarantia().toString().length():null,sheet);
            
            userRow.createCell(26).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaIniTratamiento()));
            ActualizarLongitudColumna (26, preeliminar.getFechaIniTratamiento() != null ?  preeliminar.getFechaIniTratamiento().toString().length():0,sheet);
            
            userRow.createCell(27).setCellValue(preeliminar.getNroEvolucion());
            ActualizarLongitudColumna (27, preeliminar.getNroEvolucion() != null ?  preeliminar.getNroEvolucion().toString().length():null,sheet);
            
            UsuarioBean usuarioBeanMonitoreo = null; 
            if ( listaUsuarios != null)
            	usuarioBeanMonitoreo=listaUsuarios.stream().filter(x -> x.getCodUsuario().equals(preeliminar.getCodigoResponsableMonitoreo())).findFirst()
                    .orElse(null);
            
            userRow.createCell(28).setCellValue(usuarioBeanMonitoreo != null ? usuarioBeanMonitoreo.getNombreApellido() :null);
            ActualizarLongitudColumna (28, usuarioBeanMonitoreo != null ?  usuarioBeanMonitoreo.getNombreApellido().toString().length():0,sheet);
            
            //String usuarioSolben = buscarUsuarioSolben( Integer.parseInt(preeliminar.getAutorizadorPertinenciaSolicitudPre()),headers);
            userRow.createCell(29).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaProgMonitoreo()));
            ActualizarLongitudColumna (29, preeliminar.getFechaProgMonitoreo() != null ?  preeliminar.getFechaProgMonitoreo().toString().length():0,sheet);
            
            userRow.createCell(30).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaRealMonitoreo()));
            ActualizarLongitudColumna (30, preeliminar.getFechaRealMonitoreo() != null ?  preeliminar.getFechaRealMonitoreo().toString().length():0,sheet);
            
            userRow.createCell(31).setCellValue(preeliminar.getEstadoMonitoreo());
            ActualizarLongitudColumna (31, preeliminar.getEstadoMonitoreo() != null ?  preeliminar.getEstadoMonitoreo().toString().length():0,sheet);
            
            userRow.createCell(32).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaProgMonitoreo()));
            ActualizarLongitudColumna (32, preeliminar.getFechaProgMonitoreo() != null ?  preeliminar.getFechaProgMonitoreo().toString().length():0,sheet);
            
            userRow.createCell(33).setCellValue(preeliminar.getTolerancia());
            ActualizarLongitudColumna (33, preeliminar.getTolerancia() != null ?  preeliminar.getTolerancia().toString().length():0,sheet);
            
            userRow.createCell(34).setCellValue(preeliminar.getToxicidad());
            ActualizarLongitudColumna (34, preeliminar.getToxicidad() != null ?  preeliminar.getToxicidad().toString().length():0,sheet);
            
            userRow.createCell(35).setCellValue(preeliminar.getRespuestaClinica());
            ActualizarLongitudColumna (35, preeliminar.getRespuestaClinica() != null ?  preeliminar.getRespuestaClinica().toString().length():0,sheet);
            
            userRow.createCell(36).setCellValue(preeliminar.getAtencionAlertas());
            ActualizarLongitudColumna (36, preeliminar.getAtencionAlertas() != null ?  preeliminar.getAtencionAlertas().toString().length():0,sheet);
            
            userRow.createCell(37).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaUltimoConsumo()));
            ActualizarLongitudColumna (37, preeliminar.getFechaUltimoConsumo() != null ?  preeliminar.getFechaUltimoConsumo().toString().length():0,sheet);
            
            userRow.createCell(38).setCellValue(preeliminar.getCantidadUltimoConsumo());
            ActualizarLongitudColumna (38, preeliminar.getCantidadUltimoConsumo() != null ?  preeliminar.getCantidadUltimoConsumo().toString().length():0,sheet);
            
            userRow.createCell(39).setCellValue(preeliminar.getResultadoEvolucion());
            ActualizarLongitudColumna (39, preeliminar.getResultadoEvolucion() != null ?  preeliminar.getResultadoEvolucion().toString().length():0,sheet);
               
            userRow.createCell(40).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaInactivacion()));
            ActualizarLongitudColumna (40, preeliminar.getFechaInactivacion() != null ?  preeliminar.getFechaInactivacion().toString().length():0,sheet);
            
            userRow.createCell(41).setCellValue( preeliminar.getMotivoInactivacion());
            ActualizarLongitudColumna (41, preeliminar.getMotivoInactivacion() != null ?  preeliminar.getMotivoInactivacion().toString().length():0,sheet);
            
            userRow.createCell(42).setCellValue(preeliminar.getEjecutivoMonitoreo());
            ActualizarLongitudColumna (42, preeliminar.getEjecutivoMonitoreo() != null ?  preeliminar.getEjecutivoMonitoreo().toString().length():0,sheet);
            
            userRow.createCell(43).setCellValue(preeliminar.getEstadoMonitoreo());
            ActualizarLongitudColumna (43, preeliminar.getEstadoMonitoreo() != null ?  preeliminar.getEstadoMonitoreo().toString().length():0,sheet);
            
            userRow.createCell(44).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaRegistroSE()));
            ActualizarLongitudColumna (45, preeliminar.getFechaRegistroSE() != null ?  preeliminar.getFechaRegistroSE().toString().length():0,sheet);
            
           
            
            rowCount++;
            
        }
        
        
     

        try {
        	
        	ByteArrayOutputStream baos = new ByteArrayOutputStream();
        	workbook.write(baos);
        	byte[] xls = baos.toByteArray();
        	String pathFileByte = configFTPProp.getRutaTemp()+File.separator  + "ReporteSolicitudesMonitoreo" + ".xlsx";
            File fileByte = new File(pathFileByte);
        	 OutputStream   os  = new FileOutputStream(fileByte); 

	         // Starts writing the bytes in it 
	         os.write(xls); 
        	 os.close();
        	 
            FileOutputStream outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            workbook.close();

            responseBody.setResponse(xls);
            responseBody.setCodResultado(0);
            responseBody.setMsgResultado("Indicador en Excel Generado");
            
            //responseBody.setName(file.getName());
            //responseBody.setLength(file.length());
            //responseBody.setUrl(configFTPProp.getRutaTemp()+File.separator  + file.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        //responseBody.setUrl(pathFile);
		
		return responseBody;
	}

	@Override
	public ApiOutResponse generarReporteGeneralesMonitoresDatos(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		ApiOutResponse responseBody = new ApiOutResponse();
		Map<String, Object> out = new HashMap<>();

		try {
			/** Call store procedure */
			log.info("Call store procedure : SP_PFC_I_REP_SOL_MONITOR ");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_REPORTE_INDICADORES)
					.withProcedureName("SP_PFC_I_REP_SOL_MONITOR");

			/** Asignar los parameters input a la consulta */
			log.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_FEC_MES", requestRequest.getMes(), Types.NUMERIC);
			in.addValue("PN_FEC_ANO", requestRequest.getAno(), Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			
			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			} else {
				responseBody.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
				responseBody.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("Error al listar : generarReporteGeneralesMonitoresDatos ", e);
		}
		
		
		return responseBody;

	}
	
}
