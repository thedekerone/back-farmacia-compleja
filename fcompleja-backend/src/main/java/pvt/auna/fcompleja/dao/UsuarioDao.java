package pvt.auna.fcompleja.dao;

import java.util.List;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.ParticipanteBeanRequest;
import pvt.auna.fcompleja.model.api.request.ValidarRegParticipantRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.ParticipanteRequest;
import pvt.auna.fcompleja.model.api.response.ResponseGenericoObject;
import pvt.auna.fcompleja.model.bean.CasosEvaCmacBean;
import pvt.auna.fcompleja.model.bean.UsuarioBean;

public interface UsuarioDao {

	List<UsuarioBean> listRolPersona(int codRol) throws Exception;
	
	List<UsuarioBean> listarRolPersonaGrupoDiag(String codSolEva, int codRol) throws Exception;

	List<UsuarioBean> listarRolMonitoreo(int codRol) throws Exception;

	List<UsuarioBean> listarRolAutorizadorCmac(int codRol,String codDescSolEva) throws Exception;

	List<CasosEvaCmacBean> listarCasosEvaCmac(int codEstadoProgCmac) throws Exception;
	
	ApiOutResponse listarUsuarioFarmacia(ParticipanteRequest participante);

	ApiOutResponse listarUsuarioFarmaciaDet(ParticipanteRequest participante);
	
	ApiOutResponse listarParticipantes(ParticipanteBeanRequest participante);
	
	ApiOutResponse registrarParticipantes(ParticipanteBeanRequest participante);

	ResponseGenericoObject validarRegistroParticipante (ValidarRegParticipantRequest request);

	
}
