package pvt.auna.fcompleja.dao.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.dao.UsuarioDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.request.ParticipanteBeanRequest;
import pvt.auna.fcompleja.model.api.request.ParticipanteDetalleBeanRequest;
import pvt.auna.fcompleja.model.api.request.UsuarioRolRequest;
import pvt.auna.fcompleja.model.api.request.ValidarRegParticipantRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.ParticipanteRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.BandejaMonitoreoRequest;
import pvt.auna.fcompleja.model.api.response.ResponseGenericoObject;
import pvt.auna.fcompleja.model.api.response.evaluacion.ParticipanteResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoResponse;
import pvt.auna.fcompleja.model.bean.CasosEvaCmacBean;
import pvt.auna.fcompleja.model.bean.ExamenMedicoDetalleBean;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.model.bean.UsuarioRolBean;
import pvt.auna.fcompleja.model.mapper.CasosEvaCmacMapper;
import pvt.auna.fcompleja.model.mapper.ExamenMedicoDetalleRowMapper;
import pvt.auna.fcompleja.model.mapper.ParticipanteDetalleBeanRowMapper;
import pvt.auna.fcompleja.model.mapper.ParticipanteRequestRowMapper;
import pvt.auna.fcompleja.model.mapper.UsuarioRolRowMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.ParticipanteMapper;
import pvt.auna.fcompleja.model.mapper.evaluacion.ParticipanteUsuarioRowMapper;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@Repository
public class UsuarioDaoImpl implements UsuarioDao{
	
	private final Logger LOGGER = Logger.getLogger(getClass());

	@Qualifier("dataSource")
	@Autowired
	DataSource dataSource;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UsuarioBean> listRolPersona(int codRol) throws Exception {
		
		List<UsuarioBean> lista = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		String msgResultado;
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA_SEGURIDAD).
					withCatalogName(ConstanteUtil.PAQUETE_SEGURIDAD_USUARIO).withProcedureName("SP_PPA_S_ROL_PERSONA").
					returningResultSet("TC_LIST", new UsuarioRolRowMapper());
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_CODROL",codRol, Types.NUMERIC);
			out = simpleJdbcCall.execute(in);
			lista = (List<UsuarioBean>) out.get("TC_LIST");
			int codResultado = ((BigDecimal)out.get("PN_COD_RESULTADO")).intValue();
			msgResultado = (String)out.get("PV_MSG_RESULTADO");

		} catch (Exception e) {
			LOGGER.error("Error al consultar", e);
		}
		return lista;
	}

	@Override
	public List<UsuarioBean> listarRolPersonaGrupoDiag(String codSolEva, int codRol) throws Exception {
		List<UsuarioBean> lista = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		String msgResultado;
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).
					withSchemaName(ConstanteUtil.ESQUEMA).
					withCatalogName(ConstanteUtil.PAQUETE_EVALUACION).
					withProcedureName("SP_PFC_S_EVA_ROL_LIDER_TUMOR").
					returningResultSet("TC_LIST", new UsuarioRolRowMapper());

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_ROL",codRol, Types.NUMERIC);
			in.addValue("PN_COD_DESC_SOL_EVA",codSolEva, Types.VARCHAR);
			out = simpleJdbcCall.execute(in);
			lista = (List<UsuarioBean>) out.get("TC_LIST");
			int codResultado = ((BigDecimal)out.get("PN_COD_RESULTADO")).intValue();
			msgResultado = (String)out.get("PV_MSG_RESULTADO");

		} catch (Exception e) {
			LOGGER.error("Error al consultar", e);
		}
		return lista;
	}

	@Override
	public List<UsuarioBean> listarRolMonitoreo(int codRol) throws Exception {
		List<UsuarioBean> lista = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		String msgResultado;
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).
					withSchemaName(ConstanteUtil.ESQUEMA).
					withCatalogName(ConstanteUtil.PAQUETE_PRELIMINAR).
					withProcedureName("SP_PFC_S_ROL_PERSONA").
					returningResultSet("TC_LIST", new UsuarioRolRowMapper());

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_CODROL",codRol, Types.NUMERIC);
			out = simpleJdbcCall.execute(in);
			lista = (List<UsuarioBean>) out.get("TC_LIST");
			int codResultado = ((BigDecimal)out.get("PN_COD_RESULTADO")).intValue();
			msgResultado = (String)out.get("PV_MSG_RESULTADO");

		} catch (Exception e) {
			LOGGER.error("Error al consultar", e);
		}
		return lista;
	}

	
	@Override
	public List<UsuarioBean> listarRolAutorizadorCmac(int codRol ,String codDescSolEva) throws Exception {
		List<UsuarioBean> lista = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		String msgResultado;
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).
					withSchemaName(ConstanteUtil.ESQUEMA).
					withCatalogName(ConstanteUtil.PAQUETE_EVALUACION).
							withProcedureName("SP_PFC_S_EVA_ROL_CMAC").
							returningResultSet("TC_LIST", new UsuarioRolRowMapper());

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_ROL",codRol, Types.NUMERIC);
			in.addValue("PN_COD_DESC_SOL_EVA",codDescSolEva, Types.NUMERIC);
			out = simpleJdbcCall.execute(in);
			lista = (List<UsuarioBean>) out.get("TC_LIST");
			int codResultado = ((BigDecimal)out.get("PN_COD_RESULTADO")).intValue();
			msgResultado = (String)out.get("PV_MSG_RESULTADO");

		} catch (Exception e) {
			LOGGER.error("Error al consultar", e);
		}
		return lista;
	}


	@Override
	public List<CasosEvaCmacBean> listarCasosEvaCmac(int codEstadoProgCmac) throws Exception {
		List<CasosEvaCmacBean> lista = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		String msgResultado;
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).
					withSchemaName(ConstanteUtil.ESQUEMA).
					withCatalogName(ConstanteUtil.PAQUETE_EVALUACION).
							withProcedureName("SP_PFC_S_CASOS_EVALUACION_CMAC").
							returningResultSet("TC_LIST", new CasosEvaCmacMapper());

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_P_ESTADO_EVA",codEstadoProgCmac, Types.NUMERIC);
			out = simpleJdbcCall.execute(in);
			lista = (List<CasosEvaCmacBean>) out.get("TC_LIST");
			int codResultado = ((BigDecimal)out.get("PN_COD_RESULTADO")).intValue();
			msgResultado = (String)out.get("PV_MSG_RESULTADO");

		} catch (Exception e) {
			LOGGER.error("Error al consultar", e);
		}
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse listarUsuarioFarmacia(ParticipanteRequest participante) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<ParticipanteResponse> lista = new ArrayList<>();

		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_PARTICIPANTE")
					.returningResultSet("AC_LISTA", new ParticipanteMapper());

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_ROL", participante.getCodRol(), Types.NUMERIC);
			in.addValue("PN_COD_USUARIO", participante.getCodUsuario(), Types.NUMERIC);
			in.addValue("PN_COD_PARTICIPANTE", participante.getCodParticipante(), Types.NUMERIC);
			
			
			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			lista = (List<ParticipanteResponse>) out.get("AC_LISTA");
			outResponse.setResponse(lista);

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Error al listar : ", e);
		}
		return outResponse;
	}

	@Override
	public ApiOutResponse listarUsuarioFarmaciaDet(ParticipanteRequest participante) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<ParticipanteResponse> lista = new ArrayList<>();

		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_PARTICIPANTE_DET")
					.returningResultSet("AC_LISTA", new ParticipanteUsuarioRowMapper());

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_ROL", participante.getCodRol(), Types.NUMERIC);
			in.addValue("PV_COD_GRP_DIAG", participante.getCodGrpDiag(),Types.VARCHAR);
			in.addValue("PN_P_RANGO_EDAD", participante.getpRangoEdad(), Types.NUMERIC);
			in.addValue("PN_COD_USUARIO", participante.getCodUsuario(),Types.NUMERIC);
			in.addValue("PN_COD_PARTICIPANTE", participante.getCodParticipante(),Types.NUMERIC);
            
			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			lista = (List<ParticipanteResponse>) out.get("AC_LISTA");
			outResponse.setResponse(lista);

			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Error al listar : ", e);
		}
		return outResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse listarParticipantes(ParticipanteBeanRequest participante) {

		List<ParticipanteBeanRequest> lista = new ArrayList<>();
		List<ParticipanteDetalleBeanRequest> listaDetalle = new ArrayList<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		Map<String, Object> out = new HashMap<>();
		String msgResultado;
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).
					withSchemaName(ConstanteUtil.ESQUEMA).
					withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").
							withProcedureName("SP_PFC_S_LISTAR_PARTICIPANTES").
							returningResultSet("TC_LIST", new ParticipanteRequestRowMapper())
							.returningResultSet("TC_LIST_GRP", new ParticipanteDetalleBeanRowMapper());

			MapSqlParameterSource in = new MapSqlParameterSource();
			
			String apellidosNombres = "";
			Integer codigoUsuario = null;
			
			if (participante.getApellidos() != null ) {
				apellidosNombres = participante.getApellidos();
			}
			
			if (participante.getCodUsuario() != null ) {
				codigoUsuario = participante.getCodUsuario();
			}
			
			
			in.addValue("PV_APELLIDO_NOMBRE",apellidosNombres, Types.VARCHAR);
			in.addValue("PN_COD_USUARIO",codigoUsuario, Types.NUMERIC);
			out = simpleJdbcCall.execute(in);
			lista = (List<ParticipanteBeanRequest>) out.get("TC_LIST");
			listaDetalle = (List<ParticipanteDetalleBeanRequest>) out.get("TC_LIST_GRP");
			
			for ( ParticipanteBeanRequest par: lista) {
				
				List<ParticipanteDetalleBeanRequest> listaDetalleTemp = new ArrayList<>();
				for ( ParticipanteDetalleBeanRequest parDetalle: listaDetalle) {
					if ( par.getCodParticipante() == parDetalle.getCodParticipante()) {
						listaDetalleTemp.add(parDetalle);
					}
				}
				if ( listaDetalleTemp.size() > 0)
				  par.setGruposDiagnosticos(listaDetalleTemp);
			}
			
			
			//sdfsd
			
			
			outResponse.setResponse(lista);
			
			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			LOGGER.error("Error al consultar", e);
		}
		return outResponse;
	}

	@Override
	public ApiOutResponse registrarParticipantes(ParticipanteBeanRequest participante) {
		// TODO Auto-generated method stub
		ApiOutResponse apiOut = new ApiOutResponse();

		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_I_U_PARTICIPANTES");

		try {
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_PARTICIPANTE", participante.getCodParticipante(), Types.NUMERIC);
			in.addValue("PN_COD_USUARIO", participante.getCodUsuario(), Types.NUMERIC);
			in.addValue("PV_ESTADO_PARTICIPANTE", participante.getEstadoParticipante(), Types.VARCHAR);
			in.addValue("PV_CMP_MEDICO", participante.getCmpMedico(), Types.VARCHAR);
			in.addValue("PV_NOMBRE_FIRMA", participante.getNombreFirma(), Types.VARCHAR);
			in.addValue("PN_COD_ROL", participante.getCodRol(), Types.NUMERIC);
			in.addValue("PN_COD_ARCHIVO_FIRMA", participante.getCodigoArchivoFirma(), Types.NUMERIC);
			in.addValue("PV_CORREO_ELECTRONICO", participante.getCorreoElectronico(), Types.VARCHAR);
			in.addValue("PV_APELLIDOS", participante.getApellidos(), Types.VARCHAR);
			in.addValue("PV_NOMBRES", participante.getNombres(), Types.VARCHAR);
			in.addValue("PN_P_ESTADO", participante.getpEstado(), Types.NUMERIC);
			in.addValue("PN_COORDINADOR", participante.getCoordinador(), Types.NUMERIC);
			
			
			out = simpleJdbcCall.execute(in);

			apiOut.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			apiOut.setMsgResultado((String) out.get("PV_MSG_RESULTADO"));

			if (apiOut.getCodResultado() != 0) {
				LOGGER.error(apiOut.getMsgResultado());
				throw new Exception(apiOut.getMsgResultado());
			}

			participante.setCodParticipante(Integer.parseInt(out.get("PN_CODIGO_PARTICIPANTE").toString()));

			apiOut.setResponse(participante);
			
			/*Elimina Examen Medico Detalle */
			eliminarParticipanteDetalle(participante.getCodParticipante());

			/* Guardar Detalle Participante -> Grupo Diagnostico */

			guardarParticipanteGrupoDiagnostico(participante);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
		}

		return apiOut;
	}
	
	
	private void eliminarParticipanteDetalle ( Integer codParticipante) {
		
		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_D_GRUPO_PARTICPANTES");

		try {
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_PARTICIPANTE", codParticipante, Types.NUMERIC);
			
			out = simpleJdbcCall.execute(in);

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) != 0) {
				LOGGER.error((String) out.get("PV_MSG_RESULTADO"));
				throw new Exception((String) out.get("PV_MSG_RESULTADO"));
			}

			LOGGER.info((String) out.get("PV_MSG_RESULTADO"));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());

		}
		
	}
	
	private void guardarParticipanteGrupoDiagnostico (ParticipanteBeanRequest participante ) {
		Map<String, Object> out = new HashMap<>();

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA").withProcedureName("SP_PFC_I_U_PARTICIPANTES_GRUPO");

		try {

			for (ParticipanteDetalleBeanRequest detalleGrupo : participante.getGruposDiagnosticos()) {

				MapSqlParameterSource in = new MapSqlParameterSource();
				in.addValue("PN_COD_PARTICIPANTE", participante.getCodParticipante(), Types.NUMERIC);
				in.addValue("PV_COD_GRP_DIAG", detalleGrupo.getCodGrupoDiagnostico(), Types.VARCHAR);
				in.addValue("PN_P_RANGO_EDAD", detalleGrupo.getpRangoEdad(), Types.NUMERIC);
				

				out = simpleJdbcCall.execute(in);

				if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) != 0) {
					LOGGER.error((String) out.get("PV_MSG_RESULTADO"));
					throw new Exception((String) out.get("PV_MSG_RESULTADO"));
				}

				LOGGER.info((String) out.get("PV_MSG_RESULTADO"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());

		}

	}

	public ResponseGenericoObject validarRegistroParticipante (ValidarRegParticipantRequest request) {

		Map<String, Object> out;
		ResponseGenericoObject response=new ResponseGenericoObject();
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource)
				.withSchemaName(ConstanteUtil.ESQUEMA)
				.withCatalogName("PCK_PFC_CONFIGURACION_SISTEMA")
				.withProcedureName("SP_PFC_S_VALIDAR_R_PARTICIPA");

		try {

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_GRUPO_DIAG", request.getCodGrupoDiagnostico(), Types.NUMERIC);
			in.addValue("PN_COD_RANGO_EDAD", request.getCodRangoEdad(), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			response.setData(null);
			AudiResponse audiResponse=new AudiResponse();
			audiResponse.setCodigoRespuesta((out.get("PN_COD_RESULTADO")).toString());
			audiResponse.setMensajeRespuesta((String) out.get("PV_MSG_RESULTADO"));
			response.setAudiResponse(audiResponse);

		} catch (Exception e) {
			LOGGER.error(e);
			response.setData(null);
			AudiResponse audiResponse=new AudiResponse();
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Se produjo un error al validar registro participante.");
			response.setAudiResponse(audiResponse);
		}

		return response;
	}
	
}
