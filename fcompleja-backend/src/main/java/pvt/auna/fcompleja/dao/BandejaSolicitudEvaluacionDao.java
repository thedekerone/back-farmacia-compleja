package pvt.auna.fcompleja.dao;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.EmailAlertaCmacRequest;
import pvt.auna.fcompleja.model.api.request.ListaBandejaRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.request.registrarEvaluacionCmac;
import pvt.auna.fcompleja.model.api.request.evaluacion.ProgramacionCmacRequest;

public interface BandejaSolicitudEvaluacionDao {

	public ApiOutResponse BandejaEvaluacion(ListaBandejaRequest objA_evaluacion) throws Exception ;
	
	public ApiOutResponse consultarEvaluacionXFecha(EmailAlertaCmacRequest objA_evaluacion);
	
	public ApiOutResponse registrarEvaluacionCmac(registrarEvaluacionCmac objA_evaluacion);
	
	public WsResponse eliminarRegEvaluacionCmac(SolicitudEvaluacionRequest objA_evaluacion);
	
	public ApiOutResponse EvaluacionXCodigo(ListaBandejaRequest objA_evaluacion);
	
	public ApiOutResponse consultarEvaluacionXCodigo(EmailAlertaCmacRequest objA_evaluacion);
	
	public ApiOutResponse actualizarActaEscanProgramacionCmac(ProgramacionCmacRequest request);
	
}
