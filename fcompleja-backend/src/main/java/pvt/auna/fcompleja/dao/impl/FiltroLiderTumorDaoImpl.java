package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pvt.auna.fcompleja.dao.FiltroLiderTumorDao;
import pvt.auna.fcompleja.model.api.FiltroLiderTumorRequest;
import pvt.auna.fcompleja.model.api.FiltroLiderTumorResponse;
import pvt.auna.fcompleja.model.api.ListFiltroLiderTumorResponse;
import pvt.auna.fcompleja.model.mapper.ListFiltroLiderTumorRowMapper;
import pvt.auna.fcompleja.util.ConstanteUtil;

@Repository
public class FiltroLiderTumorDaoImpl implements FiltroLiderTumorDao {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	DataSource datasource;

	@SuppressWarnings("unchecked")
	@Override
	public FiltroLiderTumorResponse FiltroLiderTumor(FiltroLiderTumorRequest ListaTumor) throws Exception {

		FiltroLiderTumorResponse lista = new FiltroLiderTumorResponse();
		List<ListFiltroLiderTumorResponse> listaFiltro = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(datasource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION).withProcedureName("SP_S_FILTRO_LIDER_TUMOR")
					.returningResultSet("AC_LISTA_LI_TUMOR", new ListFiltroLiderTumorRowMapper());

			MapSqlParameterSource in = new MapSqlParameterSource();

			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PN_ROL", Types.VARCHAR));
			in.addValue("PN_ROL", ListaTumor.getRolLiderTumor());

			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("AC_LISTA_LI_TUMOR", OracleTypes.CURSOR,
					new BeanPropertyRowMapper<FiltroLiderTumorResponse>(FiltroLiderTumorResponse.class)));
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PN_COD_RESULTADO", Types.NUMERIC));
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PV_MSG_RESULTADO", Types.VARCHAR));

			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			listaFiltro = (List<ListFiltroLiderTumorResponse>) out.get("AC_LISTA_LI_TUMOR");

			lista.setFiltroLiderTumor(listaFiltro);
			LOGGER.info(out.get("PN_COD_RESULTADO"));
			LOGGER.info(out.get("PV_MSG_RESULTADO"));

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

			LOGGER.info(listaFiltro.size());

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("Error al consultar ---> listar Lider tumor", e);
		}
		// TODO Auto-generated method stub
		return lista;
	}

}
