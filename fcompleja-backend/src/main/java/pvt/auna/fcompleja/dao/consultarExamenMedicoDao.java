package pvt.auna.fcompleja.dao;

import pvt.auna.fcompleja.model.api.request.ListaExamenMedicoRequest;
import pvt.auna.fcompleja.model.api.response.ListaExamenMedicoResponse;

public interface consultarExamenMedicoDao {

	ListaExamenMedicoResponse consultarExamenMedico(ListaExamenMedicoRequest listaExamenMed) throws Exception;
}
