package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pvt.auna.fcompleja.dao.FiltroUsrRolDao;
import pvt.auna.fcompleja.model.api.FiltroParametroResponse;
import pvt.auna.fcompleja.model.api.FiltroUsrRolRequest;
import pvt.auna.fcompleja.model.api.FiltroUsrRolResponse;
import pvt.auna.fcompleja.model.api.ListFiltroRolResponse;
import pvt.auna.fcompleja.model.mapper.FiltroUsrRolRowMapper;
import pvt.auna.fcompleja.util.ConstanteUtil;

@Repository
public class FiltroUsrRolDaoImpl implements FiltroUsrRolDao {

private final Logger LOGGER = Logger.getLogger(getClass());
	
	@Autowired
	DataSource datasource;
	
	@SuppressWarnings("unchecked")
	@Override
	public FiltroUsrRolResponse FiltroUsuarioRol(FiltroUsrRolRequest ListaUsuario) throws Exception {
		// TODO Auto-generated method stub
		FiltroUsrRolResponse listaUsuarioR = new FiltroUsrRolResponse();
		List<ListFiltroRolResponse> listListaUsuarioR = new ArrayList<>();
		Map<String,Object> out = new HashMap<>();
		
		try {
			SimpleJdbcCall simpleJdbcCall =  new SimpleJdbcCall(datasource).withSchemaName(ConstanteUtil.ESQUEMA).withCatalogName(ConstanteUtil.PAQUETE_UTIL)
					.withProcedureName("SP_S_FILTRO_USR_ROL")
					.returningResultSet("AC_LISTA", new FiltroUsrRolRowMapper());
			
			MapSqlParameterSource in = new MapSqlParameterSource();
			
			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PN_CODIGO", Types.VARCHAR));
			in.addValue("PN_CODIGO",ListaUsuario.getCodigoRol());
			
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("AC_LISTA", OracleTypes.CURSOR, new BeanPropertyRowMapper<FiltroParametroResponse>(FiltroParametroResponse.class)));
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PN_COD_RESULTADO", Types.NUMERIC));
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PV_MSG_RESULTADO", Types.VARCHAR));
			
			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			listListaUsuarioR 	= (List<ListFiltroRolResponse>)out.get("AC_LISTA");
			
			listaUsuarioR.setListaRol(listListaUsuarioR);
			
			listaUsuarioR.setCodigoResultado(Long.parseLong(out.get("PN_COD_RESULTADO").toString()));
			listaUsuarioR.setMensajeResultado(out.get("PV_MSG_RESULTADO").toString());

			LOGGER.info(out.get("PN_COD_RESULTADO"));
			 LOGGER.info(out.get("PV_MSG_RESULTADO"));
			 
			 if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1){
	                throw new Exception(out.get("PV_MSG_RESULTADO").toString());
	            }

			 LOGGER.info(listListaUsuarioR.size());
			 LOGGER.info("TRAE DATA " + listListaUsuarioR.size());
			 
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("Error al consultar ---> listar Usuario Rol", e);
		}
		
	return listaUsuarioR;
	}

}
