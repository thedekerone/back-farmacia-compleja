package pvt.auna.fcompleja.dao.impl;

import java.math.BigInteger;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.dao.AuditoriaDao;
import pvt.auna.fcompleja.util.ConstanteUtil;

@Repository
public class AuditoriaDaoImpl implements AuditoriaDao {

	private final Logger LOGGER = Logger.getLogger(getClass());;
	@SuppressWarnings("unused")
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	DataSource dataSource;

	@PostConstruct
	public void init() {
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public Integer registrarErrorDmlLogerThread(Integer codProcess, Integer idNumEvento,
			Integer idNumLog, String nombreSp, String lmensajeError, String tipoProcess) {

		Thread one = new Thread() {
			public void run() {
				try {
					registrarErrorDmlLoger(idNumEvento, idNumLog, nombreSp, lmensajeError,
							codProcess, tipoProcess);
					Thread.sleep(1000);
				} catch (Exception e) {
					LOGGER.error(e.getMessage());
					LOGGER.error("[AuditoriaDAOImpl] Error al registrarErrorDmlLogerThread", e);
				}
			}

		};
		one.start();
		// }
		return BigInteger.ONE.intValue();
	}

	@SuppressWarnings("unused")
	private void registrarErrorDmlLoger(Integer idNumEvento, Integer idNumLog, String nombreSp,
			String lmensajeError, Integer codProcess, String tipoProcess) {
		Map<String, Object> out = new HashMap<>();
		String erroMensaje = "";
		// inicio [acaceres -06-11-2018] Contador de reintentos para auditoria
		if (lmensajeError != null && lmensajeError.length() > 4000) {
			erroMensaje = lmensajeError.substring(0, 4000);
		} else {
			erroMensaje = lmensajeError;
		}
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource)
					.withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_AUDIT)
					.withProcedureName("SP_INS_LOGERROR");
			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("ip_id_num_eve", idNumEvento, Types.NUMERIC);
			in.addValue("ip_id_num_log", idNumLog, Types.NUMERIC);
			in.addValue("ip_nombresp_vch", nombreSp, Types.VARCHAR);
			in.addValue("ip_mensaje_vch", erroMensaje, Types.VARCHAR);
			in.addValue("ip_tip_audit_vch", tipoProcess, Types.VARCHAR);
			in.addValue("ip_cod_num_status", codProcess, Types.VARCHAR);
			out = simpleJdbcCall.execute(in);

			LOGGER.info(" [AuditoriaDAOImpl] Error al registrarErrorDmlLoger [idNumEvento = "
					+ idNumEvento + ", idNumLog= " + idNumLog + ", nombreSp = " + nombreSp
					+ ", lmensajeError = " + lmensajeError + ", codProcess = " + codProcess
					+ ", tipoProcess = " + tipoProcess + " ]");

		} catch (Exception e) {
			LOGGER.error(" [AuditoriaDAOImpl] Error al registrarErrorDmlLoger [idNumEvento = "
					+ idNumEvento + ", idNumLog= " + idNumLog + ", nombreSp = " + nombreSp
					+ ", lmensajeError = " + lmensajeError + ", codProcess = " + codProcess
					+ ", tipoProcess = " + tipoProcess + " ]");

			LOGGER.error("[AuditoriaDAOImpl] Error al registrarErrorDmlLoger", e);
			throw e;
		}
	}

}
