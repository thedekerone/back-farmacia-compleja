package pvt.auna.fcompleja.dao.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.dao.SolbenDao;
import pvt.auna.fcompleja.model.api.request.SolbenWsRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudPreliminarRequest;
import pvt.auna.fcompleja.model.api.response.SolbenWsResponse;
import pvt.auna.fcompleja.model.api.response.SolicitudPreliminarResponse;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;

@Repository
public class SolbenDaoImpl implements SolbenDao {
	private static final Logger log = LoggerFactory.getLogger(SolbenDaoImpl.class);

	@Qualifier("dataSource")
	@Autowired
	private DataSource dataSource;

	@Override
	public SolicitudPreliminarResponse getScgSolbenAndInsertSolPreliminar(SolicitudPreliminarRequest request) {

		Map<String, Object> out = new HashMap<>();

		SolicitudPreliminarResponse response = new SolicitudPreliminarResponse();
		Date date = new Date();

		try {
			log.info("--Llama al SP 'SP_GET_SCG_AND_INSERT_PREL'  y pasa los parametros--");

			/** Call store procedure */
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_PRELIMINAR).withProcedureName("SP_PFC_SI_SCG_SOLBEN_PREL");

			/** Llamar a los parametros de entrada del store procedure */
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PV_COD_SCG_SOLBEN", request.getCod_scg_solben(), Types.VARCHAR);
			in.addValue("PV_COD_CLINICA", request.getCod_clinica(), Types.VARCHAR);
			in.addValue("PV_COD_AFI_PACIENTE", request.getCod_afi_paciente(), Types.VARCHAR);
			in.addValue("PN_EDAD_PACIENTE", request.getEdad_paciente(), Types.NUMERIC);
			in.addValue("PV_DES_CONTRATANTE", request.getDes_contratante(), Types.VARCHAR);
			in.addValue("PV_DES_PLAN", request.getDes_plan(), Types.VARCHAR);
			in.addValue("PV_FEC_AFILIACION", DateUtils.getDateToStringDDMMYYYY(request.getFec_afiliacion()), Types.VARCHAR);
			in.addValue("PV_COD_DIAGNOSTICO", request.getCod_diagnostico(), Types.VARCHAR);
			in.addValue("PV_CMP_MEDICO", request.getCmp_medico(), Types.VARCHAR);
			in.addValue("PV_MEDICO_TRATANTE_PRESCRIPTOR", request.getMedico_tratante_prescriptor(), Types.VARCHAR);
			in.addValue("PV_FEC_RECETA", DateUtils.getDateToStringDDMMYYYY(request.getFec_receta()), Types.VARCHAR);
			in.addValue("PV_FEC_QUIMIO", DateUtils.getDateToStringDDMMYYYY(request.getFec_quimio()), Types.VARCHAR);
			in.addValue("PV_FEC_HOSP_INICIO", DateUtils.getDateToStringDDMMYYYY(request.getFec_hosp_inicio()), Types.VARCHAR);
			in.addValue("PV_FEC_HOSP_FIN", DateUtils.getDateToStringDDMMYYYY(request.getFec_hosp_fin()), Types.VARCHAR);
			in.addValue("PV_FEC_SCG_SOLBEN", DateUtils.getDateToStringDDMMYYYY(request.getFec_scg_solben()), Types.VARCHAR);
			in.addValue("PV_HORA_SCG_SOLBEN", request.getHora_scg_solben(), Types.VARCHAR);
			in.addValue("PV_TIPO_SCG_SOLBEN", request.getTipo_scg_solben(), Types.VARCHAR);
			in.addValue("PV_ESTADO_SCG", request.getEstado_scg(), Types.VARCHAR);
			in.addValue("PV_DESC_MEDICAMENTO", request.getDesc_medicamento(), Types.VARCHAR);
			in.addValue("PV_DESC_ESQUEMA", request.getDesc_esquema(), Types.VARCHAR);
			in.addValue("PN_TOTAL_PRESUPUESTO", request.getTotal_presupuesto(), Types.NUMERIC);
			in.addValue("PV_DESC_PROCEDIMIENTO", request.getDesc_procedimiento(), Types.VARCHAR);
			in.addValue("PV_PERSON_CONTACTO", request.getPerson_contacto(), Types.VARCHAR);
			in.addValue("PV_OBS_CLINICA", request.getObs_clinica(), Types.VARCHAR);
			in.addValue("PN_IND_VALIDA", request.getInd_valida(), Types.NUMERIC);
			in.addValue("PV_TX_DATO_ADIC1", request.getTx_dato_adic1(), Types.VARCHAR);
			in.addValue("PV_TX_DATO_ADIC2", request.getTx_dato_adic2(), Types.VARCHAR);
			in.addValue("PV_TX_DATO_ADIC3", request.getTx_dato_adic3(), Types.VARCHAR);
			in.addValue("PV_COD_GRP_DIAGNOSTICO", request.getCodGrpDiag(), Types.VARCHAR);
			in.addValue("PV_FEC_ACTUAL", DateUtils.getDateToStringDDMMYYYHHMMSS(date), Types.VARCHAR);

			/** Return registros of store procedure for output */
			out = simpleJdbcCall.execute(in);

			Object ref = out.get("PV_OUT_COD_SCG_SOLBEN");
			response.setOp_cod_scg_solben(ref != null ? ref.toString() : null);
			ref = out.get("PN_OUT_COD_SOL_PRE");
			response.setOp_cod_sol_pre((ref != null ? ((BigDecimal) ref).intValue() : 0));
			ref = out.get("PV_OUT_FEC_SOL_PRE");
			response.setOp_fec_sol_pre(ref != null ? ref.toString() : null);
			ref = out.get("PV_OUT_HORA_SOL_PRE");
			response.setOp_hora_sol_pre(ref != null ? ref.toString() : null);

			response.setOp_cod_resultado(((BigDecimal) out.get("PN_OUT_COD_RESULTADO")).intValue());
			response.setOp_msg_resultado((String) out.get("PV_OUT_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_OUT_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_OUT_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("--Error al consultar el 'SP SP_GET_SCG_AND_INSERT_PREL'--", e);
		}

		return response;
	}

	/**
	 * Proposito : Actualizar Estado de la SCG y CG Solben
	 * 
	 * @param request
	 * @return
	 */
	@Override
	public SolbenWsResponse actualizarSolben(SolbenWsRequest request) {

		Map<String, Object> out = new HashMap<>();

		SolbenWsResponse response = new SolbenWsResponse();

		try {
			log.info("Call store procedure");
			log.info("[DAO: ACTUALIZAR ESTADO SCG Y CG SOLBEN][INICIO]");
			log.info(request.toString());
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_UTIL).withProcedureName("SP_PFC_SU_SOLBEN_ACTUALIZAR");

			log.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PV_COD_SCG_SOLBEN", request.getCod_scg_solben(), Types.VARCHAR);
			in.addValue("PV_COD_AFI_PACIENTE", request.getCod_afi_paciente(), Types.VARCHAR);
			in.addValue("PV_COD_ESTADO_SCG", request.getCod_estado_scg(), Types.VARCHAR);
			in.addValue("PV_FEC_ESTADO_SCG", request.getFec_estado_scg(), Types.VARCHAR);
			in.addValue("PV_NRO_CG", request.getNro_cg(), Types.VARCHAR);
			in.addValue("PV_FEC_CG", request.getFec_cg(), Types.VARCHAR);
			in.addValue("PV_TX_DATO_ADIC1", request.getTxt_dato_adic1(), Types.VARCHAR);
			in.addValue("PV_TX_DATO_ADIC2", request.getTxt_dato_adic2(), Types.VARCHAR);
			in.addValue("PV_TX_DATO_ADIC3", request.getTxt_dato_adic3(), Types.VARCHAR);

			out = simpleJdbcCall.execute(in);

			if (out.get("PV_COD_SCG_SOLBEN") != null)
				response.setCod_scg_solben((String) out.get("PV_COD_SCG_SOLBEN"));
			if (out.get("PV_COD_SOL_EVAL") != null)
				response.setCod_sol_eval((String) out.get("PV_COD_SOL_EVAL"));
			if (out.get("PN_COD_RESULTADO") != null)
				response.setCod_rpta(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			if (out.get("PV_MSG_RESULTADO") != null)
				response.setMsg_rpta((String) out.get("PV_MSG_RESULTADO"));
			if (out.get("PV_TX_DATO_ADIC1") != null)
				response.setTxt_dato_adic1((String) out.get("PV_TX_DATO_ADIC1"));
			if (out.get("PV_TX_DATO_ADIC2") != null)
				response.setTxt_dato_adic2((String) out.get("PV_TX_DATO_ADIC2"));
			if (out.get("PV_TX_DATO_ADIC3") != null)
				response.setTxt_dato_adic3((String) out.get("PV_TX_DATO_ADIC3"));

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			log.error("ERROR AL ACTUALIZAR LA SCG Y CG SOLBEN : " + e);
		}
		return response;
	}
}
