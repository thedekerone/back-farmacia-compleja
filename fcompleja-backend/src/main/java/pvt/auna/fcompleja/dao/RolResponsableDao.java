package pvt.auna.fcompleja.dao;

import pvt.auna.fcompleja.model.api.RolResponsableResponse;

public interface RolResponsableDao {

	public RolResponsableResponse RolResponsable() throws Exception;
}
