package pvt.auna.fcompleja.dao;

import pvt.auna.fcompleja.model.api.FiltroLiderTumorRequest;
import pvt.auna.fcompleja.model.api.FiltroLiderTumorResponse;

public interface FiltroLiderTumorDao {

	FiltroLiderTumorResponse FiltroLiderTumor(FiltroLiderTumorRequest ListaTumor) throws Exception;
}
