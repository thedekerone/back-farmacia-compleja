package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pvt.auna.fcompleja.dao.FiltroParametroDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.FiltroParametroRequest;
import pvt.auna.fcompleja.model.api.FiltroParametroResponse;
import pvt.auna.fcompleja.model.api.ListFiltroParametroResponse;
import pvt.auna.fcompleja.model.mapper.ControlFilaParametroRowMapper;
import pvt.auna.fcompleja.model.mapper.ListFiltroParametroRowMapper;
import pvt.auna.fcompleja.util.ConstanteUtil;

@Repository
public class FiltroParametroDaoImpl implements FiltroParametroDao {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	DataSource datasource;

	@SuppressWarnings("unchecked")
	@Override
	public FiltroParametroResponse FiltroParametro(FiltroParametroRequest ListaParametro) throws Exception {
		// TODO Auto-generated method stub
		FiltroParametroResponse listaParametro = new FiltroParametroResponse();
		List<ListFiltroParametroResponse> listListaParametro = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(datasource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_UTIL).withProcedureName("SP_PFC_S_FILTRO_PARAMETRO")
					.returningResultSet("TC_LIST_PARAM", new ListFiltroParametroRowMapper());

			MapSqlParameterSource in = new MapSqlParameterSource();

			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PV_COD_GRUPO", Types.VARCHAR));
			in.addValue("PV_COD_GRUPO", ListaParametro.getCodigoGrupo());

			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("TC_LIST_PARAM", OracleTypes.CURSOR,
					new BeanPropertyRowMapper<FiltroParametroResponse>(FiltroParametroResponse.class)));
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PN_COD_RESULTADO", Types.NUMERIC));
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PV_MSG_RESULTADO", Types.VARCHAR));

			out = (Map<String, Object>) simpleJdbcCall.execute(in);

			LOGGER.info(out.get("PN_COD_RESULTADO"));
			LOGGER.info(out.get("PV_MSG_RESULTADO"));

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

			listListaParametro = (List<ListFiltroParametroResponse>) out.get("TC_LIST_PARAM");

			listaParametro.setCodigoResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			listaParametro.setMensageResultado(out.get("PV_MSG_RESULTADO").toString());
			listaParametro.setFiltroParametro(listListaParametro);

		} catch (Exception e) {
			LOGGER.error("Error al consultar ---> listar Parametro", e);
			listaParametro.setCodigoResultado(-1);
			listaParametro.setMensageResultado(e.getMessage());
			listaParametro.setFiltroParametro(null);
		}

		return listaParametro;

	}

	@SuppressWarnings("unchecked")
	@Override
	public FiltroParametroResponse ControlFilaParametro(FiltroParametroRequest request) throws Exception {
		// TODO Auto-generated method stub
		FiltroParametroResponse response = new FiltroParametroResponse();
		List<ListFiltroParametroResponse> listListaParametro = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(datasource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_UTIL).withProcedureName("SP_PFC_S_PARAMETRO")
					.returningResultSet("TC_LIST", new ControlFilaParametroRowMapper());

			MapSqlParameterSource in = new MapSqlParameterSource();

			simpleJdbcCall.addDeclaredParameter(new SqlParameter("PN_COD_PARAMETRO", Types.VARCHAR));
			in.addValue("PN_COD_PARAMETRO", request.getCodigoParametro());

			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("TC_LIST", OracleTypes.CURSOR,
					new BeanPropertyRowMapper<FiltroParametroResponse>(FiltroParametroResponse.class)));
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PN_COD_RESULTADO", Types.NUMERIC));
			simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PV_MSG_RESULTADO", Types.VARCHAR));

			out = (Map<String, Object>) simpleJdbcCall.execute(in);
			listListaParametro = (List<ListFiltroParametroResponse>) out.get("TC_LIST");

			response.setFiltroParametro(listListaParametro);
			response.setCodigoResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMensageResultado(out.get("PV_MSG_RESULTADO").toString());

			LOGGER.info(out.get("PN_COD_RESULTADO"));
			LOGGER.info(out.get("PV_MSG_RESULTADO"));

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

			LOGGER.info(listListaParametro.size());
			LOGGER.info("TRAE DATA " + listListaParametro.size());

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("Error al consultar ---> listar Parametro", e);
		}

		return response;
	}

	@Override
	@SuppressWarnings("unchecked")
	public ApiOutResponse ConsultaParametroValue(FiltroParametroRequest request) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();
		// FiltroParametroResponse listaParametro = new FiltroParametroResponse();
		List<ListFiltroParametroResponse> listListaParametro = new ArrayList<>();

		try {
			/** Call store procedure */
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(datasource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_UTIL).withProcedureName("SP_PFC_S_PARAM_VAL")
					.returningResultSet("TC_LIST_PAR", new ListFiltroParametroRowMapper());

			/** Asignar los parameters input a la consulta */
			LOGGER.info("Asigna parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_GRUPO", request.getCodigoGrupo(), Types.NUMERIC);
			in.addValue("PN_CODIGO", request.getCodigoParam(), Types.VARCHAR);
			in.addValue("PN_VALUE", request.getValueParam(), Types.VARCHAR);
			in.addValue("PN_COD_PARAMETRO", request.getCodigoParametro(), Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);
			
			listListaParametro = (List<ListFiltroParametroResponse>) out.get("TC_LIST_PAR");
			response.setResponse(listListaParametro);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			LOGGER.error("Error al listar : ", e);
		}

		return response;
	}
}