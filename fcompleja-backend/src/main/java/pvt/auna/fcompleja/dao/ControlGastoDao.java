package pvt.auna.fcompleja.dao;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ExisteArchivoResponse;
import pvt.auna.fcompleja.model.bean.ConsumoBean;
import pvt.auna.fcompleja.model.bean.FiltroHistorialCargaBean;
import pvt.auna.fcompleja.model.bean.GastoConsumoBean;
import pvt.auna.fcompleja.model.bean.HistorialCargaGastosBean;

import java.util.List;

public interface ControlGastoDao {

    ApiOutResponse registrarConsumos (List<GastoConsumoBean> consumoMedicamentos);
    ApiOutResponse listArchivoCargadoGastos(FiltroHistorialCargaBean filtro);
	ApiOutResponse InsertaRegistroGastosCab(HistorialCargaGastosBean objRegistro);
	ApiOutResponse InsertaRegistroGastosDet(GastoConsumoBean objRegistro);
	ApiOutResponse ActualizaRegistroGastosCab(GastoConsumoBean objRegistro);

	ApiOutResponse validarTratamiento(GastoConsumoBean objRegistro);
	void actualizarMonitoreo(ConsumoBean consumoBean);
	ExisteArchivoResponse validarExisteArchivo();

	void actualizarEstado();
}
