package pvt.auna.fcompleja.dao;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.LineaTratamientoRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.BandejaMonitoreoRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionMarcadorRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.MarcadorRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.MonitoreoEvolucionRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.SegEjecutivoRequest;

public interface MonitoreoDao {

	public ApiOutResponse listaMonitoreo(BandejaMonitoreoRequest bandejaMonitoreoRequest);

	public ApiOutResponse historialLineaTratamiento(LineaTratamientoRequest request);

	public ApiOutResponse getUltimoLineaTratamiento(LineaTratamientoRequest request);

	public ApiOutResponse regDatosEvolucion(EvolucionRequest request);

	public ApiOutResponse actDatosEvolucion(EvolucionRequest request);

	public ApiOutResponse consultarEvoluciones(EvolucionRequest request);

	public ApiOutResponse regResultadoEvolucion(EvolucionRequest request);

	public ApiOutResponse consultarDetalleEvolucion(EvolucionMarcadorRequest request);

	public ApiOutResponse listarMarcadores(MarcadorRequest request);

	public ApiOutResponse listarHistorialMarcadores(MarcadorRequest request);

	public ApiOutResponse actualizarMonitoreoPendInfo(EvolucionRequest request);

	public ApiOutResponse regSeguimientoEjecutivo(SegEjecutivoRequest request);

	public ApiOutResponse listarSeguimientoEjecutivo(SegEjecutivoRequest request);

	public ApiOutResponse getSeguimientosPendientes(SegEjecutivoRequest request);
	
	public ApiOutResponse actEstadoSegPendientes(SegEjecutivoRequest request);
	
	public ApiOutResponse getUltimoRegistroMarcador(EvolucionMarcadorRequest request);
	
	public ApiOutResponse getUltimoMonitoreo(MonitoreoEvolucionRequest bandejaMonitoreo);

}
