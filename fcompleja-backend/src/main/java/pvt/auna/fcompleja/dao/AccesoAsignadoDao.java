package pvt.auna.fcompleja.dao;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.request.seguridad.UsuarioRequest;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;

public interface AccesoAsignadoDao {

	public OncoWsResponse consultarMenu(UsuarioRequest request, HttpHeaders headers) throws Exception;

	public OncoWsResponse consultarUsuarioRol(UsuarioRequest request) throws Exception;

	public OncoWsResponse consultarUsuario(UsuarioRequest request) throws Exception;

	public OncoWsResponse consultarUsuariosPorRol(UsuarioRequest request, HttpHeaders headers) throws Exception;
	
	public OncoWsResponse obtenerPorUsuarioId(UsuarioRequest request, HttpHeaders headers);

}
