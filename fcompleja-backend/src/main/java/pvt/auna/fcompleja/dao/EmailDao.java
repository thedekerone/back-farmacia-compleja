package pvt.auna.fcompleja.dao;

import java.util.List;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.CasosEvaluar;
import pvt.auna.fcompleja.model.api.TipoCorreoResponse;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.response.EmailCodigoEnvioSolicitudResponse;
import pvt.auna.fcompleja.model.api.response.ParametroCorreoResponse;
import pvt.auna.fcompleja.model.api.response.ParametroGrupoResponse;
import pvt.auna.fcompleja.model.bean.EmailBean;

public interface EmailDao {
	
	public EmailBean obtenerDatosCorreo(Long codEmail)throws Exception;

	public TipoCorreoResponse obtenerDatoTipoCorreo(int codTipoEmail)throws Exception;
	
	public ParametroGrupoResponse consultarParticipanteGrupo(EmailBean email);

	public EmailCodigoEnvioSolicitudResponse codigoEnvioSolicitud(Integer codSolEvaluacion);
	
	public ApiOutResponse actualizarEstadoSolicitud(EmailBean request);
	
	public ParametroCorreoResponse consultarParticipantePorRol(Integer codRol);

	public ApiOutResponse actParamsCorreoLiderTumor(SolicitudEvaluacionRequest solicitud);
	
	public ApiOutResponse actualizarEstCorreoSolEvaluacion(EmailBean request, String solicitudes);
	
	public ApiOutResponse actualizarEstCorreoLidTumSolEvalucion(EmailBean request);
	
	public EmailBean getDataCorreoReenvio(EmailBean request);
	
	public ApiOutResponse actParamsCorreoCmac(SolicitudEvaluacionRequest solicitud);
	
	public ApiOutResponse actParamsCorreoCmacV2(SolicitudEvaluacionRequest solicitud);
	
	public List<CasosEvaluar> listarSolEvalucionPorCodActa(Integer codSolEva);
	
	public ApiOutResponse obtenerDatosAutorizadorPert(String request);
}
