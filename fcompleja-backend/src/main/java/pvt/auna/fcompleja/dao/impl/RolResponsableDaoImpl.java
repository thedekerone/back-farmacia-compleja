package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pvt.auna.fcompleja.dao.RolResponsableDao;
import pvt.auna.fcompleja.model.api.ListRolResponsableResponse;
import pvt.auna.fcompleja.model.api.RolResponsableResponse;
import pvt.auna.fcompleja.model.mapper.ListRolResponsableRowMapper;
import pvt.auna.fcompleja.util.ConstanteUtil;

@Repository
public class RolResponsableDaoImpl implements RolResponsableDao {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	DataSource dataSource;

	@SuppressWarnings("unchecked")
	@Override
	public RolResponsableResponse RolResponsable() throws Exception {
		// TODO Auto-generated method stub
		RolResponsableResponse response = new RolResponsableResponse();
		List<ListRolResponsableResponse> ListaRolResponsable = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();

		try {
			SimpleJdbcCall SimpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_UTIL).withProcedureName("SP_S_ROL_RESPONSABLE")
					.returningResultSet("TC_LIST_ROL", new ListRolResponsableRowMapper());

			MapSqlParameterSource in = new MapSqlParameterSource();

			SimpleJdbcCall.addDeclaredParameter(new SqlOutParameter("TC_LIST_ROL", OracleTypes.CURSOR,
					new BeanPropertyRowMapper<ListRolResponsableResponse>(ListRolResponsableResponse.class)));
			SimpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PN_COD_RESULTADO", Types.NUMERIC));
			SimpleJdbcCall.addDeclaredParameter(new SqlOutParameter("PV_MSG_RESULTADO", Types.VARCHAR));

			out = (Map<String, Object>) SimpleJdbcCall.execute(in);

			response.setCodigoResultado(Long.parseLong(out.get("PN_COD_RESULTADO").toString()));
			response.setMensageResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

			ListaRolResponsable = (List<ListRolResponsableResponse>) out.get("TC_LIST_ROL");

			response.setRolResponsable(ListaRolResponsable);

			LOGGER.info(out.get("PN_COD_RESULTADO"));
			LOGGER.info(out.get("PV_MSG_RESULTADO"));

			LOGGER.info(ListaRolResponsable.size());
			LOGGER.info(response);
		} catch (Exception e) {
			LOGGER.error("Error al consultar ---> listar Rol responsables", e);
			response.setCodigoResultado(Long.parseLong("-1"));
			response.setMensageResultado(e.getMessage());
		}
		
		return response;
	}

}
