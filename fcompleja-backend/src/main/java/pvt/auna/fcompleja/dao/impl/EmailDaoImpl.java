package pvt.auna.fcompleja.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pvt.auna.fcompleja.dao.EmailDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.CasosEvaluar;
import pvt.auna.fcompleja.model.api.TipoCorreoResponse;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.response.EmailCodigoEnvioSolicitudResponse;
import pvt.auna.fcompleja.model.api.response.ParametroCorreoResponse;
import pvt.auna.fcompleja.model.api.response.ParametroGrupoResponse;
import pvt.auna.fcompleja.model.api.response.evaluacion.ParticipanteResponse;
import pvt.auna.fcompleja.model.bean.CodigoEnvioSolicitudBean;
import pvt.auna.fcompleja.model.bean.EmailBean;
import pvt.auna.fcompleja.model.bean.ParticipanteBean;
import pvt.auna.fcompleja.model.bean.ParticipanteCorreoBean;
import pvt.auna.fcompleja.model.bean.ParticipanteGrupoBean;
import pvt.auna.fcompleja.model.mapper.CodigoEnvioSolicitudRowMapper;
import pvt.auna.fcompleja.model.mapper.ParticipanteGrupoDiagRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.CasosEvaluarRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.CorreoParticipanteRowMapper;
import pvt.auna.fcompleja.model.mapper.monitoreo.ParticipanteCodUsuarioRowMapper;
import pvt.auna.fcompleja.util.ConstanteUtil;

@Repository
public class EmailDaoImpl implements EmailDao{
	
private final Logger LOGGER = Logger.getLogger(getClass());

	@Qualifier("dataSource")
	@Autowired
	DataSource dataSource;
	
	public EmailBean obtenerDatosCorreo(Long codTipoEmail) throws Exception{
		
		EmailBean objeto = new EmailBean();
//		long time =System.currentTimeMillis();
//		Map<String, Object> out = new HashMap<>();
//		
//		String msg;
		
//		try {
//			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
//					.withCatalogName(ConstanteUtil.PAQUETE_PRELIMINAR).withProcedureName("SP_PFC_S_TIPO_CORREO");
//			
//			MapSqlParameterSource in = new MapSqlParameterSource();
//			in.addValue("PN_CODTIPOCO", codTipoEmail, Types.NUMERIC);
//			
//			out = simpleJdbcCall.execute(in);			
//			
//			msg 	= (String)out.get("PV_MESSAGE");
//			asunto = (String)out.get("PV_ASUNTO");
//			cuerpo = (String)out.get("PV_CUERPO");
//			
//			if(msg == null) {
////				objeto.setAsunto(asunto);
////				objeto.setCuerpo(cuerpo);
//			} else {
//				LOGGER.error(msg);
//			}
			
//		} catch (Exception e) {
//			LOGGER.error("Error al consultar", e);
//			
//		}
//		System.out.println("Duracion de consulta obtenerDatosCorreoxCodigoEmail------------------------->"+ (System.currentTimeMillis()-time)/1000.00);

		
		return objeto;		
	}

	@Override
	public TipoCorreoResponse obtenerDatoTipoCorreo(int codTipoEmail) throws Exception {
		long time =System.currentTimeMillis();
		TipoCorreoResponse response = new TipoCorreoResponse();
		Map<String, Object> out = new HashMap<>();
		String asunto = "";
		String cuerpo = "";

		String msg;

		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).
					withSchemaName(ConstanteUtil.ESQUEMA).
					withCatalogName(ConstanteUtil.PAQUETE_PRELIMINAR).
					withProcedureName("SP_PFC_S_TIPO_CORREO");

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_CODTIPOCO", codTipoEmail, Types.NUMERIC);

			out = simpleJdbcCall.execute(in);

			msg 	= (String)out.get("PV_MSG_RESULTADO");
			asunto = (String)out.get("PV_ASUNTO");
			cuerpo = (String)out.get("PV_CUERPO");

			if(msg == null) {
				response.setAsunto(asunto);
				response.setCuerpo(cuerpo);
			} else {
				LOGGER.error(msg);
			}

		} catch (Exception e) {
			LOGGER.error("Error al consultar", e);

		}
		System.out.println("Duracion de consulta obtenerDatosCorreoxCodigoEmail------------------------->"+ (System.currentTimeMillis()-time)/1000.00);


		return response;
	}
	

	@Override
	public ParametroGrupoResponse consultarParticipanteGrupo(EmailBean email) {
		Map<String, Object> out = new HashMap<>();
		ParametroGrupoResponse listaResponse = new ParametroGrupoResponse();
		List<ParticipanteResponse> lParticipante = new ArrayList<>();

		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_RESP_MON_PARTICIPANTE")
					.returningResultSet("AC_LISTA", new ParticipanteGrupoDiagRowMapper());
 
			LOGGER.info("No tiene parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PV_COD_GRP_DIAG",  email.getCodigoGrupoDiagnostico(), Types.VARCHAR);
			in.addValue("PN_EDAD",  email.getEdadPaciente(), Types.NUMERIC);
			in.addValue("PN_COD_ROL",  email.getCodRol(), Types.NUMERIC);
			
			out = simpleJdbcCall.execute(in);

			lParticipante = (List<ParticipanteResponse>) out.get("AC_LISTA");
			listaResponse.setListaParticipante(lParticipante);
			listaResponse.setCodigo(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			listaResponse.setMensaje(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			LOGGER.error("Error al listar [SP_PFC_S_PARTICIPANTE_GRP_DIAG] : ", e);
		}

		return listaResponse;
	}

	@Override
	public EmailCodigoEnvioSolicitudResponse codigoEnvioSolicitud(Integer codSolEvaluacion) {
		Map<String, Object> out = new HashMap<>();
		EmailCodigoEnvioSolicitudResponse listaResponse = new EmailCodigoEnvioSolicitudResponse();
		List<CodigoEnvioSolicitudBean> codEnvioSolicitudBean = new ArrayList<>();

		try {
			/** Call store procedure */
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION)
					.withProcedureName("SP_PFC_S_SOL_EVA_COD_ENVIO")
					.returningResultSet("CODIGOENVIOSOLEVA", new CodigoEnvioSolicitudRowMapper());
 
			/** Asignar los parameters input a la consulta */
			LOGGER.info("No tiene parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PV_COD_SOL_EVA",  codSolEvaluacion, Types.NUMERIC);

			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			codEnvioSolicitudBean = (List<CodigoEnvioSolicitudBean>) out.get("CODIGOENVIOSOLEVA");
			listaResponse.setObjCodigoEnvioSolicitudBean(codEnvioSolicitudBean.get(0));
			

			listaResponse.setCodigo(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			listaResponse.setMensaje(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			LOGGER.error("Error al listar : ", e);
		}

		return listaResponse;
	}

	@Override
	public ApiOutResponse actualizarEstadoSolicitud(EmailBean request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();
		LOGGER.info("Inicio del método actualizarEstadoSolicitud");
		String codigo = "PN_COD_RESULTADO", mensaje = "PV_MSG_RESULTADO";

		try {
			LOGGER.info("Call store procedure SP_PFC_UPDATE_EST_SOL_EVA");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource)
					.withSchemaName(ConstanteUtil.ESQUEMA).withCatalogName(ConstanteUtil.PAQUETE_EVALUACION)
					.withProcedureName("SP_PFC_U_EST_SOL_EVA");
			
			MapSqlParameterSource in = new MapSqlParameterSource()
					.addValue("PN_COD_SOL_EVA", request.getCodSolicitudEvaluacion(), Types.NUMERIC)
					.addValue("PV_EST_SOL_EVAL", request.getEstadoSolicitudEvaluacion(), Types.VARCHAR);

			out = (Map<String, Object>) simpleJdbcCall.execute(in);
		
			if (Integer.parseInt(out.get(codigo).toString()) == -1) {
				LOGGER.error("Error al actualizar el codigo de envio : " + out.get(mensaje).toString());
				throw new Exception(out.get(mensaje).toString());
			} else {
				response.setCodResultado(Integer.parseInt(out.get(codigo).toString()));
				response.setMsgResultado(out.get(mensaje).toString());
				response.setResponse(request);
			}
			LOGGER.info("Fin del método actualizarCodigoEnvio");
		} catch (Exception e) {
			LOGGER.error("ERROR AL EJECUTAR EL PROCEDIMIENTO SP_PFC_UPDATE_EST_SOL_EVA " + e.getMessage());
		}

		return response;
	}

	@Override
	public ParametroCorreoResponse consultarParticipantePorRol(Integer codRol) {
		Map<String, Object> out = new HashMap<>();
		ParametroCorreoResponse listaResponse = new ParametroCorreoResponse();
		List<ParticipanteCorreoBean> participanteLineaTratBean = new ArrayList<>();

		try {
			/** Call store procedure */
			LOGGER.info("Call store procedure SP_PFC_S_PARTICIPANTE");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION)
					.withProcedureName("SP_PFC_S_PARTICIPANTE")
					.returningResultSet("AC_LISTA", new ParticipanteGrupoDiagRowMapper());
 
			/** Asignar los parameters input a la consulta */
			LOGGER.info("No tiene parametros input");
			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PV_COD_ROL",  codRol, Types.NUMERIC).
			addValue("PV_COD_GRP_DIAG", null,Types.VARCHAR).
			addValue("PN_P_RANGO_EDAD", null, Types.NUMERIC).
			addValue("PN_COD_USUARIO", null,Types.NUMERIC);
	
			/**
			 * Ejecuta la call store procedure con los parameters of input and
			 * simpleJdbcCall
			 */
			out = simpleJdbcCall.execute(in);

			participanteLineaTratBean = (List<ParticipanteCorreoBean>) out.get("AC_LISTA");
			listaResponse.setLista(participanteLineaTratBean);

			listaResponse.setCodigo(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			listaResponse.setMensaje(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			LOGGER.error("Error al listar [SP_PFC_S_PARTICIPANTE] : ", e);
		}

		return listaResponse;
	} 

	@Override
	public ApiOutResponse actParamsCorreoLiderTumor(SolicitudEvaluacionRequest solicitud) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();

		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_EVALUACION)
					.withProcedureName("SP_PFC_U_PARAMS_CORREO_EVA");

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_SOL_EVA", solicitud.getCodSolicitudEvaluacion(), Types.NUMERIC);
			in.addValue("PN_COD_ENVIO_ENV_LIDER_TUMOR", solicitud.getCodigoEnvioEnvLiderTumor(), Types.NUMERIC);
			in.addValue("PN_EST_CORREO_ENV_LIDER_TUMOR", solicitud.getEstadoCorreoEnvLiderTumor(), Types.NUMERIC);
			
			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			response.setResponse(solicitud);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Error al listar : ", e);
		}
		return response;
	}

	@Override
	public ApiOutResponse actualizarEstCorreoSolEvaluacion(EmailBean request, String solicitudes) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();

		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_U_SOLEVA_CORREO_CMAC");

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_ENVIO", Long.parseLong(request.getCodigoEnvio()), Types.NUMERIC);
			in.addValue("PV_COD_PLANTILLA", request.getCodPlantilla(), Types.VARCHAR);
			in.addValue("PN_TIPO_ENVIO", request.getTipoEnvio(), Types.NUMERIC);
			in.addValue("PN_FLAG_ADJUNTO", request.getFlagAdjunto(), Types.NUMERIC);
			in.addValue("PV_RUTA", request.getRuta(), Types.VARCHAR);
			in.addValue("PV_USRAPP", request.getUsrApp(), Types.VARCHAR);
			in.addValue("PV_ASUNTO", request.getAsunto(), Types.VARCHAR);
			in.addValue("PV_CUERPO", request.getCuerpo(), Types.VARCHAR);
			in.addValue("PV_DESTINATARIO", request.getDestinatario(), Types.VARCHAR);
			in.addValue("PV_SOLICITUDES", solicitudes, Types.VARCHAR);
			
			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			response.setResponse(request);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Error al listar : ", e);
		}
		return response;
	}

	@Override
	public ApiOutResponse actualizarEstCorreoLidTumSolEvalucion(EmailBean request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();

		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_U_SOLEVA_CORREO_LIDTUM");

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_ENVIO", Long.parseLong(request.getCodigoEnvio()), Types.NUMERIC);
			in.addValue("PV_COD_PLANTILLA", request.getCodPlantilla(), Types.VARCHAR);
			in.addValue("PN_TIPO_ENVIO", request.getTipoEnvio(), Types.NUMERIC);
			in.addValue("PN_FLAG_ADJUNTO", request.getFlagAdjunto(), Types.NUMERIC);
			in.addValue("PV_RUTA", request.getRuta(), Types.VARCHAR);
			in.addValue("PV_USRAPP", request.getUsrApp(), Types.VARCHAR);
			in.addValue("PV_ASUNTO", request.getAsunto(), Types.VARCHAR);
			in.addValue("PV_CUERPO", request.getCuerpo(), Types.VARCHAR);
			in.addValue("PV_DESTINATARIO", request.getDestinatario(), Types.VARCHAR);
			in.addValue("PN_COD_SOL_EVA", Long.parseLong(request.getListaCasosEvaluar().get(0).getNumSolicitudEvaluacion()), Types.NUMERIC);
			in.addValue("PN_ESTADO_CORREO_LIDTUM", 0, Types.NUMERIC);//0 => SIGNIFICA EN PROCESO ENVIO
			
			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			response.setResponse(request);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Error al listar : ", e);
		}
		return response;
	}
	
	@Override
	public EmailBean getDataCorreoReenvio(EmailBean request) {
		Map<String, Object> out = new HashMap<>();
		List<EmailBean> lista = new ArrayList<>();
		EmailBean obj = null;
		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_CORREO_PARTICIPANTE")
					.returningResultSet("AC_LISTA", new CorreoParticipanteRowMapper());

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_ENVIO", Long.parseLong(request.getCodigoEnvio()), Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			lista = (List<EmailBean>) out.get("AC_LISTA");
			obj = lista.get(0);
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Error al listar : ", e);
		}
		return obj;
	}

	@Override
	public ApiOutResponse actParamsCorreoCmac(SolicitudEvaluacionRequest solicitud) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();

		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_U_PARAMS_CMAC_EVA");

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_SOL_EVA", solicitud.getCodSolicitudEvaluacion(), Types.NUMERIC);
			in.addValue("PN_COD_ENVIO_ENV_MAC", solicitud.getCodigoEnvioEnvMac(), Types.NUMERIC);
			in.addValue("PN_EST_CORREO_ENV_CMAC", solicitud.getEstadoCorreoEnvCmac(), Types.NUMERIC);
			
			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			response.setResponse(solicitud);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Error al listar : ", e);
		}
		return response;
	}
	
	@Override
	public ApiOutResponse actParamsCorreoCmacV2(SolicitudEvaluacionRequest solicitud) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse response = new ApiOutResponse();

		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_U_PARAMS_CMAC_EVA_V2");

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();

			in.addValue("PN_COD_ENVIO_ENV_MAC", solicitud.getCodigoEnvioEnvMac(), Types.NUMERIC);
			in.addValue("PN_EST_CORREO_ENV_CMAC", solicitud.getEstadoCorreoEnvCmac(), Types.NUMERIC);
			
			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			// SETEAR ID GENERADO DE EVOLUCION
			response.setResponse(solicitud);
			response.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			response.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Error al listar : ", e);
		}
		return response;
	}

	@Override
	public List<CasosEvaluar> listarSolEvalucionPorCodActa(Integer codSolEva) {
		Map<String, Object> out = new HashMap<>();
		List<CasosEvaluar> lista = new ArrayList<>();
		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_SOLEVA_X_COD_ACTA")
					.returningResultSet("AC_LISTA", new CasosEvaluarRowMapper());

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PN_COD_SOL_EVA", codSolEva, Types.NUMERIC);

			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			lista = (List<CasosEvaluar>) out.get("AC_LISTA");
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Error al listar : ", e);
		}
		return lista;
	}

	@Override
	public ApiOutResponse obtenerDatosAutorizadorPert(String request) {
		Map<String, Object> out = new HashMap<>();
		ApiOutResponse outResponse = new ApiOutResponse();
		List<ParticipanteBean> lista = new ArrayList<>();

		try {
			LOGGER.info("Call store procedure");
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withSchemaName(ConstanteUtil.ESQUEMA)
					.withCatalogName(ConstanteUtil.PAQUETE_BANDEJA_MONITOREO)
					.withProcedureName("SP_PFC_S_COD_AUTO_PERT_SOL")
					.returningResultSet("AC_LISTA", new ParticipanteCodUsuarioRowMapper());

			LOGGER.info("Asigna parametros input");

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("PV_COD_SOL_EVA", request, Types.VARCHAR);

			out = simpleJdbcCall.execute(in);
			LOGGER.info("Respuesta del sp");

			lista = (List<ParticipanteBean>) out.get("AC_LISTA");
			outResponse.setResponse(lista);
			outResponse.setCodResultado(Integer.parseInt(out.get("PN_COD_RESULTADO").toString()));
			outResponse.setMsgResultado(out.get("PV_MSG_RESULTADO").toString());

			if (Integer.parseInt(out.get("PN_COD_RESULTADO").toString()) == -1) {
				throw new Exception(out.get("PV_MSG_RESULTADO").toString());
			}

		} catch (Exception e) {
			LOGGER.info("Error al listar : ", e);
		}
		return outResponse;
	}

}
