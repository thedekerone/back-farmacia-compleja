package pvt.auna.fcompleja.enums;

public enum WebServiceEnum
{
	WS_Sede("1"),
	WS_Obtener_Credenciales("2"),
	WS_Obtebner_Datos_Paciente("3"),
	WS_Server_Oauth2("4"),
	WS_CONSULTA_CITAS_APELLIDOS("5"),
	WS_CONSULTA_CITAS_DOCUMENTOS("6"),
	WS_RECOVERY_PASSWORD("7"),
	WS_FIRMA_DIGITAL("8"),
	WS_INTEGRACION_RPA("9"),
	WS_ADMISION_CITAS("10"),
	WS_OBTENERS_IAFAS("11");
	
	
  private String strPv_valor;
  
  private WebServiceEnum(String strA_valor)
  {
    this.strPv_valor = strA_valor;
  }
  
  public String getStrPv_valor()
  {
    return this.strPv_valor;
  }
  
  
}
