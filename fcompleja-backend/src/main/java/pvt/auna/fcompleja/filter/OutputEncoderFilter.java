package pvt.auna.fcompleja.filter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import pvt.auna.fcompleja.config.PropiedadesOauth;
import pvt.auna.fcompleja.filter.util.ResettableStreamHttpServletRequest;
import pvt.auna.fcompleja.filter.util.ResettableStreamHttpServletResponse;
import pvt.auna.fcompleja.util.SecurityDataProviderUtil;

@Component
@Order(2)
public class OutputEncoderFilter implements Filter {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	PropiedadesOauth propiedadesOauth;

	public void destroy() {
	}

	@SuppressWarnings("unused")
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		ResettableStreamHttpServletRequest wrappedRequest = new ResettableStreamHttpServletRequest(
				(HttpServletRequest) request);
		String context = "";

		ResettableStreamHttpServletResponse wrappedResponse = new ResettableStreamHttpServletResponse(
				(HttpServletResponse) response);

		try {

			context = wrappedRequest.getServletPath();
			response.setBufferSize(999999);
			chain.doFilter(request, wrappedResponse);
			wrappedResponse.flushBuffer();

		} finally {

			if (!(context.equals("/pub/insertarSolbenPreliminar")
					|| context.equals("/pub/lstMacMedicamento"))) {

				String payloadOUT = new String(wrappedResponse.getCopy(), StandardCharsets.UTF_8);
				if (Optional.ofNullable(wrappedResponse.getResponse().getContentType())
						.isPresent()) {
					if (wrappedResponse.getResponse().getContentType()
							.contains(MediaType.APPLICATION_JSON_VALUE)) {
						if (payloadOUT != null && !payloadOUT.equals("")) {
							try {

								LOGGER.info("payload en clarito:");
								LOGGER.info(payloadOUT);
								String payloadEncodedOUT = SecurityDataProviderUtil
										.encryptAES(propiedadesOauth.getAesKey(), payloadOUT);
								payloadEncodedOUT = wrapping(payloadEncodedOUT);
								wrappedResponse.getResponse().resetBuffer();
								wrappedResponse.getResponse().getOutputStream()
										.write(payloadEncodedOUT.getBytes());
							} catch (Exception e) {
								LOGGER.info("Error: " + e);
							}
						} else {
							try {
								String payloadEncodedOUT = SecurityDataProviderUtil
										.encryptAES(propiedadesOauth.getAesKey(), "");
								payloadEncodedOUT = wrapping(payloadEncodedOUT);
								wrappedResponse.getResponse().resetBuffer();
								wrappedResponse.getResponse().getOutputStream()
										.write(payloadEncodedOUT.getBytes());
							} catch (Exception e) {
								LOGGER.error("Error: " + e);
							}
						}
					}
				}
			}
		}

	}

	public String wrapping(
			String payloadEncodedOUT) throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, Object> wrapper = new HashMap<String, Object>();
		wrapper.put("data", payloadEncodedOUT);
		payloadEncodedOUT = new ObjectMapper().writeValueAsString(wrapper);
		return payloadEncodedOUT;
	}

	public void init(FilterConfig arg0) throws ServletException {
		System.out.println("call filter Output");
	}

}