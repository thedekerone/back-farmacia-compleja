package pvt.auna.fcompleja.filter;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import pvt.auna.fcompleja.config.PropiedadesOauth;
import pvt.auna.fcompleja.filter.util.ResettableStreamHttpServletRequest;
import pvt.auna.fcompleja.filter.util.Util;
import pvt.auna.fcompleja.util.SecurityDataProviderUtil;

@Component
@Order(1)
public class InputEncoderFilter implements Filter {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	PropiedadesOauth propiedadesOauth;

	public void destroy() {
	}

	@SuppressWarnings("unchecked")
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		ResettableStreamHttpServletRequest wrappedRequest = new ResettableStreamHttpServletRequest(
				(HttpServletRequest) request);

		String context = wrappedRequest.getServletPath();

		if (!(context.equals("/pub/insertarSolbenPreliminar")
				|| context.equals("/pub/lstMacMedicamento"))) {

			if ((wrappedRequest.getRequest().getContentType() != null && wrappedRequest.getRequest()
					.getContentType().contains(MediaType.APPLICATION_JSON_VALUE))) {

				String payloadIN = IOUtils.toString(wrappedRequest.getReader());

				if (payloadIN != null && !payloadIN.equals("")) {
					Map<String, Object> mapData;
					try {
						mapData = (Map<String, Object>) Util.castStringToJson(payloadIN);
						String payloadDecodedIN = SecurityDataProviderUtil.decryptAES(
								propiedadesOauth.getAesKey(), mapData.get("data").toString());
						wrappedRequest.resetInputStream(payloadDecodedIN.getBytes());
						chain.doFilter(wrappedRequest, response);
					} catch (Exception e) {
						LOGGER.error("Error:", e);
					}
				} else {
					wrappedRequest.resetInputStream(payloadIN.getBytes());
					chain.doFilter(wrappedRequest, response);
				}
			} else {
				chain.doFilter(request, response);
			}
		} else {
			chain.doFilter(request, response);
		}

	}

	public void init(FilterConfig arg0) throws ServletException {
	}

}
