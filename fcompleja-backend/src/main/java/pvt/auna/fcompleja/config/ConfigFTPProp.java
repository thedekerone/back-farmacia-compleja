package pvt.auna.fcompleja.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "configuracionFTP")
public class ConfigFTPProp {
	private String rutaTemp;
	private String rutaFTP;
	private String userApp;
	private String reporteEva;
	private String reportePlanilla;
	private String rutaReportes;
	private String rutaImagenes;

	public String getRutaTemp() {
		return rutaTemp;
	}

	public void setRutaTemp(String rutaTemp) {
		this.rutaTemp = rutaTemp;
	}

	public String getRutaFTP() {
		return rutaFTP;
	}

	public void setRutaFTP(String rutaFTP) {
		this.rutaFTP = rutaFTP;
	}

	public String getUserApp() {
		return userApp;
	}

	public void setUserApp(String userApp) {
		this.userApp = userApp;
	}

	public String getReporteEva() {
		return reporteEva;
	}

	public void setReporteEva(String reporteEva) {
		this.reporteEva = reporteEva;
	}

	public String getReportePlanilla() {
		return reportePlanilla;
	}

	public void setReportePlanilla(String reportePlanilla) {
		this.reportePlanilla = reportePlanilla;
	}

	/**
	 * @return the rutaReportes
	 */
	public String getRutaReportes() {
		return rutaReportes;
	}

	/**
	 * @param rutaReportes the rutaReportes to set
	 */
	public void setRutaReportes(String rutaReportes) {
		this.rutaReportes = rutaReportes;
	}

	/**
	 * @return the rutaImagenes
	 */
	public String getRutaImagenes() {
		return rutaImagenes;
	}

	/**
	 * @param rutaImagenes the rutaImagenes to set
	 */
	public void setRutaImagenes(String rutaImagenes) {
		this.rutaImagenes = rutaImagenes;
	}

	

	
}
