package pvt.auna.fcompleja.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "portalComun")
public class AseguramientoPropiedades {

	Integer aplicacion;
	Integer rolMiembroCmac;
	String aseguramiento;
	String rutaEmail;
	String rutaFTP;
	String oauth2;
	String version;
	String parametro;
	String auditoria;

	public String getParametro() {
		return parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	public Integer getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(Integer aplicacion) {
		this.aplicacion = aplicacion;
	}

	public Integer getRolMiembroCmac() {
		return rolMiembroCmac;
	}

	public void setRolMiembroCmac(Integer rolMiembroCmac) {
		this.rolMiembroCmac = rolMiembroCmac;
	}

	public String getRutaFTP() {
		return rutaFTP;
	}

	public void setRutaFTP(String rutaFTP) {
		this.rutaFTP = rutaFTP;
	}

	public String getAseguramiento() {
		return aseguramiento;
	}

	public void setAseguramiento(String aseguramiento) {
		this.aseguramiento = aseguramiento;
	}

	public String getRutaEmail() {
		return rutaEmail;
	}

	public void setRutaEmail(String rutaEmail) {
		this.rutaEmail = rutaEmail;
	}

	public String getOauth2() {
		return oauth2;
	}

	public void setOauth2(String oauth2) {
		this.oauth2 = oauth2;
	}
	
	public String getAuditoria() {
		return auditoria;
	}
	
	public void setAuditoria(String auditoria) {
		this.auditoria = auditoria;
	}
}
