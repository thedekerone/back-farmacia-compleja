package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.seguridad.UsuarioRequest;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.model.api.response.UsuarioOauthResponse;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.service.AccesoAsignadoService;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@RestController
public class AccesoAsignadoController {

	private static final Logger log = LoggerFactory.getLogger(AccesoAsignadoController.class);

	@Autowired
	AseguramientoPropiedades portalprop;

	@Autowired
	private AccesoAsignadoService accesoService;
	
	@Autowired
	UsuarioService userService;
	
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/obtenerAplicacion", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseOnc> obtenerAplicacion(@RequestBody UsuarioBean obj) {
		obj.setCodAplicacion(portalprop.getAplicacion());
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		
		log.info("API method: USUARIOS POR ROL");

		log.info("[SERVICIO:  OBTENER APLICACION][INICIO]");
		log.info("[SERVICIO:  OBTENER APLICACION][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.info("[SERVICIO:  OBTENER APLICACION][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.debug("[SERVICIO: OBTENER APLICACION][REQUEST][BODY][" + obj.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;
		
		try {
			resultado = userService.obtenerAplicacion(obj, headers);
			
			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					log.info("[SERVICIO:  OBTENER APLICACION][REQUEST][BODY][Obtener Aplicacion]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<UsuarioBean>) resultado.getDataList());
				} else {
					log.error("[SERVICIO:  OBTENER APLICACION][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					log.error("[SERVICIO:  OBTENER APLICACION][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				log.error("[SERVICIO:  LISTAR POR ROL][REQUEST][FIN][Error en el servicio para obtener listado de Aplicaciones]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio para obtener listado de aplicaciones.");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			log.error(e.getMessage());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/menu", produces = "application/json; charset=UTF-8")
	public @ResponseBody OncoWsResponse consultarMenu(@RequestBody UsuarioRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion,
		    @RequestHeader(value = "Authorization") String authorization){

		log.info("[SERVICIO: CONSULTAR LISTA MENU OPCIONES][INICIO]");
		log.info("[SERVICIO: CONSULTAR LISTA MENU OPCIONES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.info("[SERVICIO: CONSULTAR LISTA MENU OPCIONES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion
				+ "]");

		log.debug(authorization);

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		request.setCodAplicacion(portalprop.getAplicacion());

		OncoWsResponse response = new OncoWsResponse();
		AudiResponse audiResponse = new AudiResponse();

		UsuarioOauthResponse respo = accesoService.obtenerUsuarioPorToken(
				authorization.replace("bearer","")
							 .replace("Bearer","").trim()	);
		if(!respo.getAudiResponse().getCodRespuesta().equalsIgnoreCase("0")){
			audiResponse.setCodigoRespuesta(respo.getAudiResponse().getCodRespuesta());
			audiResponse.setMensajeRespuesta(respo.getAudiResponse().getMensajeRespuesta());
			response.setAudiResponse(audiResponse);
			return response;
		}else{
			if(!respo.getCodUsuario().equals(request.getCodUsuario())){
				audiResponse.setCodigoRespuesta("1");
				audiResponse.setMensajeRespuesta("Acceso denegado.");
				response.setAudiResponse(audiResponse);
				return response;
			}
		}
		try {
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(fechaTransaccion);
			OncoWsResponse service = accesoService.consultarMenu(request, headers);
			if (service.getAudiResponse().getCodigoRespuesta().equals("0")) {
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
				response.setDataList(service.getDataList());
				//response.setDataList(null);
				log.info("[SERVICIO: CONSULTAR LISTA MENU OPCIONES][RESPONSE] " + response.toString());
				log.info("[SERVICIO: CONSULTAR LISTA MENU OPCIONES][FIN]");
			} else {
				log.info("[SERVICIO: CONSULTAR LISTA MENU OPCIONES][RESPONSE] Lista vacia");
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
			}
		} catch (Exception e) {
			log.error("error : ", e);
			audiResponse.setCodigoRespuesta(new Date().toString());
			audiResponse.setMensajeRespuesta(e.getMessage());
		}
                audiResponse.setTokenTemporal(request.getTokenTemporal());
		response.setAudiResponse(audiResponse);
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/usuarioRolAseg", produces = "application/json; charset=UTF-8")
	public @ResponseBody OncoWsResponse consultarUsuarioRol(@RequestBody UsuarioRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) {

		OncoWsResponse response = new OncoWsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(new Date().toString());
			OncoWsResponse service = accesoService.consultarUsuarioRol(request);
			if (service.getAudiResponse().getCodigoRespuesta().equals("0")) {
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
				response.setDataList(service.getDataList());
				log.info("[SERVICIO: CONSULTAR OPCION][RESPONSE] " + response.toString());
				log.info("[SERVICIO: CONSULTAR OPCION][FIN]");
			} else {
				log.info("Lista vacia");
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
			}

		} catch (Exception e) {
			log.error("error : ", e);
			audiResponse.setCodigoRespuesta(new Date().toString());
			audiResponse.setMensajeRespuesta(e.getMessage());
		}
		response.setAudiResponse(audiResponse);
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/usuario", produces = "application/json; charset=UTF-8")
	public @ResponseBody OncoWsResponse consultarUsuario(@RequestBody UsuarioRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) {

		OncoWsResponse response = new OncoWsResponse();
		AudiResponse audiResponse = new AudiResponse();

		request.setCodAplicacion(portalprop.getAplicacion());

		try {
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(new Date().toString());
			audiResponse.setUser(request.getUsuario());
			OncoWsResponse service = accesoService.consultarUsuario(request);
			if (service.getAudiResponse().getCodigoRespuesta().equals("0")) {
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
				response.setDataList(service.getDataList());
				log.info("[SERVICIO: CONSULTAR USUARIO][RESPONSE] " + response.toString());
				log.info("[SERVICIO: CONSULTAR USUARIO][FIN]");
			} else {
				log.info("Objeto vacio");
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
			}

		} catch (Exception e) {
			log.error("error : ", e);
			audiResponse.setCodigoRespuesta(new Date().toString());
			audiResponse.setMensajeRespuesta(e.getMessage());
		}
		response.setAudiResponse(audiResponse);
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/seguridad/usuario/listarPorRol", produces = "application/json; charset=UTF-8")
	public @ResponseBody OncoWsResponse consultarUsuarioPorRol(@RequestBody UsuarioRequest request) {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		OncoWsResponse response = new OncoWsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(new Date().toString());
			OncoWsResponse service = accesoService.consultarUsuariosPorRol(request, headers);
			if (service.getAudiResponse().getCodigoRespuesta().equals("0")) {
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
				response.setDataList(service.getDataList());
				log.info("[SERVICIO: CONSULTAR USUARIO FARMACIA POR ROL][RESPONSE] " + response.toString());
				log.info("[SERVICIO: CONSULTAR USUARIO FARMACIA POR ROL][FIN]");
			} else {
				log.info("Objeto vacio");
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
			}

		} catch (Exception e) {
			log.error("error : ", e);
			audiResponse.setCodigoRespuesta("-2");
			audiResponse.setMensajeRespuesta(e.getMessage());
		}
		response.setAudiResponse(audiResponse);
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/seguridad/usuario/obtenerPorUsuarioId", produces = "application/json; charset=UTF-8")
	public @ResponseBody OncoWsResponse obtenerPorUsuarioId(@RequestBody UsuarioRequest request) {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		OncoWsResponse response = new OncoWsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(new Date().toString());
			OncoWsResponse service = accesoService.consultarUsuariosPorRol(request, headers);
			if (service.getAudiResponse().getCodigoRespuesta().equals("0")) {
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
				response.setDataList(service.getDataList());
				log.info("[SERVICIO: CONSULTAR USUARIO DE SEGURIDAD POR ID][RESPONSE] " + response.toString());
				log.info("[SERVICIO: CONSULTAR USUARIO DE SEGURIDAD POR ID][FIN]");
			} else {
				log.info("Objeto vacio");
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
			}

		} catch (Exception e) {
			log.error("error : ", e);
			audiResponse.setCodigoRespuesta("-2");
			audiResponse.setMensajeRespuesta(e.getMessage());
		}
		response.setAudiResponse(audiResponse);
		return response;
	}

}
