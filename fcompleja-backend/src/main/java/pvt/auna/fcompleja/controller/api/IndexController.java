package pvt.auna.fcompleja.controller.api;



import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.ApiResponse;


@RestController
@RequestMapping("/")
public class IndexController {
    @CrossOrigin(origins = "*")
    @GetMapping("/")
    @ResponseBody
    public ApiResponse index() {

        return new ApiResponse("Ok", "Backend demo Ok!", new Date().toString());
    }
}
