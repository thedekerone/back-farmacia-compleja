package pvt.auna.fcompleja.controller.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ApiResponse;
import pvt.auna.fcompleja.model.api.ClinicaRequest;
import pvt.auna.fcompleja.model.api.DiagnosticoRequest;
import pvt.auna.fcompleja.model.api.MedicoRequest;
import pvt.auna.fcompleja.model.api.PacienteRequest;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.SolbenWsRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudPreliminarRequest;
import pvt.auna.fcompleja.model.api.response.SolbenWsResponse;
import pvt.auna.fcompleja.model.api.response.SolicitudPreliminarResponse;
import pvt.auna.fcompleja.model.bean.ClinicaBean;
import pvt.auna.fcompleja.model.bean.DiagnosticoBean;
import pvt.auna.fcompleja.model.bean.MedicoBean;
import pvt.auna.fcompleja.model.bean.PacienteBean;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.service.OncoClinicaService;
import pvt.auna.fcompleja.service.OncoDiagnosticoService;
import pvt.auna.fcompleja.service.OncoMedicoService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.service.SolbenService;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.GenericUtil;

@RestController
@RequestMapping("/")
public class SolbenController {
	private static final Logger log = LoggerFactory.getLogger(SolbenController.class);

	@Autowired
	private AseguramientoPropiedades asegProp;

	@Autowired
	private UsuarioService userService;

	@Autowired
	private SolbenService solbenService;

	@Autowired
	private OncoPacienteService pcteService;

	@Autowired
	private OncoClinicaService clinService;

	@Autowired
	private OncoDiagnosticoService diagService;

	@Autowired
	private OncoMedicoService mediService;

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/insertarSolbenPreliminar", produces = "application/json; charset=UTF-8")
	public ResponseEntity<SolicitudPreliminarResponse> getScgSolbenAndInsertSolPreliminar(
			@Validated @RequestBody SolicitudPreliminarRequest request) throws Exception {

		log.debug("Starting API getScgSolbenAndInsertSolPreliminar, input {}", request.toString());

		HttpHeaders headers = GenericUtil.obtenerCabecera();

		SolicitudPreliminarResponse response = new SolicitudPreliminarResponse();
		ApiResponse res = new ApiResponse();

		res = ValidacionesSolben(request);
		if (!res.getStatus().equalsIgnoreCase("0")) {
			response.setOp_cod_resultado(Integer.parseInt(res.getStatus()));
			response.setOp_msg_resultado(res.getMessage());
			return new ResponseEntity<SolicitudPreliminarResponse>(response, HttpStatus.BAD_REQUEST);
		}

		res = ValidarOncoClinica(request, headers);
		if (!res.getStatus().equalsIgnoreCase("0")) {
			response.setOp_cod_resultado(Integer.parseInt(res.getStatus()));
			response.setOp_msg_resultado(res.getMessage());
			return new ResponseEntity<SolicitudPreliminarResponse>(response, HttpStatus.BAD_REQUEST);
		}

		res = ValidarOncoPaciente(request, headers);
		if (!res.getStatus().equalsIgnoreCase("0")) {
			response.setOp_cod_resultado(Integer.parseInt(res.getStatus()));
			response.setOp_msg_resultado(res.getMessage());
			return new ResponseEntity<SolicitudPreliminarResponse>(response, HttpStatus.BAD_REQUEST);
		}

		res = ValidarOncoDiagnostico(request, headers);
		if (!res.getStatus().equalsIgnoreCase("0")) {
			response.setOp_cod_resultado(Integer.parseInt(res.getStatus()));
			response.setOp_msg_resultado(res.getMessage());
			return new ResponseEntity<SolicitudPreliminarResponse>(response, HttpStatus.BAD_REQUEST);
		}

		request.setCodGrpDiag(res.getResponse() + "");

		res = ValidarOncoMedico(request, headers);
		if (!res.getStatus().equalsIgnoreCase("0")) {
			response.setOp_cod_resultado(Integer.parseInt(res.getStatus()));
			response.setOp_msg_resultado(res.getMessage());
			return new ResponseEntity<SolicitudPreliminarResponse>(response, HttpStatus.BAD_REQUEST);
		}

		res = ValidarUsuarioSolben(request.getTx_dato_adic1(), headers);
		if (!res.getStatus().equalsIgnoreCase("0")) {
			response.setOp_cod_resultado(Integer.parseInt(res.getStatus()));
			response.setOp_msg_resultado(res.getMessage());
			return new ResponseEntity<SolicitudPreliminarResponse>(response, HttpStatus.BAD_REQUEST);
		}

		response = solbenService.getScgSolbenAndInsertSolPreliminar(request, headers);
		return new ResponseEntity<SolicitudPreliminarResponse>(response, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "pub/actualizarSolben", produces = "application/json; charset=UTF-8")
	public SolbenWsResponse actualizarSolben(@RequestBody SolbenWsRequest request) throws Exception {

		log.info("API method: CONSULTAR VALIDACION DE LIDER TUMOR CON GRUPO DIAGNOSTICO");

		log.info("[SERVICIO: CONSULTAR VALIDACION DE LIDER TUMOR][INICIO]");
		log.debug("[SERVICIO: CONSULTAR VALIDACION DE LIDER TUMOR][REQUEST][BODY][" + request.toString() + "]");

		SolbenWsResponse response = solbenService.actualizarSolben(request);

		if (response.getCod_rpta() == -1) {
			log.error("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + response.getMsg_rpta());
		}

		return response;
	}

	public ApiResponse ValidarOncoPaciente(SolicitudPreliminarRequest request, HttpHeaders headers) {
		ApiResponse api = new ApiResponse();
		PacienteRequest pcteRequest = new PacienteRequest();

		if (request.getCod_afi_paciente() == null || request.getCod_afi_paciente().trim().equalsIgnoreCase("")) {
			api.setStatus("6");
			api.setMessage("Por favor ingresar el código de afiliado");
			api.setResponse(null);
			return api;
		}

		pcteRequest.setPvTibus(ConstanteUtil.tipoCodAfi);
		pcteRequest.setCodigoAfiliado(request.getCod_afi_paciente());
		pcteRequest.setApellidoPaterno("''");
		pcteRequest.setApellidoMaterno("''");
		pcteRequest.setNombres("''");
		pcteRequest.setTipoDocumento("''");
		pcteRequest.setNumeroDocumento("''");

		try {
			WsResponse response = pcteService.obtenerPaciente(pcteRequest, headers);
			PacienteBean pcte = (PacienteBean) response.getData();

			if (response != null && Integer.parseInt(response.getAudiResponse().getCodigoRespuesta()) >= 0) {
				if (response.getAudiResponse().getCodigoRespuesta().equals("0")) {
					api.setStatus("0");
					api.setMessage("Validación del código del afiliado correcto.");
					api.setResponse(pcte);
				} else {
					api.setStatus(response.getAudiResponse().getCodigoRespuesta());
					api.setMessage("Código de afiliado no registrado.");
					log.error(response.getAudiResponse().getMensajeRespuesta());
					api.setResponse(null);
				}
			} else {
				api.setStatus("7");
				api.setMessage("Ocurrio un error con el servicio de Oncosys Paciente");
				log.error(response.getAudiResponse().getMensajeRespuesta());
				api.setResponse(null);
			}

		} catch (Exception e) {
			api.setStatus("7");
			api.setMessage("Ocurrio un error con el servicio de Oncosys Paciente");
			api.setResponse(null);
			log.error(e.getMessage());
		}

		return api;
	}

	public ApiResponse ValidarOncoDiagnostico(SolicitudPreliminarRequest request, HttpHeaders headers) {
		ApiResponse api = new ApiResponse();
		DiagnosticoRequest diagRequest = new DiagnosticoRequest();

		if (request.getCod_diagnostico() == null || request.getCod_diagnostico().trim().equalsIgnoreCase("")) {
			api.setStatus("6");
			api.setMessage("Por favor ingresar el código de diagnóstico");
			api.setResponse(null);
			return api;
		}

		diagRequest.setTipoBusqueda(ConstanteUtil.tipoXCod);
		diagRequest.setRegistroInicio(ConstanteUtil.registroIni);
		diagRequest.setRegistroFin(ConstanteUtil.registroIni);
		diagRequest.setCodigoDiagnostico(request.getCod_diagnostico());
		diagRequest.setNombreDiagnostico("");

		try {
			WsResponse response = diagService.obtenerDiagnostico(headers, diagRequest);
			if (response != null && Integer.parseInt(response.getAudiResponse().getCodigoRespuesta()) >= 0) {
				DiagnosticoBean diagnostico = (DiagnosticoBean) response.getData();
				if (response.getAudiResponse().getCodigoRespuesta().equals("0")) {
					if (diagnostico.getGrupo() != null && diagnostico.getGrupo().getCodigo() != null
							&& !diagnostico.getGrupo().getCodigo().equals("")) {
						api.setStatus("0");
						api.setMessage("Validación diagnóstico y grupo diagnóstico correcto.");
						api.setResponse(diagnostico.getGrupo().getCodigo());
					} else {
						api.setStatus("5");
						api.setMessage("Código de grupo diagnóstico no registrado.");
						api.setResponse(null);
					}
				} else {
					api.setStatus(response.getAudiResponse().getCodigoRespuesta());
					api.setMessage("Código de diagnóstico no registrado.");
					log.error(response.getAudiResponse().getMensajeRespuesta());
					api.setResponse(null);
				}
			} else {
				api.setStatus("10");
				api.setMessage("Ocurrio un error con el servicio de Oncosys diagnóstico");
				log.error(response.getAudiResponse().getMensajeRespuesta());
				api.setResponse(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			api.setStatus("10");
			api.setMessage("Ocurrio un error con el servicio de Oncosys Diagnóstico");
			api.setResponse(null);
		}

		return api;
	}

	public ApiResponse ValidarOncoMedico(SolicitudPreliminarRequest request, HttpHeaders headers) {
		ApiResponse api = new ApiResponse();
		MedicoRequest medicoRequest = new MedicoRequest();

		if (request.getCmp_medico() == null || request.getCmp_medico().trim().equalsIgnoreCase("")) {
			api.setStatus("6");
			api.setMessage("Por favor ingresar el código del médico");
			api.setResponse(null);
			return api;
		}

		medicoRequest.setIni(ConstanteUtil.registroIni);
		medicoRequest.setFin(ConstanteUtil.registroIni);
		medicoRequest.setTipo(ConstanteUtil.tipoCmp);
		medicoRequest.setCodigoMed("");
		medicoRequest.setApellidoPat("");
		medicoRequest.setApellidoMat("");
		medicoRequest.setNombres("");
		medicoRequest.setCodcmp(request.getCmp_medico());
		medicoRequest.setTotal("");

		try {
			WsResponse response = mediService.obtenerMedicoTratante(headers, medicoRequest);
			MedicoBean medico = (MedicoBean) response.getData();
			if (medico != null) {
				api.setStatus("0");
				api.setMessage("Validación de código CMP correcto.");
				api.setResponse(null);
			} else {
				api.setStatus("6");
				api.setMessage("Código CMP no registrado.");
				api.setResponse(null);
			}
		} catch (Exception e) {
			api.setStatus("10");
			api.setMessage("Ocurrio un error con el servicio de Oncosys Médico");
			api.setResponse(null);
		}

		return api;
	}

	public ApiResponse ValidarOncoClinica(SolicitudPreliminarRequest request, HttpHeaders headers) {
		ApiResponse api = new ApiResponse();
		ClinicaRequest clinicaRequest = new ClinicaRequest();

		if (request.getCod_clinica() == null || request.getCod_clinica().trim().equalsIgnoreCase("")) {
			api.setStatus("6");
			api.setMessage("Por favor ingresar el código de la clínica");
			api.setResponse(null);
			return api;
		}

		clinicaRequest.setIni(ConstanteUtil.registroIni);
		clinicaRequest.setFin(ConstanteUtil.registroFinClinicas);
		clinicaRequest.setTipoBus(ConstanteUtil.tipoXCod);
		clinicaRequest.setCodcli(request.getCod_clinica());
		clinicaRequest.setNomcli("''");

		try {
			WsResponse response = clinService.obtenerClinica(clinicaRequest, headers);
			ClinicaBean clinica = (ClinicaBean) response.getData();

			if (response != null && Integer.parseInt(response.getAudiResponse().getCodigoRespuesta()) >= 0) {
				if (response.getAudiResponse().getCodigoRespuesta().equals("0")) {
					api.setStatus("0");
					api.setMessage("Validación del código de la clínica correcto.");
					api.setResponse(clinica);
				} else {
					api.setStatus(response.getAudiResponse().getCodigoRespuesta());
					api.setMessage(response.getAudiResponse().getMensajeRespuesta());
					api.setResponse(null);
				}
			} else {
				api.setStatus("10");
				api.setMessage("Ocurrio un error con el servicio de Oncosys Clínica");
				api.setResponse(null);
			}
		} catch (Exception e) {
			api.setStatus("10");
			api.setMessage("Ocurrio un error con el servicio de Oncosys Clínica");
			api.setResponse(null);
		}

		return api;
	}

	public ApiResponse ValidarUsuarioSolben(String usuarioSolben, HttpHeaders headers) {
		ApiResponse api = new ApiResponse();
		UsuarioBean userRquest = new UsuarioBean();

		if (usuarioSolben == null || usuarioSolben.trim().equalsIgnoreCase("")) {
			api.setStatus("8");
			api.setMessage("Por favor, ingresar el código de usuario solben en el campo txt");
			api.setResponse(null);
			return api;
		}

		try {
			userRquest.setCodAplicacion(asegProp.getAplicacion());
			userRquest.setCodSolben(Integer.parseInt(usuarioSolben));

		} catch (NumberFormatException e) {
			api.setStatus("9");
			api.setMessage("Código usuario solben solo permite digitos del 0 al 9");
			api.setResponse(null);
			return api;
		}

		try {
			ApiOutResponse response = userService.getListarUsuarioSolben(userRquest, headers);

			if (response != null) {
				if (response.getCodResultado() == 0) {
					api.setStatus("0");
					api.setMessage("Validación del código del usuario solben correcto.");
					api.setResponse(response.getResponse());
				} else if (response.getCodResultado() == 3) {
					api.setStatus("9");
					api.setMessage("Código de usuario solben no es válido");
					api.setResponse(null);
				} else {
					api.setStatus(response.getCodResultado() + "");
					api.setMessage(response.getMsgResultado());
					api.setResponse(null);
				}
			} else {
				api.setStatus("10");
				api.setMessage("Ocurrio un error con el Servicio Usuario Solben");
				api.setResponse(null);
			}
		} catch (Exception e) {
			api.setStatus("10");
			api.setMessage("Ocurrio un error con el Servicio Usuario Solben. " + e.getMessage());
			api.setResponse(null);
		}

		return api;
	}

	private ApiResponse ValidacionesSolben(SolicitudPreliminarRequest request) {
		ApiResponse api = new ApiResponse();

		if (GenericUtil.isNulo(request.getCod_scg_solben())) {
			api.setStatus("6");
			api.setMessage("Código solben no pueder ser nulo o vacio");
			return api;
		}
		
		if (GenericUtil.isNulo(request.getCod_clinica())) {
			api.setStatus("6");
			api.setMessage("Código de la clínica no pueder ser nulo o vacio.");
			return api;
		}
		
		if (GenericUtil.isNulo(request.getCod_afi_paciente())) {
			api.setStatus("6");
			api.setMessage("Código del afiliado no pueder ser nulo o vacio.");
			return api;
		}
		
		if (GenericUtil.isNulo(request.getDes_contratante())) {
			api.setStatus("6");
			api.setMessage("El campo 'des_contratante' no pueder ser nulo o vacio.");
			return api;
		}
		
		if (GenericUtil.isNulo(request.getDes_plan())) {
			api.setStatus("6");
			api.setMessage("El campo 'des_plan' no pueder ser nulo o vacio.");
			return api;
		}
		
		if (GenericUtil.isNulo(request.getCod_diagnostico())) {
			api.setStatus("6");
			api.setMessage("Código de diagnóstico no pueder ser nulo o vacio.");
			return api;
		}
		
		if (GenericUtil.isNulo(request.getCmp_medico())) {
			api.setStatus("6");
			api.setMessage("Código de CMP no pueder ser nulo o vacio.");
			return api;
		}
		
		if (GenericUtil.isNulo(request.getTipo_scg_solben())) {
			api.setStatus("6");
			api.setMessage("El campo 'tipo_scg_solben' no pueder ser nulo o vacio.");
			return api;
		}
		
		if (GenericUtil.isNulo(request.getEstado_scg())) {
			api.setStatus("6");
			api.setMessage("El campo 'estado_scg' no pueder ser nulo o vacio.");
			return api;
		}
		
		if (GenericUtil.isNulo(request.getInd_valida())) {
			api.setStatus("6");
			api.setMessage("El campo 'ind_valida' no pueder ser nulo o vacio.");
			return api;
		}
		
		if (GenericUtil.isNulo(request.getTx_dato_adic1())) {
			api.setStatus("6");
			api.setMessage("El campo 'tx_dato_adic1' no pueder ser nulo o vacio, colocar el Código Usuario Solben.");
			return api;
		}
		
		api.setStatus("0");
		api.setMessage("Validación de campos correcta.");
		return api;
	}

}
