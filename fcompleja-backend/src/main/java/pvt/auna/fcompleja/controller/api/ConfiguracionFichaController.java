package pvt.auna.fcompleja.controller.api;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import pvt.auna.fcompleja.model.api.request.HeaderBean;
import pvt.auna.fcompleja.model.api.response.ResponseGenerico;
import pvt.auna.fcompleja.service.GenericoService;
import pvt.auna.fcompleja.util.AunaUtil;

@RestController
@RequestMapping("/")
public class ConfiguracionFichaController {

	private final Logger LOGGER = Logger.getLogger(getClass());
	
	@Autowired
	@SuppressWarnings("rawtypes")
	AunaUtil util;
	
	@Autowired
	GenericoService genericoService;
    
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/cargaVersionFichaTecnica", produces = "application/json; charset=UTF-8")
	public ResponseGenerico cargarFichaTecnica(@RequestHeader HttpHeaders headers,
			@RequestParam("codMac") String codMac,
			@RequestParam("version") String version,
    		@RequestParam("estado") String estado,
    		@RequestParam("nomArchivo") String nombreArchivo,
    		@RequestParam("archivo") MultipartFile archivo) throws Exception {

		ResponseGenerico response = new ResponseGenerico();

		HeaderBean header = util.validHeader(headers);
  
        
        LOGGER.info(
				"[SERVICIO: CARGAR FICHA TECNICA][" + header.getIdTransaccion() + "][INICIO]");
		LOGGER.info("[SERVICIO: CARGAR FICHA TECNICA][" + header.getIdTransaccion()
				+ "][REQUEST][BODY][" + "]");

		
//		try {
//			ResponseFTP response1 = genericoService.enviarArchivo("ONTPFC", nombreArchivo, archivo, "/home/peruvia1/temp1/ONTPFC", headers);
//            response= macService.registroFichaTecnica(Integer.parseInt(version), Integer.parseInt(codMac), nombreArchivo, Integer.parseInt(estado), header);
//            if (response != null) {
//				response = util.getResponse(header, "0", response.getDataList());
//			} else {
//				response = util.getResponse(header, "2", null);
//			}
//
//        } catch (Exception e) {
//			LOGGER.error(this.getClass().getName(), e);
//			LOGGER.error(
//					"[SERVICIO: REGISTRAR CRITERIO DE EXCLUCION][" + header.getIdTransaccion() + "][FIN]");
//			return util.getResponse(header, "-2", null);
//		}
        
        return response;

	}	
}
