package pvt.auna.fcompleja.controller.api;

 
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.collections.CollectionUtils; 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.nio.file.Paths;

import pvt.auna.fcompleja.model.api.request.EmailSolicInscripRequest;
import pvt.auna.fcompleja.model.api.response.EmailSolicInscripResponse;
import pvt.auna.fcompleja.model.bean.EmailBean;
import pvt.auna.fcompleja.model.bean.Mail;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.service.EmailService;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.ConstanteUtil;

@RestController
public class SolicitudInscripController {
	
	byte[] document = null;
	
	private static final Logger log = LoggerFactory.getLogger(BusquedaMacController.class);
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private UsuarioService personaService;
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/solicitudInscrip")
	public ResponseEntity<List<EmailSolicInscripResponse>> solicitudInscrip(@RequestParam("file") MultipartFile file, @RequestParam("nombreMac") String nombreMac,
			 @RequestParam("medicoTratante") String medicoTratante,  @RequestParam("nroScgSolben") String nroScgSolben,
			 @RequestParam("medicoAuditor") String medicoAuditor) {
	    String uploadFile = "";
		EmailSolicInscripRequest solInsRequest = new EmailSolicInscripRequest();
		List<EmailSolicInscripResponse> listaUsuarioResponse = new ArrayList<>();
		List<EmailSolicInscripResponse> listaSolicInscrip = new ArrayList<>();
		
		Mail mail = new Mail();
		String cuerpo = new String();
		String asunto = new String();
	    //------------------------------------------------------------------------
	    try {
	    	solInsRequest.setNombreMac(nombreMac);
			solInsRequest.setMedicoTratante(medicoTratante);
			solInsRequest.setNroScgSolben(nroScgSolben);
			solInsRequest.setMedicoAuditor(medicoAuditor);
			
	    	byte[] byteFile = file.getBytes();
	        Path path = Paths.get(file.getOriginalFilename());
	        uploadFile = path.toString();
	        solInsRequest.setNombDocument(uploadFile);
	    	//OBTENEMOS LOS DATOS DEL CORREO PARA LA SOLICITUD DE INSCRIPCION
	    	EmailBean emailBean = emailService.obtenerDatosCorreo(ConstanteUtil.COD_EMAIL_SOLIC_INSCRIP);
//			asunto 	= emailBean.getAsunto().replace( "{{NOMBRE_MAC}}" , solInsRequest.getNombreMac() != null ? solInsRequest.getNombreMac() : "");
//			cuerpo 	= emailBean.getCuerpo().replace( "{{MEDICO_TRATANTE}}" , solInsRequest.getMedicoTratante() != null ? solInsRequest.getMedicoTratante() : "");
			cuerpo	= cuerpo.replace( "{{NOMBRE_MAC}}" , solInsRequest.getNombreMac() != null ? solInsRequest.getNombreMac() : "" );
			cuerpo	= cuerpo.replace( "{{NRO_SCG_SOLBEN}}" , solInsRequest.getNroScgSolben() != null ? solInsRequest.getNroScgSolben() : "");
			cuerpo	= cuerpo.replace( "{{MEDICO_AUDITOR}}" , solInsRequest.getMedicoAuditor() != null ? solInsRequest.getMedicoAuditor() : "");
			
			mail.setContent(cuerpo);
			mail.setSubject(asunto);
			
			//LISTAMOS LOS USUARIOS CON EL ROL RESPONSABLE DE MANTENIMIENTO MAC
			List<UsuarioBean> lista = personaService.listRolPersona(ConstanteUtil.COD_ROL_RESP_MANT_MAC);

			if(CollectionUtils.isNotEmpty(lista)) {
				for(UsuarioBean objeto : lista) {
					EmailSolicInscripResponse solInsResponse = new EmailSolicInscripResponse();
					mail.setTo(objeto.getUsuario().trim());
					listaSolicInscrip = emailService.sendSolicitudInscripcionMac(mail, solInsRequest, byteFile);
					solInsResponse.setCodUsuario(objeto.getCodUsuario());
					solInsResponse.setUsuario(objeto.getUsuario());
					solInsResponse.setEnvioCorreo(listaSolicInscrip.get(0).isEnvioCorreo());
					solInsResponse.setErrorCorreo(listaSolicInscrip.get(0).getErrorCorreo());
					listaUsuarioResponse.add(solInsResponse);
				}
				//mailReceptor = mailReceptor.substring(1, mailReceptor.length());
			} else {
				listaUsuarioResponse = null;
			}
	    	
	    } catch (IOException e) {
	    	log.error("error",e);
	        e.printStackTrace();
	    } catch (MessagingException e) {
	    	log.error("error",e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			log.error("error",e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return new ResponseEntity<List<EmailSolicInscripResponse>>(listaUsuarioResponse,HttpStatus.OK);
	}
}
