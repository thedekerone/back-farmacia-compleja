package pvt.auna.fcompleja.controller.api;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.LineaTratamientoRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.BandejaMonitoreoRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionMarcadorRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.MarcadorRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.MonitoreoEvolucionRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.SegEjecutivoRequest;
import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoResponse;
import pvt.auna.fcompleja.service.MonitoreoService;
import pvt.auna.fcompleja.util.ResponseCodesEnum;

@RestController
@RequestMapping("/")
public class BandejaMonitoreoController {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	MonitoreoService monitoreoService;

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/bandejaMonitoreo", produces = "application/json; charset=UTF-8")
	public WsResponse bandejaMonitoreo(@RequestBody BandejaMonitoreoRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: CONSULTAR MONITOREOS");
		LOGGER.info("[SERVICIO:  CONSULTAR MONITOREOS][INICIO]");
		LOGGER.info("[SERVICIO:  CONSULTAR MONITOREOS][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  CONSULTAR MONITOREOS][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  CONSULTAR MONITOREOS][REQUEST][BODY][" + request.toString() + "]");

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		headers.add("Content-Type", "application/json");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.listaMonitoreo(request, headers);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO:  CONSULTAR MONITOREOS][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error(" CONSULTAR MONITOREOS : ", e);
			LOGGER.info("[SERVICIO:  CONSULTAR MONITOREOS][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/listlineatratamiento", produces = "application/json; charset=UTF-8")
	public WsResponse listLineaTratamiento(@RequestBody LineaTratamientoRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: CONSULTAR LINEAS TRATAMIENTO");
		LOGGER.info("[SERVICIO:  CONSULTAR LINEAS TRATAMIENTO][INICIO]");
		LOGGER.info("[SERVICIO:  CONSULTAR LINEAS TRATAMIENTO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  CONSULTAR LINEAS TRATAMIENTO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion
				+ "]");
		LOGGER.info("[SERVICIO:  CONSULTAR LINEAS TRATAMIENTO][REQUEST][BODY][" + request.toString() + "]");

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.listLineaTratamiento(request, headers);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO:  CONSULTAR LINEAS TRATAMIENTO][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error(" CONSULTAR LINEAS TRATAMIENTOS : ", e);
			LOGGER.info("[SERVICIO:  CONSULTAR LINEAS TRATAMIENTO][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/getultlineatratamiento", produces = "application/json; charset=UTF-8")
	public WsResponse getUltLineaTratamiento(@RequestBody LineaTratamientoRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: CONSULTAR ULTIMA LINEA TRATAMIENTO");

		LOGGER.info("[SERVICIO:  CONSULTAR ULTIMA LINEA TRATAMIENTO][INICIO]");
		LOGGER.info("[SERVICIO:  CONSULTAR ULTIMA LINEA TRATAMIENTO][REQUEST][HEADER][idTransaccion][" + idTransaccion
				+ "]");
		LOGGER.info("[SERVICIO:  CONSULTAR ULTIMA LINEA TRATAMIENTO][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  CONSULTAR ULTIMA LINEA TRATAMIENTO][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.getUltimoLineaTratamiento(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: CONSULTAR ULTIMA LINEA TRATAMIENTO][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("CONSULTAR ULTIMA LINEA TRATAMIENTO : ", e);
			LOGGER.info("[SERVICIO: CONSULTAR ULTIMA LINEA TRATAMIENTO][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/listarmarcadores", produces = "application/json; charset=UTF-8")
	public WsResponse listarMarcadores(@RequestBody MarcadorRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: CONSULTAR LISTAR MARCADORES");

		LOGGER.info("[SERVICIO:  LISTAR MARCADORES][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR MARCADORES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR MARCADORES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR MARCADORES][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.consultarMarcadores(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: LISTAR MARCADORES][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("LISTAR MARCADORES : ", e);
			LOGGER.info("[SERVICIO: LISTAR MARCADORES][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/regdatosevolucion", produces = "application/json; charset=UTF-8")
	public WsResponse registrarDatosEvolucion(@RequestBody EvolucionRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: REG DATOS EVOLUCION");
		LOGGER.info("[SERVICIO:  REG DATOS EVOLUCION][INICIO]");
		LOGGER.info("[SERVICIO:  REG DATOS EVOLUCION][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REG DATOS EVOLUCION][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REG DATOS EVOLUCION][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.regDatosEvolucion(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: REG DATOS EVOLUCION][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("REG DATOS EVOLUCION : ", e);
			LOGGER.info("[SERVICIO: REG DATOS EVOLUCION][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/actdatosevolucion", produces = "application/json; charset=UTF-8")
	public WsResponse actualizarDatosEvolucion(@RequestBody EvolucionRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: ACT DATOS EVOLUCION");
		LOGGER.info("[SERVICIO:  ACT DATOS EVOLUCION][INICIO]");
		LOGGER.info("[SERVICIO:  ACT DATOS EVOLUCION][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACT DATOS EVOLUCION][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACT DATOS EVOLUCION][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.actDatosEvolucion(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: ACT DATOS EVOLUCION][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("ACT DATOS EVOLUCION : ", e);
			LOGGER.info("[SERVICIO: ACT DATOS EVOLUCION][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/listarevolucion", produces = "application/json; charset=UTF-8")
	public WsResponse listarEvolucion(@RequestBody EvolucionRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: CONSULTAR LISTAR EVOLUCIONES");

		LOGGER.info("[SERVICIO:  LISTAR EVOLUCIONES][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR EVOLUCIONES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR EVOLUCIONES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR EVOLUCIONES][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.consultarEvoluciones(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: LISTAR EVOLUCIONES][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("LISTAR EVOLUCIONES : ", e);
			LOGGER.info("[SERVICIO: LISTAR EVOLUCIONES][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/regresultadoevolucion", produces = "application/json; charset=UTF-8")
	public WsResponse RegResultadoEvolucion(@RequestBody EvolucionRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: REG RESULTADO EVOLUCION");
		LOGGER.info("[SERVICIO: REG RESULTADO EVOLUCION][INICIO]");
		LOGGER.info("[SERVICIO: REG RESULTADO EVOLUCION][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO: REG RESULTADO EVOLUCION][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO: REG RESULTADO EVOLUCION][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.regResultadoEvolucion(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: REG RESULTADO EVOLUCION][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("REG RESULTADO EVOLUCION : ", e);
			LOGGER.info("[SERVICIO: REG RESULTADO EVOLUCION][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/listardetalleevolucion", produces = "application/json; charset=UTF-8")
	public WsResponse listarDetalleEvolucion(@RequestBody EvolucionMarcadorRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: LIST DET EVOLUCION");

		LOGGER.info("[SERVICIO: LIST DET EVOLUCION][INICIO]");
		LOGGER.info("[SERVICIO: LIST DET EVOLUCION][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO: LIST DET EVOLUCION][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO: LIST DET EVOLUCION][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.consultarDetalleEvolucion(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: LIST DET EVOLUCION][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("LIST DET EVOLUCION : ", e);
			LOGGER.info("[SERVICIO: LIST DET EVOLUCION][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/listarhistorialmarcadores", produces = "application/json; charset=UTF-8")
	public WsResponse ListarHistorialMarcadores(@RequestBody MarcadorRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: HISTORIAL MARCADORES");
		LOGGER.info("[SERVICIO: HISTORIAL MARCADORES]");
		LOGGER.info("[SERVICIO: HISTORIAL MARCADORES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO: HISTORIAL MARCADORES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO: HISTORIAL MARCADORES][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.listarHistorialMarcadores(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: HISTORIAL MARCADORES][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("HISTORIAL MARCADORES : ", e);
			LOGGER.info("[SERVICIO: HISTORIAL MARCADORES][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/actualizarMonitoreoPendInfo", produces = "application/json; charset=UTF-8")
	public WsResponse actualizarMonitoreoPendInfo(@RequestBody EvolucionRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: MOD MONITOREO PENDIENTE INFO");
		LOGGER.info("[SERVICIO: MOD MONITOREO PENDIENTE INFO][INICIO]");
		LOGGER.info("[SERVICIO: MOD MONITOREO PENDIENTE INFO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO: MOD MONITOREO PENDIENTE INFO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion
				+ "]");
		LOGGER.info("[SERVICIO: MOD MONITOREO PENDIENTE INFO][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.actualizarMonitoreoPendInfo(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: MOD MONITOREO PENDIENTE INFO][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("MOD MONITOREO PENDIENTE INFO : ", e);
			LOGGER.info("[SERVICIO: MOD MONITOREO PENDIENTE INFO][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/regsegejecutivo", produces = "application/json; charset=UTF-8")
	public WsResponse regSeguimientoEjecutivo(@RequestBody SegEjecutivoRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: REGISTRAR SEGUIMIENTO EJECUTIVO");
		LOGGER.info("[SERVICIO:  REGISTRAR SEGUIMIENTO EJECUTIVO][INICIO]");
		LOGGER.info(
				"[SERVICIO:  REGISTRAR SEGUIMIENTO EJECUTIVO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR SEGUIMIENTO EJECUTIVO][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR SEGUIMIENTO EJECUTIVO][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.regSeguimientoEjecutivo(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: REGISTRAR SEGUIMIENTO EJECUTIVO][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("REGISTRAR SEGUIMIENTO EJECUTIVO : ", e);
			LOGGER.info("[SERVICIO: REGISTRAR SEGUIMIENTO EJECUTIVO][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/listarsegejecutivo", produces = "application/json; charset=UTF-8")
	public WsResponse ListarHistorialMarcadores(@RequestBody SegEjecutivoRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: LISTAR SEGUIMIENTO EJECUTIVO");
		LOGGER.info("[SERVICIO: LISTAR SEGUIMIENTO EJECUTIVO]");
		LOGGER.info("[SERVICIO: LISTAR SEGUIMIENTO EJECUTIVO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO: LISTAR SEGUIMIENTO EJECUTIVO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion
				+ "]");
		LOGGER.info("[SERVICIO: LISTAR SEGUIMIENTO EJECUTIVO][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.listarSeguimientoEjecutivo(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: LISTAR SEGUIMIENTO EJECUTIVO][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("LISTAR SEGUIMIENTO EJECUTIVO : ", e);
			LOGGER.info("[SERVICIO: LISTAR SEGUIMIENTO EJECUTIVO][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/getseguimientospendientes", produces = "application/json; charset=UTF-8")
	public WsResponse getSeguimientosPendientes(@RequestBody SegEjecutivoRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: GET SEGUIMIENTOS PENDIENTES");
		LOGGER.info("[SERVICIO: GET SEGUIMIENTOS PENDIENTES]");
		LOGGER.info("[SERVICIO: GET SEGUIMIENTOS PENDIENTES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info(
				"[SERVICIO: GET SEGUIMIENTOS PENDIENTES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO: GET SEGUIMIENTOS PENDIENTES][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.getSeguimientosPendientes(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: GET SEGUIMIENTOS PENDIENTES][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("GET SEGUIMIENTOS PENDIENTES : ", e);
			LOGGER.info("[SERVICIO: GET SEGUIMIENTOS PENDIENTES][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/actestsegpendientes", produces = "application/json; charset=UTF-8")
	public WsResponse actEstadoSegPendientes(@RequestBody SegEjecutivoRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: ACTUALIZAR ESTADO SEG PENDIENTES");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO SEG PENDIENTES][INICIO]");
		LOGGER.info(
				"[SERVICIO:  ACTUALIZAR ESTADO SEG PENDIENTES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO SEG PENDIENTES][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO SEG PENDIENTES][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.actEstadoSegPendientes(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: ACTUALIZAR ESTADO SEG PENDIENTES][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("ACTUALIZAR ESTADO SEG PENDIENTES : ", e);
			LOGGER.info("[SERVICIO: ACTUALIZAR ESTADO SEG PENDIENTES][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/getultregistromarcador", produces = "application/json; charset=UTF-8")
	public WsResponse getUltRegistroMarcador(@RequestBody EvolucionMarcadorRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: CONSULTAR ULTIMA REGISTRO MARCADOR");

		LOGGER.info("[SERVICIO:  CONSULTAR ULTIMA REGISTRO MARCADOR][INICIO]");
		LOGGER.info("[SERVICIO:  CONSULTAR ULTIMA REGISTRO MARCADOR][REQUEST][HEADER][idTransaccion][" + idTransaccion
				+ "]");
		LOGGER.info("[SERVICIO:  CONSULTAR ULTIMA REGISTRO MARCADOR][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  CONSULTAR ULTIMA REGISTRO MARCADOR][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.getUltRegistroMarcador(request);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: CONSULTAR ULTIMA REGISTRO MARCADOR][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("CONSULTAR ULTIMA REGISTRO MARCADOR : ", e);
			LOGGER.info("[SERVICIO: CONSULTAR ULTIMA REGISTRO MARCADOR][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/consdatosgrpdiag", produces = "application/json; charset=UTF-8")
	public WsResponse consDatosGrpDiagnostico(@RequestBody MonitoreoResponse request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: CONSULTAR DATOS GRUPO DIAGNOSTICO");

		LOGGER.info("[SERVICIO:  CONSULTAR DATOS GRUPO DIAGNOSTICO][INICIO]");
		LOGGER.info("[SERVICIO:  CONSULTAR DATOS GRUPO DIAGNOSTICO][REQUEST][HEADER][idTransaccion][" + idTransaccion
				+ "]");
		LOGGER.info("[SERVICIO:  CONSULTAR DATOS GRUPO DIAGNOSTICO][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  CONSULTAR DATOS GRUPO DIAGNOSTICO][REQUEST][BODY][" + request.toString() + "]");

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.consDatosGrpDiagnostico(request, headers);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: CONSULTAR DATOS GRUPO DIAGNOSTICO][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("CONSULTAR DATOS GRUPO DIAGNOSTICO : ", e);
			LOGGER.info("[SERVICIO: CONSULTAR DATOS GRUPO DIAGNOSTICO][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/getUltimoMonitoreo", produces = "application/json; charset=UTF-8")
	public WsResponse getUltimoMonitoreo(@RequestBody MonitoreoEvolucionRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: CONSULTA ULTIMO MONITOREO");

		LOGGER.info("[SERVICIO:  CONSULTA ULTIMO MONITOREO][INICIO]");
		LOGGER.info("[SERVICIO:  CONSULTA ULTIMO MONITOREO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info(
				"[SERVICIO:  CONSULTA ULTIMO MONITOREO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  CONSULTA ULTIMO MONITOREO][REQUEST][BODY][" + request.toString() + "]");

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = monitoreoService.getUltimoMonitoreo(request, headers);

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(service.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setData(service.getResponse());

			if (service.getCodResultado() != 0) {
				LOGGER.info("[SERVICIO: CONSULTA ULTIMO MONITOREO][FIN][CODIGO DE RESPUESTA DE ERROR : "
						+ service.getCodResultado() + "]");
				LOGGER.info(service.getMsgResultado());
			}
		} catch (Exception e) {
			LOGGER.error("CONSULTA ULTIMO MONITOREO : ", e);
			LOGGER.info("[SERVICIO: CONSULTA ULTIMO MONITOREO][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Error interno de servidor");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		}
		return response;
	}
}
