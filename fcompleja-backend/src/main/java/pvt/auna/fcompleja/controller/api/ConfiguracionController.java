package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.response.FiltroMACResponse;
import pvt.auna.fcompleja.model.bean.MACBean;
import pvt.auna.fcompleja.service.MacService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@RestController
@RequestMapping("/")
public class ConfiguracionController {
	
	private final Logger LOGGER = Logger.getLogger(getClass());

    @Autowired
    private MacService macService;

    @SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
    @PostMapping(value = "/api/filtrarMac", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public ResponseEntity<FiltroMACResponse> filtrarMAC(@RequestBody MACBean filtro) {
        
        String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: LISTAR MAC");

		LOGGER.info("[SERVICIO:  LISTAR MAC][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR MAC][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR MAC][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  LISTAR MAC][REQUEST][BODY][" + filtro.toString() + "]");
		
		FiltroMACResponse response = new FiltroMACResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;
		
        try {        	
        	resultado = macService.listFiltroMAC(filtro);
			if (resultado != null) {
				LOGGER.info("[SERVICIO:  LISTAR MAC][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<MACBean>) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error("[SERVICIO:  LISTAR MAC][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setDataList(null);
			}
			
			response.setAudiResponse(audiResponse);
			return new ResponseEntity<FiltroMACResponse>(response, HttpStatus.OK);
			
        } catch (Exception e) {
        	e.printStackTrace();
			LOGGER.error("[SERVICIO:  LISTAR MAC][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<FiltroMACResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
