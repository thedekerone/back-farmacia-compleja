package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.ListaExamenMedicoRequest;
import pvt.auna.fcompleja.model.api.response.ListaExamenMedicoResponse;
import pvt.auna.fcompleja.model.bean.CriteriosBean;
import pvt.auna.fcompleja.model.bean.ExamenMedicoBean;
import pvt.auna.fcompleja.model.bean.MarcadoresBean;
import pvt.auna.fcompleja.model.bean.ProductoAsociadoBean;
import pvt.auna.fcompleja.service.ConsultarExamenMedicoService;
import pvt.auna.fcompleja.service.MaestroService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@RestController
@RequestMapping("/")
public class MaestroController {

	@Autowired
	ConsultarExamenMedicoService consultarExamenMedicoService;
	
	@Autowired MaestroService maestroService;
	
	private final Logger LOGGER = Logger.getLogger(getClass());
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/api/ListaExamen", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ListaExamenMedicoResponse consultarExamenMedico(@RequestBody ListaExamenMedicoRequest ListaExamenMedico) {
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		
		ListaExamenMedicoResponse listaExamenMedicoResponse = new ListaExamenMedicoResponse();
		
		try {
			
			listaExamenMedicoResponse = consultarExamenMedicoService.consultarExamenMedico(ListaExamenMedico);
			
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("Error",e);
		}
		return listaExamenMedicoResponse;
	}
	
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/registroExamenMedico",produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> registroExamenMedico(@RequestBody ExamenMedicoBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR EXAMEN MEDICO");

		LOGGER.info("[SERVICIO:  REGISTRAR EXAMEN MEDICO][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR EXAMEN MEDICO][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR EXAMEN MEDICO][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR EXAMEN MEDICO][REQUEST][BODY][" + obj.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = maestroService.registroExamenesMedicos(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR EXAMEN MEDICO][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((ExamenMedicoBean) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  REGISTRAR EXAMEN MEDICO][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el Registro de Examenes Medicos");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  REGISTRAR EXAMEN MEDICO][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/listarExamenMedico",produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> listarExamenMedico(@RequestBody ExamenMedicoBean examenesMedicos ) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR LISTAR EXAMEN MEDICO");

		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR EXAMEN MEDICO][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR EXAMEN MEDICO][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR EXAMEN MEDICO][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR EXAMEN MEDICO][REQUEST][BODY][" +  "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = maestroService.listarExamenesMedicos(examenesMedicos);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR LISTAR EXAMEN MEDICO][REQUEST][BODY][Listar Examenes realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((ArrayList<ExamenMedicoBean>) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  REGISTRAR EXAMEN MEDICO][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el Listar de Examenes Medicos");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  REGISTRAR LISTAR EXAMEN MEDICO][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/registroMarcador",produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> registroMarcador(@RequestBody MarcadoresBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR MARCADOR");

		LOGGER.info("[SERVICIO:  REGISTRAR MARCADOR][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR MARCADOR][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR MARCADOR][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR MARCADOR][REQUEST][BODY][" + obj.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = maestroService.registroMarcadores(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR MARCADOR][REQUEST][BODY][Registro realizado correctamente]");
				audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
				audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
				response.setData(null);
			} else {
				LOGGER.error(
						"[SERVICIO:  REGISTRAR MARCADOR][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el Registro de Marcador");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  REGISTRAR MARCADOR][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/listarMarcadores",produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> listarMarcadores(@RequestBody MarcadoresBean marcadores) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR LISTAR MARCADORES");

		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR MARCADORES][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR MARCADORES][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR MARCADORES][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR MARCADORES][REQUEST][BODY][" + marcadores.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = maestroService.listarMarcaodres(marcadores);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR LISTAR MARCADORES][REQUEST][BODY][Listar Marcadores]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((ArrayList<MarcadoresBean>) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  REGISTRAR LISTAR MARCADORES][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el Listar Marcadores");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  REGISTRAR LISTAR MARCADORES][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/registroProductoAsociado",produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> registroProductoAsociado(@RequestBody ProductoAsociadoBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR PRODUCTO ASOCIADO");

		LOGGER.info("[SERVICIO:  REGISTRAR PRODUCTO ASOCIADO][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR PRODUCTO ASOCIADO][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR PRODUCTO ASOCIADO][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR PRODUCTO ASOCIADO][REQUEST][BODY][" + obj.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = maestroService.registroProductoAsociado(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR PRODUCTO ASOCIADO][REQUEST][BODY][Registro realizado correctamente]");
				audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
				audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
				response.setData(null);
			} else {
				LOGGER.error(
						"[SERVICIO:  REGISTRAR MARCADOR][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el Registro de Producto Asociado");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  REGISTRAR PRODUCTO ASOCIADO][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/listarProductoAsociado",produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> listarProductoAsociado(@RequestBody ProductoAsociadoBean productoAsociado) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR LISTAR PRODUCTO ASOCIADO");

		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR PRODUCTO ASOCIADO][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR PRODUCTO ASOCIADO][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR PRODUCTO ASOCIADO][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR LISTAR PRODUCTO ASOCIADO][REQUEST][BODY][" +productoAsociado.toString()+  "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = maestroService.listarProductoAsociado(productoAsociado);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR LISTAR PRODUCTO ASOCIADO][REQUEST][BODY][Listar Producto Asociado]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((ArrayList<ProductoAsociadoBean>) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  REGISTRAR LISTAR PRODUCTO ASOCIADO][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el Listar Producto Asociado");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  REGISTRAR LISTAR PRODUCTO ASOCIADO][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
}
