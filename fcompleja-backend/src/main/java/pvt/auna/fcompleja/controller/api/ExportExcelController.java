package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ClinicaRequest;
import pvt.auna.fcompleja.model.api.ExcelDownloadResponse;
import pvt.auna.fcompleja.model.api.ListaBandeja;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.ListaBandejaRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudesPreeliminarFiltroRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.BandejaMonitoreoRequest;
import pvt.auna.fcompleja.model.api.response.ListSolPreeliminarResponse;
import pvt.auna.fcompleja.model.bean.ClinicaBean;
import pvt.auna.fcompleja.model.bean.DiagnosticoBean;
import pvt.auna.fcompleja.model.bean.PacienteBean;
import pvt.auna.fcompleja.service.BandejaSolicitudEvaluacionService;
import pvt.auna.fcompleja.service.BandejaSolicitudPreliminarService;
import pvt.auna.fcompleja.service.MonitoreoService;
import pvt.auna.fcompleja.service.OncoClinicaService;
import pvt.auna.fcompleja.service.OncoDiagnosticoService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;
import pvt.auna.fcompleja.util.view.ExcelPreBandejaView;

@RestController
@RequestMapping("/")
public class ExportExcelController {

	private static final Logger log = LoggerFactory.getLogger(ExportExcelController.class);

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	OncoPacienteService pcteService;

	@Autowired
	OncoClinicaService clinService;
	
	@Autowired
	OncoDiagnosticoService diagService;

	AfiliadosRequest pacienteRequest;
	ClinicaRequest clinicaRequest;

	@Autowired
	BandejaSolicitudPreliminarService bandejaSolicitudesService;

	@Autowired
	BandejaSolicitudEvaluacionService bandejaSolicitudEvaluacionService;

	@Autowired
	MonitoreoService monitoreoService;

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(name = "downloadPreliminar", path = "downloadPreliminar/", produces = "application/json")
	@ResponseBody
	public ExcelDownloadResponse ExportExcelBandejaPreliminar(
			@RequestBody SolicitudesPreeliminarFiltroRequest filtroBean2, HttpServletRequest request) throws Exception {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		log.info("API method: EXCEL BANDEJA PRELIMINAR");

		log.info("[SERVICIO:  EXCEL BANDEJA PRELIMINAR][INICIO]");
		log.debug("[SERVICIO:  EXCEL BANDEJA PRELIMINAR][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.debug("[SERVICIO:  EXCEL BANDEJA PRELIMINAR][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.debug("[SERVICIO:  EXCEL BANDEJA PRELIMINAR][REQUEST][BODY][" + filtroBean2.toString() + "]");

		ApiOutResponse outResponse = null;
		ArrayList<ListSolPreeliminarResponse> listaSolPreliminar = null;

		try {
			outResponse = bandejaSolicitudesService.listaBandejaSolicitudes(filtroBean2);
			String codigoAfiliado = "";
			log.info(
					"[SERVICIO:  EXCEL BANDEJA PRELIMINAR][REQUEST][INFO][Proceso de la Transacción realizada con éxito]");
			listaSolPreliminar = (ArrayList<ListSolPreeliminarResponse>) outResponse.getResponse();
			if (listaSolPreliminar != null && listaSolPreliminar.size() > 0) {
				Integer total = 0;
				for (ListSolPreeliminarResponse obj : listaSolPreliminar) {
					if (codigoAfiliado.indexOf(obj.getCodAfiliado()) == -1) {
						total++;
						codigoAfiliado += "|" + obj.getCodAfiliado();
					}
				}

				ArrayList<PacienteBean> listaPaciente = obtenerNombresAfiliados(codigoAfiliado, total, headers);
				if (listaPaciente != null && !listaPaciente.isEmpty()) {
					for (ListSolPreeliminarResponse pre : listaSolPreliminar) {
						for (PacienteBean pcte : listaPaciente) {
							if (pre.getCodAfiliado().equalsIgnoreCase(pcte.getCodafir())) {
								pre.setNombrePaciente(pcte.getApelNomb());
								break;
							}
						}
					}
					log.info("[SERVICIO:  EXCEL BANDEJA PRELIMINAR][REQUEST][INFO][Paciente obtenidos exitosamente]");
				} else {
					outResponse.setCodResultado(1);
					outResponse.setMsgResultado("No se logró obtener datos del afiliado.");
				}

				ArrayList<ClinicaBean> listaClinicas = obtenerNombresClinicas(headers);
				if (listaClinicas != null && !listaClinicas.isEmpty()) {
					for (ListSolPreeliminarResponse pre : listaSolPreliminar) {
						for (ClinicaBean clinica : listaClinicas) {
							if (pre.getCodClinica().equalsIgnoreCase(clinica.getCodcli())) {
								pre.setDescClinica(clinica.getNomcli());
								break;
							}
						}
					}
					log.info("[SERVICIO:  BANDEJA PRELIMINAR][REQUEST][INFO][Clinicas obtenidos exitosamente]");
				} else {
					outResponse.setCodResultado(2);
					outResponse.setMsgResultado("No se logró obtener datos de las Clínicas.");
				}
			}

		} catch (Exception e) {

		}

		return ExcelPreBandejaView.exportPreliminar(request,
				(List<ListSolPreeliminarResponse>) outResponse.getResponse());
	}

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(name = "downloadEvaluacion", path = "downloadEvaluacion/", produces = "application/json")
	@ResponseBody
	public ExcelDownloadResponse ExportExcelBandejaEvaluacion(@RequestBody ListaBandejaRequest listaBandeja,
			HttpServletRequest request) throws Exception {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		log.info("API method: EXCEL BANDEJA PRELIMINAR");

		log.info("[SERVICIO:  EXCEL BANDEJA EVALUACION][INICIO]");
		log.debug("[SERVICIO:  EXCEL BANDEJA EVALUACION][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.debug("[SERVICIO:  EXCEL BANDEJA EVALUACION][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.debug("[SERVICIO:  EXCEL BANDEJA EVALUACION][REQUEST][BODY][" + listaBandeja.toString() + "]");

		ApiOutResponse outResponse = null;
		ArrayList<ListaBandeja> listaEvaluaciones = null;

		try {
			outResponse = bandejaSolicitudEvaluacionService.BandejaEvaluacion(listaBandeja);
			String codigoAfiliado = "";
			log.info(
					"[SERVICIO:  EXCEL BANDEJA PRELIMINAR][REQUEST][INFO][Proceso de la Transacción realizada con éxito]");
			listaEvaluaciones = (ArrayList<ListaBandeja>) outResponse.getResponse();
			if (listaEvaluaciones != null && listaEvaluaciones.size() > 0) {
				Integer total = 0;
				for (ListaBandeja obj : listaEvaluaciones) {
					if (codigoAfiliado.indexOf(obj.getCodigoPaciente()) == -1) {
						total++;
						codigoAfiliado += "|" + obj.getCodigoPaciente();
					}
				}

				ArrayList<PacienteBean> listaPaciente = obtenerNombresAfiliados(codigoAfiliado, total, headers);
				if (listaPaciente != null && !listaPaciente.isEmpty()) {
					for (ListaBandeja pre : listaEvaluaciones) {
						for (PacienteBean pcte : listaPaciente) {
							if (pre.getCodigoPaciente().equalsIgnoreCase(pcte.getCodafir())) {
								pre.setNombrePaciente(pcte.getApelNomb());
								break;
							}
						}
					}
					log.info("[SERVICIO:  EXCEL BANDEJA EVALUACION][REQUEST][INFO][Paciente obtenidos exitosamente]");
				} else {
					outResponse.setCodResultado(2);
					outResponse.setMsgResultado("No se logró obtener datos del afiliado.");
				}

				ArrayList<ClinicaBean> listaClinicas = obtenerNombresClinicas(headers);
				if (listaClinicas != null && !listaClinicas.isEmpty()) {
					for (ListaBandeja pre : listaEvaluaciones) {
						for (ClinicaBean clinica : listaClinicas) {
							if (pre.getCodigoClinica().equalsIgnoreCase(clinica.getCodcli())) {
								pre.setDescClinica(clinica.getNomcli());
								break;
							}
						}
					}
					log.info("[SERVICIO:  EXCEL BANDEJA EVALUACION][REQUEST][INFO][Clinicas obtenidos exitosamente]");
				} else {
					outResponse.setCodResultado(2);
					outResponse.setMsgResultado("No se logró obtener datos de las Clínicas.");
				}
				
				ArrayList<DiagnosticoBean> listaDiagnostico = obtenerNombresDiagnosticos(headers);
				if (listaDiagnostico != null && !listaDiagnostico.isEmpty()) {
					for (ListaBandeja pre : listaEvaluaciones) {
						for (DiagnosticoBean diagnostico : listaDiagnostico) {
							if (pre.getCodigoDiagnostico().equalsIgnoreCase(diagnostico.getCodigo())) {
								pre.setNombreDiagnostico(diagnostico.getDiagnostico());
								break;
							}
						}
					}
					log.info("[SERVICIO:  CONSULTAR EVALUACIONES][RESPONSE][INFO][Diagnósticos obtenidos exitosamente]");
				} else {
					outResponse.setCodResultado(2);
					outResponse.setMsgResultado("No se logró obtener datos de los diagnósticos.");
				}
			}

		} catch (Exception e) {

		}

		return ExcelPreBandejaView.exportEvaluacion(request, (ArrayList<ListaBandeja>) outResponse.getResponse());
	}

	@CrossOrigin(origins = "*")
	@PostMapping(name = "downloadMonitoreo", path = "downloadMonitoreo/", produces = "application/json")
	@ResponseBody
	public ExcelDownloadResponse exportExcelBandejaMonitoreo(@RequestBody BandejaMonitoreoRequest requestMonitoreo,
			HttpServletRequest request) throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", request.getParameter("idTransaccion"));
		headers.add("fechaTransaccion", request.getParameter("fechaTransaccion"));

		return ExcelPreBandejaView.exportMonitoreo(request,monitoreoService.listaMonitoreo(requestMonitoreo, headers).getResponse());
	}

	@SuppressWarnings("unchecked")
	private ArrayList<PacienteBean> obtenerNombresAfiliados(String codigoAfiliado, Integer total, HttpHeaders headers) {
		ArrayList<PacienteBean> listaPaciente = null;
		pacienteRequest = new AfiliadosRequest();
		pacienteRequest.setIni(ConstanteUtil.registroIni);
		pacienteRequest.setFin(total + 1);
		pacienteRequest.setCodafir(codigoAfiliado);

		try {
			ResponseOnc pacienteRes = pcteService.obtenerListaPacientexCodigos(pacienteRequest, headers);

			if (pacienteRes != null && Integer.parseInt(pacienteRes.getAudiResponse().getCodigoRespuesta()) >= 0) {
				listaPaciente = (ArrayList<PacienteBean>) pacienteRes.getDataList();
			} else {
				listaPaciente = null;
			}

		} catch (Exception e) {
			log.error(this.getClass().getName() + ".obtenerNombresAfiliados: " + e.getMessage());
			listaPaciente = null;
		}

		return listaPaciente;
	}

	@SuppressWarnings("unchecked")
	private ArrayList<ClinicaBean> obtenerNombresClinicas(HttpHeaders headers) {
		ArrayList<ClinicaBean> listaClinicas = null;

		clinicaRequest = new ClinicaRequest();
		clinicaRequest.setIni(ConstanteUtil.registroIni);
		clinicaRequest.setFin(ConstanteUtil.registroFinClinicas);
		clinicaRequest.setTipoBus(ConstanteUtil.tipoTodos);
		clinicaRequest.setCodcli("''");
		clinicaRequest.setNomcli("''");

		try {
			ResponseOnc clinicaRes = clinService.obtenerListaClinica(clinicaRequest, headers);

			if (clinicaRes != null && Integer.parseInt(clinicaRes.getAudiResponse().getCodigoRespuesta()) >= 0) {
				listaClinicas = (ArrayList<ClinicaBean>) clinicaRes.getDataList();
			} else {
				listaClinicas = null;
			}
		} catch (Exception e) {
			log.error(this.getClass().getName() + ".obtenerNombresClinicas: " + e.getMessage());
			listaClinicas = null;
		}

		return listaClinicas;
	}
	
	@SuppressWarnings("unchecked")
	private ArrayList<DiagnosticoBean> obtenerNombresDiagnosticos(HttpHeaders headers) {
		ArrayList<DiagnosticoBean> listaDiagnosticos = null;

		try {
			ResponseOnc diagResponse = diagService.getListarDiagnostico(headers);

			if (diagResponse != null && Integer.parseInt(diagResponse.getAudiResponse().getCodigoRespuesta()) >= 0) {
				listaDiagnosticos = (ArrayList<DiagnosticoBean>) diagResponse.getDataList();
			} else {
				listaDiagnosticos = null;
			}
		} catch (Exception e) {
			log.error(".obtenerNombresDiagnosticos: " + e.getMessage());
			listaDiagnosticos = null;
		}

		return listaDiagnosticos;
	}
}
