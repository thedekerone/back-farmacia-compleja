package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.response.ResponseGenerico;
import pvt.auna.fcompleja.model.bean.CheckListBean;
import pvt.auna.fcompleja.model.bean.ComplicacionesBean;
import pvt.auna.fcompleja.model.bean.CriteriosBean;
import pvt.auna.fcompleja.model.bean.FichaTecnicaBean;
import pvt.auna.fcompleja.model.bean.MACBean;
import pvt.auna.fcompleja.service.MacService;
import pvt.auna.fcompleja.util.AunaUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@RestController
@RequestMapping("/")
public class ConfiguracionDetalleController {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	@SuppressWarnings("rawtypes")
	AunaUtil util;

	@Autowired
	private MacService macService;

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/registrarMac", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> registroMAC(@RequestBody MACBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: REGISTRAR MAC");

		LOGGER.info("[SERVICIO:  REGISTRAR MAC][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR MAC][REQUEST][HEADER][idTransaccion][" + idTransaccion
				+ "]");
		LOGGER.info("[SERVICIO:  REGISTRAR MAC][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  REGISTRAR MAC][REQUEST][BODY][" + obj.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;
		try {
			resultado = macService.registroMAC(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR MAC][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((MACBean) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error("[SERVICIO:  REGISTRAR MAC][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/listCheckListConfig", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseGenerico> listarCheckListConf(@RequestBody CheckListBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: LISTAR INDICADORES");

		LOGGER.info("[SERVICIO:  LISTAR INDICADORES][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR INDICADORES][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR INDICADORES][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  LISTAR INDICADORES][REQUEST][BODY][" + obj.toString() + "]");

		ResponseGenerico responseFinal = new ResponseGenerico();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {

			resultado = macService.listaCheckListConfiguracion(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  LISTAR INDICADORES][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					responseFinal.setDataList((ArrayList<MACBean>) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					responseFinal.setDataList(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  LISTAR INDICADORES][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				responseFinal.setDataList(null);
			}

			responseFinal.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseGenerico>(responseFinal, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("[SERVICIO:  LISTAR INDICADORES][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			responseFinal.setAudiResponse(audiResponse);
			responseFinal.setDataList(null);
			return new ResponseEntity<ResponseGenerico>(responseFinal,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin(origins = "*")
	@PostMapping(
			value = "/api/registroCheckListConfig",
			produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> registroCheckListConfiguracion(
			@RequestBody CheckListBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR INDICADOR");

		LOGGER.info("[SERVICIO:  REGISTRAR INDICADOR][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR INDICADOR][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR INDICADOR][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  REGISTRAR INDICADOR][REQUEST][BODY][" + obj.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = macService.registrarCheckListConfiguracion(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR INDICADOR][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((CheckListBean) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  REGISTRAR INDICADOR][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  REGISTRAR INDICADOR][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(
			value = "/api/listarCriterioInclusion",
			produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseGenerico> listarCriterioInclusion(
			@RequestBody CriteriosBean obj) {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: LISTAR INCLUSION");

		LOGGER.info("[SERVICIO:  LISTAR INCLUSION][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR INCLUSION][REQUEST][HEADER][idTransaccion][" + idTransaccion
				+ "]");
		LOGGER.info("[SERVICIO:  LISTAR INCLUSION][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  LISTAR INCLUSION][REQUEST][BODY][" + obj.toString() + "]");

		ResponseGenerico response = new ResponseGenerico();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;
		try {
			resultado = macService.listarCriterioInclusion(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  LISTAR INCLUSION][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<CriteriosBean>) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  LISTAR INCLUSION][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseGenerico>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  LISTAR INCLUSION][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseGenerico>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/listarCriterioExclusion", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseGenerico> listarCriterioExclusion(
			@RequestBody CriteriosBean obj) {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: LISTAR EXCLUSION");

		LOGGER.info("[SERVICIO:  LISTAR EXCLUSION][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR EXCLUSION][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR EXCLUSION][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  LISTAR EXCLUSION][REQUEST][BODY][" + obj.toString() + "]");

		ResponseGenerico response = new ResponseGenerico();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = macService.listarCriterioExclusion(obj);
			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  LISTAR EXCLUSION][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<CriteriosBean>) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  LISTAR EXCLUSION][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseGenerico>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  LISTAR EXCLUSION][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseGenerico>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/registroCriterioExclusion", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> registroCriterioExclusion(@RequestBody CriteriosBean obj) {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR EXCLUSION");

		LOGGER.info("[SERVICIO:  REGISTRAR EXCLUSION][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR EXCLUSION][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR EXCLUSION][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR EXCLUSION][REQUEST][BODY][" + obj.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = macService.registroCriterioExclusion(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR EXCLUSION][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((CriteriosBean) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  REGISTRAR EXCLUSION][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  REGISTRAR EXCLUSION][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin(origins = "*")
	@PostMapping(
			value = "/api/registroCriterioInclusion",
			produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> registroCriterioInclusion(@RequestBody CriteriosBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR INCLUSION");

		LOGGER.info("[SERVICIO:  REGISTRAR INCLUSION][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR INCLUSION][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR INCLUSION][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR INCLUSION][REQUEST][BODY][" + obj.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = macService.registroCriterioInclusion(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR INCLUSION][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((CriteriosBean) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  REGISTRAR EXCLUSION][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  REGISTRAR EXCLUSION][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(
			value = "/api/listaVersionFichaTecnica",
			produces = "application/json; charset=UTF-8")
	public ResponseEntity<WsResponse> listarFichasTecnicas(
			@RequestBody FichaTecnicaBean obj) throws Exception {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: LISTAR FICHA TECNICA");

		LOGGER.info("[SERVICIO:  LISTAR FICHA TECNICA][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR FICHA TECNICA][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR FICHA TECNICA][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR FICHA TECNICA][REQUEST][BODY][" + obj.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = macService.listarFichasTecnicas(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  LISTAR INCLUSION][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((ArrayList<FichaTecnicaBean>) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  LISTAR INCLUSION][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta(
						"Error al obtener el listado de las Fichas Técnicas de la Mac: "
								+ obj.getCodMac());
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(
					"[SERVICIO:  LISTAR FICHA TECNICA][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin(origins = "*")
	@PostMapping(
			value = "/api/registroComplicacionesMedicas",
			produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> registroComplicacionesMedicas(
			@RequestBody ComplicacionesBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR COMPLICACIONES MEDICAS");

		LOGGER.info("[SERVICIO:  REGISTRAR COMPLICACIONES MEDICAS][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR COMPLICACIONES MEDICAS][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info(
				"[SERVICIO:  REGISTRAR COMPLICACIONES MEDICAS][REQUEST][HEADER][fechaTransaccion]["
						+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR COMPLICACIONES MEDICAS][REQUEST][BODY][" + obj.toString()
				+ "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = macService.registroComplicacionesMedicas(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR COMPLICACIONES MEDICAS][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((ComplicacionesBean) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado().toString());
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[REGISTRAR COMPLICACIONES MEDICAS][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el Registro de Complicaciones Médicas");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(
					"[SERVICIO:  REGISTRAR COMPLICACIONES MEDICAS][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(
			value = "/api/listarComplicacionesMedicas",
			produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseGenerico> listarComplicacionesMedicas(
			@RequestBody ComplicacionesBean obj) {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: LISTAR COMPLICACIONES MEDICAS");

		LOGGER.info("[SERVICIO:  LISTAR COMPLICACIONES MEDICAS][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR COMPLICACIONES MEDICAS][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR COMPLICACIONES MEDICAS][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  LISTAR COMPLICACIONES MEDICAS][REQUEST][BODY][" + obj.toString()
				+ "]");

		ResponseGenerico response = new ResponseGenerico();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = macService.listaComplicacionesMedicas(obj);
			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  LISTAR COMPLICACIONES MEDICAS][REQUEST][BODY][Consulta realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<ComplicacionesBean>) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado().toString());
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO:  LISTAR COMPLICACIONES MEDICAS][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el Lista de Complicaciones Médicas");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseGenerico>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(
					"[SERVICIO:  LISTAR COMPLICACIONES MEDICAS][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseGenerico>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@CrossOrigin(origins = "*")
	@PostMapping(
			value = "/api/registroFichaTecnica",
			produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> registroFichaTecnica(
			@RequestBody FichaTecnicaBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR FICHA TECNICA ");

		LOGGER.info("[SERVICIO:  REGISTRAR FICHA TECNICA ][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR FICHA TECNICA ][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info(
				"[SERVICIO:  REGISTRAR FICHA TECNICA ][REQUEST][HEADER][fechaTransaccion]["
						+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR FICHA TECNICA ][REQUEST][BODY][" + obj.toString()
				+ "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = macService.registroFichaTecnica(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO:  REGISTRAR FICHA TECNICA ][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((FichaTecnicaBean) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado().toString());
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[REGISTRAR FICHA TECNICA ][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el Registro de Ficha Técnica");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(
					"[SERVICIO:  REGISTRAR FICHA TECNICA ][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/registrarIndicacionCriterios", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> registrarIndicacionCriterios(@RequestBody CheckListBean obj) {//CriteriosBean
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		LOGGER.info("API method: REGISTRAR INDICACION");

		LOGGER.info("[SERVICIO:  REGISTRAR INDICACION][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR INDICACION][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR INDICACION][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR INDICACION][REQUEST][BODY][" + obj.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = macService.registrarIndicacionCriterios(obj);

			if (resultado != null) {
				LOGGER.info(
						"[SERVICIO: REGISTRAR INDICACION][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error(
						"[SERVICIO: REGISTRAR INDICACION][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("[SERVICIO: REGISTRAR INDICACION][REQUEST][BODY][Se encontraron errores]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
