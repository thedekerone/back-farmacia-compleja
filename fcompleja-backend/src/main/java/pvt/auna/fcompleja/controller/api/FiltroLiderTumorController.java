package pvt.auna.fcompleja.controller.api;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.FiltroLiderTumorRequest;
import pvt.auna.fcompleja.model.api.FiltroLiderTumorResponse;
import pvt.auna.fcompleja.model.api.ListFiltroLiderTumorResponse;
import pvt.auna.fcompleja.service.FiltroLiderTumorService;

@RestController
@RequestMapping("/")
public class FiltroLiderTumorController {

	@Autowired
	FiltroLiderTumorService FiltroLiderTumorService;

	private final Logger LOGGER = Logger.getLogger(getClass());

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/api/Lidertumor", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody FiltroLiderTumorResponse FiltroLiderTumor(
			@RequestBody FiltroLiderTumorRequest ListaLiderTumor) {
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");

		FiltroLiderTumorResponse FiltroLiderTumorResponse = new FiltroLiderTumorResponse();
		try {
			FiltroLiderTumorResponse = FiltroLiderTumorService.FiltroLiderTumor(ListaLiderTumor);
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("Error", e);
		}
		return FiltroLiderTumorResponse;
	}

}
