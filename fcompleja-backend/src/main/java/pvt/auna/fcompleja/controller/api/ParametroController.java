package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.ParametroBeanRequest;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.service.ParametroService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@RestController
@RequestMapping("/parametro-backend")
public class ParametroController {
	
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(UsuariosController.class);

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	AseguramientoPropiedades portalProp;
	
	@Autowired
	ParametroService parametroService;

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/buscar", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseOnc> buscarParametro(@RequestBody ParametroBeanRequest request) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: BUSCAR PARAMETROS");

		LOGGER.info("[SERVICIO: BUSCAR PARAMETROS][INICIO]");
		LOGGER.info("[SERVICIO: BUSCAR PARAMETROS][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO: BUSCAR PARAMETROS][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO: BUSCAR PARAMETROS][REQUEST][BODY][" + request.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = parametroService.buscarParametro(request, headers);
			
			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					LOGGER.info("[SERVICIO: BUSCAR PARAMETROS][REQUEST][BODY][Buscar realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<UsuarioBean>) resultado.getResponse());
				} else {
					LOGGER.error("[SERVICIO: BUSCAR PARAMETROS][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO: BUSCAR PARAMETROS][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error("[SERVICIO: BUSCAR PARAMETROS][REQUEST][FIN][Error en el servicio de Buscar Parametros]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio de Buscar Parametros.");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
