package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.*;
import pvt.auna.fcompleja.model.api.request.evaluacion.ParticipanteRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.BandejaMonitoreoRequest;
import pvt.auna.fcompleja.model.api.response.ResponseGenericoObject;
import pvt.auna.fcompleja.model.bean.ParticipanteBean;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.model.bean.UsuarioBeanRequest;
import pvt.auna.fcompleja.model.bean.UsuarioRolBean;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;
import pvt.auna.fcompleja.util.ResponseCodesEnum;

@RestController
@RequestMapping("/usuarios")
public class UsuariosController {

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(UsuariosController.class);

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	AseguramientoPropiedades portalProp;
	
	@Autowired
	UsuarioService userService;

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/portalComun/getListarRoles", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseOnc> getListarPorRol(@RequestBody UsuarioBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		// obj.setCodAplicacion(portalProp.getAplicacion());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: LISTAR POR ROL");

		LOGGER.info("[SERVICIO:  LISTAR ROLES][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR ROLES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR ROLES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  LISTAR ROLES][REQUEST][BODY][" + obj.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = userService.getListarRoles(obj, headers);

			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					LOGGER.info("[SERVICIO:  LISTAR POR ROL][REQUEST][BODY][Registro realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<UsuarioBean>) resultado.getResponse());
				} else {
					LOGGER.error("[SERVICIO:  LISTAR POR ROL][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO:  LISTAR POR ROL][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error("[SERVICIO:  LISTAR POR ROL][REQUEST][FIN][Error en el servicio para obtener roles]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio para obtener roles.");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/portalComun/getListarUsuariosPorRol", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseOnc> getListarUsuariosPorRol(@RequestBody UsuarioBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		obj.setCodAplicacion(portalProp.getAplicacion());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: USUARIOS POR ROL");

		LOGGER.info("[SERVICIO:  USUARIOS POR ROL][INICIO]");
		LOGGER.info("[SERVICIO:  USUARIOS POR ROL][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  USUARIOS POR ROL][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  USUARIOS POR ROL][REQUEST][BODY][" + obj.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = userService.getListarUsuariosxRol(obj, headers);

			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					LOGGER.info("[SERVICIO:  LISTAR POR ROL][REQUEST][BODY][Registro realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<UsuarioBean>) resultado.getResponse());
				} else {
					LOGGER.error("[SERVICIO:  LISTAR POR ROL][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO:  LISTAR POR ROL][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error("[SERVICIO:  LISTAR POR ROL][REQUEST][FIN][Error en el servicio de descarga]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio de descarga de archivos.");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
		
	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/listarUsuarioFarmacia", produces = "application/json; charset=UTF-8")
	public WsResponse listarUsuarioFarmacia(@RequestBody ParticipanteRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: CONSULTAR PARTICIPANTES");

		LOGGER.info("[SERVICIO:  CONSULTAR PARTICIPANTES][INICIO]");
		LOGGER.info("[SERVICIO:  CONSULTAR PARTICIPANTES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  CONSULTAR PARTICIPANTES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  CONSULTAR PARTICIPANTES][REQUEST][BODY][" + request.toString() + "]");
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		headers.add("Content-Type", "application/json");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = userService.listarUsuarioFarmacia(request, headers);

			if (service.getCodResultado() != -1) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());

			} else {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}

		} catch (Exception e) {
			// TODO: handle exception
			// TODO: handle exception
			LOGGER.error(" CONSULTAR MONITOREOS : ", e);
			LOGGER.info("[SERVICIO:  CONSULTAR MONITOREOS][FIN]");
		}
		return response;

	}
	
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/listarUsuarioFarmaciaDet", produces = "application/json; charset=UTF-8")
	public WsResponse listarUsuarioFarmaciaDet(@RequestBody ParticipanteRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: LISTAR PARTICIPANTES DETALLADO");

		LOGGER.info("[SERVICIO:  LISTAR PARTICIPANTES DETALLADO][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR PARTICIPANTES DETALLADO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR PARTICIPANTES DETALLADO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR PARTICIPANTES DETALLADO][REQUEST][BODY][" + request.toString() + "]");
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		headers.add("Content-Type", "application/json");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = userService.listarUsuarioFarmaciaDet(request, headers);

			if (service.getCodResultado() != -1) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());

			} else {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}

		} catch (Exception e) {
			
			LOGGER.error(" LISTAR PARTICIPANTES DETALLADO : ", e.getMessage());
			LOGGER.info("[SERVICIO:  LISTAR PARTICIPANTES DETALLADO][FIN]");
		}
		return response;

	}
	
	
	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/portalComun/listarUsuariosRol", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseOnc> getListarUsuariosRol(@RequestBody UsuarioRolRequest obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		obj.setCodAplicacion(portalProp.getAplicacion());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: USUARIOS - ROL");

		LOGGER.info("[SERVICIO:  USUARIOS - ROL][INICIO]");
		LOGGER.info("[SERVICIO:  USUARIOS - ROL][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  USUARIOS - ROL][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  USUARIOS - ROL][REQUEST][BODY][" + obj.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = userService.listarUsuarioRol(obj, headers);

			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					LOGGER.info("[SERVICIO:  USUARIOS - ROL][REQUEST][BODY][Registro realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<UsuarioBean>) resultado.getResponse());
				} else {
					LOGGER.error("[SERVICIO:  USUARIOS - ROL][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO:  USUARIOS - ROL][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error("[SERVICIO:  USUARIOS - ROL][REQUEST][FIN][Error en el servicio de listar Usuario - Rol]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio de listar Usuario - Rol .");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/portalComun/listarParticipantes", produces = "application/json; charset=UTF-8")
	public WsResponse listarParticipantes(@RequestBody ParticipanteBeanRequest request) throws Exception {
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());


		LOGGER.info("API method: LISTAR PARTICIPANTES ");

		LOGGER.info("[SERVICIO:  LISTAR PARTICIPANTES ][INICIO]");
		LOGGER.info("[SERVICIO:  LISTAR PARTICIPANTES ][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR PARTICIPANTES ][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  LISTAR PARTICIPANTES ][REQUEST][BODY][" + request.toString() + "]");
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = userService.listarParticipantes(request);

			if (service.getCodResultado() != -1) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());

			} else {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}

		} catch (Exception e) {
			
			LOGGER.error(" LISTAR PARTICIPANTES  : ", e.getMessage());
			LOGGER.info("[SERVICIO:  LISTAR PARTICIPANTES ][FIN]");
		}
		return response;

	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/portalComun/registrarParticipantes", produces = "application/json; charset=UTF-8")
	public WsResponse registrarParticipantes(@RequestBody ParticipanteBeanRequest request) throws Exception {
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());


		LOGGER.info("API method: REGISTRAR PARTICIPANTES ");

		LOGGER.info("[SERVICIO:  REGISTRAR PARTICIPANTES ][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR PARTICIPANTES ][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR PARTICIPANTES ][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR PARTICIPANTES ][REQUEST][BODY][" + request.toString() + "]");
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = userService.registrarParticipantes(request);

			if (service.getCodResultado() != -1) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());

			} else {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}

		} catch (Exception e) {
			
			LOGGER.error(" REGISTRAR PARTICIPANTES  : ", e.getMessage());
			LOGGER.info("[SERVICIO:  REGISTRAR PARTICIPANTES ][FIN]");
		}
		return response;

	}
	
	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/portalComun/listarUsuariosSeguridad", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseOnc> getListarUsuariosSeguridad(@RequestBody UsuarioSeguridadRequest obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		//obj.setCodAplicacion(portalProp.getAplicacion().toString());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: USUARIOS - SEGURIDAD");

		LOGGER.info("[SERVICIO:  USUARIOS - SEGURIDAD][INICIO]");
		LOGGER.info("[SERVICIO:  USUARIOS - SEGURIDAD][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  USUARIOS - SEGURIDAD][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  USUARIOS - SEGURIDAD][REQUEST][BODY][" + obj.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = userService.listarUsuarioSeguridad(obj, headers);

			if (resultado != null) {
				if (resultado.getCodResultado() >= 0) {
					LOGGER.info("[SERVICIO:  USUARIOS - SEGURIDAD][REQUEST][BODY][Listar realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<UsuarioBean>) resultado.getResponse());
				} else {
					LOGGER.error("[SERVICIO:  USUARIOS - SEGURIDAD][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO:  USUARIOS - SEGURIDAD][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error("[SERVICIO:  USUARIOS - ROL][REQUEST][FIN][Error en el servicio de Listar Usuario - Seguridad]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio de listar Usuario - Seguridad .");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/registrar", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseOnc> registrarUsuario(@RequestBody UsuarioBeanRequest request) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: REGISTRAR USUARIO");

		LOGGER.info("[SERVICIO: REGISTRAR USUARIO][INICIO]");
		LOGGER.info("[SERVICIO: REGISTRAR USUARIO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO: REGISTRAR USUARIO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO: REGISTRAR USUARIO][REQUEST][BODY][" + request.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = userService.registrarUsuario(request, headers);
			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					LOGGER.info("[SERVICIO: REGISTRAR USUARIO][REQUEST][BODY][Registro realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<UsuarioBean>) resultado.getResponse());
				} else {
					LOGGER.error("[SERVICIO: REGISTRAR USUARIO][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO: REGISTRAR USUARIO][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error("[SERVICIO: REGISTRAR USUARIO][REQUEST][FIN][Error en el servicio de Registrar Usuario]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio de Registrar Usuario.");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/actualizar", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseOnc> actualizarUsuario(@RequestBody UsuarioBeanRequest request) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: ACTUALIZAR USUARIO");

		LOGGER.info("[SERVICIO: ACTUALIZAR USUARIO][INICIO]");
		LOGGER.info("[SERVICIO: ACTUALIZAR USUARIO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO: ACTUALIZAR USUARIO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO: ACTUALIZAR USUARIO][REQUEST][BODY][" + request.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			resultado = userService.actualizarUsuario(request, headers);
			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					LOGGER.info("[SERVICIO: ACTUALIZAR USUARIO][REQUEST][BODY][Actualizar realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<UsuarioBean>) resultado.getResponse());
				} else {
					LOGGER.error("[SERVICIO: ACTUALIZAR USUARIO][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO: ACTUALIZAR USUARIO][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				LOGGER.error("[SERVICIO: ACTUALIZAR USUARIO][REQUEST][FIN][Error en el servicio de Actualizar Usuario]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio de Actualizar Usuario.");
				response.setDataList(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@CrossOrigin(origins = "*")
	@PostMapping(value = "/validarRegistroGrupo", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseGenericoObject> validarRegistroParticipante(@RequestBody ValidarRegParticipantRequest request) {


		LOGGER.info("[SERVICIO: VALIDAR REGISTRO PARTICIPANTE][INICIO]");
		LOGGER.info("[SERVICIO: VALIDAR REGISTRO PARTICIPANTE][REQUEST][BODY][" + request.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		ResponseGenericoObject resultado = null;

		try {
			resultado = userService.validarRegistroParticipante(request);

			return new ResponseEntity<>(resultado, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("validarRegistroParticipante",e);
			AudiResponse audiResponse=new AudiResponse();
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta("Se produjo un error al validar registro participante.");
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
