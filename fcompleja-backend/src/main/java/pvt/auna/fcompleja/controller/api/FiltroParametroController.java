package pvt.auna.fcompleja.controller.api;

import java.util.Date;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.FiltroParametroRequest;
import pvt.auna.fcompleja.model.api.FiltroParametroResponse;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.service.FiltroParametroService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;
import pvt.auna.fcompleja.util.ResponseCodesEnum;

@RestController
@RequestMapping("/")
public class FiltroParametroController {

	private static final org.slf4j.Logger log = LoggerFactory.getLogger(FiltroParametroController.class);

	@Autowired
	FiltroParametroService FiltroParametroService;

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/api/parametro", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody FiltroParametroResponse FiltroParametro(
			@RequestBody FiltroParametroRequest FiltroListaParametro) {
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		header.add("idTransaccion", GenericUtil.getUniqueID());
		header.add("fechaTransaccion", DateUtils.getDateToStringDDMMYYYY(new Date()));

		FiltroParametroResponse filtroResponse = new FiltroParametroResponse();

		try {
			filtroResponse = FiltroParametroService.FiltroParametro(FiltroListaParametro);

			if (filtroResponse != null) {
				filtroResponse.setFiltroParametro(filtroResponse.getFiltroParametro());
				filtroResponse.setCodigoResultado(filtroResponse.getCodigoResultado());
				filtroResponse.setMensageResultado(filtroResponse.getMensageResultado());
			} else {
				filtroResponse = new FiltroParametroResponse();
				filtroResponse.setFiltroParametro(null);
				filtroResponse.setCodigoResultado(-1);
				filtroResponse.setMensageResultado("Ocurrio un error al obtener lista de parametros.");
			}
		} catch (Exception e) {
			log.error("Error", e);
			filtroResponse.setCodigoResultado(-1);
			filtroResponse.setMensageResultado(e.getMessage());
			filtroResponse.setFiltroParametro(null);
		}

		return filtroResponse;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/consultaParam", produces = "application/json; charset=UTF-8")
	public WsResponse consultaParametroValue(@RequestBody FiltroParametroRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();
		log.info("API method: SERVICIO: CONSULTA PARAMETRO");

		log.debug("[SERVICIO: CONSULTA PARAMETRO VALUE][INICIO]");
		log.debug("[SERVICIO: CONSULTA PARAMETRO VALUE][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.debug("[SERVICIO: CONSULTA PARAMETRO VALUE][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.debug("[SERVICIO: CONSULTA PARAMETRO VALUE][REQUEST][BODY][" + request.getCodigoGrupo() + "]");
		log.debug("[SERVICIO: CONSULTA PARAMETRO VALUE][REQUEST][BODY][" + request.getCodigoParam() + "]");
		log.debug("[SERVICIO: CONSULTA PARAMETRO VALUE][REQUEST][BODY][" + request.getValueParam() + "]");

		try {

			ApiOutResponse service = FiltroParametroService.ConsultaParametroValue(request);

			if (service.getCodResultado() == 0) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());

			} else {
				log.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				log.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error(" CONSULTAR RESULTADO BASAL : ", e);
			log.info("[SERVICIO: SERVICIO: CONSULTA PARAMETRO VALUE][FIN]");
		}
		return response;
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/api/ControlParametro", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody FiltroParametroResponse parametro(@RequestBody FiltroParametroRequest request) {
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");

		FiltroParametroResponse FiltroParametroResponse = new FiltroParametroResponse();

		try {

			FiltroParametroResponse = FiltroParametroService.ControlFilaParametro(request);

			if (FiltroParametroResponse != null && !FiltroParametroResponse.getFiltroParametro().isEmpty()) {
				FiltroParametroResponse.setFiltroParametro(FiltroParametroResponse.getFiltroParametro());
				FiltroParametroResponse.setCodigoResultado(Integer.parseInt(ResponseCodesEnum.SUCCESS.getCode()));
				FiltroParametroResponse.setMensageResultado("Listado con exito");
			} else {
				log.info("Lista vacia");
				FiltroParametroResponse.setCodigoResultado(Integer.parseInt(ResponseCodesEnum.FAIL.getCode()));
				FiltroParametroResponse.setMensageResultado("No cuenta con lista de parametro");
			}
			// return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error", e);
			FiltroParametroResponse.setFiltroParametro(null);
			FiltroParametroResponse.setCodigoResultado(-1);
			FiltroParametroResponse.setMensageResultado(e.getMessage());
		}
		return FiltroParametroResponse;
	}
}
