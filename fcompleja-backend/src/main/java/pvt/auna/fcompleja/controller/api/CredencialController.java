package pvt.auna.fcompleja.controller.api;

import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.seguridad.CambioContrasenaRequest;
import pvt.auna.fcompleja.model.api.request.seguridad.EnvioCorreoContrasenaRequest;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;
import pvt.auna.fcompleja.util.constant.HeaderRequest;

import java.util.Date;

@RestController
@RequestMapping("/credencial")
public class CredencialController {

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CredencialController.class);

	private AseguramientoPropiedades portalProp;
	
	private UsuarioService userService;

	public CredencialController(AseguramientoPropiedades portalProp, UsuarioService userService) {
		this.portalProp = portalProp;
		this.userService = userService;
	}

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/cambiar",
				produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseOnc> cambiarCredenciales(@RequestBody CambioContrasenaRequest request) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		request.setCodAplicacion(portalProp.getAplicacion());

		HttpHeaders headers = new HttpHeaders();
		headers.add(HeaderRequest.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		headers.add(HeaderRequest.ID_TX, idTransaccion);
		headers.add(HeaderRequest.FECHA_TX, fechaTransaccion);

		LOGGER.info("[SERVICIO:  CAMBIAR CREDENCIALES][INICIO]");
		LOGGER.info("[SERVICIO:  CAMBIAR CREDENCIALES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  CAMBIAR CREDENCIALES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  CAMBIAR CREDENCIALES][REQUEST][BODY][" + request.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = userService.cambiarContrasena(request, headers);
			audiResponse.setCodigoRespuesta(resultado.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(resultado.getMsgResultado());

			response.setDataList(null);
			response.setAudiResponse(audiResponse);
			return new ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return generarInternalServerError(response, audiResponse);
		}
	}

	private ResponseEntity<ResponseOnc> generarInternalServerError(ResponseOnc response, AudiResponse audiResponse) {
		audiResponse.setCodigoRespuesta("-1");
		audiResponse.setMensajeRespuesta("Se produjo un error interno del servidor");
		response.setAudiResponse(audiResponse);
		response.setDataList(null);
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/recuperar",
				produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<ResponseOnc> recuperarCredencial(@RequestBody EnvioCorreoContrasenaRequest request) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		request.setCodAplicacion(portalProp.getAplicacion());

		HttpHeaders headers = new HttpHeaders();
		headers.add(HeaderRequest.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		headers.add(HeaderRequest.ID_TX, idTransaccion);
		headers.add(HeaderRequest.FECHA_TX, fechaTransaccion);

		LOGGER.info("[SERVICIO:  RECUPERAR CREDNCIALE][INICIO]");
		LOGGER.info("[SERVICIO:  RECUPERAR CREDNCIALE][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  RECUPERAR CREDNCIALE][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO: RECUPERAR CREDNCIALE][REQUEST][BODY][" + request.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado;

		try {
			resultado = userService.envioCorreoRecuperarContrasena(request, headers);
			audiResponse.setCodigoRespuesta(resultado.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
			response.setAudiResponse(audiResponse);
			response.setDataList(null);
			return new ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return generarInternalServerError(response, audiResponse);
		}
	}

	
}
