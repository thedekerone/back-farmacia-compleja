package pvt.auna.fcompleja.controller.api;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.FiltroUsrRolRequest;
import pvt.auna.fcompleja.model.api.FiltroUsrRolResponse;
import pvt.auna.fcompleja.service.FiltroUsrRolService;

@RestController
@RequestMapping("/")
public class FiltroUsrRolController {

	@Autowired
	FiltroUsrRolService filtroUsrRolService;
	
	private final Logger LOGGER = Logger.getLogger(getClass());
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/api/usuarioRol", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody FiltroUsrRolResponse FiltroUsuarioRol(@RequestBody FiltroUsrRolRequest FiltroListaUsuario) {
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		
		FiltroUsrRolResponse filtroUsrRolResponse = new FiltroUsrRolResponse();
		
		try {			
			filtroUsrRolResponse = filtroUsrRolService.FiltroUsuarioRol(FiltroListaUsuario);
			
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("Error",e);
		}
		return filtroUsrRolResponse;
	}
}
