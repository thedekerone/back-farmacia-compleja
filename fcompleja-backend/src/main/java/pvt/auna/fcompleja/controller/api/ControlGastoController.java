package pvt.auna.fcompleja.controller.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.config.ConfigFTPProp;
import pvt.auna.fcompleja.model.api.*;
import pvt.auna.fcompleja.model.bean.FiltroHistorialCargaBean;
import pvt.auna.fcompleja.model.bean.HistorialCargaGastosBean;
import pvt.auna.fcompleja.service.ControlGastoService;
import pvt.auna.fcompleja.service.FiltroParametroService;
import pvt.auna.fcompleja.service.GenericoService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping("/")
public class ControlGastoController {

	@Autowired
	AseguramientoPropiedades portalprop;	
	
    @Autowired
    private ControlGastoService controlGastoService;
    
    @Autowired
    private FiltroParametroService filtroParametro;

	@Autowired
	GenericoService service;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ConfigFTPProp ftpProp;

    private static final Logger log = LoggerFactory.getLogger(ControlGastoController.class);

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api/listaParametros", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ResponseEntity<ApiOutResponse> listaParametros() {   
    	ApiOutResponse  response = new ApiOutResponse();
    	try {    		    		    		
        		FiltroParametroRequest filtro = new FiltroParametroRequest();  
        		filtro.setCodigoGrupo("69");
        		filtro.setCodigoParametro(315);

                FiltroParametroResponse  resparam =  filtroParametro.ControlFilaParametro(filtro);
                response.setExtra(resparam.getFiltroParametro());
    	   		return new ResponseEntity<>(response, HttpStatus.OK);
	    } catch (Exception e) {
	        log.error("listaParametros",e);
	        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
    }   

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/api/ListarHistorialCarga", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<ApiOutResponse> ListarHistorialCarga(@RequestBody FiltroHistorialCargaBean filtro) {
		ApiOutResponse response = new ApiOutResponse();
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		AudiResponse audi = new AudiResponse(idTransaccion, fechaTransaccion);
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		ApiOutResponse outResponse = null;
		ArrayList<HistorialCargaGastosBean> listaDetalle = null;

		try {
			outResponse = controlGastoService.listArchivoCargadoGastos(filtro); 
			listaDetalle = (ArrayList<HistorialCargaGastosBean>) outResponse.getResponse();
			Integer total = listaDetalle.size();		
			response.setTotal(total);
			
			
			if (listaDetalle != null && listaDetalle.size() > 0) {
			} else {
				audi.setCodigoRespuesta(outResponse.getCodResultado() + "");
				audi.setMensajeRespuesta(outResponse.getMsgResultado());
			}
		} catch (Exception e) {
			audi.setCodigoRespuesta((outResponse != null) ? outResponse.getCodResultado() + "" : "99");
			audi.setMensajeRespuesta((outResponse != null) ? outResponse.getMsgResultado() : e.getMessage());
			response.setListResponse(null);
			e.printStackTrace();
		}

		audi.setCodigoRespuesta(
				(audi.getCodigoRespuesta() != null) ? audi.getCodigoRespuesta() : outResponse.getCodResultado() + "");
		audi.setMensajeRespuesta(
				(audi.getMensajeRespuesta() != null) ? audi.getMensajeRespuesta() : outResponse.getMsgResultado());

		
		response.setTotal(listaDetalle.size());
		response.setCodResultado(Integer.parseInt(audi.getCodigoRespuesta()));
		response.setMsgResultado(audi.getMensajeRespuesta());		
		response.setDataList(listaDetalle);

		if (response.getCodResultado().equals(0)
				|| response.getCodResultado().equals(1)
				|| response.getCodResultado().equals(2)) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}

    @CrossOrigin(origins = "*")
	@PostMapping(value = "/api/importarArchivo", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ApiOutResponse> saveImport(@RequestParam ("file") MultipartFile file,
													 @RequestParam ("codUsuario") String codUsuario,
													 @RequestParam ("nombres") String nombres ) {

    	ApiOutResponse  response = new ApiOutResponse();
    	try {	
    		response=controlGastoService.importFile(file,Integer.parseInt(codUsuario),nombres);
			controlGastoService.actualizarEstado();

			log.info("controller ",response.toString() );
    		
    		if(response.getCodResultado()==0) {
        		FiltroParametroRequest filtro = new FiltroParametroRequest();  
        		filtro.setCodigoGrupo("69");
        		filtro.setCodigoParametro(315);
        		try {
    				FiltroParametroResponse  resparam =  filtroParametro.ControlFilaParametro(filtro);
    				response.setExtra(resparam.getFiltroParametro());
    			} catch (Exception e) {
					log.info("controller ex filtro ",e.getMessage() );
    				e.printStackTrace();
    			}    		    			
    		}
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			log.info("controller ex ",e.getMessage() );
			controlGastoService.actualizarEstado();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}			
	
    }   
    

    @CrossOrigin(origins = "*")
	@PostMapping(value = "/api/obtieneArchivoLog", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> obtieneArchivoLog(@RequestBody ArchivoFTPBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		obj.setUsrApp(ftpProp.getUserApp());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;

		try {
			
			ArchivoFTPBean objAux = obj;
			String ruta =  "/";
			String nomArchivo= "CONTROL_GASTO_CARGA_" + obj.getCodArchivo()+ ".txt";
			objAux.setNomArchivo(nomArchivo);
			objAux.setRuta(ftpProp.getRutaFTP() + ruta);
			
			resultado = service.descargarArchivo(objAux, headers);

			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());					
					response.setData((ArchivoFTPBean) resultado.getResponse());
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio de descarga de archivos.");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}  
    
    
    @CrossOrigin(origins = "*")
  	@PostMapping(value = "/api/obtieneArchivoXls", produces = "application/json; charset=UTF-8")
  	@ResponseBody
  	public ResponseEntity<WsResponse> obtieneArchivoXls(@RequestBody ArchivoFTPBean obj) {
  		String idTransaccion = GenericUtil.getUniqueID();
  		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

  		obj.setUsrApp(ftpProp.getUserApp());
  		
  		HttpHeaders headers = new HttpHeaders();
  		headers.add("Content-Type", "application/json");
  		headers.add("idTransaccion", idTransaccion);
  		headers.add("fechaTransaccion", fechaTransaccion);

  		WsResponse response = new WsResponse();
  		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
  		ApiOutResponse resultado = null;
  		
  		
  		try {
  			
  			ArchivoFTPBean objAux = obj;
  			String ruta =  "/";
  			String nomArchivo= "CONTROL_GASTO_CARGA_" + obj.getCodArchivo()+ ".xls";
  			objAux.setNomArchivo(nomArchivo);
  			objAux.setRuta(ftpProp.getRutaFTP() + ruta);
  			
  			resultado = service.descargarArchivo(objAux, headers);

  			if (resultado != null) {
  				if (resultado.getCodResultado() == 0) {
  					audiResponse.setCodigoRespuesta("0");
  					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());					
  					response.setData((ArchivoFTPBean) resultado.getResponse());
  				} else {
  					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
  					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
  					response.setData(null);
  				}
  			} else {
  				audiResponse.setCodigoRespuesta("-3");
  				audiResponse.setMensajeRespuesta("Error en el servicio de descarga de archivos.");
  				response.setData(null);
  			}

  			response.setAudiResponse(audiResponse);
  			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

  		} catch (Exception e) {
  			log.error(e.getMessage());
  			audiResponse.setCodigoRespuesta("-1");
  			audiResponse.setMensajeRespuesta(e.getMessage());
  			response.setAudiResponse(audiResponse);
  			response.setData(null);
  			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
  		}
  	}      
    
}
