package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ListaBandeja;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.model.api.response.ResponseGenerico;
import pvt.auna.fcompleja.model.api.response.evaluacion.ProgramacionCmacDetResponse;
import pvt.auna.fcompleja.model.bean.EmailBean;
import pvt.auna.fcompleja.service.EmailService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;
import pvt.auna.fcompleja.util.ResponseCodesEnum;

@RestController
@RequestMapping("/")
public class EmailController {
	
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(EmailController.class);
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	EmailService emailService;
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/generarCorreo", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseGenerico> generarCorreo(@RequestBody EmailBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: GENERAR CORREO");

		LOGGER.info("[SERVICIO:  GENERAR CORREO][INICIO]");
		LOGGER.info("[SERVICIO:  GENERAR CORREO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  GENERAR CORREO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  GENERAR CORREO][REQUEST][BODY][" + obj.toString() + "]");
		
		ResponseGenerico resultado = new ResponseGenerico();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		
		try {
			resultado = emailService.generarCorreo(obj, headers);

			if (resultado != null) {
				LOGGER.info("[SERVICIO:  GENERAR CORREO][REQUEST][BODY][Se obtuvieron resultados correctamente]");
				return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.OK);
			} else {				
				LOGGER.error("[SERVICIO:  GENERAR CORREO][REQUEST][BODY][Se encontraron errores]");
				resultado = new ResponseGenerico();
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error al generar el envio del Correo.");
				resultado.setAudiResponse(audiResponse);
				resultado.setDataList(null);
				return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
			}	

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  GENERAR CORREO][REQUEST][BODY][Ocurrio un Error]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			resultado.setAudiResponse(audiResponse);
			resultado.setDataList(null);
			return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/enviarCorreo", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseGenerico> enviarCorreo(@RequestBody EmailBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: ENVIAR CORREO");

		LOGGER.info("[SERVICIO:  ENVIAR CORREO][INICIO]");
		LOGGER.info("[SERVICIO:  ENVIAR CORREO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ENVIAR CORREO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][" + obj.toString() + "]");
		
		ResponseGenerico resultado = new ResponseGenerico();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		
		try {
			resultado = emailService.enviarCorreo(obj, headers);

			if (resultado != null) {
				LOGGER.info("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][Se obtuvieron resultados correctamente]");
				return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.OK);
			} else {				
				LOGGER.error("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][Se encontraron errores]");
				resultado = new ResponseGenerico();
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error al generar el envio del Correo.");
				resultado.setAudiResponse(audiResponse);
				resultado.setDataList(null);
				return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
			}	

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][Ocurrio un Error]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			resultado.setAudiResponse(audiResponse);
			resultado.setDataList(null);
			return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/verificarEnvioCorreo", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseGenerico> verificarEnvioCorreo(@RequestBody EmailBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: GENERAR CORREO");

		LOGGER.info("[SERVICIO:  GENERAR CORREO][INICIO]");
		LOGGER.info("[SERVICIO:  GENERAR CORREO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  GENERAR CORREO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  GENERAR CORREO][REQUEST][BODY][" + obj.toString() + "]");
		
		ResponseGenerico resultado = new ResponseGenerico();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		
		try {

			resultado = emailService.verificarEnvioCorreo(obj,headers);

			if (resultado != null) {
				LOGGER.info("[SERVICIO:  GENERAR CORREO][REQUEST][BODY][Se obtuvieron resultados correctamente]");
				return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.OK);
			} else {				
				LOGGER.error("[SERVICIO:  GENERAR CORREO][REQUEST][BODY][Se encontraron errores]");
				resultado = new ResponseGenerico();
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error al generar el envio del Correo.");
				resultado.setAudiResponse(audiResponse);
				resultado.setDataList(null);
				return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
			}	

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  GENERAR CORREO][REQUEST][BODY][Ocurrio un Error]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			resultado.setAudiResponse(audiResponse);
			resultado.setDataList(null);
			return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/enviarCorreoReunionMac", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseGenerico> enviarCorreoReunionMac(@RequestBody EmailBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: ENVIAR CORREO");

		LOGGER.info("[SERVICIO:  ENVIAR CORREO][INICIO]");
		LOGGER.info("[SERVICIO:  ENVIAR CORREO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ENVIAR CORREO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][" + obj.toString() + "]");
		
		ResponseGenerico resultado = new ResponseGenerico();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		
		try {
			resultado = emailService.enviarCorreoReunionMac(obj, headers);

			if (resultado != null) {
				LOGGER.info("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][Se obtuvieron resultados correctamente]");
				return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.OK);
			} else {				
				LOGGER.error("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][Se encontraron errores]");
				resultado = new ResponseGenerico();
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error al generar el envio del Correo.");
				resultado.setAudiResponse(audiResponse);
				resultado.setDataList(null);
				return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
			}	

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][Ocurrio un Error]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			resultado.setAudiResponse(audiResponse);
			resultado.setDataList(null);
			return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/actParamsCorreoLiderTumor", produces = "application/json; charset=UTF-8")
	public WsResponse actParamsCorreoLiderTumor(@RequestBody SolicitudEvaluacionRequest request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: ACT ESTADO ENVIO EMAIL LIDER TUMOR");
		LOGGER.info("[SERVICIO:  ACT ESTADO ENVIO EMAIL LIDER TUMOR][INICIO]");
		LOGGER.info("[SERVICIO:  ACT ESTADO ENVIO EMAIL LIDER TUMOR][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACT ESTADO ENVIO EMAIL LIDER TUMOR][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACT ESTADO ENVIO EMAIL LIDER TUMOR][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = emailService.actParamsCorreoLiderTumor(request);

			if (service.getCodResultado() != -1) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());
			} else {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}
		} catch (Exception e) {
			LOGGER.error("ACT ESTADO ENVIO EMAIL LIDER TUMOR : ", e);
			LOGGER.info("[SERVICIO: ACT ESTADO ENVIO EMAIL LIDER TUMOR][FIN]");
		}
		return response;
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/reenviarCorreos", produces = "application/json; charset=UTF-8")
	public WsResponse reenviarCorreoLiderTumor(@RequestBody List<ListaBandeja> request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: REENVIO EMAIL PENDIENTES");
		LOGGER.info("[SERVICIO:  REENVIO EMAIL PENDIENTES][INICIO]");
		LOGGER.info("[SERVICIO:  REENVIO EMAIL PENDIENTES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  REENVIO EMAIL PENDIENTES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REENVIO EMAIL PENDIENTES][REQUEST][BODY][" + request.toString() + "]");

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		
		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		List<String> listLider = new ArrayList<>();
		List<String> listCmac = new ArrayList<>();
		List<String> listNoCumple = new ArrayList<>();
		try {
			//FILTRO EN CASO DE VARIAS SOLICTUDES PARA UNA REUNION CMAC
			request = filtrarSolicitudesCmac(request);
			
			for (ListaBandeja req : request) {
				if (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_AUTORIZADOR) && (!(req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_AUTORIZADOR) && req.getCodigoP().equals(ConstanteUtil.liderTumorMedicoTrata)))) {
					listLider.add("Solicitud " + req.getNumeroSolEvaluacion() + ": "+ emailService.reenviarCorreoLiderTumor(req, headers));
				} else {
					if (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_LIDER_TUMOR) || (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_AUTORIZADOR) && req.getCodigoP().equals(ConstanteUtil.liderTumorMedicoTrata))) {
						listCmac.add("Solicitud " + req.getNumeroSolEvaluacion() + ": "+ emailService.reenviarCorreoCmac(req, headers));
					} else {
						listNoCumple.add("Solicitud " + req.getNumeroSolEvaluacion() + ": El caso no corresponde a envio de correo");
					}
				}
			}

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(fechaTransaccion);
			audiResponse.setCodigoRespuesta("0");
			audiResponse.setMensajeRespuesta("Procesado correctamente");
			response.setAudiResponse(audiResponse);
			response.setData(unirListaDetalle(listLider, listCmac,listNoCumple));
		} catch (Exception e) {
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(fechaTransaccion);
			audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			LOGGER.error("REENVIO EMAIL PENDIENTES: ", e);
			LOGGER.info("[SERVICIO: REENVIO PENDIENTES][FIN]");
		}
		return response;
	}
	
	public List<ListaBandeja> filtrarSolicitudesCmac(List<ListaBandeja> lSolicitudes){
		//FILTRO DE SOLICTUDE SQUE SE REPITEN EN CASO DE PERTENECER A LA MISMA REUNION CMAC
		List<ListaBandeja> lFiltrado = new ArrayList<>();
		Map<Long,Long> map = new HashMap<>();
		
		for (ListaBandeja req : lSolicitudes) {
			if (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_LIDER_TUMOR) || (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_AUTORIZADOR) && req.getCodigoP().equals(ConstanteUtil.liderTumorMedicoTrata))) {
				Long codEnvio = req.getCodigoEnvioEnvMac();
				if(!map.containsKey(codEnvio)) {
					map.put(codEnvio, codEnvio);
					lFiltrado.add(req);
				}
			} else {
				lFiltrado.add(req);
			}
		}
		return lFiltrado;
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/actualizarCorreosPendientes", produces = "application/json; charset=UTF-8")
	public WsResponse actualizarCorreosPendientes(@RequestBody List<ListaBandeja> request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: ACTUALIZAR EMAIL PENDIENTES");
		LOGGER.info("[SERVICIO:  ACTUALIZAR EMAIL PENDIENTES][INICIO]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR EMAIL PENDIENTES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR EMAIL PENDIENTES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR EMAIL PENDIENTES][REQUEST][BODY][" + request.toString() + "]");

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		
		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		List<String> listLider = new ArrayList<>();
		List<String> listCmac = new ArrayList<>();
		List<String> listNoCumple = new ArrayList<>();
		try {
			for (ListaBandeja req : request) {
				if (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_AUTORIZADOR) && (!(req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_AUTORIZADOR) && req.getCodigoP().equals(ConstanteUtil.liderTumorMedicoTrata)))) {
					listLider.add("Solicitud " + req.getNumeroSolEvaluacion() + ": "+ emailService.actualizarCorreosPendLiderTumor(req, headers));
				} else {
					if (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_LIDER_TUMOR) || (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_AUTORIZADOR) && req.getCodigoP().equals(ConstanteUtil.liderTumorMedicoTrata))) {
						listCmac.add("Solicitud " + req.getNumeroSolEvaluacion() + ": "+ emailService.actualizarCorreosPendCmac(req, headers));
					} else {
						listNoCumple.add("Solicitud " + req.getNumeroSolEvaluacion() + ": El caso no corresponde a envio de correo");
					}
				}
			}

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(fechaTransaccion);
			audiResponse.setCodigoRespuesta("0");
			audiResponse.setMensajeRespuesta("Procesado correctamente");
			response.setAudiResponse(audiResponse);
			response.setData(unirListaDetalle(listLider, listCmac,listNoCumple));
		} catch (Exception e) {
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(fechaTransaccion);
			audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			LOGGER.error("ACTUALIZAR EMAIL PENDIENTES: ", e);
			LOGGER.info("[SERVICIO: ACTUALIZAR EMAIL PENDIENTES][FIN]");
		}
		return response;
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/actualizarCorreosPendientesV2", produces = "application/json; charset=UTF-8")
	public WsResponse actualizarCorreosPendientesV2(@RequestBody List<ListaBandeja> request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: ACTUALIZAR EMAIL PENDIENTES");
		LOGGER.info("[SERVICIO:  ACTUALIZAR EMAIL PENDIENTES][INICIO]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR EMAIL PENDIENTES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR EMAIL PENDIENTES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR EMAIL PENDIENTES][REQUEST][BODY][" + request.toString() + "]");

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		
		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();
		try {
			for (ListaBandeja req : request) {
				if (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_AUTORIZADOR) && (!(req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_AUTORIZADOR) && req.getCodigoP().equals(ConstanteUtil.liderTumorMedicoTrata))) && !(req.getCodigoEnvioEnvLiderTumor().equals(0L)) && (req.getEstadoCorreoEnvLiderTumor().equals(0))) {
					emailService.actualizarCorreosPendLiderTumorV2(req, headers);
				} else {
					if (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_LIDER_TUMOR) || (req.getCodigoEstadoEvaluacion().equals(ConstanteUtil.SOL_EVALUACION_OBS_AUTORIZADOR) && req.getCodigoP().equals(ConstanteUtil.liderTumorMedicoTrata)) && !(req.getCodigoEnvioEnvMac().equals(0L)) && (req.getEstadoCorreoEnvCmac().equals(0))) {
						emailService.actualizarCorreosPendCmacV2(req, headers);
					} else {
						LOGGER.info("[SERVICIO:  ACTUALIZAR EMAIL PENDIENTES][REQUEST][NO CORRESPONSE A ACTUALIZAR ESTADO CORREO]["+req.toString()+"]");
					}
				}
			}

			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(fechaTransaccion);
			audiResponse.setCodigoRespuesta("0");
			audiResponse.setMensajeRespuesta("Procesado correctamente");
			response.setAudiResponse(audiResponse);
			response.setData(request);
		} catch (Exception e) {
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(fechaTransaccion);
			audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			LOGGER.error("ACTUALIZAR EMAIL PENDIENTES: ", e);
			LOGGER.info("[SERVICIO: ACTUALIZAR EMAIL PENDIENTES][FIN]");
		}
		return response;
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/enviarCorreoGenerico", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<ResponseGenerico> enviarCorreoGenerico(@RequestBody EmailBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: ENVIAR CORREO");

		LOGGER.info("[SERVICIO:  ENVIAR CORREO][INICIO]");
		LOGGER.info("[SERVICIO:  ENVIAR CORREO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ENVIAR CORREO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][" + obj.toString() + "]");
		
		ResponseGenerico resultado = new ResponseGenerico();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		
		try {
			resultado = emailService.enviarCorreoGenerico(obj, headers);

			if (resultado != null) {
				LOGGER.info("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][Se obtuvieron resultados correctamente]");
				return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.OK);
			} else {				
				LOGGER.error("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][Se encontraron errores]");
				resultado = new ResponseGenerico();
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error al generar el envio del Correo.");
				resultado.setAudiResponse(audiResponse);
				resultado.setDataList(null);
				return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
			}	

		} catch (Exception e) {
			LOGGER.error("[SERVICIO:  ENVIAR CORREO][REQUEST][BODY][Ocurrio un Error]");
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("99");
			audiResponse.setMensajeRespuesta(e.getMessage());
			resultado.setAudiResponse(audiResponse);
			resultado.setDataList(null);
			return new ResponseEntity<ResponseGenerico>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/actualizarEstCorreoSolEvaluacion", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public WsResponse registrarDatosEvolucion(@RequestBody EmailBean request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: ACTUALIZAR ESTADO CORREO CMAC SOLICITUD EVALUACION");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO CORREO CMAC SOLICITUD EVALUACION][INICIO]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO CORREO CMAC SOLICITUD EVALUACION][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO CORREO CMAC SOLICITUD EVALUACION][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO CORREO CMAC SOLICITUD EVALUACION][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = emailService.actualizarEstCorreoSolEvaluacion(request);
			
			if (service.getCodResultado() != -1) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());

			} else {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}

		} catch (Exception e) {
			// TODO: handle exception
			// TODO: handle exception
			LOGGER.error("ACTUALIZAR ESTADO CORREO CMAC SOLICITUD EVALUACION : ", e);
			LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO CORREO CMAC SOLICITUD EVALUACION][FIN]");
			audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
			audiResponse.setMensajeRespuesta(e.getMessage());
		}
		return response;
	}
	
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/actualizarEstCorreoLidTumSolEvalucion", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public WsResponse actualizarEstCorreoLidTumSolEvalucion(@RequestBody EmailBean request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: ACTUALIZAR ESTADO CORREO LIDER TUM SOLICITUD EVALUACION");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO CORREO LIDER TUM SOLICITUD EVALUACION][INICIO]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO CORREO LIDER TUM SOLICITUD EVALUACION][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO CORREO LIDER TUM SOLICITUD EVALUACION][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO CORREO LIDER TUM SOLICITUD EVALUACION][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = emailService.actualizarEstCorreoLidTumSolEvalucion(request);
			
			if (service.getCodResultado() != -1) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());

			} else {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}

		} catch (Exception e) {
			// TODO: handle exception
			// TODO: handle exception
			LOGGER.error("ACTUALIZAR ESTADO CORREO LIDER TUM SOLICITUD EVALUACION : ", e);
			LOGGER.info("[SERVICIO:  ACTUALIZAR ESTADO CORREO LIDER TUM SOLICITUD EVALUACION][FIN]");
			audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
			audiResponse.setMensajeRespuesta(e.getMessage());
		}
		return response;
	}
	
	public List<String> unirListaDetalle(List<String> lLiderTum, List<String> lMiemCmac, List<String> lNoCumple){
		List<String> lFinal = new ArrayList<>();
		if(lLiderTum.size()>0) {
			lFinal.add("CORREOS A LIDER TUMOR");
			for(String msg: lLiderTum) {
				lFinal.add("*"+msg);
			}
			lFinal.add("");
		}
		
		if(lMiemCmac.size()>0) {
			lFinal.add("CORREOS A MIEMBROS CMAC");
			for(String msg: lMiemCmac) {
				lFinal.add("*"+msg);
			}
			lFinal.add("");
		}
		
		if(lNoCumple.size()>0) {
			lFinal.add("NO CORRESPONDE A VERIFICACION DE ESTADO");
			for(String msg: lNoCumple) {
				lFinal.add("*"+msg);
			}
			lFinal.add("");
		}
		
		return lFinal;
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/obtenerDatosAutorizadorPert", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public OncoWsResponse obtenerDatosAutorizadorPert(@RequestBody List<ProgramacionCmacDetResponse> request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: OBTENER DATOS AUTORIZADOR");
		LOGGER.info("[SERVICIO: OBTENER DATOS AUTORIZADOR][INICIO]");
		LOGGER.info("[SERVICIO: OBTENER DATOS AUTORIZADOR][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO: OBTENER DATOS AUTORIZADOR][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO: OBTENER DATOS AUTORIZADOR][REQUEST][BODY][" + request.toString() + "]");

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		
		AudiResponse audi = new AudiResponse();
		OncoWsResponse service=new OncoWsResponse();
		
		audi.setFechaTransaccion(fechaTransaccion);
		audi.setIdTransaccion(idTransaccion);
		try {
			ApiOutResponse api = emailService.obtenerDatosAutorizadorPert(request,headers);
			audi.setCodigoRespuesta(api.getCodResultado()+"");
			audi.setMensajeRespuesta(api.getMsgResultado());
			
			service.setAudiResponse(audi);
			service.setDataList(api.getResponse());
		} catch (Exception e) {
			LOGGER.error("error : OBTENER DATOS AUTORIZADOR ", e);
			audi.setCodigoRespuesta("-1");
			audi.setMensajeRespuesta(e.getMessage());
			service.setAudiResponse(audi);
			service.setDataList(new ArrayList<>());
		}
		return service;
	}
}
