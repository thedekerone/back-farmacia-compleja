package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ClinicaRequest;
import pvt.auna.fcompleja.model.api.GrupoDiagnosticoRequest;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.ListaPacienteRequest;
import pvt.auna.fcompleja.model.bean.ClinicaBean;
import pvt.auna.fcompleja.model.bean.PacienteBean;
import pvt.auna.fcompleja.service.OncoClinicaService;
import pvt.auna.fcompleja.service.OncoDiagnosticoService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.service.impl.OncoDiagnosticoServiceImpl;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@RestController
@RequestMapping("/")
public class OncosysController {
	private static final Logger log = LoggerFactory.getLogger(OncoDiagnosticoServiceImpl.class);

	@Autowired
	OncoDiagnosticoService diagnosticoService;

	@Autowired
	OncoPacienteService pacienteService;

	@Autowired
	OncoClinicaService clinicaService;

	@CrossOrigin(origins = "*")
	@GetMapping(value = "/api/listarGrupoDiagnostico", produces = "application/json")
	@ResponseBody
	public ResponseEntity<ResponseOnc> listarGrupoDiagnostico() {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		log.info("API method: LISTAR GRP DIAGNOSTICO");

		log.info("[SERVICIO:  LISTAR GRP DIAGNOSTICO][INICIO]");
		log.info("[SERVICIO:  LISTAR GRP DIAGNOSTICO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.info("[SERVICIO:  LISTAR GRP DIAGNOSTICO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.info("[SERVICIO:  LISTAR GRP DIAGNOSTICO][REQUEST][BODY][]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);

		try {
			response = diagnosticoService.getListarGrupoDiagnostico(headers);
			ArrayList<?> listaGrupo = null;

			if (response != null) {
				listaGrupo = (ArrayList<?>) response.getDataList();

				if (listaGrupo != null && listaGrupo.size() > 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta("Consulta exitosa");
				} else {
					audiResponse.setCodigoRespuesta("1");
					audiResponse.setMensajeRespuesta("No se encontraron datos.");
					listaGrupo = null;
				}

				response.setAudiResponse(audiResponse);
				response.setDataList((ArrayList<?>) listaGrupo);
				return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);
			} else {
				response = new ResponseOnc();
				response.setAudiResponse(audiResponse);
				response.getAudiResponse().setCodigoRespuesta("-3");
				response.getAudiResponse().setMensajeRespuesta("Error en Servicio Oncosys Diagnóstico (Grupo)");
				response.setDataList(null);
				return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (Exception e) {
			log.error("Error", e);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/buscarGrupoDiagnostico", produces = "application/json")
	@ResponseBody
	public ResponseEntity<ResponseOnc> buscarGrupoDiagnostico(@RequestBody GrupoDiagnosticoRequest request) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		log.info("API method: BUSCAR GRP DIAGNOSTICO");

		log.info("[SERVICIO:  BUSCAR GRP DIAGNOSTICO][INICIO]");
		log.info("[SERVICIO:  BUSCAR GRP DIAGNOSTICO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.info("[SERVICIO:  BUSCAR GRP DIAGNOSTICO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.info("[SERVICIO:  BUSCAR GRP DIAGNOSTICO][REQUEST][BODY][]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);

		try {
			response = diagnosticoService.buscarGrupoDiagnostico(headers, request);
			ArrayList<?> listaGrupo = null;

			if (response != null) {
				listaGrupo = (ArrayList<?>) response.getDataList();

				if (listaGrupo != null && listaGrupo.size() > 0) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta("Consulta exitosa");
				} else {
					audiResponse.setCodigoRespuesta("1");
					audiResponse.setMensajeRespuesta("No se encontraron datos.");
					listaGrupo = null;
				}

				response.setAudiResponse(audiResponse);
				response.setDataList((ArrayList<?>) listaGrupo);
				return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);
			} else {
				response = new ResponseOnc();
				response.setAudiResponse(audiResponse);
				response.getAudiResponse().setCodigoRespuesta("-3");
				response.getAudiResponse().setMensajeRespuesta("Error en Servicio Oncosys Diagnóstico (Grupo)");
				response.setDataList(null);
				return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (Exception e) {
			log.error("Error", e);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// BusClinica
	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/listarBusClinica", produces = "application/json")
	@ResponseBody
	public ResponseEntity<ResponseOnc> listarBusClinica(@RequestBody ClinicaRequest obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);

		log.info("API method: LISTAR CLINICAS");

		log.info("[SERVICIO:  LISTAR CLINICAS][INICIO]");
		log.info("[SERVICIO:  LISTAR CLINICAS][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.info("[SERVICIO:  LISTAR CLINICAS][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.info("[SERVICIO:  LISTAR CLINICAS][REQUEST][BODY][" + obj.toString() + "]");

		try {
			response = clinicaService.obtenerListaClinica(obj, headers);
			ArrayList<ClinicaBean> listaClinicas = (ArrayList<ClinicaBean>) response.getDataList();

			if (listaClinicas != null && listaClinicas.size() > 0) {
				audiResponse.setCodigoRespuesta("0");
				audiResponse.setMensajeRespuesta("Consulta exitosa");
			} else {
				audiResponse.setCodigoRespuesta("1");
				audiResponse.setMensajeRespuesta("No se encontraron datos.");
				listaClinicas = null;
			}

			response.setAudiResponse(audiResponse);
			response.setDataList((ArrayList<?>) listaClinicas);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);

		} catch (Exception e) {
			log.error("Error", e);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/listarBusquedaPaciente", produces = "application/json")
	@ResponseBody
	public ResponseEntity<ResponseOnc> ObteneBusquedaPaciente(@RequestBody ListaPacienteRequest request) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		log.info("API method: LISTAR PACIENTES");

		log.info("[SERVICIO:  LISTAR PACIENTES][INICIO]");
		log.debug("[SERVICIO:  LISTAR PACIENTES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.debug("[SERVICIO:  LISTAR PACIENTES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.debug("[SERVICIO:  LISTAR PACIENTES][REQUEST][BODY][" + request.toString() + "]");

		ResponseOnc response = new ResponseOnc();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);

		try {
			System.out.println("Entra al try");
			response = pacienteService.obtenerListaPacientexDatos(request, headers);

			if (response != null) {
				ArrayList<PacienteBean> listaPacientes = (ArrayList<PacienteBean>) response.getDataList();
				System.out.println(listaPacientes);
				if (listaPacientes != null ) {
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta("Consulta exitosa");
				} else {
					if(response.getAudiResponse().getMensajeRespuesta().equals("No se encontraron datos")) {
						audiResponse.setMensajeRespuesta(response.getAudiResponse().getMensajeRespuesta());
					}else {
						audiResponse.setMensajeRespuesta("Error al obtener el paciente.");//No se encontraron coincidencias.
					}
					audiResponse.setCodigoRespuesta("-2");
					listaPacientes = null;
				}

				response.setAudiResponse(audiResponse);
				response.setDataList((ArrayList<PacienteBean>) listaPacientes);
				return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);
			} else {
				response = new ResponseOnc();
				response.setAudiResponse(audiResponse);
				response.getAudiResponse().setCodigoRespuesta("-3");
				response.getAudiResponse().setCodigoRespuesta("Servidor Oncosys no responde");
				response.setDataList(null);
				return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			log.error("Error", e);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
