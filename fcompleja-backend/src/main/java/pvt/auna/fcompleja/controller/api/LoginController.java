package pvt.auna.fcompleja.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.request.LoginRequest;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.service.LoginService;


@RestController
public class LoginController {

	@Autowired
	LoginService loginService;

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/validarIntentoLogin", produces = MediaType.APPLICATION_JSON_VALUE)
	public OncoWsResponse validarIntentoLogin(@RequestBody LoginRequest loginrq,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		return loginService.validarIntentoLogin(loginrq, idTransaccion, fechaTransaccion);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/resetearReintentosLogin", produces = MediaType.APPLICATION_JSON_VALUE)
	public OncoWsResponse resetearReintentosLogin(@RequestBody LoginRequest loginrq,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		return loginService.resetearReintentosLogin(loginrq, idTransaccion, fechaTransaccion);
	}
}
