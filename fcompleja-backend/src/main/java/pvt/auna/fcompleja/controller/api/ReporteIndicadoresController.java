package pvt.auna.fcompleja.controller.api;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.ReporteIndicadoresRequest;
import pvt.auna.fcompleja.service.ReporteService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@RestController
@RequestMapping("/")
public class ReporteIndicadoresController {
	
	private final Logger LOGGER = Logger.getLogger(getClass());
	
	@Autowired
	ReporteService reporteService;
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/generarIndicador", produces = "application/json; charset=UTF-8")
	public ResponseEntity<WsResponse> generarIndicador(@RequestBody ReporteIndicadoresRequest request)
			throws Exception {
		
		LOGGER.info(" *** INGRESO *** [" + "/pub/generarIndicador" + "] ");
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());
		WsResponse wsResponse = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse apiOutResponse = new ApiOutResponse();
		
		apiOutResponse = reporteService.generarIndicador(request);

		/* Construir Response */

		if ( apiOutResponse.getCodResultado() == 0) {
			audiResponse.setCodigoRespuesta(apiOutResponse.getCodResultado().toString());
			audiResponse.setMensajeRespuesta("Los Indicadores  para el mes " + mesLetra(request.getMes()) + " y año " + request.getAno()+" se generaron de manera exitosa.");
		}
		else {
			audiResponse.setCodigoRespuesta(apiOutResponse.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(apiOutResponse.getMsgResultado());
				
		}
				
		wsResponse.setData(apiOutResponse.getResponse());
		wsResponse.setAudiResponse(audiResponse);
		
		
        return new ResponseEntity<WsResponse>(wsResponse, HttpStatus.OK);

	}
	
     private String mesLetra ( Integer mes) {
		
		String mesString;
		switch (mes) {
		        case 1:  mesString = "Enero";
		                 break;
		        case 2:  mesString  = "Febrero";
		                 break;
		        case 3:  mesString = "Marzo";
		                 break;
		        case 4:  mesString = "Abril";
		                 break;
		        case 5:  mesString = "Mayo";
		                 break;
		        case 6:  mesString = "Junio";
		                 break;
		        case 7:  mesString = "Julio";
		                 break;
		        case 8:  mesString = "Agosto";
		                 break;
		        case 9:  mesString = "Septiembre";
		                 break;
		        case 10: mesString = "Octubre";
		                 break;
		        case 11: mesString = "Noviembre";
		                 break;
		        case 12: mesString = "Diciembre";
		                 break;
		        default: mesString = "Mes NO Válido";
		                 break;
		        }
		
		
		return mesString;
		
		
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/descargarIndicadorExcel", produces = "application/json; charset=UTF-8")
	public ResponseEntity<WsResponse> descargarIndicadorExcel(@RequestBody ReporteIndicadoresRequest reporteRequest,
			HttpServletRequest request)
			throws Exception {
		LOGGER.info(" *** INGRESO *** [" + "/pub/descargarIndicadorExcel" + "] ");
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());
		WsResponse wsResponse = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse apiOutResponse = new ApiOutResponse();	

		      
		apiOutResponse = reporteService.generarIndicadoresExcel(reporteRequest,request);
		
         /* Construir Response */

 		if ( apiOutResponse.getCodResultado() == 0) {
 			audiResponse.setCodigoRespuesta("0");
 			audiResponse.setMensajeRespuesta("Los Indicadores En Excel ya fueron generados para el mes " + mesLetra(reporteRequest.getMes()) + " del " + reporteRequest.getAno());
 		}
 		else {
 			audiResponse.setCodigoRespuesta(apiOutResponse.getCodResultado().toString());
 			audiResponse.setMensajeRespuesta(apiOutResponse.getMsgResultado());
 				
 		}
 				
 		wsResponse.setData(apiOutResponse.getResponse());
 		wsResponse.setAudiResponse(audiResponse);
 		
 		
         return new ResponseEntity<WsResponse>(wsResponse, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/pub/generarIndicadorPorMedicamentoMAC", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<byte[]> generarIndicadorPorMedicamentoMAC(@RequestBody ReporteIndicadoresRequest request) throws Exception {
		LOGGER.info("[SERVICIO: generarIndicadorPorMedicamentoMAC][INICIO]");
        byte[] reporte;
        HttpEntity<byte[]> aux = null;
        try {
        	request.setTipo(1);
        	reporte = reporteService.generarIndicadoresConsumoPorMacExcel(request);
        	
        	HttpHeaders headers = new HttpHeaders();
        	
        	headers.add("Content-Description", "File Transfer");
			headers.add("Content-Disposition", "attachment; filename=indicadores.xlsx");
			headers.add("Content-Transfer-Encoding", "binary");
			headers.add("Connection", "Keep-Alive");
			
			headers.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
			
			aux = new HttpEntity<>(reporte, headers);
        } catch (Exception e) {
        	LOGGER.error("Error al generarIndicadorPorMedicamentoMAC: ", e);
            LOGGER.info("[SERVICIO: generarIndicadorPorMedicamentoMAC][FIN]");
            throw e;
        }
        return aux;
	}
	
	/*@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/generarIndicadorPorMedicamentoMAC", produces = "application/json; charset=UTF-8")
	public ResponseEntity<WsResponse> generarIndicadorPorMedicamentoMAC(@RequestBody ReporteIndicadoresRequest request)
			throws Exception {
			
		LOGGER.info(" *** INGRESO *** [" + "/pub/generarIndicadorPorMedicamentoMAC" + "] ");
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		WsResponse wsResponse = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse apiOutResponse = new ApiOutResponse();

		try {    

			apiOutResponse = reporteService.generarIndicadoresConsumoPorMacExcel(request);
	

		audiResponse.setCodigoRespuesta("0");
		audiResponse.setMensajeRespuesta("Indicador Por Medicamento MAC Generado");

		wsResponse.setData(apiOutResponse.getResponse());
		wsResponse.setAudiResponse(audiResponse);
		} catch (Exception e) {
			LOGGER.error(" *** ERROR *** [" + "/api/generarIndicadorPorMedicamentoMAC" + "] ");
			LOGGER.error(" *** ERROR *** [" + e);
			throw e;
		}
		
        return new ResponseEntity<WsResponse>(wsResponse, HttpStatus.OK);

	}*/
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/pub/generarIndicadorPorMedicamentoGrupoMAC", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<byte[]> generarIndicadorPorMedicamentoGrupoMAC(@RequestBody ReporteIndicadoresRequest request) throws Exception {
		LOGGER.info("[SERVICIO: generarIndicadorPorMedicamentoGrupoMAC][INICIO]");
        byte[] reporte;
        HttpEntity<byte[]> aux = null;
        try {
        	request.setTipo(2);
        	reporte = reporteService.generarIndicadoresConsumoPorMacExcel(request);
        	
        	HttpHeaders headers = new HttpHeaders();
        	
        	headers.add("Content-Description", "File Transfer");
			headers.add("Content-Disposition", "attachment; filename=indicadores.xlsx");
			headers.add("Content-Transfer-Encoding", "binary");
			headers.add("Connection", "Keep-Alive");
			
			headers.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
			
			aux = new HttpEntity<>(reporte, headers);
        } catch (Exception e) {
        	LOGGER.error("Error al generarIndicadorPorMedicamentoGrupoMAC: ", e);
            LOGGER.info("[SERVICIO: generarIndicadorPorMedicamentoGrupoMAC][FIN]");
            throw e;
        }
        return aux;
	}
	
	/*@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/generarIndicadorPorMedicamentoGrupoMAC", produces = "application/json; charset=UTF-8")
	public ResponseEntity<WsResponse> generarIndicadorPorMedicamentoGrupoMAC(@RequestBody ReporteIndicadoresRequest request)
			throws Exception {
			
		LOGGER.info(" *** INGRESO *** [" + "/pub/generarIndicadorPorMedicamentoGrupoMAC" + "] ");
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		WsResponse wsResponse = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse apiOutResponse = new ApiOutResponse();

		try {    
		    
			apiOutResponse = reporteService.generarIndicadoresConsumoPorGrupoMacExcel(request);

		audiResponse.setCodigoRespuesta("0");
		audiResponse.setMensajeRespuesta("Indicador Por Medicamento Grupo Diagnostico - MAC Generado");

		wsResponse.setData(apiOutResponse.getResponse());
		wsResponse.setAudiResponse(audiResponse);
		} catch (Exception e) {
			LOGGER.error(" *** ERROR *** [" + "/pub/generarIndicadorPorMedicamentoGrupoMAC" + "] ");
			LOGGER.error(" *** ERROR *** [" + e);
			throw e;
		}
		
        return new ResponseEntity<WsResponse>(wsResponse, HttpStatus.OK);

	}*/
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/pub/generarIndicadorPorMedicamentoGrupoMACLinea", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<byte[]> generarIndicadorPorMedicamentoGrupoMACLinea(@RequestBody ReporteIndicadoresRequest request) throws Exception {
		LOGGER.info("[SERVICIO: generarIndicadorPorMedicamentoGrupoMACLinea][INICIO]");
        byte[] reporte;
        HttpEntity<byte[]> aux = null;
        try {
        	request.setTipo(3);
        	reporte = reporteService.generarIndicadoresConsumoPorMacExcel(request);
        	
        	HttpHeaders headers = new HttpHeaders();
        	
        	headers.add("Content-Description", "File Transfer");
			headers.add("Content-Disposition", "attachment; filename=indicadores.xlsx");
			headers.add("Content-Transfer-Encoding", "binary");
			headers.add("Connection", "Keep-Alive");
			
			headers.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
			
			aux = new HttpEntity<>(reporte, headers);
        } catch (Exception e) {
        	LOGGER.error("Error al generarIndicadorPorMedicamentoGrupoMACLinea: ", e);
            LOGGER.info("[SERVICIO: generarIndicadorPorMedicamentoGrupoMACLinea][FIN]");
            throw e;
        }
        return aux;
	}
	
	/*@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/generarIndicadorPorMedicamentoGrupoMACLinea", produces = "application/json; charset=UTF-8")
	public ResponseEntity<WsResponse> generarIndicadorPorMedicamentoGrupoMACLinea(@RequestBody ReporteIndicadoresRequest request)
			throws Exception {
			
		LOGGER.info(" *** INGRESO *** [" + "/pub/generarIndicadorPorMedicamentoGrupoMACLinea" + "] ");
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		WsResponse wsResponse = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse apiOutResponse = new ApiOutResponse();

		try {    
		
			apiOutResponse = reporteService.generarIndicadoresConsumoPorGrupoMacLineaExcel(request);

		audiResponse.setCodigoRespuesta("0");
		audiResponse.setMensajeRespuesta("Indicador Por Medicamento Grupo Diagnostico - MAC - Linea Generado");

		wsResponse.setData(apiOutResponse.getResponse());
		wsResponse.setAudiResponse(audiResponse);
		} catch (Exception e) {
			LOGGER.error(" *** ERROR *** [" + "/pub/generarIndicadorPorMedicamentoGrupoMACLinea" + "] ");
			LOGGER.error(" *** ERROR *** [" + e);
			throw e;
		}
		
        return new ResponseEntity<WsResponse>(wsResponse, HttpStatus.OK);

	}*/
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/pub/generarIndicadorPorMedicamentoGrupoMACLineaTiempo", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<byte[]> generarIndicadorPorMedicamentoGrupoMACLineaTiempo(@RequestBody ReporteIndicadoresRequest request) throws Exception {
		LOGGER.info("[SERVICIO: generarIndicadorPorMedicamentoGrupoMACLineaTiempo][INICIO]");
        byte[] reporte;
        HttpEntity<byte[]> aux = null;
        try {
        	request.setTipo(4);
        	reporte = reporteService.generarIndicadoresConsumoPorMacExcel(request);
        	
        	HttpHeaders headers = new HttpHeaders();
        	
        	headers.add("Content-Description", "File Transfer");
			headers.add("Content-Disposition", "attachment; filename=indicadores.xlsx");
			headers.add("Content-Transfer-Encoding", "binary");
			headers.add("Connection", "Keep-Alive");
			
			headers.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
			
			aux = new HttpEntity<>(reporte, headers);
        } catch (Exception e) {
        	LOGGER.error("Error al generarIndicadorPorMedicamentoGrupoMACLineaTiempo: ", e);
            LOGGER.info("[SERVICIO: generarIndicadorPorMedicamentoGrupoMACLineaTiempo][FIN]");
            throw e;
        }
        return aux;
	}
	
	/*@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/generarIndicadorPorMedicamentoGrupoMACLineaTiempo", produces = "application/json; charset=UTF-8")
	public ResponseEntity<WsResponse> generarIndicadorPorMedicamentoGrupoMACLineaTiempo(@RequestBody ReporteIndicadoresRequest request)
			throws Exception {
			
		LOGGER.info(" *** INGRESO *** [" + "/pub/generarIndicadorPorMedicamentoGrupoMACLineaTiempo" + "] ");
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		WsResponse wsResponse = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse apiOutResponse = new ApiOutResponse();

		try {    

			apiOutResponse = reporteService.generarIndicadoresConsumoPorGrupoMacLineaTiempoExcel(request);

		audiResponse.setCodigoRespuesta("0");
		audiResponse.setMensajeRespuesta("Indicador Por Medicamento Grupo Diagnostico - MAC - Linea - Tiempo de Uso Generado");

		wsResponse.setData(apiOutResponse.getResponse());
		wsResponse.setAudiResponse(audiResponse);
		} catch (Exception e) {
			LOGGER.error(" *** ERROR *** [" + "/pub/generarIndicadorPorMedicamentoGrupoMACLineaTiempo" + "] ");
			LOGGER.error(" *** ERROR *** [" + e);
			throw e;
		}
		
        return new ResponseEntity<WsResponse>(wsResponse, HttpStatus.OK);

	}*/
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/generarReporteSolicitudAutorizaciones", produces = "application/json; charset=UTF-8")
	public ResponseEntity<WsResponse> generarReporteSolicitudAutorizaciones(@RequestBody ReporteIndicadoresRequest request)
			throws Exception {
			
		LOGGER.info(" *** INGRESO *** [" + "/pub/generarReporteSolicitudAutorizaciones" + "] ");
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		WsResponse wsResponse = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse apiOutResponse = new ApiOutResponse();

		try {    
		
			/*Reporte en PDF*/
			//apiOutResponse = reporteService.generarReporteIndicadorMedicamentoGrupoMACLinea(request);
			
			/*Reporte en Excel*/
			apiOutResponse = reporteService.generarReporteSolicitudesAutorizaciones(request);
			
			
		
		/* Construir Response */

		audiResponse.setCodigoRespuesta("0");
		audiResponse.setMensajeRespuesta("Reporte General de Ssolicitud de Autorizaciones - Generado");

		wsResponse.setData(apiOutResponse.getResponse());
		wsResponse.setAudiResponse(audiResponse);
		} catch (Exception e) {
			LOGGER.error(" *** ERROR *** [" + "/pub/generarReporteSolicitudAutorizaciones" + "] ");
			LOGGER.error(" *** ERROR *** [" + e);
			throw e;
		}
		
        return new ResponseEntity<WsResponse>(wsResponse, HttpStatus.OK);

	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/generarReporteMonitoreo", produces = "application/json; charset=UTF-8")
	public ResponseEntity<WsResponse> generarReporteMonitoreo(@RequestBody ReporteIndicadoresRequest request)
			throws Exception {
			
		LOGGER.info(" *** INGRESO *** [" + "/pub/generarReporteMonitoreo" + "] ");
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		WsResponse wsResponse = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse apiOutResponse = new ApiOutResponse();

		try {    
		
			/*Reporte en PDF*/
			//apiOutResponse = reporteService.generarReporteIndicadorMedicamentoGrupoMACLinea(request);
			
			/*Reporte en Excel*/
			apiOutResponse = reporteService.generarReporteSolicitudesMonitoreo(request);
			
			
		
		/* Construir Response */

		audiResponse.setCodigoRespuesta("0");
		audiResponse.setMensajeRespuesta("Reporte General de Monitoreo - Generado");

		wsResponse.setData(apiOutResponse.getResponse());
		wsResponse.setAudiResponse(audiResponse);
		} catch (Exception e) {
			LOGGER.error(" *** ERROR *** [" + "/pub/generarReporteMonitoreo" + "] ");
			LOGGER.error(" *** ERROR *** [" + e);
			throw e;
		}
		
        return new ResponseEntity<WsResponse>(wsResponse, HttpStatus.OK);

	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/generarReportesGenerales", produces = "application/json; charset=UTF-8")
	public ResponseEntity<WsResponse> generarReportesGenerales(@RequestBody ReporteIndicadoresRequest request)
			throws Exception {
			
		LOGGER.info(" *** INGRESO *** [" + "/pub/generarReportesGenerales" + "] ");
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		WsResponse wsResponse = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse apiOutResponse = new ApiOutResponse();

		try {    
		
			
			/*Proceso que Genera los Datos en Tabla*/
			apiOutResponse = reporteService.generarReporteGenerales(request);
			
			/* Construir Response */
			
		if(apiOutResponse.getCodResultado() == null) {
			audiResponse.setCodigoRespuesta("-2");
			audiResponse.setMensajeRespuesta("Error de Base de Datos Reportes Generales");
		} else {
			audiResponse.setCodigoRespuesta(apiOutResponse.getCodResultado().toString());
			audiResponse.setMensajeRespuesta(apiOutResponse.getMsgResultado());
		}
		
		wsResponse.setData(apiOutResponse.getResponse());
		wsResponse.setAudiResponse(audiResponse);
		} catch (Exception e) {
			LOGGER.error(" *** ERROR *** [" + "/pub/generarReportesGenerales" + "] ");
			LOGGER.error(" *** ERROR *** [" + e);
			throw e;
		}
		
        return new ResponseEntity<WsResponse>(wsResponse, HttpStatus.OK);

	}
	

}
