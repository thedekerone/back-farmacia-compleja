package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ClinicaRequest;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudesPreeliminarFiltroRequest;
import pvt.auna.fcompleja.model.api.response.ListSolPreeliminarResponse;
import pvt.auna.fcompleja.model.bean.ClinicaBean;
import pvt.auna.fcompleja.model.bean.PacienteBean;
import pvt.auna.fcompleja.service.BandejaSolicitudPreliminarService;
import pvt.auna.fcompleja.service.OncoClinicaService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@RestController
@RequestMapping("/")
public class BandejaSolicitudPreliminarController {

	@Autowired
	BandejaSolicitudPreliminarService banSolPreService;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	OncoPacienteService pcteService;

	@Autowired
	OncoClinicaService clinService;

	AfiliadosRequest pacienteRequest;
	ClinicaRequest clinicaRequest;

	private static final Logger log = LoggerFactory.getLogger(BandejaSolicitudPreliminarController.class);

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/api/BandejaSolicitudes", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<ResponseOnc> BandejaSolicitudes(@RequestBody SolicitudesPreeliminarFiltroRequest filtroBean) {

		ResponseOnc response = new ResponseOnc();
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		AudiResponse audi = new AudiResponse(idTransaccion, fechaTransaccion);
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		ApiOutResponse outResponse = null;
		ArrayList<ListSolPreeliminarResponse> listaSolPreliminar = null;

		log.info("API method: BANDEJA PRELIMINAR");

		log.info("[SERVICIO:  BANDEJA PRELIMINAR][INICIO]");
		log.debug("[SERVICIO:  BANDEJA PRELIMINAR][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.debug("[SERVICIO:  BANDEJA PRELIMINAR][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.debug("[SERVICIO:  BANDEJA PRELIMINAR][REQUEST][BODY][" + filtroBean.toString() + "]");

		try {
			outResponse = banSolPreService.listaBandejaSolicitudes(filtroBean);
			String codigoAfiliado = "";
			log.info("[SERVICIO:  BANDEJA PRELIMINAR][REQUEST][INFO][Proceso de la Transacción realizada con éxito]");
			listaSolPreliminar = (ArrayList<ListSolPreeliminarResponse>) outResponse.getResponse();

			if (listaSolPreliminar != null && listaSolPreliminar.size() > 0) {
				Integer total = 0;
				for (ListSolPreeliminarResponse obj : listaSolPreliminar) {
					if (codigoAfiliado.indexOf(obj.getCodAfiliado()) == -1) {
						total++;
						codigoAfiliado += "|" + obj.getCodAfiliado();
					}
				}

				ArrayList<PacienteBean> listaPaciente = obtenerNombresAfiliados(codigoAfiliado, total, headers);
				if (listaPaciente != null && !listaPaciente.isEmpty()) {
					for (ListSolPreeliminarResponse pre : listaSolPreliminar) {
						for (PacienteBean pcte : listaPaciente) {
							if (pre.getCodAfiliado().equalsIgnoreCase(pcte.getCodafir())) {
								pre.setNombrePaciente(pcte.getApelNomb());
								break;
							}
						}
					}
					log.info("[SERVICIO:  BANDEJA PRELIMINAR][REQUEST][INFO][Paciente obtenidos exitosamente]");
				} else {
					audi.setCodigoRespuesta("1");
					audi.setMensajeRespuesta("No se logró obtener datos del afiliado.");
				}

				ArrayList<ClinicaBean> listaClinicas = obtenerNombresClinicas(headers);
				if (listaClinicas != null && !listaClinicas.isEmpty()) {
					for (ListSolPreeliminarResponse pre : listaSolPreliminar) {
						for (ClinicaBean clinica : listaClinicas) {
							if (pre.getCodClinica().equalsIgnoreCase(clinica.getCodcli())) {
								pre.setDescClinica(clinica.getNomcli());
								break;
							}
						}
					}
					log.info("[SERVICIO:  BANDEJA PRELIMINAR][REQUEST][INFO][Clinicas obtenidos exitosamente]");
				} else {
					audi.setCodigoRespuesta("2");
					audi.setMensajeRespuesta("No se logró obtener datos de las Clínicas.");
				}
			} else {
				audi.setCodigoRespuesta(outResponse.getCodResultado() + "");
				audi.setMensajeRespuesta(outResponse.getMsgResultado());
			}
		} catch (Exception e) {
			audi.setCodigoRespuesta((outResponse != null) ? outResponse.getCodResultado() + "" : "99");
			audi.setMensajeRespuesta((outResponse != null) ? outResponse.getMsgResultado() : e.getMessage());
			response.setDataList(null);
			log.error(this.getClass().getName() + ".BandejaSolicitudes: " + e.getMessage());
		}

		audi.setCodigoRespuesta(
				(audi.getCodigoRespuesta() != null) ? audi.getCodigoRespuesta() : outResponse.getCodResultado() + "");
		audi.setMensajeRespuesta(
				(audi.getMensajeRespuesta() != null) ? audi.getMensajeRespuesta() : outResponse.getMsgResultado());

		response.setAudiResponse(audi);
		response.setTotal(outResponse.getTotal());
		response.setDataList(listaSolPreliminar);

		if (response.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")
				|| response.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("1")
				|| response.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("2")) {
			log.info("[SERVICIO:  BANDEJA PRELIMINAR][REQUEST][INFO][Response generado exitosamente]");
			log.info("asdas");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} else {
			log.info("[SERVICIO:  BANDEJA PRELIMINAR][REQUEST][INFO][Response generado con errores]");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}

	@SuppressWarnings("unchecked")
	private ArrayList<PacienteBean> obtenerNombresAfiliados(String codigoAfiliado, Integer total, HttpHeaders headers) {
		ArrayList<PacienteBean> listaPaciente = null;
		pacienteRequest = new AfiliadosRequest();
		pacienteRequest.setIni(ConstanteUtil.registroIni);
		pacienteRequest.setFin(total + 1);
		pacienteRequest.setCodafir(codigoAfiliado);

		try {
			ResponseOnc pacienteRes = pcteService.obtenerListaPacientexCodigos(pacienteRequest, headers);
			
			if(pacienteRes != null && Integer.parseInt(pacienteRes.getAudiResponse().getCodigoRespuesta()) >= 0) {
				listaPaciente = (ArrayList<PacienteBean>) pacienteRes.getDataList();				
			} else {
				listaPaciente = null;
			}			

		} catch (Exception e) {
			log.error(".obtenerNombresAfiliados: " + e.getMessage());
			listaPaciente = null;
		}

		return listaPaciente;
	}

	@SuppressWarnings("unchecked")
	private ArrayList<ClinicaBean> obtenerNombresClinicas(HttpHeaders headers) {
		ArrayList<ClinicaBean> listaClinicas = null;

		clinicaRequest = new ClinicaRequest();
		clinicaRequest.setIni(ConstanteUtil.registroIni);
		clinicaRequest.setFin(ConstanteUtil.registroFinClinicas);
		clinicaRequest.setTipoBus(ConstanteUtil.tipoTodos);
		clinicaRequest.setCodcli("''");
		clinicaRequest.setNomcli("''");

		try {
			ResponseOnc clinicaRes = clinService.obtenerListaClinica(clinicaRequest, headers);
			
			if(clinicaRes != null && Integer.parseInt(clinicaRes.getAudiResponse().getCodigoRespuesta()) >= 0) {
				listaClinicas = (ArrayList<ClinicaBean>) clinicaRes.getDataList();				
			} else {
				listaClinicas = null;
			}
		} catch (Exception e) {
			log.error(".obtenerNombresClinicas: " + e.getMessage());
			listaClinicas = null;
		}

		return listaClinicas;
	}
}
