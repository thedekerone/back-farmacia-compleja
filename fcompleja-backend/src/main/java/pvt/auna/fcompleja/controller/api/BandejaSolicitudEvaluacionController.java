package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jfree.util.Log;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ClinicaRequest;
import pvt.auna.fcompleja.model.api.DiagnosticoRequest;
import pvt.auna.fcompleja.model.api.ListaBandeja;
import pvt.auna.fcompleja.model.api.PacienteRequest;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.EmailAlertaCmacRequest;
import pvt.auna.fcompleja.model.api.request.ListaBandejaRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.request.registrarEvaluacionCmac;
import pvt.auna.fcompleja.model.api.request.evaluacion.ProgramacionCmacRequest;
import pvt.auna.fcompleja.model.api.response.evaluacion.ListaEvaluacionXFecha;
import pvt.auna.fcompleja.model.api.response.evaluacion.ProgramacionCmacDetResponse;
import pvt.auna.fcompleja.model.bean.ClinicaBean;
import pvt.auna.fcompleja.model.bean.DiagnosticoBean;
import pvt.auna.fcompleja.model.bean.EmailBean;
import pvt.auna.fcompleja.model.bean.PacienteBean;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.service.BandejaSolicitudEvaluacionService;
import pvt.auna.fcompleja.service.EmailService;
import pvt.auna.fcompleja.service.OncoClinicaService;
import pvt.auna.fcompleja.service.OncoDiagnosticoService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.AunaDefine;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;
import pvt.auna.fcompleja.util.ResponseCodesEnum;

@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/")
public class BandejaSolicitudEvaluacionController {

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(BandejaSolicitudEvaluacionController.class);

	@Autowired
	AseguramientoPropiedades asegProp;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	OncoPacienteService pcteService;

	@Autowired
	OncoClinicaService clinService;

	@Autowired
	OncoDiagnosticoService diagService;

	@Autowired
	BandejaSolicitudEvaluacionService bandejaSolicitudEvaluacionService;

	@Autowired
	EmailService emailService;

	@Autowired
	UsuarioService userService;

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/api/BandejaEvaluacion", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<ResponseOnc> BandejaEvaluacion(@RequestBody ListaBandejaRequest listaBandeja) {
		ResponseOnc response = new ResponseOnc();
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		AudiResponse audi = new AudiResponse(idTransaccion, fechaTransaccion);
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		ApiOutResponse outResponse = null;
		ArrayList<ListaBandeja> listaDetalle = null;

		LOGGER.info("API method: CONSULTAR EVALUACIONES");

		LOGGER.info("[SERVICIO:  CONSULTAR EVALUACIONES][INICIO]");
		LOGGER.debug("[SERVICIO:  CONSULTAR EVALUACIONES][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.debug(
				"[SERVICIO:  CONSULTAR EVALUACIONES][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  CONSULTAR EVALUACIONES][REQUEST][BODY][" + listaBandeja.toString() + "]");

		try {
			outResponse = bandejaSolicitudEvaluacionService.BandejaEvaluacion(listaBandeja);
			String codigoAfiliado = "";
			String codigoClinicas = "";
			String codigoDiagnostico = "";
			LOGGER.info(
					"[SERVICIO:  CONSULTAR EVALUACIONES][REQUEST][INFO][Proceso de la Transacción realizada con éxito]");
			listaDetalle = (ArrayList<ListaBandeja>) outResponse.getResponse();

			if (listaDetalle != null && listaDetalle.size() > 0) {
				Integer total = 0;
				Integer totalCli = 0;
				Integer totalDia = 0;
				for (ListaBandeja obj : listaDetalle) {
					if (codigoAfiliado.indexOf(obj.getCodigoPaciente()) == -1) {
						total++;
						codigoAfiliado += "|" + obj.getCodigoPaciente();
					}

					if (codigoClinicas.indexOf(obj.getCodigoClinica()) == -1) {
						totalCli++;
						codigoClinicas += "|" + obj.getCodigoClinica();
					}

					if (codigoDiagnostico.indexOf(obj.getCodigoDiagnostico()) == -1) {
						totalDia++;
						codigoDiagnostico += obj.getCodigoDiagnostico() + "|";
					}
				}

				ArrayList<PacienteBean> listaPaciente = obtenerNombresAfiliados(codigoAfiliado, total, headers);
				if (listaPaciente != null && !listaPaciente.isEmpty()) {
					for (ListaBandeja pre : listaDetalle) {
						for (PacienteBean pcte : listaPaciente) {
							if (pre.getCodigoPaciente().equalsIgnoreCase(pcte.getCodafir())) {
								pre.setNombrePaciente(pcte.getApelNomb());
								break;
							}
						}
					}
					LOGGER.info("[SERVICIO:  CONSULTAR EVALUACIONES][REQUEST][INFO][Paciente obtenidos exitosamente]");
				} else {
					audi.setCodigoRespuesta("1");
					audi.setMensajeRespuesta("No se logró obtener datos del afiliado.");
				}

				ArrayList<ClinicaBean> listaClinicas = obtenerNombresClinicas(codigoClinicas, headers);
				if (listaClinicas != null && !listaClinicas.isEmpty()) {
					for (ListaBandeja pre : listaDetalle) {
						for (ClinicaBean clinica : listaClinicas) {
							if (pre.getCodigoClinica().equalsIgnoreCase(clinica.getCodcli())) {
								pre.setDescClinica(clinica.getNomcli());
								break;
							}
						}
					}
					LOGGER.info("[SERVICIO:  CONSULTAR EVALUACIONES][RESPONSE][INFO][Clinicas obtenidos exitosamente]");
				} else {
					audi.setCodigoRespuesta("2");
					audi.setMensajeRespuesta("No se logró obtener datos de las Clínicas.");
				}

				ArrayList<DiagnosticoBean> listaDiagnostico = obtenerNombresDiagnosticos(codigoDiagnostico, total,
						headers);
				if (listaDiagnostico != null && !listaDiagnostico.isEmpty()) {
					for (ListaBandeja pre : listaDetalle) {
						for (DiagnosticoBean diagnostico : listaDiagnostico) {
							if (pre.getCodigoDiagnostico().equalsIgnoreCase(diagnostico.getCodigo())) {
								pre.setNombreDiagnostico(diagnostico.getDiagnostico());
								break;
							}
						}
					}
					LOGGER.info(
							"[SERVICIO:  CONSULTAR EVALUACIONES][RESPONSE][INFO][Diagnósticos obtenidos exitosamente]");
				} else {
					audi.setCodigoRespuesta("2");
					audi.setMensajeRespuesta("No se logró obtener datos de las Diagnósticos.");
				}

				ArrayList<UsuarioBean> listaRoles = obtenerRoles(headers);
				if (listaRoles != null && !listaRoles.isEmpty()) {
					for (ListaBandeja pre : listaDetalle) {
						for (UsuarioBean rol : listaRoles) {
							if (pre.getCodRolRespEvaluacion() == rol.getCodRol()) {
								pre.setRolResponsableEvaluacion(rol.getNombreRol());
								break;
							}
						}
					}
					LOGGER.info("[SERVICIO:  CONSULTAR EVALUACIONES][RESPONSE][INFO][Roles obtenidos exitosamente]");
				} else {
					audi.setCodigoRespuesta("2");
					audi.setMensajeRespuesta("No se logró obtener datos de los Roles.");
				}

				ArrayList<UsuarioBean> listaUsuarios = obtenerUsuarios(headers);
				if (listaUsuarios != null && !listaUsuarios.isEmpty()) {
					for (ListaBandeja pre : listaDetalle) {
						for (UsuarioBean user : listaUsuarios) {
							Integer usuario = user.getCodUsuario();
							Integer evaUser =  pre.getCodAutorPerte();
							if (usuario.equals(evaUser)) {
								pre.setAuditorPertenencia(user.getNombreApellido());
								break;
							}
						}
					}
					LOGGER.info("[SERVICIO:  CONSULTAR EVALUACIONES][RESPONSE][INFO][Usuarios obtenidos exitosamente]");
				} else {
					audi.setCodigoRespuesta("2");
					audi.setMensajeRespuesta("No se logró obtener datos de los Usuarios.");
				}
			} else {
				audi.setCodigoRespuesta(outResponse.getCodResultado() + "");
				audi.setMensajeRespuesta(outResponse.getMsgResultado());
			}
		} catch (Exception e) {
			audi.setCodigoRespuesta((outResponse != null) ? outResponse.getCodResultado() + "" : "99");
			audi.setMensajeRespuesta((outResponse != null) ? outResponse.getMsgResultado() : e.getMessage());
			response.setDataList(null);
			LOGGER.error(this.getClass().getName() + ".BandejaSolicitudes: " + e.getMessage());
			e.printStackTrace();
		}

		audi.setCodigoRespuesta(
				(audi.getCodigoRespuesta() != null) ? audi.getCodigoRespuesta() : outResponse.getCodResultado() + "");
		audi.setMensajeRespuesta(
				(audi.getMensajeRespuesta() != null) ? audi.getMensajeRespuesta() : outResponse.getMsgResultado());

		response.setAudiResponse(audi);
		response.setDataList(listaDetalle);

		if (response.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")
				|| response.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("1")
				|| response.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("2")) {
			LOGGER.info("[SERVICIO:  CONSULTAR EVALUACIONES][RESPONSE][INFO][Response generado exitosamente]");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} else {
			LOGGER.info("[SERVICIO:  CONSULTAR EVALUACIONES][RESPONSE][INFO][Response generado con errores]");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/consultarEvaluacionXFecha", produces = "application/json; charset=UTF-8")
	public WsResponse consultarEvaluacionXFecha(@RequestBody EmailAlertaCmacRequest request) throws Exception {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: EVALUACIONES CMAC");
		LOGGER.info("[SERVICIO:  EVALUACIONES CMAC][INICIO]");
		LOGGER.info("[SERVICIO:  EVALUACIONES CMAC][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  EVALUACIONES CMAC][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  EVALUACIONES CMAC][REQUEST][BODY][" + request.getFechaCmac() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = bandejaSolicitudEvaluacionService.consultarEvaluacionXFecha(request);
			String codigoAfiliado = "";
			String codigoDiagnostico = "";
			String mensajes = "";

			if (service.getCodResultado() == 0) {
				mensajes = service.getMsgResultado();

				ListaEvaluacionXFecha evaluacion = (ListaEvaluacionXFecha) service.getResponse();
				ArrayList<ProgramacionCmacDetResponse> bandeja = evaluacion.getListaBandeja();

				Integer total = 0;
				Integer totalDia = 0;

				for (ProgramacionCmacDetResponse obj : bandeja) {
					if (codigoAfiliado.indexOf(obj.getCodigoPaciente()) == -1) {
						total++;
						codigoAfiliado += "|" + obj.getCodigoPaciente();
					}

					if (codigoDiagnostico.indexOf(obj.getCodigoDiagnostico()) == -1) {
						totalDia++;
						codigoDiagnostico += obj.getCodigoDiagnostico() + "|";
					}
				}

				ArrayList<PacienteBean> listaPaciente = obtenerNombresAfiliados(codigoAfiliado, total, headers);
				if (listaPaciente != null && !listaPaciente.isEmpty()) {
					for (ProgramacionCmacDetResponse pre : bandeja) {
						for (PacienteBean pcte : listaPaciente) {
							if (pre.getCodigoPaciente().equalsIgnoreCase(pcte.getCodafir())) {
								pre.setPaciente(pcte.getApelNomb());
								break;
							}
						}
					}
					LOGGER.info("[SERVICIO:  EVALUACIONES CMAC][RESPONSE][INFO][Paciente obtenidos exitosamente]");
					evaluacion.setListaBandeja(bandeja);
				} else {
					mensajes += "\n*No se logró obtener datos del afiliado de Oncosys.";
					audiResponse.setMensajeRespuesta(mensajes);
					LOGGER.error("[SERVICIO:  EVALUACIONES CMAC][RESPONSE][ERROR][Paciente no obtenidos de Oncosys]");
					evaluacion.setListaBandeja(bandeja);
				}

				ArrayList<DiagnosticoBean> listaDiagnostico = obtenerNombresDiagnosticos(codigoDiagnostico, totalDia,
						headers);
				if (listaDiagnostico != null && !listaDiagnostico.isEmpty()) {
					for (ProgramacionCmacDetResponse pre : bandeja) {
						for (DiagnosticoBean diagnostico : listaDiagnostico) {
							if (pre.getCodigoDiagnostico().equalsIgnoreCase(diagnostico.getCodigo())) {
								pre.setDiagnostico(diagnostico.getDiagnostico());
								break;
							}
						}
					}
					LOGGER.info("[SERVICIO:  EVALUACIONES CMAC][RESPONSE][INFO][Diagnósticos obtenidos exitosamente]");
					evaluacion.setListaBandeja(bandeja);
				} else {
					mensajes += "\n*No se logró obtener datos de diagnósticos de Oncosys.";
					audiResponse.setMensajeRespuesta(mensajes);
					LOGGER.error(
							"[SERVICIO:  EVALUACIONES CMAC][RESPONSE][ERROR][Diagnósticos no obtenidos de Oncosys]");
					evaluacion.setListaBandeja(bandeja);
				}

				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(fechaTransaccion);
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				response.setAudiResponse(audiResponse);
				response.setData(evaluacion);

			} else {
				LOGGER.info("[SERVICIO:  EVALUACIONES CMAC][RESPONSE][ERROR]" + service.getCodResultado());
				LOGGER.info("[SERVICIO:  EVALUACIONES CMAC][RESPONSE][ERROR]" + service.getMsgResultado());
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(fechaTransaccion);
				audiResponse.setCodigoRespuesta(service.getCodResultado() + "");
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
			}

		} catch (Exception e) {
			LOGGER.error(" CONSULTAR EVALUACIONES PROGRAMADAS POR FECHA CMAC : ", e);
			LOGGER.error("[SERVICIO:  CONSULTAR EVALUACIONES PROGRAMADAS POR FECHA CMAC][FIN]");
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(fechaTransaccion);
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
		}

		response.setAudiResponse(audiResponse);

		return response;

	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/registrarEvaluacionCmac", produces = "application/json; charset=UTF-8")
	public WsResponse registrarEvaluacionCmac(@RequestBody registrarEvaluacionCmac request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {
		LOGGER.info("API method: REGISTRAR EVALUACION CMAC");

		LOGGER.info("[SERVICIO:  REGISTRAR EVALUACION CMAC][INICIO]");
		LOGGER.info("[SERVICIO:  REGISTRAR EVALUACION CMAC][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info(
				"[SERVICIO:  REGISTRAR EVALUACION CMAC][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  REGISTRAR EVALUACION CMAC][REQUEST][BODY][" + request.getListaEvaluacion() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			request.setFechaEstado(fechaTransaccion);
			ApiOutResponse service = bandejaSolicitudEvaluacionService.registrarEvaluacionCmac(request);

			if (service.getCodResultado() == 0) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());

			} else {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}

		} catch (Exception e) {
			LOGGER.error(" REGISTRAR EVALUACION CMAC : ", e);
			LOGGER.info("[SERVICIO:  REGISTRAR EVALUACION CMAC][FIN]");
		}
		return response;
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/eliminarRegEvaluacionCmac", produces = "application/json; charset=UTF-8")
	public WsResponse eliminarRegEvaluacionCmac(@RequestBody SolicitudEvaluacionRequest request) throws Exception {
		
		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		
		LOGGER.info("API method: ELIMINAR EVALUACION CMAC");
		LOGGER.info("[SERVICIO:  ELIMINAR EVALUACION X CODIGO EVALUACION][INICIO]");
		LOGGER.info(
				"[SERVICIO:  ELIMINAR EVALUACION X CODIGO EVALUACION][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  ELIMINAR EVALUACION X CODIGO EVALUACION][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion
				+ "]");
		LOGGER.info("[SERVICIO:  ELIMINAR EVALUACION X CODIGO EVALUACION][REQUEST][BODY][" + request.getCodSolicitudEvaluacion() + "]");
		
		try {
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(fechaTransaccion);
			WsResponse service = bandejaSolicitudEvaluacionService.eliminarRegEvaluacionCmac(request);
			if (service.getAudiResponse() != null) {
				audiResponse.setCodigoRespuesta(service.getAudiResponse().getCodigoRespuesta());
				audiResponse.setMensajeRespuesta(service.getAudiResponse().getMensajeRespuesta());
				
				
			} else {
				LOGGER.error("[SERVICIO:  ELIMINAR REGISTRO EVALUACION CMAC][REQUEST][BODY][Error en la eliminacion del registro]");
				audiResponse.setCodigoRespuesta("-2");
				audiResponse.setMensajeRespuesta("Error en la eliminacion del registro");
			}
		} catch (Exception e) {
			audiResponse.setCodigoRespuesta("-3");
			audiResponse.setMensajeRespuesta("Error de la Aplicacion");
		}
		response.setAudiResponse(audiResponse);
		response.setData(null);
		
		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/evaluacionXCodigo", produces = "application/json; charset=UTF-8")
	public WsResponse EvaluacionXCodigo(@RequestBody ListaBandejaRequest request) throws Exception {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: REGISTRAR EVALUACION CMAC");
		LOGGER.info("[SERVICIO:  CONSULTAR EVALUACION X CODIGO][INICIO]");
		LOGGER.info(
				"[SERVICIO:  CONSULTAR EVALUACION X CODIGO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  CONSULTAR EVALUACION X CODIGO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion
				+ "]");
		LOGGER.info("[SERVICIO:  CONSULTAR EVALUACION X CODIGO][REQUEST][BODY][" + request.getCodigoEvaluacion() + "]");
		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = bandejaSolicitudEvaluacionService.EvaluacionXCodigo(request);

			if (service != null) {
				if (service.getCodResultado() == 0) {
					ListaBandeja evaluacion = (ListaBandeja) service.getResponse();
					if(evaluacion != null) {
						PacienteBean pcte = obtenerPacienteOncosys(evaluacion, headers);
						DiagnosticoBean diag = obtenerDiagnosticoOncosys(evaluacion, headers);
						evaluacion.setNombrePaciente((pcte != null) ? pcte.getApelNomb() : "NO HALLADO(ONCOSYS)");
						evaluacion.setNombreDiagnostico((diag != null) ? diag.getDiagnostico() : "NO HALLADO(ONCOSYS)");

						audiResponse.setIdTransaccion(idTransaccion);
						audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
						audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
						audiResponse.setMensajeRespuesta(service.getMsgResultado());
						response.setAudiResponse(audiResponse);
						response.setData(evaluacion);
					} else {
						audiResponse.setIdTransaccion(idTransaccion);
						audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
						audiResponse.setCodigoRespuesta("1");
						audiResponse.setMensajeRespuesta("NO SE ENCUENTRA DATOS PARA EL CODIGO INGRESADO");
						response.setAudiResponse(audiResponse);
						response.setData(evaluacion);
					}
					

				} else {
					LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
					LOGGER.info(service.getMsgResultado());
					audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
					audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
					response.setData(null);
				}
			} else {
				LOGGER.error("[SERVICIO:  REGISTRAR MAC][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("99");
				audiResponse.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setData(null);
			}

		} catch (Exception e) {
			LOGGER.error(" CONSULTAR EVALUACION X CODIGO : ", e.getMessage());
			LOGGER.info("[SERVICIO:  CONSULTAR EVALUACION X CODIGO][FIN]");
			audiResponse.setCodigoRespuesta("-2");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
		}

		return response;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/consultaEvaluacionCmacXCodigo", produces = "application/json; charset=UTF-8")
	public WsResponse consultarEvaluacionXCodigo(@RequestBody EmailAlertaCmacRequest request) throws Exception {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		
		LOGGER.info("API method: ADD EVALUACIONES A CMAC");

		LOGGER.info("[SERVICIO:  EVALUACIONES A CMAC][INICIO]");
		LOGGER.info("[SERVICIO:  EVALUACIONES A CMAC][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  EVALUACIONES A CMAC][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO:  EVALUACIONES A CMAC][REQUEST][BODY][" + request.getCodLagoSolEva() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = bandejaSolicitudEvaluacionService.consultarEvaluacionXCodigo(request);

			ListaBandeja bandeja = null;
			
			audiResponse.setIdTransaccion(idTransaccion);
			audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());

			if (service.getCodResultado() == 0) {

				Integer total = 1;
				List<ListaBandeja> evaluacion = (List<ListaBandeja>) service.getResponse();
				
				if (evaluacion != null && evaluacion.size() != 0) {
					bandeja = evaluacion.get(0);
	
					if (bandeja.getCodigoPaciente() == null) {
						throw new Exception("NO SE ENCUENTRA EL CODIGO DEL AFILIADO");
					}
	
					String codigoAfiliado = "|" + bandeja.getCodigoPaciente();
					String codigoDiagnostico = "|" + bandeja.getCodigoDiagnostico();
	
					ArrayList<PacienteBean> listaPaciente = obtenerNombresAfiliados(codigoAfiliado,
							total, headers);
					if (listaPaciente != null && !listaPaciente.isEmpty()) {
						int cont = 0;
						for (PacienteBean pcte : listaPaciente) {
							if (bandeja.getCodigoPaciente().equalsIgnoreCase(pcte.getCodafir())) {
								bandeja.setPaciente(pcte.getApelNomb());
								cont = 1;
								break;
							}
							bandeja.setNombrePaciente(pcte.getApelNomb());
						}
						if (cont == 0) {
							bandeja.setPaciente(AunaDefine.NO_DISPONIBLE);
						}
					} else {
						bandeja.setPaciente(AunaDefine.NO_DISPONIBLE);
					}
	
					ArrayList<DiagnosticoBean> listaDiagnostico = obtenerNombresDiagnosticos(
							codigoDiagnostico, total, headers);
	
					if (listaDiagnostico != null && !listaDiagnostico.isEmpty()) {
						int cont = 0;
						for (DiagnosticoBean diagnostico : listaDiagnostico) {
							if (bandeja.getCodigoDiagnostico()
									.equalsIgnoreCase(diagnostico.getCodigo())) {
								bandeja.setDiagnostico(diagnostico.getDiagnostico());
								cont = 1;
								break;
							}
						}
						if (cont == 0) {
							bandeja.setDiagnostico(AunaDefine.NO_DISPONIBLE);
						}
					} else {
						bandeja.setPaciente(AunaDefine.NO_DISPONIBLE);
					}
	
					
					audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
					audiResponse.setMensajeRespuesta(service.getMsgResultado());
					response.setAudiResponse(audiResponse);
					response.setData(service.getResponse());

				} else {
				
					audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
					audiResponse.setMensajeRespuesta("NO SE ENCONTRARON DATOS CON EL CODIGO INGRESADO");
					response.setAudiResponse(audiResponse);
					response.setData(null);
				}
			} else if(service.getCodResultado() > 0) {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
			} else if (service.getCodResultado() < 0) {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("Error de aplicacion");
			}

		} catch (Exception e) {
			LOGGER.error(" CONSULTAR EVALUACIONES POR EL CODIGO EN EVALUACION CMAC : ", e);
			LOGGER.info("[SERVICIO:  CONSULTAR EVALUACIONES POR EL CODIGO EN EVALUACION CMAC][FIN]");
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
		}

		response.setAudiResponse(audiResponse);
		return response;
	}

	private PacienteBean obtenerPacienteOncosys(ListaBandeja evaluacion, HttpHeaders headers) {
		PacienteBean paciente = null;
		try {
			PacienteRequest pcteRq = new PacienteRequest();
			pcteRq.setPvTibus(3);
			pcteRq.setCodigoAfiliado(evaluacion.getCodigoPaciente());
			pcteRq.setTipoDocumento("''");
			pcteRq.setNumeroDocumento("''");
			pcteRq.setNombres("''");
			pcteRq.setApellidoPaterno("''");
			pcteRq.setApellidoMaterno("''");

			paciente = (PacienteBean) pcteService.obtenerPaciente(pcteRq, headers).getData();

		} catch (Exception e) {
			Log.error("obtenerPacienteOncosys: No se logró obtener el Paciente de Oncosys. " + e.getMessage());
			paciente = null;
		}

		return paciente;
	}

	private DiagnosticoBean obtenerDiagnosticoOncosys(ListaBandeja evaluacion, HttpHeaders headers) {
		DiagnosticoBean diagnostico = null;

		try {
			DiagnosticoRequest diaRq = new DiagnosticoRequest();
			diaRq.setRegistroInicio(ConstanteUtil.registroIni);
			diaRq.setRegistroFin(ConstanteUtil.registroIni);
			diaRq.setTipoBusqueda(ConstanteUtil.tipoXCod);
			diaRq.setCodigoDiagnostico(evaluacion.getCodigoDiagnostico());
			diaRq.setNombreDiagnostico("");

			diagnostico = (DiagnosticoBean) diagService.obtenerDiagnostico(headers, diaRq).getData();
		} catch (Exception e) {
			Log.error("obtenerDiagnosticoOncosys: " + e.getMessage());
			diagnostico = null;
		}

		return diagnostico;
	}

	private ArrayList<PacienteBean> obtenerNombresAfiliados(String codigoAfiliado, Integer total, HttpHeaders headers) {
		ArrayList<PacienteBean> listaPaciente = null;

		AfiliadosRequest pacienteRequest = new AfiliadosRequest();
		pacienteRequest.setIni(ConstanteUtil.registroIni);
		pacienteRequest.setFin(total + 1);
		pacienteRequest.setCodafir(codigoAfiliado);

		try {
			ResponseOnc pacienteRes = pcteService.obtenerListaPacientexCodigos(pacienteRequest, headers);

			if (pacienteRes != null && Integer.parseInt(pacienteRes.getAudiResponse().getCodigoRespuesta()) >= 0) {
				listaPaciente = (ArrayList<PacienteBean>) pacienteRes.getDataList();
			} else {
				listaPaciente = null;
			}

		} catch (Exception e) {
			LOGGER.error(this.getClass().getName() + ".obtenerNombresAfiliados: " + e.getMessage());
			listaPaciente = null;
		}

		return listaPaciente;
	}

	private ArrayList<ClinicaBean> obtenerNombresClinicas(String codigoClinicas, HttpHeaders headers) {
		ArrayList<ClinicaBean> listaClinicas = null;

		ClinicaRequest clinicaRequest = new ClinicaRequest();
		clinicaRequest.setCodcli(codigoClinicas);

		try {
			ResponseOnc clinicaRes = clinService.obtenerListaClinicaXCodigos(clinicaRequest, headers);

			if (clinicaRes != null && Integer.parseInt(clinicaRes.getAudiResponse().getCodigoRespuesta()) >= 0) {
				listaClinicas = (ArrayList<ClinicaBean>) clinicaRes.getDataList();
			} else {
				listaClinicas = null;
			}
		} catch (Exception e) {
			LOGGER.error(".obtenerNombresClinicas: " + e.getMessage());
			listaClinicas = null;
		}

		return listaClinicas;
	}

	private ArrayList<DiagnosticoBean> obtenerNombresDiagnosticos(String codigoDiagnostico, Integer total,
			HttpHeaders headers) {
		ArrayList<DiagnosticoBean> listaDiagnosticos = null;

		try {
			ResponseOnc diagResponse = diagService.getListarDiagnosticoXcodigos(codigoDiagnostico, total, headers);

			if (diagResponse != null && Integer.parseInt(diagResponse.getAudiResponse().getCodigoRespuesta()) >= 0) {
				listaDiagnosticos = (ArrayList<DiagnosticoBean>) diagResponse.getDataList();
			} else {
				listaDiagnosticos = null;
			}
		} catch (Exception e) {
			LOGGER.error(".obtenerNombresDiagnosticos: " + e.getMessage());
			listaDiagnosticos = null;
		}

		return listaDiagnosticos;
	}

	private ArrayList<UsuarioBean> obtenerRoles(HttpHeaders headers) {
		ArrayList<UsuarioBean> listaRoles = null;

		UsuarioBean request = new UsuarioBean();
		request.setCodAplicacion(asegProp.getAplicacion());

		try {
			ApiOutResponse rolResponse = userService.getListarRoles(request, headers);

			if (rolResponse != null && rolResponse.getCodResultado() >= 0) {
				listaRoles = (ArrayList<UsuarioBean>) rolResponse.getResponse();
			} else {
				listaRoles = null;
			}
		} catch (Exception e) {
			LOGGER.error(".obtenerRoles: " + e.getMessage());
			listaRoles = null;
		}

		return listaRoles;
	}

	private ArrayList<UsuarioBean> obtenerUsuarios(HttpHeaders headers) {
		ArrayList<UsuarioBean> listaUsuarios = null;

		UsuarioBean request = new UsuarioBean();
		request.setCodAplicacion(asegProp.getAplicacion());

		try {
			ApiOutResponse userResp = userService.getListarUsuarios(request, headers);

			if (userResp != null && userResp.getCodResultado() >= 0) {
				listaUsuarios = (ArrayList<UsuarioBean>) userResp.getResponse();
			} else {
				listaUsuarios = null;
			}
		} catch (Exception e) {
			LOGGER.error(".obtenerUsuarios: " + e.getMessage());
			listaUsuarios = null;
		}

		return listaUsuarios;
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/actualizarEstSolicitudEvaluacion", produces = "application/json; charset=UTF-8")
	public WsResponse actualizarEstSolicitudEvaluacion(@RequestBody EmailBean request,
			@RequestHeader(value = "idTransaccion") String idTransaccion,
			@RequestHeader(value = "fechaTransaccion") String fechaTransaccion) throws Exception {

		LOGGER.info("API method: ACTUALIZAR ESTADO SOLICITUD EVALUACION");
		LOGGER.info("[SERVICIO: ACTUALIZAR ESTADO SOLICITUD EVALUACION][INICIO]");
		LOGGER.info("[SERVICIO: ACTUALIZAR ESTADO SOLICITUD EVALUACION][REQUEST][HEADER][idTransaccion]["
				+ idTransaccion + "]");
		LOGGER.info("[SERVICIO: ACTUALIZAR ESTADO SOLICITUD EVALUACION][REQUEST][HEADER][fechaTransaccion]["
				+ fechaTransaccion + "]");
		LOGGER.info("[SERVICIO: ACTUALIZAR ESTADO SOLICITUD EVALUACION][REQUEST][BODY][" + request.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();

		try {
			ApiOutResponse service = emailService.actualizarEstSolicitudEvaluacion(request);

			if (service.getCodResultado() != -1) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());

			} else {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}

		} catch (Exception e) {
			// TODO: handle exception
			// TODO: handle exception
			LOGGER.error("ACTUALIZAR ESTADO SOLICITUD EVALUACION : ", e);
			LOGGER.info("[SERVICIO: ACTUALIZAR ESTADO SOLICITUD EVALUACION][FIN]");
		}
		return response;

	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "api/actualizarActaEscanProgramacionCmac", produces = "application/json; charset=UTF-8")
	public WsResponse actualizarActaEscanProgramacionCmac(@RequestBody ProgramacionCmacRequest request) throws Exception {
		
		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse();
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		
		LOGGER.info("API method: ACTUALIZAR PROG CMAC");
		LOGGER.info("[SERVICIO: ACTUALIZAR PROG CMAC][INICIO]");
		LOGGER.info("[SERVICIO: ACTUALIZAR PROG CMAC][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO: ACTUALIZAR PROG CMAC][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.info("[SERVICIO: ACTUALIZAR PROG CMAC][REQUEST][BODY][" + request.toString() + "]");
		
		try {
			ApiOutResponse service = bandejaSolicitudEvaluacionService.actualizarActaEscanProgramacionCmac(request);

			if (service.getCodResultado() != -1) {
				audiResponse.setIdTransaccion(idTransaccion);
				audiResponse.setFechaTransaccion(ResponseCodesEnum.FECHA_TRANSACCION.getFec());
				audiResponse.setCodigoRespuesta(service.getCodResultado().toString());
				audiResponse.setMensajeRespuesta(service.getMsgResultado());
				response.setAudiResponse(audiResponse);
				response.setData(service.getResponse());

			} else {
				LOGGER.info("SE OBTUVO UN CODIGO DE RESPUESTA DE ERROR : " + service.getCodResultado());
				LOGGER.info(service.getMsgResultado());
				audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
				audiResponse.setMensajeRespuesta("No se enviaron datos de entrada correctos");
			}

		} catch (Exception e) {
			LOGGER.error("ACTUALIZAR PROG CMAC : ", e);
			LOGGER.info("[SERVICIO: ACTUALIZAR PROG CMAC][FIN]");
			audiResponse.setCodigoRespuesta(ResponseCodesEnum.FAIL.getCode());
			audiResponse.setMensajeRespuesta("Ecurrio una excepcion");
		}
		return response;
	}
	
}
