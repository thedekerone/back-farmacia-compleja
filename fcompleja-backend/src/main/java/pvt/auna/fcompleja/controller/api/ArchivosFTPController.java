package pvt.auna.fcompleja.controller.api;

import java.util.Date;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import pvt.auna.fcompleja.config.ConfigFTPProp;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ArchivoFTPBean;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.service.GenericoService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@RestController
@RequestMapping("/")
public class ArchivosFTPController {

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ArchivosFTPController.class);

	@Autowired
	GenericoService service;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ConfigFTPProp ftpProp;

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/descargarArchivo", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> descargarArchivo(@RequestBody ArchivoFTPBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		obj.setUsrApp(ftpProp.getUserApp());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: DESCARGAR ARCHIVO");

		LOGGER.info("[SERVICIO:  DESCARGAR ARCHIVO][INICIO]");
		LOGGER.info("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		LOGGER.debug("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][BODY][" + obj.toString() + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;
		
		
		try {
			resultado = service.descargarArchivo(obj, headers);

			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					LOGGER.info("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][BODY][Registro realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((ArchivoFTPBean) resultado.getResponse());
				} else {
					LOGGER.error("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][FIN][Error en el servicio de descarga]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio de descarga de archivos.");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/subirArchivo", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public ResponseEntity<WsResponse> subirArchivo(@RequestParam("file") MultipartFile file,
			@RequestParam("nomArchivo") String nomArchivo, @RequestParam("ruta") String ruta) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		LOGGER.info("API method: SUBIR ARCHIVO");

		LOGGER.info("[SERVICIO:  SUBIR ARCHIVO][INICIO]");
		LOGGER.info("[SERVICIO:  SUBIR ARCHIVO][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		LOGGER.info("[SERVICIO:  SUBIR ARCHIVO][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;
		try {
			resultado = service.enviarArchivoFTP(ftpProp.getUserApp(), nomArchivo, file, ftpProp.getRutaFTP() + ruta, headers);

			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					LOGGER.info("[SERVICIO:  SUBIR ARCHIVO][REQUEST][FIN][Registro realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setAudiResponse(audiResponse);
					response.setData((ArchivoFTPBean) resultado.getResponse());
					return new ResponseEntity<WsResponse>(response, HttpStatus.OK);
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					LOGGER.error("[SERVICIO:  SUBIR ARCHIVO][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO:  SUBIR ARCHIVO][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setAudiResponse(audiResponse);
					response.setData(null);
					return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
				}
			} else {				
				LOGGER.error("[SERVICIO:  SUBIR ARCHIVO][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error al subir el archivo FTP.");
				response.setAudiResponse(audiResponse);
				response.setData(null);
				return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			LOGGER.error("[SERVICIO:  SUBIR ARCHIVO][REQUEST][FIN][Ocurrio un excepcion]");
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
