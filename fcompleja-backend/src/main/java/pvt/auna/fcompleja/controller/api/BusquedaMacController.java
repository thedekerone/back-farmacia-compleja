package pvt.auna.fcompleja.controller.api;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.bean.MACBean;
import pvt.auna.fcompleja.service.MacService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping(value = "/")
public class BusquedaMacController {

	@Autowired
	private MacService medicamentoService;

	private static final Logger log = LoggerFactory.getLogger(BusquedaMacController.class);

	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/pub/lstMacMedicamento", produces = "application/json; charset=UTF-8")
	public ResponseEntity<ResponseOnc> lstMacMedicamento(@RequestBody MACBean filtro) {
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
		
		HttpHeaders header = new HttpHeaders();
		
		header.add("idTransaccion", idTransaccion);
		header.add("fechaTransaccion", fechaTransaccion);

		log.info("API method: LISTAR BUSQUEDA MAC");

		log.debug("[SERVICIO:  LISTAR BUSQUEDA MAC][INICIO]");
		log.debug("[SERVICIO:  LISTAR BUSQUEDA MAC][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.debug("[SERVICIO:  LISTAR BUSQUEDA MAC][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.debug("[SERVICIO:  LISTAR BUSQUEDA MAC][REQUEST][BODY][" + filtro.toString() + "]");
		
		ResponseOnc response = new ResponseOnc();
		AudiResponse audi = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;
		
		try {
			resultado = medicamentoService.listFiltroMAC(filtro);
			
			if (resultado != null) {
				log.info("[SERVICIO:  LISTAR BUSQUEDA MAC][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audi.setCodigoRespuesta("0");
					audi.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<MACBean>) resultado.getResponse());
				} else {
					audi.setCodigoRespuesta(resultado.getCodResultado() + "");
					audi.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				log.error("[SERVICIO:  LISTAR BUSQUEDA MAC][REQUEST][BODY][Se encontraron errores]");
				audi.setCodigoRespuesta("99");
				audi.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setDataList(null);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			audi.setCodigoRespuesta("99");
			audi.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audi);
			response.setDataList(null);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.setAudiResponse(audi);		
		return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/api/lstMacMedicamento", produces = "application/json; charset=UTF-8")
	public ResponseEntity<ResponseOnc> apiLstMacMedicamento(@RequestBody MACBean filtro) {
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
		
		HttpHeaders header = new HttpHeaders();
		
		header.add("idTransaccion", idTransaccion);
		header.add("fechaTransaccion", fechaTransaccion);

		log.info("API method: LISTAR BUSQUEDA MAC");

		log.debug("[SERVICIO:  LISTAR BUSQUEDA MAC][INICIO]");
		log.debug("[SERVICIO:  LISTAR BUSQUEDA MAC][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.debug("[SERVICIO:  LISTAR BUSQUEDA MAC][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.debug("[SERVICIO:  LISTAR BUSQUEDA MAC][REQUEST][BODY][" + filtro.toString() + "]");
		
		ResponseOnc response = new ResponseOnc();
		AudiResponse audi = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;
		
		try {
			resultado = medicamentoService.listFiltroMAC(filtro);
			
			if (resultado != null) {
				log.info("[SERVICIO:  LISTAR BUSQUEDA MAC][REQUEST][BODY][Registro realizado correctamente]");
				if (resultado.getCodResultado() == 0) {
					audi.setCodigoRespuesta("0");
					audi.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList((ArrayList<MACBean>) resultado.getResponse());
				} else {
					audi.setCodigoRespuesta(resultado.getCodResultado() + "");
					audi.setMensajeRespuesta(resultado.getMsgResultado());
					response.setDataList(null);
				}
			} else {
				log.error("[SERVICIO:  LISTAR BUSQUEDA MAC][REQUEST][BODY][Se encontraron errores]");
				audi.setCodigoRespuesta("99");
				audi.setMensajeRespuesta("Error en el mantenimiento de la MAC");
				response.setDataList(null);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			audi.setCodigoRespuesta("99");
			audi.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audi);
			response.setDataList(null);
			return new ResponseEntity<ResponseOnc>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.setAudiResponse(audi);		
		return new ResponseEntity<ResponseOnc>(response, HttpStatus.OK);
	}

}
