package pvt.auna.fcompleja.controller.api;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pvt.auna.fcompleja.model.api.RolResponsableResponse;
import pvt.auna.fcompleja.service.RolResponsableService;

@RestController
@RequestMapping("/")
public class RolResponsableController {

	@Autowired
	RolResponsableService rolResponsableService;

	private final Logger LOGGER = Logger.getLogger(getClass());

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/api/RolResponsable", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody RolResponsableResponse RolResponsable() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		RolResponsableResponse rolResponsableResponse = new RolResponsableResponse();

		try {

			rolResponsableResponse = rolResponsableService.RolResponsable();

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("Error", e);
		}
		return rolResponsableResponse;
	}
}
