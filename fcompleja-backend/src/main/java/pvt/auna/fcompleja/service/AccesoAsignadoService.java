package pvt.auna.fcompleja.service;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.request.seguridad.UsuarioRequest;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.model.api.response.UsuarioOauthResponse;

public interface AccesoAsignadoService {

	public OncoWsResponse consultarMenu(UsuarioRequest request, HttpHeaders headers) throws Exception;

	public OncoWsResponse consultarUsuarioRol(UsuarioRequest request) throws Exception;

	public OncoWsResponse consultarUsuario(UsuarioRequest request) throws Exception;

	public OncoWsResponse consultarUsuariosPorRol(UsuarioRequest request, HttpHeaders header) throws Exception;
	
	public OncoWsResponse obtenerPorUsuarioId(UsuarioRequest request, HttpHeaders headers);

	UsuarioOauthResponse obtenerUsuarioPorToken(String token);



}
