package pvt.auna.fcompleja.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ClinicaRequest;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudesPreeliminarFiltroRequest;
import pvt.auna.fcompleja.service.BandejaSolicitudPreliminarService;
import pvt.auna.fcompleja.dao.BandejaSolicitudPreliminarDao;

@Service
public class BandejaSolicitudPreliminarServiceImpl implements BandejaSolicitudPreliminarService {

	AfiliadosRequest pacienteRequest;
	ClinicaRequest clinicaRequest;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private BandejaSolicitudPreliminarDao banSolPreDao;

	@Override
	public ApiOutResponse listaBandejaSolicitudes(SolicitudesPreeliminarFiltroRequest objA_preeliminar)
			throws Exception {
		return banSolPreDao.listarSolicitudes(objA_preeliminar);
	}

}
