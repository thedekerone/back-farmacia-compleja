package pvt.auna.fcompleja.service.impl;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.config.OncoPropiedades;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.ClinicaRequest;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.bean.ClinicaBean;
import pvt.auna.fcompleja.service.OncoClinicaService;
import pvt.auna.fcompleja.util.GenericUtil;

@Service
public class OncoClinicaServiceImpl implements OncoClinicaService {

	private static final Logger log = LoggerFactory.getLogger(OncoClinicaServiceImpl.class);

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private OncoPropiedades prop;

	@Override
	public WsResponse obtenerClinica(ClinicaRequest clinicaRq, HttpHeaders headers) throws Exception {
		ResponseOnc clinicaResponse = new ResponseOnc();
		ClinicaBean clinica = null;
		WsResponse response = new WsResponse();

		String UrlVchar = prop.getOncoGenerico() + "/listar/clinicas";

		try {
			HttpEntity<ClinicaRequest> request = new HttpEntity<ClinicaRequest>(clinicaRq, headers);
			clinicaResponse = restTemplate.postForObject(UrlVchar, request, ResponseOnc.class);

			if (clinicaResponse != null && clinicaResponse.getAudiResponse() != null) {
				AudiResponse audi = clinicaResponse.getAudiResponse();

				if (audi.getCodigoRespuesta().equalsIgnoreCase("0")) {
					if (clinicaResponse.getDataList() != null && clinicaResponse.getDataList().size() == 1) {
						HashMap<?, ?> cliResponse = (HashMap<?, ?>) clinicaResponse.getDataList().get(0);
						clinica = new ClinicaBean();

						clinica.setCodcli((String) cliResponse.get("codcli"));
						clinica.setNomcli((String) cliResponse.get("nomcli"));
						clinica.setRn((Integer) cliResponse.get("rn"));
						
						response.setAudiResponse(clinicaResponse.getAudiResponse());
						response.setData(clinica);
					} else {
						audi = GenericUtil.obtenerResponse(audi, "2", "Se encontraron más de un resultado.");
						log.error(response.getAudiResponse().getMensajeRespuesta());
						response.setAudiResponse(audi);
					}
				} else {
					log.error(".obtenerClinica: " + audi.getMensajeRespuesta());
				}
			} else {
				response = GenericUtil.responseErrorData("Error en Servidor Oncosys clínicas");
				log.error(".obtenerClinica: " + response.getAudiResponse().getMensajeRespuesta());
			}

		} catch (Exception e) {
			response = GenericUtil.responseErrorData(e.getMessage());
			log.error(".obtenerClinica: " + e.getMessage());
		}

		return response;
	}

	@Override
	public ResponseOnc obtenerListaClinica(ClinicaRequest request, HttpHeaders headers) throws Exception {

		ResponseOnc response = new ResponseOnc();
		ArrayList<ClinicaBean> listaClinicas = null;

		String UrlVchar = prop.getOncoGenerico() + "/listar/clinicas";

		try {
			HttpEntity<ClinicaRequest> rqClinica = new HttpEntity<ClinicaRequest>(request, headers);
			response = restTemplate.postForObject(UrlVchar, rqClinica, ResponseOnc.class);

			if (response != null && response.getAudiResponse() != null) {
				AudiResponse audi = response.getAudiResponse();

				if (audi.getCodigoRespuesta().equals("0")) {
					listaClinicas = new ArrayList<ClinicaBean>();
					for (int i = 0; i < response.getDataList().size(); i++) {
						HashMap<?, ?> clinResponse = (HashMap<?, ?>) response.getDataList().get(i);

						ClinicaBean objClinica = new ClinicaBean();
						objClinica.setCodcli((String) clinResponse.get("codcli"));
						objClinica.setNomcli((String) clinResponse.get("nomcli"));
						objClinica.setRn((Integer) clinResponse.get("rn"));

						listaClinicas.add(objClinica);
					}

					response.setDataList((ArrayList<ClinicaBean>) listaClinicas);

				} else {
					log.error(".obtenerListaClinica(): " + audi.getMensajeRespuesta());
				}
			} else {
				response = GenericUtil.responseErrorList("Error en el Servidor Oncosys Clinicas.");
			}
		} catch (Exception e) {
			log.error(".obtenerListaClinica(): " + e.getMessage());
			response = GenericUtil.responseErrorList(e.getMessage());
		}

		return response;
	}

	@Override
	public ResponseOnc obtenerListaClinicaXCodigos(ClinicaRequest clinicaRequest, HttpHeaders headers) {
		ResponseOnc response = new ResponseOnc();
		ArrayList<ClinicaBean> listaClinicas = null;

		String UrlVchar = prop.getOncoGenerico() + "/listar/clinicas-codigo";

		try {
			HttpEntity<ClinicaRequest> rqClinica = new HttpEntity<ClinicaRequest>(clinicaRequest, headers);
			response = restTemplate.postForObject(UrlVchar, rqClinica, ResponseOnc.class);

			if (response != null && response.getAudiResponse() != null) {
				AudiResponse audi = response.getAudiResponse();

				if (audi.getCodigoRespuesta().equals("0")) {
					listaClinicas = new ArrayList<ClinicaBean>();
					for (int i = 0; i < response.getDataList().size(); i++) {
						HashMap<?, ?> clinResponse = (HashMap<?, ?>) response.getDataList().get(i);

						ClinicaBean objClinica = new ClinicaBean();
						objClinica.setCodcli((String) clinResponse.get("codcli"));
						objClinica.setNomcli((String) clinResponse.get("nomcli"));
						objClinica.setRn((Integer) clinResponse.get("rn"));

						listaClinicas.add(objClinica);
					}

					response.setDataList((ArrayList<ClinicaBean>) listaClinicas);

				} else {
					log.error(".obtenerListaClinicaXcodigos(): " + audi.getMensajeRespuesta());
				}
			} else {
				response = GenericUtil.responseErrorList("Error en el Servidor Oncosys Clinicas.");
			}
		} catch (Exception e) {
			log.error(".obtenerListaClinicaXcodigos(): " + e.getMessage());
			response = GenericUtil.responseErrorList(e.getMessage());
		}

		return response;
	}

}
