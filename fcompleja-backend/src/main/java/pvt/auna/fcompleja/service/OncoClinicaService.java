package pvt.auna.fcompleja.service;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.ClinicaRequest;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.WsResponse;

public interface OncoClinicaService {

	WsResponse obtenerClinica(ClinicaRequest clinica, HttpHeaders headers) throws Exception;

	ResponseOnc obtenerListaClinica(ClinicaRequest request, HttpHeaders headers) throws Exception;

	ResponseOnc obtenerListaClinicaXCodigos(ClinicaRequest clinicaRequest, HttpHeaders headers);

}
