package pvt.auna.fcompleja.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.FiltroParametroRequest;
import pvt.auna.fcompleja.model.api.FiltroParametroResponse;
import pvt.auna.fcompleja.service.FiltroParametroService;
import pvt.auna.fcompleja.dao.FiltroParametroDao;

@Service
public class FiltroParametroServiceImpl implements FiltroParametroService {

	@Autowired
	FiltroParametroDao FiltroParametroDao;
	
	@Override
	public FiltroParametroResponse FiltroParametro(FiltroParametroRequest ListaParametro) throws Exception {
		// TODO Auto-generated method stub
		
		return FiltroParametroDao.FiltroParametro(ListaParametro);
	}

	@Override
	public FiltroParametroResponse ControlFilaParametro(FiltroParametroRequest request) throws Exception {
		
		return FiltroParametroDao.ControlFilaParametro(request);
	}

	@Override
	public ApiOutResponse ConsultaParametroValue(FiltroParametroRequest request) throws Exception {
		// TODO Auto-generated method stub
		return FiltroParametroDao.ConsultaParametroValue(request);
	}

}
