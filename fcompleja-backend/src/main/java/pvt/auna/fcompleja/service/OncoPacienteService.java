package pvt.auna.fcompleja.service;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.PacienteRequest;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.ListaPacienteRequest;

public interface OncoPacienteService {
	WsResponse obtenerPaciente(PacienteRequest paciente, HttpHeaders headers) throws Exception;

	ResponseOnc obtenerListaPacientexCodigos(AfiliadosRequest request, HttpHeaders headers) throws Exception;

	ResponseOnc obtenerListaPacientexDatos(ListaPacienteRequest request, HttpHeaders headers);
}
