package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ApiResponse;
import pvt.auna.fcompleja.model.api.request.InformacionScgPreRequest;

import java.util.HashMap;

public interface DetalleSolicitudPreliminarService {

    public ApiOutResponse consultarInformacionScgPre(InformacionScgPreRequest request);

    public ApiResponse updateEstadoSolicitudPreliminar(InformacionScgPreRequest request);

    public HashMap<?,?> actualizarEstadoPreliminar(InformacionScgPreRequest request);
    
    public ApiResponse updateEstadoPendInscriptionMAC(InformacionScgPreRequest request);

}
