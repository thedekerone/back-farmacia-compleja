package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.request.LoginRequest;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;

public interface LoginService {

	public OncoWsResponse validarIntentoLogin(LoginRequest loginrq, String idTransaccion, String fechaTransaccion)
			throws Exception;
	
	public OncoWsResponse resetearReintentosLogin(LoginRequest loginrq, String idTransaccion, String fechaTransaccion)
			throws Exception;
}
