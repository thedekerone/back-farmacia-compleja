package pvt.auna.fcompleja.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import org.springframework.web.client.RestTemplate;
import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.dao.impl.AccesoAsignadoDaoImpl;
import pvt.auna.fcompleja.model.api.*;
import pvt.auna.fcompleja.model.api.request.seguridad.UsuarioRequest;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.model.api.response.UsuarioOauthResponse;
import pvt.auna.fcompleja.service.AccesoAsignadoService;

import java.util.HashMap;

@Service
public class AccesoAsignadoServiceImpl implements AccesoAsignadoService {

	@Autowired
	private AccesoAsignadoDaoImpl accesoAsignadoDaoImpl;

	@Autowired
	private AseguramientoPropiedades aseProp;

	@Autowired
	RestTemplate restTemplate;

	@Override
	public OncoWsResponse consultarMenu(UsuarioRequest request, HttpHeaders headers) throws Exception {
		return this.accesoAsignadoDaoImpl.consultarMenu(request, headers);
	}

	@Override
	public OncoWsResponse consultarUsuarioRol(UsuarioRequest request) throws Exception {
		return this.accesoAsignadoDaoImpl.consultarUsuarioRol(request);
	}

	@Override
	public OncoWsResponse consultarUsuario(UsuarioRequest request) throws Exception {
		return this.accesoAsignadoDaoImpl.consultarUsuario(request);
	}

	@Override
	public OncoWsResponse consultarUsuariosPorRol(UsuarioRequest request, HttpHeaders header) throws Exception {
		return this.accesoAsignadoDaoImpl.consultarUsuariosPorRol(request, header);
	}

	@Override
	public OncoWsResponse obtenerPorUsuarioId(UsuarioRequest request, HttpHeaders headers) {
		return this.accesoAsignadoDaoImpl.obtenerPorUsuarioId(request, headers);
	}

	@Override
	public UsuarioOauthResponse obtenerUsuarioPorToken(String token) {
		UsuarioOauthResponse response=new UsuarioOauthResponse();

		String oauth = this.aseProp.getOauth2() + "/token/obtenerUsuario/"+token;

		try {
			response = restTemplate.postForObject(oauth, null,UsuarioOauthResponse.class);

		} catch (Exception e) {
			AudiOauthResponse audi=new AudiOauthResponse();
			audi.setCodRespuesta("-1");
			audi.setMensajeRespuesta("Error al obtener usuario por token.");
			response.setAudiResponse(audi);
			response.setCodUsuario(null);
			response.setUsuario("");
		}
		return response;
	}

}
