package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.bean.FiltroHistorialCargaBean;
import pvt.auna.fcompleja.model.bean.GastoConsumoBean;
import pvt.auna.fcompleja.model.bean.HistorialCargaGastosBean;

import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public interface ControlGastoService {
    ApiOutResponse listArchivoCargadoGastos(FiltroHistorialCargaBean filtro);
	ApiOutResponse importFile(MultipartFile mFile ,int codusuario,String nombreUsuario);
    void actualizarEstado();
}
