package pvt.auna.fcompleja.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pvt.auna.fcompleja.dao.BandejaSolicitudEvaluacionDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ListFiltroRolResponse;
import pvt.auna.fcompleja.model.api.ListaBandeja;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.EmailAlertaCmacRequest;
import pvt.auna.fcompleja.model.api.request.ListaBandejaRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.request.registrarEvaluacionCmac;
import pvt.auna.fcompleja.model.api.request.evaluacion.ProgramacionCmacRequest;
import pvt.auna.fcompleja.service.BandejaSolicitudEvaluacionService;

@Service
public class BandejaSolicitudEvaluacionServiceImpl implements BandejaSolicitudEvaluacionService {

	@Autowired
	private BandejaSolicitudEvaluacionDao listaBandejaEvaluacionDao;

	@Override
	public ApiOutResponse BandejaEvaluacion(ListaBandejaRequest objA_evaluacion) throws Exception {
		return listaBandejaEvaluacionDao.BandejaEvaluacion(objA_evaluacion);
	}

	@Override
	public ApiOutResponse consultarEvaluacionXFecha(EmailAlertaCmacRequest objA_request) {
		return listaBandejaEvaluacionDao.consultarEvaluacionXFecha(objA_request);
	}

	@Override
	public ApiOutResponse registrarEvaluacionCmac(registrarEvaluacionCmac request) {
		// TODO Auto-generated method stub
		List<ListaBandeja> listaEvaluacion = request.getListaEvaluacion();
		List<ListFiltroRolResponse> listaParticipante = request.getListaParticipante();
		String evaluacion;
		String participantes;

		StringBuilder listaEvaluacioncmac = new StringBuilder();
		StringBuilder listaparticipanteCmac = new StringBuilder();

		for (ListaBandeja listaevaluaciones : listaEvaluacion) {
			listaEvaluacioncmac.append(listaevaluaciones.getCodSolEvaluacion() + ","
					+ listaevaluaciones.getResultadoCMAC() + "," + listaevaluaciones.getObservacionCMAC() + "|");
		}
		evaluacion = listaEvaluacioncmac.substring(0, listaEvaluacioncmac.length() - 1);
		request.setEvaluacion(evaluacion);

		if (request.getCodActaFtp() == null) {
			for (ListFiltroRolResponse listaEvaluacionParticipante : listaParticipante) {
				listaparticipanteCmac.append(listaEvaluacionParticipante.getCodigoUsuario() + ",");
			}
		} else {
			for (ListFiltroRolResponse listaEvaluacionParticipante : listaParticipante) {
				listaparticipanteCmac.append(listaEvaluacionParticipante.getCodUsuario() + ",");
			}
		}

		participantes = listaparticipanteCmac.substring(0, listaparticipanteCmac.length() - 1);
		request.setParticipanteCmac(participantes);

		return listaBandejaEvaluacionDao.registrarEvaluacionCmac(request);
	}
	
	@Override
	public WsResponse eliminarRegEvaluacionCmac( SolicitudEvaluacionRequest request ) {
		return listaBandejaEvaluacionDao.eliminarRegEvaluacionCmac(request);
	};

	@Override
	public ApiOutResponse EvaluacionXCodigo(ListaBandejaRequest request) {
		// TODO Auto-generated method stub
		return listaBandejaEvaluacionDao.EvaluacionXCodigo(request);
	}

	@Override
	public ApiOutResponse consultarEvaluacionXCodigo(EmailAlertaCmacRequest request) {
		// TODO Auto-generated method stub
		return listaBandejaEvaluacionDao.consultarEvaluacionXCodigo(request);
	}

	@Override
	public ApiOutResponse actualizarActaEscanProgramacionCmac(ProgramacionCmacRequest request) {
		return listaBandejaEvaluacionDao.actualizarActaEscanProgramacionCmac(request);
	}

}
