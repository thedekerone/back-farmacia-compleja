package pvt.auna.fcompleja.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.config.OncoPropiedades;
import pvt.auna.fcompleja.dao.AccesoAsignadoDao;
import pvt.auna.fcompleja.dao.FiltroUsrRolDao;
import pvt.auna.fcompleja.dao.MonitoreoDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.DiagnosticoRequest;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.LineaTratamientoRequest;
import pvt.auna.fcompleja.model.api.request.UsuariosPorCodigoRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.BandejaMonitoreoRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionMarcadorRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.MarcadorRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.MonitoreoEvolucionRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.SegEjecutivoRequest;
import pvt.auna.fcompleja.model.api.request.seguridad.UsuarioRequest;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.model.api.response.evaluacion.ParticipanteResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.LineaTratamientoResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoEvolucionResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.SegEjecutivoResponse;
import pvt.auna.fcompleja.model.bean.PacienteBean;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.service.GenericoService;
import pvt.auna.fcompleja.service.MonitoreoService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;

@Service
public class MonitoreoServiceImpl implements MonitoreoService {

	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	MonitoreoDao monitoreoDao;
	@Autowired
	FiltroUsrRolDao filtroUsrRolDao;
	@Autowired
	OncoPacienteService oncoPacienteService;
	@Autowired
	GenericoService genericoService;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private OncoPropiedades propiedades;
	@Autowired
	private AccesoAsignadoDao accesoAsignadoDao;
	@Autowired
	UsuarioService usuarioService;

	private Formatter fmt;

	@Override
	@SuppressWarnings("unchecked")
	public ApiOutResponse listaMonitoreo(BandejaMonitoreoRequest bandejaMonitoreoRequest, HttpHeaders headers) {
		System.out.println(bandejaMonitoreoRequest);
		ApiOutResponse out = monitoreoDao.listaMonitoreo(bandejaMonitoreoRequest);
		List<MonitoreoResponse> lista = (List<MonitoreoResponse>) out.getResponse();

		try {
			// LISTA DATOS DATOS RESPONSABLE
			UsuariosPorCodigoRequest usuarioPorCodigoRequest = new UsuariosPorCodigoRequest();
			String codUsuarioCod = "";
			for (MonitoreoResponse mon : lista) {
				if (mon.getCodResponsableMonitoreo() != null) {
					if (codUsuarioCod.equals(""))
						codUsuarioCod = codUsuarioCod + mon.getCodResponsableMonitoreo();
					else
						codUsuarioCod = codUsuarioCod + "|" + mon.getCodResponsableMonitoreo();
				}
			}

			usuarioPorCodigoRequest.setCodigoUsuario(codUsuarioCod);

			List<UsuarioBean> listUsuario;
			listUsuario = (List<UsuarioBean>) usuarioService.listarUsuarioPorCodigos(usuarioPorCodigoRequest, headers)
					.getDataList();

			for (MonitoreoResponse mon : lista) {
				if (mon.getCodResponsableMonitoreo() != null) {
					for (UsuarioBean userRol : listUsuario) {
						if (userRol.getCodUsuario().toString().equals(mon.getCodResponsableMonitoreo().toString())) {
							mon.setNomResponsableMonitoreo(userRol.getApellidoPaterno() + " "
									+ userRol.getApellidoMaterno() + ", " + userRol.getNombre());
							break;
						}
					}
				}
			}

			// LISTA LOS DATOS DE PACIENTE
			String codigosRequest = "";
			HashMap<String, String> map = new HashMap<>();
			for (MonitoreoResponse mon : lista) {
				map.put(mon.getCodigoPaciente(), mon.getCodigoPaciente());
			}
			for (Map.Entry<String, String> entry : map.entrySet()) {
				codigosRequest += "|" + entry.getValue();
			}

			AfiliadosRequest request = new AfiliadosRequest(codigosRequest);
			request.setIni(0);
			request.setFin(map.size());

			List<PacienteBean> listaPaciente = (List<PacienteBean>) oncoPacienteService
					.obtenerListaPacientexCodigos(request, headers).getDataList();
			if (listaPaciente == null) {
				listaPaciente = new ArrayList<>();
			}

			// COMPLETA DATOS POR CADA MONITOREO
			for (MonitoreoResponse mon : lista) {
				// FILTRACION DE DATOS DE PACIENTE
				List<PacienteBean> tempPac = listaPaciente.stream()
						.filter(x -> x.getCodafir().equals(mon.getCodigoAfiliado())).collect(Collectors.toList());
				if (tempPac.size() > 0) {
					mon.setNombrePaciente(tempPac.get(0).getApepat() + " " + tempPac.get(0).getApemat() + ", "
							+ tempPac.get(0).getNombr1()
							+ (tempPac.get(0).getNombr2() != null ? " " + tempPac.get(0).getNombr2() : ""));
					mon.setSexoPaciente(tempPac.get(0).getSexafi());
				} else {
					mon.setNombrePaciente("-");
					mon.setSexoPaciente("-");
				}
			}
		} catch (Exception e) {
			LOGGER.info(
					"[SERVICIO:  CONSULTAR MONITOREOS - ERROR AL LISTAR DATOS DE PACIENTE][" + e.getMessage() + "]");
		}

		out.setResponse(lista);
		return out;
	}

	@Override
	@SuppressWarnings("unchecked")
	public ApiOutResponse listLineaTratamiento(LineaTratamientoRequest request, HttpHeaders headers) {
		ApiOutResponse out = monitoreoDao.historialLineaTratamiento(request);
		List<LineaTratamientoResponse> listLinea = (List<LineaTratamientoResponse>) out.getResponse();
		try {
			UsuarioRequest req = new UsuarioRequest();
			for (LineaTratamientoResponse lin : listLinea) {
				if (lin.getCodAuditorEvaluacion() != null) {
					req.setCodUsuario(lin.getCodAuditorEvaluacion());
					req.setCodAplicacion(1);
					OncoWsResponse oncoResp = accesoAsignadoDao.obtenerPorUsuarioId(req, headers);

					if (oncoResp.getAudiResponse().getCodigoRespuesta().equals("0")) {
						List<UsuarioBean> lUsuarios = (List<UsuarioBean>) oncoResp.getDataList();
						Map<String, String> usu = (Map<String, String>) lUsuarios.get(0);
						lin.setNomAuditorEvaluacion(usu.get("apellidoPaterno") + " " + usu.get("apellidoMaterno") + ", "
								+ usu.get("nombre"));
						// lin.setNomAuditorEvaluacion(""+oncoResp.);
					}
				}
			}

			out.setResponse(listLinea);
		} catch (Exception e) {
			LOGGER.info("Ocurrio un erro en consulta de datos de Servicio Seguridad");
		}
		return out;
	}

	@Override
	public ApiOutResponse getUltimoLineaTratamiento(LineaTratamientoRequest request) {
		// TODO Auto-generated method stub
		return monitoreoDao.getUltimoLineaTratamiento(request);
	}

	@Override
	public ApiOutResponse consultarMarcadores(MarcadorRequest request) {
		return monitoreoDao.listarMarcadores(request);
	}

	@Override
	public ApiOutResponse regDatosEvolucion(EvolucionRequest request) {
		String marcadoresStr = "";
		if (request.getListaMarcadores().size() > 0) {
			for (EvolucionMarcadorRequest em : request.getListaMarcadores()) {
				marcadoresStr = marcadoresStr + em.getCodMarcador() + ","
						+ ((em.getCodResultado() != null) ? em.getCodResultado() : 0) + ","
						+ ((em.getResultado() != null) ? em.getResultado() : " ") + ","
						+ DateUtils.getDateToStringDDMMYYYY(em.getFecResultado()) + "," + em.getTieneRegHc() + ","
						+ em.getpPerMinima() + "," + em.getpPerMaxima() + "," + em.getpTipoIngresoRes() + ","
						+ em.getDescPerMinima() + "," + em.getDescPerMaxima() + "," + em.getDescripcion() + ","
						+ ((em.getUnidadMedida() != null) ? em.getUnidadMedida() : " ") + "|";

			}
			marcadoresStr = marcadoresStr.substring(0, marcadoresStr.length() - 1);
			fmt = new Formatter();
			request.setNroDescEvolucion(fmt.format("%03d", request.getNroEvolucion()).toString());

			System.out.println(marcadoresStr);
			request.setCadenaMarcadores(marcadoresStr);
		}

		return monitoreoDao.regDatosEvolucion(request);
	}

	@Override
	public ApiOutResponse actDatosEvolucion(EvolucionRequest request) {
		String marcadoresStr = "";
		if (request.getListaMarcadores().size() > 0) {
			for (EvolucionMarcadorRequest em : request.getListaMarcadores()) {
				marcadoresStr = marcadoresStr + em.getCodEvolucionMarcador() + ","
						+ ((em.getCodResultado() != null) ? em.getCodResultado() : 0) + ","
						+ ((em.getResultado() != null) ? em.getResultado() : " ") + ","
						+ DateUtils.getDateToStringDDMMYYYY(em.getFecResultado()) + "," + em.getTieneRegHc() + "|";
			}
			marcadoresStr = marcadoresStr.substring(0, marcadoresStr.length() - 1);

			System.out.println(marcadoresStr);
			request.setCadenaMarcadores(marcadoresStr);
		}

		return monitoreoDao.actDatosEvolucion(request);
	}

	@Override
	public ApiOutResponse consultarEvoluciones(EvolucionRequest request) {
		return monitoreoDao.consultarEvoluciones(request);
	}

	@Override
	public ApiOutResponse regResultadoEvolucion(EvolucionRequest request) {
		return monitoreoDao.regResultadoEvolucion(request);
	}

	@Override
	public ApiOutResponse consultarDetalleEvolucion(EvolucionMarcadorRequest request) {
		return monitoreoDao.consultarDetalleEvolucion(request);
	}

	@Override
	public ApiOutResponse listarHistorialMarcadores(MarcadorRequest request) {
		return monitoreoDao.listarHistorialMarcadores(request);
	}

	@Override
	public ApiOutResponse actualizarMonitoreoPendInfo(EvolucionRequest request) {
		return monitoreoDao.actualizarMonitoreoPendInfo(request);
	}

	@Override
	public ApiOutResponse regSeguimientoEjecutivo(SegEjecutivoRequest request) {
		return monitoreoDao.regSeguimientoEjecutivo(request);
	}

	@Override
	@SuppressWarnings("unchecked")
	public ApiOutResponse listarSeguimientoEjecutivo(SegEjecutivoRequest request) {
		ApiOutResponse out = monitoreoDao.listarSeguimientoEjecutivo(request);
		List<SegEjecutivoResponse> lseg = (List<SegEjecutivoResponse>) out.getResponse();
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
		for (SegEjecutivoResponse seg : lseg) {
			seg.setHoraRegistro(sdf.format(seg.getFechaRegistro()));
		}

		out.setResponse(lseg);
		return out;
	}

	@Override
	public ApiOutResponse getSeguimientosPendientes(SegEjecutivoRequest request) {
		return monitoreoDao.getSeguimientosPendientes(request);
	}

	@Override
	public ApiOutResponse actEstadoSegPendientes(SegEjecutivoRequest request) {
		return monitoreoDao.actEstadoSegPendientes(request);
	}

	@Override
	public ApiOutResponse getUltRegistroMarcador(EvolucionMarcadorRequest request) {
		return monitoreoDao.getUltimoRegistroMarcador(request);
	}

	@Override
	public ApiOutResponse consDatosGrpDiagnostico(MonitoreoResponse request, HttpHeaders headers) {
		ApiOutResponse outResponse = new ApiOutResponse();

		ResponseOnc diagnosticoResponse = new ResponseOnc();
		diagnosticoResponse = consultaOncosysDatosDiag(request.getCodDiagnostico(), headers);

		if (diagnosticoResponse.getDataList().size() > 0) {
			HashMap<?, ?> map = (HashMap<?, ?>) diagnosticoResponse.getDataList().get(0);
			request.setNomGrupoDiagnostico(map.get("nomgru").toString());
			request.setNomDiagnostico(map.get("nomdia").toString());

			outResponse.setResponse(request);
			outResponse.setCodResultado(0);
			outResponse.setMsgResultado("Consulta exitosa");
		} else {
			outResponse.setResponse(null);
			outResponse.setCodResultado(1);
			outResponse.setMsgResultado("No se pudo obtener datos");
		}

		return outResponse;
	}

	@Override
	public ResponseOnc consultaOncosysDatosDiag(String codDiagnostico, HttpHeaders headers) {
		DiagnosticoRequest diagnosticoRequest = new DiagnosticoRequest();
		ResponseOnc diagnosticoResponse = new ResponseOnc();

		String UrlVcharDiag = propiedades.getOncoDiagnostico() + "/consulta/diagnostico";
		diagnosticoRequest.setTipoBusqueda(ConstanteUtil.tipoXCod);
		diagnosticoRequest.setCodigoDiagnostico(codDiagnostico);
		diagnosticoRequest.setNombreDiagnostico("''");
		diagnosticoRequest.setRegistroInicio(1);
		diagnosticoRequest.setRegistroFin(10);

		HttpEntity<DiagnosticoRequest> Requestdiagnostico = new HttpEntity<DiagnosticoRequest>(diagnosticoRequest,
				headers);
		diagnosticoResponse = restTemplate.postForObject(UrlVcharDiag, Requestdiagnostico, ResponseOnc.class);

		return diagnosticoResponse;
	}

	@Override
	@SuppressWarnings("unchecked")
	public ApiOutResponse getUltimoMonitoreo(MonitoreoEvolucionRequest request, HttpHeaders headers) {
		ApiOutResponse out = monitoreoDao.getUltimoMonitoreo(request);
		if (out.getCodResultado().equals(0)) {// SE OBTUVO RESULTADO
			List<MonitoreoEvolucionResponse> lista = (List<MonitoreoEvolucionResponse>) out.getResponse();
			MonitoreoEvolucionResponse monEvoRes = lista.get(0);

			UsuariosPorCodigoRequest usuarioPorCodigoRequest = new UsuariosPorCodigoRequest();
			String codUsuarioCodRol = "" + monEvoRes.getCodResponsableMonitoreo();
//			for (ParticipanteResponse part : lpart) {
//				if (part.getCodUsuario() != null) {
//					if (codUsuarioCodRol.equals(""))
//						codUsuarioCodRol = codUsuarioCodRol + part.getCodUsuario();
//					else
//						codUsuarioCodRol = codUsuarioCodRol + "|" + part.getCodUsuario();
//				}
//			}

			usuarioPorCodigoRequest.setCodigoUsuario(codUsuarioCodRol);

			 List<UsuarioBean> listUsuario = (List<UsuarioBean>) usuarioService.listarUsuarioPorCodigos(usuarioPorCodigoRequest, headers)
					.getDataList();

			for (UsuarioBean userRol : listUsuario) {
				if (userRol.getCodUsuario().toString().equals(monEvoRes.getCodResponsableMonitoreo().toString())) {
					monEvoRes.setNomResponsableMonitoreo(userRol.getApellidoPaterno() + " " + userRol.getApellidoMaterno()+" ,"+userRol.getNombre());
					break;
				}
			}

			out.setResponse(lista.get(0));
//			// COMPARACION DE FECHAS
//			if (lista.size() == 1) {
//				out.setResponse(lista.get(0));
//			} else {
//				// COMPARACION DE FECHAS
//				MonitoreoEvolucionResponse mon = (MonitoreoEvolucionResponse) lista.get(0);
//				MonitoreoEvolucionResponse mon2 = (MonitoreoEvolucionResponse) lista.get(1);
//				// if(mon2.getCodEstadoMonitoreo().equals(118))//PENDIENTE
//				if (mon.getCodEstadoMonitoreo().equals(ConstanteUtil.estadoPendienteMonitoreo)) {// VERIFICAR SI EL
//																									// ULTIMO ESTA
//																									// ACTIVO O INACTIVO
//					out.setResponse(mon);
//				} else {
//					Date fecha2 = DateUtils.getStringToDateDDMMYYYY(mon2.getFechaMonitoreo());
//					Long time2 = fecha2.getTime();// FECHA PROGRAMADA
//					Date fecha = DateUtils.getStringToDateDDMMYYYY(mon.getFechaMonitoreo());
//					Long time = fecha.getTime();// FECHA RESULTADO
//					//Long actual = request.getFecha().getTime();
//					Date fechaddMMyyyy = DateUtils.getStringToDateDDMMYYYY(request.getFecha());
//					Long actual = fechaddMMyyyy.getTime();
//
//					Long comparacion2 = actual - time2;
//					Long comparacion = actual - time;
//
//					if (comparacion2 < comparacion) {
//						out.setResponse(mon2);
//					} else {
//						out.setResponse(mon);
//					}
//				}
//			}
		} else {
			out.setResponse(null);
		}
		return out;
	}

}
