package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.FiltroUsrRolRequest;
import pvt.auna.fcompleja.model.api.FiltroUsrRolResponse;

public interface FiltroUsrRolService {

	FiltroUsrRolResponse FiltroUsuarioRol( FiltroUsrRolRequest FiltroListaUsuario)throws Exception;
}
