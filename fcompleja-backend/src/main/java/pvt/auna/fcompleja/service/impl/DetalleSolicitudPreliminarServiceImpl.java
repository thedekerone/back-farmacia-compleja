package pvt.auna.fcompleja.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pvt.auna.fcompleja.dao.DetalleSolicitudPreliminarDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ApiResponse;
import pvt.auna.fcompleja.model.api.request.InformacionScgPreRequest;
import pvt.auna.fcompleja.service.DetalleSolicitudPreliminarService;

import java.util.HashMap;


@Service
public class DetalleSolicitudPreliminarServiceImpl implements DetalleSolicitudPreliminarService {

    private static final Logger log = LoggerFactory.getLogger(DetalleSolicitudPreliminarServiceImpl.class);

    @Autowired
    DetalleSolicitudPreliminarDao detalleSolicitudPreliminarDao;

    @Override
    public ApiOutResponse consultarInformacionScgPre(InformacionScgPreRequest request) {
    	return detalleSolicitudPreliminarDao.consultarInformacionScgPre(request);
    }

    @Override
    public ApiResponse updateEstadoSolicitudPreliminar(InformacionScgPreRequest request) {
        return detalleSolicitudPreliminarDao.updateEstadoSolicitudPreliminar(request);
    }

	@Override
	public HashMap<?,?> actualizarEstadoPreliminar(InformacionScgPreRequest request) {
		
		HashMap<?,?> hshDataMap = new HashMap<>();
		try {
			log.info("Ingreso al Service [DetalleSolicitudPreliminarServiceImpl][actualizarEstadoPreliminar]");
		hshDataMap = detalleSolicitudPreliminarDao.actualizarEstadoPreliminar(request);
		} catch (Exception e) {
			log.error("Error [DetalleSolicitudPreliminarServiceImpl][actualizarEstadoPreliminar] : "+e);
		}
		return hshDataMap;
	}

	@Override
	public ApiResponse updateEstadoPendInscriptionMAC(InformacionScgPreRequest request) {
		return this.detalleSolicitudPreliminarDao.updateEstadoPendInscriptionMAC(request);
	}
    

}

