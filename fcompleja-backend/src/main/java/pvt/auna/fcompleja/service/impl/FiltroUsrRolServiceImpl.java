package pvt.auna.fcompleja.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pvt.auna.fcompleja.dao.FiltroUsrRolDao;
import pvt.auna.fcompleja.model.api.FiltroUsrRolRequest;
import pvt.auna.fcompleja.model.api.FiltroUsrRolResponse;
import pvt.auna.fcompleja.service.FiltroUsrRolService;

@Service
public class FiltroUsrRolServiceImpl implements FiltroUsrRolService {

	@Autowired
	FiltroUsrRolDao filtroUsrRolDao;
	
	@Override
	public FiltroUsrRolResponse FiltroUsuarioRol(FiltroUsrRolRequest ListaUsuario) throws Exception {
		// TODO Auto-generated method stub
		return filtroUsrRolDao.FiltroUsuarioRol(ListaUsuario);
	}

}
