package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.RolResponsableResponse;

public interface RolResponsableService {

	RolResponsableResponse RolResponsable()throws Exception;
}
