package pvt.auna.fcompleja.service;

import javax.servlet.http.HttpServletRequest;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ExcelDownloadResponse;
import pvt.auna.fcompleja.model.api.request.ListaActasMAC;
import pvt.auna.fcompleja.model.api.request.ReporteIndicadoresRequest;
import pvt.auna.fcompleja.model.api.request.RequestImputReportEvaluacion;
import pvt.auna.fcompleja.model.api.request.evaluacion.ListCasosEvaluar;
import pvt.auna.fcompleja.model.api.response.ReporteIndicadorResponse;
import pvt.auna.fcompleja.model.bean.ReporteListaCasosRequestBean;

public interface ReporteService {

    byte[] informeSolEvaReporte(RequestImputReportEvaluacion request, byte[] imagenFirma) throws Exception;
    
    ApiOutResponse informeSolEvaReporte(RequestImputReportEvaluacion request) throws Exception;

    byte[] imprimirReporteListaCasos(ReporteListaCasosRequestBean request) throws Exception;

	byte[] informeListaCasosReporte(ListCasosEvaluar objA_request) throws Exception;
	
	byte[] informeListaActasMACReporte(ListaActasMAC objA_request) throws Exception;
	
	ApiOutResponse generarIndicador(ReporteIndicadoresRequest objA_request) throws Exception;
	ApiOutResponse generarIndicadoresExcel(ReporteIndicadoresRequest reporteRequest,HttpServletRequest request)throws Exception;
	ApiOutResponse generarReporteIndicadorMedicamentoMAC(ReporteIndicadoresRequest requestRequest)throws Exception;
	ApiOutResponse generarReporteIndicadorMedicamentoGrupoMAC(ReporteIndicadoresRequest requestRequest)throws Exception;
	ApiOutResponse generarReporteIndicadorMedicamentoGrupoMACLinea(ReporteIndicadoresRequest requestRequest)throws Exception;
	// ApiOutResponse generarIndicadoresConsumoPorMacExcel(ReporteIndicadoresRequest requestRequest)throws Exception;
	byte[] generarIndicadoresConsumoPorMacExcel(ReporteIndicadoresRequest requestRequest)throws Exception;
	ApiOutResponse generarIndicadoresConsumoPorGrupoMacExcel(ReporteIndicadoresRequest requestRequest)throws Exception;
	ApiOutResponse generarIndicadoresConsumoPorGrupoMacLineaExcel(ReporteIndicadoresRequest requestRequest)throws Exception;
	public ApiOutResponse generarIndicadoresConsumoPorGrupoMacLineaTiempoExcel(ReporteIndicadoresRequest requestRequest)throws Exception;
	public ApiOutResponse generarReporteSolicitudesAutorizaciones(ReporteIndicadoresRequest requestRequest)throws Exception;
	public ApiOutResponse generarReporteGenerales(ReporteIndicadoresRequest requestRequest)throws Exception;
	public ApiOutResponse generarReporteSolicitudesMonitoreo(ReporteIndicadoresRequest requestRequest)throws Exception;
}
