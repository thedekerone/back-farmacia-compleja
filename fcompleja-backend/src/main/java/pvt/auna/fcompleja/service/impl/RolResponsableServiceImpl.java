package pvt.auna.fcompleja.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pvt.auna.fcompleja.dao.RolResponsableDao;
import pvt.auna.fcompleja.model.api.RolResponsableResponse;
import pvt.auna.fcompleja.service.RolResponsableService;

@Service
public class RolResponsableServiceImpl implements RolResponsableService {

	@Autowired
	RolResponsableDao ListaRolDao;

	@Override
	public RolResponsableResponse RolResponsable() throws Exception {
		// TODO Auto-generated method stub
		return ListaRolDao.RolResponsable();
	}

}
