/**
 * 
 */
package pvt.auna.fcompleja.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pvt.auna.fcompleja.dao.MaestrosDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.bean.ExamenMedicoBean;
import pvt.auna.fcompleja.model.bean.MarcadoresBean;
import pvt.auna.fcompleja.model.bean.ProductoAsociadoBean;
import pvt.auna.fcompleja.service.MaestroService;

/**
 * @author Jose.Reyes/MDP
 *
 */
@Service
public class MaestroServiceImpl implements MaestroService {
	
	@Autowired
	private MaestrosDao maestroDao;


	@Override
	public ApiOutResponse registroExamenesMedicos(ExamenMedicoBean examenMedicoBean) {
		// TODO Auto-generated method stub
		return maestroDao.registroExamenesMedicos(examenMedicoBean);
	}

	@Override
	public ApiOutResponse listarExamenesMedicos(ExamenMedicoBean examenesMedicos) {
		// TODO Auto-generated method stub
		return maestroDao.listarExamenesMedicos(examenesMedicos);
	}

	@Override
	public ApiOutResponse registroMarcadores(MarcadoresBean marcadores) {
		// TODO Auto-generated method stub
		return maestroDao.registroMarcadores(marcadores);
	}

	@Override
	public ApiOutResponse listarMarcaodres(MarcadoresBean marcadores) {
		// TODO Auto-generated method stub
		return maestroDao.listarMarcaodres(marcadores);
	}

	@Override
	public ApiOutResponse registroProductoAsociado(ProductoAsociadoBean productoAsociado) {
		// TODO Auto-generated method stub
		return maestroDao.registroProductoAsociado(productoAsociado);
	}

	@Override
	public ApiOutResponse listarProductoAsociado(ProductoAsociadoBean productoAsociado) {
		// TODO Auto-generated method stub
		return maestroDao.listarProductoAsociado(productoAsociado);
	}

}
