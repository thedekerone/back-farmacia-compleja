package pvt.auna.fcompleja.service;

import org.springframework.http.HttpHeaders;

import org.springframework.web.multipart.MultipartFile;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ArchivoFTPBean;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.RequestImputReportEvaluacion;
import pvt.auna.fcompleja.model.api.response.ResponseFTP;

public interface GenericoService {

	public ResponseFTP enviarArchivo(String usrApp, String nomArchivo, MultipartFile archivo, String ruta,
			HttpHeaders header) throws Exception;

//	  public String obtenerDiagnostico(String codDiagnostico) throws Exception;

	public RequestImputReportEvaluacion obtenerAfiliados(long codAfiliado) throws Exception;

	public byte[] obtenerArchivoFTP(String nombre, String ruta, String usrApp) throws Exception;

	public String obtenerGrupoDiagnostico(String codDiagnostico) throws Exception;

	public String obtenerPaciente(AfiliadosRequest pacienteRequest) throws Exception;

	public ApiOutResponse enviarArchivoFTP(String usrApp, String nomArchivo, MultipartFile archivo, String ruta,
			HttpHeaders header) throws Exception;

	public ApiOutResponse descargarArchivo(ArchivoFTPBean archivoBean, HttpHeaders headers) throws Exception;
	
	public ApiOutResponse enviarArchivoFTP(String usrApp, String nomArchivo, byte[] archivo, String ruta,
			HttpHeaders header) throws Exception;

}
