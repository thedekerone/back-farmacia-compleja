package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.FiltroLiderTumorRequest;
import pvt.auna.fcompleja.model.api.FiltroLiderTumorResponse;

public interface FiltroLiderTumorService {

	FiltroLiderTumorResponse FiltroLiderTumor(FiltroLiderTumorRequest ListaLiderTumor ) throws Exception;
	
}
