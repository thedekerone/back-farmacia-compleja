package pvt.auna.fcompleja.service.impl;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import org.jfree.util.Log;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationTemp;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;


import pvt.auna.fcompleja.config.ConfigFTPProp;
import pvt.auna.fcompleja.dao.ControlGastoDao;
import pvt.auna.fcompleja.model.api.*;
import pvt.auna.fcompleja.model.api.request.ListaPacienteRequest;
import pvt.auna.fcompleja.model.api.request.seguridad.UsuarioRequest;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoResponse;
import pvt.auna.fcompleja.model.bean.*;
import pvt.auna.fcompleja.service.*;
import pvt.auna.fcompleja.util.AunaUtil;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@Service
public class ControlGastoServiceImpl implements ControlGastoService {

    @Autowired
    private ControlGastoDao controlGastoDao;
    
    @Autowired
    private  MonitoreoService monitoreo;

    @Autowired
    private  AccesoAsignadoService usuarios;

    
    @Autowired
	@SuppressWarnings("rawtypes")
    private AunaUtil util; 
    
    
	@Autowired
	GenericoService service;

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	private ConfigFTPProp FTPprop;	

   @Autowired
    private MacService macService;

	@Autowired
	OncoDiagnosticoService diagService;

	@Autowired
	OncoPacienteService oncoPacienteService;
	
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ControlGastoServiceImpl.class);

	@Override
	public ApiOutResponse listArchivoCargadoGastos(FiltroHistorialCargaBean filtro) {
		ApiOutResponse apiOut = new ApiOutResponse();
		
		try {						
			apiOut= controlGastoDao.listArchivoCargadoGastos(filtro); 			
			apiOut=ObtieneNombreUsuario(apiOut);
			
		} catch (Exception e) {
			apiOut.setCodResultado(99);
			apiOut.setMsgResultado(e.getMessage());
			apiOut.setResponse(null);
			e.printStackTrace();
		}
		return apiOut;
	}       
    
    
    private List<ListaOrdenenadaCargasBean> Castear(ArrayList<HistorialCargaGastosBean> listaAOrdenar) {
		// TODO Auto-generated method stub
    	List<ListaOrdenenadaCargasBean> respuesta = new ArrayList<>();
    	
    	for(HistorialCargaGastosBean item : listaAOrdenar) {

    		ListaOrdenenadaCargasBean obj = null;

    		obj.setCodigoControlGasto(item.getCodigoControlGasto());
    		obj.setFechaInicio(item.getFechaInicio());
    		obj.setFechaFinal(item.getFechaFinal());
    		obj.setFechaCarga(item.getFechaCarga());
    		obj.setResponsableCarga(item.getResponsableCarga());
    		obj.setEstadoCarga(item.getEstadoCarga());
    		obj.setDesEstadoCarga(item.getDesEstadoCarga());
    		obj.setRegistroTotal(item.getRegistroTotal());
    		obj.setRegistroCargado(item.getRegistroCargado());
    		obj.setRegistroError(item.getRegistroError());
    		obj.setVerLog(item.getVerLog());
    		obj.setDescarga(item.getDescarga());
    		obj.setCodArchivoLog(item.getCodArchivoLog());
    		obj.setCodArchivoDescarga(item.getCodArchivoDescarga());
    		obj.setUsuarioCreacion(item.getUsuarioCreacion());
    		obj.setFechaCreacion(item.getFechaCreacion());
    		obj.setUsuarioModificacion(item.getUsuarioModificacion());
    		obj.setFechaModificacion(item.getFechaModificacion());
    		obj.setNombreUsuario(item.getNombreUsuario());
    		obj.setHoraCarga(item.getHoraCarga());
    		obj.setArchivoNombre(item.getArchivoNombre());
    		obj.setArchivoRuta(item.getArchivoRuta());
    		
    		respuesta.add(obj);
    	}
    	return respuesta;
	}


	private ApiOutResponse ObtieneNombreUsuario(ApiOutResponse apiOut) {
    	ApiOutResponse respuesta = new ApiOutResponse();
    	
		UsuarioRequest request =new UsuarioRequest();

		ArrayList<HistorialCargaGastosBean> itemCargaGasto = (ArrayList<HistorialCargaGastosBean>) apiOut.getResponse();
		ArrayList<HistorialCargaGastosBean> itemCargaGastoNombre = (ArrayList<HistorialCargaGastosBean>) apiOut.getResponse();
		respuesta = apiOut;
		String nombreUsuario="";
		
		try {
			int i=0;
			for(HistorialCargaGastosBean item : itemCargaGasto){
				HistorialCargaGastosBean  itemConNombre = new HistorialCargaGastosBean();
				itemConNombre=item;
				request.setCodUsuario(item.getResponsableCarga());
				OncoWsResponse usuario = usuarios.consultarUsuarioRol(request);

				List<LinkedHashMap<String, String>> linkedHashMaps = (ArrayList<LinkedHashMap<String,String>>) usuario.getDataList();

				nombreUsuario  = linkedHashMaps.get(0).get("nombres") + " " + linkedHashMaps.get(0).get("apePaterno");

				itemConNombre.setNombreUsuario(nombreUsuario);
				itemCargaGastoNombre.set(i, itemConNombre);
				i++;
			}				
			respuesta.setResponse(itemCargaGastoNombre); 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
		return respuesta;
	}


	@Override
    public ApiOutResponse importFile(MultipartFile mFile , int codusuario, String nombreUsuario) {
    	
		InputStream file;; 
    	ApiOutResponse askImportFile = new ApiOutResponse();
    	Boolean IndRegistrar=false;
    	        
        HistorialCargaGastosBean objCabecera = new HistorialCargaGastosBean();    
        List<ApiOutResponse> ListaAskdetalleGasto=new ArrayList<>();
        List<ApiOutResponse> ListaAskdetalleGastoBackend=new ArrayList<>();
 
        // Creación de un libro de trabajo a partir de un archivo de Excel (.xls o .xlsx)
        boolean archivoInvalido = false;
        Workbook workbook = null;
        try {
        	file= mFile.getInputStream();
            workbook = WorkbookFactory.create(file);
        } catch (Exception e) {
        	archivoInvalido=true;
        }

        // Obtención de la hoja en el índice cero
        Sheet sheet = workbook.getSheetAt(0);
        Integer totalRegistros = sheet.getPhysicalNumberOfRows()-1;

        ApiOutResponse  listaAskGasto = ValidarArchivo(workbook,sheet);
  
        if (listaAskGasto!=null) {    

		  	ApiOutResponse askCabeceraGasto = new ApiOutResponse();        	        	
	    	objCabecera.setNombreUsuario(nombreUsuario);        
	    	askCabeceraGasto.setResponse(objCabecera);
        	List<Object> listResponse = new ArrayList<>();
        	listResponse.add(askCabeceraGasto);        	
        	askImportFile.setMsgResultado("Archivo Observado");
        	askImportFile.setListResponse(listResponse);
        	return listaAskGasto;
        }

        //1.- Registro Cabecera Gasto (POA : 20190613: OJO:Pendiente incluir en Transaccion)
        //Obtener Ubicacion de datos
        objCabecera.setFechaCarga(null);  	             
        objCabecera.setResponsableCarga(codusuario);           
        objCabecera.setEstadoCarga(0);                
        objCabecera.setRegistroTotal(totalRegistros);
        objCabecera.setRegistroCargado(0);            
        objCabecera.setRegistroError(0);              
        objCabecera.setVerLog(null);                     
        objCabecera.setDescarga(null);                   
        objCabecera.setCodArchivoLog(null);              
        objCabecera.setCodArchivoDescarga(null);	
        objCabecera.setUsuarioCreacion(codusuario);
        objCabecera.setUsuarioModificacion(codusuario);
        objCabecera.setNombreUsuario(nombreUsuario);

    	ApiOutResponse askCabeceraGasto = new ApiOutResponse();

    	//Aqui registra en la tabla pfc_hist_control_gasto - Cabecera
    	askCabeceraGasto = InsertaRegistroGastosCab(objCabecera);

    	HistorialCargaGastosBean objCabeceraAux = new HistorialCargaGastosBean();
    	objCabeceraAux = (HistorialCargaGastosBean)askCabeceraGasto.getResponse();
    	Integer codigoControlGasto = objCabeceraAux.getCodigoControlGasto();

    	objCabecera.setCodigoControlGasto(codigoControlGasto);
    	objCabecera.setNombreUsuario(nombreUsuario);

        List<GastoConsumoBean> consumoMedicamentos = new ArrayList<>();

        ApiOutResponse AskdetalleGasto = new ApiOutResponse();

        //listas para integracion
		List<ConsumoBean> listaTemp = new ArrayList<>();

        int i = 0;
        Integer nroItemsConError = 0;

        for (Row row: sheet) {
            if (i>0) {
            	////
            	GastoConsumoBean consumoMedicamento = new GastoConsumoBean();
            	ConsumoBean consumoBean = new ConsumoBean();

				ApiOutResponse ApiOutListItems;

            	consumoMedicamento = ValidaItemDetalle(row,i,objCabecera);  //Obtiene Registro y todas sus observaciones

				ApiOutListItems=ConObservacionesEnDetalle(consumoMedicamento.getObsDetalle());
                ApiOutListItems.setResponse((Object)consumoMedicamento);
            	nroItemsConError =  consumoMedicamento.getObsDetalle().size() > 0 ? ++nroItemsConError : nroItemsConError;
                if(nroItemsConError>0){
                	ApiOutListItems.setCodResultado(1);
                	AskdetalleGasto.setCodResultado(1);
                }

                ListaAskdetalleGasto.add(ApiOutListItems);


            	IndRegistrar=(consumoMedicamento.getIndCampoObligatorio().equals(0) && consumoMedicamento.getIndCampoClave().equals(0))?true:false;
            	if(IndRegistrar) {
                	AskdetalleGasto=InsertaRegistroGastosDet(consumoMedicamento);
                	ListaAskdetalleGastoBackend.add(AskdetalleGasto);

					//------------------------------------------------------------------------------------------------
					//INTEGRACION 1 - AQUI INSERTA LOS CONSUMOS

					consumoBean.setCantidadConsumo(consumoMedicamento.getCantidadConsumo());
					consumoBean.setCodAfiliado(consumoMedicamento.getCodAfiliado());
					consumoBean.setCodDiagnostico(consumoMedicamento.getCodDiagnostico());
					consumoBean.setCodMac(consumoMedicamento.getCodMac());
					consumoBean.setFechaConsumo(consumoMedicamento.getFechaConsumo());
					consumoBean.setMontoConsumo(consumoMedicamento.getMontoConsumo());
					consumoBean.setTipoEncuentro(consumoMedicamento.getTipoEncuentro());
					consumoBean.setLineaTratamiento(consumoMedicamento.getLineaTratamiento());

					Boolean unico = false;
					if (listaTemp.size() == 0) {
						listaTemp.add(consumoBean);
					} else {
						for (ConsumoBean tempo : listaTemp) {
							if (!tempo.getCodAfiliado().equals(consumoBean.getCodAfiliado()) ||
								!tempo.getCodMac().equals(consumoBean.getCodMac()) ||
								!tempo.getCodDiagnostico().equals(consumoBean.getCodDiagnostico())) {
								unico = true;
							} else {

								if (tempo.getTipoEncuentro().equals("HOSPITALIZACION")) {
									tempo.setMontoConsumo(tempo.getMontoConsumo() + consumoBean.getMontoConsumo());
									tempo.setCantidadConsumo(tempo.getCantidadConsumo() + consumoBean.getCantidadConsumo());
									if (tempo.getFechaConsumo().after(consumoBean.getFechaConsumo())) {
										tempo.setFechaConsumo(tempo.getFechaConsumo());
									} else {
										tempo.setFechaConsumo(consumoBean.getFechaConsumo());
									}

								} else {
									if (tempo.getFechaConsumo().after(consumoBean.getFechaConsumo())) {
										tempo.setFechaConsumo(tempo.getFechaConsumo());
										tempo.setMontoConsumo(tempo.getMontoConsumo());
										tempo.setCantidadConsumo(tempo.getCantidadConsumo());
									} else {
										tempo.setFechaConsumo(consumoBean.getFechaConsumo());
										tempo.setMontoConsumo(consumoBean.getMontoConsumo());
										tempo.setCantidadConsumo(consumoBean.getCantidadConsumo());
									}
								}
								unico = false;
							}
						}

						if (unico){
							listaTemp.add(consumoBean);
						}
					}

					//-----------------------------------------------------------------------------------------------

				}
            }
            i++;
        }

		if (listaTemp.size() > 0) {
			for ( ConsumoBean con: listaTemp){
				controlGastoDao.actualizarMonitoreo(con);
			}
		}

    	objCabeceraAux.setNombreUsuario(nombreUsuario);    	
    	Object objAux = (Object)objCabeceraAux;     	    	
    	askCabeceraGasto.setResponse(objAux);
    	askCabeceraGasto.setCodResultado(nroItemsConError);
    	
        
		GastoConsumoBean objActualiza = ActualizaEstadoCarga(objCabecera.getCodigoControlGasto(),nroItemsConError);
		
		LOGGER.info("Actualiza Estado a Cargado", objActualiza);   
        //askImportFile.setCodResultado(askCabeceraGasto.getCodResultado());
		askImportFile.setCodResultado(0);
		askImportFile.setMsgResultado(askCabeceraGasto.getMsgResultado());
        
		try {
			List<Object> listResponse = new ArrayList<>();
			listResponse.add(askCabeceraGasto);							//Resultado Cabecera
			listResponse.add(listaAskGasto);								//Log Cabecera
			listResponse.add(ListaAskdetalleGasto);						//Resultados x Item de Detalle
			listResponse.add(ListaAskdetalleGastoBackend);
			
			
			askImportFile.setListResponse(listResponse);
			
		} catch (Exception e) {
		}

		// if(IndRegistrar) {SubeArchivoXlsFTP(askCabeceraGasto, mFile);}
        SubeArchivoXlsFTP(askCabeceraGasto, mFile);
        SubeArchivoLogFTP(GrabaLog(askImportFile,askCabeceraGasto,listaAskGasto,ListaAskdetalleGasto,ListaAskdetalleGastoBackend));

        LOGGER.info("despeus de subir a ftp.... ");

		return askImportFile;
 
    }


	private void SubeArchivoXlsFTP(ApiOutResponse askCabeceraGasto, MultipartFile file) {

		String ext = FilenameUtils.getExtension(file.getOriginalFilename());

		HistorialCargaGastosBean archivo = (HistorialCargaGastosBean) askCabeceraGasto.getResponse();
		String nomArchivo= "CONTROL_GASTO_CARGA_" + archivo.getCodigoControlGasto() + '.' + ext;
		String ruta = FTPprop.getRutaTemp() + "\\" + nomArchivo;		
        archivo.setArchivoNombre(nomArchivo);
        archivo.setArchivoRuta(ruta);				
        
        SubeArchivoXlsFTP(archivo, file);		
		
	}


	private void SubeArchivoLogFTP(HistorialCargaGastosBean archivo) {
		String ruta = archivo.getArchivoRuta();
		String nomArchivo = archivo.getArchivoNombre();
		MultipartFile file = formatoMultimpart(ruta , nomArchivo);
		String pathFtp = "/CONTROL-GASTO";

		ResponseEntity<WsResponse> respuesta= subirArchivo(file,nomArchivo, pathFtp);
		LOGGER.info("el log........ ", respuesta.toString());
		WsResponse body = respuesta.getBody();
		LOGGER.info("el body........ ", body.toString());
		ArchivoFTPBean data =  (ArchivoFTPBean)body.getData();
		LOGGER.info("el data........ ", data.toString());
		GastoConsumoBean objActualiza = ActualizaCodArchivoLog(data.getCodArchivo(),archivo.getCodigoControlGasto());
		LOGGER.info("el objActualiza........ ", objActualiza.toString());
	}

	
	
	

	private GastoConsumoBean ActualizaCodArchivoLog(Integer codigoFileLogFtp,  Integer codigoHistoricoControlGasto) {
		GastoConsumoBean objActualiza = new GastoConsumoBean();
		try {
			LOGGER.info("Actualiza codigo de archivo LOG generado en FTP", codigoFileLogFtp);
			objActualiza.setCodHistControlGasto(codigoHistoricoControlGasto);
			objActualiza.setCodArchivoLog(codigoFileLogFtp);			
			ApiOutResponse  respuesta=ActualizaRegistroGastosCab(objActualiza);
			LOGGER.info("Respuesta: Actualiza codigo de archivo LOG generado en FTP", respuesta);
			
		} catch (Exception e) {
			LOGGER.error("ActualizaEstadoCarga" + " :" + e.getMessage());
		}				
		return objActualiza;
	}


	private void SubeArchivoXlsFTP(HistorialCargaGastosBean archivo,MultipartFile file) {
		String ruta = archivo.getArchivoRuta();
		String nomArchivo = archivo.getArchivoNombre();
		String pathFtp = "/CONTROL-GASTO";

		ResponseEntity<WsResponse> respuesta= subirArchivo(file,nomArchivo, pathFtp);
		WsResponse body = respuesta.getBody();
		ArchivoFTPBean data =  (ArchivoFTPBean)body.getData();				
		GastoConsumoBean objActualiza = ActualizaCodArchivoXls(data.getCodArchivo(),archivo.getCodigoControlGasto());
				
	}	
	
	
	private GastoConsumoBean ActualizaCodArchivoXls(Integer codigoFileXlsFtp,  Integer codigoHistoricoControlGasto) {
		GastoConsumoBean objActualiza = new GastoConsumoBean();
		try {
			objActualiza.setCodHistControlGasto(codigoHistoricoControlGasto);
			objActualiza.setCodArchivoDescarga(codigoFileXlsFtp);			
			ApiOutResponse  respuesta=ActualizaRegistroGastosCab(objActualiza);
		} catch (Exception e) {
		}
		return objActualiza;
	}	


	private MultipartFile formatoMultimpart(String ruta, String nomarchivo) {
		
		Path path = Paths.get(ruta);
		String name = nomarchivo;
		String originalFileName = nomarchivo;
		String contentType = "text/plain";
		byte[] content = null;
		try {
			content = Files.readAllBytes(path);

			MultipartFile result = new MockMultipartFile(name,
                    originalFileName, contentType, content);	
			
			return result;
			
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

		
	}
	
	private HistorialCargaGastosBean  GrabaLog(ApiOutResponse askImportFile, ApiOutResponse askCabeceraGasto, ApiOutResponse listaAskGasto, List<ApiOutResponse> listaAskdetalleGasto, List<ApiOutResponse> listaAskdetalleGastoBackend) {
        		HistorialCargaGastosBean archivo = (HistorialCargaGastosBean) askCabeceraGasto.getResponse();
				String nomArchivo= "CONTROL_GASTO_CARGA_" + archivo.getCodigoControlGasto() + ".txt";
				String ruta = FTPprop.getRutaTemp() + "\\" + nomArchivo;

		        try {
		        	
		            String contenido = "Contenido de ejemplo";
		            File file = new File(ruta);
		            // Si el archivo no existe es creado
		            if (!file.exists()) {
		                file.createNewFile();
		            }
		            FileWriter fw = new FileWriter(file);
		            BufferedWriter bw = new BufferedWriter(fw);
		            	
	            	List<List<String>> secciones = ObtieneSeccionesFiles(askImportFile, askCabeceraGasto, listaAskGasto, listaAskdetalleGasto,listaAskdetalleGastoBackend);  
	            	bw.write("--------------------------------INICIO CARGA ---------------------------------------------------" + "\n");

	            		for(List<String> seccion : secciones) {
			            	for(String linea : seccion) {
			            		bw.write(linea);
			            	}
		            	}
	            		
	            	bw.write("--------------------------------FINAL CARGA ---------------------------------------------------" + "\n");
		            
		            bw.close();
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		        
		        

		        archivo.setArchivoNombre(nomArchivo);
		        archivo.setArchivoRuta(ruta);
		        
		return archivo;
	}


	private List<List<String>> ObtieneSeccionesFiles(ApiOutResponse askImportFile, ApiOutResponse askCabeceraGasto, 
													ApiOutResponse listaAskGasto, 
													List<ApiOutResponse> listaAskdetalleGasto, 
													List<ApiOutResponse> listaAskdetalleGastoBackend) {
		// TODO Auto-generated method stub
		        	
        	List<List<String>>	seccionesFile = new ArrayList<>();        	                
			seccionesFile.add(FormateaSalidaCabecera(askCabeceraGasto));
			seccionesFile.add(FormateaSalidaObsCabecera(listaAskGasto));
			seccionesFile.add(FormateaSalidaObsDetalle(listaAskdetalleGasto));
			//seccionesFile.add(FormateaSalidaObsDetalleBD(listaAskdetalleGastoBackend));
			
		return seccionesFile;
	}


	private List<String>   FormateaSalidaObsDetalleBD(List<ApiOutResponse> itemResponse) {
		// TODO Auto-generated method stub
		List<String> seccion = new ArrayList<>();
		

		String  line="";		
		String  fechaHora = "";
				
		fechaHora = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
						
		int i=1;
			for(ApiOutResponse Item : itemResponse) {														
				line = fechaHora + "[VALIDACION REGISTRO en BD]:"+ i  + "[INFO] :" + Item.getMsgResultado() + "\n";				
				seccion.add(line);				
				i++;				
			}
			
		return seccion;
	}


	private List<String>   FormateaSalidaObsDetalle(List<ApiOutResponse> itemResponse) {
		
		List<String> seccion = new ArrayList<>();
		

		String  line="";		
		String  fechaHora = "";
		String  sangriaDetalle =  "    ";
				
		fechaHora = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
		

		//seccion.add(sangriaDetalle+".......................INICIO:Validacion Registro.............................................." + "\n");
		int i=1;
			for(ApiOutResponse Item : itemResponse) {
				
				GastoConsumoBean mac = (GastoConsumoBean)Item.getResponse();
				List<String> listObs =  (List<String>) mac.getObsDetalle();

				seccion.add(sangriaDetalle+".......................INICIO:Validacion Registro.............................................." + "\n");
				line =
						"[VALIDACION REGISTRO Nro : "+ i + "]" + " [INFO] :" +
				((Item.getListResponse().size()==0)?"[OK]" + " ->" +
				"{Cod. Afiliado:" + mac.getCodAfiliado()  +  "} ->" +
				"{Cod. MAC" + mac.getCodMac() + "-"+ mac.getDescripPresentGenerico() +  " ->" +
				"{Cod. CIE10:" + mac.getCodDiagnostico()  +  "} ->" +
				"{Fecha Consumo: " + DateUtils.getDateToStringDDMMYYYHHMMSS(mac.getFechaConsumo())  + "} ->" +				
				"{Cantidad Consumo: " + mac.getCantidadConsumo()  + "} ->" +
				"{Monto Consumo: " + mac.getMontoConsumo()  + "} ->" : "[ERROR]") +
				"\n";
				seccion.add(sangriaDetalle+line);				
				i++;		
				Integer nroObervaciones = Item.getListResponse().size();
				seccion.add(sangriaDetalle + "Observaciones: " + (nroObervaciones==0?"Ninguna":nroObervaciones.toString())  +"\n");

				int j=1;
				for(Object obs : Item.getListResponse()) {
					
					ApiOutResponse  obsApi = (ApiOutResponse) obs; 
					line = ">>>>>> Observación "+ j + "  : " + obsApi.getMsgResultado() + "\n";
					seccion.add(sangriaDetalle+line);				
					j++;
				}
				seccion.add(sangriaDetalle+".......................FINAL:Validacion Registro..............................................." + "\n\n");

			}
			
		return seccion;
	}


	private List<String>   FormateaSalidaObsCabecera(ApiOutResponse listaAskGasto) {
		List<String> seccion = new ArrayList<>();
		

		String  line="";		
		String  fechaHora = "";
				
		if(listaAskGasto!=null) {			
			
			Object obj = listaAskGasto.getResponse();
			
			ApiOutResponse lista = (ApiOutResponse) obj;			
			line = fechaHora + "[VALIDACION ARCHIVO]:" + 
					(listaAskGasto.getCodResultado()==0? "ERROR ARCHIVO": "")  + 
							"[INFO] :" + listaAskGasto.getMsgResultado() + "\n";				
			seccion.add(line);	
						
			GastoConsumoBean cabecera = (GastoConsumoBean)lista.getResponse();
		}
		return seccion;	
	}


	private List<String>  FormateaSalidaCabecera(ApiOutResponse itemResponse) {
		// TODO Auto-generated method stub
		List<String> seccion = new ArrayList<>();


		String  line="";		
		String  fechaHora = "";
		
		if(itemResponse==null) return seccion;
		
		HistorialCargaGastosBean lista = (HistorialCargaGastosBean) itemResponse.getResponse();			
		fechaHora = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
		String dia =  fechaHora.substring(0,10); 
		String hora =  fechaHora.substring(11);

		line = "[ARCHIVO: " + lista.getCodigoControlGasto()+"]" + "[Fecha Carga : "+ dia + "  "+ hora +  "]" + "[Usuario: " + lista.getNombreUsuario() + "]" + ((itemResponse.getCodResultado()==0)?"[OK]": "[ERROR]") + "\n\n";  //itemResponse.getMsgResultado()
		seccion.add(line);			

				
		return seccion;
	}


	private GastoConsumoBean ValidaItemDetalle(Row row, int i, HistorialCargaGastosBean objCabecera) {

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		ApiOutResponse ApiOutListItems;

		MACBean request = new MACBean();
		
		Integer obsObligatorios=0;
		Integer obsClaves=0;
		GastoConsumoBean  askProcedure = new GastoConsumoBean();
		List<String>  obsDetalle = new ArrayList<String>();

	    // Cree un DataFormatter para formatear y obtener el valor de cada celda como String
        DataFormatter dataFormatter = new DataFormatter();


        String codCIE10 = "";
        try {
        	codCIE10 = dataFormatter.formatCellValue(row.getCell(13)).trim();
            if (codCIE10.isEmpty()) {
         		obsObligatorios++;
         		obsClaves++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna CIE10: El campo esta vacío");
             }
            else {
                // if (!ExisteDiagnosticoEnOncosys(codCIE10)) {
            	if (false) {		//OJO servicio no respone
                	obsDetalle.add("No fue posible verificar el codigo de Diagnostico en Oncosys ");
                // throw new ServiceException("No existe codigo diagnostico en Oncosys "+(i+1));
                }
            }
        }catch (Exception e) {
     		obsObligatorios++;
     		obsClaves++;
            obsDetalle.add("Error en la columna CIE10");
            //throw new ServiceException("Error en la columna CIE10 "+(i+1));
        }
        codCIE10 = dataFormatter.formatCellValue(row.getCell(13)).trim();
        askProcedure.setCodDiagnostico(codCIE10);


        String codMac = "";
        try {
            codMac = dataFormatter.formatCellValue(row.getCell(0)).trim();
            if (codMac.isEmpty()) {
        		obsObligatorios++;
        		obsClaves++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna COD MAC: El campo esta vacío");
            	//obsDetalle.add("Error en la columna codMac");
            }
            else {            	
        	   //if (!ExisteMacEnOncosys(codMac) ) {
            	if (false) {  //Ojo temporal
	           		obsObligatorios++;
	           		obsClaves++;          		   
                   	obsDetalle.add("No fue posible verificar el codigo Mac en Oncosys ");
                   	//throw new ServiceException("No existe codigo diagnostico en Oncosys "+(i+1));                	
                   }            	            	 
            }

        }catch (Exception e) {
     		obsObligatorios++;
     		obsClaves++;        	            	        	        	
        	obsDetalle.add("Error en la columna codMac");
            //throw new ServiceException("Error en la columna codMac "+(i+1));
        }
        askProcedure.setCodMac(codMac);
        

        String tipoEncuentro = "";
        try {
            tipoEncuentro = dataFormatter.formatCellValue(row.getCell(1)).toUpperCase().trim();
            if (tipoEncuentro.isEmpty()) {
         		obsObligatorios++;
         		obsClaves++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna TIPO DE ENCUENTRO: El campo esta vacío");
			} else if (!tipoEncuentro.matches("HOSPITALIZACION|EMERGENCIA|CONSULTAS EXTERNAS")) {
				obsObligatorios++;
				obsClaves++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna TIPO DE ENCUENTRO: Debe ser HOSPITALIZACION, EMERGENCIA o CONSULTAS EXTERNAS");
			}

        }catch (Exception e) {
     		obsObligatorios++;
     		obsClaves++;        	            	        	        	
        	obsDetalle.add("Error en la columna tipoEncuentro");
            //throw new ServiceException("Error en la columna tipoEncuentro "+(i+1));
        }
        askProcedure.setTipoEncuentro(tipoEncuentro);

        Long encuentro = null;
        try {
            String encu = dataFormatter.formatCellValue(row.getCell(2)).trim();
            if (!encu.isEmpty()) {
                encuentro = Long.parseLong(encu);
            }
            else {
         		obsObligatorios++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna ENCUENTRO: El campo esta vacío");
            	//obsDetalle.add("Error en la columna encuentro");
            }
        }catch (Exception e) {
     		obsObligatorios++;
        	obsDetalle.add("Error en la columna encuentro");
            //throw new ServiceException("Error en la columna encuentro "+(i+1));
        }
        askProcedure.setEncuentro(encuentro);

        
        String codigoSap = "";
        try {
            codigoSap = dataFormatter.formatCellValue(row.getCell(3)).trim();
            if (codigoSap.isEmpty()) {
         		obsObligatorios++;
         		obsClaves++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna PREST. - CODIGO: El campo esta vacío");
            	//obsDetalle.add("Error en la columna codigoSap");
            }                      

        }catch (Exception e) {
     		obsObligatorios++;
     		obsClaves++;        	            	        	        	
        	obsDetalle.add("Error en la columna codigoSap");
            //throw new ServiceException("Error en la columna codigoSap "+(i+1));
        }
        askProcedure.setCodigoSap(codigoSap);

        
        
        String descripActivoMolecula = "";
        try {
            descripActivoMolecula = dataFormatter.formatCellValue(row.getCell(4)).trim();

            if (descripActivoMolecula.isEmpty()) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna GEN. - PRIN. ACT.: El campo esta vacío");
            	//obsDetalle.add("Error en la columna descripActivoMolecula");
            }                      
        
        }catch (Exception e) {
           	obsDetalle.add("Error en la columna descripActivoMolecula");                	
            //throw new ServiceException("Error en la columna descripActivoMolecula "+(i+1));
        }
        askProcedure.setDescripActivoMolecula(descripActivoMolecula);
        

        String descripPresentGenerico = "";
        try {
            descripPresentGenerico = dataFormatter.formatCellValue(row.getCell(5)).trim();
            if (descripPresentGenerico.isEmpty()) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna GENERICO: El campo esta vacío");
            	//obsDetalle.add("Error en la columna descripPresentGenerico");
            }                      
        }catch (Exception e) {
        	obsDetalle.add("Error en la columna descripPresentGenerico");   
            //throw new ServiceException("Error en la columna descripPresentGenerico "+(i+1));
        }
        askProcedure.setDescripPresentGenerico(descripPresentGenerico);
        
        
        String prestacion = "";
        try {
            prestacion = dataFormatter.formatCellValue(row.getCell(6)).trim();
            if (prestacion.isEmpty()) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna PRESTACION: El campo esta vacío");
            	//obsDetalle.add("Error en la columna prestacion");
            }              

        }catch (Exception e) {
        	obsDetalle.add("Error en la columna prestacion");
            //throw new ServiceException("Error en la columna prestacion "+(i+1));
        }
        askProcedure.setPrestacion(prestacion);
        

        String codAfiliado = "";
        try {
            codAfiliado = dataFormatter.formatCellValue(row.getCell(7)).trim();
            if (codAfiliado.isEmpty()) {
         		obsObligatorios++;
         		obsClaves++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna COD AFILIADO: El campo esta vacío");
            	//obsDetalle.add("Error en la columna codAfiliado");
            } else if (!codAfiliado.matches("[0-9]{17}")) {
				obsObligatorios++;
				obsClaves++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna COD AFILIADO: Debe ser de 17 dígitos");
			}
        }catch (Exception e) {
     		obsObligatorios++;
     		obsClaves++;        	            	        	        	
        	obsDetalle.add("Error en la columna codAfiliado");
            //throw new ServiceException("Error en la columna codAfiliado "+(i+1));
        }
        askProcedure.setCodAfiliado(codAfiliado);
        

        String codPaciente = "";
        try {
            codPaciente = dataFormatter.formatCellValue(row.getCell(8)).trim();
            if (codPaciente.isEmpty()) {
         		obsObligatorios++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna PAC. - COD. OS: El campo esta vacío");
            	//obsDetalle.add("Error en la columna codPaciente");
            }             
        }catch (Exception e) {
     		obsObligatorios++;
        	obsDetalle.add("Error en la columna codPaciente");
            //throw new ServiceException("Error en la columna codPaciente "+(i+1));
        }
        askProcedure.setCodPaciente(codPaciente);

        
        String nombrePaciente = "";
        try {
            nombrePaciente = dataFormatter.formatCellValue(row.getCell(9)).trim();
            if (nombrePaciente.isEmpty()) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna PACIENTE: El campo esta vacío");
            	//obsDetalle.add("Error en la columna nombrePaciente");
            }              
        }catch (Exception e) {
        	obsDetalle.add("Error en la columna nombrePaciente");
            //throw new ServiceException("Error en la columna nombrePaciente "+(i+1));
        }
        askProcedure.setNombrePaciente(nombrePaciente);

        
        String clinica = "";
        try {
            clinica = dataFormatter.formatCellValue(row.getCell(10)).trim();
            if (clinica.isEmpty()) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna INSTITUCION: El campo esta vacío");
            	//obsDetalle.add("Error en la columna clinica");
            }            
        }catch (Exception e) {
        	obsDetalle.add("Error en la columna clinica");
            //throw new ServiceException("Error en la columna clinica "+(i+1));
        }
        askProcedure.setClinica(clinica);

        
        // fecha actual.
        Date date = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy");
        String fechaActual = formatDate.format(date);
        Date fechaActualDDMMYYYY = null;
        int fechaActualMM = 0;
        int fechaConsumoMM = 0;
        int fechaActualYYYY = 0;
        int fechaConsumoYYYY = 0;
        
        Date fechaConsumo = null;
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        
        //FORMATO MES
        SimpleDateFormat dateFormatMM = new SimpleDateFormat("MM");
        SimpleDateFormat dateFormatYYYY = new SimpleDateFormat("YYYY");
        
        try {
            String feco = dataFormatter.formatCellValue(row.getCell(11)).trim();

            if (!feco.isEmpty()) {
                
            	fechaActualDDMMYYYY = formatDate.parse(fechaActual);
            	
            	fechaConsumo = df.parse(feco);
                
                fechaActualMM = Integer.parseInt(dateFormatMM.format(fechaActualDDMMYYYY));
                fechaConsumoMM = Integer.parseInt(dateFormatMM.format(fechaConsumo));
                fechaActualYYYY = Integer.parseInt(dateFormatYYYY.format(fechaActualDDMMYYYY));
                fechaConsumoYYYY = Integer.parseInt(dateFormatYYYY.format(fechaConsumo));
                
                
                if ( fechaActualYYYY >= fechaConsumoYYYY &&
                		( (fechaActualMM == 1 && (fechaConsumoMM == 12 || fechaConsumoMM == 11 || fechaConsumoMM == 10) ) ||
                		  (fechaActualMM == 2 && (fechaConsumoMM == 1 || fechaConsumoMM == 12 || fechaConsumoMM == 11) ) ||
                		  (fechaActualMM == 3 && (fechaConsumoMM == 1 || fechaConsumoMM == 2 || fechaConsumoMM == 12) ) ||
                		  (fechaActualMM > fechaConsumoMM && fechaActualMM - 3 <= fechaConsumoMM)  ) ) {
                	
                } else {
                	
                	obsDetalle.add("Error en la fila " + row.getRowNum() +
                			" columna FECHA CONSUMO: El campo fecha de consumo no cumple con los " + ConstanteUtil.NUMERO_MESES_CARGA
                			+ " meses de periodo de carga aceptable");
                   	//obsDetalle.add("Error en la columna fechaConsumo");
             		obsClaves++;    
                }
                
            }
            else {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna FECHA - CONSUMO: El campo esta vacío");
               	//obsDetalle.add("Error en la columna fechaConsumo");
         		obsClaves++;        	            	        	        	
            }
        }catch (Exception e) {
     		obsClaves++;        	            	        	        	        	
        	obsDetalle.add("Error en la columna fechaConsumo");                	
            //throw new ServiceException("Error en la columna fechaConsumo "+(i+1));
        }
        askProcedure.setFechaConsumo(fechaConsumo);

        String descripGrpDiag = "";
        try {
            descripGrpDiag = dataFormatter.formatCellValue(row.getCell(12)).trim();
            if (descripGrpDiag.isEmpty()) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna GRUPO DE DIAGNOSTICO: El campo esta vacío");
            	//obsDetalle.add("Error en la columna descripGrpDiag");
            }              
        }catch (Exception e) {
        	obsDetalle.add("Error en la columna descripGrpDiag");
            //throw new ServiceException("Error en la columna descripGrpDiag "+(i+1));
        }
        askProcedure.setDescripGrpDiag(descripGrpDiag);

        
        String codDiagnostico = "";
        try {
            codDiagnostico = dataFormatter.formatCellValue(row.getCell(13)).trim();
            if (codDiagnostico.isEmpty()) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna CIE 10: El campo esta vacío");
				//obsDetalle.add("Error en la columna codDiagnostico");
            }              
        }catch (Exception e) {
        	obsDetalle.add("Error en la columna codDiagnostico");
            //throw new ServiceException("Error en la columna codDiagnostico "+(i+1));
        }
        askProcedure.setCodDiagnostico(codDiagnostico);

        
        
        String descripDiagnostico = "";
        try {
            descripDiagnostico = dataFormatter.formatCellValue(row.getCell(14)).trim();
            if (descripDiagnostico.isEmpty()) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna DIAGNOSTICO: El campo esta vacío");
            	//obsDetalle.add("Error en la columna descripDiagnostico");
            }            
        }catch (Exception e) {
        	obsDetalle.add("Error en la columna descripDiagnostico");                	
            //throw new ServiceException("Error en la columna descripDiagnostico "+(i+1));
        }
        askProcedure.setDescripDiagnostico(descripDiagnostico);


		Long lineaTratamiento = null;
            try {
                String lintra = dataFormatter.formatCellValue(row.getCell(15)).trim();
                if (!lintra.isEmpty()) {
                    lineaTratamiento = Long.parseLong(lintra);
                }
                else
                {
					obsDetalle.add("Error en la fila " + row.getRowNum() + " columna LINEA DE TRATAMIENTO: El campo esta vacío");
                	//obsDetalle.add("Error en la columna lineaTratamiento");
                }	
        }catch (Exception e) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna LINEA DE TRATAMIENTO: El valor del campo debe ser numérico");
				//obsDetalle.add("Error en la columna lineaTratamiento");
            //throw new ServiceException("Error en la columna lineaTratamiento "+(i+1));
        }
            askProcedure.setLineaTratamiento(lineaTratamiento);

        String medicoTratante = "";
        try {
            medicoTratante = dataFormatter.formatCellValue(row.getCell(16)).trim();
            if (medicoTratante.isEmpty()) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna MEDICO ATE.: El campo esta vacío");
            	//obsDetalle.add("Error en la columna medicoTratante");
            }
            
        }catch (Exception e) {
        	obsDetalle.add("Error en la columna medicoTratante");
            //throw new ServiceException("Error en la columna medicoTratante "+(i+1));
        }
        askProcedure.setMedicoTratante(medicoTratante);
        

        String plan = "";
        try {
            plan = dataFormatter.formatCellValue(row.getCell(17)).trim();
            if (plan.isEmpty()) {
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna ECON. - PLAN COP.: El campo esta vacío");
            	//obsDetalle.add("Error en la columna plan");
            }
            
        }catch (Exception e) {
        	obsDetalle.add("Error en la columna plan");
            //throw new ServiceException("Error en la columna plan "+(i+1));
        }
        askProcedure.setPlan(plan);
        
        
        Long cantidadConsumo = null;
        try {
            String cancon = dataFormatter.formatCellValue(row.getCell(18)).trim();
            if (!cancon.isEmpty()) {            	
                cantidadConsumo = Long.parseLong(cancon);
            }
            else {
         		obsObligatorios++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna CANT. PREST.: El campo esta vacío");
            	//obsDetalle.add("Error en la columna cantidadConsumo");
            }
            	
        }catch (Exception e) {
     		obsObligatorios++;
        	obsDetalle.add("Error en la columna cantidadConsumo");
            //throw new ServiceException("Error en la columna cantidadConsumo "+(i+1));
        }
        askProcedure.setCantidadConsumo(cantidadConsumo);
        

       Double montoConsumo = null;
        NumberFormat numf = NumberFormat.getInstance();
        try {
            String moncon = dataFormatter.formatCellValue(row.getCell(19)).trim();
            if (!moncon.isEmpty()) {
              montoConsumo = numf.parse(moncon).doubleValue();
              askProcedure.setMontoConsumo(montoConsumo);
            }
            else {
         		obsObligatorios++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + " columna INGRESOS SERV.: El campo esta vacío");
            	//obsDetalle.add("Error en la columna montoConsumo");
            	askProcedure.setMontoConsumo(0.0);
            }
            	
        }catch (Exception e) {
     		obsObligatorios++;
        	obsDetalle.add("Error en la columna montoConsumo");
        	askProcedure.setMontoConsumo(0.0);
            //throw new ServiceException("Error en la columna montoConsumo "+(i+1));
        }

		//Valida si el registro ya fue cargado
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String today = sdf.format(new Date());
		String consumDate = null;
		if (askProcedure.getFechaConsumo() != null)
			consumDate = sdf.format(askProcedure.getFechaConsumo());

		String mifecha = LocalDate
				.parse(today)
				.minusMonths(4)
				.toString();

		try {
			
			if (askProcedure.getFechaConsumo() != null) {
				Date today2 = sdf.parse(mifecha);
				
				Date consumDate2 = sdf.parse(consumDate);
				
				if (consumDate2.after(today2)) {
	
				} else if (consumDate2.equals(today2)) {
	
				} else {
					obsObligatorios++;
					obsClaves++;
					obsDetalle.add("Error en la fila " + row.getRowNum() + ": El consumo ya fue subido al sistema en cargas anteriores");
				}
			} else {
				obsObligatorios++;
				obsClaves++;
				obsDetalle.add("Error en la fila " + row.getRowNum() + ": La fecha de consumo está como campo vacío");
			}

		} catch (Exception e) {
			//e.printStackTrace();
			obsObligatorios++;
			obsClaves++;
        	obsDetalle.add("Error en la columna fecha de consumo : "+askProcedure.getFechaConsumo());
		}*/


		//Valida que exista una línea tratamiento
		ApiOutListItems = controlGastoDao.validarTratamiento(askProcedure);
		if ( ApiOutListItems.getTratamientoExiste() != 1){
			obsObligatorios++;
			obsClaves++;
			obsDetalle.add("Error en la fila " + row.getRowNum() + ": No existe una línea tratamiento para la combinación de paciente, medicamento MAC y código de diagnóstico CIE10");
		}
		//Valida que el CIE10 exista y este vigente en el oncosys
		DiagnosticoBean diag = obtenerDiagnosticoOncosys(askProcedure.getCodDiagnostico(), headers);
		if (diag == null){
			obsObligatorios++;
			obsClaves++;
			obsDetalle.add("Error en la fila " + row.getRowNum() + ": El CIE10 no existe y/o no se encuentra vigente en el ONCOSYS");
		}

		//Valida que el MAC exista y se encuentre vigente en Farmacia.
		ApiOutResponse macExiste = null;
		request.setCodigoLargo(askProcedure.getCodMac());
		macExiste =  macService.listFiltroMAC(request);
		List<MACBean> mac = (List<MACBean>) macExiste.getResponse();

		if (mac.size() == 0) {
			obsObligatorios++;
			obsClaves++;
			obsDetalle.add("Error en la fila " + row.getRowNum() + ": El código de medicamento MAC no existe y/o no se encuentra vigente en el módulo de Farmacia Compleja");
		}

		ListaPacienteRequest listaPacienteRequest = new ListaPacienteRequest();
		listaPacienteRequest.setCodafir(askProcedure.getCodAfiliado());
		listaPacienteRequest.setIni(1);
		listaPacienteRequest.setFin(10);
		listaPacienteRequest.setVigencia("1");
		ResponseOnc responseOnc = oncoPacienteService.obtenerListaPacientexDatos(listaPacienteRequest, headers);

		if (responseOnc.getDataList() == null){
			obsObligatorios++;
			obsClaves++;
			obsDetalle.add("Error en la fila " + row.getRowNum() + ": El código del afiliado no existe y no se encuentra vigente como paciente en el ONCOSYS");
		}

        ///////////////////////////////////////////////////////////////////////////////////////
        askProcedure.setObsDetalle(obsDetalle);
        
        askProcedure.setRegistroCargado(i);  
        askProcedure.setResponsableCarga(objCabecera.getResponsableCarga());
        askProcedure.setCodHistControlGasto(objCabecera.getCodigoControlGasto());
        askProcedure.setUsuarioCreacion(objCabecera.getUsuarioCreacion());
        askProcedure.setUsuarioModificacion(objCabecera.getUsuarioModificacion());
        askProcedure.setIndCampoObligatorio(obsObligatorios);        
        askProcedure.setIndCampoClave(obsClaves);
		return askProcedure;
	}

	private DiagnosticoBean obtenerDiagnosticoOncosys(String codDiag, HttpHeaders headers) {
		DiagnosticoBean diagnostico = null;

		try {
			DiagnosticoRequest diaRq = new DiagnosticoRequest();
			diaRq.setRegistroInicio(ConstanteUtil.registroIni);
			diaRq.setRegistroFin(ConstanteUtil.registroIni);
			diaRq.setTipoBusqueda(ConstanteUtil.tipoXCod);
			diaRq.setCodigoDiagnostico(codDiag);
			diaRq.setNombreDiagnostico("");

			diagnostico = (DiagnosticoBean) diagService.obtenerDiagnostico(headers, diaRq).getData();
		} catch (Exception e) {
			Log.error("obtenerDiagnosticoOncosys: " + e.getMessage());
			diagnostico = null;
		}

		return diagnostico;
	}


	private ApiOutResponse ValidarArchivo(Workbook workbook, Sheet sheet) {

		List<String> obsCabecera = new ArrayList<>();

		ExisteArchivoResponse existeArchivoResponse = controlGastoDao.validarExisteArchivo();
		if (existeArchivoResponse.getExiste()==1) {
			obsCabecera.add("Ya existe un archivo de consumo previamente cargado para este mes");
			return ConObservacionesEnCabecera(obsCabecera);
		}

        if (workbook.getNumberOfSheets()>1) {
			obsCabecera.add("El archivo excel debe tener solo 1 hoja");
        }

        Integer totalRegistros = sheet.getPhysicalNumberOfRows()-1;
        if (totalRegistros>10000) {
			obsCabecera.add("Su archivo tiene mas de 10000 registros");
        }
        
        if (obsCabecera.size()>0) {
        	return ConObservacionesEnCabecera(obsCabecera);
        }

		return null;
	}

	private boolean ExisteDiagnosticoEnOncosys(String codCIE10) {
		// TODO Auto-generated method stub
				
		boolean respuesta=false;
		
		MonitoreoResponse request =new MonitoreoResponse();
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);		
		
		
		request.setCodDiagnostico(codCIE10);
		ApiOutResponse reponse =  monitoreo.consDatosGrpDiagnostico(request, headers); 
		
		List<MonitoreoResponse> lista = (List<MonitoreoResponse>) reponse.getResponse();		  

		
		respuesta = (lista.size()==0)?false:true;			
				
				
		return respuesta;
	}



	private boolean ExisteMacEnOncosys(String codMac) {
					
			boolean respuesta=false;
			
			MACBean request =new MACBean();
			
			String idTransaccion = GenericUtil.getUniqueID();
			String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type", "application/json");
			headers.add("idTransaccion", idTransaccion);
			headers.add("fechaTransaccion", fechaTransaccion);		
			
			request.setCodigoLargo(codMac);
			ApiOutResponse reponse =  macService.listFiltroMAC(request);
			
			List<MACBean> lista = (List<MACBean>) reponse.getResponse();		  
			respuesta = (lista.size()==0)?false:true;		
					
			return respuesta;
		}
	
	
	private ApiOutResponse ConObservacionesEnCabecera(List<String> obsCabecera) {
		// TODO Auto-generated method stub
		
		ApiOutResponse askImportFile = new ApiOutResponse();
		askImportFile.setCodResultado(1);

		int i=1;
		List<Object> listResponse = new ArrayList<>();
		for (String observacion : obsCabecera) {		
				ApiOutResponse askCabeceraGasto = new ApiOutResponse();
				LOGGER.info(observacion);
				askCabeceraGasto.setCodResultado(i); i++;
				askCabeceraGasto.setMsgResultado(observacion);
				listResponse.add(askCabeceraGasto);	//Resultado Cabecera				
        }		
		askImportFile.setListResponse(listResponse);    				
		return askImportFile;
	}

	

	private ApiOutResponse ConObservacionesEnDetalle(List<String> obsDetalle) {
		// TODO Auto-generated method stub
		
		
		ApiOutResponse askImportFileDet = new ApiOutResponse();
		askImportFileDet.setCodResultado(0);

		int i=1;
		List<Object> listResponse = new ArrayList<>();
		for (String observacion : obsDetalle) {		
				ApiOutResponse askDetalleGasto = new ApiOutResponse();
				LOGGER.info(observacion);
				askDetalleGasto.setCodResultado(i); i++;
				askDetalleGasto.setMsgResultado(observacion);
				listResponse.add(askDetalleGasto);	//Resultado Detalle				
        }		
		askImportFileDet.setListResponse(listResponse);    				
		return askImportFileDet;
	}	

	private GastoConsumoBean ActualizaEstadoCarga(int codigoHistoricoControlGasto, int nroItemsConError) {
		GastoConsumoBean objActualiza = new GastoConsumoBean();
		try {
			objActualiza.setCodHistControlGasto(codigoHistoricoControlGasto);

			objActualiza.setEstadoCarga(254);  //Cargado

			//nroItemsConError
			objActualiza.setRegistroError(nroItemsConError);			
			ApiOutResponse  respuesta=ActualizaRegistroGastosCab(objActualiza);

		} catch (Exception e) {
			LOGGER.error("ActualizaEstadoCarga" + " :" + e.getMessage());
		}				
		return objActualiza;
		
	}

	private ApiOutResponse InsertaRegistroGastosCab(HistorialCargaGastosBean objRegistro) {
		return controlGastoDao.InsertaRegistroGastosCab(objRegistro);
	}    
    

	private ApiOutResponse ActualizaRegistroGastosCab(GastoConsumoBean objRegistro) {
		return controlGastoDao.ActualizaRegistroGastosCab(objRegistro);
	}   	
	
	private ApiOutResponse InsertaRegistroGastosDet(GastoConsumoBean objRegistro) {
		return controlGastoDao.InsertaRegistroGastosDet(objRegistro);
	}  	
	
	
	
	public ResponseEntity<WsResponse> descargarArchivo(ArchivoFTPBean obj) {
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		obj.setUsrApp(FTPprop.getUserApp());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;
		
		
		try {
			resultado = service.descargarArchivo(obj, headers);

			if (resultado != null) {
				if (resultado.getCodResultado() == 0) {
					LOGGER.info("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][BODY][Registro realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData((ArchivoFTPBean) resultado.getResponse());
				} else {
					LOGGER.error("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setData(null);
				}
			} else {
				LOGGER.error("[SERVICIO:  DESCARGAR ARCHIVO][REQUEST][FIN][Error en el servicio de descarga]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error en el servicio de descarga de archivos.");
				response.setData(null);
			}

			response.setAudiResponse(audiResponse);
			return new ResponseEntity<WsResponse>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<WsResponse> subirArchivo(MultipartFile file,
			String nomArchivo, String ruta) {
		
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();

		headers.add("Content-Type", "application/json");
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);
		WsResponse response = new WsResponse();
		AudiResponse audiResponse = new AudiResponse(idTransaccion, fechaTransaccion);
		ApiOutResponse resultado = null;
		try {
			resultado = service.enviarArchivoFTP(FTPprop.getUserApp(), nomArchivo, file, FTPprop.getRutaFTP() + ruta, headers);

			if (resultado != null) {
				LOGGER.info("resultado...  ", resultado.toString());
				if (resultado.getCodResultado() == 0) {
					LOGGER.info("resultado.getCodResultado  ", resultado.getCodResultado().toString());
					LOGGER.info("[SERVICIO:  SUBIR ARCHIVO][REQUEST][FIN][Registro realizado correctamente]");
					audiResponse.setCodigoRespuesta("0");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setAudiResponse(audiResponse);
					response.setData((ArchivoFTPBean) resultado.getResponse());
					return new ResponseEntity<WsResponse>(response, HttpStatus.OK);
				} else {
					audiResponse.setCodigoRespuesta(resultado.getCodResultado() + "");
					LOGGER.error("[SERVICIO:  SUBIR ARCHIVO][REQUEST][FIN][" + resultado.getMsgResultado() + "]");
					LOGGER.error("[SERVICIO:  SUBIR ARCHIVO][REQUEST][FIN][Se encontraron errores]");
					audiResponse.setMensajeRespuesta(resultado.getMsgResultado());
					response.setAudiResponse(audiResponse);
					response.setData(null);
					return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
				}
			} else {				
				LOGGER.error("[SERVICIO:  SUBIR ARCHIVO][REQUEST][BODY][Se encontraron errores]");
				audiResponse.setCodigoRespuesta("-3");
				audiResponse.setMensajeRespuesta("Error al subir el archivo FTP.");
				response.setAudiResponse(audiResponse);
				response.setData(null);
				return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			LOGGER.error("[SERVICIO:  SUBIR ARCHIVO][REQUEST][FIN][Ocurrio un excepcion]");
			audiResponse.setCodigoRespuesta("-1");
			audiResponse.setMensajeRespuesta(e.getMessage());
			response.setAudiResponse(audiResponse);
			response.setData(null);
			return new ResponseEntity<WsResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public void actualizarEstado(){
		controlGastoDao.actualizarEstado();
	}
	
	
}