package pvt.auna.fcompleja.service.impl;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import pvt.auna.fcompleja.config.ConfigFTPProp;
import pvt.auna.fcompleja.dao.ReporteDao;
import pvt.auna.fcompleja.dao.UsuarioDao;
import pvt.auna.fcompleja.model.api.ActaMac;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ApiResponse;
import pvt.auna.fcompleja.model.api.ArchivoFTPBean;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.CasosEvaluar;
import pvt.auna.fcompleja.model.api.ExcelDownloadResponse;
import pvt.auna.fcompleja.model.api.ListaIndicador;
import pvt.auna.fcompleja.model.api.Participantes;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.ListaActasMAC;
import pvt.auna.fcompleja.model.api.request.ParticipanteBeanRequest;
import pvt.auna.fcompleja.model.api.request.ReporteIndicadoresRequest;
import pvt.auna.fcompleja.model.api.request.RequestImputReportEvaluacion;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.ListCasosEvaluar;
import pvt.auna.fcompleja.model.api.request.evaluacion.ParticipanteRequest;
import pvt.auna.fcompleja.model.api.response.AnalisiConclusionResponse;
import pvt.auna.fcompleja.model.api.response.CriterioExclusion;
import pvt.auna.fcompleja.model.api.response.DetalleEvaluacionPdfResponse;
import pvt.auna.fcompleja.model.api.response.IndicadorConsumoPorGrupoMacResponse;
import pvt.auna.fcompleja.model.api.response.LineaTratPdfResponse;
import pvt.auna.fcompleja.model.api.response.ReporteIndicadorResponse;
import pvt.auna.fcompleja.model.api.response.evaluacion.LinTrataReportDetalleBean;
import pvt.auna.fcompleja.model.api.response.evaluacion.LinTratamientoReportResponse;
import pvt.auna.fcompleja.model.bean.ConclusionBean;
import pvt.auna.fcompleja.model.bean.CondicionBasalBean;
import pvt.auna.fcompleja.model.bean.DetalleReportBean;
import pvt.auna.fcompleja.model.bean.ReporteListaCasosRequestBean;
import pvt.auna.fcompleja.service.GenericoService;
import pvt.auna.fcompleja.service.ReporteService;
import pvt.auna.fcompleja.util.AunaUtil;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

@Service
public class ReporteServiceImpl implements ReporteService {

	private static final Logger log = LoggerFactory.getLogger(ReporteServiceImpl.class);
	
	@Autowired
	@SuppressWarnings("rawtypes")
	AunaUtil util;

	@Autowired
	private ReporteDao reporteDao;

	@Qualifier("dataSource")
	@Autowired
	private DataSource dataSource;

	@Autowired
	ConfigFTPProp ftpProp;
	
	@Autowired
	private UsuarioDao lstUsuarioDao;

	@Autowired
	GenericoService genericoService;

	@SuppressWarnings("unchecked")
	@Override
	public byte[] informeSolEvaReporte(RequestImputReportEvaluacion request, byte[] imagenFirma) throws Exception {
		log.info(" \t - INICIO - [" + "obtenerReporteEvaluacionAutorizador" + "] ");

		JasperReport jasperReport;
		JasperPrint jasperPrint;
		ByteArrayOutputStream reporte = new ByteArrayOutputStream();
		String reporteEva = ftpProp.getReporteEva();
		String reportePlanilla = ftpProp.getReportePlanilla();

		try {

			String path = "";

			if (request.getCodSolicitudEvaluacion().equals(ConstanteUtil.REPORTE_PDF)) {
				path = reportePlanilla;
			} else {
				path = reporteEva;
			}

			Image in = ImageIO.read(new ByteArrayInputStream(imagenFirma));

			Map parameters = new HashMap();

			parameters.put("CODIGO_EVALUACION", request.getCodSolicitudEvaluacion());
			parameters.put("COD_DIAGNOSTICO", request.getCodDiagnostico().toString());
			parameters.put("DESCRIPCION_DIAGNOSTICO", request.getDescripcionDiagnostico());
			parameters.put("SEXO", request.getSexo());
			parameters.put("NOMBRE_APELLIDO", request.getNombrePaciente());
			parameters.put("NOMBRE_MEDICO", request.getNombreMedicoAuditor());
			parameters.put("IMAGEN_FIRMA", in);
			parameters.put("CODIGO_AFILIADO", request.getCodAfiliado());
			parameters.put("CODIGO_MAC", request.getCodMac());
			parameters.put("CODIGO_GRUPO_DIAGNOSTICO", request.getCodGrupopDiagnostico());

			log.info("\t\t * parameters : [" + parameters.toString() + "]");
			log.info("\t\t * codigo evaluacion : [" + request.getCodSolicitudEvaluacion() + "]");
			log.info("\t\t * path : [" + path + "]");
			File file = new File(path);
			log.info("\t\t * file : [" + path + file.exists() + "]");

			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);
			// jasperReport =
			// JasperCompileManager.compileReport("src\\main\\webapp\\reporteAutorizador\\ReportEvaluacion.jrxml");

			if (jasperReport != null) {
				log.info("\t\t * Entro validacion");
			}

			log.info("\t\t * Antes el jasperPrint");

			JRBeanCollectionDataSource dbDatSourde = new JRBeanCollectionDataSource(null);

			Connection con = null;
			con = dataSource.getConnection();

			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);

			log.info("\t\t * Despues el jasperPrint");
			JRPdfExporter jrPdfExporter = new JRPdfExporter();
			jrPdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reporte));
			jrPdfExporter.exportReport();

		} catch (Exception e) {
			// log.error(e);
			throw e;
		}
		log.info(" \t - FIN - [" + "obtenerReporteEvaluacionAutorizador" + "] ");
		return reporte.toByteArray();

	}

	@Override
	public byte[] imprimirReporteListaCasos(ReporteListaCasosRequestBean objA_request) throws Exception {
		log.info(" \t - INICIO - [" + "imprimirReporteListaCasos" + "] ");

		JasperReport jasperReport;
		JasperPrint jasperPrint;
		ByteArrayOutputStream reporte = new ByteArrayOutputStream();

		try {

			String titulo = "";
			String path = "";

			if (objA_request.getCodSolEva().equals(ConstanteUtil.REPORTE_PDF)) {
				// path = "/app/hhmm/jasper/planillas.jasper";
				path = "D:/Alex/Proyecto AUNA/PDF_IMPORTANTE/REPOR_CMAC/ReportCmac.jasper";
			}

			Map parameters = new HashMap();
			parameters.put("ptitulo", "Liquidaciones : " + titulo);
			parameters.put("pusuario", objA_request.getUsuario());

			File file = new File(path);
			log.info("\t\t * file : [" + path + file.exists() + "]");

			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);
			if (jasperReport != null) {
				log.info("\t\t * Entro validacion ");
			}

			log.info("\t\t * Antes el jasperPrint");
			jasperPrint = JasperFillManager.fillReport(jasperReport, null,
					new JRBeanCollectionDataSource(objA_request.getReporteListCasos()));
			log.info("\t\t * Despues el jasperPrint");

			JRPdfExporter jrPdfExporter = new JRPdfExporter();
			jrPdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reporte));
			jrPdfExporter.exportReport();

		} catch (Exception e) {
			// log.error(e);
			throw e;
		}
		log.info(" \t - FIN - [" + "obtenerReportePlanilla" + "] ");
		return reporte.toByteArray();
	}

	@Override
	public byte[] informeListaCasosReporte(ListCasosEvaluar objA_request) throws Exception {
		log.info(" \t - INICIO - [" + "informeListaCasosReporte" + "] ");

		JasperReport jasperReport;
		JasperPrint jasperPrint;
		List<CasosEvaluar> listaCasosEvaluar = objA_request.getListaCasosEvaluar();
		String rutaReporteJasper = ftpProp.getRutaReportes();

		ByteArrayOutputStream reporte = new ByteArrayOutputStream();

		final Map<String, Object> parameters = new HashMap<String, Object>();

		List<CasosEvaluar> LISTA_SOLICITUD_EVALUACION = new ArrayList<>();
		LISTA_SOLICITUD_EVALUACION.clear();

		for (int i = 0; i < listaCasosEvaluar.size(); i++) {
			listaCasosEvaluar.get(i).setFechaMac(objA_request.getFecha());
			LISTA_SOLICITUD_EVALUACION.add(listaCasosEvaluar.get(i));

		}

		try {

			String path = "";
			path = rutaReporteJasper + "ReportListaCasos.jasper";

			File file = new File(path);

			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);
			if (jasperReport != null) {

			}

			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(LISTA_SOLICITUD_EVALUACION);
			parameters.put("LISTA_SOLICITUD_EVALUACION", ds);
			parameters.put("FECHA_MAC", objA_request.getFecha());

			Connection con = null;
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			log.info("\t\t * Despues el jasperPrint");
			JRPdfExporter jrPdfExporter = new JRPdfExporter();
			jrPdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reporte));
			jrPdfExporter.exportReport();

			log.info(" \t - FIN - [" + "informeListaCasosReporte" + "] ");
			return reporte.toByteArray();

		} catch (Exception e) {
			log.error("Error", e);
			return null;
		}

	}

	@Override
	public byte[] informeListaActasMACReporte(ListaActasMAC objA_request) throws Exception {
		log.info(" \t - INICIO - [" + "Imprimir Lista Actas CMAC " + "] ");

		JasperReport jasperReport;
		JasperPrint jasperPrint;
		List<ActaMac> LISTA_CASOS_EVALUAR = objA_request.getListaActasMAC();

		List<Participantes> LISTA_PARTICIPANTES = objA_request.getListaParticipantes();

		ByteArrayOutputStream reporte = new ByteArrayOutputStream();

		final Map<String, Object> parameters = new HashMap<String, Object>();

		// LISTA_SOLICITUD_EVALUACION.add(new CasosEvaluar("001","Omar Zegarra", "MUY
		// BUEN PACIENTE", "MED003", "MELATONINA", "12-13-2019"));
		// LISTA_SOLICITUD_EVALUACION.add(new CasosEvaluar("002","Omar Balmaceda", "MUY
		// BUEN PACIENTE", "MED003", "MELATONINA", "12-13-2019"));

		try {

			String path = ftpProp.getRutaReportes() + "ImprimirActaMac.jasper";

			File file = new File(path);

			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);
			if (jasperReport != null) {

			}

			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(LISTA_CASOS_EVALUAR);
			JRBeanCollectionDataSource ds2 = new JRBeanCollectionDataSource(LISTA_PARTICIPANTES);
			parameters.put("LISTA_CASOS_EVALUAR", ds);
			parameters.put("LISTA_PARTICIPANTES", ds2);
			parameters.put("FECHA", objA_request.getFecha());
			parameters.put("HORA", objA_request.getHora());
			parameters.put("CODIGO_DE_ACTA", objA_request.getCodigoActa());

			Connection con = null;
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			log.info("\t\t * Imprimir Lista Actas CMAC | Despues el jasperPrint");
			JRPdfExporter jrPdfExporter = new JRPdfExporter();
			jrPdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reporte));
			jrPdfExporter.exportReport();

		} catch (Exception e) {
			// log.error(e);
			throw e;
		}
		log.info(" \t - FIN - [" + "Imprimir Lista Actas CMAC" + "] ");
		return reporte.toByteArray();
	}

	@Override
	public ApiOutResponse generarIndicador(ReporteIndicadoresRequest objA_request) throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarIndicador" + "] ");
		return reporteDao.generarIndicadores(objA_request);
	}

	@Override
	public ApiOutResponse generarIndicadoresExcel(ReporteIndicadoresRequest reporteRequest, HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarIndicadoresExcel" + "] ");
		return reporteDao.generarIndicadoresExcel(reporteRequest, request);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ApiOutResponse informeSolEvaReporte(RequestImputReportEvaluacion request) throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "obtenerReporteEvaluacionAutorizador" + "] ");

		ApiOutResponse serviceResponse = new ApiOutResponse();
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		ArchivoFTPBean archivoFTPBean = new ArchivoFTPBean();

		HttpHeaders headers = new HttpHeaders();
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		headers.add("Content-Type", "application/json");

		String usrApp = ftpProp.getUserApp();

		byte[] fileFirma = null;
		
		/* INICIO codFirma */
		
		ParticipanteBeanRequest participanteRequest = new ParticipanteBeanRequest();
		participanteRequest.setCodUsuario(request.getCodUsuario());
		
		ApiOutResponse responseParticipante = lstUsuarioDao.listarParticipantes(participanteRequest);
		
		@SuppressWarnings("unchecked")
		ArrayList<ParticipanteBeanRequest> lista = (ArrayList<ParticipanteBeanRequest>) responseParticipante
				.getResponse();
		
		for (ParticipanteBeanRequest par : lista) {
			request.setCodArchivo(par.getCodigoArchivoFirma());
		}
		
		/* FIN codFirma */

		/* Buscar Archivos de Firma */

		if (request.getCodArchivo() != null && request.getCodArchivo() != 0) {
			ArchivoFTPBean archivoBean = new ArchivoFTPBean();
			archivoBean.setCodArchivo(request.getCodArchivo());
			archivoBean.setUsrApp(usrApp);
			ApiOutResponse firmaOut = genericoService.descargarArchivo(archivoBean, headers);
			fileFirma = ((ArchivoFTPBean) firmaOut.getResponse()).getArchivo();
		} else {

			File dataFile = ResourceUtils.getFile(ftpProp.getRutaImagenes() + "sin-firma.jpg");
			fileFirma = Files.readAllBytes(dataFile.toPath());
		}

		/*-------------------------------------------------*/

		JasperReport jasperReport;
		JasperPrint jasperPrint;
		ByteArrayOutputStream reporte = new ByteArrayOutputStream();
		String reporteReportes = ftpProp.getRutaReportes();
		String reportePlanilla = ftpProp.getReportePlanilla();

		try {

			String path = reporteReportes + "ReportEvaluacion.jasper";

			Image in = ImageIO.read(new ByteArrayInputStream(fileFirma));

			@SuppressWarnings("rawtypes")
			Map parameters = new HashMap();

			parameters.put("CODIGO_EVALUACION", request.getCodSolicitudEvaluacion());
			parameters.put("COD_DIAGNOSTICO", request.getCodDiagnostico().toString());
			parameters.put("DESCRIPCION_DIAGNOSTICO", request.getDescripcionDiagnostico());
			parameters.put("SEXO", request.getSexo());
			parameters.put("NOMBRE_APELLIDO", request.getNombrePaciente());
			parameters.put("NOMBRE_MEDICO", request.getNombreMedicoAuditor());
			parameters.put("IMAGEN_FIRMA", in);
			parameters.put("CODIGO_AFILIADO", request.getCodAfiliado());
			parameters.put("CODIGO_MAC", request.getCodMac());
			parameters.put("CODIGO_GRUPO_DIAGNOSTICO", request.getCodGrupopDiagnostico());
			parameters.put("RUTA_SUB_REPORTE", reporteReportes);

			log.info("\t\t * parameters : [" + parameters.toString() + "]");
			log.info("\t\t * codigo evaluacion : [" + request.getCodSolicitudEvaluacion() + "]");
			log.info("\t\t * path : [" + path + "]");
			File file = new File(path);
			log.info("\t\t * file : [" + path + file.exists() + "]");

			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);

			if (jasperReport != null) {
				log.info("\t\t * Entro validacion");
			}

			log.info("\t\t * Antes el jasperPrint");

			JRBeanCollectionDataSource dbDatSourde = new JRBeanCollectionDataSource(null);

			Connection con = null;
			con = dataSource.getConnection();

			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);

			log.info("\t\t * Despues el jasperPrint");
			JRPdfExporter jrPdfExporter = new JRPdfExporter();
			jrPdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reporte));
			jrPdfExporter.exportReport();

			/* Construir Response */

			archivoFTPBean.setArchivo(reporte.toByteArray());
			archivoFTPBean.setNomArchivo("InformeAutorizador_" + request.getCodSolicitudEvaluacion().toString());
			archivoFTPBean.setExtension("pdf");

			serviceResponse.setCodResultado(0);
			serviceResponse.setMsgResultado("Informe Autorizador Generado ");
			serviceResponse.setResponse(archivoFTPBean);

		} catch (Exception e) {
			// log.error(e);
			serviceResponse.setCodResultado(-1);
			serviceResponse.setMsgResultado("ERROR Generando : Informe Autorizador [" + e.getMessage() + "]");

			throw e;
		}
		log.info(" \t - FIN - [" + "obtenerReporteEvaluacionAutorizador" + "] ");
		return serviceResponse;
	}

	@Override
	public ApiOutResponse generarReporteIndicadorMedicamentoMAC(ReporteIndicadoresRequest requestRequest) throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarReporteIndicadorMedicamentoMAC" + "] ");
		
		ApiOutResponse serviceResponse = new ApiOutResponse();
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());
		ByteArrayOutputStream reporte = new ByteArrayOutputStream();
		ArchivoFTPBean archivoFTPBean =  new ArchivoFTPBean ();

		JasperReport jasperReport;
		JasperPrint jasperPrint;
		
		try {

			String path = ftpProp.getRutaReportes() + "ReporteConsumoMAC2.jasper";

			

			
			@SuppressWarnings("rawtypes")
			Map parameters = new HashMap();

			parameters.put("P_MES", requestRequest.getMes());
			parameters.put("P_ANO", requestRequest.getAno());
			

			log.info("\t\t * parameters : [" + parameters.toString() + "]");
			log.info("\t\t * path : [" + path + "]");
			File file = new File(path);
			log.info("\t\t * file : [" + path + file.exists() + "]");

			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);

			if (jasperReport != null) {
				log.info("\t\t * Entro validacion");
			}

			log.info("\t\t * Antes el jasperPrint");

			
			Connection con = null;
			con = dataSource.getConnection();

			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);

			log.info("\t\t * Despues el jasperPrint");
			JRPdfExporter jrPdfExporter = new JRPdfExporter();
			jrPdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reporte));
			jrPdfExporter.exportReport();

			/* Construir Response */

			archivoFTPBean.setArchivo(reporte.toByteArray());
			archivoFTPBean.setNomArchivo("InformePorMedicamentoMAC_" + requestRequest.getMes().toString() + "_"+ requestRequest.getAno().toString());
			archivoFTPBean.setExtension("pdf");

			serviceResponse.setCodResultado(0);
			serviceResponse.setMsgResultado("Informe Por Medicamento MAC Generado ");
			serviceResponse.setResponse(archivoFTPBean);
			
			/* Test Para Exportar a Excel */
			 
			 JRXlsExporter xlsExporter = new JRXlsExporter();
			 
			 String pathFile = ftpProp.getRutaTemp() + "InformePorMedicamentoMAC_" + requestRequest.getMes().toString() + "_"+ requestRequest.getAno().toString() + ".xls";
			 File fileExport = new File (pathFile);
			 OutputStream outputStream = null;
			 ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
			 SimpleOutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(outputStream);
			 
			 xlsExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		     xlsExporter.setExporterOutput(exporterOutput);
		     
		    
		     SimpleXlsReportConfiguration xlsReportConfiguration = new SimpleXlsReportConfiguration();
//		     xlsReportConfiguration.setOnePagePerSheet(false);
		     xlsReportConfiguration.setRemoveEmptySpaceBetweenRows(true);
//		     xlsReportConfiguration.setDetectCellType(false);
		     xlsReportConfiguration.setWhitePageBackground(false);
		     xlsExporter.setConfiguration(xlsReportConfiguration);
		     
		     
		     
		     
		     xlsExporter.exportReport();
			
			 log.info("\t\t * Exportado a Excel : "  + pathFile);
			
			

		} catch (Exception e) {
			// log.error(e);
			serviceResponse.setCodResultado(-1);
			serviceResponse.setMsgResultado("ERROR Generando : Informe Por Medicamento MAC [" + e.getMessage() + "]");

			throw e;
		}
		log.info(" \t - FIN - [" + "generarReporteIndicadorMedicamentoMAC" + "] ");
		return serviceResponse;
	}

	@Override
	public ApiOutResponse generarReporteIndicadorMedicamentoGrupoMAC(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarReporteIndicadorMedicamentoGrupoDiagnosticoMAC" + "] ");
		
		ApiOutResponse serviceResponse = new ApiOutResponse();
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());
		ByteArrayOutputStream reporte = new ByteArrayOutputStream();
		ArchivoFTPBean archivoFTPBean =  new ArchivoFTPBean ();

		JasperReport jasperReport;
		JasperPrint jasperPrint;
		
		try {

			String path = ftpProp.getRutaReportes() +File.separator  + "ReporteConsumoGrupoDiagMAC.jasper";

			

			
			@SuppressWarnings("rawtypes")
			Map parameters = new HashMap();

			parameters.put("P_MES", requestRequest.getMes());
			parameters.put("P_ANO", requestRequest.getAno());
			

			log.info("\t\t * parameters : [" + parameters.toString() + "]");
			log.info("\t\t * path : [" + path + "]");
			File file = new File(path);
			log.info("\t\t * file : [" + path + file.exists() + "]");

			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);

			if (jasperReport != null) {
				log.info("\t\t * Entro validacion");
			}

			log.info("\t\t * Antes el jasperPrint");

			
			Connection con = null;
			con = dataSource.getConnection();

			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);

			log.info("\t\t * Despues el jasperPrint");
			JRPdfExporter jrPdfExporter = new JRPdfExporter();
			jrPdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reporte));
			jrPdfExporter.exportReport();

			/* Construir Response */

			archivoFTPBean.setArchivo(reporte.toByteArray());
			archivoFTPBean.setNomArchivo("InformePorMedicamentoGrupoMAC_" + requestRequest.getMes().toString() + "_"+ requestRequest.getAno().toString());
			archivoFTPBean.setExtension("pdf");

			serviceResponse.setCodResultado(0);
			serviceResponse.setMsgResultado("Informe Por Medicamento Grupo Diagnostico - MAC Generado ");
			serviceResponse.setResponse(archivoFTPBean);
			
			/* Test Para Exportar a Excel 
			 JRExporter exporter = null;
			 exporter = new JRXlsExporter();
			 exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			 exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			 exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			 //we set the one page per sheet parameter here
			 exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			 exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			 exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, ftpProp.getRutaTemp() + "InformePorMedicamentoGrupoMAC_" + requestRequest.getMes().toString() + "_"+ requestRequest.getAno().toString()+".xls");
			 exporter.exportReport();
			
			 log.info("\t\t * Exportado a Excel : " +  ftpProp.getRutaTemp() + "InformePorMedicamentoGrupoMAC_" + requestRequest.getMes().toString() + "_"+ requestRequest.getAno().toString() + ".xls");
			*/

		} catch (Exception e) {
			// log.error(e);
			serviceResponse.setCodResultado(-1);
			serviceResponse.setMsgResultado("ERROR Generando : Informe Por Medicamento Grupo Diagnostico - MAC [" + e.getMessage() + "]");

			throw e;
		}
		log.info(" \t - FIN - [" + "generarReporteIndicadorMedicamentoGrupoDiagnosticoMAC" + "] ");
		return serviceResponse;
	}

	@Override
	public ApiOutResponse generarReporteIndicadorMedicamentoGrupoMACLinea(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarReporteIndicadorMedicamentoGrupoDiagnosticoMACLinea" + "] ");
		
		ApiOutResponse serviceResponse = new ApiOutResponse();
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());
		ByteArrayOutputStream reporte = new ByteArrayOutputStream();
		ArchivoFTPBean archivoFTPBean =  new ArchivoFTPBean ();

		JasperReport jasperReport;
		JasperPrint jasperPrint;
		
		try {

			String path = ftpProp.getRutaReportes() +File.separator  + "ReporteConsumoGrupoDiagMACLineaTratamiento.jasper";

			

			
			@SuppressWarnings("rawtypes")
			Map parameters = new HashMap();

			parameters.put("P_MES", requestRequest.getMes());
			parameters.put("P_ANO", requestRequest.getAno());
			

			log.info("\t\t * parameters : [" + parameters.toString() + "]");
			log.info("\t\t * path : [" + path + "]");
			File file = new File(path);
			log.info("\t\t * file : [" + path + file.exists() + "]");

			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);

			if (jasperReport != null) {
				log.info("\t\t * Entro validacion");
			}

			log.info("\t\t * Antes el jasperPrint");

			
			Connection con = null;
			con = dataSource.getConnection();

			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);

			log.info("\t\t * Despues el jasperPrint");
			JRPdfExporter jrPdfExporter = new JRPdfExporter();
			jrPdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reporte));
			jrPdfExporter.exportReport();

			/* Construir Response */

			archivoFTPBean.setArchivo(reporte.toByteArray());
			archivoFTPBean.setNomArchivo("InformePorMedicamentoGrupoMACLinea_" + requestRequest.getMes().toString() + "_"+ requestRequest.getAno().toString());
			archivoFTPBean.setExtension("pdf");

			serviceResponse.setCodResultado(0);
			serviceResponse.setMsgResultado("Informe Por Medicamento Grupo Diagnostico - MAC - Linea Generado ");
			serviceResponse.setResponse(archivoFTPBean);

		} catch (Exception e) {
			// log.error(e);
			serviceResponse.setCodResultado(-1);
			serviceResponse.setMsgResultado("ERROR Generando : Informe Por Medicamento Grupo Diagnostico - MAC - Linea [" + e.getMessage() + "]");

			throw e;
		}
		log.info(" \t - FIN - [" + "generarReporteIndicadorMedicamentoGrupoDiagnosticoMACLinea" + "] ");
		return serviceResponse;
	}
	
	@Override
	public  byte[] generarIndicadoresConsumoPorMacExcel(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		// log.info(" \t - INICIO - [" + "generarIndicadoresConsumoPorMacExcel" + "] ");
		// return reporteDao.generarIndicadoresConsumoPorMacExcel(requestRequest);
		
		JasperReport jasperReport;
		JasperPrint jasperPrint = null;
		ByteArrayOutputStream reporte = new ByteArrayOutputStream();
		
		// ApiOutResponse serviceResponse = new ApiOutResponse();
		
		try {
			String path = "";
			
			switch (requestRequest.getTipo()) {
				case 1:
					path = ftpProp.getRutaReportes() + File.separator + "/ReporteConsumoMAC.jasper";
					break;
				case 2:
					path = ftpProp.getRutaReportes() + File.separator + "/ReporteConsumoGrupoDiagMAC.jasper";
					
//					List<IndicadorConsumoPorGrupoMacResponse> ListIndicadoresConsumoPorGrupoMacBean = new ArrayList<>();
//					ListIndicadoresConsumoPorGrupoMacBean = reporteDao.generarIndicadoresConsumoPorGrupoMacExcel(requestRequest);
					
					break;
				case 3:
					path = ftpProp.getRutaReportes() + File.separator + "/ReporteConsumoGrupoDiagMACLineaTratamiento.jasper";
					break;
				case 4:
					path = ftpProp.getRutaReportes() + File.separator + "/ReporteConsumoGrupoDiagMACLineaTratamientoTiempo.jasper";
					break;
				default:
					break;
			}
			
			Map parameters = new HashMap();
			parameters.put("P_MES", requestRequest.getMes());
			parameters.put("P_ANO", requestRequest.getAno());
			parameters.put("P_MESNOMBRE",  util.mesLetra(requestRequest.getMes()));
			
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			
			Connection con = null;
			con = dataSource.getConnection();
			File file = new File(path);
			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);
			
//			File file = null;
//			
//			switch (requestRequest.getTipo()) {
//				case 1:
//					path = ftpProp.getRutaReportes() + File.separator + "/ReporteConsumoMAC.jasper";
//					con = dataSource.getConnection();
//					
//					file = new File(path);
//					jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);
//					jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);
//					break;
//				case 2:
//					path = ftpProp.getRutaReportes() + File.separator + "/ReporteConsumoGrupoDiagMAC.jasper";
//					
//					List<IndicadorConsumoPorGrupoMacResponse> ListIndicadoresConsumoPorGrupoMacBean = new ArrayList<>();
//					ListIndicadoresConsumoPorGrupoMacBean = reporteDao.generarIndicadoresConsumoPorGrupoMacExcel(requestRequest);
//					
//					file = new File(path);
//					jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);
//					jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,  new JRBeanCollectionDataSource(ListIndicadoresConsumoPorGrupoMacBean));
//					break;
//				case 3:
//					path = ftpProp.getRutaReportes() + File.separator + "/ReporteConsumoGrupoDiagMACLineaTratamiento.jasper";
//					break;
//				case 4:
//					path = ftpProp.getRutaReportes() + File.separator + "/ReporteConsumoGrupoDiagMACLineaTratamientoTiempo.jasper";
//					break;
//				default:
//					break;
//			}
			
			
			JRXlsxExporter jrXlsxExporter = new JRXlsxExporter();
			jrXlsxExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			jrXlsxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reporte));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setDetectCellType(true);
			configuration.setCollapseRowSpan(true);
			jrXlsxExporter.setConfiguration(configuration);
			jrXlsxExporter.exportReport();
		
			/*serviceResponse.setCodResultado(0);
			serviceResponse.setMsgResultado("Informe Por Medicamento Grupo Diagnostico - MAC - Linea Generado ");
			serviceResponse.setResponse(reporte.toByteArray());*/

		} catch (Exception e) {
			/*serviceResponse.setCodResultado(-1);
			serviceResponse.setMsgResultado("ERROR Generando : Informe Por Medicamento Grupo Diagnostico - MAC - Linea [" + e.getMessage() + "]");*/

			throw e;
		}
		log.info(" \t - FIN - [" + "generarReporteIndicadorMedicamentoGrupoDiagnosticoMACLinea" + "] ");
		return reporte.toByteArray();
	}

	/*@Override
	public ApiOutResponse generarIndicadoresConsumoPorMacExcel(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		// log.info(" \t - INICIO - [" + "generarIndicadoresConsumoPorMacExcel" + "] ");
		// return reporteDao.generarIndicadoresConsumoPorMacExcel(requestRequest);
		
		JasperReport jasperReport;
		JasperPrint jasperPrint;
		ByteArrayOutputStream reporte = new ByteArrayOutputStream();
		
		ApiOutResponse serviceResponse = new ApiOutResponse();
		
		try {
			String path = ftpProp.getRutaReportes() + File.separator + "/ReporteConsumoMAC.jasper";
			
			Map parameters = new HashMap();
			parameters.put("P_MES", requestRequest.getMes());
			parameters.put("P_ANO", requestRequest.getAno());
			
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			
			File file = new File(path);
			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(path);
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);
			
			JRXlsxExporter jrXlsxExporter = new JRXlsxExporter();
			jrXlsxExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			jrXlsxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reporte));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setDetectCellType(true);
			configuration.setCollapseRowSpan(true);
			jrXlsxExporter.setConfiguration(configuration);
			jrXlsxExporter.exportReport();
		
			serviceResponse.setCodResultado(0);
			serviceResponse.setMsgResultado("Informe Por Medicamento Grupo Diagnostico - MAC - Linea Generado ");
			serviceResponse.setResponse(reporte.toByteArray());

		} catch (Exception e) {
			serviceResponse.setCodResultado(-1);
			serviceResponse.setMsgResultado("ERROR Generando : Informe Por Medicamento Grupo Diagnostico - MAC - Linea [" + e.getMessage() + "]");

			throw e;
		}
		log.info(" \t - FIN - [" + "generarReporteIndicadorMedicamentoGrupoDiagnosticoMACLinea" + "] ");
		return serviceResponse;
	}*/

	@Override
	public ApiOutResponse generarIndicadoresConsumoPorGrupoMacExcel(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarIndicadoresConsumoPorGrupoMacExcel" + "] ");
		// return reporteDao.generarIndicadoresConsumoPorGrupoMacExcel(requestRequest);
		return null;
	}

	@Override
	public ApiOutResponse generarIndicadoresConsumoPorGrupoMacLineaExcel(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarIndicadoresConsumoPorGrupoMacLineaExcel" + "] ");
		return reporteDao.generarIndicadoresConsumoPorGrupoMacLineaExcel(requestRequest);
	}

	@Override
	public ApiOutResponse generarIndicadoresConsumoPorGrupoMacLineaTiempoExcel(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarIndicadoresConsumoPorGrupoMacLineaTiempoExcel" + "] ");
		return reporteDao.generarIndicadoresConsumoPorGrupoMacLineaTiempoExcel(requestRequest);
	}

	@Override
	public ApiOutResponse generarReporteSolicitudesAutorizaciones(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarReporteSolicitudesAutorizaciones" + "] ");
		return reporteDao.generarReporteSolicitudesAutorizaciones(requestRequest);
	}

	@Override
	public ApiOutResponse generarReporteGenerales(ReporteIndicadoresRequest requestRequest) throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarReporteGenerales" + "] ");
		return reporteDao.generarReporteGenerales(requestRequest);
	}

	@Override
	public ApiOutResponse generarReporteSolicitudesMonitoreo(ReporteIndicadoresRequest requestRequest)
			throws Exception {
		// TODO Auto-generated method stub
		log.info(" \t - INICIO - [" + "generarReporteSolicitudesMonitoreo" + "] ");
		return reporteDao.generarReporteSolicitudesMonitoreo(requestRequest);
	}

}
