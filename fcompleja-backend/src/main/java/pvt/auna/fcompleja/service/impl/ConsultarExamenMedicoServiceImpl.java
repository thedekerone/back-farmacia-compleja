package pvt.auna.fcompleja.service.impl;

import pvt.auna.fcompleja.dao.consultarExamenMedicoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pvt.auna.fcompleja.model.api.request.ListaExamenMedicoRequest;
import pvt.auna.fcompleja.model.api.response.ListaExamenMedicoResponse;
import pvt.auna.fcompleja.service.ConsultarExamenMedicoService;

@Service
public class ConsultarExamenMedicoServiceImpl implements ConsultarExamenMedicoService {
	
	@Autowired
	consultarExamenMedicoDao consultarExamenMedicoDao;

	@Override
	public ListaExamenMedicoResponse consultarExamenMedico(ListaExamenMedicoRequest listaExamenMed)
			throws Exception {
		// TODO Auto-generated method stub
		return consultarExamenMedicoDao.consultarExamenMedico(listaExamenMed);
	}

}
