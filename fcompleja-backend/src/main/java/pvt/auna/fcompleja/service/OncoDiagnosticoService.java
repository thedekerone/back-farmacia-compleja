package pvt.auna.fcompleja.service;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.DiagnosticoRequest;
import pvt.auna.fcompleja.model.api.GrupoDiagnosticoRequest;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.WsResponse;

public interface OncoDiagnosticoService {
	public ResponseOnc getListarDiagnostico(HttpHeaders headers) throws Exception;

	public WsResponse obtenerDiagnostico(HttpHeaders headers, DiagnosticoRequest request) throws Exception;

	public ResponseOnc getListarGrupoDiagnostico(HttpHeaders headers) throws Exception;
	
	public ResponseOnc buscarGrupoDiagnostico(HttpHeaders headers, GrupoDiagnosticoRequest request) throws Exception;

	public WsResponse obtenerGrupoDiagnostico(HttpHeaders headers, GrupoDiagnosticoRequest request) throws Exception;
	
	public ResponseOnc getListarDiagnosticoXcodigos(String codigoDiagnostico, Integer total, HttpHeaders headers) throws Exception;
}
