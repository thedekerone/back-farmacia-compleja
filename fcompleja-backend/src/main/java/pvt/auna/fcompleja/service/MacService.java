package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.bean.CheckListBean;
import pvt.auna.fcompleja.model.bean.ComplicacionesBean;
import pvt.auna.fcompleja.model.bean.CriteriosBean;
import pvt.auna.fcompleja.model.bean.FichaTecnicaBean;
import pvt.auna.fcompleja.model.bean.MACBean;

public interface MacService {

	public ApiOutResponse listFiltroMAC(MACBean filtro);

	public ApiOutResponse registroMAC(MACBean filtro);

	public ApiOutResponse listaCheckListConfiguracion(CheckListBean obj);

	public ApiOutResponse registrarCheckListConfiguracion(CheckListBean obj);

	public ApiOutResponse listarCriterioInclusion(CriteriosBean obj);

	public ApiOutResponse listarCriterioExclusion(CriteriosBean obj);

	public ApiOutResponse registroCriterioExclusion(CriteriosBean obj);

	public ApiOutResponse registroCriterioInclusion(CriteriosBean obj);

	public ApiOutResponse registroFichaTecnica(FichaTecnicaBean obj);

	public ApiOutResponse listarFichasTecnicas(FichaTecnicaBean obj);
	
	public ApiOutResponse registroComplicacionesMedicas(ComplicacionesBean obj);
	
	public ApiOutResponse listaComplicacionesMedicas(ComplicacionesBean obj);
	
	public ApiOutResponse registrarIndicacionCriterios(CheckListBean obj);
}
