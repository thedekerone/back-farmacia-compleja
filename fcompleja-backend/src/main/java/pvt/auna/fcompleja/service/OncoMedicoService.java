package pvt.auna.fcompleja.service;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.MedicoRequest;
import pvt.auna.fcompleja.model.api.WsResponse;

public interface OncoMedicoService {
	public WsResponse obtenerMedicoTratante(HttpHeaders headers, MedicoRequest request);
}
