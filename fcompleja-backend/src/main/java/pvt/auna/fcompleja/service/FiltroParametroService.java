package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.FiltroParametroRequest;
import pvt.auna.fcompleja.model.api.FiltroParametroResponse;

public interface FiltroParametroService {

	public FiltroParametroResponse FiltroParametro(FiltroParametroRequest FiltroListaParametro) throws Exception;
	public FiltroParametroResponse ControlFilaParametro(FiltroParametroRequest request) throws Exception;
	public ApiOutResponse ConsultaParametroValue(FiltroParametroRequest request) throws Exception;
}
