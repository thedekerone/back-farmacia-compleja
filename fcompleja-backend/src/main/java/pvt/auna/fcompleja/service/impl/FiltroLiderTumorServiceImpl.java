package pvt.auna.fcompleja.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pvt.auna.fcompleja.model.api.FiltroLiderTumorRequest;
import pvt.auna.fcompleja.model.api.FiltroLiderTumorResponse;
import pvt.auna.fcompleja.service.FiltroLiderTumorService;
import pvt.auna.fcompleja.dao.FiltroLiderTumorDao;;;

@Service
public class FiltroLiderTumorServiceImpl implements FiltroLiderTumorService {

	@Autowired
	FiltroLiderTumorDao FiltroLiderTumorDao;
	
	@Override
	public FiltroLiderTumorResponse FiltroLiderTumor(FiltroLiderTumorRequest ListaTumor) throws Exception {
		// TODO Auto-generated method stub
		return FiltroLiderTumorDao.FiltroLiderTumor(ListaTumor);
	}

}
