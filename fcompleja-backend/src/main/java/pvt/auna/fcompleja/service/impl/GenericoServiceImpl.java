package pvt.auna.fcompleja.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.config.ConfigFTPProp;
import pvt.auna.fcompleja.config.OncoPropiedades;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ArchivoFTPBean;
import pvt.auna.fcompleja.model.api.DiagnosticoRequest;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.AfiliadoRequest;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.RequestImputReportEvaluacion;
import pvt.auna.fcompleja.model.api.request.RequestObtenerArchivo;
import pvt.auna.fcompleja.model.api.response.ResponseFTP;
import pvt.auna.fcompleja.model.api.response.ResponseFTPObtener;
import pvt.auna.fcompleja.model.bean.PacienteBean;
import pvt.auna.fcompleja.service.GenericoService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@Service
public class GenericoServiceImpl implements GenericoService {

	@Autowired
	private RestTemplate restTemplate;

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(GenericoServiceImpl.class);

	@Autowired
	private OncoPropiedades propiedades;

	@Autowired
	private AseguramientoPropiedades asegProp;

	@Autowired
	private ConfigFTPProp FTPprop;

	@Autowired
	OncoPacienteService pcteService;

	public ResponseFTP enviarArchivo(String usrApp, String nomArchivo, MultipartFile archivo, String ruta,
									 HttpHeaders header) throws Exception {

		header.setContentType(MediaType.MULTIPART_FORM_DATA);

		String url = asegProp.getRutaFTP() + "/enviarArchivo";

		String tempFileName = FTPprop.getRutaTemp() + archivo.getOriginalFilename();
		FileOutputStream fo;

		fo = new FileOutputStream(tempFileName);
		fo.write(archivo.getBytes());
		fo.close();

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("usrApp", usrApp);
		body.add("nomArchivo", nomArchivo);
		body.add("archivo", new FileSystemResource(tempFileName));// archivo); //new
		// InputStreamResource(archivo.getInputStream()));
		body.add("ruta", ruta);

		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, header);

		ResponseFTP response = null;
		restTemplate = new RestTemplate();

		try {
			response = restTemplate.postForObject(url, requestEntity, ResponseFTP.class);
		} catch (HttpClientErrorException e) {
			LOGGER.info(e.getStatusCode().toString());
			LOGGER.info(e.getResponseBodyAsString());
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
		}

		return response;

	}

//	@Override
//	public String obtenerDiagnostico(String codDiagnostico) throws Exception {
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		headers.add("idTransaccion", "123456");
//		headers.add("fechaTransaccion", "17/04/2019");
//		DiagnosticoRequest diagnosticoRequest = new DiagnosticoRequest();
//		ResponseOnc diagnosticoResponse = new ResponseOnc();
//
//		String UrlVcharDiag = propiedades.getOncoDiagnostico() + "/consulta/diagnostico";
//		diagnosticoRequest.setTipoBusqueda(ConstanteUtil.tipoXCod); // busqueda por codigo
//		// de diagnostico
//		diagnosticoRequest.setCodigoDiagnostico(codDiagnostico.toString());
//		diagnosticoRequest.setNombreDiagnostico("''");
//		diagnosticoRequest.setRegistroInicio(1);
//		diagnosticoRequest.setRegistroFin(10);
//
//		HttpEntity<DiagnosticoRequest> Requestdiagnostico = new HttpEntity<DiagnosticoRequest>(diagnosticoRequest,
//				headers);
//		diagnosticoResponse = restTemplate.postForObject(UrlVcharDiag, Requestdiagnostico, ResponseOnc.class);
//
//		System.out.println(diagnosticoResponse.toString());
//
//		return (String) ((HashMap<?, ?>) diagnosticoResponse.getDataList().get(0)).get("nomdia");
//	}

	@Override
	public RequestImputReportEvaluacion obtenerAfiliados(long codAfiliado) throws Exception {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("idTransaccion", "123456");
		headers.add("fechaTransaccion", "17/04/2019");

		String url = propiedades.getOncoAfiliado() + "/buscar/contacto";

		RequestImputReportEvaluacion objRes = new RequestImputReportEvaluacion();

		AfiliadoRequest objAfi = new AfiliadoRequest();
		objAfi.setCodigoAfiliado(codAfiliado);

		HttpEntity<AfiliadoRequest> requestEntity = new HttpEntity<AfiliadoRequest>(objAfi, headers);

		ResponseOnc response = new ResponseOnc();
		restTemplate = new RestTemplate();

		try {

			response = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);

		} catch (HttpClientErrorException e) {
			LOGGER.info(e.getStatusCode().toString());
			LOGGER.info(e.getResponseBodyAsString());
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
		}

		objRes.setSexo((String) ((HashMap<?, ?>) response.getDataList().get(0)).get("sexafi"));
		objRes.setNombrePaciente((String) ((HashMap<?, ?>) response.getDataList().get(0)).get("nombr1") + " "
				+ (String) ((HashMap<?, ?>) response.getDataList().get(0)).get("apepat"));

		return objRes;
	}

	@Override
	public byte[] obtenerArchivoFTP(String nombre, String ruta, String usrApp) throws Exception {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("idTransaccion", "123456");
		headers.add("fechaTransaccion", "12/12/19");

		String url = asegProp.getRutaFTP() + "/obtenerArchivo";

		RequestObtenerArchivo objRep = new RequestObtenerArchivo();
		objRep.setNomArchivo(nombre);
		objRep.setRuta(ruta);
		objRep.setUsrApp(usrApp);

		HttpEntity<RequestObtenerArchivo> requestEntity = new HttpEntity<RequestObtenerArchivo>(objRep, headers);

		ResponseFTPObtener response = new ResponseFTPObtener();
		restTemplate = new RestTemplate();

		try {

			response = restTemplate.postForObject(url, requestEntity, ResponseFTPObtener.class);

		} catch (HttpClientErrorException e) {
			LOGGER.info(e.getStatusCode().toString());
			LOGGER.info(e.getResponseBodyAsString());
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
		}

		System.out.println(response.getDataList().get(0).getArchivo().toString());
		return response.getDataList().get(0).getArchivo();
	}

	@Override
	public String obtenerGrupoDiagnostico(String codDiagnostico) throws Exception {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("idTransaccion", "123456");
		headers.add("fechaTransaccion", "17/04/2019");
		DiagnosticoRequest diagnosticoRequest = new DiagnosticoRequest();
		ResponseOnc diagnosticoResponse = new ResponseOnc();

		String descripGrpDiag = null;

		String UrlVcharDiag = propiedades.getOncoDiagnostico() + "/consulta/diagnostico";
		diagnosticoRequest.setTipoBusqueda(ConstanteUtil.tipoXCod); // busqueda por codigo
		// de diagnostico
		diagnosticoRequest.setCodigoDiagnostico(codDiagnostico.toString());
		diagnosticoRequest.setNombreDiagnostico(null);
		diagnosticoRequest.setRegistroInicio(1);
		diagnosticoRequest.setRegistroFin(10);

		HttpEntity<DiagnosticoRequest> Requestdiagnostico = new HttpEntity<DiagnosticoRequest>(diagnosticoRequest,
				headers);
		diagnosticoResponse = restTemplate.postForObject(UrlVcharDiag, Requestdiagnostico, ResponseOnc.class);

		if (diagnosticoResponse.getDataList() != null && diagnosticoResponse.getDataList().size() > 0
				&& diagnosticoResponse.getDataList().get(0) != null) {
			descripGrpDiag = (String) ((HashMap<?, ?>) diagnosticoResponse.getDataList().get(0)).get("nomgru");
		}
		return descripGrpDiag;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String obtenerPaciente(AfiliadosRequest pacienteRequest) throws Exception {

		ArrayList<PacienteBean> listaPaciente = null;
		String descripPaciente = null;

		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYY(new Date());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		ResponseOnc pacienteRes = pcteService.obtenerListaPacientexCodigos(pacienteRequest, headers);
		if (pacienteRes != null && Integer.parseInt(pacienteRes.getAudiResponse().getCodigoRespuesta()) >= 0) {
			listaPaciente = (ArrayList<PacienteBean>) pacienteRes.getDataList();

			if (listaPaciente != null && !listaPaciente.isEmpty()) {
				for (PacienteBean pcte : listaPaciente) {
					if (pcte.getApepat() != null) {
						descripPaciente = pcte.getApepat();
					}
					if (pcte.getApemat() != null) {
						descripPaciente = descripPaciente + " " + pcte.getApemat();
					}
					if (pcte.getNombr1() != null) {
						descripPaciente = descripPaciente + ", " + pcte.getNombr1();
					}
					if (pcte.getNombr2() != null) {
						descripPaciente = descripPaciente + " " + pcte.getNombr2();
					}
					// descripPaciente = pcte.getApepat() + " " + pcte.getApemat() + ", " +
					// pcte.getNombr1() + " " + pcte.getNombr2();
				}
			}
			descripPaciente = descripPaciente.trim();
		} else {
			listaPaciente = null;
		}

		return descripPaciente;
	}

	@Override
	public ApiOutResponse descargarArchivo(ArchivoFTPBean archivoBean, HttpHeaders headers) throws Exception {

		String url = asegProp.getRutaFTP() + "/obtenerArchivo";


		ResponseFTP resultado = null;
		ApiOutResponse response = new ApiOutResponse();
		try {
			HttpEntity<ArchivoFTPBean> requestEntity = new HttpEntity<ArchivoFTPBean>(archivoBean, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseFTP.class);

			if (resultado != null) {
				if (resultado.getAuditResponse().getCodigoRespuesta().equalsIgnoreCase("0")) {
					response.setCodResultado(Integer.parseInt(resultado.getAuditResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAuditResponse().getDescripcionRespuesta());

					ArchivoFTPBean archivoJson = ((ArrayList<ArchivoFTPBean>) resultado.getDataList()).get(0);
					response.setResponse(archivoJson);
				} else {
					response.setCodResultado(1);
					response.setMsgResultado("Error en el servicio de obtener archivo del FTP");
					response.setResponse(null);
				}
			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error en el servicio de obtener archivo del FTP");
				response.setResponse(null);
			}
		} catch (Exception e) {

			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error en el servicio de subir archivo FTP");
			response.setResponse(null);
		}

		return response;

//		System.out.println(response.getDataList().get(0).getArchivo().toString());
//		return response.getDataList().get(0).getArchivo();
	}

	@Override
	public ApiOutResponse enviarArchivoFTP(String usrApp, String nomArchivo, MultipartFile archivo, String ruta,
										   HttpHeaders header) throws Exception {
		header.setContentType(MediaType.MULTIPART_FORM_DATA);

		String url = asegProp.getRutaFTP() + "/enviarArchivo";

		String tempFileName = FTPprop.getRutaTemp() + "/" +archivo.getOriginalFilename();

		ResponseFTP resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		File fileTemp = null;
		FileOutputStream fo = null;

		LOGGER.info("[SERVICIO:  SUBIR ARCHIVO][TEMPFILE: "+tempFileName+" - RUTA FTP: "+ruta+"]");
		try {
			fileTemp = new File(tempFileName);

			LOGGER.info("fileTemp1: " + fileTemp.toString());

			LOGGER.info("ini write");
			fo = new FileOutputStream(fileTemp.getPath());
			fo.write(archivo.getBytes());
			fo.close();

			LOGGER.info("fin write");

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
			body.add("usrApp", usrApp);
			body.add("nomArchivo", nomArchivo);
			body.add("archivo", new FileSystemResource(tempFileName));
			body.add("ruta", ruta);

			LOGGER.info("body: " + body.toString());

			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, header);

			resultado = restTemplate.postForObject(url, requestEntity, ResponseFTP.class);

			LOGGER.info("resultado1: " + resultado.toString());
			if (resultado != null) {

				LOGGER.info("resultado2: " + resultado.toString());
				if (resultado.getAuditResponse().getCodigoRespuesta().equals("0")) {
					LOGGER.info("resultado3: " + resultado.toString());
					response.setCodResultado(Integer.parseInt(resultado.getAuditResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAuditResponse().getDescripcionRespuesta());
					response.setResponse(((ArrayList<ArchivoFTPBean>) resultado.getDataList()).get(0));
				} else {
					response.setCodResultado(1);
					response.setMsgResultado("Error en el servicio de obtener archivo del FTP");
					response.setResponse(null);
				}
			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error en el servicio de obtener archivo del FTP");
				response.setResponse(null);
			}

			LOGGER.info("fileTemp2: " + fileTemp.toString());

			fileTemp.delete();

		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error en el servicio de subir archivo FTP");
			response.setResponse(null);
			if(fo != null) fo.close();
			if(fileTemp != null) fileTemp.delete();
		}

		LOGGER.info("responseFin: " + response.toString());

		return response;
	}

	@Override
	public ApiOutResponse enviarArchivoFTP(String usrApp, String nomArchivo, byte[] archivo, String ruta,
										   HttpHeaders header) throws Exception {
		header.setContentType(MediaType.MULTIPART_FORM_DATA);

		String url = asegProp.getRutaFTP() + "/enviarArchivo";

		String tempFileName = FTPprop.getRutaTemp() + "/" + nomArchivo;//TEMPORAL

		ResponseFTP resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		File fileTemp = null;
		FileOutputStream fo = null;

		try {
			fileTemp = new File(tempFileName);
			fo = new FileOutputStream(tempFileName);
			fo.write(archivo);
			fo.close();

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
			body.add("usrApp", usrApp);
			body.add("nomArchivo", nomArchivo);
			body.add("archivo", new FileSystemResource(tempFileName));
			body.add("ruta", ruta);

			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, header);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseFTP.class);
			if (resultado != null) {
				if (resultado.getAuditResponse().getCodigoRespuesta().equals("0")) {
					response.setCodResultado(Integer.parseInt(resultado.getAuditResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAuditResponse().getDescripcionRespuesta());
					response.setResponse(((ArrayList<ArchivoFTPBean>) resultado.getDataList()).get(0));
				} else {
					response.setCodResultado(1);
					response.setMsgResultado("Error en el servicio de obtener archivo del FTP");
					response.setResponse(null);
				}
			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error en el servicio de obtener archivo del FTP");
				response.setResponse(null);
			}
			fileTemp.delete();
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error en el servicio de subir archivo FTP");
			response.setResponse(null);
			if(fo != null) fo.close();
			if(fileTemp != null) fileTemp.delete();
		}

		return response;
	}

}