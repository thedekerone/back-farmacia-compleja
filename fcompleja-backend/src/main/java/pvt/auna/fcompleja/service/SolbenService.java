package pvt.auna.fcompleja.service;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.request.SolbenWsRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudPreliminarRequest;
import pvt.auna.fcompleja.model.api.response.SolbenWsResponse;
import pvt.auna.fcompleja.model.api.response.SolicitudPreliminarResponse;

public interface SolbenService {
	public SolicitudPreliminarResponse getScgSolbenAndInsertSolPreliminar(SolicitudPreliminarRequest request,HttpHeaders headers );
	public SolbenWsResponse actualizarSolben(SolbenWsRequest request);
}
