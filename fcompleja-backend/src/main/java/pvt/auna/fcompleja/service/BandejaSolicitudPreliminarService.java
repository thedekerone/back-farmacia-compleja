package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.SolicitudesPreeliminarFiltroRequest;

public interface BandejaSolicitudPreliminarService {
	ApiOutResponse listaBandejaSolicitudes(SolicitudesPreeliminarFiltroRequest filtroBean) throws Exception;
}
