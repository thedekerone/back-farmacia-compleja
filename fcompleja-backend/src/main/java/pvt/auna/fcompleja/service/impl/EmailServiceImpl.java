package pvt.auna.fcompleja.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.dao.DetalleSolicitudEvaluacionDao;
import pvt.auna.fcompleja.dao.EmailDao;
import pvt.auna.fcompleja.dao.MonitoreoDao;
import pvt.auna.fcompleja.dao.UsuarioDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.CasosEvaluar;
import pvt.auna.fcompleja.model.api.EmailResponse;
import pvt.auna.fcompleja.model.api.ListaBandeja;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.TipoCorreoResponse;
import pvt.auna.fcompleja.model.api.request.AfiliadosRequest;
import pvt.auna.fcompleja.model.api.request.EmailAlertaCmacRequest;
import pvt.auna.fcompleja.model.api.request.EmailAlertaLiderTumorRequest;
import pvt.auna.fcompleja.model.api.request.EmailAlertaMonitoreoRequest;
import pvt.auna.fcompleja.model.api.request.EmailSolicInscripRequest;
import pvt.auna.fcompleja.model.api.request.InformeSolEvaReporteRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.request.UsuariosPorCodigoRequest;
import pvt.auna.fcompleja.model.api.request.evaluacion.ParticipanteRequest;
import pvt.auna.fcompleja.model.api.response.EmailAlertaMonitoreoResponse;
import pvt.auna.fcompleja.model.api.response.EmailAlertaResponse;
import pvt.auna.fcompleja.model.api.response.EmailCodigoEnvioSolicitudResponse;
import pvt.auna.fcompleja.model.api.response.EmailSolicInscripResponse;
import pvt.auna.fcompleja.model.api.response.ParametroGrupoResponse;
import pvt.auna.fcompleja.model.api.response.ResponseGenerico;
import pvt.auna.fcompleja.model.api.response.evaluacion.ChecklistRequisitoResponse;
import pvt.auna.fcompleja.model.api.response.evaluacion.ParticipanteResponse;
import pvt.auna.fcompleja.model.api.response.evaluacion.ProgramacionCmacDetResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.DetCorreoParticipanteResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.ResponseGeneric;
import pvt.auna.fcompleja.model.bean.DiagnosticoBean;
import pvt.auna.fcompleja.model.bean.EmailBean;
import pvt.auna.fcompleja.model.bean.Mail;
import pvt.auna.fcompleja.model.bean.PacienteBean;
import pvt.auna.fcompleja.model.bean.ParticipanteBean;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.service.EmailService;
import pvt.auna.fcompleja.service.MonitoreoService;
import pvt.auna.fcompleja.service.OncoDiagnosticoService;
import pvt.auna.fcompleja.service.OncoPacienteService;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.ConstanteUtil;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@Service
public class EmailServiceImpl implements EmailService {

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

	@Autowired
	private AseguramientoPropiedades prop;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private EmailDao emailDao;
	@Autowired
	private JavaMailSender emailSender;
	@Autowired
	private UsuarioDao lstUsuarioDao;
	@Autowired
	MonitoreoService monitoreoService;
	@Autowired
	MonitoreoDao monitoreoDao;
	@Autowired
	OncoPacienteService pcteService;
	@Autowired
	OncoDiagnosticoService diagService;
	@Autowired
	DetalleSolicitudEvaluacionDao detalleEvaluaDao;
	@Autowired
	UsuarioService usuarioService;

	public EmailBean obtenerDatosCorreo(Long codEmail) throws Exception {
		return this.emailDao.obtenerDatosCorreo(codEmail);
	}

	@Override
	public TipoCorreoResponse obtenerDatoTipoCorreo(int codTipoEmail) throws Exception {
		return this.emailDao.obtenerDatoTipoCorreo(codTipoEmail);
	}

	public List<EmailSolicInscripResponse> sendSolicitudInscripcionMac(Mail mail,
			EmailSolicInscripRequest solInsrequest, byte[] byteFile) throws MessagingException, Exception {

		List<EmailSolicInscripResponse> listaEmailSolic = new ArrayList<>();
		EmailSolicInscripResponse emailSolic = new EmailSolicInscripResponse();

		MimeMessage message = emailSender.createMimeMessage();
		message.setHeader("Content-Type", "text/plain; charset=UTF-8");

		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		try {
			helper.setSubject(mail.getSubject());
			helper.setText(mail.getContent(), true);
			if (solInsrequest.getNombDocument() != null && byteFile != null) {
				helper.addAttachment(solInsrequest.getNombDocument(), new ByteArrayResource(byteFile));
			}
			helper.setTo(mail.getTo());
			emailSender.send(message);
			emailSolic.setEnvioCorreo(true);
			emailSolic.setErrorCorreo("");
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Éxito
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		} catch (MailSendException em) {
			System.out.println("Error MailSendException al enviar: " + em.getMessage());
			emailSolic.setErrorCorreo(em.getMessage());
			emailSolic.setEnvioCorreo(false);
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Error
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		} catch (Exception e) {
			System.out.println("Error Exception al enviar: " + e.getMessage());
			emailSolic.setErrorCorreo(e.getMessage());
			emailSolic.setEnvioCorreo(false);
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Error
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		}
		listaEmailSolic.add(emailSolic);
		return listaEmailSolic;
	}

	@Override
	public List<EmailAlertaMonitoreoResponse> enviarEmailAlertaMonitoreo(EmailResponse emailResponse,
			EmailAlertaMonitoreoRequest request) throws MessagingException, Exception {
		List<EmailAlertaMonitoreoResponse> listaResponse = new ArrayList<>();
		EmailAlertaMonitoreoResponse response = new EmailAlertaMonitoreoResponse();

		MimeMessage message = emailSender.createMimeMessage();
		message.setHeader("Content-Type", "text/plain; charset=UTF-8");

		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		try {
			helper.setSubject(emailResponse.getSubject());
			helper.setText(emailResponse.getContent(), true);

			helper.setTo(emailResponse.getTo());
			emailSender.send(message);
			response.setEnvioCorreo(true);
			response.setErrorCorreo("");
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Éxito
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		} catch (MailSendException em) {
			System.out.println("Error MailSendException al enviar: " + em.getMessage());
			response.setErrorCorreo(em.getMessage());
			response.setEnvioCorreo(false);
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Error
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		} catch (Exception e) {
			System.out.println("Error Exception al enviar: " + e.getMessage());
			response.setErrorCorreo(e.getMessage());
			response.setEnvioCorreo(false);
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Error
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		}
		listaResponse.add(response);
		return listaResponse;
	}

	@Override
	public List<EmailAlertaResponse> enviarEmailAlertaCmac(EmailResponse emailResponse, EmailAlertaCmacRequest request)
			throws MessagingException, Exception {
		List<EmailAlertaResponse> listaResponse = new ArrayList<>();
		EmailAlertaResponse response = new EmailAlertaResponse();

		MimeMessage message = emailSender.createMimeMessage();
		message.setHeader("Content-Type", "text/html; charset=UTF-8");

		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		try {
			helper.setSubject(emailResponse.getSubject());
			helper.setText(emailResponse.getContent(), true);

			helper.setTo(emailResponse.getTo());
			emailSender.send(message);
			response.setEnvioCorreo(true);
			response.setErrorCorreo("");
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Éxito
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		} catch (MailSendException em) {
			System.out.println("Error MailSendException al enviar: " + em.getMessage());
			response.setErrorCorreo(em.getMessage());
			response.setEnvioCorreo(false);
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Error
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		} catch (Exception e) {
			System.out.println("Error Exception al enviar: " + e.getMessage());
			response.setErrorCorreo(e.getMessage());
			response.setEnvioCorreo(false);
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Error
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		}
		listaResponse.add(response);
		return listaResponse;
	}

	@Override
	public List<EmailAlertaMonitoreoResponse> enviarEmailAlertaLiderTumor(Mail mail,
			EmailAlertaLiderTumorRequest request, byte[] byteFile1, byte[] byteFile2, byte[] byteFile3,
			byte[] byteFile4, byte[] byteFile5) throws MessagingException, Exception {

		List<EmailAlertaMonitoreoResponse> listaResponse = new ArrayList<>();
		EmailAlertaMonitoreoResponse response = new EmailAlertaMonitoreoResponse();

		MimeMessage message = emailSender.createMimeMessage();
		message.setHeader("Content-Type", "text/plain; charset=UTF-8");

		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		try {
			helper.setSubject(mail.getSubject());
			helper.setText(mail.getContent(), true);
			if (request.getFileInformeEvaAuditorOrAutorizador() != null && byteFile1 != null) {
				helper.addAttachment(request.getNameDocument(), new ByteArrayResource(byteFile1));
			} else if (request.getFileInformeMedico() != null && byteFile2 != null) {
				helper.addAttachment(request.getNameDocument(), new ByteArrayResource(byteFile2));
			} else if (request.getFileReceta() != null && byteFile3 != null) {
				helper.addAttachment(request.getFileReceta(), new ByteArrayResource(byteFile3));
			} else if (request.getFileGeriatrico() != null && byteFile4 != null) {
				helper.addAttachment(request.getNameDocument(), new ByteArrayResource(byteFile4));
			} else if (request.getFileDocument() != null && byteFile5 != null) {
				helper.addAttachment(request.getNameDocument(), new ByteArrayResource(byteFile5));
			}

			helper.setTo(mail.getTo());
			emailSender.send(message);
			response.setEnvioCorreo(true);
			response.setErrorCorreo("");
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Éxito
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		} catch (MailSendException em) {
			System.out.println("Error MailSendException al enviar: " + em.getMessage());
			response.setErrorCorreo(em.getMessage());
			response.setEnvioCorreo(false);
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Error
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		} catch (Exception e) {
			System.out.println("Error Exception al enviar: " + e.getMessage());
			response.setErrorCorreo(e.getMessage());
			response.setEnvioCorreo(false);
			// auditoriaDao.registrarErrorDmlLogerThread(1,
			// ConstanteUtil.EVENTO_ENVIAR_EMAIL, null, "APP_ENVIAR COREEO SIMPLE", "Error
			// al enviar Correo", ConstanteUtil.INDICADOR_PROCESO_JAVA);
		}
		listaResponse.add(response);
		return listaResponse;
	}

	@Override
	public ResponseGenerico generarCorreo(EmailBean obj, HttpHeaders headers) {

		ResponseGenerico response = null;

		HttpEntity<EmailBean> request = null;

		try {
			request = new HttpEntity<EmailBean>(obj, headers);

			String url = prop.getRutaEmail() + "/generarEnviov1";
			response = restTemplate.postForObject(url, request, ResponseGenerico.class);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			response = null;
		}
		return response;
	}

	@Override
	public ResponseGenerico enviarCorreo(EmailBean obj, HttpHeaders headers) {
		ResponseGenerico response = null;
		HttpEntity<EmailBean> request = null;

		try {
			ParametroGrupoResponse participante = emailDao.consultarParticipanteGrupo(obj);
			if (participante.getListaParticipante().size() > 0) {
				List<ParticipanteResponse> lpart = participante.getListaParticipante();

				UsuariosPorCodigoRequest usuarioPorCodigoRequest = new UsuariosPorCodigoRequest();
				String codUsuarioCodRol = "";
				for (ParticipanteResponse part : lpart) {
					if (part.getCodUsuario() != null) {
						if (codUsuarioCodRol.equals(""))
							codUsuarioCodRol = codUsuarioCodRol + part.getCodUsuario();
						else
							codUsuarioCodRol = codUsuarioCodRol + "|" + part.getCodUsuario();
					}
				}

				usuarioPorCodigoRequest.setCodigoUsuario(codUsuarioCodRol);

				List<UsuarioBean> listUsuario;
				listUsuario = (List<UsuarioBean>) usuarioService.listarUsuarioPorCodigos(usuarioPorCodigoRequest, headers).getDataList();

				for (ParticipanteResponse part : lpart) {
					if (part.getCodUsuario() != null) {
						for (UsuarioBean userRol : listUsuario) {
							if (userRol.getCodUsuario().toString().equals(part.getCodUsuario().toString())) {
								part.setNombres(userRol.getNombre());
								part.setApellidos(userRol.getApellidoPaterno() + " " + userRol.getApellidoMaterno());
								part.setCorreoElectronico(userRol.getCorreo());
								break;
							}
						}
					}
				}

				if (lpart.get(0).getCorreoElectronico() != null && !(lpart.get(0).getCorreoElectronico().equals(""))) {
					obj.setDestinatario(lpart.get(0).getCorreoElectronico());

					request = new HttpEntity<EmailBean>(obj, headers);

					String url = prop.getRutaEmail() + "/finalizarEnvio";
					response = restTemplate.postForObject(url, request, ResponseGenerico.class);
				} else {
					response = new ResponseGenerico(new AudiResponse(), null);
					response.getAudiResponse().setCodigoRespuesta("-2");
					response.getAudiResponse().setMensajeRespuesta(
							"Error al enviar correo, no se pudo obtener el email del responsable monitoreo: ");
				}
			} else {
				response = new ResponseGenerico(new AudiResponse(), null);
				response.getAudiResponse().setCodigoRespuesta("-2");
				response.getAudiResponse().setMensajeRespuesta(
						"Error al enviar correo, no existe un responsable monitoreo para el codigo grupo diagnostico: "
								+ obj.getCodigoGrupoDiagnostico());
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			response = new ResponseGenerico(new AudiResponse(), null);
			response.getAudiResponse().setCodigoRespuesta("-3");
			response.getAudiResponse().setMensajeRespuesta("Error interno del servidor al enviar correo");
		}
		return response;
	}

	@Override
	public ResponseGenerico enviarCorreoReunionMac(EmailBean obj, HttpHeaders headers) {
		ResponseGenerico response = null;
		HttpEntity<EmailBean> request = null;

		try {

			request = new HttpEntity<EmailBean>(obj, headers);

			String url = prop.getRutaEmail() + "/finalizarEnvio";
			response = restTemplate.postForObject(url, request, ResponseGenerico.class);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			response = null;
		}

		return response;
	}

	@Override
	public ResponseGenerico verificarEnvioCorreo(EmailBean obj, HttpHeaders headers) {
		ResponseGenerico response = null;
		HttpEntity<EmailBean> request = null;

		try {
			request = new HttpEntity<EmailBean>(obj, headers);
			EmailCodigoEnvioSolicitudResponse emailCodigoEnvioSolicitudResponse = emailDao
					.codigoEnvioSolicitud(obj.getCodSolicitudEvaluacion());
			String codigoEnvio = emailCodigoEnvioSolicitudResponse.getObjCodigoEnvioSolicitudBean()
					.getCodEnvioSolEvaluacion();

			if (codigoEnvio != null) {
				obj.setCodigoEnvio(codigoEnvio);

				String url = prop.getRutaEmail() + "/verificarEstadoEnvio";
				response = restTemplate.postForObject(url, request, ResponseGenerico.class);

				Map<String, String> mapa = (Map<String, String>) response.getDataList().get(0);

				if (response.getAudiResponse().getCodigoRespuesta().equals("0")) {// CONSULTA CORRECTA
					if (mapa.get("estadoEnvio").equals("1")) {// VALIDAR ENVIADO 1 => ENVIADO CORRECTO 0 => PENDIENTE

						// ACTUALIZA ESTADO
						obj.setEstadoSolicitudEvaluacion(ConstanteUtil.ESTADO_PENDIENTE_RESPUESTA_MONITOREO);
						ApiOutResponse apiOutResponse = emailDao.actualizarEstadoSolicitud(obj);
						if (apiOutResponse.getCodResultado() == 0) {
							response.getAudiResponse().setMensajeRespuesta(apiOutResponse.getMsgResultado());
							LOGGER.info("Respuesta Correcta : " + response);
						} else {
							response.getAudiResponse().setMensajeRespuesta(apiOutResponse.getMsgResultado());
							LOGGER.info("Respuesta Incorrecta :" + response);
						}
					} else {
						response.getAudiResponse().setCodigoRespuesta("-1");
						response.getAudiResponse()
								.setMensajeRespuesta("El correo se encuentra en estado pendiente de envio");
					}
				}
			} else {
				response = new ResponseGenerico(new AudiResponse(), null);
				response.getAudiResponse().setCodigoRespuesta("-2");
				response.getAudiResponse().setMensajeRespuesta(
						"No se encontro ningun pendiente de envio, reintente enviar el correo nuevamente");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			LOGGER.info("Error en la base de datos : " + response);
			response.getAudiResponse().setCodigoRespuesta("-1");
			response.getAudiResponse().setMensajeRespuesta(e.getMessage());
		}
		return response;
	}

	@Override
	public ApiOutResponse actParamsCorreoLiderTumor(SolicitudEvaluacionRequest solicitud) {
		return emailDao.actParamsCorreoLiderTumor(solicitud);
	}

	@Override
	public String reenviarCorreoLiderTumor(ListaBandeja request, HttpHeaders headers) {
		String mensaje = "";
		try {
			if (request.getEstadoCorreoEnvLiderTumor().equals(ConstanteUtil.estadoCorreoEnviadoSI)) {
				mensaje = "Correo ENVIADO";
			} else {
				if (request.getCodigoEnvioEnvLiderTumor().equals(0L)) {
					Map<String, Object> mapa = enviarEmailLiderTumorPrimeraVez(request, headers);// ENVIO PRIMERA VEZ
					Long codEnvioGenerado = (Long) mapa.get("codigoEnvio");
					if (codEnvioGenerado != null) {
						mensaje = mapa.get("mensaje").toString();
					} else {
						mensaje = mapa.get("mensaje").toString();
					}
				} else {// NO SE ENVIO => REENVIAR
					mensaje = procesarReenvioCorreoLiderTumor(request, headers);
				}
			}
		} catch (Exception e) {
			mensaje = "Exception en reenvio de correo";
		}
		return mensaje;
	}

	public Map<String, Object> enviarEmailLiderTumorPrimeraVez(ListaBandeja request, HttpHeaders headers)
			throws Exception {
		ResponseOnc onco = monitoreoService.consultaOncosysDatosDiag(request.getCodigoDiagnostico(), headers);
		Map<String, Object> mapResp = new HashMap<>();
		mapResp.put("codigoEnvio", null);

		if (onco.getDataList().size() > 0) {
			HashMap<?, ?> map = (HashMap<?, ?>) onco.getDataList().get(0);
			ParticipanteRequest req = new ParticipanteRequest();
			req.setCodRol(Long.parseLong(ConstanteUtil.COD_ROL_LIDER_TUMOR + ""));
			req.setCodGrpDiag(map.get("codgru").toString());

			List<ParticipanteResponse> lista = (List<ParticipanteResponse>) lstUsuarioDao.listarUsuarioFarmaciaDet(req)
					.getResponse();
			if (lista.size() > 0) {
				ParticipanteResponse part = lista.get(0);
				if (GenericUtil.emailFormat(part.getCorreoElectronico())) {// VALIDA
																			// CORREO
					InformeSolEvaReporteRequest reqChkList = new InformeSolEvaReporteRequest();
					reqChkList.setCodSolEva(Integer.parseInt(request.getCodSolEvaluacion()));

					ApiOutResponse respCheckList = detalleEvaluaDao.listarCodDocumentosChecklist(reqChkList);

					if (respCheckList.getCodResultado().equals(0)) {
						String rutaDoc = "";
						List<ChecklistRequisitoResponse> listCheckList = (List<ChecklistRequisitoResponse>) respCheckList
								.getResponse();
						for (ChecklistRequisitoResponse chk : listCheckList) {
							if (chk.getCodArchivo() != null) {
								rutaDoc += chk.getCodArchivo() + ",";
							}
						}
						rutaDoc = rutaDoc.substring(0, rutaDoc.length() - 1);

						EmailBean correoRequest = new EmailBean();
						correoRequest.setCodPlantilla(ConstanteUtil.EVALUACION_LIDER_TUMOR_codigoPlantilla);
						correoRequest.setFechaProgramada(DateUtils.getDateToStringDDMMYYYHHMMSS(new Date()));
						correoRequest.setFlagAdjunto(ConstanteUtil.EVALUACION_LIDER_TUMOR_flagAdjunto);
						correoRequest.setTipoEnvio(ConstanteUtil.EVALUACION_LIDER_TUMOR_tipoEnvio);
						correoRequest.setUsrApp("ONTPFC");

						ResponseGenerico respGen = generarCorreo(correoRequest, headers);
						if (respGen.getAudiResponse().getCodigoRespuesta().equals("0")) {
							String result = "";
							String nombreUsuario = request.getNombrePaciente();
							Map<String, Object> plantilla = (Map<String, Object>) respGen.getDataList().get(0);
							result += (String) plantilla.get("cuerpo");
							Long codEnvio = Long.parseLong(((Integer) plantilla.get("codigoEnvio")).toString());

							result = result.replace("{{codSolEvaluacion}}", request.getNumeroSolEvaluacion());
							result = result.replace("{{paciente}}", request.getNombrePaciente());
							result = result.replace("{{diagnostico}}", request.getNombreDiagnostico());
							result = result.replace("{{codMedicamento}}", request.getCodMac());
							result = result.replace("{{descripcionMac}}", request.getDescripcionCmac());
							result = result.replace("{{medicoAutorizador}}",
									part.getApellidos() + ", " + part.getNombres());

							correoRequest.setAsunto(
									ConstanteUtil.EVALUACION_LIDER_TUMOR_asunto + request.getNombrePaciente());
							correoRequest.setCuerpo(result);
							correoRequest.setCodigoEnvio(codEnvio + "");
							correoRequest.setDestinatario(part.getCorreoElectronico());
							correoRequest.setRuta(rutaDoc);
							correoRequest.setCodigoPlantilla(ConstanteUtil.EVALUACION_LIDER_TUMOR_codigoPlantilla);

							ResponseGenerico respMail = enviarCorreoGenerico(correoRequest, headers);
							if (respMail.getAudiResponse().getCodigoRespuesta().equals("0")) {
								CasosEvaluar eval = new CasosEvaluar();
								eval.setNumSolicitudEvaluacion(request.getCodSolEvaluacion());

								List<CasosEvaluar> listaEva = new ArrayList<>();
								listaEva.add(eval);

								correoRequest.setListaCasosEvaluar(listaEva);

								ApiOutResponse outEnv = emailDao.actualizarEstCorreoLidTumSolEvalucion(correoRequest);
								if (outEnv.getCodResultado().equals(0)) {
									mapResp.put("mensaje", "Envio de correo exitoso, primera vez");
									mapResp.put("codigoEnvio", codEnvio);
								} else {
									mapResp.put("mensaje", "Error envio correo, primera vez (registro estado)");
								}
							} else {
								mapResp.put("mensaje", "Error envio correo, primera vez");
							}
						} else {
							mapResp.put("mensaje", "Error envio correo, primera vez (generar correo)");
						}
					} else {
						mapResp.put("mensaje", "Error envio correo, primera vez (error en adjuntos)");
					}
				} else {
					mapResp.put("mensaje", "Error envio correo, primera vez (cuenta sin email o formato invalido)");
				}
			} else {
				mapResp.put("mensaje", "Error envio correo, primera vez (no hay lider tumor)");
			}
		} else {
			mapResp.put("mensaje", "Error envio correo, primera vez (error ONCOSYS)");
		}
		return mapResp;
	}

	public String procesarReenvioCorreoLiderTumor(ListaBandeja request, HttpHeaders headers) throws Exception {
		ResponseGeneric response = new ResponseGeneric();
		ApiOutResponse outResp = null;

		String respuesta = "";

		String url = prop.getRutaEmail();
		try {
			EmailBean email = new EmailBean();
			email.setCodigoEnvio(request.getCodigoEnvioEnvLiderTumor().toString());
			email.setUsrApp("ONTPFC");
			HttpEntity<EmailBean> rqPcte = new HttpEntity<EmailBean>(email, headers);
			response = restTemplate.postForObject(url + "/verificarEstadoEnvio", rqPcte, ResponseGeneric.class);

			Integer sumaListaCorrectas = 0;
			Integer sumaListaInconclusas = 0;

			List<DetCorreoParticipanteResponse> lista = response.getDataList();
			List<DetCorreoParticipanteResponse> listaErrados = new ArrayList<>();
			if (lista.size() > 0) {
				// RECORRER TODA LA LISTA Y COLOCAR EN UNA LISTA APARTE LOS ESTADO ERRADOS
				for (DetCorreoParticipanteResponse det : lista) {
					if (det.getEstadoEnvio().equals(1)) {
						sumaListaCorrectas++;
					} else {
						if (det.getEstadoEnvio().equals(0)) {
							sumaListaInconclusas++;
						} else {
							listaErrados.add(det);
						}
					}
				}

				// VERIFICAR SI HAY ERRADOS
				if (listaErrados.size() == 0) {
					// REVISAR CUANTAS CONCLUIDAS Y CUANTAS FALTANTES
					if (lista.size() == sumaListaCorrectas) {// TODAS SE ENVIARON => ACTUALIZA ESTADO DE SOL EVA
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvLiderTumor(request.getCodigoEnvioEnvLiderTumor());
						req.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoSI);// ESTADO DE ENVIO CORREO
																								// => SI

						outResp = emailDao.actParamsCorreoLiderTumor(req);
						if (outResp.getCodResultado().equals(0)) {
							respuesta = "Correo ya fue ENVIADO, se actualizo estado";
						} else {
							respuesta = "Correo ya fue ENVIADO, error actualizar estado";
						}

					} else {
						respuesta = "Estado reenvio correos, enviado: " + sumaListaCorrectas + " pendiente: "
								+ sumaListaInconclusas;
					}
				} else {
					Integer ErradoCorregido = 0;
					// FILTRO TODOS LOS ERRADOS SIN REPETICION
					Integer sumaListaReenviados = 0;
					HashMap<String, String> mapErrados = new HashMap<>();
					for (DetCorreoParticipanteResponse detErrados : listaErrados) {
						mapErrados.put(detErrados.getDestinatario(), detErrados.getDestinatario());
					}

					for (Map.Entry<String, String> entry : mapErrados.entrySet()) {
						Boolean hayUnoPendiente = false;
						Boolean hayUnoEnviado = false;
						for (DetCorreoParticipanteResponse det : lista) {
							if (entry.getValue().equals(det.getDestinatario())) {
								if (det.getEstadoEnvio().equals(0)) {
									hayUnoPendiente = true;
								}

								if (det.getEstadoEnvio().equals(1)) {
									hayUnoEnviado = true;
								}
							}
						}

						if (hayUnoEnviado) {// SIGNIFICA QUE DE LA LISTA DE ERRADOS => 1 SE ENVIO
							ErradoCorregido++;
						} else {
							if (!hayUnoPendiente) {
								email.setCodigoEnvio(request.getCodigoEnvioEnvLiderTumor().toString());

								EmailBean bean = emailDao.getDataCorreoReenvio(email);
								bean.setDestinatario(entry.getValue());

								ResponseGenerico respReenvio = reintentarEnvioCorreo(bean, headers);
								if (respReenvio.getAudiResponse().getCodigoRespuesta().equals("0")) {
									System.out.println("Reenvio");
									sumaListaReenviados++;
								}
							}
						}
					}

					if ((mapErrados.size() == ErradoCorregido) && (sumaListaInconclusas == 0)) {
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvLiderTumor(request.getCodigoEnvioEnvLiderTumor());
						req.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoSI);// ESTADO DE ENVIO CORREO
																								// => SI

						outResp = emailDao.actParamsCorreoLiderTumor(req);
						if (outResp.getCodResultado().equals(0)) {
							respuesta = "Correo ya fue ENVIADO, se actualizo estado";
						} else {
							respuesta = "Correo ya fue ENVIADO, error actualizar estado";
						}
					} else {
						// PARA ACTUALIZAR ESTADO
						if (sumaListaReenviados > 0) {
							SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
							req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
							req.setCodigoEnvioEnvLiderTumor(request.getCodigoEnvioEnvLiderTumor());
							req.setEstadoCorreoEnvLiderTumor(0);// ESTADO PENDIENTE

							emailDao.actParamsCorreoLiderTumor(req);
						}
						respuesta = "Estado reenvio correos, enviado:" + sumaListaCorrectas + " pendiente:"
								+ sumaListaInconclusas + " errado:" + (mapErrados.size() - ErradoCorregido)
								+ "~reenviado: " + sumaListaReenviados;
					}
				}
			} else {
				respuesta = "Error no se encontraron registros de correos";
			}
		} catch (Exception e) {
			respuesta = "Exception en reenvio de correo";
		}
		return respuesta;
	}

	@Override
	public String reenviarCorreoCmac(ListaBandeja request, HttpHeaders headers) throws Exception {
		String mensaje = "";
		try {
			if (request.getEstadoCorreoEnvCmac().equals(ConstanteUtil.estadoCorreoEnviadoSI)) {
				mensaje = "Correo ENVIADO";
			} else {
				if (request.getCodigoEnvioEnvMac().equals(0L)) {// NO SE ENVIO EMAIL EN NINGUN MOMENTO
					Map<String, Object> mapa = enviarEmailCmacPrimeraVez(request, headers);// ENVIO PRIMERA VEZ
					Long codEnvioGenerado = (Long) mapa.get("codigoEnvio");
					if (codEnvioGenerado != null) {
						mensaje = mapa.get("mensaje").toString();
					} else {
						mensaje = mapa.get("mensaje").toString();
					}
				} else {// NO SE ENVIO => REENVIAR
					mensaje = procesarReenvioCorreoCmac(request, headers);
				}
			}
		} catch (Exception e) {
			mensaje = "Exception en reenvio de correo";
		}
		return mensaje;
	}

	public Map<String, Object> enviarEmailCmacPrimeraVez(ListaBandeja request, HttpHeaders headers) throws Exception {
		Map<String, Object> mapResp = new HashMap<>();
		mapResp.put("codigoEnvio", null);

		List<CasosEvaluar> listSolEva = emailDao
				.listarSolEvalucionPorCodActa(Integer.parseInt(request.getCodSolEvaluacion()));
		if (listSolEva != null) {
			if (listSolEva.size() > 0) {
				// LISTAR MIEMBROS CMAC
				ParticipanteRequest req = new ParticipanteRequest();
				req.setCodRol(Long.parseLong(ConstanteUtil.COD_ROL_MIEMBRO_CMAC + ""));

				List<ParticipanteResponse> lista = (List<ParticipanteResponse>) lstUsuarioDao.listarUsuarioFarmacia(req)
						.getResponse();
				if (lista.size() > 0) {
					EmailBean correoRequest = new EmailBean();
					correoRequest.setCodPlantilla(ConstanteUtil.EVALUACION_PROGRAMAR_CMAC_codigoPlantilla);
					correoRequest.setFechaProgramada(DateUtils.getDateToStringDDMMYYYHHMMSS(new Date()));
					correoRequest.setFlagAdjunto(ConstanteUtil.EVALUACION_PROGRAMAR_CMAC_flagAdjunto);
					correoRequest.setTipoEnvio(ConstanteUtil.EVALUACION_PROGRAMAR_CMAC_tipoEnvio);
					correoRequest.setUsrApp("ONTPFC");

					ResponseGenerico respGen = generarCorreo(correoRequest, headers);
					if (respGen.getAudiResponse().getCodigoRespuesta().equals("0")) {
						String result = "";
						Map<String, Object> plantilla = (Map<String, Object>) respGen.getDataList().get(0);
						Long codEnvio = Long.parseLong(((Integer) plantilla.get("codigoEnvio")).toString());

						String html = "<table border='0' cellpadding='0' cellspacing='0' class='grilla'>";
						html += "<tr><th>N° Solicitud de Evaluación</th><th>Paciente</th><th>Diagnóstico</th><th>Código Medicamento</th><th>Medicamento Solicitado</th><th>Fecha Programada de CMAC</th></tr>";
						List<CasosEvaluar> listSeleccionada = new ArrayList<>();

						// COMPLETA DATOS DE DIAGNOSTICO Y NOMBRE PACIENTE
						completarPacienteYDiagnostico(listSeleccionada, headers);
						for (CasosEvaluar c : listSolEva) {
							listSeleccionada.add(
									new CasosEvaluar(c.getCodSolEva().toString(), c.getPaciente(), c.getDiagnostico(),
											c.getCodigoMedicamento(), c.getMedicamentoSolicitado(), c.getFechaMac()));
							html += "<tr><td>" + c.getNumSolicitudEvaluacion() + "</td><td>" + c.getPaciente()
									+ "</td><td>" + c.getDiagnostico() + "</td><td>" + c.getCodigoMedicamento()
									+ "</td><td>" + c.getMedicamentoSolicitado() + "</td><td>" + c.getFechaMac()
									+ "</td></tr>";
						}

						html += "</table>";
						result += (String) plantilla.get("cuerpo");
						result = result.replace("{{grilla}}", html);
						result = result.replace("{{nomMedico}}", "");

						correoRequest.setAsunto(
								ConstanteUtil.EVALUACION_PROGRAMAR_CMAC_asunto + " " + listSolEva.get(0).getFechaMac());
						correoRequest.setCuerpo(result);
						correoRequest.setRuta("");
						correoRequest.setCodigoPlantilla(ConstanteUtil.EVALUACION_PROGRAMAR_CMAC_codigoPlantilla);
						correoRequest.setCodigoEnvio(codEnvio + "");
						correoRequest.setListaCasosEvaluar(listSeleccionada);

						String destinatarioTodos = "";

						for (ParticipanteResponse par : lista) {
							if (par.getCorreoElectronico() != null) {
								destinatarioTodos += par.getCorreoElectronico();
							}
						}

						for (ParticipanteResponse par : lista) {
							if (par.getCorreoElectronico() != null) {
								correoRequest.setDestinatario(par.getCorreoElectronico());

								ResponseGenerico respCorreo = enviarCorreoReunionMac(correoRequest, headers);
								if (respCorreo.getAudiResponse().getCodigoRespuesta().equals("0")) {
									LOGGER.info("SE ENVIO CORREO CMAC: " + correoRequest.toString());
								} else {
									LOGGER.info("ERROR DE ENVIO CORREO CMAC: " + correoRequest.toString());
								}
							} else {
								System.out.println("Usuario no cuenta con email: " + par.toString());
							}
						}
						// AQUI
						correoRequest.setDestinatario(destinatarioTodos);
						ApiOutResponse outEnv = actualizarEstCorreoSolEvaluacion(correoRequest);// VALIDAR
																								// RESPOUESTA
						if (outEnv.getCodResultado().equals(0)) {
							mapResp.put("mensaje", "Envio de correo exitoso, primera vez");
							mapResp.put("codigoEnvio", codEnvio);
						} else {
							mapResp.put("mensaje", "Error envio correo, primera vez (registro estado)");
						}
					} else {
						mapResp.put("mensaje", "Error envio correo, primera vez (generar correo)");
					}
				} else {
					mapResp.put("mensaje", "Error envio correo, primera vez (no hay miembros CMAC)");
				}
			} else {
				mapResp.put("mensaje", "Error envio correo, primera vez (sin lista solicitudes)");
			}
		} else {
			mapResp.put("mensaje", "Error envio correo, primera vez (sin program. cmac)");
		}
		return mapResp;
	}

	public String procesarReenvioCorreoCmac(ListaBandeja request, HttpHeaders headers) throws Exception {
		ApiOutResponse outResp = new ApiOutResponse();
		ResponseGeneric response = new ResponseGeneric();
		String respuesta = "";

		String urlPcte = prop.getRutaEmail();
		try {
			EmailBean email = new EmailBean();
			email.setCodigoEnvio(request.getCodigoEnvioEnvMac().toString());
			email.setUsrApp("ONTPFC");
			HttpEntity<EmailBean> rqPcte = new HttpEntity<EmailBean>(email, headers);
			response = restTemplate.postForObject(urlPcte + "/verificarEstadoEnvio", rqPcte, ResponseGeneric.class);

			Integer sumaListaCorrectas = 0;
			Integer sumaListaInconclusas = 0;
			List<DetCorreoParticipanteResponse> lista = response.getDataList();
			List<DetCorreoParticipanteResponse> listaErrados = new ArrayList<>();
			if (lista.size() > 0) {
				// RECORRER TODA LA LISTA Y COLOCAR EN UNA LISTA APARTE LOS ESTADO ERRADOS
				for (DetCorreoParticipanteResponse det : lista) {
					if (det.getEstadoEnvio().equals(1)) {
						sumaListaCorrectas++;
					} else {
						if (det.getEstadoEnvio().equals(0)) {
							sumaListaInconclusas++;
						} else {
							listaErrados.add(det);
						}
					}
				}

				if (listaErrados.size() == 0) {
					// REVISAR CUANTAS CONCLUIDAS Y CUANTAS FALTANTES
					if (lista.size() == sumaListaCorrectas) {// TODAS SE ENVIARON => ACTUALIZA ESTADO DE SOL EVA
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvMac(request.getCodigoEnvioEnvMac());
						req.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoSI);

						outResp = emailDao.actParamsCorreoCmacV2(req);

						if (outResp.getCodResultado().equals(0)) {
							respuesta = "Correo ya fue ENVIADO, se actualizo estado";
						} else {
							respuesta = "Correo ya fue ENVIADO, error actualizar estado";
						}
					} else {
						respuesta = "Estado reenvio correos, enviado: " + sumaListaCorrectas + " pendiente: "
								+ sumaListaInconclusas;
					}
				} else {
					Integer ErradoCorregido = 0;
					Integer sumaListaReenviados = 0;

					HashMap<String, String> mapErrados = new HashMap<>();
					for (DetCorreoParticipanteResponse detErrados : listaErrados) {
						mapErrados.put(detErrados.getDestinatario(), detErrados.getDestinatario());
					}

					for (Map.Entry<String, String> entry : mapErrados.entrySet()) {// RECORRER LA LISTA Y VERIFICAR SI
						Boolean hayUnoPendiente = false; // SI EL PRODUCTO SALE CERO SIGNIFICA QUE HAY UNO PENDIENTE =>
						Boolean hayUnoEnviado = false;

						for (DetCorreoParticipanteResponse det : lista) {
							if (entry.getValue().equals(det.getDestinatario())) {
								if (det.getEstadoEnvio().equals(0)) {
									hayUnoPendiente = true;
								}

								if (det.getEstadoEnvio().equals(1)) {
									hayUnoEnviado = true;
								}
							}
						}

						if (hayUnoEnviado) {// SIGNIFICA QUE DE LA LISTA DE ERRADOS => 1 SE ENVIO
							ErradoCorregido++;
						} else {
							if (!hayUnoPendiente) {
								email.setCodigoEnvio(request.getCodigoEnvioEnvMac().toString());

								EmailBean bean = emailDao.getDataCorreoReenvio(email);
								bean.setDestinatario(entry.getValue());

								ResponseGenerico respReenvio = reintentarEnvioCorreo(bean, headers);
								if (respReenvio.getAudiResponse().getCodigoRespuesta().equals("0")) {
									LOGGER.info("SE REENVIO UN EMAIL: " + bean.toString());
									sumaListaReenviados++;
								}
							}
						}
					}

					if ((mapErrados.size() == ErradoCorregido) && (sumaListaInconclusas == 0)) {
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvMac(request.getCodigoEnvioEnvMac());
						req.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoSI);// ESTADO DE ENVIO CORREO => SI

						outResp = emailDao.actParamsCorreoCmacV2(req);
						if (outResp.getCodResultado().equals(0)) {
							respuesta = "Correo ya fue ENVIADO, se actualizo estado";
						} else {
							respuesta = "Correo ya fue ENVIADO, error actualizar estado";
						}
					} else {
						// PARA ACTUALIZAR ESTADO
						if (sumaListaReenviados > 0) {
							SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
							req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
							req.setCodigoEnvioEnvMac(request.getCodigoEnvioEnvMac());
							req.setEstadoCorreoEnvCmac(0);// PENDIENTE

							outResp = emailDao.actParamsCorreoCmacV2(req);
						}
						respuesta = "Estado reenvio correos, enviado:" + sumaListaCorrectas + " pendiente:"
								+ sumaListaInconclusas + " errado:" + (mapErrados.size() - ErradoCorregido)
								+ "~reenviado: " + sumaListaReenviados;
					}
				}
			} else {
				respuesta = "Error no se encontraron registros de correos";
			}
		} catch (Exception e) {
			respuesta = "Exception en reenvio de correo";
		}
		return respuesta;
	}

	@Override
	public ResponseGenerico enviarCorreoGenerico(EmailBean obj, HttpHeaders headers) {
		ResponseGenerico response = null;
		HttpEntity<EmailBean> request = null;

		try {
			request = new HttpEntity<EmailBean>(obj, headers);
			String url = prop.getRutaEmail() + "/finalizarEnvio";
			response = restTemplate.postForObject(url, request, ResponseGenerico.class);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			response = null;
		}
		return response;
	}

	@Override
	public ResponseGenerico reintentarEnvioCorreo(EmailBean obj, HttpHeaders headers) {
		ResponseGenerico response = null;
		HttpEntity<EmailBean> request = null;

		try {
			request = new HttpEntity<EmailBean>(obj, headers);
			String url = prop.getRutaEmail() + "/reintentoEnvio";
			response = restTemplate.postForObject(url, request, ResponseGenerico.class);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			response = null;
		}
		return response;
	}

	public Long reenviarEmailLiderTumor(ListaBandeja request, HttpHeaders headers) throws Exception {
		ResponseOnc onco = monitoreoService.consultaOncosysDatosDiag(request.getCodigoDiagnostico(), headers);

		if (onco.getDataList().size() > 0) {
			HashMap<?, ?> map = (HashMap<?, ?>) onco.getDataList().get(0);
			ParticipanteRequest req = new ParticipanteRequest();
			req.setCodRol(8L);// LIDER TUMOR
			req.setCodGrpDiag(map.get("codgru").toString());

			List lista = (List<ParticipanteResponse>) lstUsuarioDao.listarUsuarioFarmacia(req).getResponse();
			// CONTINUA

			return null;
		} else {
			throw new Exception();
		}
	}

	@Override
	public ApiOutResponse actualizarEstSolicitudEvaluacion(EmailBean request) {
		return emailDao.actualizarEstadoSolicitud(request);
	}

	@Override
	public ApiOutResponse actualizarEstCorreoSolEvaluacion(EmailBean request) {
		String solicitudes = "";
		if (request.getListaCasosEvaluar().size() > 0) {
			for (CasosEvaluar em : request.getListaCasosEvaluar()) {
				solicitudes += em.getNumSolicitudEvaluacion() + "," + 0 + "|";// 0=> PENDIENTE ENVIO
			}
			solicitudes = solicitudes.substring(0, solicitudes.length() - 1);
			System.out.println(solicitudes);
		}
		return emailDao.actualizarEstCorreoSolEvaluacion(request, solicitudes);
	}

	@Override
	public ApiOutResponse actualizarEstCorreoLidTumSolEvalucion(EmailBean request) {
		return emailDao.actualizarEstCorreoLidTumSolEvalucion(request);
	}

	@Override
	public String actualizarCorreosPendCmac(ListaBandeja request, HttpHeaders headers) throws Exception {
		String mensaje = "";
		try {
			if (request.getEstadoCorreoEnvCmac().equals(ConstanteUtil.estadoCorreoEnviadoSI)) {
				mensaje = "Correo ENVIADO";
			} else {
				if (request.getEstadoCorreoEnvCmac().equals(0)) {// NO SE ENVIO EMAIL EN NINGUN MOMENTO
					mensaje = "No hay ningun correo en proceso de envio";
				} else {// NO SE ENVIO => REENVIAR
					mensaje = procesarActualizacionEstadoCorreoCmac(request, headers);
				}
			}
		} catch (Exception e) {
			mensaje = "Error al validar estado correo";
		}
		return mensaje;
	}

	@Override
	public void actualizarCorreosPendCmacV2(ListaBandeja request, HttpHeaders headers) throws Exception {
		ApiOutResponse outResp = new ApiOutResponse();
		ResponseGeneric response = new ResponseGeneric();
		String urlPcte = prop.getRutaEmail();
		try {
			EmailBean email = new EmailBean();
			email.setCodigoEnvio(request.getCodigoEnvioEnvMac().toString());
			email.setUsrApp("ONTPFC");
			HttpEntity<EmailBean> rqPcte = new HttpEntity<EmailBean>(email, headers);
			response = restTemplate.postForObject(urlPcte + "/verificarEstadoEnvio", rqPcte, ResponseGeneric.class);

			Integer sumaListaCorrectas = 0;
			Integer sumaListaInconclusas = 0;
			List<DetCorreoParticipanteResponse> lista = response.getDataList();
			List<DetCorreoParticipanteResponse> listaErrados = new ArrayList<>();
			if (lista.size() > 0) {
				for (DetCorreoParticipanteResponse det : lista) {
					if (det.getEstadoEnvio().equals(1)) {
						sumaListaCorrectas++;
					} else {
						if (det.getEstadoEnvio().equals(0)) {
							sumaListaInconclusas++;
						} else {
							listaErrados.add(det);
						}
					}
				}

				if (listaErrados.size() == 0) {
					// REVISAR CUANTAS CONCLUIDAS Y CUANTAS FALTANTES
					if (lista.size() == sumaListaCorrectas) {// TODAS SE ENVIARON => ACTUALIZA ESTADO DE SOL EVA
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvMac(request.getCodigoEnvioEnvMac());
						req.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoSI);

						outResp = emailDao.actParamsCorreoCmac(req);

						if (outResp.getCodResultado().equals(0)) {
							request.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoSI);
						} else {
							request.setEstadoCorreoEnvCmac(0);
						}
					} else {
						request.setEstadoCorreoEnvCmac(0);
					}
				} else {
					Integer ErradoCorregido = 0;
					Integer ErradoSinCorregido = 0;

					HashMap<String, String> mapErrados = new HashMap<>();
					for (DetCorreoParticipanteResponse detErrados : listaErrados) {
						mapErrados.put(detErrados.getDestinatario(), detErrados.getDestinatario());
					}

					for (Map.Entry<String, String> entry : mapErrados.entrySet()) {// RECORRER LA LISTA Y VERIFICAR SI
																					// SE HIZO UN REENVIO
						Boolean hayUnoPendiente = false; // SI EL PRODUCTO SALE CERO SIGNIFICA QUE HAY UNO PENDIENTE =>
						Boolean hayUnoEnviado = false;

						for (DetCorreoParticipanteResponse det : lista) {
							if (entry.getValue().equals(det.getDestinatario())) {
								if (det.getEstadoEnvio().equals(0)) {
									hayUnoPendiente = true;
								}

								if (det.getEstadoEnvio().equals(1)) {
									hayUnoEnviado = true;
								}
							}
						}

						if (hayUnoEnviado) {// SIGNIFICA QUE DE LA LISTA DE ERRADOS => 1 SE ENVIO
							ErradoCorregido++;
						} else {
							if (!hayUnoPendiente) {
								ErradoSinCorregido++;
							}
						}
					}

					if ((mapErrados.size() == ErradoCorregido) && (sumaListaInconclusas == 0)) {
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvMac(request.getCodigoEnvioEnvMac());
						req.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoSI);// ESTADO DE ENVIO CORREO => SI

						outResp = emailDao.actParamsCorreoCmac(req);
						if (outResp.getCodResultado().equals(0)) {
							request.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoSI);
						} else {
							request.setEstadoCorreoEnvCmac(0);
						}
					} else {
						if ((ErradoSinCorregido > 0) && (sumaListaInconclusas == 0)) {// TIENE ERRADOS
							SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
							req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
							req.setCodigoEnvioEnvMac(request.getCodigoEnvioEnvMac());
							req.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoNO);// ESTADO DE ENVIO CORREO =>
																							// SI

							outResp = emailDao.actParamsCorreoCmac(req);
							if (outResp.getCodResultado().equals(0)) {
								request.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoNO);
							} else {
								request.setEstadoCorreoEnvCmac(0);
							}
						} else {
							request.setEstadoCorreoEnvCmac(0);
						}
					}
				}
			} else {
				request.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoNO);
			}
		} catch (Exception e) {
			request.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoNO);
		}
	}

	public String procesarActualizacionEstadoCorreoCmac(ListaBandeja request, HttpHeaders headers) throws Exception {
		ApiOutResponse outResp = new ApiOutResponse();
		ResponseGeneric response = new ResponseGeneric();
		String respuesta = "";

		String urlPcte = prop.getRutaEmail();
		try {
			EmailBean email = new EmailBean();
			email.setCodigoEnvio(request.getCodigoEnvioEnvMac().toString());
			email.setUsrApp("ONTPFC");
			HttpEntity<EmailBean> rqPcte = new HttpEntity<EmailBean>(email, headers);
			response = restTemplate.postForObject(urlPcte + "/verificarEstadoEnvio", rqPcte, ResponseGeneric.class);

			Integer sumaListaCorrectas = 0;
			Integer sumaListaInconclusas = 0;
			List<DetCorreoParticipanteResponse> lista = response.getDataList();
			List<DetCorreoParticipanteResponse> listaErrados = new ArrayList<>();
			if (lista.size() > 0) {
				for (DetCorreoParticipanteResponse det : lista) {
					if (det.getEstadoEnvio().equals(1)) {
						sumaListaCorrectas++;
					} else {
						if (det.getEstadoEnvio().equals(0)) {
							sumaListaInconclusas++;
						} else {
							listaErrados.add(det);
						}
					}
				}

				if (listaErrados.size() == 0) {
					// REVISAR CUANTAS CONCLUIDAS Y CUANTAS FALTANTES
					if (lista.size() == sumaListaCorrectas) {// TODAS SE ENVIARON => ACTUALIZA ESTADO DE SOL EVA
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvMac(request.getCodigoEnvioEnvMac());
						req.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoSI);

						outResp = emailDao.actParamsCorreoCmac(req);

						if (outResp.getCodResultado().equals(0)) {
							respuesta = "El estado de correo se ha actualizado a ENVIADO";
						} else {
							respuesta = "Error al actualizar estado de correo";
						}
					} else {
						outResp.setMsgResultado("Estado de correos, enviado: " + sumaListaCorrectas + " pendiente: "
								+ sumaListaInconclusas);
					}
				} else {
					Integer ErradoCorregido = 0;

					HashMap<String, String> mapErrados = new HashMap<>();
					for (DetCorreoParticipanteResponse detErrados : listaErrados) {
						mapErrados.put(detErrados.getDestinatario(), detErrados.getDestinatario());
					}

					for (Map.Entry<String, String> entry : mapErrados.entrySet()) {// RECORRER LA LISTA Y VERIFICAR SI
																					// SE HIZO UN REENVIO
						Boolean hayUnoPendiente = false; // SI EL PRODUCTO SALE CERO SIGNIFICA QUE HAY UNO PENDIENTE =>
						Boolean hayUnoEnviado = false;

						for (DetCorreoParticipanteResponse det : lista) {
							if (entry.getValue().equals(det.getDestinatario())) {
								if (det.getEstadoEnvio().equals(0)) {
									hayUnoPendiente = true;
								}

								if (det.getEstadoEnvio().equals(1)) {
									hayUnoEnviado = true;
								}
							}
						}

						if (hayUnoEnviado) {// SIGNIFICA QUE DE LA LISTA DE ERRADOS => 1 SE ENVIO
							ErradoCorregido++;
						} else {
							if (!hayUnoPendiente) {
								// DEBERIA SER REENVIADO
							}
						}
					}

					if (mapErrados.size() == ErradoCorregido) {
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvMac(request.getCodigoEnvioEnvMac());
						req.setEstadoCorreoEnvCmac(ConstanteUtil.estadoCorreoEnviadoSI);// ESTADO DE ENVIO CORREO => SI

						outResp = emailDao.actParamsCorreoCmac(req);
						if (outResp.getCodResultado().equals(0)) {
							respuesta = "El estado de correo se ha actualizado a ENVIADO";
						} else {
							respuesta = "Error al actualizar estado de correo";
						}
					} else {
						respuesta = "Estado correos, enviado: " + sumaListaCorrectas + " pendiente: "
								+ sumaListaInconclusas + " Errado: " + (mapErrados.size() - ErradoCorregido);
					}
				}
			} else {
				respuesta = "Error no se encontraron registros de correos";
			}
		} catch (Exception e) {
			respuesta = "Exception al validar estado correo";
		}
		return respuesta;
	}

	@Override
	public String actualizarCorreosPendLiderTumor(ListaBandeja request, HttpHeaders headers) {
		String mensaje = "";
		try {
			if (request.getEstadoCorreoEnvLiderTumor() == ConstanteUtil.estadoCorreoEnviadoSI) {
				mensaje = "Correo ENVIADO";
			} else {
				if (request.getEstadoCorreoEnvLiderTumor() == 0) {
					mensaje = "No hay ningun correo en proceso de envio";
				} else {// NO SE ENVIO => REENVIAR
					mensaje = procesarActualizacionEstadoCorreoLiderTumor(request, headers);
				}
			}
		} catch (Exception e) {
			mensaje = "Error al validar estado correo";
		}
		return mensaje;
	}

	@Override
	public void actualizarCorreosPendLiderTumorV2(ListaBandeja request, HttpHeaders headers) {
		ResponseGeneric response = new ResponseGeneric();
		ApiOutResponse outResp = null;
		String url = prop.getRutaEmail();
		try {
			EmailBean email = new EmailBean();
			email.setCodigoEnvio(request.getCodigoEnvioEnvLiderTumor().toString());
			email.setUsrApp("ONTPFC");
			HttpEntity<EmailBean> rqPcte = new HttpEntity<EmailBean>(email, headers);
			response = restTemplate.postForObject(url + "/verificarEstadoEnvio", rqPcte, ResponseGeneric.class);

			Integer sumaListaCorrectas = 0;
			Integer sumaListaInconclusas = 0;

			List<DetCorreoParticipanteResponse> lista = response.getDataList();
			List<DetCorreoParticipanteResponse> listaErrados = new ArrayList<>();
			if (lista.size() > 0) {
				// RECORRER TODA LA LISTA Y COLOCAR EN UNA LISTA APARTE LOS ESTADO ERRADOS
				for (DetCorreoParticipanteResponse det : lista) {
					if (det.getEstadoEnvio().equals(1)) {
						sumaListaCorrectas++;
					} else {
						if (det.getEstadoEnvio().equals(0)) {
							sumaListaInconclusas++;
						} else {
							listaErrados.add(det);
						}
					}
				}

				// VERIFICAR SI HAY ERRADOS
				if (listaErrados.size() == 0) {
					// REVISAR CUANTAS CONCLUIDAS Y CUANTAS FALTANTES
					if (lista.size() == sumaListaCorrectas) {// TODAS SE ENVIARON => ACTUALIZA ESTADO DE SOL EVA
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvLiderTumor(request.getCodigoEnvioEnvLiderTumor());
						req.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoSI);// ESTADO DE ENVIO CORREO
																								// => SI

						outResp = emailDao.actParamsCorreoLiderTumor(req);
						if (outResp.getCodResultado().equals(0)) {
							request.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoSI);
						} else {
							request.setEstadoCorreoEnvLiderTumor(0);
						}
					} else {
						request.setEstadoCorreoEnvLiderTumor(0);
					}
				} else {
					Integer ErradoCorregido = 0;
					Integer ErradoSinCorregido = 0;

					// FILTRO TODOS LOS ERRADOS SIN REPETICION
					HashMap<String, String> mapErrados = new HashMap<>();
					for (DetCorreoParticipanteResponse detErrados : listaErrados) {
						mapErrados.put(detErrados.getDestinatario(), detErrados.getDestinatario());
					}

					for (Map.Entry<String, String> entry : mapErrados.entrySet()) {
						Boolean hayUnoPendiente = false;
						Boolean hayUnoEnviado = false;
						for (DetCorreoParticipanteResponse det : lista) {
							if (entry.getValue().equals(det.getDestinatario())) {
								if (det.getEstadoEnvio().equals(0)) {
									hayUnoPendiente = true;
								}

								if (det.getEstadoEnvio().equals(1)) {
									hayUnoEnviado = true;
								}
							}
						}

						if (hayUnoEnviado) {// SIGNIFICA QUE DE LA LISTA DE ERRADOS => 1 SE ENVIO
							ErradoCorregido++;
						} else {
							if (!hayUnoPendiente) {
								// DEBERIA SER REENVIADO
								ErradoSinCorregido++;
							}
						}
					}

					if ((mapErrados.size() == ErradoCorregido) && (sumaListaInconclusas == 0)) {
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvLiderTumor(request.getCodigoEnvioEnvLiderTumor());
						req.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoSI);// ESTADO DE ENVIO CORREO
																								// => SI

						outResp = emailDao.actParamsCorreoLiderTumor(req);
						if (outResp.getCodResultado().equals(0)) {
							request.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoSI);
						} else {
							request.setEstadoCorreoEnvLiderTumor(0);
						}
					} else {
						if ((ErradoSinCorregido > 0) && (sumaListaInconclusas == 0)) {// TIENE ERRADOS
							SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
							req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
							req.setCodigoEnvioEnvLiderTumor(request.getCodigoEnvioEnvLiderTumor());
							req.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoNO);// ESTADO DE ENVIO
																									// CORREO
																									// => SI

							outResp = emailDao.actParamsCorreoLiderTumor(req);
							if (outResp.getCodResultado().equals(0)) {
								request.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoNO);
							} else {
								request.setEstadoCorreoEnvLiderTumor(0);
							}
						} else {
							request.setEstadoCorreoEnvCmac(0);
						}
					}
				}
			} else {
				request.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoNO);
			}
		} catch (Exception e) {
			request.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoNO);
		}
	}

	public String procesarActualizacionEstadoCorreoLiderTumor(ListaBandeja request, HttpHeaders headers)
			throws Exception {
		ResponseGeneric response = new ResponseGeneric();
		ApiOutResponse outResp = null;

		String respuesta = "";

		String url = prop.getRutaEmail();
		try {
			EmailBean email = new EmailBean();
			email.setCodigoEnvio(request.getCodigoEnvioEnvLiderTumor().toString());
			email.setUsrApp("ONTPFC");
			HttpEntity<EmailBean> rqPcte = new HttpEntity<EmailBean>(email, headers);
			response = restTemplate.postForObject(url + "/verificarEstadoEnvio", rqPcte, ResponseGeneric.class);

			Integer sumaListaCorrectas = 0;
			Integer sumaListaInconclusas = 0;

			List<DetCorreoParticipanteResponse> lista = response.getDataList();
			List<DetCorreoParticipanteResponse> listaErrados = new ArrayList<>();
			if (lista.size() > 0) {
				// RECORRER TODA LA LISTA Y COLOCAR EN UNA LISTA APARTE LOS ESTADO ERRADOS
				for (DetCorreoParticipanteResponse det : lista) {
					if (det.getEstadoEnvio().equals(1)) {
						sumaListaCorrectas++;
					} else {
						if (det.getEstadoEnvio().equals(0)) {
							sumaListaInconclusas++;
						} else {
							listaErrados.add(det);
						}
					}
				}

				// VERIFICAR SI HAY ERRADOS
				if (listaErrados.size() == 0) {
					// REVISAR CUANTAS CONCLUIDAS Y CUANTAS FALTANTES
					if (lista.size() == sumaListaCorrectas) {// TODAS SE ENVIARON => ACTUALIZA ESTADO DE SOL EVA
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvLiderTumor(request.getCodigoEnvioEnvLiderTumor());
						req.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoSI);// ESTADO DE ENVIO CORREO
																								// => SI

						outResp = emailDao.actParamsCorreoLiderTumor(req);
						if (outResp.getCodResultado().equals(0)) {
							respuesta = "El estado de correo se ha actualizado a ENVIADO";
						} else {
							respuesta = "Error al actualizar estado de correo";
						}

					} else {
						respuesta = "Estado de correos, enviado: " + sumaListaCorrectas + " pendiente: "
								+ sumaListaInconclusas;
					}
				} else {
					Integer ErradoCorregido = 0;

					// FILTRO TODOS LOS ERRADOS SIN REPETICION
					HashMap<String, String> mapErrados = new HashMap<>();
					for (DetCorreoParticipanteResponse detErrados : listaErrados) {
						mapErrados.put(detErrados.getDestinatario(), detErrados.getDestinatario());
					}

					for (Map.Entry<String, String> entry : mapErrados.entrySet()) {
						Boolean hayUnoPendiente = false;
						Boolean hayUnoEnviado = false;
						for (DetCorreoParticipanteResponse det : lista) {
							if (entry.getValue().equals(det.getDestinatario())) {
								if (det.getEstadoEnvio().equals(0)) {
									hayUnoPendiente = true;
								}

								if (det.getEstadoEnvio().equals(1)) {
									hayUnoEnviado = true;
								}
							}
						}

						if (hayUnoEnviado) {// SIGNIFICA QUE DE LA LISTA DE ERRADOS => 1 SE ENVIO
							ErradoCorregido++;
						} else {
							if (!hayUnoPendiente) {
								// DEBERIA SER REENVIADO
							}
						}
					}

					if (mapErrados.size() == ErradoCorregido) {
						SolicitudEvaluacionRequest req = new SolicitudEvaluacionRequest();
						req.setCodSolicitudEvaluacion(Integer.parseInt(request.getCodSolEvaluacion()));
						req.setCodigoEnvioEnvLiderTumor(request.getCodigoEnvioEnvLiderTumor());
						req.setEstadoCorreoEnvLiderTumor(ConstanteUtil.estadoCorreoEnviadoSI);// ESTADO DE ENVIO CORREO
																								// => SI

						outResp = emailDao.actParamsCorreoLiderTumor(req);
						if (outResp.getCodResultado().equals(0)) {
							respuesta = "El estado de correo se ha actualizado a ENVIADO";
						} else {
							respuesta = "Error al actualizar estado de correo";
						}
					} else {
						respuesta = "Estado correos, enviado: " + sumaListaCorrectas + " pendiente: "
								+ sumaListaInconclusas + " Errado: " + (mapErrados.size() - ErradoCorregido);
					}
				}
			} else {
				respuesta = "Error no se encontraron registros de correos";
			}
		} catch (Exception e) {
			respuesta = "Exception al validar estado correo";
		}
		return respuesta;
	}

	public void completarPacienteYDiagnostico(List<CasosEvaluar> lCasos, HttpHeaders headers) {
		String codigoAfiliado = "";
		String codigoDiagnostico = "";

		Integer total = 0;
		Integer totalCli = 0;
		Integer totalDia = 0;
		for (CasosEvaluar obj : lCasos) {
			if (codigoAfiliado.indexOf(obj.getCodAfipaciente()) == -1) {
				total++;
				codigoAfiliado += "|" + obj.getCodAfipaciente();
			}

			if (codigoDiagnostico.indexOf(obj.getCodDiagnostico()) == -1) {
				totalDia++;
				codigoDiagnostico += "|" + obj.getCodDiagnostico();
			}
		}

		ArrayList<PacienteBean> listaPaciente = obtenerNombresAfiliados(codigoAfiliado, total, headers);
		if (listaPaciente != null && !listaPaciente.isEmpty()) {
			for (CasosEvaluar pre : lCasos) {
				for (PacienteBean pcte : listaPaciente) {
					if (pre.getCodAfipaciente().equalsIgnoreCase(pcte.getCodafir())) {
						pre.setPaciente(pcte.getApelNomb());
						break;
					}
				}
			}
			LOGGER.info("[SERVICIO:  REENVIAR CORREOS CMAC][REQUEST][INFO][Paciente obtenidos exitosamente]");
		} else {
			LOGGER.info("[SERVICIO:  REENVIAR CORREOS CMAC][REQUEST][INFO][Eror datos de paciente]");
		}

		ArrayList<DiagnosticoBean> listaDiagnostico = obtenerNombresDiagnosticos(codigoDiagnostico, total, headers);
		if (listaDiagnostico != null && !listaDiagnostico.isEmpty()) {
			for (CasosEvaluar pre : lCasos) {
				for (DiagnosticoBean diagnostico : listaDiagnostico) {
					if (pre.getCodDiagnostico().equalsIgnoreCase(diagnostico.getCodigo())) {
						pre.setDiagnostico(diagnostico.getDiagnostico());
						break;
					}
				}
			}
			LOGGER.info("[SERVICIO:  REENVIAR CORREOS CMAC][RESPONSE][INFO][Diagnósticos obtenidos exitosamente]");
		} else {
			LOGGER.info("[SERVICIO:  REENVIAR CORREOS CMAC][RESPONSE][INFO][Error datos diagnostico]");
		}
	}

	private ArrayList<PacienteBean> obtenerNombresAfiliados(String codigoAfiliado, Integer total, HttpHeaders headers) {
		ArrayList<PacienteBean> listaPaciente = null;

		AfiliadosRequest pacienteRequest = new AfiliadosRequest();
		pacienteRequest.setIni(ConstanteUtil.registroIni);
		pacienteRequest.setFin(total + 1);
		pacienteRequest.setCodafir(codigoAfiliado);

		try {
			ResponseOnc pacienteRes = pcteService.obtenerListaPacientexCodigos(pacienteRequest, headers);

			if (pacienteRes != null && Integer.parseInt(pacienteRes.getAudiResponse().getCodigoRespuesta()) >= 0) {
				listaPaciente = (ArrayList<PacienteBean>) pacienteRes.getDataList();
			} else {
				listaPaciente = null;
			}

		} catch (Exception e) {
			LOGGER.error(this.getClass().getName() + ".obtenerNombresAfiliados: " + e.getMessage());
			listaPaciente = null;
		}

		return listaPaciente;
	}

	private ArrayList<DiagnosticoBean> obtenerNombresDiagnosticos(String codigoDiagnostico, Integer total,
			HttpHeaders headers) {
		ArrayList<DiagnosticoBean> listaDiagnosticos = null;

		try {
			ResponseOnc diagResponse = diagService.getListarDiagnosticoXcodigos(codigoDiagnostico, total, headers);

			if (diagResponse != null && Integer.parseInt(diagResponse.getAudiResponse().getCodigoRespuesta()) >= 0) {
				listaDiagnosticos = (ArrayList<DiagnosticoBean>) diagResponse.getDataList();
			} else {
				listaDiagnosticos = null;
			}
		} catch (Exception e) {
			LOGGER.error(".obtenerNombresDiagnosticos: " + e.getMessage());
			listaDiagnosticos = null;
		}

		return listaDiagnosticos;
	}

	@Override
	public ApiOutResponse obtenerDatosAutorizadorPert(List<ProgramacionCmacDetResponse> request, HttpHeaders headers) {
		String codSolEva = "";
		for (ProgramacionCmacDetResponse progCmac : request) {
			if (progCmac.getCodSolEvaluacion() != null) {
				if (codSolEva.equals(""))
					codSolEva = codSolEva + progCmac.getCodSolEvaluacion();
				else
					codSolEva = codSolEva + "|" + progCmac.getCodSolEvaluacion();
			}
		}
		List<ParticipanteBean> lPart = (List<ParticipanteBean>) emailDao.obtenerDatosAutorizadorPert(codSolEva)
				.getResponse();

		// CONSULTA A SERVICIO
		UsuariosPorCodigoRequest usuarioPorCodigoRequest = new UsuariosPorCodigoRequest();
		String codUsuarioCodRol = "";
		Map<Integer, Integer> mapa = new HashMap<>();
		for (ParticipanteBean par : lPart) {
			mapa.put(par.getCodUsuario(), par.getCodUsuario());
		}

		for (Map.Entry<Integer, Integer> entry : mapa.entrySet()) {
			codUsuarioCodRol += "|" + entry.getValue();
		}

		usuarioPorCodigoRequest.setCodigoUsuario(codUsuarioCodRol);
		return usuarioService.listarUsuarioPorCodigos(usuarioPorCodigoRequest, headers);
	}

}
