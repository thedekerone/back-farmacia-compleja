package pvt.auna.fcompleja.service.impl;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.ParametroBeanRequest;
import pvt.auna.fcompleja.model.bean.ParametroBean;
import pvt.auna.fcompleja.service.ParametroService;

@Service
public class ParametroServiceImpl implements ParametroService {
	
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(GenericoServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private AseguramientoPropiedades asegProp;

	@Override
	public ApiOutResponse buscarParametro(ParametroBeanRequest request, HttpHeaders headers) {
		// TODO Auto-generated method stub
		String url = asegProp.getParametro() + "/parametro/listar";
		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<ParametroBeanRequest> requestEntity = new HttpEntity<ParametroBeanRequest>(request, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);
			if (resultado != null) {
				if (resultado.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")) {
					if (resultado.getDataList().size() == 0) {
						response.setCodResultado(3);
						response.setMsgResultado("No se encontraron coincidencias");
						response.setResponse(null);
					} else {
						response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
						response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());

						ArrayList<ParametroBean> listaParametro = new ArrayList<ParametroBean>();

						for (int i = 0; i < resultado.getDataList().size(); i++) {
							HashMap<?, ?> resp = (HashMap<?, ?>) resultado.getDataList().get(i);
							ParametroBean parametro = new ParametroBean();
							parametro.setCodParametro((Integer) resp.get("codParametro"));
							parametro.setCodGrupoParametro((Integer) resp.get("codGrupoParametro"));
							parametro.setDenominacion(resp.get("denominacion") != null ? resp.get("denominacion").toString() : "");
							parametro.setAbreviatura(resp.get("abreviatura") != null ? resp.get("abreviatura").toString() : "");
							parametro.setEstado(resp.get("estado") != null ? resp.get("estado").toString() : "");
							listaParametro.add(parametro);
						}
						
						response.setResponse(listaParametro);
						response.setDataList(listaParametro);
					}

				} else {
					response.setCodResultado(1);
					response.setMsgResultado("Error al Buscar Parametro");
					response.setResponse(null);
				}
			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error al Buscar Parametro");
				response.setResponse(null);
			}
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error al Buscar Parametro");
			response.setResponse(null);
		}

		return response;
	}

}
