package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.EmailAlertaCmacRequest;
import pvt.auna.fcompleja.model.api.request.ListaBandejaRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudEvaluacionRequest;
import pvt.auna.fcompleja.model.api.request.registrarEvaluacionCmac;
import pvt.auna.fcompleja.model.api.request.evaluacion.ProgramacionCmacRequest;

public interface BandejaSolicitudEvaluacionService {

	public ApiOutResponse BandejaEvaluacion(ListaBandejaRequest listaBandeja) throws Exception;
	
	public ApiOutResponse consultarEvaluacionXFecha( EmailAlertaCmacRequest request );
	
	public ApiOutResponse registrarEvaluacionCmac( registrarEvaluacionCmac request );
	
	public WsResponse eliminarRegEvaluacionCmac( SolicitudEvaluacionRequest request );
	
	public ApiOutResponse EvaluacionXCodigo( ListaBandejaRequest request );
	
	public ApiOutResponse consultarEvaluacionXCodigo( EmailAlertaCmacRequest request );
	
	public ApiOutResponse actualizarActaEscanProgramacionCmac(ProgramacionCmacRequest request);
	
}
