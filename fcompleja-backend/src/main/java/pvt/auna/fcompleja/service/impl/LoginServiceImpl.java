package pvt.auna.fcompleja.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.model.api.request.LoginRequest;
import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.model.bean.LoginBean;
import pvt.auna.fcompleja.proxy.bean.DetalleReintentoBean;
import pvt.auna.fcompleja.proxy.bean.HeaderBean;
import pvt.auna.fcompleja.proxy.request.AuditoriaProxyRequest;
import pvt.auna.fcompleja.proxy.service.impl.ReintentoProxyServiceImpl;
import pvt.auna.fcompleja.service.LoginService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class LoginServiceImpl implements LoginService {
	Logger log = Logger.getLogger(getClass());

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private AseguramientoPropiedades propiedades;

	@Override
	public OncoWsResponse validarIntentoLogin(LoginRequest loginrq, String idTransaccion, String fechaTransaccion)
			throws Exception {

		log.info("[SERVICIO: VALIDAR INTENTO DE LOGIN][INICIO]");
		log.info("[SERVICIO: VALIDAR INTENTO DE LOGIN][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.info("[SERVICIO: VALIDAR INTENTO DE LOGIN][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.info("[SERVICIO: VALIDAR INTENTO DE LOGIN][REQUEST][BODY][" + loginrq.toString() + "]");

		String url = propiedades.getAseguramiento() + "/login/validarIntentoLogin";
		String urlHistorial = propiedades.getAuditoria();
		OncoWsResponse response = new OncoWsResponse();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		HttpEntity<LoginRequest> requestEntity = new HttpEntity<>(loginrq, headers);

		String respuestaString;

		try {

			respuestaString = restTemplate.postForObject(url, requestEntity, String.class);

			String estado = "";

			if (respuestaString != null) {
				List<LoginBean> lista = new ArrayList();

				ObjectMapper clienteMapper = new ObjectMapper();
				JsonNode resultClienteResponse = null;

				resultClienteResponse = clienteMapper.readTree(respuestaString);

				JsonNode arrayArchivoJsonAuditoria = resultClienteResponse.get("audiResponse");
				AudiResponse audiResponse = new AudiResponse();
				audiResponse.setIdTransaccion(arrayArchivoJsonAuditoria.get("idTransaccion").asText());
				audiResponse.setFechaTransaccion(arrayArchivoJsonAuditoria.get("fechaTransaccion").asText());
				audiResponse.setCodigoRespuesta(arrayArchivoJsonAuditoria.get("codigoRespuesta").asText());
				audiResponse.setMensajeRespuesta(arrayArchivoJsonAuditoria.get("mensajeRespuesta").asText());

				if (audiResponse.getCodigoRespuesta().equals("0")) {

					JsonNode arrayArchivoJson = resultClienteResponse.get("dataList");

					for (JsonNode obtenerArchivoJson : arrayArchivoJson) {
						LoginBean loginBean = new LoginBean();

						loginBean.setEstado(obtenerArchivoJson.get("estado").asInt());
						loginBean.setCodResultado(obtenerArchivoJson.get("codResultado").asInt());
						loginBean.setMsgResultado(obtenerArchivoJson.get("msgResultado").asText());

						lista.add(loginBean);

					}

					if (lista.size() > 0) {
						LoginBean loginBean = lista.get(0);

						if (loginBean.getEstado() == 0) {
							estado = "REINTENTO";
						} else if (loginBean.getEstado() == 1) {
							estado = "BLOQUEO TEMPORAL";
						} else if(loginBean.getEstado() == 2){
							estado = "BLOQUEO DEFINITIVO";
						}else if(loginBean.getEstado() == 3){
							estado = "USUARIO NO EXISTE EN BD";
						}else {
							estado = "ERROR NO IDENTIFICADO";
						}
						
						invocarAuditoriaReintentoLogin(loginrq, idTransaccion, fechaTransaccion, 
								estado, urlHistorial);
						
					}

				} else {
					
					invocarAuditoriaReintentoLogin(loginrq, idTransaccion, fechaTransaccion, 
							"ERROR SERVICIO VALIDAR INTENTO LOGIN", urlHistorial);
				
				}
				audiResponse.setUser(loginrq.getUsuario());
				response.setAudiResponse(audiResponse);
				response.setDataList(lista);

			}

			log.info("[SERVICIO: VALIDAR INTENTO DE LOGIN][RESPONSE] " + response.toString());
			log.info("[SERVICIO: VALIDAR INTENTO DE LOGIN][FIN]");

			return response;

		} catch (Exception e) {
			AudiResponse audiResponse = new AudiResponse(idTransaccion, new Date().toString(), "1",
					"Error Inesperado - validación de reintentoLogin");

			log.error("Error al consultar la WS validar intento de Login - Portal Comun  : ", e);
			log.info("[SERVICIO: VALIDAR INTENTO DE LOGIN][FIN]");

			return new OncoWsResponse(audiResponse, null);
		}

	}

	@Override
	public OncoWsResponse resetearReintentosLogin(LoginRequest loginrq, String idTransaccion, String fechaTransaccion)
			throws Exception {

		log.info("[SERVICIO: RESETEAR REINTENTO DE LOGIN][INICIO]");
		log.info("[SERVICIO: RESETEAR REINTENTO DE LOGIN][REQUEST][HEADER][idTransaccion][" + idTransaccion + "]");
		log.info(
				"[SERVICIO: RESETEAR REINTENTO DE LOGIN][REQUEST][HEADER][fechaTransaccion][" + fechaTransaccion + "]");
		log.info("[SERVICIO: RESETEAR REINTENTO DE LOGIN][REQUEST][BODY][" + loginrq.toString() + "]");

		String url = propiedades.getAseguramiento() + "/login/resetearReintentosLogin";
		String urlHistorial = propiedades.getAuditoria();
		OncoWsResponse response = new OncoWsResponse();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		HttpEntity<LoginRequest> requestEntity = new HttpEntity<>(loginrq, headers);
		
		String respuestaString;

		try {

			respuestaString = restTemplate.postForObject(url, requestEntity, String.class);

			if (respuestaString != null) {
				List<LoginBean> lista = new ArrayList();
				
				ObjectMapper clienteMapper = new ObjectMapper();
				JsonNode resultClienteResponse = null;

				resultClienteResponse = clienteMapper.readTree(respuestaString);

				JsonNode arrayArchivoJsonAuditoria = resultClienteResponse.get("audiResponse");
				AudiResponse audiResponse = new AudiResponse();
				audiResponse.setIdTransaccion(arrayArchivoJsonAuditoria.get("idTransaccion").asText());
				audiResponse.setFechaTransaccion(arrayArchivoJsonAuditoria.get("fechaTransaccion").asText());
				audiResponse.setCodigoRespuesta(arrayArchivoJsonAuditoria.get("codigoRespuesta").asText());
				audiResponse.setMensajeRespuesta(arrayArchivoJsonAuditoria.get("mensajeRespuesta").asText());
				
				
				if (audiResponse.getCodigoRespuesta().equals("0")) {

					JsonNode arrayArchivoJson = resultClienteResponse.get("dataList");

					for (JsonNode obtenerArchivoJson : arrayArchivoJson) {
						LoginBean loginBean = new LoginBean();

						loginBean.setEstado(obtenerArchivoJson.get("estado").asInt());
						loginBean.setCodResultado(obtenerArchivoJson.get("codResultado").asInt());
						loginBean.setMsgResultado(obtenerArchivoJson.get("msgResultado").asText());

						lista.add(loginBean);

					}
				}

				invocarAuditoriaReintentoLogin(loginrq, idTransaccion, fechaTransaccion, 
						"RESETEO DE REINTENTO EN LOGIN", urlHistorial);
								
				response.setAudiResponse(audiResponse);
				response.setDataList(lista);

			}			

			log.info("[SERVICIO: RESETEAR REINTENTO DE LOGIN][RESPONSE] " + response.toString());
			log.info("[SERVICIO: RESETEAR REINTENTO DE LOGIN][FIN]");

			return response;

		} catch (Exception e) {
			AudiResponse audiResponse = new AudiResponse(idTransaccion, new Date().toString(), "1",
					"Error Inesperado - Consumo de WS validarIntentoLogin - Portal Comun");

			log.error("Error al consultar la ws validar intento de Login - Portal Comun  : ", e);
			log.info("[SERVICIO: RESETEAR REINTENTO DE LOGIN][FIN]");

			return new OncoWsResponse(audiResponse, null);
		}

	}
	
	
	public void invocarAuditoriaReintentoLogin(LoginRequest loginrq, String idTransaccion,String fechaTransaccion,
			String tipoAccion, String url) {
		
		AuditoriaProxyRequest auditoriaProxyRequest = new AuditoriaProxyRequest();
		DetalleReintentoBean detalleReintentoBean = new DetalleReintentoBean();
		HeaderBean headerBean = new HeaderBean(idTransaccion, fechaTransaccion);

		detalleReintentoBean.setId_transaccion(idTransaccion);// **
		detalleReintentoBean.setUsuario(loginrq.getUsuario());
		detalleReintentoBean.setMotivo("INICIO SESION");
		detalleReintentoBean.setTipo_accion(tipoAccion);// **
		detalleReintentoBean.setFecha_hora(new Date().toString());// **
		detalleReintentoBean.setUrl(loginrq.getUrl());// **
		detalleReintentoBean.setAcces_token(loginrq.getAccesToken());// **

		auditoriaProxyRequest.setUsrapp("ONTPAC");
		auditoriaProxyRequest.setUsuario(loginrq.getUsuario());// ***
		auditoriaProxyRequest.setCod_usuario(loginrq.getUsuario());
		auditoriaProxyRequest.setResponse(detalleReintentoBean); // ***
		auditoriaProxyRequest.setId_transaccion(idTransaccion); // ***
		auditoriaProxyRequest.setProceso("REINTENTO LOGIN");
		auditoriaProxyRequest.setNombre_db("ONTPPA");
		auditoriaProxyRequest.setPaquete("PCK_PPA_SEGURIDAD");
		auditoriaProxyRequest.setNombre_tabla("PPA_AUDITREG");
		auditoriaProxyRequest.setEstado("1");

		ReintentoProxyServiceImpl reintentoProxyServiceImpl = new ReintentoProxyServiceImpl();

		try {
			reintentoProxyServiceImpl.auditoriaReintento(auditoriaProxyRequest, headerBean,url);
		} catch (Exception e) {
			log.error("Error al registrar hitorial - REINTENTO EN LOGIN:  - ws Auditoria  : ", e);
			e.printStackTrace();
		}
		
	}
	

}
