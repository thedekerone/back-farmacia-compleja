package pvt.auna.fcompleja.service;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.ParametroBeanRequest;

public interface ParametroService {
	public ApiOutResponse buscarParametro(ParametroBeanRequest request, HttpHeaders headers);
}
