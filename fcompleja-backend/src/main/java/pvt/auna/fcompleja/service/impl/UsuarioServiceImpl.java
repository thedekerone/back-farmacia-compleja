package pvt.auna.fcompleja.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.dao.UsuarioDao;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.WsResponse;
import pvt.auna.fcompleja.model.api.request.*;
import pvt.auna.fcompleja.model.api.request.evaluacion.ParticipanteRequest;
import pvt.auna.fcompleja.model.api.request.seguridad.CambioContrasenaRequest;
import pvt.auna.fcompleja.model.api.request.seguridad.EnvioCorreoContrasenaRequest;
import pvt.auna.fcompleja.model.api.response.ResponseGenericoObject;
import pvt.auna.fcompleja.model.api.response.evaluacion.ParticipanteResponse;
import pvt.auna.fcompleja.model.bean.CasosEvaCmacBean;
import pvt.auna.fcompleja.model.bean.ParametroBean;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.model.bean.UsuarioBeanRequest;
import pvt.auna.fcompleja.model.bean.UsuarioRolBean;
import pvt.auna.fcompleja.model.bean.UsuarioSeguridadBean;
import pvt.auna.fcompleja.service.UsuarioService;
import pvt.auna.fcompleja.util.DateUtils;
import pvt.auna.fcompleja.util.GenericUtil;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(GenericoServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private AseguramientoPropiedades asegProp;

	@Autowired
	private UsuarioDao lstUsuarioDao;

	@Override
	public List<UsuarioBean> listRolPersona(int codRol) throws Exception {
		List<UsuarioBean> lista = lstUsuarioDao.listRolPersona(codRol);
		return lista;
	}

	@Override
	public List<UsuarioBean> listarRolMonitoreo(int codRol) throws Exception {

		List<UsuarioBean> lista = lstUsuarioDao.listarRolMonitoreo(codRol);

		return lista;
	}

	@Override
	public List<UsuarioBean> listarRolPersonaGrupoDiag(String codSolEva, int codRol) throws Exception {
		List<UsuarioBean> lista = lstUsuarioDao.listarRolPersonaGrupoDiag(codSolEva, codRol);
		return lista;
	}

	@Override
	public List<UsuarioBean> listarRolAutorizadorCmac(int codRol, String codDescSolEva) throws Exception {
		List<UsuarioBean> lista = lstUsuarioDao.listarRolAutorizadorCmac(codRol, codDescSolEva);
		return lista;
	}

	@Override
	public List<CasosEvaCmacBean> listarCasosEvaCmac(int codEstadoProgCmac) throws Exception {
		List<CasosEvaCmacBean> lista = lstUsuarioDao.listarCasosEvaCmac(codEstadoProgCmac);
		return lista;
	}

	@Override
	public ApiOutResponse getListarRoles(UsuarioBean usuario, HttpHeaders headers) {

		String url = asegProp.getAseguramiento() + "/seguridad/rol/listar";

		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<UsuarioBean> requestEntity = new HttpEntity<UsuarioBean>(usuario, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);

			if (resultado != null) {
				if (resultado.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")) {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());

					ArrayList<UsuarioBean> listaRoles = new ArrayList<UsuarioBean>();

					for (int i = 0; i < resultado.getDataList().size(); i++) {
						HashMap<?, ?> resp = (HashMap<?, ?>) resultado.getDataList().get(i);
						UsuarioBean user = new UsuarioBean();
						user.setCodRol((Integer) resp.get("codRol"));
						user.setNombreRol((String) resp.get("nomRol"));

						listaRoles.add(user);
					}

					response.setResponse(listaRoles);

				} else {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());
					response.setResponse(null);
				}
			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error en el servicio de Listar roles");
				response.setResponse(null);
			}
		} catch (

		Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error en el servicio de Listar Roles");
			response.setResponse(null);
		}

		return response;
	}

	/**
	 * Proposito: Obtener el listado de Aplicaciones
	 * 
	 * @param usuario
	 * @param headers
	 * @return
	 */
	@Override
	public ApiOutResponse obtenerAplicacion(UsuarioBean usuario, HttpHeaders headers) {
		String url = asegProp.getAseguramiento() + "/usuario/obtenerAplicacion";

		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<UsuarioBean> requestEntity = new HttpEntity<UsuarioBean>(usuario, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);

			if (resultado != null) {

				if (resultado.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")) {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());
					response.setDataList(resultado.getDataList());
				} else {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());
					response.setResponse(null);
				}

			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error en el servicio de Obtener Aplicaciones");
				response.setResponse(null);
			}

		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error en el servicio de Obtener Aplicaciones");
			response.setResponse(null);
		}
		return response;
	}

	@Override
	public ApiOutResponse getListarUsuarios(UsuarioBean usuario, HttpHeaders headers) {
		String url = asegProp.getAseguramiento() + "/seguridad/usuario/listar";

		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<UsuarioBean> requestEntity = new HttpEntity<UsuarioBean>(usuario, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);

			if (resultado != null) {
				if (resultado.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")) {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());

					ArrayList<UsuarioBean> listaUsuarios = new ArrayList<UsuarioBean>();

					for (int i = 0; i < resultado.getDataList().size(); i++) {
						HashMap<?, ?> resp = (HashMap<?, ?>) resultado.getDataList().get(i);
						UsuarioBean user = new UsuarioBean();
						user.setCodUsuario(Integer.parseInt(resp.get("codUsuario").toString()));
						user.setNombre((String) resp.get("nombre"));
						user.setApellidoPaterno((String) resp.get("apellidoPaterno"));
						user.setApellidoMaterno((String) resp.get("apellidoMaterno"));
						user.setCorreo((String) resp.get("correo"));
						user.setNumDocumento((String) resp.get("numDocumento"));

						listaUsuarios.add(user);
					}

					response.setResponse(listaUsuarios);
				} else {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());
				}
			} else {
				response.setCodResultado(-1);
				response.setMsgResultado("Error en el servicio de Listar Usuarios por Roles");
				response.setResponse(null);
			}
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error en el servicio de Listar Usuarios por Roles");
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ApiOutResponse getListarUsuariosxRol(UsuarioBean usuario, HttpHeaders headers) {
		String url = asegProp.getAseguramiento() + "/seguridad/usuario/listarPorRol";

		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<UsuarioBean> requestEntity = new HttpEntity<UsuarioBean>(usuario, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);

			if (resultado != null) {
				if (resultado.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")) {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());

					ArrayList<UsuarioBean> listaUsuarios = new ArrayList<UsuarioBean>();

					for (int i = 0; i < resultado.getDataList().size(); i++) {
						HashMap<?, ?> resp = (HashMap<?, ?>) resultado.getDataList().get(i);
						UsuarioBean user = new UsuarioBean();
						user.setCodUsuario((Integer) resp.get("codUsuario"));
						user.setNombre((String) resp.get("nombre"));
						user.setApellidoPaterno((String) resp.get("apellidoPaterno"));
						user.setApellidoMaterno((String) resp.get("apellidoMaterno"));
						user.setCorreo((String) resp.get("correo"));
						user.setNumDocumento((String) resp.get("numDocumento"));

						listaUsuarios.add(user);
					}

					response.setResponse(listaUsuarios);
				} else {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());
				}
			} else {
				response.setCodResultado(-1);
				response.setMsgResultado("Error en el servicio de Listar Usuarios por Roles");
				response.setResponse(null);
			}
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error en el servicio de Listar Usuarios por Roles");
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ApiOutResponse getListarUsuariosXRolCodigos(UsuarioBean usuario, HttpHeaders headers) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ApiOutResponse listarUsuarioFarmacia(ParticipanteRequest participante, HttpHeaders headers) {
		ApiOutResponse out = lstUsuarioDao.listarUsuarioFarmacia(participante);
		List<ParticipanteResponse> lpart = (List<ParticipanteResponse>) out.getResponse();
		
		UsuariosPorCodigoRequest usuarioPorCodigoRequest = new UsuariosPorCodigoRequest();
		String codUsuarioCodRol = "";
		for (ParticipanteResponse part : lpart) {
			if (part.getCodUsuario() != null) {
				if (codUsuarioCodRol.equals(""))
					codUsuarioCodRol = codUsuarioCodRol + part.getCodUsuario();
				else
					codUsuarioCodRol = codUsuarioCodRol + "|" + part.getCodUsuario();
			}
		}

		usuarioPorCodigoRequest.setCodigoUsuario(codUsuarioCodRol);

		List<UsuarioBean> listUsuario;
		listUsuario = (List<UsuarioBean>) listarUsuarioPorCodigos(usuarioPorCodigoRequest, headers).getDataList();

		for (ParticipanteResponse part : lpart) {
			if (part.getCodUsuario() != null) {
				for (UsuarioBean userRol : listUsuario) {
					if (userRol.getCodUsuario().toString().equals(part.getCodUsuario().toString())) {
						part.setNombres(userRol.getNombre());
						part.setApellidos(userRol.getApellidoPaterno() + " " + userRol.getApellidoMaterno());
						part.setCorreoElectronico(userRol.getCorreo());
						break;
					}
				}
			}
		}
		
		out.setResponse(lpart);
		return out;
	}

	@Override
	public ApiOutResponse listarUsuarioFarmaciaDet(ParticipanteRequest participante, HttpHeaders headers) {
		return lstUsuarioDao.listarUsuarioFarmaciaDet(participante);
	}

	@Override
	public ApiOutResponse getListarUsuarioSolben(UsuarioBean usuario, HttpHeaders headers) {
		String url = asegProp.getAseguramiento() + "/seguridad/usuario/listarPorSolben";

		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<UsuarioBean> requestEntity = new HttpEntity<UsuarioBean>(usuario, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);

			if (resultado != null) {
				if (resultado.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")) {
					if (resultado.getDataList().size() == 0) {
						response.setCodResultado(3);
						response.setMsgResultado("No se encontraron coincidencias");
						response.setResponse(null);
					} else {
						response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
						response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());

						ArrayList<UsuarioBean> listaUsuarios = new ArrayList<UsuarioBean>();

						for (int i = 0; i < resultado.getDataList().size(); i++) {
							HashMap<?, ?> resp = (HashMap<?, ?>) resultado.getDataList().get(i);
							UsuarioBean user = new UsuarioBean();
							user.setCodUsuario((Integer) resp.get("codUsuario"));
							user.setNombre((String) resp.get("nombre"));
							user.setApellidoPaterno((String) resp.get("apellidoPaterno"));
							user.setApellidoMaterno((String) resp.get("apellidoMaterno"));
							user.setCorreo((String) resp.get("correo"));
							user.setNumDocumento((String) resp.get("numDocumento"));

							listaUsuarios.add(user);
						}

						response.setResponse(listaUsuarios);
					}

				} else {
					response.setCodResultado(1);
					response.setMsgResultado("Error al validar Usuario Solben");
					response.setResponse(null);
				}
			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error en el servicio de Usuario Solben");
				response.setResponse(null);
			}
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error en el servicio de Usuario Solben");
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ApiOutResponse listarUsuarioRol(UsuarioRolRequest request, HttpHeaders headers) {
		// TODO Auto-generated method stub
		String url = asegProp.getAseguramiento() + "/seguridad/usuarioRol/listar";

		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<UsuarioRolRequest> requestEntity = new HttpEntity<UsuarioRolRequest>(request, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);

			if (resultado != null) {
				if (resultado.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")) {
					if (resultado.getDataList().size() == 0) {
						response.setCodResultado(3);
						response.setMsgResultado("No se encontraron coincidencias");
						response.setResponse(null);
					} else {
						response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
						response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());

						ArrayList<UsuarioRolBean> listaUsuariosRol = new ArrayList<UsuarioRolBean>();

						for (int i = 0; i < resultado.getDataList().size(); i++) {
							HashMap<?, ?> resp = (HashMap<?, ?>) resultado.getDataList().get(i);
							UsuarioRolBean userRol = new UsuarioRolBean();
							userRol.setCodUsuario(resp.get("codUsuario").toString());
							userRol.setNomUsuario(resp.get("nomUsuario").toString());
							userRol.setNombres(resp.get("nomUsuario").toString());
							userRol.setApellidos(
									resp.get("apePaterno").toString() + " " + resp.get("apeMaterno").toString());
							userRol.setCodRol((Integer) resp.get("codRol"));
							userRol.setNomRol(resp.get("nomRol").toString());
							listaUsuariosRol.add(userRol);
						}

						response.setResponse(listaUsuariosRol);
						response.setDataList(listaUsuariosRol);
					}

				} else {
					response.setCodResultado(1);
					response.setMsgResultado("Error al validar Usuario Rol");
					response.setResponse(null);
				}
			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error en el servicio de Usuario Rol");
				response.setResponse(null);
			}
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error en el servicio de Usuario Rol");
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ApiOutResponse envioCorreoRecuperarContrasena(EnvioCorreoContrasenaRequest request, HttpHeaders headers) {

		String url = asegProp.getAseguramiento() + "/credencial/recuperar";

		WsResponse resultado;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<EnvioCorreoContrasenaRequest> requestEntity = new HttpEntity<>(request, headers);
			resultado = restTemplate.postForObject(url, requestEntity, WsResponse.class);

			if (resultado != null) {
				String codigoRespuesta = resultado.getAudiResponse().getCodigoRespuesta();
				if (codigoRespuesta.equalsIgnoreCase("0")) {
					response.setCodResultado(0);
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());
				} else {
					response.setCodResultado(Integer.valueOf(codigoRespuesta));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());
					response.setResponse(null);
				}
			} else {
				throw new Exception("Resultado vacio.");
			}
		} catch (Exception e) {
			LOGGER.error("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error al enviar correo de recuperacion de contraseña");
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ApiOutResponse cambiarContrasena(CambioContrasenaRequest request, HttpHeaders headers) {
		String url = asegProp.getAseguramiento() + "/credencial/cambiar";

		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<CambioContrasenaRequest> requestEntity = new HttpEntity<>(request, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);

			if (resultado != null) {
				String codigoRespuesta = resultado.getAudiResponse().getCodigoRespuesta();
				if (codigoRespuesta.equalsIgnoreCase("0")) {
					response.setCodResultado(0);
					response.setMsgResultado("Credenciales actualizadas");
				} else {
					response.setCodResultado(Integer.valueOf(codigoRespuesta));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());
					response.setResponse(null);
				}
			} else {
				throw new Exception("Resultado vacio.");
			}
		} catch (Exception e) {
			LOGGER.error("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error al solicitar cambio de contraseña");
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ApiOutResponse listarParticipantes(ParticipanteBeanRequest participante) {

		ApiOutResponse responseParticipante = lstUsuarioDao.listarParticipantes(participante);

		@SuppressWarnings("unchecked")
		ArrayList<ParticipanteBeanRequest> lista = (ArrayList<ParticipanteBeanRequest>) responseParticipante
				.getResponse();

		/* Actualizar Nombre y Apellidos Usuarios de Seguridad */
		String idTransaccion = GenericUtil.getUniqueID();
		String fechaTransaccion = DateUtils.getDateToStringDDMMYYYHHMMSS(new Date());
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("idTransaccion", idTransaccion);
		headers.add("fechaTransaccion", fechaTransaccion);

		UsuariosPorCodigoRequest usuarioPorCodigoRequest = new UsuariosPorCodigoRequest();

		String codUsuarioCodRol = "";

		for (ParticipanteBeanRequest par : lista) {
			if (par.getCodRol() != null && par.getCodUsuario() != null) {
				if (codUsuarioCodRol.equals(""))
					codUsuarioCodRol = codUsuarioCodRol + par.getCodUsuario();
				else
					codUsuarioCodRol = codUsuarioCodRol + "|" + par.getCodUsuario();
			}
		}

		usuarioPorCodigoRequest.setCodigoUsuario(codUsuarioCodRol);

		List<UsuarioBean> listUsuario;

		listUsuario = (List<UsuarioBean>) listarUsuarioPorCodigos(usuarioPorCodigoRequest, headers).getDataList();

		/* Llamada a Servicio de Roles de Farmacia Compleja */
		UsuarioBean usuarioBean = new UsuarioBean();
		ArrayList<UsuarioBean> listaRoles;
		usuarioBean.setCodAplicacion(asegProp.getAplicacion());
		listaRoles = (ArrayList<UsuarioBean>) getListarRoles(usuarioBean, headers).getResponse();

		/* Actualización de Nombres y Apellidos */

		for (ParticipanteBeanRequest par : lista) {
			if (par.getCodUsuario() != null) {
				for (UsuarioBean userRol : listUsuario) {
					if (userRol.getCodUsuario().toString().equals(par.getCodUsuario().toString())) {
						par.setNombres(userRol.getNombre());
						par.setApellidos(userRol.getApellidoPaterno() + " " + userRol.getApellidoMaterno());
						par.setCorreoElectronico(userRol.getCorreo());
						break;
					}
				}
			}
		}
		/* Actualización de Descripción de Perfil Participantes de Farmacia Compleja */

		for (ParticipanteBeanRequest par : lista) {
			for (UsuarioBean usuarioB : listaRoles) {
				if (par.getCodRol().equals(usuarioB.getCodRol())) {
					par.setDescripcionRol(usuarioB.getNombreRol());
					break;
				}
			}
		}

		/* Filtro de Lista Segun Criterio de Busqueda */

		ArrayList<ParticipanteBeanRequest> listaFiltrada = new ArrayList<ParticipanteBeanRequest>();

		String apellidosNombres = "";

		if (participante.getApellidos() != null) {
			apellidosNombres = participante.getApellidos();
		}

		for (ParticipanteBeanRequest par : lista) {

			if ((par.getNombres() + " " + par.getApellidos()).indexOf(apellidosNombres) != -1) {
				listaFiltrada.add(par);
			}

		}
		responseParticipante.setResponse(listaFiltrada);
		return responseParticipante;
	}

	private String descripcionPerfil(String codUsuario, List<UsuarioRolBean> listUsuarioRol) {
		String descripcionPerfil = "";

		for (UsuarioRolBean userRol : listUsuarioRol) {
			if (userRol.getCodUsuario().equals(codUsuario)) {
				descripcionPerfil = userRol.getNomRol();
				break;
			}
		}
		return !descripcionPerfil.equals("") ? descripcionPerfil : "NO REGISTRADO";
	}

	@Override
	public ApiOutResponse registrarParticipantes(ParticipanteBeanRequest participante) {
		return lstUsuarioDao.registrarParticipantes(participante);
	}

	@Override
	public ApiOutResponse listarUsuarioSeguridad(UsuarioSeguridadRequest request, HttpHeaders headers) {
		// TODO Auto-generated method stub
		String url = asegProp.getAseguramiento() + "/seguridad/usuario/listarUsuarioPorNombre";

		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<UsuarioSeguridadRequest> requestEntity = new HttpEntity<UsuarioSeguridadRequest>(request,
					headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);

			if (resultado != null) {
				if (resultado.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")) {
					if (resultado.getDataList().size() == 0) {
						response.setCodResultado(3);
						response.setMsgResultado("No se encontraron coincidencias");
						response.setResponse(null);
					} else {
						response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
						response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());

						ArrayList<UsuarioSeguridadBean> listaUsuariosSeguridad = new ArrayList<UsuarioSeguridadBean>();

						for (int i = 0; i < resultado.getDataList().size(); i++) {
							HashMap<?, ?> resp = (HashMap<?, ?>) resultado.getDataList().get(i);
							UsuarioSeguridadBean userSeguridad = new UsuarioSeguridadBean();
							userSeguridad.setCodUsuario((Integer) resp.get("codUsuario"));
							userSeguridad.setUsuario(resp.get("usuario") != null ? resp.get("usuario").toString() : "");
							userSeguridad.setNombres(resp.get("nombres") != null ? resp.get("nombres").toString() : "");
							userSeguridad.setApePate(resp.get("apePate") != null ? resp.get("apePate").toString() : "");
							userSeguridad.setApeMate(resp.get("apeMate") != null ? resp.get("apeMate").toString() : "");
							userSeguridad.setCorreo(resp.get("correo") != null ? resp.get("correo").toString() : "");
							userSeguridad.setTipoDoc(resp.get("tipoDoc") != null ? resp.get("tipoDoc").toString() : "");
							userSeguridad.setNumeroDoc(
									resp.get("numeroDoc") != null ? resp.get("numeroDoc").toString() : "");
							userSeguridad
									.setTelefono(resp.get("telefono") != null ? resp.get("telefono").toString() : "");
							userSeguridad.setCelular(resp.get("celular") != null ? resp.get("celular").toString() : "");
							userSeguridad.setEstado(resp.get("estado") != null ? resp.get("estado").toString() : "");
							userSeguridad.setCodAplicacion(
									resp.get("codAplicacion") != null ? resp.get("codAplicacion").toString() : "");
							userSeguridad.setAplicacion(
									resp.get("aplicacion") != null ? resp.get("aplicacion").toString() : "");
							userSeguridad.setCodRol(resp.get("codRol") != null ? resp.get("codRol").toString() : "");
							userSeguridad.setRol(resp.get("rol") != null ? resp.get("rol").toString() : "");
							listaUsuariosSeguridad.add(userSeguridad);
						}

						response.setResponse(listaUsuariosSeguridad);
						response.setDataList(listaUsuariosSeguridad);
					}

				} else {
					response.setCodResultado(1);
					response.setMsgResultado("Error al Listar Usuario Seguridad");
					response.setResponse(null);
				}
			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error al Listar Usuario Seguridad");
				response.setResponse(null);
			}
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error al Listar Usuario Seguridad");
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ApiOutResponse listarUsuarioPorCodigos(UsuariosPorCodigoRequest request, HttpHeaders headers) {
		// TODO Auto-generated method stub
		String url = asegProp.getAseguramiento() + "/seguridad/usuario/listarUsuarioPorCodigos";

		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<UsuariosPorCodigoRequest> requestEntity = new HttpEntity<UsuariosPorCodigoRequest>(request,
					headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);

			if (resultado != null) {
				if (resultado.getAudiResponse().getCodigoRespuesta().equalsIgnoreCase("0")) {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());

					ArrayList<UsuarioBean> listaUsuarios = new ArrayList<UsuarioBean>();

					for (int i = 0; i < resultado.getDataList().size(); i++) {
						HashMap<?, ?> resp = (HashMap<?, ?>) resultado.getDataList().get(i);
						UsuarioBean user = new UsuarioBean();
						user.setCodUsuario(Integer.parseInt(resp.get("codUsuario").toString()));
						user.setNombre((String) resp.get("nombre"));
						user.setApellidoPaterno((String) resp.get("apellidoPaterno"));
						user.setApellidoMaterno((String) resp.get("apellidoMaterno"));
						user.setCorreo((String) resp.get("correo"));
						user.setNumDocumento((String) resp.get("numDocumento"));

						listaUsuarios.add(user);
					}

					response.setResponse(listaUsuarios);
					response.setDataList(listaUsuarios);
				} else {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());
				}
			} else {
				response.setCodResultado(-1);
				response.setMsgResultado("Error en el servicio de Listar Usuarios por Codigos");
				response.setResponse(null);
			}
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error en el servicio de Listar Usuarios por Codigos");
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ApiOutResponse registrarUsuario(UsuarioBeanRequest request, HttpHeaders headers) {
		// TODO Auto-generated method stub
		String url = asegProp.getAseguramiento() + "/usuario/registrar";
		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<UsuarioBeanRequest> requestEntity = new HttpEntity<UsuarioBeanRequest>(request, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);
			if (resultado != null) {
				int result = Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta());
				if (result >= 0) {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());

					ArrayList<ParametroBean> listaParametro = new ArrayList<ParametroBean>();

					response.setResponse(listaParametro);
					response.setDataList(listaParametro);
				} else {
					response.setCodResultado(1);
					response.setMsgResultado("Error al Registrar Usuario");
					response.setResponse(null);
				}
			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error al Registrar Usuario");
				response.setResponse(null);
			}
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error al Registrar Usuario");
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ApiOutResponse actualizarUsuario(UsuarioBeanRequest request, HttpHeaders headers) {
		// TODO Auto-generated method stub
		String url = asegProp.getAseguramiento() + "/usuario/actualizar";
		ResponseOnc resultado = null;
		ApiOutResponse response = new ApiOutResponse();

		try {
			HttpEntity<UsuarioBeanRequest> requestEntity = new HttpEntity<UsuarioBeanRequest>(request, headers);
			resultado = restTemplate.postForObject(url, requestEntity, ResponseOnc.class);
			if (resultado != null) {
				int result = Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta());
				if (result >= 0) {
					response.setCodResultado(Integer.parseInt(resultado.getAudiResponse().getCodigoRespuesta()));
					response.setMsgResultado(resultado.getAudiResponse().getMensajeRespuesta());

					ArrayList<ParametroBean> listaParametro = new ArrayList<ParametroBean>();

					response.setResponse(listaParametro);
					response.setDataList(listaParametro);
				} else {
					response.setCodResultado(1);
					response.setMsgResultado("Error al Actualizar Usuario");
					response.setResponse(null);
				}
			} else {
				response.setCodResultado(-2);
				response.setMsgResultado("Error al Actualizar Usuario");
				response.setResponse(null);
			}
		} catch (Exception e) {
			LOGGER.info("ERROR: " + e.getMessage());
			response.setCodResultado(-1);
			response.setMsgResultado("Error al Actualizar Usuario");
			response.setResponse(null);
		}

		return response;
	}

	@Override
	public ResponseGenericoObject validarRegistroParticipante(ValidarRegParticipantRequest request) {
		return lstUsuarioDao.validarRegistroParticipante(request);
	}

}
