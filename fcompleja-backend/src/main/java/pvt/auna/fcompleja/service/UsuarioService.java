package pvt.auna.fcompleja.service;

import java.util.List;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.*;
import pvt.auna.fcompleja.model.api.request.evaluacion.ParticipanteRequest;
import pvt.auna.fcompleja.model.api.request.seguridad.CambioContrasenaRequest;
import pvt.auna.fcompleja.model.api.request.seguridad.EnvioCorreoContrasenaRequest;
import pvt.auna.fcompleja.model.api.response.ResponseGenericoObject;
import pvt.auna.fcompleja.model.bean.CasosEvaCmacBean;
import pvt.auna.fcompleja.model.bean.UsuarioBean;
import pvt.auna.fcompleja.model.bean.UsuarioBeanRequest;

public interface UsuarioService {

	public List<UsuarioBean> listRolPersona(int codRol) throws Exception;

	public List<UsuarioBean> listarRolMonitoreo(int codRol) throws Exception;

	public List<UsuarioBean> listarRolAutorizadorCmac(int codRol, String codDescSolEva) throws Exception;

	public List<UsuarioBean> listarRolPersonaGrupoDiag(String codSolEva, int codRol) throws Exception;

	public List<CasosEvaCmacBean> listarCasosEvaCmac(int codEstadoProgCmac) throws Exception;

	public ApiOutResponse getListarRoles(UsuarioBean usuario, HttpHeaders headers);
	
	public ApiOutResponse obtenerAplicacion(UsuarioBean usuario, HttpHeaders headers);

	public ApiOutResponse getListarUsuarios(UsuarioBean usuario, HttpHeaders headers);

	public ApiOutResponse getListarUsuariosxRol(UsuarioBean usuario, HttpHeaders headers);

	public ApiOutResponse getListarUsuariosXRolCodigos(UsuarioBean usuario, HttpHeaders headers);
	
	public ApiOutResponse listarUsuarioFarmacia(ParticipanteRequest participante, HttpHeaders headers);

	public ApiOutResponse listarUsuarioFarmaciaDet(ParticipanteRequest participante, HttpHeaders headers);
	
	public ApiOutResponse getListarUsuarioSolben(UsuarioBean usuario, HttpHeaders headers);
	
	public ApiOutResponse listarUsuarioRol(UsuarioRolRequest request, HttpHeaders headers);
	
	public ApiOutResponse listarParticipantes(ParticipanteBeanRequest participante);
	
	public ApiOutResponse registrarParticipantes(ParticipanteBeanRequest participante);
	
	public ApiOutResponse listarUsuarioSeguridad(UsuarioSeguridadRequest request, HttpHeaders headers);
	
	
	public ApiOutResponse listarUsuarioPorCodigos(UsuariosPorCodigoRequest request, HttpHeaders headers);

	ApiOutResponse envioCorreoRecuperarContrasena(EnvioCorreoContrasenaRequest request, HttpHeaders headers);

	ApiOutResponse cambiarContrasena(CambioContrasenaRequest request, HttpHeaders headers);
	
	public ApiOutResponse registrarUsuario(UsuarioBeanRequest request, HttpHeaders headers);

	public ApiOutResponse actualizarUsuario(UsuarioBeanRequest request, HttpHeaders headers);

	ResponseGenericoObject validarRegistroParticipante (ValidarRegParticipantRequest request);

}
