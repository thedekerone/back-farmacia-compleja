package pvt.auna.fcompleja.service;

import org.springframework.http.HttpHeaders;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.ResponseOnc;
import pvt.auna.fcompleja.model.api.request.LineaTratamientoRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.BandejaMonitoreoRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionMarcadorRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.MarcadorRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.MonitoreoEvolucionRequest;
import pvt.auna.fcompleja.model.api.request.monitoreo.SegEjecutivoRequest;
import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoResponse;

public interface MonitoreoService {

	public ApiOutResponse listaMonitoreo(BandejaMonitoreoRequest bandejaMonitoreoRequest, HttpHeaders headers);

	public ApiOutResponse listLineaTratamiento(LineaTratamientoRequest request, HttpHeaders headers);

	public ApiOutResponse getUltimoLineaTratamiento(LineaTratamientoRequest request);

	public ApiOutResponse consultarMarcadores(MarcadorRequest request);

	public ApiOutResponse regDatosEvolucion(EvolucionRequest request);

	public ApiOutResponse actDatosEvolucion(EvolucionRequest request);

	public ApiOutResponse consultarEvoluciones(EvolucionRequest request);

	public ApiOutResponse regResultadoEvolucion(EvolucionRequest request);

	public ApiOutResponse consultarDetalleEvolucion(EvolucionMarcadorRequest request);

	public ApiOutResponse listarHistorialMarcadores(MarcadorRequest request);

	public ApiOutResponse actualizarMonitoreoPendInfo(EvolucionRequest request);

	public ApiOutResponse regSeguimientoEjecutivo(SegEjecutivoRequest request);

	public ApiOutResponse listarSeguimientoEjecutivo(SegEjecutivoRequest request);

	public ApiOutResponse getSeguimientosPendientes(SegEjecutivoRequest request);

	public ApiOutResponse actEstadoSegPendientes(SegEjecutivoRequest request);

	public ApiOutResponse getUltRegistroMarcador(EvolucionMarcadorRequest request);

	public ApiOutResponse consDatosGrpDiagnostico(MonitoreoResponse request, HttpHeaders headers);

	public ResponseOnc consultaOncosysDatosDiag(String codDiagnostico, HttpHeaders headers);

	public ApiOutResponse getUltimoMonitoreo(MonitoreoEvolucionRequest request,HttpHeaders headers);

}
