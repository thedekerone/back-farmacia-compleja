/**
 * 
 */
package pvt.auna.fcompleja.service;

import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.bean.ExamenMedicoBean;
import pvt.auna.fcompleja.model.bean.MarcadoresBean;
import pvt.auna.fcompleja.model.bean.ProductoAsociadoBean;

/**
 * @author Jose.Reyes/MDP
 *
 */
public interface MaestroService {
	
	
	
	public ApiOutResponse registroExamenesMedicos ( ExamenMedicoBean examenMedicoBean);

	public ApiOutResponse listarExamenesMedicos (ExamenMedicoBean examenesMedicos);
	
	public ApiOutResponse registroMarcadores ( MarcadoresBean marcadores);

	public ApiOutResponse listarMarcaodres (MarcadoresBean marcadores);
	
	public ApiOutResponse registroProductoAsociado ( ProductoAsociadoBean productoAsociado);

	public ApiOutResponse listarProductoAsociado (ProductoAsociadoBean productoAsociado);
	

}
