package pvt.auna.fcompleja.service.impl;

import java.util.Formatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pvt.auna.fcompleja.dao.impl.MacDaoImpl;
import pvt.auna.fcompleja.model.api.ApiOutResponse;
import pvt.auna.fcompleja.model.api.request.monitoreo.EvolucionMarcadorRequest;
import pvt.auna.fcompleja.model.bean.CheckListBean;
import pvt.auna.fcompleja.model.bean.ComplicacionesBean;
import pvt.auna.fcompleja.model.bean.CriteriosBean;
import pvt.auna.fcompleja.model.bean.FichaTecnicaBean;
import pvt.auna.fcompleja.model.bean.MACBean;
import pvt.auna.fcompleja.service.MacService;
import pvt.auna.fcompleja.util.DateUtils;

@Service
public class MacServiceImpl implements MacService {

	@Autowired
	private MacDaoImpl lstMacDaoImpl;

	@Override
	public ApiOutResponse listFiltroMAC(MACBean filtro) {
		return lstMacDaoImpl.listFiltroMAC(filtro);
	}

	@Override
	public ApiOutResponse registroMAC(MACBean obj) {
		return lstMacDaoImpl.registroMAC(obj);
	}

	@Override
	public ApiOutResponse listaCheckListConfiguracion(CheckListBean obj) {
		return lstMacDaoImpl.listaCheckListConfiguracion(obj);
	}

	@Override
	public ApiOutResponse registrarCheckListConfiguracion(CheckListBean obj) {
		return lstMacDaoImpl.registroCheckListConfiguracion(obj);
	}

	@Override
	public ApiOutResponse listarCriterioInclusion(CriteriosBean obj) {
		return lstMacDaoImpl.listarCriterioInclusion(obj);
	}

	@Override
	public ApiOutResponse listarCriterioExclusion(CriteriosBean obj) {
		return lstMacDaoImpl.listarCriterioExclusion(obj);
	}

	@Override
	public ApiOutResponse registroCriterioExclusion(CriteriosBean obj) {
		return lstMacDaoImpl.registroCriterioExclusion(obj);
	}

	@Override
	public ApiOutResponse registroCriterioInclusion(CriteriosBean obj) {
		return lstMacDaoImpl.registroCriterioInclusion(obj);
	}

	@Override
	public ApiOutResponse registroFichaTecnica(FichaTecnicaBean obj) {
		return lstMacDaoImpl.registroFichaTecnica(obj);
	}

	@Override
	public ApiOutResponse listarFichasTecnicas(FichaTecnicaBean obj) {
		return lstMacDaoImpl.listarFichasTecnicas(obj);
	}

	@Override
	public ApiOutResponse registroComplicacionesMedicas(ComplicacionesBean obj) {
		// TODO Auto-generated method stub
		return lstMacDaoImpl.registroComplicacionesMedicas(obj);
	}

	@Override
	public ApiOutResponse listaComplicacionesMedicas(ComplicacionesBean obj) {
		// TODO Auto-generated method stub
		return lstMacDaoImpl.listaComplicacionesMedicas(obj);
	}

	@Override
	public ApiOutResponse registrarIndicacionCriterios(CheckListBean obj) {
		String lCriterioInclu = "";
		String lCriterioExclu = "";
		if (obj.getListaCriterioInclusion().size() > 0) {
			for (CriteriosBean in : obj.getListaCriterioInclusion()) {
				lCriterioInclu = lCriterioInclu + ((in.getCodCriterio() != null) ? in.getCodCriterio() : 0) + "$"
						+ ((in.getCodChkListIndi() != null) ? in.getCodChkListIndi() : 0) + "$"
						+ ((in.getCodCriterioLargo() != null) ? in.getCodCriterioLargo() : " ") + "$"
						+ ((in.getDescripcion() != null) ? in.getDescripcion() : " ") + "$"
						+ ((in.getOrden() != null) ? in.getOrden() : 0) + "$"
						+ ((in.getCodEstado() != null) ? in.getCodEstado() : 0) + "$"
						+ ((in.getCodMac() != null) ? in.getCodMac() : 0) + "$"
						+ ((in.getCodGrpDiag() != null) ? in.getCodGrpDiag() : 0) + "|";

			}
			lCriterioInclu = lCriterioInclu.substring(0, lCriterioInclu.length() - 1);

			System.out.println(lCriterioInclu);
			obj.setlCriterioInclusion(lCriterioInclu);
		}

		if (obj.getListaCriterioExclusion().size() > 0) {
			for (CriteriosBean in : obj.getListaCriterioExclusion()) {
				lCriterioExclu = lCriterioExclu + ((in.getCodCriterio() != null) ? in.getCodCriterio() : 0) + "$"
						+ ((in.getCodChkListIndi() != null) ? in.getCodChkListIndi() : 0) + "$"
						+ ((in.getCodCriterioLargo() != null) ? in.getCodCriterioLargo() : " ") + "$"
						+ ((in.getDescripcion() != null) ? in.getDescripcion() : " ") + "$"
						+ ((in.getOrden() != null) ? in.getOrden() : 0) + "$"
						+ ((in.getCodEstado() != null) ? in.getCodEstado() : 0) + "$"
						+ ((in.getCodMac() != null) ? in.getCodMac() : 0) + "$"
						+ ((in.getCodGrpDiag() != null) ? in.getCodGrpDiag() : 0) + "|";

			}
			lCriterioExclu = lCriterioExclu.substring(0, lCriterioExclu.length() - 1);

			System.out.println(lCriterioExclu);
			obj.setlCriterioExclusion(lCriterioExclu);
		}

		return lstMacDaoImpl.registrarIndicacionCriterios(obj);
	}
}
