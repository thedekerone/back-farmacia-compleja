package pvt.auna.fcompleja.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import pvt.auna.fcompleja.dao.SolbenDao;
import pvt.auna.fcompleja.model.api.request.SolbenWsRequest;
import pvt.auna.fcompleja.model.api.request.SolicitudPreliminarRequest;
import pvt.auna.fcompleja.model.api.response.SolbenWsResponse;
import pvt.auna.fcompleja.model.api.response.SolicitudPreliminarResponse;
import pvt.auna.fcompleja.service.SolbenService;

@Service
public class SolbenServiceImpl implements SolbenService {

	@Autowired
	private SolbenDao solbenWebServiceDao;

	@Override
	public SolicitudPreliminarResponse getScgSolbenAndInsertSolPreliminar(SolicitudPreliminarRequest request,
			HttpHeaders headers) {
		return solbenWebServiceDao.getScgSolbenAndInsertSolPreliminar(request);
	}

	public SolbenWsResponse actualizarSolben(SolbenWsRequest request) {
		return solbenWebServiceDao.actualizarSolben(request);
	}
}
