package pvt.auna.fcompleja.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

@Component
public class SecurityDataProviderUtil {
	// Definición del tipo de algoritmo a utilizar (AES, DES, RSA)
	private final static String alg = "AES";
	// Definición del modo de cifrado a utilizar
	private final static String cI = "AES/CBC/PKCS5Padding";

	/**
	 * Función de tipo String que recibe una llave (key), un vector de
	 * inicialización (iv) y el texto que se desea cifrar
	 * 
	 * @param key       la llave en tipo String a utilizar
	 * @param iv        el vector de inicialización a utilizar
	 * @param cleartext el texto sin cifrar a encriptar
	 * @return el texto cifrado en modo String
	 * @throws Exception puede devolver excepciones de los siguientes tipos:
	 *                   NoSuchAlgorithmException, InvalidKeyException,
	 *                   InvalidAlgorithmParameterException,
	 *                   IllegalBlockSizeException, BadPaddingException,
	 *                   NoSuchPaddingException
	 */

	public static String encryptAES(String aesKey, String cleartext) throws Exception {
		String key = new String(aesKey);
		String iv = new String(aesKey); // VECTOR ===> CLAVE= VECTOR = String key = "AUNAPERU12345678";

		try {
			Cipher cipher = Cipher.getInstance(cI);
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), alg);
			IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);
			byte[] encrypted = cipher.doFinal(cleartext.getBytes("UTF-8"));
			return new String(Base64.encodeBase64String(encrypted));
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * Función de tipo String que recibe una llave (key), un vector de
	 * inicialización (iv) y el texto que se desea descifrar
	 * 
	 * @param key       la llave en tipo String a utilizar
	 * @param iv        el vector de inicialización a utilizar
	 * @param encrypted el texto cifrado en modo String
	 * @return el texto desencriptado en modo String
	 * @throws Exception puede devolver excepciones de los siguientes tipos:
	 *                   NoSuchAlgorithmException, NoSuchPaddingException,
	 *                   InvalidKeyException, InvalidAlgorithmParameterException,
	 *                   IllegalBlockSizeException
	 */

	public static String decryptAES(String aesKey, String encrypted) throws Exception {
		String key = new String(aesKey); // CLAVE
		String iv = new String(aesKey); // KEY
		try {
			encrypted = encrypted.replaceAll(" ", "+");
			Cipher cipher = Cipher.getInstance(cI);
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), alg);
			IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
			byte[] enc = Base64.decodeBase64(encrypted);
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec);
			byte[] decrypted = cipher.doFinal(enc);
			return new String(decrypted);
		} catch (Exception e) {
			return null;
		}

	}

	public static String convertHexToString(String hex) {
		StringBuilder sb = new StringBuilder();
		StringBuilder temp = new StringBuilder();
		// 49204c6f7665204a617661 split into two characters 49, 20, 4c...
		for (int i = 0; i < hex.length() - 1; i += 2) {
			// grab the hex in pairs
			String output = hex.substring(i, (i + 2));
			// convert hex to decimal
			int decimal = Integer.parseInt(output, 16);
			// convert the decimal to character
			sb.append((char) decimal);
			temp.append(decimal);
		}
		return sb.toString();
	}

	public static String convertStringToHex(String str) {
		char[] chars = str.toCharArray();
		StringBuffer hex = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			hex.append(Integer.toHexString((int) chars[i]));
		}
		return hex.toString();
	}
}