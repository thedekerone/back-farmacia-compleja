package pvt.auna.fcompleja.util.view;

import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import pvt.auna.fcompleja.model.api.ExcelDownloadResponse;
import pvt.auna.fcompleja.model.api.ListaBandeja;
import pvt.auna.fcompleja.model.api.response.ListSolPreeliminarResponse;
import pvt.auna.fcompleja.model.api.response.monitoreo.MonitoreoResponse;
import pvt.auna.fcompleja.util.DateUtils;

@SuppressWarnings("deprecation")
public class ExcelPreBandejaView {

	public static ExcelDownloadResponse exportPreliminar(HttpServletRequest request,
			List<ListSolPreeliminarResponse> ListSolPreeliminar) {
		ExcelDownloadResponse responseBody = new ExcelDownloadResponse();
		String pathFile = ensureDir(request.getRealPath("downloadPreliminar")) + File.separator
				+ "SolicitudesPreliminares" + DateUtils.getDateToStringYYYYMMDDHHmmss(new Date()) + ".xlsx";
		File file = new File(pathFile);
		Workbook workbook = new XSSFWorkbook();
		responseBody.setMime("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		// responseBody.setHeader("Content-Disposition", "attachment;
		// filename=\"Solicitudes_Preliminares.xls\"");
		Sheet sheet = workbook.createSheet("Bandeja Preliminar");

		sheet.setColumnWidth(0, 18000);

		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.BLUE.index);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		font.setBold(true);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);
		style.setWrapText(true);

		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("N° SOLICITUD PRELIMINAR");
		header.getCell(0).setCellStyle(style);
		header.createCell(1).setCellValue("FECHA SOLICITUD PRELIMINAR");
		header.getCell(1).setCellStyle(style);
		header.createCell(2).setCellValue("HORA SOLICITUD PRELIMINAR");
		header.getCell(2).setCellStyle(style);
		header.createCell(3).setCellValue("CLINICAS");
		header.getCell(3).setCellStyle(style);
		header.createCell(4).setCellValue("PACIENTES");
		header.getCell(4).setCellStyle(style);
		header.createCell(5).setCellValue("N° SCG SOLBEN");
		header.getCell(5).setCellStyle(style);
		header.createCell(6).setCellValue("FECHA SCG SOLBEN");
		header.getCell(6).setCellStyle(style);
		header.createCell(7).setCellValue("HORA SCG SOLBEN");
		header.getCell(7).setCellStyle(style);
		header.createCell(8).setCellValue("TIPO SCG SOLBEN");
		header.getCell(8).setCellStyle(style);
		header.createCell(9).setCellValue("ESTADO SOLICITUD PRELIMINAR");
		header.getCell(9).setCellStyle(style);
		header.createCell(10).setCellValue("AUTORIZADOR PERTENENCIA");
		header.getCell(10).setCellStyle(style);

		int rowCount = 1;

		for (ListSolPreeliminarResponse preeliminar : ListSolPreeliminar) {
			Row userRow = sheet.createRow(rowCount++);
			userRow.createCell(0).setCellValue(preeliminar.getCodPre());

			userRow.createCell(1).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaPre()));
			userRow.createCell(2).setCellValue(preeliminar.getHoraPre());
			userRow.createCell(3).setCellValue(preeliminar.getDescClinica());
			userRow.createCell(4).setCellValue(preeliminar.getNombrePaciente());
			userRow.createCell(5).setCellValue(preeliminar.getCodScgSolben());
			userRow.createCell(6).setCellValue(DateUtils.getDateToStringDDMMYYYY(preeliminar.getFechaScgSolben()));
			userRow.createCell(7).setCellValue(preeliminar.getHoraScgSolben());
			userRow.createCell(8).setCellValue(preeliminar.getTipoScgSolben());
			userRow.createCell(9).setCellValue(preeliminar.getEstadoSol());
			userRow.createCell(10).setCellValue(preeliminar.getMedicoTratante());
		}

		int numberOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < numberOfSheets; i++) {
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(sheet.getFirstRowNum());
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					int columnIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columnIndex);
				}
			}
		}

		try {
			FileOutputStream outputStream = new FileOutputStream(file);
			workbook.write(outputStream);
			workbook.close();

			responseBody.setName(file.getName());
			responseBody.setLength(file.length());

		} catch (Exception e) {
			e.printStackTrace();
		}

		responseBody.setUrl(request.getRequestURL() + file.getName());
		return responseBody;
	}

	public static ExcelDownloadResponse exportEvaluacion(HttpServletRequest request,
			ArrayList<ListaBandeja> ListSolEvaluacion) {
		ExcelDownloadResponse responseBody = new ExcelDownloadResponse();
		// String pathFile = ensureUserDir("download") + File.separator +
		// request.getSession().getId() + ".xlsx";
		// String pathFile = ensureDir(request.getRealPath("download")) + File.separator
		// + request.getSession().getId() + ".xlsx";
		String pathFile = ensureDir(request.getRealPath("downloadEvaluacion")) + File.separator + "SolicitudEvaluacion_"
				+ DateUtils.getDateToStringYYYYMMDDHHmmss(new Date()) + ".xlsx";
		// response.setHeader("Content-Disposition", "attachment;
		// filename=\"Solicitudes_Preliminares.xls\"" );--getUniqueID()
		File file = new File(pathFile);
		Workbook workbook = new XSSFWorkbook();
		responseBody.setMime("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		// responseBody.setHeader("Content-Disposition", "attachment;
		// filename=\"Solicitudes_Preliminares.xls\"");
		Sheet sheet = workbook.createSheet("Detalle Solicitud");

		sheet.setDefaultColumnWidth(30);

		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.BLUE.index);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		font.setBold(true);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);
		style.setWrapText(true);

		// create header row
		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("N° SOLICITUD");
		header.getCell(0).setCellStyle(style);
		header.createCell(1).setCellValue("N° SCG");
		header.getCell(1).setCellStyle(style);
		header.createCell(2).setCellValue("TIPO SCG");
		header.getCell(2).setCellStyle(style);
		header.createCell(3).setCellValue("ESTADO SCG");
		header.getCell(3).setCellStyle(style);
		header.createCell(4).setCellValue("N° CARTA GARANTÍA");
		header.getCell(4).setCellStyle(style);
		header.createCell(5).setCellValue("CLINICA");
		header.getCell(5).setCellStyle(style);
		header.createCell(6).setCellValue("PACIENTE");
		header.getCell(6).setCellStyle(style);
		header.createCell(7).setCellValue("DIAGNOSTICO");
		header.getCell(7).setCellStyle(style);
		header.createCell(8).setCellValue("MAC");
		header.getCell(8).setCellStyle(style);
		header.createCell(9).setCellValue("FECHA REGISTRO");
		header.getCell(9).setCellStyle(style);
		header.createCell(10).setCellValue("TIPO");
		header.getCell(10).setCellStyle(style);
		header.createCell(11).setCellValue("ESTADO");
		header.getCell(11).setCellStyle(style);
		header.createCell(12).setCellValue("ROL RESPONSABLE");
		header.getCell(12).setCellStyle(style);
		header.createCell(13).setCellValue("AUTORIZADOR PERTINENCIA");
		header.getCell(13).setCellStyle(style);
		header.createCell(14).setCellValue("LIDER TUMOR");
		header.getCell(14).setCellStyle(style);
		header.createCell(15).setCellValue("FECHA REUNION CMAC");
		header.getCell(15).setCellStyle(style);
		header.createCell(16).setCellValue("CORREO ENVIADO LIDER TUMOR");
		header.getCell(16).setCellStyle(style);
		header.createCell(17).setCellValue("CORREO ENVIADO CMAC");
		header.getCell(17).setCellStyle(style);

		int rowCount = 1;

		for (ListaBandeja listaEvaluacion : ListSolEvaluacion) {
			Row userRow = sheet.createRow(rowCount++);
			userRow.createCell(0).setCellValue(listaEvaluacion.getNumeroSolEvaluacion());
			userRow.createCell(1).setCellValue(listaEvaluacion.getNumeroScgSolben());
			userRow.createCell(2).setCellValue(listaEvaluacion.getTipoScgSolben());
			userRow.createCell(3).setCellValue(listaEvaluacion.getDescEstadoSCGSolben());
			userRow.createCell(4).setCellValue(listaEvaluacion.getNumeroCartaGarantia());
			userRow.createCell(5).setCellValue(listaEvaluacion.getDescClinica());
			userRow.createCell(6).setCellValue(listaEvaluacion.getNombrePaciente());
			userRow.createCell(7).setCellValue(listaEvaluacion.getNombreDiagnostico());
			userRow.createCell(8).setCellValue(listaEvaluacion.getDescripcionCmac());
			userRow.createCell(9)
					.setCellValue(DateUtils.getDateToStringDDMMYYYHHMMSS(listaEvaluacion.getFechaRegistroEvaluacion()));
			userRow.createCell(10).setCellValue(listaEvaluacion.getTipoEvaluacion());
			userRow.createCell(11).setCellValue(listaEvaluacion.getEstadoEvaluacion());
			userRow.createCell(12).setCellValue(listaEvaluacion.getRolResponsableEvaluacion());
			userRow.createCell(13).setCellValue(listaEvaluacion.getAuditorPertenencia());
			userRow.createCell(14).setCellValue(listaEvaluacion.getLiderTumor());
			userRow.createCell(15).setCellValue(DateUtils.getDateToStringDDMMYYYY(listaEvaluacion.getFechaCmac()));
			userRow.createCell(16).setCellValue(listaEvaluacion.getCorreoLiderTumor());
			userRow.createCell(17).setCellValue(listaEvaluacion.getCorreoCmac());
		}

		int numberOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < numberOfSheets; i++) {
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(sheet.getFirstRowNum());
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					int columnIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columnIndex);
				}
			}
		}

		try {
			FileOutputStream outputStream = new FileOutputStream(file);
			workbook.write(outputStream);
			workbook.close();

			responseBody.setName(file.getName());
			responseBody.setLength(file.length());

		} catch (Exception e) {
			e.printStackTrace();
		}
		responseBody.setUrl(request.getRequestURL() + file.getName());
		return responseBody;
	}

	@SuppressWarnings("unchecked")
	public static ExcelDownloadResponse exportMonitoreo(HttpServletRequest request, Object response) {
		// TODO Auto-generated method stub
		List<MonitoreoResponse> listaTareaMonitoreo = new ArrayList<>();
		listaTareaMonitoreo = (List<MonitoreoResponse>) response;

		ExcelDownloadResponse responseBody = new ExcelDownloadResponse();

		// String pathFile = ensureUserDir("download") + File.separator +
		// request.getSession().getId() + ".xlsx";
		// String pathFile = ensureDir(request.getRealPath("download")) + File.separator
		// + request.getSession().getId() + ".xlsx";

		String pathFile = ensureDir(request.getRealPath("downloadMonitoreo")) + File.separator + "Tareas de Monitoreo"
				+ ".xlsx";
		// response.setHeader("Content-Disposition", "attachment;
		// filename=\"Solicitudes_Preliminares.xls\"" );--getUniqueID()
		File file = new File(pathFile);
		Workbook workbook = new XSSFWorkbook();
		responseBody.setMime("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		// responseBody.setHeader("Content-Disposition", "attachment;
		// filename=\"Solicitudes_Preliminares.xls\"");
		Sheet sheet = workbook.createSheet("Detalle Monitoreo");

		sheet.setDefaultColumnWidth(30);

		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.BLUE.index);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		font.setBold(true);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);
		style.setWrapText(true);

		// create header row
		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("Codigo Tarea de Monitoreo");
		header.getCell(0).setCellStyle(style);
		header.createCell(1).setCellValue("Nro. Solicitud Evaluacion");
		header.getCell(1).setCellStyle(style);
		header.createCell(2).setCellValue("Fecha Aprobacion");
		header.getCell(2).setCellStyle(style);
		header.createCell(3).setCellValue("Nro. SCG Solben");
		header.getCell(3).setCellStyle(style);
		header.createCell(4).setCellValue("Nro. CG Solben");
		header.getCell(4).setCellStyle(style);
		header.createCell(5).setCellValue("Medicamento Solicitado");
		header.getCell(5).setCellStyle(style);
		header.createCell(6).setCellValue("Linea de Tratamiento Actual");
		header.getCell(6).setCellStyle(style);
		header.createCell(7).setCellValue("Paciente");
		header.getCell(7).setCellStyle(style);
		// header.createCell(3).setCellValue("Rol Responsable");
		// header.getCell(3).setCellStyle(style);
		header.createCell(8).setCellValue("Estado de Monitoreo");
		header.getCell(8).setCellStyle(style);
		header.createCell(9).setCellValue("Fecha Proximo de Monitoreo");
		header.getCell(9).setCellStyle(style);
		header.createCell(10).setCellValue("Responsable de Monitoreo");
		header.getCell(10).setCellStyle(style);

		int rowCount = 1;

		for (MonitoreoResponse listaMonitoreo : listaTareaMonitoreo) {
			Row userRow = sheet.createRow(rowCount++);
			userRow.createCell(0).setCellValue(listaMonitoreo.getCodigoDescripcionMonitoreo());
			userRow.createCell(1).setCellValue(listaMonitoreo.getCodigoEvaluacion());
			userRow.createCell(2)
					.setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(listaMonitoreo.getFechAprobacion()));
			userRow.createCell(3).setCellValue(listaMonitoreo.getCodigoScgSolben());
			userRow.createCell(4).setCellValue(listaMonitoreo.getNroCartaGarantia());
			userRow.createCell(5).setCellValue(listaMonitoreo.getNomMedicamento());
			userRow.createCell(6).setCellValue(listaMonitoreo.getNumeroLineaTratamiento());
			userRow.createCell(7).setCellValue(listaMonitoreo.getNombrePaciente());
			userRow.createCell(8).setCellValue(listaMonitoreo.getNomEstadoMonitoreo());
			userRow.createCell(9)
					.setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(listaMonitoreo.getFechaProximoMonitoreo()));
			userRow.createCell(10).setCellValue(listaMonitoreo.getNomResponsableMonitoreo());
		}

		try {
			FileOutputStream outputStream = new FileOutputStream(file);
			workbook.write(outputStream);
			workbook.close();

			responseBody.setName(file.getName());
			responseBody.setLength(file.length());

		} catch (Exception e) {
			e.printStackTrace();
		}
		responseBody.setUrl(request.getRequestURL() + file.getName());
		return responseBody;
	}

	public static String ensureDir(String path) {
		File f_out = new File(path);
		if (!f_out.exists()) {
			f_out.mkdirs();
		}
		return f_out.getAbsolutePath();
	}

	public static String ensureUserDir(String path) {
		String outPath = System.getProperty("user.dir") + File.separator + path;
		File f_out = new File(outPath);
		if (!f_out.exists()) {
			f_out.mkdirs();
		}
		return f_out.getAbsolutePath();
	}

	public static String getUniqueID() {
		String r = "";
		try {
			MessageDigest salt = MessageDigest.getInstance("SHA-256");
			salt.update(UUID.randomUUID().toString().getBytes("UTF-8"));
			r = bytesToHex(salt.digest());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return r;
	}

	public static byte[] _bytesToHex(byte[] data) {
		String encoded = javax.xml.bind.DatatypeConverter.printHexBinary(data);
		return encoded.getBytes();
	}

	public static String bytesToHex(byte[] data) {
		return new String(_bytesToHex(data));
	}

}