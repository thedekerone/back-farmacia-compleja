package pvt.auna.fcompleja.util;

/**
 * @author usuario
 *
 */
public class ConstanteUtil {

	/** Datos de conexión de base de datos */
	public static final String ESQUEMA = "ONTPFC";
	public static final String ESQUEMA_SEGURIDAD = "ONTPPA";
	public static final String PAQUETE_PRELIMINAR = "PCK_PFC_BANDEJA_PRELIMINAR";
	public static final String PAQUETE1 = "PKC_PFC_SCG";
	public static final String PAQUETE_AUDIT = "PCK_PTP_PDC_AUDITORIA";
	public static final String SP_TIPO_CORREO = "SP_PFC_S_TIPO_CORREO";
	public static final String PAQUETE_EVALUACION = "PCK_PFC_BANDEJA_EVALUACION";
	public static final String PAQUETE_MONITOREO = "PCK_PFC_MONITOREO";
	public static final String PAQUETE_UTIL = "PCK_PFC_UTIL";
	public static final String PAQUETE_REPORTE_EVALUACION = "PCK_PFC_REPORTE_EVALUACION";
	public static final String SP_ESTADO_PRELIMINAR = "SP_PFC_U_ESTADO_PRELIMINAR";
	public static final String PAQUETE_MAESTRO = "PCK_PFC_MAESTROS";
	public static final String PAQUETE_SEGURIDAD_USUARIO = "PCK_PPA_USUARIO_ROL";
	public static final String PAQUETE_BANDEJA_MONITOREO = "PCK_PFC_BANDEJA_MONITOREO";
	public static final String PAQUETE_REPORTE_INDICADORES = "PCK_PFC_INDICADORES";
	public static final String PAQUETE_CONFIGURACION_SISTEMA = "PCK_PFC_CONFIGURACION_SISTEMA";

	/** CONSTANTES DE VALOR UNICO **/
	public static final long COD_EMAIL_SOLIC_INSCRIP = 1l;
	public static final int COD_TIPO_EMAIL_ALERTA_MONITOREO = 2;
	public static final long COD_TIPO_EMAIL_ALERTA_LIDER_TUMOR = 3L;
	public static final int COD_TIPO_EMAIL_ALERTA_AUTORIZADOR_CMAC = 4;
	// public static final long COD_EMAIL_ALERTA = 2L;
	// public static final int COD_TIPO_EMAIL_ALERTA_MONITOREO = 3;
	public static final long COD_EMAIL_ALERTA_LIDER_TUMOR = 2L;
	public static final int COD_ROL_RESP_MANT_MAC = 1;
	// public static final int COD_ROL_LIDER_TUMOR = 8;
	public static final int COD_ROL_RESP_MONITOREO = 5;
	public static final int COD_ROL_LIDER_TUMOR = 8;
	public static final int COD_ROL_MIEMBRO_CMAC = 9;
	public static final int COD_ROL_AUTORIZADOR_CMAC = 11;
	public static final int TIPO_INGRESO_RESULTADO = 123;
	public static final int SIN_CODIGO_CMP_MEDICO = 8;
	public static final int SIN_CODIGO_PACIENTE = 4;
	public static final int SIN_CODIGO_DIAGNOSTICO = 5;
	public static final int SIN_CODIGO_CLINICA = 7;
	public static final int NUMERO_MESES_CARGA = 3;

	public static final int COD_ESTADO_PROG_CMAC = 2;
	public static final String PN_COD_DESC_SOL_EVA = "00000003";

	public static final String INDICADOR_SI = "1";
	public static final String INDICADOR_NO = "0";
	public static final String CODIGO_SUCCESS = "000";

	/** REPORTE ***/
	public static final int REPORTE_PDF = 1;

	/** MENSAJES WS **/
	public static String MSG_SAP_EXITO = "Proceso completado";
	public static final String MENSAJE_REGISTRO_EXITO = "Registro con exito";

	public static final String FORMATO_FECHA_HORA_EXTENDIDO = "dd/MM/yyyy hh:mm a";
	public static final String FORMATO_FECHA_DD_MM_YYYY = "dd/MM/yyyy";
	public static final String FORMATO_FECHA_dd_MM_yyyy_HH_mm_ss = "dd/MM/yyyy HH:mm:ss";
	public static final String FORMATO_FECHA_YYYY_MM_DD_HH_mm_ss = "yyyyMMdd-HHmmss";
	public static final String FORMATO_FECHA_HH_mm_ss = "HH:mm:ss";
	public static final String FORMATO_HORA_HH_mm_ss = "HH:mm:ss";
	public static final String SOLES = "S/.";
	public static final String FORMATO_NUMERO_DECIMAL = "#,##0.00";
	public static final String FORMATO_NUMERO_CANTIDAD = "#,##0";

	public static final Character FORMATO_DECIMAL_CARACTER = '.';
	public static final Character FORMATO_MILES_CARACTER = ',';
	public static final String ESPACIO_GUION_ESPACIO = " - ";
	/* Códigos de los eventos */
	public static Integer EVENTO_ENVIAR_EMAIL = 10;

	/**
	 * Constante que indica el nombre de la variable del usuario que se usarA en
	 * todo el contexto de la aplicaciOn.
	 */

	public static final String USER_APP_CONTEXT = "userAppContext";
	public static final String REGLA_APP_CONTEXT = "reglaAppContext";
	/**
	 * Constantes de Webservices Maestra
	 */

	public static final String KEY_BEARER_TOKEN = "Bearer ";

	/**
	 * Constantes detalle Preliminar
	 */
	public static final String ESTADO_PRELIMINAR = "PENDIENTE";

	/** Constantes de Mensajes **/
	public static final String MENSAJE_SIN_RESULTADO = "No se encontraron resultados";
	public static final String MENSAJE_INGRESAR_CODIGO = "Por favor ingresar el codigo de afiliado";
	public static final String MENSAJE_INGRESAR__COD_DIAG = "Por favor ingresar el codigo de diagnostico";
	public static final String MENSAJE_INGRESAR__COD_CLIN = "Por favor ingresar el codigo de clinica";
	public static final String MENSAJE_INGRESAR__COD_MEDICO = "Por favor ingresar el codigo de medico";

	/** HEADERS */
	public static final String HEAD_ID_TRX = "No se encontraron resultados";
	public static final String HEAD_FEC_TRX = "No se encontraron resultados";

	/** FLAG BUSQUEDA POR PACIENTE */
	public static final String FLAGPACIENTE = "1";
	/** FLACG BUSQUEDA CLINICA */
	public static final String FLAGCLINICA = "2";

	public static final String CODRESPEXITOSO = "0";
	public static final String CODRESPSINDATOS = "2";

	public static final Integer registroIni = 1;
	public static final Integer registroFinGrupoDiag = 300;
	public static final Integer registroFinDiagnostico = 200;
	public static final Integer registroFinClinicas = 100;
	

	public static final Integer tipoTodos = 1;
	public static final Integer tipoXCod = 2;
	public static final Integer tipoXDes = 3;
	public static final Integer tipoCodAfi = 3;
	public static final Integer tipoCmp = 3;
	
	
	public static Integer GRUPO_RANGO_EDAD_01 = 244;
	public static Integer GRUPO_RANGO_EDAD_02 = 245;
	public static Integer GRUPO_RANGO_EDAD_03 = 246;
	public static Integer GRUPO_RANGO_EDAD_TODOS = 247;
	
	//ESTADOS MONITOREO
	public static Integer estadoPendienteMonitoreo = 118;
	public static Integer estadoAtendido = 119;
	public static Integer estadoPendRegResultado = 120;
	public static Integer estadoPendInformacion = 121;
	
	public static Integer estadoCorreoEnviadoSI = 30;
	public static Integer estadoCorreoEnviadoNO = 31;
	
	public static final Integer ESTADO_PENDIENTE_RESPUESTA_MONITOREO = 14;
	
	//PARAMETROS DE PLANTILLA CORREOS
	public static Integer EVALUACION_LIDER_TUMOR_tipoEnvio= 1;
	public static Integer EVALUACION_LIDER_TUMOR_flagAdjunto= 1;
	public static String EVALUACION_LIDER_TUMOR_codigoPlantilla= "10";
	public static String EVALUACION_LIDER_TUMOR_asunto= "PENDIENTE DE EVALUACIÓN LIDER DE TUMOR _ ";

	public static Integer EVALUACION_PROGRAMAR_CMAC_tipoEnvio= 1;
	public static Integer EVALUACION_PROGRAMAR_CMAC_flagAdjunto= 0;
	public static String EVALUACION_PROGRAMAR_CMAC_codigoPlantilla= "11";
	public static String EVALUACION_PROGRAMAR_CMAC_asunto= "CASOS A EVALUAR EN CÓMITE MÉDICO DE ALTA COMPLEJIDAD CMAC";
	
	//ESTADO DE SOLICITUD EVALUACION
	public static String SOL_EVALUACION_OBS_AUTORIZADOR = "22";
	public static String SOL_EVALUACION_OBS_LIDER_TUMOR ="25";
	
	public static String liderTumorMedicoTrata = "0";

}
