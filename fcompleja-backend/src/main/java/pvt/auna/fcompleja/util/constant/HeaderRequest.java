package pvt.auna.fcompleja.util.constant;

public class HeaderRequest {

    public static final String CONTENT_TYPE="Content-Type";

    public static final String ID_TX="idTransaccion";

    public static final String FECHA_TX="fechaTransaccion";

    public static final String TOKEN="Token";
}
