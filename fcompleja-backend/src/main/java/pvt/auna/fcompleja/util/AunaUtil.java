package pvt.auna.fcompleja.util;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import pvt.auna.fcompleja.model.api.AudiResponse;
import pvt.auna.fcompleja.model.api.request.HeaderBean;
import pvt.auna.fcompleja.model.api.response.ResponseGenerico;

@Component
public class AunaUtil<T> {

	public HeaderBean validHeader(HttpHeaders headers) {
		HeaderBean header = null;
		List<String> idTransaccion;
		List<String> fechaTransaccion;
		try {
			idTransaccion = headers.get(AunaDefine.HEAD_ID_TRX);
			fechaTransaccion = headers.get(AunaDefine.HEAD_FEC_TRX);
			if (idTransaccion != null && fechaTransaccion != null) {
				header = new HeaderBean(idTransaccion.get(0), fechaTransaccion.get(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return header;
	}

	public ResponseGenerico getResponse(HeaderBean header, String index, ArrayList<Object> objectList) {

		if (objectList == null) {
			objectList = new ArrayList<>();
		}

		ResponseGenerico response = null;

		switch (index) {

		case "0":
			response = new ResponseGenerico(
					new AudiResponse(header.getIdTransaccion(), getDateTimeZone(), "0", AunaDefine.RES_OK), objectList);
			break;
		case "2":
			response = new ResponseGenerico(
					new AudiResponse(header.getIdTransaccion(), getDateTimeZone(), "2", AunaDefine.RES_NOT_FOUND_DATA),
					objectList);
			break;
		case "1":
			response = new ResponseGenerico(
					new AudiResponse(header.getIdTransaccion(), getDateTimeZone(), "1", AunaDefine.RES_PARAM_ERR),
					objectList);
			break;
		case "-2":
			response = new ResponseGenerico(
					new AudiResponse(header.getIdTransaccion(), getDateTimeZone(), "-2", AunaDefine.RES_INT_ERR),
					objectList);
			break;
		case "-4":
			response = new ResponseGenerico(new AudiResponse("", getDateTimeZone(), "-4", AunaDefine.RES_HEAD_ERRR),
					objectList);
			break;
		}

		return response;
	}

	private String getDateTimeZone() {
		String fecha = ZonedDateTime.now().toString();
		fecha = fecha.substring(0, 29);
		return fecha;
	}
	
	public static Object castStringToJson(String jsonTxt) throws JSONException, IOException {
		return new ObjectMapper().readValue(jsonTxt, Object.class);
	}
	
	public String mesLetra ( Integer mes) {
		
		String mesString;
		switch (mes) {
		        case 1:  mesString = "Enero";
		                 break;
		        case 2:  mesString  = "Febrero";
		                 break;
		        case 3:  mesString = "Marzo";
		                 break;
		        case 4:  mesString = "Abril";
		                 break;
		        case 5:  mesString = "Mayo";
		                 break;
		        case 6:  mesString = "Junio";
		                 break;
		        case 7:  mesString = "Julio";
		                 break;
		        case 8:  mesString = "Agosto";
		                 break;
		        case 9:  mesString = "Septiembre";
		                 break;
		        case 10: mesString = "Octubre";
		                 break;
		        case 11: mesString = "Noviembre";
		                 break;
		        case 12: mesString = "Diciembre";
		                 break;
		        default: mesString = "Mes NO Válido";
		                 break;
		        }
		
		
		return mesString;
		
		
	}
}
