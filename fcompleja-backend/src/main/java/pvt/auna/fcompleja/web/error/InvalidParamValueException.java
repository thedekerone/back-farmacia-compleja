package pvt.auna.fcompleja.web.error;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import com.google.common.base.CaseFormat;

public class InvalidParamValueException extends ControllerException {

	private static final long serialVersionUID = -1748199813023098962L;

	private static final String TEMPLATE = "Valor inválido '%s' del parámetro '%s'";

	public InvalidParamValueException(String mensaje) {
		super(HttpStatus.BAD_REQUEST, mensaje);
	}
	
	public InvalidParamValueException(String param, Object value) {
		super(HttpStatus.BAD_REQUEST, String.format(TEMPLATE, value, toSnakeCase(param)));
	}

	public InvalidParamValueException(List<FieldError> error) {
		this(generarMensaje(error));
	}

	private static String toSnakeCase(String param) {
		return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, param);
	}

	private static String generarMensaje(List<FieldError> listaError) {
		String mensaje = "";
		for (FieldError error : listaError) {
			mensaje = mensaje + String.format(TEMPLATE, error.getRejectedValue(), toSnakeCase(error.getField())) + "\n";
		}
		return mensaje;
	}

}
