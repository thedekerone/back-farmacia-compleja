package pvt.auna.fcompleja.web.error;

public class FComplejaException extends RuntimeException{

	private static final long serialVersionUID = 627472295477738592L;

	public FComplejaException(final String mensaje) {
		super(mensaje);
	}
}
