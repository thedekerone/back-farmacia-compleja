package pvt.auna.fcompleja.web.error;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import pvt.auna.fcompleja.model.bean.ErrorBean;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler {
	private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler({ EntityException.class })
	public ResponseEntity<ErrorBean> handleOwnException(Exception exception, HttpServletRequest request) {
		log.error(exception.getMessage());
		return new ResponseEntity<>(new ErrorBean(exception), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ ControllerException.class })
	public ResponseEntity<ErrorBean> handleControllerException(ControllerException exception,
			HttpServletRequest request) {

		log.error(exception.getMessage());
		return exception.getResponse();
	}

	@ExceptionHandler({ HttpMessageNotReadableException.class})
	public ResponseEntity<ErrorBean> handleHttpMessageNotReadableException(HttpMessageNotReadableException exception,
			HttpServletRequest request) {

		log.error(exception.getMessage());
		
		JsonMappingException jme = (JsonMappingException) exception.getCause();

		return new InvalidRequestException(jme.getPath().get(0).getFieldName(), jme.getPath().get(0).getDescription()).getResponse();
	}

	@ExceptionHandler({ MethodArgumentTypeMismatchException.class })
	public ResponseEntity<ErrorBean> handleMethodArgumentTypeMismatchException(
			MethodArgumentTypeMismatchException exception, HttpServletRequest request) {

		log.error(exception.getMessage());

		return new InvalidParamValueException(exception.getName(), exception.getValue()).getResponse();
	}

	@ExceptionHandler({ MethodArgumentNotValidException.class, InvalidFormatException.class })
	public ResponseEntity<ErrorBean> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception,
			HttpServletRequest request) {

		log.error(exception.getMessage());

		BindingResult result = exception.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();

		for (int i = 0; i < fieldErrors.size(); i++) {
			System.out.println(fieldErrors.get(i));
		}

		return new InvalidParamValueException(fieldErrors).getResponse();
	}
}
