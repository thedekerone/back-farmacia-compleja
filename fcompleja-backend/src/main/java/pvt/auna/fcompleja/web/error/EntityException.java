package pvt.auna.fcompleja.web.error;

public class EntityException extends RuntimeException {

	private static final long serialVersionUID = 5504344698303494570L;

	public EntityException(final String mensaje) {
		super(mensaje);
	}
	
}
