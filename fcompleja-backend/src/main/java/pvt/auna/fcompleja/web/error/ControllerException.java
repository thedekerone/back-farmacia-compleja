package pvt.auna.fcompleja.web.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import pvt.auna.fcompleja.model.bean.ErrorBean;

public class ControllerException extends RuntimeException {
	private static final long serialVersionUID = -6895085519750266206L;
	
	private final HttpStatus status;
	private final String message;
	
	public ControllerException(final HttpStatus status, final String message) {
		super(message);
		this.status = status;
		this.message = message;
	}
	
	public ResponseEntity<ErrorBean> getResponse() {
		return ResponseEntity.status(status).contentType(MediaType.APPLICATION_JSON)
				.body(new ErrorBean(this.getClass().getSimpleName(), message));
	}
}
