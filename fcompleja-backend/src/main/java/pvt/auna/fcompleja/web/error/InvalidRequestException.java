package pvt.auna.fcompleja.web.error;

import org.springframework.http.HttpStatus;

public class InvalidRequestException extends ControllerException {

	private static final long serialVersionUID = -7569599203193309108L;
	
	private static final String TEMPLATE = "Variable '%s' con valor inválido '%s'";
	
	public InvalidRequestException(String params, Object valor) {
		super(HttpStatus.BAD_REQUEST, String.format(TEMPLATE, params, valor));
	}

}
