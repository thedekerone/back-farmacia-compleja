package pvt.auna.fcompleja.proxy.service;

import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.proxy.bean.HeaderBean;
import pvt.auna.fcompleja.proxy.request.AuditoriaProxyRequest;

public interface ReintentoProxyService {

	public OncoWsResponse auditoriaReintento(AuditoriaProxyRequest auditoriaProxyRequest,
			HeaderBean headers, String url) throws Exception;
	
}
