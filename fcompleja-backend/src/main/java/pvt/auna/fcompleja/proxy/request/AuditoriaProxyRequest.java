package pvt.auna.fcompleja.proxy.request;

public class AuditoriaProxyRequest {

	private String usrapp;
	private String usuario;
	private String cod_usuario;
	private Object response;
	private String id_transaccion;
	private String proceso;
	private String nombre_db;
	private String paquete;
	private String nombre_tabla;
	private String estado;
	
	public String getUsrapp() {
		return usrapp;
	}
	public void setUsrapp(String usrapp) {
		this.usrapp = usrapp;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getCod_usuario() {
		return cod_usuario;
	}
	public void setCod_usuario(String cod_usuario) {
		this.cod_usuario = cod_usuario;
	}
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
	public String getId_transaccion() {
		return id_transaccion;
	}
	public void setId_transaccion(String id_transaccion) {
		this.id_transaccion = id_transaccion;
	}
	public String getProceso() {
		return proceso;
	}
	public void setProceso(String proceso) {
		this.proceso = proceso;
	}
	public String getNombre_db() {
		return nombre_db;
	}
	public void setNombre_db(String nombre_db) {
		this.nombre_db = nombre_db;
	}
	public String getPaquete() {
		return paquete;
	}
	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}
	public String getNombre_tabla() {
		return nombre_tabla;
	}
	public void setNombre_tabla(String nombre_tabla) {
		this.nombre_tabla = nombre_tabla;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	@Override
	public String toString() {
		return "AuditoriaProxyBean [usrapp=" + usrapp + ", usuario=" + usuario + ", cod_usuario="
				+ cod_usuario + ", response=" + response + ", id_transaccion=" + id_transaccion
				+ ", proceso=" + proceso + ", nombre_db=" + nombre_db + ", paquete=" + paquete
				+ ", nombre_tabla=" + nombre_tabla + ", estado=" + estado + "]";
	}
	
	
	
	
}
