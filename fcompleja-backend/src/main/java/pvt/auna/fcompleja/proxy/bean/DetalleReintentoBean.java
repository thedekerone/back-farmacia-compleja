package pvt.auna.fcompleja.proxy.bean;

public class DetalleReintentoBean {
	
	private String id_transaccion;
	private String usuario;
	private String motivo;
	private String tipo_accion;
	private String fecha_hora;
	private String url;
	private String acces_token;
	
	public String getId_transaccion() {
		return id_transaccion;
	}
	public void setId_transaccion(String id_transaccion) {
		this.id_transaccion = id_transaccion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getTipo_accion() {
		return tipo_accion;
	}
	public void setTipo_accion(String tipo_accion) {
		this.tipo_accion = tipo_accion;
	}
	public String getFecha_hora() {
		return fecha_hora;
	}
	public void setFecha_hora(String fecha_hora) {
		this.fecha_hora = fecha_hora;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAcces_token() {
		return acces_token;
	}
	public void setAcces_token(String acces_token) {
		this.acces_token = acces_token;
	}
	
	@Override
	public String toString() {
		return "DetalleReintentoBean [id_transaccion=" + id_transaccion + ", usuario=" + usuario
				+ ", motivo=" + motivo + ", tipo_accion=" + tipo_accion + ", fecha_hora="
				+ fecha_hora + ", url=" + url + ", acces_token=" + acces_token + "]";
	}
	
	
	
}
