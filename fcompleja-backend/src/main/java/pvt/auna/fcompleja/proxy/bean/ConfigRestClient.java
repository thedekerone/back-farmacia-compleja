package pvt.auna.fcompleja.proxy.bean;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class ConfigRestClient {
    private RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        int timeout = 5000;
        int readtimeout = 10000;
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
                = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(timeout);
        clientHttpRequestFactory.setReadTimeout(readtimeout);
        return clientHttpRequestFactory;
    }

    /**
     * @return the restTemplate
     */
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    /**
     * @param restTemplate the restTemplate to set
     */
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
