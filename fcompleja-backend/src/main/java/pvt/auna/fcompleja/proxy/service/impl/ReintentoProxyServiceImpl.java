package pvt.auna.fcompleja.proxy.service.impl;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import pvt.auna.fcompleja.config.AseguramientoPropiedades;
import pvt.auna.fcompleja.model.api.response.OncoWsResponse;
import pvt.auna.fcompleja.proxy.bean.HeaderBean;
import pvt.auna.fcompleja.proxy.request.AuditoriaProxyRequest;
import pvt.auna.fcompleja.proxy.service.ReintentoProxyService;
import pvt.auna.fcompleja.util.ConfigRestClient;

@Service
public class ReintentoProxyServiceImpl extends ConfigRestClient implements ReintentoProxyService {

	private final Logger LOGGER = Logger.getLogger(getClass());

	
	@Autowired
	private AseguramientoPropiedades propiedades;

	@Override
	public OncoWsResponse auditoriaReintento(AuditoriaProxyRequest auditoriaProxyRequest,
			HeaderBean headers, String url) throws Exception {

		OncoWsResponse oncoWsResponse = new OncoWsResponse();

		LOGGER.info("[SERVICIO: REGISTRAR REINTENTO][INICIANDO]");
		LOGGER.info("[SERVICIO: REGISTRAR REINTENTO][HEADER][" + headers.toString() + "][REQUEST]["
				+ auditoriaProxyRequest.toString() + "]");

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectBody = mapper.createObjectNode();
		objectBody.put("usrapp", auditoriaProxyRequest.getUsrapp());
		objectBody.put("usuario", auditoriaProxyRequest.getUsuario());
		objectBody.put("cod_usuario", auditoriaProxyRequest.getCod_usuario());
		objectBody.put("response", objectToJsonString(auditoriaProxyRequest.getResponse()));
		objectBody.put("id_transaccion", auditoriaProxyRequest.getId_transaccion());
		objectBody.put("proceso", auditoriaProxyRequest.getProceso());
		objectBody.put("nombre_db", auditoriaProxyRequest.getNombre_db());
		objectBody.put("paquete", auditoriaProxyRequest.getPaquete());
		objectBody.put("nombre_tabla", auditoriaProxyRequest.getNombre_tabla());
		objectBody.put("estado", auditoriaProxyRequest.getEstado());

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.add("idTransaccion", headers.getIdTransaccion());
		header.add("fechaTransaccion", headers.getFechaTransaccion());

		HttpEntity<String> request = new HttpEntity<>(objectBody.toString(), header);

		String respuesta = "";

		try {
			respuesta = getRestTemplate().postForObject(url, request, String.class);
			oncoWsResponse = procesarRespuesta(respuesta);
		} catch (HttpClientErrorException ex) {
			respuesta = "";
			LOGGER.error("[SERVICIO: REGISTRAR REINTENTO][ERROR]", ex);
			oncoWsResponse = procesarRespuesta(respuesta);
		} catch (Exception ex) {
			respuesta = "";
			LOGGER.error("[SERVICIO: REGISTRAR REINTENTO][ERROR]", ex);
			oncoWsResponse = procesarRespuesta(respuesta);
		}
		LOGGER.info("[SERVICIO: REGISTRAR REINTENTO][FINALIZANDO]");
		return oncoWsResponse;
	}

	private OncoWsResponse procesarRespuesta(String respuesta) {
		ObjectMapper clienteMapper = new ObjectMapper();
		if (!respuesta.equals("")) {
			JsonNode respuestaJson;
			try {
				respuestaJson = clienteMapper.readTree(respuesta);
				JsonNode audiResponse = respuestaJson.get("audiResponse");
				String cod_resp = audiResponse.get("codigoRespuesta").asText();
				String mensaje = audiResponse.get("mensajeRespuesta").asText();
				LOGGER.info("[SERVICIO: REGISTRAR REINTENTO][CODIGO RESPUESTA][" + cod_resp
						+ "][MENSAJE][" + mensaje + "]");
			} catch (JsonProcessingException e) {
				LOGGER.error("[SERVICIO: REGISTRAR REINTENTO][ERROR PROCESAR RESPUESTA]", e);
			} catch (IOException e) {
				LOGGER.error("[SERVICIO: REGISTRAR REINTENTO][ERROR PROCESAR RESPUESTA]", e);
			}
		}
		return new OncoWsResponse();
	}

	private String objectToJsonString(Object object) {
		ObjectMapper Obj = new ObjectMapper();
		String jsonStr = "";
		try {
			jsonStr = Obj.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			LOGGER.error("[SERVICIO: REGISTRAR REINTENTO][ERROR PROCESAR OBJECT TO JSON STRING]",
					e);
		}
		return jsonStr;
	}
}
