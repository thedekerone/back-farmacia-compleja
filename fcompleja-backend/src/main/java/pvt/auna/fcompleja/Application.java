package pvt.auna.fcompleja;

import java.util.Date;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@EnableCaching
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

	private static final Logger log = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		log.info("---------Start class Application FARMACIA COMPLEJA---------");
		log.info("Fecha del día: " + new Date() + " (default system)");
		log.info("Timezone por defecto es: " + TimeZone.getDefault().getDisplayName());
		SpringApplication.run(Application.class, args);
	}
      
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}