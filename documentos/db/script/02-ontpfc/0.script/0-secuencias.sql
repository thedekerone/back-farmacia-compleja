--------------------------------------------------------
--  DDL for Sequence SQ_PFC_ARC_COM_COD_ARC_COM
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_ARC_COM_COD_ARC_COM"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_CHK_REQ_COD_CHKLIST_REQ
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_CHK_REQ_COD_CHKLIST_REQ"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_CHKLST_INDI_COD_CHKLST
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_CHKLST_INDI_COD_CHKLST"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 75 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_ACTA_CMAC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_ACTA_CMAC"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 241 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_EVOL_MARCADOR
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_EVOL_MARCADOR"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 501 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_EVOLUCION
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_EVOLUCION"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 281 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_EXAMEN_MED
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_EXAMEN_MED"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_METASTASIS
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_METASTASIS"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 33 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_PROG_CMAC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_PROG_CMAC"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_PROG_CMAC_DET
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_PROG_CMAC_DET"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_SEG_EJECUTIVO
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_SEG_EJECUTIVO"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 181 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 173 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_SEV_CON_PAC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_SEV_CON_PAC"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_SEV_LIDER_TUM
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_SEV_LIDER_TUM"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 41 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COD_SOLBEN
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COD_SOLBEN"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 141 CACHE 10 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_CONF_MARC_COD_CONF_MARC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_CONF_MARC_COD_CONF_MARC"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 312 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_CONT_DOC_COD_CONT_DOC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_CONT_DOC_COD_CONT_DOC"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_CONTINUADOR_COD_CONT
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_CONTINUADOR_COD_CONT"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_COR_PART_COD_COR_PART
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_COR_PART_COD_COR_PART"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 201 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_CORREO_COD_CORREO
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_CORREO_COD_CORREO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_CRIT_EXC_COD_CRIT_EXC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_CRIT_EXC_COD_CRIT_EXC"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 221 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_CRIT_INC_COD_CRIT_INC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_CRIT_INC_COD_CRIT_INC"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 141 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_DOC_EVA_COD_DOC_EVA
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_DOC_EVA_COD_DOC_EVA"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 68 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_EXAM_MED_DET_COD_EXAM
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_EXAM_MED_DET_COD_EXAM"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_FICHA_TEC_COD_FICHA_TEC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_FICHA_TEC_COD_FICHA_TEC"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_HIS_LINE_TRAT_COD_HIS
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_HIS_LINE_TRAT_COD_HIS"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 19 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_MAC_COD_MAC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_MAC_COD_MAC"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 185 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_MEDIC_NUEVO_COD_MEDIC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_MEDIC_NUEVO_COD_MEDIC"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 201 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_MONITOREO_COD_EVOLUCION
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_MONITOREO_COD_EVOLUCION"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_MONITOREO_COD_MONITOREO
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_MONITOREO_COD_MONITOREO"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 105 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_PARTICIPANTE_CMAC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_PARTICIPANTE_CMAC"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 481 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_PROD_ASOC_COD_PROD_ASOC
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_PROD_ASOC_COD_PROD_ASOC"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_RES_BAS_MAR_COD_RES_BAS
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_RES_BAS_MAR_COD_RES_BAS"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 79 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_SEGUIMIENTO_COD_SEG
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_SEGUIMIENTO_COD_SEG"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1535 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_SEV_ANALISIS_CONCLUSION
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_SEV_ANALISIS_CONCLUSION"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_SEV_CHK_PER_PAC_COD_CHK
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_SEV_CHK_PER_PAC_COD_CHK"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 140 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_SEV_CHK_REQ_PAC_COD_CHK
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_SEV_CHK_REQ_PAC_COD_CHK"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_SEV_CON_BAS_PAC_COD_CON
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_SEV_CON_BAS_PAC_COD_CON"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 23 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_SEV_LIN_TRA_COD_SEV_LIN
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_SEV_LIN_TRA_COD_SEV_LIN"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 201 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_SOL_EVA_COD_SOL_EVA
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_SOL_EVA_COD_SOL_EVA"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 27 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_SOL_PRE_COD_SOL_PRE
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_SOL_PRE_COD_SOL_PRE"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 48 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_PFC_TIPO_CO_COD_TIPO_CO
--------------------------------------------------------

   CREATE SEQUENCE  "ONTPFC"."SQ_PFC_TIPO_CO_COD_TIPO_CO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  ORDER  NOCYCLE ;
