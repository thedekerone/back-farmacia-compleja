
DROP TABLE ONTPFC.PFC_PRODUCTO_ASOCIADO CASCADE CONSTRAINTS;
DROP TABLE ONTPFC.PFC_MAC CASCADE CONSTRAINTS;
DROP TABLE ONTPFC.PFC_PARAMETRO CASCADE CONSTRAINTS;
DROP TABLE ONTPFC.PFC_GRUPO CASCADE CONSTRAINTS;


CREATE TABLE ONTPFC.pfc_archivo_complicacion (
    cod_archivo_comp      NUMBER(10) NOT NULL,
    cod_version           VARCHAR2(20 BYTE),
    nombre_archivo        VARCHAR2(80 BYTE),
    fec_inicio_vig        DATE,
    fec_fin_vig           DATE,
    p_estado              NUMBER(7),
    cod_mac               NUMBER(10) NOT NULL,
    codigo_archivo_comp   NUMBER
);

COMMENT ON COLUMN ONTPFC.pfc_archivo_complicacion.cod_archivo_comp IS
    'CODIGO ARCHIVO DE COMPLICACIONES';

CREATE UNIQUE INDEX ONTPFC.pfc_archivo_complicacion_pk ON
    ONTPFC.pfc_archivo_complicacion (
        cod_archivo_comp
    ASC );

ALTER TABLE ONTPFC.pfc_archivo_complicacion
    ADD CONSTRAINT pfc_archivo_complicacion_pk PRIMARY KEY ( cod_archivo_comp )
        USING INDEX ONTPFC.pfc_archivo_complicacion_pk;

CREATE TABLE ONTPFC.pfc_checklist_requisito (
    cod_checklist_req       NUMBER(10) NOT NULL,
    linea_tratamiento       NUMBER(2),
    cod_sev_chk_req_pac     NUMBER(10) NOT NULL,
    cod_mac                 NUMBER(10),
    p_tipo_documento        NUMBER(7),
    descripcion_documento   VARCHAR2(100 BYTE),
    flag_carga              CHAR(1 BYTE),
    fec_carga               DATE,
    url_descarga            VARCHAR2(100 BYTE),
    estado                  CHAR(1 BYTE),
    p_estado                NUMBER(7),
    cod_archivo             NUMBER(10)
);

COMMENT ON COLUMN ONTPFC.pfc_checklist_requisito.cod_checklist_req IS
    'MEDICAMENTO NUEVO PASO 2 CODIGO';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_checklist_requisito ON
    ONTPFC.pfc_checklist_requisito (
        cod_checklist_req
    ASC );

ALTER TABLE ONTPFC.pfc_checklist_requisito
    ADD CONSTRAINT pk_pfc_checklist_requisito PRIMARY KEY ( cod_checklist_req )
        USING INDEX ONTPFC.pk_pfc_checklist_requisito;

CREATE TABLE ONTPFC.pfc_chklist_indicacion (
    cod_chklist_indi         NUMBER(10) NOT NULL,
    cod_chklist_indi_largo   VARCHAR2(20 BYTE),
    descripcion              VARCHAR2(400 BYTE),
    p_estado                 NUMBER(7),
    cod_mac                  NUMBER(10) NOT NULL,
    cod_grp_diag             NUMBER(10) NOT NULL,
    fecha_ini_vigencia       DATE,
    fecha_fin_vigencia       DATE
);

COMMENT ON COLUMN ONTPFC.pfc_chklist_indicacion.cod_chklist_indi IS
    'Codigo de Indicacion';

COMMENT ON COLUMN ONTPFC.pfc_chklist_indicacion.cod_chklist_indi_largo IS
    'Codigo Largo de Indicacion';

COMMENT ON COLUMN ONTPFC.pfc_chklist_indicacion.descripcion IS
    'Descripcion de la Tabla de Indicacion';

COMMENT ON COLUMN ONTPFC.pfc_chklist_indicacion.p_estado IS
    'Estado de la Indicacion';

COMMENT ON COLUMN ONTPFC.pfc_chklist_indicacion.cod_mac IS
    'Codigo Mac';

COMMENT ON COLUMN ONTPFC.pfc_chklist_indicacion.cod_grp_diag IS
    'Codigo Grupo de diagnostico';

COMMENT ON COLUMN ONTPFC.pfc_chklist_indicacion.fecha_ini_vigencia IS
    'Fecha de Inicio de Vigencia de la Indicacion';

COMMENT ON COLUMN ONTPFC.pfc_chklist_indicacion.fecha_fin_vigencia IS
    'Fecha fin de Vigencia de la Indicacion';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_chklist_indicacion ON
    ONTPFC.pfc_chklist_indicacion (
        cod_chklist_indi
    ASC,
        cod_mac
    ASC,
        cod_grp_diag
    ASC );

ALTER TABLE ONTPFC.pfc_chklist_indicacion
    ADD CONSTRAINT pk_pfc_chklist_indicacion PRIMARY KEY ( cod_chklist_indi,
                                                           cod_mac,
                                                           cod_grp_diag )
        USING INDEX ONTPFC.pk_pfc_chklist_indicacion;

CREATE TABLE ONTPFC.pfc_configuracion_marcador (
    cod_config_marca         NUMBER(10) NOT NULL,
    cod_config_marca_largo   VARCHAR2(15 BYTE),
    cod_examen_med           NUMBER(10) NOT NULL,
    cod_mac                  NUMBER(10) NOT NULL,
    cod_grp_diag             NUMBER(10) NOT NULL,
    p_tipo_marcador          NUMBER(7) NOT NULL,
    p_per_maxima             NUMBER(7) NOT NULL,
    p_per_minima             NUMBER(7) NOT NULL,
    p_estado                 NUMBER(7) NOT NULL
);

COMMENT ON COLUMN ONTPFC.pfc_configuracion_marcador.p_per_maxima IS
    'PERIOCIDAD MAXIMA';

COMMENT ON COLUMN ONTPFC.pfc_configuracion_marcador.p_per_minima IS
    'PERIOCIDAD MINIMA';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_configuracion_m ON
    ONTPFC.pfc_configuracion_marcador (
        cod_config_marca
    ASC );

ALTER TABLE ONTPFC.pfc_configuracion_marcador
    ADD CONSTRAINT pk_pfc_configuracion_m PRIMARY KEY ( cod_config_marca )
        USING INDEX ONTPFC.pk_pfc_configuracion_m;

CREATE TABLE ONTPFC.pfc_consumo_medicamento (
    cod_hist_control_gasto     NUMBER(10) NOT NULL,
    cod_mac                    VARCHAR2(20 BYTE) NOT NULL,
    tipo_encuentro             VARCHAR2(20 BYTE),
    encuentro                  NUMBER(10),
    codigo_sap                 VARCHAR2(20 BYTE),
    descrip_activo_molecula    VARCHAR2(50 BYTE),
    descrip_present_generico   VARCHAR2(50 BYTE),
    prestacion                 VARCHAR2(1000 BYTE),
    cod_afiliado               VARCHAR2(20 BYTE) NOT NULL,
    cod_paciente               VARCHAR2(20 BYTE),
    nombre_paciente            VARCHAR2(100 BYTE),
    clinica                    VARCHAR2(50 BYTE),
    fecha_consumo              DATE,
    descrip_grp_diag           VARCHAR2(100 BYTE),
    cod_diagnostico            VARCHAR2(10 BYTE) NOT NULL,
    descrip_diagnostico        VARCHAR2(50 BYTE),
    linea_tratamiento          NUMBER(10),
    medico_tratante            VARCHAR2(100 BYTE),
    plan                       VARCHAR2(100 BYTE),
    cantidad_consumo           NUMBER(10),
    monto_consumo              NUMBER(10, 2),
    cod_consumo_medicamento    NUMBER(15) NOT NULL
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_consumo_medicamento ON
    ONTPFC.pfc_consumo_medicamento (
        cod_hist_control_gasto
    ASC,
        cod_consumo_medicamento
    ASC,
        cod_mac
    ASC,
        cod_afiliado
    ASC,
        cod_diagnostico
    ASC );

ALTER TABLE ONTPFC.pfc_consumo_medicamento
    ADD CONSTRAINT pk_pfc_consumo_medicamento PRIMARY KEY ( cod_hist_control_gasto,
                                                            cod_consumo_medicamento,
                                                            cod_mac,
                                                            cod_afiliado,
                                                            cod_diagnostico )
        USING INDEX ONTPFC.pk_pfc_consumo_medicamento;

CREATE TABLE ONTPFC.pfc_continuador (
    cod_continuador             NUMBER(10) NOT NULL,
    cod_sol_eva                 NUMBER(10) NOT NULL,
    p_resultado_autorizador     NUMBER(7),
    cod_responsable_monitoreo   NUMBER(10),
    comentario                  VARCHAR2(2000 BYTE),
    p_resultado_monitoreo       NUMBER(7),
    nro_tarea_monitoreo         VARCHAR2(20 BYTE),
    fec_monitoreo               DATE,
    grabar                      VARCHAR2(1 BYTE)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_continuador ON
    ONTPFC.pfc_continuador (
        cod_continuador
    ASC );

ALTER TABLE ONTPFC.pfc_continuador
    ADD CONSTRAINT pk_pfc_continuador PRIMARY KEY ( cod_continuador )
        USING INDEX ONTPFC.pk_pfc_continuador;

CREATE TABLE ONTPFC.pfc_continuador_documento (
    cod_continuador_doc     NUMBER(10) NOT NULL,
    cod_continuador         NUMBER(10) NOT NULL,
    p_tipo_documento        NUMBER(7),
    descripcion_documento   VARCHAR2(100 BYTE),
    fec_carga               DATE,
    url_descarga            VARCHAR2(100 BYTE),
    estado                  VARCHAR2(1 BYTE),
    p_estado                NUMBER(7),
    cod_archivo             NUMBER(10)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_continuador_det ON
    ONTPFC.pfc_continuador_documento (
        cod_continuador_doc
    ASC );

ALTER TABLE ONTPFC.pfc_continuador_documento
    ADD CONSTRAINT pk_pfc_continuador_det PRIMARY KEY ( cod_continuador_doc )
        USING INDEX ONTPFC.pk_pfc_continuador_det;

CREATE TABLE ONTPFC.pfc_correo (
    cod_correo         NUMBER(7) NOT NULL,
    cod_correo_largo   VARCHAR2(15 BYTE),
    destinatario       VARCHAR2(80 BYTE),
    paciente           VARCHAR2(50 BYTE),
    medico_tratante    VARCHAR2(100 BYTE),
    mac_solicitado     VARCHAR2(50 BYTE),
    nro_scg_solben     NUMBER(30),
    documento          VARCHAR2(100 BYTE),
    cod_tipo_correo    NUMBER(7)
);

COMMENT ON COLUMN ONTPFC.pfc_correo.medico_tratante IS
    'MEDICO TRATANTE/PRESCRIPTOR';

COMMENT ON COLUMN ONTPFC.pfc_correo.mac_solicitado IS
    'MEDICAMENTO SOLICITADO';

COMMENT ON COLUMN ONTPFC.pfc_correo.nro_scg_solben IS
    'NRO DE SOLICITUD DE CARTA SOLBEN';

COMMENT ON COLUMN ONTPFC.pfc_correo.documento IS
    'RECETA MEDICA O SUSTENTO';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_correo ON
    ONTPFC.pfc_correo (
        cod_correo
    ASC );

ALTER TABLE ONTPFC.pfc_correo
    ADD CONSTRAINT pk_pfc_correo PRIMARY KEY ( cod_correo )
        USING INDEX ONTPFC.pk_pfc_correo;

CREATE TABLE ONTPFC.pfc_correo_participante (
    cod_correo_participante   NUMBER(19) NOT NULL,
    cod_envio                 NUMBER(19),
    cod_plantilla             VARCHAR2(20 BYTE),
    tipo_envio                NUMBER(19),
    flag_adjunto              NUMBER(1),
    ruta                      VARCHAR2(100 BYTE),
    usrapp                    VARCHAR2(50 BYTE),
    asunto                    VARCHAR2(255 BYTE),
    cuerpo                    VARCHAR2(4000 BYTE),
    destinatario              VARCHAR2(250 BYTE),
    fec_crea                  DATE
);

ALTER TABLE ONTPFC.pfc_correo_participante
    ADD CONSTRAINT pfc_correo_participante_pk PRIMARY KEY ( cod_correo_participante );

CREATE TABLE ONTPFC.pfc_criterio_exclusion (
    cod_criterio_exclu         NUMBER(10) NOT NULL,
    cod_chklist_indi           NUMBER(10) NOT NULL,
    cod_criterio_exclu_largo   VARCHAR2(20 BYTE),
    descripcion                VARCHAR2(1000 BYTE),
    orden                      NUMBER(5),
    p_estado                   NUMBER(7),
    cod_mac                    NUMBER(10) NOT NULL,
    cod_grp_diag               NUMBER(10) NOT NULL
);

COMMENT ON COLUMN ONTPFC.pfc_criterio_exclusion.cod_criterio_exclu IS
    'Codigo corto de criterio de Inclusion';

COMMENT ON COLUMN ONTPFC.pfc_criterio_exclusion.cod_chklist_indi IS
    'Codigo de la tabla de Indicacion';

COMMENT ON COLUMN ONTPFC.pfc_criterio_exclusion.cod_criterio_exclu_largo IS
    'CodigoLargo de criterio de Exclusion';

COMMENT ON COLUMN ONTPFC.pfc_criterio_exclusion.descripcion IS
    'Descripcion del criterio de Exclusion';

COMMENT ON COLUMN ONTPFC.pfc_criterio_exclusion.orden IS
    'Numero de Orden del Criterio';

COMMENT ON COLUMN ONTPFC.pfc_criterio_exclusion.p_estado IS
    'Estado del Criterio';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_criterio_exclusion_ex2 ON
    ONTPFC.pfc_criterio_exclusion (
        cod_criterio_exclu
    ASC,
        cod_chklist_indi
    ASC,
        cod_mac
    ASC,
        cod_grp_diag
    ASC );

ALTER TABLE ONTPFC.pfc_criterio_exclusion
    ADD CONSTRAINT pk_pfc_criterio_exclusion_ex2 PRIMARY KEY ( cod_criterio_exclu,
                                                               cod_chklist_indi,
                                                               cod_mac,
                                                               cod_grp_diag )
        USING INDEX ONTPFC.pk_pfc_criterio_exclusion_ex2;

CREATE TABLE ONTPFC.pfc_criterio_inclusion (
    cod_criterio_inclu         NUMBER(10) NOT NULL,
    cod_chklist_indi           NUMBER(10) NOT NULL,
    cod_criterio_inclu_largo   VARCHAR2(20 BYTE),
    descripcion                VARCHAR2(1000 BYTE),
    orden                      NUMBER(5),
    p_estado                   NUMBER(7),
    cod_mac                    NUMBER(10) NOT NULL,
    cod_grp_diag               NUMBER(10) NOT NULL
);

COMMENT ON COLUMN ONTPFC.pfc_criterio_inclusion.cod_criterio_inclu IS
    'Codigo corto del criterio de Inclusion';

COMMENT ON COLUMN ONTPFC.pfc_criterio_inclusion.cod_chklist_indi IS
    'Codigo de la tabla de Indicacion';

COMMENT ON COLUMN ONTPFC.pfc_criterio_inclusion.cod_criterio_inclu_largo IS
    'Codigo largo del criterio de Inclusion';

COMMENT ON COLUMN ONTPFC.pfc_criterio_inclusion.descripcion IS
    'Descripcion del criterio de Inclusion';

COMMENT ON COLUMN ONTPFC.pfc_criterio_inclusion.orden IS
    'Numero de Orden de criterio de Inclusion';

COMMENT ON COLUMN ONTPFC.pfc_criterio_inclusion.p_estado IS
    'Estado de Criterio de Inclusion';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_criterio_inclusion ON
    ONTPFC.pfc_criterio_inclusion (
        cod_criterio_inclu
    ASC,
        cod_chklist_indi
    ASC,
        cod_mac
    ASC,
        cod_grp_diag
    ASC );

ALTER TABLE ONTPFC.pfc_criterio_inclusion
    ADD CONSTRAINT pk_pfc_criterio_inclusion PRIMARY KEY ( cod_criterio_inclu,
                                                           cod_chklist_indi,
                                                           cod_mac,
                                                           cod_grp_diag )
        USING INDEX ONTPFC.pk_pfc_criterio_inclusion;

CREATE TABLE ONTPFC.pfc_documento_evaluacion (
    cod_documento_evaluacion   NUMBER(15) NOT NULL,
    cod_sol_eva                NUMBER(15) NOT NULL,
    cod_afi_paciente           VARCHAR2(20 BYTE) NOT NULL,
    cod_mac                    NUMBER(10),
    p_tipo_documento           NUMBER(10),
    descripcion_documento      VARCHAR2(100 BYTE),
    fec_carga                  DATE,
    estado                     VARCHAR2(1 BYTE),
    p_estado                   NUMBER(10),
    cod_archivo                NUMBER(10),
    usuario_creacion           NUMBER(10),
    fecha_creacion             DATE,
    usuario_modificacion       NUMBER(10),
    fecha_modificacion         DATE
);

CREATE UNIQUE INDEX ONTPFC.pk_documento_evaluacion ON
    ONTPFC.pfc_documento_evaluacion (
        cod_documento_evaluacion
    ASC,
        cod_sol_eva
    ASC,
        cod_afi_paciente
    ASC );

ALTER TABLE ONTPFC.pfc_documento_evaluacion
    ADD CONSTRAINT pk_documento_evaluacion PRIMARY KEY ( cod_documento_evaluacion,
                                                         cod_sol_eva,
                                                         cod_afi_paciente )
        USING INDEX ONTPFC.pk_documento_evaluacion;

CREATE TABLE ONTPFC.pfc_eva_lider_tum (
    cod_eva_lider_tum      NUMBER(10) NOT NULL,
    fecha_eva              DATE,
    p_resultado_eva        NUMBER(7),
    comentario             NVARCHAR2(200),
    cod_sol_eva            NUMBER(10) NOT NULL,
    cod_lider_tumor        NUMBER(10),
    usuario_creacion       NUMBER(15),
    fecha_creacion         DATE,
    usuario_modificacion   NUMBER(15),
    fecha_modificacion     DATE
);

COMMENT ON COLUMN ONTPFC.pfc_eva_lider_tum.cod_eva_lider_tum IS
    'Codigo de evaluacion Lider Tumor';

COMMENT ON COLUMN ONTPFC.pfc_eva_lider_tum.fecha_eva IS
    'Fecha donde se hizo la evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_eva_lider_tum.p_resultado_eva IS
    'Resultado de la Evaluacion Lider Tumor';

COMMENT ON COLUMN ONTPFC.pfc_eva_lider_tum.comentario IS
    'Comentario de la Evaluacion Lider Tumor';

COMMENT ON COLUMN ONTPFC.pfc_eva_lider_tum.cod_sol_eva IS
    'Codigo de la Tabla de pfc_solicitu_evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_eva_lider_tum.cod_lider_tumor IS
    'Codigo del usuario que realizó la evaluacion de lider Tumor';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_eva_lider_tum ON
    ONTPFC.pfc_eva_lider_tum (
        cod_eva_lider_tum
    ASC );

ALTER TABLE ONTPFC.pfc_eva_lider_tum
    ADD CONSTRAINT pk_pfc_eva_lider_tum PRIMARY KEY ( cod_eva_lider_tum )
        USING INDEX ONTPFC.pk_pfc_eva_lider_tum;

CREATE TABLE ONTPFC.pfc_evolucion (
    cod_evolucion           NUMBER(20) NOT NULL,
    cod_monitoreo           NUMBER(20) NOT NULL,
    nro_desc_evolucion      VARCHAR2(20 BYTE),
    cod_mac                 NUMBER(7),
    p_res_evolucion         NUMBER(7),
    cod_hist_linea_trat     NUMBER(20),
    fec_monitoreo           DATE,
    fec_prox_monitoreo      DATE,
    p_motivo_inactivacion   NUMBER(7),
    fec_inactivacion        DATE,
    observacion             VARCHAR2(3000 BYTE),
    p_tolerancia            NUMBER(7),
    p_toxicidad             NUMBER(7),
    p_grado                 NUMBER(7),
    p_resp_clinica          NUMBER(7),
    p_aten_alerta           NUMBER(7),
    existe_toxicidad        VARCHAR2(1 BYTE),
    nro_evolucion           NUMBER(7),
    estado                  NUMBER(1),
    usuario_crea            VARCHAR2(25 BYTE),
    fecha_crea              DATE,
    usuario_modif           VARCHAR2(25 BYTE),
    fecha_modif             DATE,
    fec_ultimo_consumo      DATE,
    cant_ultimo_consumo     NUMBER(7),
    gasto_ultimo_consumo    NUMBER
);

ALTER TABLE ONTPFC.pfc_evolucion
    ADD CONSTRAINT pfc_evolucion_pk PRIMARY KEY ( cod_evolucion );

CREATE TABLE ONTPFC.pfc_evolucion_marcador (
    cod_evolucion_marcador   NUMBER(20) NOT NULL,
    cod_evolucion            NUMBER(20) NOT NULL,
    cod_marcador             NUMBER(10),
    cod_resultado            NUMBER(5),
    resultado                VARCHAR2(100 BYTE),
    fec_resultado            DATE,
    tiene_reg_hc             VARCHAR2(1 BYTE),
    p_per_minima             NUMBER(10),
    p_per_maxima             NUMBER(10),
    p_tipo_ingreso_res       NUMBER(10),
    desc_per_minima          VARCHAR2(100 BYTE),
    desc_per_maxima          VARCHAR2(100 BYTE),
    descripcion              VARCHAR2(1000 BYTE),
    unidad_medida            VARCHAR2(50 BYTE),
    usuario_crea             VARCHAR2(25 BYTE),
    fecha_crea               DATE,
    usuario_modif            VARCHAR2(25 BYTE),
    fecha_modif              DATE
);

ALTER TABLE ONTPFC.pfc_evolucion_marcador
    ADD CONSTRAINT pfc_evolucion_marcador_pk PRIMARY KEY ( cod_evolucion_marcador );

CREATE TABLE ONTPFC.pfc_examen_medico_detalle (
    cod_examen_med_det   NUMBER(10) NOT NULL,
    cod_examen_med       NUMBER(10) NOT NULL,
    p_tipo_ingreso_res   NUMBER(7) NOT NULL,
    unidad_medida        VARCHAR2(50 BYTE),
    rango                VARCHAR2(50 BYTE),
    valor_fijo           VARCHAR2(50 BYTE),
    p_estado             NUMBER(10),
    rango_minimo         NUMBER(7),
    rango_maximo         NUMBER(7),
    usuario_crea         VARCHAR2(25 BYTE),
    fecha_crea           DATE,
    usuario_modif        VARCHAR2(25 BYTE),
    fecha_modif          DATE
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_examen_medico_detalle ON
    ONTPFC.pfc_examen_medico_detalle (
        cod_examen_med_det
    ASC );

ALTER TABLE ONTPFC.pfc_examen_medico_detalle
    ADD CONSTRAINT pk_pfc_examen_medico_detalle PRIMARY KEY ( cod_examen_med_det )
        USING INDEX ONTPFC.pk_pfc_examen_medico_detalle;

CREATE TABLE ONTPFC.pfc_examen_medico_marcador (
    cod_examen_med         NUMBER(10) NOT NULL,
    cod_examen_med_largo   VARCHAR2(20 BYTE),
    descripcion            VARCHAR2(200 BYTE),
    p_tipo_examen          NUMBER(7) NOT NULL,
    p_estado               NUMBER(7) NOT NULL,
    usuario_crea           VARCHAR2(25 BYTE),
    fecha_crea             DATE,
    usuario_modif          VARCHAR2(25 BYTE),
    fecha_modif            DATE
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_examen_medico_marcador ON
    ONTPFC.pfc_examen_medico_marcador (
        cod_examen_med
    ASC );

ALTER TABLE ONTPFC.pfc_examen_medico_marcador
    ADD CONSTRAINT pk_pfc_examen_medico_marcador PRIMARY KEY ( cod_examen_med )
        USING INDEX ONTPFC.pk_pfc_examen_medico_marcador;

CREATE TABLE ONTPFC.pfc_ficha_tecnica (
    cod_ficha_tecnica   NUMBER(10) NOT NULL,
    cod_version         VARCHAR2(20 BYTE),
    nombre_archivo      VARCHAR2(100 BYTE),
    fec_inicio_vig      DATE,
    fec_fin_vig         DATE,
    p_estado            NUMBER(7),
    cod_mac             NUMBER(10) NOT NULL,
    cod_file_upload     NUMBER(10)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_ficha_tecnica_maestro ON
    ONTPFC.pfc_ficha_tecnica (
        cod_mac
    ASC,
        cod_ficha_tecnica
    ASC );

ALTER TABLE ONTPFC.pfc_ficha_tecnica
    ADD CONSTRAINT pk_pfc_ficha_tecnica_maestro PRIMARY KEY ( cod_mac,
                                                              cod_ficha_tecnica )
        USING INDEX ONTPFC.pk_pfc_ficha_tecnica_maestro;

CREATE TABLE ONTPFC.pfc_grupo (
    cod_grupo   NUMBER(7) NOT NULL,
    codigo      VARCHAR2(2 BYTE),
    nombre      VARCHAR2(100 BYTE),
    estado      CHAR(1 BYTE)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_grupo ON
    ONTPFC.pfc_grupo (
        cod_grupo
    ASC );

ALTER TABLE ONTPFC.pfc_grupo
    ADD CONSTRAINT pk_pfc_grupo PRIMARY KEY ( cod_grupo )
        USING INDEX ONTPFC.pk_pfc_grupo;

CREATE TABLE ONTPFC.pfc_grupo_diagnostico (
    cod_grp_diag   NUMBER(10) NOT NULL,
    nombre         VARCHAR2(50 BYTE)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_grupo_diag ON
    ONTPFC.pfc_grupo_diagnostico (
        cod_grp_diag
    ASC );

ALTER TABLE ONTPFC.pfc_grupo_diagnostico
    ADD CONSTRAINT pk_pfc_grupo_diag PRIMARY KEY ( cod_grp_diag )
        USING INDEX ONTPFC.pk_pfc_grupo_diag;

CREATE TABLE ONTPFC.pfc_hist_control_gasto (
    cod_hist_control_gasto   NUMBER(10) NOT NULL,
    fecha_hora_carga         DATE,
    responsable_carga        NUMBER(10),
    p_estado_carga           NUMBER(10),
    registro_total           NUMBER(8),
    registro_cargado         NUMBER(8),
    registro_error           NUMBER(8),
    ver_log                  NUMBER(1),
    cod_archivo_log          NUMBER(10),
    descarga                 NUMBER(1),
    cod_archivo_descarga     NUMBER(10),
    fecha_creacion           DATE,
    usuario_creacion         NUMBER(15),
    fecha_modificacion       DATE,
    usuario_modificacion     NUMBER(15)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_hist_control_gasto ON
    ONTPFC.pfc_hist_control_gasto (
        cod_hist_control_gasto
    ASC );

ALTER TABLE ONTPFC.pfc_hist_control_gasto
    ADD CONSTRAINT pk_pfc_hist_control_gasto PRIMARY KEY ( cod_hist_control_gasto )
        USING INDEX ONTPFC.pk_pfc_hist_control_gasto;

CREATE TABLE ONTPFC.pfc_hist_linea_tratamiento (
    cod_hist_linea_trat     NUMBER(7) NOT NULL,
    cod_sol_eva             NUMBER(10),
    cod_mac                 NUMBER(7),
    linea_trat              VARCHAR2(2 BYTE),
    fec_aprobacion          DATE,
    fec_inicio              DATE,
    fec_fin                 DATE,
    p_curso                 NUMBER(7),
    p_tipo_tumor            NUMBER(7),
    p_resp_alcanzada        NUMBER(7),
    p_estado                NUMBER(7),
    p_motivo_inactivacion   NUMBER(7),
    fec_inactivacion        DATE,
    monto_autorizado        NUMBER(15),
    cmp                     VARCHAR2(20 BYTE),
    medico_tratante         VARCHAR2(100 BYTE),
    p_lugar_progresion      NUMBER(7),
    cod_afiliado            VARCHAR2(20 BYTE),
    nro_informe             VARCHAR2(50 BYTE),
    fec_emision             DATE,
    cod_auditor_eval        NUMBER
);

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.cod_hist_linea_trat IS
    'CAMPO AUTOGENERADO';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.cod_sol_eva IS
    'Codigo de la solicitud de evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.cod_mac IS
    'Codigo de Medicamento Mac';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.linea_trat IS
    'Codigo de linea de tratamiento';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.fec_aprobacion IS
    'Fecha de aprobacion';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.fec_inicio IS
    'fecha de inicio';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.fec_fin IS
    'fecha fin';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.p_curso IS
    'Numero de curso';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.p_tipo_tumor IS
    'Tipo de tumor';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.p_resp_alcanzada IS
    'Respuesta Alcanzada';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.p_estado IS
    'Estado de la linea de tratamiento';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.p_motivo_inactivacion IS
    'Motivo de Inactivacion';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.fec_inactivacion IS
    'Fecha de Inactivacion';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.monto_autorizado IS
    'Monto Autorizado';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.cmp IS
    'Codigo de CMP del medico tratante';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.medico_tratante IS
    'Nombres y apellidos del medico tratante';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.p_lugar_progresion IS
    'Lugar de progresion';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.cod_afiliado IS
    'codigo de afiliado(Paciente)';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.fec_emision IS
    'Fecha de emision';

COMMENT ON COLUMN ONTPFC.pfc_hist_linea_tratamiento.cod_auditor_eval IS
    'Codigo del autorizador de la evaluacion';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_linea_tratamiento ON
    ONTPFC.pfc_hist_linea_tratamiento (
        cod_hist_linea_trat
    ASC );

CREATE TABLE ONTPFC.pfc_indicadores (
    indicador              VARCHAR2(3 BYTE),
    cantidad               NUMBER,
    quiebre                VARCHAR2(1000 BYTE),
    fec_ano                NUMBER,
    fec_mes                NUMBER,
    usuario_creacion       VARCHAR2(25 BYTE),
    fecha_creacion         DATE,
    usuario_modificacion   VARCHAR2(25 BYTE),
    fecha_modificacion     DATE,
    numerador              NUMBER,
    denominador            NUMBER
);

CREATE TABLE ONTPFC.pfc_log_seguimiento (
    codigo        VARCHAR2(20 BYTE),
    descripcion   VARCHAR2(3000 BYTE)
);

CREATE TABLE ONTPFC.pfc_mac (
    cod_mac            NUMBER(7) NOT NULL,
    cod_mac_largo      VARCHAR2(10 BYTE),
    descripcion        VARCHAR2(100 BYTE),
    p_tipo_mac         NUMBER(7),
    fec_inscripcion    DATE,
    fec_inicio_vig     DATE,
    fec_fin_vig        DATE,
    fec_creacion       TIMESTAMP(0),
    usr_creacion       VARCHAR2(25 BYTE),
    fec_modificacion   TIMESTAMP(0),
    usr_modificacion   VARCHAR2(25 BYTE),
    p_estado_mac       NUMBER(7)
);

CREATE UNIQUE INDEX ONTPFC.pk_cod_mac ON
    ONTPFC.pfc_mac (
        cod_mac
    ASC );

ALTER TABLE ONTPFC.pfc_mac
    ADD CONSTRAINT pk_cod_mac PRIMARY KEY ( cod_mac )
        USING INDEX ONTPFC.pk_cod_mac;

CREATE TABLE ONTPFC.pfc_medicamento_nuevo (
    cod_medic_nuevo       NUMBER(10) NOT NULL,
    cod_sol_eva           NUMBER(10),
    nro_linea_tra         NUMBER(2),
    cod_sev_chk_req_pac   NUMBER(10),
    cod_sev_con_pac       NUMBER(10),
    cod_sev_chk_pac       NUMBER(7),
    cod_sev_lin_tra       NUMBER(10),
    cod_sev_ana_con       NUMBER(10),
    cod_reporte           NUMBER(20),
    reporte_pdf           VARCHAR2(200 BYTE),
    chk_req_pac_grabar    VARCHAR2(1 BYTE)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_mac_nuevo ON
    ONTPFC.pfc_medicamento_nuevo (
        cod_medic_nuevo
    ASC );

ALTER TABLE ONTPFC.pfc_medicamento_nuevo
    ADD CONSTRAINT pk_pfc_mac_nuevo PRIMARY KEY ( cod_medic_nuevo )
        USING INDEX ONTPFC.pk_pfc_mac_nuevo;

CREATE TABLE ONTPFC.pfc_metastasis (
    cod_metastasis       NUMBER(10) NOT NULL,
    p_linea_metastasis   NUMBER(7),
    p_lugar_metastasis   NUMBER(7),
    cod_sev_con_pac      NUMBER(10) NOT NULL,
    estado               CHAR(1 BYTE)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_metastasis ON
    ONTPFC.pfc_metastasis (
        cod_metastasis
    ASC );

ALTER TABLE ONTPFC.pfc_metastasis
    ADD CONSTRAINT pk_pfc_metastasis PRIMARY KEY ( cod_metastasis )
        USING INDEX ONTPFC.pk_pfc_metastasis;

CREATE TABLE ONTPFC.pfc_monitoreo (
    cod_monitoreo        NUMBER(10) NOT NULL,
    cod_sol_eva          NUMBER(20) NOT NULL,
    cod_desc_monitoreo   VARCHAR2(20 BYTE),
    p_estado_monitoreo   NUMBER(7),
    fec_monitoreo        DATE,
    fec_prox_monitoreo   DATE,
    cod_sol_eva_cont     NUMBER(20)
);

ALTER TABLE ONTPFC.pfc_monitoreo
    ADD CONSTRAINT pfc_monitoreo_pk PRIMARY KEY ( cod_monitoreo );

CREATE TABLE ONTPFC.pfc_parametro (
    cod_parametro   NUMBER(7) NOT NULL,
    cod_grupo       NUMBER(7),
    codigo          VARCHAR2(20 BYTE),
    nombre          VARCHAR2(100 BYTE),
    valor1          VARCHAR2(500 BYTE),
    estado          CHAR(1 BYTE)
);

COMMENT ON COLUMN ONTPFC.pfc_parametro.cod_parametro IS
    'Codigo de Parametro';

COMMENT ON COLUMN ONTPFC.pfc_parametro.cod_grupo IS
    'Codigo de Grupo';

COMMENT ON COLUMN ONTPFC.pfc_parametro.nombre IS
    'descripcion del valor';

COMMENT ON COLUMN ONTPFC.pfc_parametro.valor1 IS
    'codigo 2, para otra consulta';

COMMENT ON COLUMN ONTPFC.pfc_parametro.estado IS
    'Estado del parametro';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_parametro ON
    ONTPFC.pfc_parametro (
        cod_parametro
    ASC );

ALTER TABLE ONTPFC.pfc_parametro
    ADD CONSTRAINT pk_pfc_parametro PRIMARY KEY ( cod_parametro )
        USING INDEX ONTPFC.pk_pfc_parametro;

CREATE TABLE ONTPFC.pfc_participante (
    cod_participante         NUMBER(10) NOT NULL,
    cod_usuario              NUMBER(10),
    estado_participante      CHAR(1 BYTE),
    cmp_medico               VARCHAR2(10 BYTE),
    nombre_firma             VARCHAR2(50 BYTE),
    cod_rol                  NUMBER(7),
    codigo_archivo_firma     NUMBER,
    correo_electronico       VARCHAR2(100 BYTE),
    nombres                  VARCHAR2(50 BYTE),
    apellidos                VARCHAR2(50 BYTE),
    cod_participante_largo   VARCHAR2(5 BYTE),
    p_estado                 NUMBER(10),
    coordinador              NUMBER(1)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_participante ON
    ONTPFC.pfc_participante (
        cod_participante
    ASC );

ALTER TABLE ONTPFC.pfc_participante
    ADD CONSTRAINT pk_pfc_participante PRIMARY KEY ( cod_participante )
        USING INDEX ONTPFC.pk_pfc_participante;

CREATE TABLE ONTPFC.pfc_participante_grupo_diag (
    cod_participante   NUMBER(10) NOT NULL,
    cod_grp_diag       VARCHAR2(20 BYTE),
    p_rango_edad       NUMBER(10)
);

CREATE TABLE ONTPFC.pfc_participantes_cmac (
    cod_participante_cmac   NUMBER(10) NOT NULL,
    cod_programacion_cmac   NUMBER(10) NOT NULL,
    cod_usuario_cmac        NUMBER(10)
);

COMMENT ON COLUMN ONTPFC.pfc_participantes_cmac.cod_participante_cmac IS
    'Codigo de participante Cmac';

COMMENT ON COLUMN ONTPFC.pfc_participantes_cmac.cod_programacion_cmac IS
    'Codigo de programacion Cmac';

COMMENT ON COLUMN ONTPFC.pfc_participantes_cmac.cod_usuario_cmac IS
    'Codigo de usuario Cmac';

CREATE UNIQUE INDEX ONTPFC.pfc_participantes_cmac_pk ON
    ONTPFC.pfc_participantes_cmac (
        cod_participante_cmac
    ASC );

ALTER TABLE ONTPFC.pfc_participantes_cmac
    ADD CONSTRAINT pfc_participantes_cmac_pk PRIMARY KEY ( cod_participante_cmac )
        USING INDEX ONTPFC.pfc_participantes_cmac_pk;

CREATE TABLE ONTPFC.pfc_persona (
    cod_persona   NUMBER(10),
    nombre        VARCHAR2(100 BYTE),
    edad          NUMBER(3)
);

CREATE TABLE ONTPFC.pfc_preferencia_inst (
    cod_pref_inst            NUMBER(10) NOT NULL,
    cod_pref_inst_largo      VARCHAR2(20 BYTE),
    cod_mac                  NUMBER(10),
    cod_grp_diag             VARCHAR2(20 BYTE),
    p_condicion_cancer       NUMBER(7),
    detalle_condicion        VARCHAR2(2000 BYTE),
    presentac_no_permitida   VARCHAR2(200 BYTE),
    p_tipo_tratamiento       NUMBER(7),
    p_estado                 NUMBER(7),
    fecha_creacion           DATE,
    usuario_creacion         NUMBER(15),
    fecha_modificacion       DATE,
    usuario_modificacion     NUMBER(15)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_pref_inst_masestro ON
    ONTPFC.pfc_preferencia_inst (
        cod_pref_inst
    ASC );

ALTER TABLE ONTPFC.pfc_preferencia_inst
    ADD CONSTRAINT pk_pfc_pref_inst_masestro PRIMARY KEY ( cod_pref_inst )
        USING INDEX ONTPFC.pk_pfc_pref_inst_masestro;

CREATE TABLE ONTPFC.pfc_producto_asociado (
    cod_mac                NUMBER(10) NOT NULL,
    cod_producto           VARCHAR2(20 BYTE) NOT NULL,
    descripcion_generica   VARCHAR2(200 BYTE),
    nombre_comercial       VARCHAR2(150 BYTE),
    p_laboratorio          NUMBER(7),
    p_estado               NUMBER(7)
);

CREATE TABLE ONTPFC.pfc_programacion_cmac (
    cod_programacion_cmac    NUMBER(10) NOT NULL,
    fec_reunion              DATE,
    hora_reunion             VARCHAR2(10 BYTE),
    p_estado_prog_cmac       NUMBER(7),
    cod_acta                 VARCHAR2(10 BYTE),
    reporte_acta_escaneada   VARCHAR2(100 BYTE),
    cod_acta_ftp             NUMBER(10),
    cod_archivo_prog         NUMBER(10)
);

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac.cod_programacion_cmac IS
    'Codigo de programacion Cmac';

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac.fec_reunion IS
    'Fecha de reunion';

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac.hora_reunion IS
    'Hora de Reunion';

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac.p_estado_prog_cmac IS
    'Estado de programacion Cmac';

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac.cod_acta IS
    'Codigo de Acta escaneada';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_programacion_cmac ON
    ONTPFC.pfc_programacion_cmac (
        cod_programacion_cmac
    ASC );

ALTER TABLE ONTPFC.pfc_programacion_cmac
    ADD CONSTRAINT pk_pfc_programacion_cmac PRIMARY KEY ( cod_programacion_cmac )
        USING INDEX ONTPFC.pk_pfc_programacion_cmac;

CREATE TABLE ONTPFC.pfc_programacion_cmac_det (
    cod_prog_cmac_det       NUMBER(10) NOT NULL,
    cod_programacion_cmac   NUMBER(10) NOT NULL,
    cod_sol_eva             NUMBER(10) NOT NULL,
    observacion             VARCHAR2(4000 BYTE),
    cod_res_evaluacion      NUMBER(10),
    cod_grabado             NUMBER(7)
);

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac_det.cod_prog_cmac_det IS
    'Codigo de detalle de programacion';

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac_det.cod_programacion_cmac IS
    'Codigo de programacion Cmac';

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac_det.cod_sol_eva IS
    'Codigo de solicitu de evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac_det.observacion IS
    'Observacion de detalle de programacion';

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac_det.cod_res_evaluacion IS
    'codigo de resultado de la evaluacion Cmac';

COMMENT ON COLUMN ONTPFC.pfc_programacion_cmac_det.cod_grabado IS
    'Codigo de grabado';

CREATE UNIQUE INDEX ONTPFC.ix_pfc_prog_cmac_det ON
    ONTPFC.pfc_programacion_cmac_det (
        cod_sol_eva
    ASC );

CREATE TABLE ONTPFC.pfc_reporte_solic_autoriza (
    fec_mes                        NUMBER,
    fec_ano                        NUMBER,
    cod_sol_eva                    VARCHAR2(20 BYTE),
    cod_paciente                   VARCHAR2(20 BYTE),
    cod_afiliado                   VARCHAR2(20 BYTE),
    fecha_afiliacion               DATE,
    tipo_afiliacion                VARCHAR2(20 BYTE),
    doc_identidad                  NUMBER,
    paciente                       VARCHAR2(100 BYTE),
    sexo                           VARCHAR2(20 BYTE),
    edad                           NUMBER,
    medicamento                    VARCHAR2(100 BYTE),
    fecha_receta                   DATE,
    cie10                          VARCHAR2(100 BYTE),
    diagnostico                    VARCHAR2(20 BYTE),
    grupo_diagnostico              VARCHAR2(20 BYTE),
    plan                           VARCHAR2(100 BYTE),
    estadio                        VARCHAR2(100 BYTE),
    tnm                            VARCHAR2(100 BYTE),
    linea                          VARCHAR2(20 BYTE),
    clinica                        VARCHAR2(100 BYTE),
    cmp                            VARCHAR2(100 BYTE),
    mec_tratante_presc             VARCHAR2(100 BYTE),
    cod_scg_solben                 VARCHAR2(20 BYTE),
    fec_reg_scg_solben             DATE,
    hor_reg_scg_solben             VARCHAR2(20 BYTE),
    tipo_scg_solben                VARCHAR2(20 BYTE),
    estado_scg_solben              VARCHAR2(100 BYTE),
    cod_sol_pre                    NUMBER,
    fec_reg_sol_pre                DATE,
    hor_reg_sol_pre                VARCHAR2(20 BYTE),
    estado_sol_pre                 VARCHAR2(20 BYTE),
    autor_pertinencia_sol_pre      VARCHAR2(100 BYTE),
    fec_hor_reg_sol_eva            TIMESTAMP,
    contratante                    VARCHAR2(100 BYTE),
    cumple_pref_inst               VARCHAR2(20 BYTE),
    indic_check_list_pac           VARCHAR2(1000 BYTE),
    cumple_indic_check_list_pac    VARCHAR2(20 BYTE),
    pertinencia                    VARCHAR2(100 BYTE),
    cond_paciente                  VARCHAR2(100 BYTE),
    tiempo_uso                     NUMBER,
    estado_sol_eva                 VARCHAR2(200 BYTE),
    rol_resp_pend_eva              VARCHAR2(100 BYTE),
    autor_pertinencia_sol_eva      VARCHAR2(100 BYTE),
    fec_eva_autor_perti            DATE,
    resultado_eva_autor_perti      VARCHAR2(1000 BYTE),
    lider_tumor                    VARCHAR2(20 BYTE),
    fec_eva_lider_tumor            DATE,
    resultado_eva_lider_tumor      VARCHAR2(1000 BYTE),
    correo_env_cmac                VARCHAR2(20 BYTE),
    fec_reunion_cmac               DATE,
    resultado_eva_cmac             VARCHAR2(1000 BYTE),
    nro_carta_garantia             NUMBER,
    tiempo_autor_pertinencia_eva   NUMBER,
    tiempo_lider_tumor             NUMBER,
    tiempo_cmac                    NUMBER,
    tiempo_total                   NUMBER
);

CREATE TABLE ONTPFC.pfc_reporte_solic_monitoreo (
    fec_mes                     NUMBER,
    fec_ano                     NUMBER,
    cod_sol_eva                 NUMBER,
    cod_paciente                VARCHAR2(20 BYTE),
    cod_afiliado                VARCHAR2(20 BYTE),
    fecha_afiliacion            DATE,
    tipo_afiliacion             VARCHAR2(20 BYTE),
    doc_identidad               VARCHAR2(20 BYTE),
    paciente                    VARCHAR2(100 BYTE),
    sexo                        VARCHAR2(20 BYTE),
    edad                        NUMBER,
    medicamento                 VARCHAR2(100 BYTE),
    cie10                       VARCHAR2(100 BYTE),
    diagnostico                 VARCHAR2(20 BYTE),
    grupo_diagnostico           VARCHAR2(20 BYTE),
    plan                        VARCHAR2(100 BYTE),
    estadio                     VARCHAR2(100 BYTE),
    tnm                         VARCHAR2(100 BYTE),
    linea                       VARCHAR2(20 BYTE),
    clinica                     VARCHAR2(100 BYTE),
    cmp                         VARCHAR2(100 BYTE),
    mec_tratante_presc          VARCHAR2(100 BYTE),
    cod_scg_solben              VARCHAR2(20 BYTE),
    estado_sol_eva              VARCHAR2(200 BYTE),
    fec_aprob_sol_eva           DATE,
    autor_pertinencia_sol_eva   VARCHAR2(100 BYTE),
    nro_carta_garantia          VARCHAR2(20 BYTE),
    fec_ini_trat                DATE,
    nro_evolucion               VARCHAR2(20 BYTE),
    cod_resp_monitoreo          NUMBER,
    fec_prog_monitoreo          DATE,
    fec_real_monitoreo          DATE,
    estado_monitoreo            VARCHAR2(200 BYTE),
    tolerancia                  VARCHAR2(200 BYTE),
    toxicidad                   VARCHAR2(200 BYTE),
    respuesta_clinica           VARCHAR2(200 BYTE),
    atencion_alertas            VARCHAR2(200 BYTE),
    fec_ultimo_consumo          DATE,
    cant_ultimo_consumo         NUMBER,
    resultado_evolucion         VARCHAR2(200 BYTE),
    fec_inactivacion            DATE,
    motivo_inactivacion         VARCHAR2(200 BYTE),
    ejecutivo_monitoreo         VARCHAR2(200 BYTE),
    estado_seguimiento          VARCHAR2(200 BYTE),
    fecha_registro_se           DATE,
    codigo_tarea_monitoreo      VARCHAR2(100 BYTE)
);

CREATE TABLE ONTPFC.pfc_resultado_basal_marcador (
    cod_resultado_marcador   NUMBER(10) NOT NULL,
    cod_sev_con_pac          NUMBER(7) NOT NULL,
    cod_config_marca         NUMBER(10) NOT NULL,
    resultado                VARCHAR2(500 BYTE),
    fec_resultado            DATE
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_resultado_marcador ON
    ONTPFC.pfc_resultado_basal_marcador (
        cod_resultado_marcador
    ASC );

ALTER TABLE ONTPFC.pfc_resultado_basal_marcador
    ADD CONSTRAINT pk_pfc_resultado_marcador PRIMARY KEY ( cod_resultado_marcador )
        USING INDEX ONTPFC.pk_pfc_resultado_marcador;

CREATE TABLE ONTPFC.pfc_seg_ejecutivo (
    cod_seg_ejecutivo         NUMBER(20) NOT NULL,
    cod_monitoreo             NUMBER(20) NOT NULL,
    cod_ejecutivo_monitoreo   NUMBER(20) NOT NULL,
    nom_ejecutivo_monitoreo   VARCHAR2(100 BYTE),
    fecha_registro            DATE,
    p_estado_seguimiento      NUMBER(10),
    detalle_evento            VARCHAR2(500 BYTE),
    visto_resp_monitoreo      NUMBER(1),
    usuario_crea              VARCHAR2(25 BYTE),
    fecha_crea                DATE,
    usuario_modif             VARCHAR2(25 BYTE),
    fecha_modif               DATE
);

ALTER TABLE ONTPFC.pfc_seg_ejecutivo
    ADD CONSTRAINT pfc_seg_ejecutivo_pk PRIMARY KEY ( cod_seg_ejecutivo );

CREATE TABLE ONTPFC.pfc_seguimiento (
    cod_seguimiento         NUMBER(7) NOT NULL,
    estado_sol_evaluacion   NUMBER(7),
    fec_estado              DATE,
    rol_resp_estado         NUMBER(10),
    usr_resp_estado         NUMBER(10),
    rol_resp_reg_estado     NUMBER(10),
    usr_resp_reg_estado     NUMBER(10),
    cod_sol_evaluacion      NUMBER(10) NOT NULL
);

COMMENT ON COLUMN ONTPFC.pfc_seguimiento.cod_seguimiento IS
    'Codigo de seguimiento';

COMMENT ON COLUMN ONTPFC.pfc_seguimiento.estado_sol_evaluacion IS
    'Estado de la solicitud de evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_seguimiento.fec_estado IS
    'fecha de estado de la evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_seguimiento.rol_resp_estado IS
    'Codigo de rol del responsable';

COMMENT ON COLUMN ONTPFC.pfc_seguimiento.usr_resp_estado IS
    'Codigo de usuario del responsable';

COMMENT ON COLUMN ONTPFC.pfc_seguimiento.rol_resp_reg_estado IS
    'Codigo de rol del responsable de registro';

COMMENT ON COLUMN ONTPFC.pfc_seguimiento.usr_resp_reg_estado IS
    'Codigo de usuario del responsable de registro';

COMMENT ON COLUMN ONTPFC.pfc_seguimiento.cod_sol_evaluacion IS
    'Codigo de solicitud de evaluacion';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_seguimiento ON
    ONTPFC.pfc_seguimiento (
        cod_seguimiento
    ASC );

ALTER TABLE ONTPFC.pfc_seguimiento
    ADD CONSTRAINT pk_pfc_seguimiento PRIMARY KEY ( cod_seguimiento )
        USING INDEX ONTPFC.pk_pfc_seguimiento;

CREATE TABLE ONTPFC.pfc_sev_analisis_conclusion (
    cod_sev_ana_con           NUMBER(10) NOT NULL,
    p_cumple_chklist_per      NUMBER(7),
    p_cumple_pref_insti       NUMBER(7),
    cod_mac                   NUMBER(10),
    p_pertinencia             NUMBER(7),
    p_condicion_paciente      NUMBER(7),
    p_tiempo_uso              NUMBER(7),
    p_resultado_autorizador   NUMBER(7),
    comentario                VARCHAR2(2000 BYTE),
    grabar                    CHAR(1 BYTE),
    fec_aprobacion            DATE
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_analisis_conclusion ON
    ONTPFC.pfc_sev_analisis_conclusion (
        cod_sev_ana_con
    ASC );

ALTER TABLE ONTPFC.pfc_sev_analisis_conclusion
    ADD CONSTRAINT pk_pfc_analisis_conclusion PRIMARY KEY ( cod_sev_ana_con )
        USING INDEX ONTPFC.pk_pfc_analisis_conclusion;

CREATE TABLE ONTPFC.pfc_sev_checklist_pac (
    cod_sev_chklist_pac    NUMBER(7) NOT NULL,
    cod_chklist_indi       NUMBER(10) NOT NULL,
    comentario             VARCHAR2(1000 BYTE),
    p_chklist_perfil_pac   NUMBER(7),
    grabar                 CHAR(1 BYTE),
    p_cumple_chklist_per   NUMBER(7),
    cod_mac                NUMBER(10),
    cod_grp_diag           NUMBER(10)
);

COMMENT ON COLUMN ONTPFC.pfc_sev_checklist_pac.cod_sev_chklist_pac IS
    'CHECKLIST DEL PACIENTE PASO 4 CODIGO';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_sev_checklist_paciente ON
    ONTPFC.pfc_sev_checklist_pac (
        cod_sev_chklist_pac
    ASC );

ALTER TABLE ONTPFC.pfc_sev_checklist_pac
    ADD CONSTRAINT pk_pfc_sev_checklist_paciente PRIMARY KEY ( cod_sev_chklist_pac )
        USING INDEX ONTPFC.pk_pfc_sev_checklist_paciente;

CREATE TABLE ONTPFC.pfc_sev_checklist_pac_det (
    cod_sev_chklist_pac   NUMBER(10) NOT NULL,
    cod_criterio_inclu    NUMBER(10),
    cod_criterio_exclu    NUMBER(10),
    p_criterio_inclu      NUMBER(7),
    p_criterio_exclu      NUMBER(7),
    cod_chklist_indi      NUMBER(10),
    cod_mac               NUMBER(10),
    cod_grp_diag          NUMBER(10)
);

CREATE TABLE ONTPFC.pfc_sev_checklist_pac_det_exc (
    cod_sev_chklist_pac   NUMBER(10) NOT NULL,
    cod_criterio_exclu    NUMBER(10),
    p_criterio_exclu      NUMBER(7)
);

CREATE TABLE ONTPFC.pfc_sev_checklist_pac_det_inc (
    cod_sev_chklist_pac   NUMBER(10) NOT NULL,
    cod_criterio_inclu    NUMBER(10),
    p_criterio_inclu      NUMBER(7)
);

CREATE TABLE ONTPFC.pfc_sev_checklist_req_pac (
    cod_sev_chk_req_pac   NUMBER(10) NOT NULL,
    grabar                CHAR(1 BYTE)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_sev_check_req_pac ON
    ONTPFC.pfc_sev_checklist_req_pac (
        cod_sev_chk_req_pac
    ASC );

ALTER TABLE ONTPFC.pfc_sev_checklist_req_pac
    ADD CONSTRAINT pk_pfc_sev_check_req_pac PRIMARY KEY ( cod_sev_chk_req_pac )
        USING INDEX ONTPFC.pk_pfc_sev_check_req_pac;

CREATE TABLE ONTPFC.pfc_sev_condicion_basal_pac (
    cod_sev_con_pac      NUMBER(10) NOT NULL,
    antecedentes_imp     VARCHAR2(4000 BYTE),
    p_ecog               NUMBER(7),
    p_existe_toxicidad   NUMBER(7),
    p_tipo_toxicidad     NUMBER(7),
    grabar               CHAR(1 BYTE)
);

COMMENT ON COLUMN ONTPFC.pfc_sev_condicion_basal_pac.cod_sev_con_pac IS
    'CONDICION BASAL DEL PACIENTE PASO 3  CODIGO';

CREATE UNIQUE INDEX ONTPFC.pk_pfc_sev_cond_basal_pac ON
    ONTPFC.pfc_sev_condicion_basal_pac (
        cod_sev_con_pac
    ASC );

ALTER TABLE ONTPFC.pfc_sev_condicion_basal_pac
    ADD CONSTRAINT pk_pfc_sev_cond_basal_pac PRIMARY KEY ( cod_sev_con_pac )
        USING INDEX ONTPFC.pk_pfc_sev_cond_basal_pac;

CREATE TABLE ONTPFC.pfc_sev_linea_tratamiento (
    cod_sev_lin_tra        NUMBER(10) NOT NULL,
    cod_pref_inst          NUMBER(10),
    p_nro_curso            NUMBER(7),
    p_tipo_tumor           NUMBER(7),
    p_lugar_progresion     NUMBER(7),
    p_resp_alcanzada       NUMBER(7),
    cod_mac                NUMBER(10),
    p_pref_inst            NUMBER(7),
    grabar                 CHAR(1 BYTE),
    p_cumple_pref_inst     NUMBER(7),
    descripcion            VARCHAR2(300 BYTE),
    p_condicion            NUMBER(7),
    fecha_creacion         DATE,
    usuario_creacion       NUMBER(15),
    fecha_modificacion     DATE,
    usuario_modificacion   NUMBER(15)
);

CREATE TABLE ONTPFC.pfc_solben (
    cod_scg                       NUMBER(10) NOT NULL,
    cod_scg_solben                VARCHAR2(20 BYTE) NOT NULL,
    cod_clinica                   VARCHAR2(20 BYTE) NOT NULL,
    cod_afi_paciente              VARCHAR2(20 BYTE) NOT NULL,
    edad_paciente                 NUMBER(3) NOT NULL,
    des_contratante               NVARCHAR2(100) NOT NULL,
    des_plan                      NVARCHAR2(20) NOT NULL,
    cod_diagnostico               VARCHAR2(20 BYTE) NOT NULL,
    cmp_medico                    VARCHAR2(10 BYTE) NOT NULL,
    fec_quimio                    DATE,
    fec_hosp_inicio               DATE,
    fec_hosp_fin                  DATE,
    tipo_scg_solben               NUMBER(7) NOT NULL,
    estado_scg                    VARCHAR2(20 BYTE) NOT NULL,
    desc_medicamento              NVARCHAR2(300),
    desc_esquema                  NVARCHAR2(300),
    total_presupuesto             NUMBER(10, 2) NOT NULL,
    desc_procedimiento            NVARCHAR2(300),
    person_contacto               NVARCHAR2(150),
    obs_clinica                   VARCHAR2(300 BYTE),
    ind_valida                    NUMBER(1) NOT NULL,
    fec_scg_solben                DATE NOT NULL,
    hora_scg_solben               VARCHAR2(20 BYTE),
    tx_dato_adic1                 NVARCHAR2(20),
    tx_dato_adic2                 NVARCHAR2(20),
    tx_dato_adic3                 NVARCHAR2(20),
    fec_receta                    DATE,
    medico_tratante_prescriptor   VARCHAR2(200 BYTE),
    fec_afiliacion                DATE NOT NULL,
    nro_cg                        VARCHAR2(20 BYTE),
    fec_cg                        VARCHAR2(20 BYTE),
    cod_grp_diag                  VARCHAR2(20 BYTE),
    fec_estado_scg                TIMESTAMP(0),
    p_tipo_medico                 NUMBER(7),
    fecha_creacion                DATE,
    usuario_creacion              NUMBER(15),
    fecha_modificacion            DATE,
    usuario_modificacion          NUMBER(15),
    desc_grp_diag                 VARCHAR2(50 BYTE)
);

CREATE UNIQUE INDEX ONTPFC.pk_pfc_solben ON
    ONTPFC.pfc_solben (
        cod_scg
    ASC );

ALTER TABLE ONTPFC.pfc_solben
    ADD CONSTRAINT pk_pfc_solben PRIMARY KEY ( cod_scg )
        USING INDEX ONTPFC.pk_pfc_solben;

CREATE TABLE ONTPFC.pfc_solicitud_evaluacion (
    cod_sol_eva                     NUMBER(10) NOT NULL,
    fec_sol_eva                     TIMESTAMP(0),
    p_tipo_eva                      NUMBER(7),
    p_estado_sol_eva                NUMBER(7),
    p_rol_resp_pendiente_eva        NUMBER(7),
    estado_correo_env_cmac          NUMBER(7),
    estado_correo_env_lider_tumor   NUMBER(7),
    cod_sol_pre                     NUMBER(20) NOT NULL,
    cod_desc_sol_eva                VARCHAR2(20 BYTE),
    p_diagnostico                   NUMBER(7),
    fecha_creacion                  DATE,
    usuario_creacion                NUMBER(15),
    fecha_modificacion              DATE,
    usuario_modificacion            NUMBER(15),
    fec_finalizar_estado            DATE,
    codigo_envio_env_mac            NUMBER(19),
    codigo_envio_env_lider_tumor    NUMBER(19),
    codigo_envio_env_aler_monit     NUMBER(19),
    cod_informe_auto                NUMBER(10),
    cod_auto_perte                  NUMBER(8),
    cod_lider_tumor                 NUMBER(8),
    fec_evaluacion_autorizador      DATE,
    p_evaluacion_autorizador        NUMBER(7),
    fec_evaluacion_lider_tumor      DATE,
    p_evaluacion_lider_tumor        NUMBER(7)
);

COMMENT ON COLUMN ONTPFC.pfc_solicitud_evaluacion.cod_sol_eva IS
    'Codigo de solicitud de evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_evaluacion.fec_sol_eva IS
    'fecha de solicitud de evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_evaluacion.p_tipo_eva IS
    'Tipo de evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_evaluacion.p_estado_sol_eva IS
    'estado de evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_evaluacion.p_rol_resp_pendiente_eva IS
    'Rol responsable de la evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_evaluacion.estado_correo_env_cmac IS
    'estado de del correo de enviado Cmac';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_evaluacion.estado_correo_env_lider_tumor IS
    'estado de del correo de enviado Lider tumor';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_evaluacion.cod_sol_pre IS
    'Codigo de solicitud preliminar';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_evaluacion.cod_desc_sol_eva IS
    'Codigo largo de la solicitud de evaluacion';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_evaluacion.p_diagnostico IS
    'Codigo de diagnostico';

CREATE UNIQUE INDEX ONTPFC.ix_pfc_sol_eva_cod_sol_pre ON
    ONTPFC.pfc_solicitud_evaluacion (
        cod_sol_pre
    ASC );

CREATE UNIQUE INDEX ONTPFC.pk_pfc_solicitud_evaluacion ON
    ONTPFC.pfc_solicitud_evaluacion (
        cod_sol_eva
    ASC );

ALTER TABLE ONTPFC.pfc_solicitud_evaluacion
    ADD CONSTRAINT pk_pfc_solicitud_evaluacion PRIMARY KEY ( cod_sol_eva )
        USING INDEX ONTPFC.pk_pfc_solicitud_evaluacion;

CREATE TABLE ONTPFC.pfc_solicitud_preliminar (
    cod_sol_pre            NUMBER(20) NOT NULL,
    cod_scg                NUMBER(10) NOT NULL,
    fec_sol_pre            DATE,
    cod_mac                NUMBER(7),
    p_estado_sol_pre       NUMBER(10),
    fecha_creacion         DATE,
    usuario_creacion       NUMBER(15),
    fecha_modificacion     DATE,
    usuario_modificacion   NUMBER(15),
    fec_finalizar_estado   DATE
);

COMMENT ON COLUMN ONTPFC.pfc_solicitud_preliminar.cod_sol_pre IS
    'Codigo de solicitud preliminar';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_preliminar.cod_scg IS
    'Codigo de solicitud de carta de garantia Solben';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_preliminar.fec_sol_pre IS
    'Fecha de solicitud preliminar';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_preliminar.cod_mac IS
    'Codigo de medicamento Mac';

COMMENT ON COLUMN ONTPFC.pfc_solicitud_preliminar.p_estado_sol_pre IS
    'Estado de solicitud preliminar';

CREATE UNIQUE INDEX ONTPFC.ix_pfc_sol_pre_id_scg_solben ON
    ONTPFC.pfc_solicitud_preliminar (
        cod_scg
    ASC );

CREATE UNIQUE INDEX ONTPFC.pk_pfc_solicitud_preliminar ON
    ONTPFC.pfc_solicitud_preliminar (
        cod_sol_pre
    ASC );

ALTER TABLE ONTPFC.pfc_solicitud_preliminar
    ADD CONSTRAINT pk_pfc_solicitud_preliminar PRIMARY KEY ( cod_sol_pre )
        USING INDEX ONTPFC.pk_pfc_solicitud_preliminar;

CREATE TABLE ONTPFC.pfc_tipo_correo (
    cod_tipo_correo       NUMBER(7) NOT NULL,
    descrip_tipo_correo   VARCHAR2(75 BYTE),
    asunto                VARCHAR2(200 BYTE),
    cuerpo                VARCHAR2(2000 BYTE),
    firma                 VARCHAR2(100 BYTE),
    estado                CHAR(1 BYTE)
);

CREATE UNIQUE INDEX ONTPFC.pk_tipo_correo ON
    ONTPFC.pfc_tipo_correo (
        cod_tipo_correo
    ASC );

ALTER TABLE ONTPFC.pfc_tipo_correo
    ADD CONSTRAINT pk_tipo_correo PRIMARY KEY ( cod_tipo_correo )
        USING INDEX ONTPFC.pk_tipo_correo;

ALTER TABLE ONTPFC.pfc_evolucion_marcador
    ADD CONSTRAINT fk_evo_marcador FOREIGN KEY ( cod_evolucion )
        REFERENCES ONTPFC.pfc_evolucion ( cod_evolucion )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_evolucion
    ADD CONSTRAINT fk_evo_mon FOREIGN KEY ( cod_monitoreo )
        REFERENCES ONTPFC.pfc_monitoreo ( cod_monitoreo )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_configuracion_marcador
    ADD CONSTRAINT fk_pfc_config_marca_examen_med FOREIGN KEY ( cod_examen_med )
        REFERENCES ONTPFC.pfc_examen_medico_marcador ( cod_examen_med )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_configuracion_marcador
    ADD CONSTRAINT fk_pfc_config_marca_mac_mae FOREIGN KEY ( cod_mac )
        REFERENCES ONTPFC.pfc_mac ( cod_mac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_continuador_documento
    ADD CONSTRAINT fk_pfc_cont_doc_cont FOREIGN KEY ( cod_continuador )
        REFERENCES ONTPFC.pfc_continuador ( cod_continuador )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_continuador
    ADD CONSTRAINT fk_pfc_cont_solicitud_eva FOREIGN KEY ( cod_sol_eva )
        REFERENCES ONTPFC.pfc_solicitud_evaluacion ( cod_sol_eva )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_correo
    ADD CONSTRAINT fk_pfc_correo_tipo_correo FOREIGN KEY ( cod_tipo_correo )
        REFERENCES ONTPFC.pfc_tipo_correo ( cod_tipo_correo )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_sev_checklist_pac_det
    ADD CONSTRAINT fk_pfc_criterio_exclusion_ex3 FOREIGN KEY ( cod_criterio_exclu,
                                                               cod_chklist_indi,
                                                               cod_mac,
                                                               cod_grp_diag )
        REFERENCES ONTPFC.pfc_criterio_exclusion ( cod_criterio_exclu,
                                                   cod_chklist_indi,
                                                   cod_mac,
                                                   cod_grp_diag )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_sev_checklist_pac_det
    ADD CONSTRAINT fk_pfc_criterio_inclusion FOREIGN KEY ( cod_criterio_inclu,
                                                           cod_chklist_indi,
                                                           cod_mac,
                                                           cod_grp_diag )
        REFERENCES ONTPFC.pfc_criterio_inclusion ( cod_criterio_inclu,
                                                   cod_chklist_indi,
                                                   cod_mac,
                                                   cod_grp_diag )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_documento_evaluacion
    ADD CONSTRAINT fk_pfc_doc_eva_sol_eva FOREIGN KEY ( cod_sol_eva )
        REFERENCES ONTPFC.pfc_solicitud_evaluacion ( cod_sol_eva )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_eva_lider_tum
    ADD CONSTRAINT fk_pfc_eva_lid_tum_sol_eva FOREIGN KEY ( cod_sol_eva )
        REFERENCES ONTPFC.pfc_solicitud_evaluacion ( cod_sol_eva )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_examen_medico_detalle
    ADD CONSTRAINT fk_pfc_exam_med_det_exam_med FOREIGN KEY ( cod_examen_med )
        REFERENCES ONTPFC.pfc_examen_medico_marcador ( cod_examen_med )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_examen_medico_detalle
    ADD CONSTRAINT fk_pfc_examen_medico_detalle FOREIGN KEY ( p_estado )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_hist_linea_tratamiento
    ADD CONSTRAINT fk_pfc_his_line_trat_mac_maes FOREIGN KEY ( cod_mac )
        REFERENCES ONTPFC.pfc_mac ( cod_mac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_hist_linea_tratamiento
    ADD CONSTRAINT fk_pfc_his_line_trat_sol_eva FOREIGN KEY ( cod_sol_eva )
        REFERENCES ONTPFC.pfc_solicitud_evaluacion ( cod_sol_eva )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_medicamento_nuevo
    ADD CONSTRAINT fk_pfc_mac_nue_sev_chk_pac FOREIGN KEY ( cod_sev_chk_pac )
        REFERENCES ONTPFC.pfc_sev_checklist_pac ( cod_sev_chklist_pac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_medicamento_nuevo
    ADD CONSTRAINT fk_pfc_mac_nue_sev_con_pac FOREIGN KEY ( cod_sev_con_pac )
        REFERENCES ONTPFC.pfc_sev_condicion_basal_pac ( cod_sev_con_pac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_medicamento_nuevo
    ADD CONSTRAINT fk_pfc_med_nue_sev_ana_con FOREIGN KEY ( cod_sev_ana_con )
        REFERENCES ONTPFC.pfc_sev_analisis_conclusion ( cod_sev_ana_con )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_medicamento_nuevo
    ADD CONSTRAINT fk_pfc_med_nue_sev_chk_req_pac FOREIGN KEY ( cod_sev_chk_req_pac )
        REFERENCES ONTPFC.pfc_sev_checklist_req_pac ( cod_sev_chk_req_pac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_medicamento_nuevo
    ADD CONSTRAINT fk_pfc_med_nue_sol_eva FOREIGN KEY ( cod_sol_eva )
        REFERENCES ONTPFC.pfc_solicitud_evaluacion ( cod_sol_eva )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_metastasis
    ADD CONSTRAINT fk_pfc_met_sev_con_pac FOREIGN KEY ( cod_sev_con_pac )
        REFERENCES ONTPFC.pfc_sev_condicion_basal_pac ( cod_sev_con_pac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_examen_medico_marcador
    ADD CONSTRAINT fk_pfc_parametro_est FOREIGN KEY ( p_estado )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_producto_asociado
    ADD CONSTRAINT fk_pfc_parametro_est_pa FOREIGN KEY ( p_estado )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_parametro
    ADD CONSTRAINT fk_pfc_parametro_grupo FOREIGN KEY ( cod_grupo )
        REFERENCES ONTPFC.pfc_grupo ( cod_grupo )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_producto_asociado
    ADD CONSTRAINT fk_pfc_parametro_lb_pa FOREIGN KEY ( p_laboratorio )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_configuracion_marcador
    ADD CONSTRAINT fk_pfc_parametro_pest FOREIGN KEY ( p_estado )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_configuracion_marcador
    ADD CONSTRAINT fk_pfc_parametro_pmax FOREIGN KEY ( p_per_maxima )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_configuracion_marcador
    ADD CONSTRAINT fk_pfc_parametro_pmin FOREIGN KEY ( p_per_minima )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_examen_medico_marcador
    ADD CONSTRAINT fk_pfc_parametro_texam FOREIGN KEY ( p_tipo_examen )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_configuracion_marcador
    ADD CONSTRAINT fk_pfc_parametro_tmar FOREIGN KEY ( p_tipo_marcador )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_examen_medico_detalle
    ADD CONSTRAINT fk_pfc_parametro_tresul FOREIGN KEY ( p_tipo_ingreso_res )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_participante_grupo_diag
    ADD CONSTRAINT fk_pfc_parti_grupo_diag FOREIGN KEY ( cod_participante )
        REFERENCES ONTPFC.pfc_participante ( cod_participante )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_participante
    ADD CONSTRAINT fk_pfc_participante_est_pa2 FOREIGN KEY ( p_estado )
        REFERENCES ONTPFC.pfc_parametro ( cod_parametro )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_participantes_cmac
    ADD CONSTRAINT fk_pfc_participantes_cmac FOREIGN KEY ( cod_programacion_cmac )
        REFERENCES ONTPFC.pfc_programacion_cmac ( cod_programacion_cmac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_preferencia_inst
    ADD CONSTRAINT fk_pfc_pref_inst_maestro_mac FOREIGN KEY ( cod_mac )
        REFERENCES ONTPFC.pfc_mac ( cod_mac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_programacion_cmac_det
    ADD CONSTRAINT fk_pfc_pro_cmacd_pfc_pro_cmac FOREIGN KEY ( cod_programacion_cmac )
        REFERENCES ONTPFC.pfc_programacion_cmac ( cod_programacion_cmac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_programacion_cmac_det
    ADD CONSTRAINT fk_pfc_pro_cmacd_pfc_sol_eva FOREIGN KEY ( cod_sol_eva )
        REFERENCES ONTPFC.pfc_solicitud_evaluacion ( cod_sol_eva )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_producto_asociado
    ADD CONSTRAINT fk_pfc_prod_asoc_mac FOREIGN KEY ( cod_mac )
        REFERENCES ONTPFC.pfc_mac ( cod_mac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_resultado_basal_marcador
    ADD CONSTRAINT fk_pfc_res_mar_sev_cond_pac FOREIGN KEY ( cod_sev_con_pac )
        REFERENCES ONTPFC.pfc_sev_condicion_basal_pac ( cod_sev_con_pac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_solicitud_preliminar
    ADD CONSTRAINT fk_pfc_sol_pre_mac FOREIGN KEY ( cod_mac )
        REFERENCES ONTPFC.pfc_mac ( cod_mac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_ficha_tecnica
    ADD CONSTRAINT fk_fic_tec_mae_mac_mae FOREIGN KEY ( cod_mac )
        REFERENCES ONTPFC.pfc_mac ( cod_mac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_monitoreo
    ADD CONSTRAINT fk_mon_soleva FOREIGN KEY ( cod_sol_eva )
        REFERENCES ONTPFC.pfc_solicitud_evaluacion ( cod_sol_eva )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_archivo_complicacion
    ADD CONSTRAINT fk_pfc_arc_com_mac FOREIGN KEY ( cod_mac )
        REFERENCES ONTPFC.pfc_mac ( cod_mac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_sev_checklist_pac_det_exc
    ADD CONSTRAINT fk_pfc_chk_pac_det_chk_pac_ex FOREIGN KEY ( cod_sev_chklist_pac )
        REFERENCES ONTPFC.pfc_sev_checklist_pac ( cod_sev_chklist_pac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_checklist_requisito
    ADD CONSTRAINT fk_pfc_chk_req_chk_req_pac FOREIGN KEY ( cod_sev_chk_req_pac )
        REFERENCES ONTPFC.pfc_sev_checklist_req_pac ( cod_sev_chk_req_pac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_chklist_indicacion
    ADD CONSTRAINT fk_pfc_chklist_indi_mac_maes FOREIGN KEY ( cod_mac )
        REFERENCES ONTPFC.pfc_mac ( cod_mac )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_criterio_inclusion
    ADD CONSTRAINT fk_pfc_chklist_indicacion FOREIGN KEY ( cod_chklist_indi,
                                                           cod_mac,
                                                           cod_grp_diag )
        REFERENCES ONTPFC.pfc_chklist_indicacion ( cod_chklist_indi,
                                                   cod_mac,
                                                   cod_grp_diag )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_sev_checklist_pac
    ADD CONSTRAINT fk_pfc_chklist_indicacion_2 FOREIGN KEY ( cod_chklist_indi,
                                                             cod_mac,
                                                             cod_grp_diag )
        REFERENCES ONTPFC.pfc_chklist_indicacion ( cod_chklist_indi,
                                                   cod_mac,
                                                   cod_grp_diag )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_criterio_exclusion
    ADD CONSTRAINT fk_pfc_chklist_indicacion_ex FOREIGN KEY ( cod_chklist_indi,
                                                              cod_mac,
                                                              cod_grp_diag )
        REFERENCES ONTPFC.pfc_chklist_indicacion ( cod_chklist_indi,
                                                   cod_mac,
                                                   cod_grp_diag )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_solicitud_preliminar
    ADD CONSTRAINT fk_pfc_sol_pre_solben FOREIGN KEY ( cod_scg )
        REFERENCES ONTPFC.pfc_solben ( cod_scg )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_solicitud_evaluacion
    ADD CONSTRAINT fk_pfc_solic_eva_sol_pre FOREIGN KEY ( cod_sol_pre )
        REFERENCES ONTPFC.pfc_solicitud_preliminar ( cod_sol_pre )
    NOT DEFERRABLE;

ALTER TABLE ONTPFC.pfc_seg_ejecutivo
    ADD CONSTRAINT fk_segeje_mon FOREIGN KEY ( cod_monitoreo )
        REFERENCES ONTPFC.pfc_monitoreo ( cod_monitoreo )
    NOT DEFERRABLE;