--------------------------------------------------------
--  DDL for Function FN_PFC_S_MEDICO_AUDITOR
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "ONTPFC"."FN_PFC_S_MEDICO_AUDITOR" ( CODIGO_EVALUACION IN NUMBER) RETURN VARCHAR2 
IS
COD NUMBER;
pn_cod_resultado  NUMBER;
pv_msg_resultado  VARCHAR2(100);
v_nombre_completo VARCHAR2(150);

BEGIN
v_nombre_completo:='';
pn_cod_resultado:=1;
pv_msg_resultado:='';

pck_pfc_reporte_evaluacion.sp_pfc_s_pdf_medico_auditor(CODIGO_EVALUACION,
                                                v_nombre_completo,
                                                pn_cod_resultado,
                                                pv_msg_resultado);
RETURN v_nombre_completo;
END FN_PFC_S_MEDICO_AUDITOR;

/
--------------------------------------------------------
--  DDL for Function FN_PFC_S_VALIDACION
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "ONTPFC"."FN_PFC_S_VALIDACION" 
( PV_CMP_MED      IN varchar2,
  PV_COD_GRP_DIAG IN varchar2)
RETURN VARCHAR2 IS
      cod_respuesta    NUMBER;
      PN_COD_RESULTADO NUMBER;
      PV_MSG_RESULTADO VARCHAR2(400);
      PN_COD_ROL_LIDER_TUM NUMBER;
      PN_COD_USR_LIDER_TUM NUMBER;
      PV_USR_LIDER_TUM     VARCHAR2(100);
      PN_CMP_LIDER_TUMOR   NUMBER;
BEGIN
    PN_COD_RESULTADO := 1;
    PV_MSG_RESULTADO := 'valor inicial';

    ONTPFC.PCK_PFC_UTIL.SP_PFC_S_VALID_LIDER_TUMOR(PV_CMP_MED,
                                                   PV_COD_GRP_DIAG,
                                                   PN_COD_ROL_LIDER_TUM,
                                                   PN_COD_USR_LIDER_TUM,
                                                   PV_USR_LIDER_TUM,
                                                   PN_CMP_LIDER_TUMOR,
                                                   PN_COD_RESULTADO,
                                                   PV_MSG_RESULTADO); 
    
    
    cod_respuesta := PN_CMP_LIDER_TUMOR;
    
  RETURN cod_respuesta;
END FN_PFC_S_VALIDACION;

/
--------------------------------------------------------
--  DDL for Function FN_PFC_S_VALIDACION_FECHA
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "ONTPFC"."FN_PFC_S_VALIDACION_FECHA" (
       PV_FECHA   VARCHAR2
) RETURN NUMBER IS
  V_FECHA       DATE;

BEGIN
  SELECT TO_DATE(PV_FECHA,'DD-MM-YYYY') INTO V_FECHA FROM DUAL;
  RETURN 0;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 1;
END FN_PFC_S_VALIDACION_FECHA;


/
--------------------------------------------------------
--  DDL for Function FN_PFC_S_VALIDACION_FECHA_HORA
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "ONTPFC"."FN_PFC_S_VALIDACION_FECHA_HORA" (
       PV_FECHA   VARCHAR2
) RETURN NUMBER IS
  V_FECHA       DATE;

BEGIN
  SELECT TO_DATE(PV_FECHA,'DD-MM-YYYY HH24:MI:SS') INTO V_FECHA FROM DUAL;
  RETURN 0;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 1;
END FN_PFC_S_VALIDACION_FECHA_HORA;


/
--------------------------------------------------------
--  DDL for Function FN_PFC_S_VALIDACION_HORA
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "ONTPFC"."FN_PFC_S_VALIDACION_HORA" (
       PV_FECHA   VARCHAR2
) RETURN NUMBER IS
  V_FECHA       DATE;

BEGIN
  SELECT TO_DATE(PV_FECHA,'HH24:MI:SS') INTO V_FECHA FROM DUAL;
  RETURN 0;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 1;
END FN_PFC_S_VALIDACION_HORA;


/
--------------------------------------------------------
--  DDL for Function IS_NUMBER
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "ONTPFC"."IS_NUMBER" (p_string IN VARCHAR2)
   RETURN INT
IS
   v_new_num NUMBER;
BEGIN
   v_new_num := TO_NUMBER(p_string);
   RETURN 1;
EXCEPTION
WHEN VALUE_ERROR THEN
   RETURN 0;
END is_number;

/
