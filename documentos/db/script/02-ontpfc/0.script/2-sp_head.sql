--------------------------------------------------------
--  DDL for Package PCK_PFC_CONSTANTE
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "ONTPFC"."PCK_PFC_CONSTANTE" AS

  /* TODO enter package declarations (types, exceptions, methods etc) here */
  TYPE TYPCUR IS REF CURSOR;
  PV_EXITOSO CONSTANT VARCHAR2(45):='No errors';
  PV_COMBO_TIPO_SCG CONSTANT VARCHAR2(5):='01';
  PV_COMBO_AUDITOR_PERTENENCIA CONSTANT VARCHAR2(5):='02';
  PV_STATUS_PERSON_ACTIVE CONSTANT VARCHAR2(1) := '1';
  PV_STATUS_USER_ROL_ACTIVE CONSTANT VARCHAR2(1) := '1';
  PV_STATUS_USER_ACTIVE CONSTANT VARCHAR2(1) := '1';
  PV_STATUS_PART_ACTIVE CONSTANT VARCHAR2(1) := '1';
  -- CAMPO GRABAR DE MEDICAMENTO NUEVO
  PV_GRABAR_PASO      CONSTANT VARCHAR2(1) := '1';
  PV_CREAR_PASO       CONSTANT VARCHAR2(1) := '0';
  -- CAMPO ESTADO CONFIGURACION MAC
  PN_ESTADO_CONFIG_MAC_ACTIVO    CONSTANT NUMBER(2) := 8;
  PN_ESTADO_CONFIG_MAC_INACTIVO  CONSTANT NUMBER(2) := 9;
  -- CORREO CMAC SI Y NO
  PN_CODIGO_CORREO_CMAC_SI      CONSTANT NUMBER(2) := 30;
  PN_CODIGO_CORREO_CMAC_NO      CONSTANT NUMBER(2) := 31;
  -- CODIGO TIPO SCG SOLBEN
  PN_IND_VALIDA_ACTIVO  CONSTANT NUMBER(1) := 0;
  PN_TIPO_FARMACIA_COMPLEJA CONSTANT NUMBER(1) := 1;
  PN_TIPO_QUIMIO_AMBULATORIA CONSTANT NUMBER(1) := 2;
  PN_TIPO_HOSPITALIZACION CONSTANT NUMBER(1) := 3;
  PN_P_ESTADO_PENDIENTE_INSCRIP CONSTANT NUMBER(7) := 6;
  PN_P_ESTADO_PENDIENTE CONSTANT NUMBER(7) := 4;
  PN_P_ESTADO_ATENDIDO CONSTANT NUMBER(7) := 5;
  PV_ESTADO_PENDIENTE_INSCRIP CONSTANT VARCHAR2(40) := 'PENDIENTE DE INSCRIPCION MAC';
  PV_ESTADO_PENDIENTE CONSTANT VARCHAR2(10) := 'PENDIENTE';
  PN_ESTADO_PREFE_INSTI CONSTANT NUMBER(5) := 82;
  PV_MENSAJE_SIN_RESULTADO CONSTANT VARCHAR2(30) := 'NO SE ENCONTRARON RESULTADOS';
  PV_MENSAJE_CONSULTAR_EXITO CONSTANT VARCHAR2(30) := 'CONSULTA EXITOSA';
  PV_MENSAJE_INSERTAR_EXITO CONSTANT VARCHAR2(30) := 'REGISTRO EXITOSO';
  PV_MENSAJE_ACTUALIZAR_EXITO CONSTANT VARCHAR2(30) := 'ACTUALIZACION EXITOSA';
  PV_MENSAJE_ELIMINAR_EXITO CONSTANT VARCHAR2(30) := 'ELIMINACION EXITOSA';
  PV_ESTADO CONSTANT VARCHAR2(1) := '1';
  PV_CHKLIST_PAC_TIPO_DOC  CONSTANT VARCHAR2(10) := '30';
  PV_CHKLIST_PAC_BOTON CONSTANT VARCHAR2(10) := '31';
  PN_CHKLIST_TIPO_DOC_OTROS CONSTANT NUMBER (7):= 87;
  PN_STATUS_PEND_PAS_CUATRO CONSTANT NUMBER(7):= 18;
  PN_STATUS_PEND_PAS_TRES CONSTANT NUMBER(7):= 17;
  PN_GRABAR CONSTANT CHAR(1):= '1';
  PN_CODIGO_ROL_MEDICO_AUDITOR CONSTANT NUMBER(7) := 12;
  PV_ESTADO_PARAMETRO CONSTANT VARCHAR2(1) := '1';
  PV_ESTADO_GRUPO CONSTANT VARCHAR2(1) := '1';
  -- ESTADO LINEA DE TRATAMIENTO HISTORICO
  PN_ESTADO_LIN_TRA_ACTIVO CONSTANT NUMBER(3) := 45;
  PN_ESTADO_LIN_TRA_INACTIVO CONSTANT NUMBER(3) := 46;
  -- ESTADO CHECKLIST DOCUMENTO - PASO 2 Y MEDICAMENTO CONTINUADOR
  PN_TIPO_DOC_RECETA_MED CONSTANT NUMBER(3) := 84;
  PN_TIPO_DOC_INFO_MED   CONSTANT NUMBER(3) := 85;
  PN_TIPO_DOC_EVA_GERIAT CONSTANT NUMBER(3) := 86;
  PN_TIPO_DOC_OTROS      CONSTANT NUMBER(3) := 87;
  -- ESTADO DE CARGA BOTON CARGAR O ELIMINAR
  PN_CHKLIST_BOTON_CARGAR CONSTANT NUMBER(7) := 88;
  PN_CHKLIST_BOTON_ELIMINAR CONSTANT NUMBER(7) := 89;
  -- ESTADO DE LA SOLICITUD DE EVALUACION
  PN_ESTADO_PENDIENTE_EVA CONSTANT NUMBER(2)     :=13;
  PN_ESTADO_PENDIENTE_RES_MON CONSTANT NUMBER(2) := 14;
  PN_ESTADO_PENDIENTE_PASO1 CONSTANT NUMBER(2)   := 15;
  PN_ESTADO_PENDIENTE_PASO2 CONSTANT NUMBER(2)   := 16;
  PN_ESTADO_PENDIENTE_PASO3 CONSTANT NUMBER(2)   := 17;
  PN_ESTADO_PENDIENTE_PASO4 CONSTANT NUMBER(2)   := 18;
  PN_ESTADO_PENDIENTE_PASO5 CONSTANT NUMBER(2)   := 19;
  PN_ESTADO_APROBADO_AUTORIZ CONSTANT NUMBER(2)  := 20;
  PN_ESTADO_RECHAZADO_AUTORIZ CONSTANT NUMBER(2)  := 21;
  PN_ESTADO_OBSERVADO_AUTORIZ CONSTANT NUMBER(2)  := 22;
  PN_ESTADO_APROBADO_LIDER_T  CONSTANT NUMBER(2)  := 23;
  PN_ESTADO_RECHAZADO_LIDER_T CONSTANT NUMBER(2)  := 24;
  PN_ESTADO_OBSERVADO_LIDER_T CONSTANT NUMBER(2)  := 25;
  PN_ESTADO_APROBADO_CMAC     CONSTANT NUMBER(2)  := 26;
  PN_ESTADO_RECHAZADO_CMAC    CONSTANT NUMBER(2)  := 27;
  -- ROL RESPONSABLE DE EVALUACION   
  PN_ROL_RESP_AUTOR_PERTE CONSTANT NUMBER(2):=43;
  -- LARGO PERMITIDO PARA EL CODIGO DE SOLICITUD EVALUACION
  PN_COD_LARGO CONSTANT NUMBER(2):=10;
  PN_COD_LARGO_MONITOREO CONSTANT NUMBER(6) := 10;
  PV_INCIO_TAREA_MONIT   CONSTANT VARCHAR2(3) := '001';
  -- COD PARAMETRO DE SOLBEN INSERT PRELIMINAR
  PN_CODIGO_RANGO_TIEMPO_SOLBEN CONSTANT NUMBER(3) := 117;
  PN_RANGO_TIEMPO_SOLBEN_HOSPIT CONSTANT NUMBER(3) := 345;
  PN_RANGO_TIEM_SOLBEN_FC_QUIMIO CONSTANT NUMBER(3) := 117;
  -- COD PARAMETRO DE SOLBEN INSERT PRELIMINAR 2
  PN_RANG_TIEM_SOLBEN_FC_QUIMIO CONSTANT NUMBER(3) := 5;
  PN_RANG_TIEM_SOLBEN_HOSPIT CONSTANT NUMBER(3) := 15;
  -- COD PARAMETRO EXAMENES MEDICOS
  PN_ESTADO_EXAMEN_MEDICO CONSTANT NUMBER(3) := 58;
  PN_CODIGO_VARIABLES_FIJOS CONSTANT NUMBER(3) := 123;
  -- RADIO BUTTON DESCRIPCION
  PV_RADIOBUTTON_SI CONSTANT VARCHAR2(2) := 'SI';
  PV_RADIOBUTTON_NO CONSTANT VARCHAR2(2) := 'NO';
  -- RESPUESTA CMAC
  PV_CODIGO_ROL_MIENBRO_CMAC CONSTANT NUMBER:=9;  
  PV_CODIGO_LONGITUD_CMAC CONSTANT NUMBER := 10;
  PV_CODIGO_GRABAR_OK CONSTANT NUMBER := 0;
  -- ESTADO DE MONITOREO
  PN_ESTADO_PENDIENTE_MONITOREO CONSTANT NUMBER(3) := 118;
  PN_ESTADO_ATENDIDO_MONITOREO CONSTANT NUMBER(3) := 119;
  PN_ESTADO_PEND_RESUL_MONITOREO CONSTANT NUMBER(3) := 120;
  PN_ESTADO_PEND_INFO_MONITOREO CONSTANT NUMBER(3) := 121;
  -- ESTADO DE INDICACION Y CRITERIO DE INCLUSION Y EXCLUSION
  PN_ESTADO_VIGENTE_INDICACION   CONSTANT NUMBER(4) := 8;
  PN_ESTADO_VIGENTE_INCLUSION    CONSTANT NUMBER(4) := 8;
  PN_ESTADO_VIGENTE_EXCLUSION    CONSTANT NUMBER(4) := 8;
  -- TIPO MEDICO QUE VIENE DEL SOLBEN
  PN_CODIGO_MEDICO_PRESCRIPTOR CONSTANT NUMBER(4) := 208;
  PN_CODIGO_MEDICO_TRATANTE    CONSTANT NUMBER(4) := 209;
  -- CONDICION DEL CANCER
  PN_CONDICION_NO_REGISTRADO   CONSTANT NUMBER(4) := 292;
  -- ROL ASEGURAMIENTO
  PN_CODIGO_LIDER_TUMOR        CONSTANT NUMBER(2) := 8;
  -- CONTROL DE TAMAÑO DE LONGITUD DE CAMPO
  PN_LONG_COMENTARIO_LIDER_TUM CONSTANT VARCHAR(3) := 200;
  -- TAMAÑO CODIGO SOLICITUDES
  PN_LONG_SOLIC_PRELIMINAR     CONSTANT NUMBER(10) := 10;
  -- USUARIOS ACTIVOS
  PN_USUARIO_ACTIVO CONSTANT NUMBER(2) := 1;
  PN_USUARIO_INACTIVO CONSTANT NUMBER(2) := 0;
  -- CODIGO PROXIMO MONITOREO
  PN_CODIGO_PROX_MONIT CONSTANT NUMBER(3) := 241;
  -- ESTADO DE PARTICIPANTES --
  PN_ESTADO_PARTICIP_ACTIVO    CONSTANT NUMBER(2) := 8;
  PN_ESTADO_PARTICIP_INACTIVO  CONSTANT NUMBER(2) := 9;
  -- ESTADO RESULTADO DE EVOLUCION--
  PN_RES_EVOLUCION_ACTIVO CONSTANT NUMBER(3) := 225;
  PN_RES_EVOLUCION_INACTIVO CONSTANT NUMBER(3) := 226;
  -- TIPO MARCADOR --
  PN_TIPO_MARC_EVAL_MONIT CONSTANT NUMBER(3):=66;
  -- ESTADO RESULTADO DE EVOLUCION--
  PN_ESTADO_SEG_EJEC_PENDIENTE CONSTANT NUMBER(3) := 242;
  PN_ESTADO_SEG_EJEC_ATENDIDO CONSTANT NUMBER(3) := 243;
  -- ROLES
  PN_ROL_AUTOR_PERTE CONSTANT NUMBER(2) := 2;
  PN_ROL_RESP_MONIT  CONSTANT NUMBER(2) := 5;
  PN_ROL_LID_TUMOR   CONSTANT NUMBER(2) := 8;
  PN_ROL_AUTOR_CMAC  CONSTANT NUMBER(2) := 11;
  PN_ROL_CORDINADOR  CONSTANT NUMBER(2) := 7;
  
   -- TIPO DE SOLICITUD EVALUACION
  PN_TIPO_EVALUACION_NUEVO CONSTANT NUMBER(2) := 33;
  PN_TIPO_EVALUACION_CONTINUADOR CONSTANT NUMBER(2) := 32;
    
  -- TIPO DE ENCUENTRO
  PV_TIPO_ENCUENTRO_FARMACIA CONSTANT VARCHAR2(20) := '1';
  PV_TIPO_ENCUENTRO_QUIMIO CONSTANT VARCHAR2(20) := '2';
  PV_TIPO_ENCUENTRO_HOSPI CONSTANT VARCHAR2(20) := '3';
  
  -- RANGO DE EDAD
  PN_RANGO_TODO CONSTANT NUMBER(3) := 247;
  
  -- PREFERENCIA INSITUCIONAL
  PN_CODIGO_CUMPLE_LINEA_TRAT CONSTANT NUMBER(3) := 37;
  PN_COD_LINEA_TRAT_SI        CONSTANT NUMBER(3) := 101;
  PN_COD_LINEA_TRAT_NO        CONSTANT NUMBER(3) := 102;
  PN_COD_LINEA_TRAT_NO_APLICA CONSTANT NUMBER(3) := 140;
  
  -- INDICADORES
  PV_TRES CONSTANT VARCHAR2(20) := '003';
  PV_CUATRO CONSTANT VARCHAR2(20) := '004';
  PV_CINCO CONSTANT VARCHAR2(20) := '005';
  PV_DIECISEIS CONSTANT VARCHAR2(20) := '016';
  
END PCK_PFC_CONSTANTE;

/
--------------------------------------------------------
--  DDL for Package PCK_PFC_BANDEJA_EVALUACION
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "ONTPFC"."PCK_PFC_BANDEJA_EVALUACION" AS
   PROCEDURE SP_PFC_S_DET_EVALUACION (
      PN_COD_SOL_EVA          IN                      NUMBER,
      TC_DETALLE              OUT                     SYS_REFCURSOR,
      PN_FLAG_VER_INFORME     OUT                     NUMBER,
      PV_REPORTE_PDF          OUT                     VARCHAR2,
      PN_FLAG_VER_ACTA_CMAC   OUT                     NUMBER,
      PV_REPORTE_ACTA_CMAC    OUT                     VARCHAR2,
      PN_COD_ROL_LIDER_TUM    OUT                     NUMBER,
      PN_COD_USR_LIDER_TUM    OUT                     NUMBER,
      PV_USR_LIDER_TUM        OUT                     VARCHAR2,
      PN_COD_ARCH_FICHA_TEC   OUT                     NUMBER,
      PN_COD_ARCH_COMPL_MED   OUT                     NUMBER,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   );

   PROCEDURE SP_PFC_S_HIST_LINEA_TRATAMIENT (
      PV_COD_AFILIADO    IN                 VARCHAR2,
      OP_OBJCURSOR       OUT                SYS_REFCURSOR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_SI_HIST_LINEA_TRATAMIEN (
      PV_COD_AFI_PACIENTE        IN                         VARCHAR2,
      PN_COD_SOL_EVA             IN                         NUMBER,
      PN_COD_MAC_SOL             IN                         NUMBER,
      PV_FEC_INICIO              IN                         VARCHAR2,
      PV_FEC_FIN                 IN                         VARCHAR2,
      PN_P_MOTIVO_INACTIVACION   IN                         NUMBER,
      PV_FEC_INACTIVACION        IN                         VARCHAR2,
      PN_P_CURSO                 IN                         NUMBER,
      PN_P_TIPO_TUMOR            IN                         NUMBER,
      PN_P_RESP_ALCANZADA        IN                         NUMBER,
      PN_P_LUGAR_PROGRESION      IN                         NUMBER,
      PV_CMP                     IN                         VARCHAR2,
      PV_MEDICO_TRATANTE         IN                         VARCHAR2,
      PN_COD_RESULTADO           OUT                        NUMBER,
      PV_MSG_RESULTADO           OUT                        VARCHAR2
   );

   PROCEDURE SP_PFC_S_BANDEJA_EVALUACION (
      PV_COD_DESC_SOL_EVA          IN                           VARCHAR2,
      PV_COD_CLINICA               IN                           VARCHAR2,
      PV_COD_PACIENTE              IN                           VARCHAR2,
      PV_FECHA_INICIO              IN                           VARCHAR2,
      PV_FECHA_FIN                 IN                           VARCHAR2,
      PN_NRO_SCG_SOLBEN            IN                           VARCHAR2,
      PV_ESTADO_SOL_EVALUACION     IN                           VARCHAR2,
      PV_TIP_SCG_SOLBEN            IN                           VARCHAR2,
      PN_ROL_RESPONSABLE           IN                           NUMBER,
      PV_ESTADO_SCG_SOLBEN         IN                           VARCHAR2,
      PV_CORREO_LIDER_TUMOR        IN                           VARCHAR2,
      PN_NRO_CARTA_GARANTIA        IN                           VARCHAR2,
      PV_CORREO_CMAC               IN                           VARCHAR2,
      PN_AUTORIZADOR_PERTENENCIA   IN                           NUMBER,
      PV_TIPO_EVALUACION           IN                           VARCHAR2,
      PN_LIDER_TUMOR               IN                           NUMBER,
      PV_FEC_REUNION_CMAC          IN                           VARCHAR2,
      AC_LISTADETALLE              OUT                          PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO             OUT                          NUMBER,
      PV_MSG_RESULTADO             OUT                          VARCHAR2
   );

   PROCEDURE SP_PFC_S_LISTA_CRITERIO (
      PN_COD_INDI        IN                 NUMBER,
      AC_LISTA_INC       OUT                PCK_PFC_CONSTANTE.TYPCUR,
      AC_LISTA_EXC       OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_LISTA_INDICADORES (
      PN_COD_EVA              IN                      NUMBER,
      PN_COD_MAC              IN                      NUMBER,
      PN_COD_GRP_DIAG         IN                      NUMBER,
      AC_LISTA_IND            OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      AC_LISTA_INC            OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      AC_LISTA_EXC            OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_CHKLIST_INDI     OUT                     NUMBER,
      AC_LISTA_INC_PRE        OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      AC_LISTA_EXC_PRE        OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_CUMPLE_CHKLIST_PER   OUT                     NUMBER,
      PV_COMENTARIO           OUT                     VARCHAR2,
      PV_GRABAR               OUT                     VARCHAR2,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   );

   PROCEDURE SP_PFC_SI_CHKLIST_PAC (
      PN_COD_EVA              IN                      NUMBER,
      PN_COD_INDI             IN                      NUMBER,
      PN_COD_MAC              IN                      NUMBER,
      PN_COD_GRP_DIAG         IN                      NUMBER,
      PV_STR_INCLU            IN                      VARCHAR2,
      PV_STR_EXC              IN                      VARCHAR2,
      PN_CUMPLE_CHKLIST_PER   IN                      NUMBER,
      PV_COMENTARIO           IN                      VARCHAR2,
      PN_COD_ROL_USUARIO      IN                      NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO         IN                      VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO          IN                      NUMBER,/* CODIGO DE USUARIO*/
      PN_RESULTADO_S          OUT                     NUMBER,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   );

   PROCEDURE SP_PFC_S_EVALUACION_AUTOR (
      PN_COD_AFI_PACIENTE   IN                    VARCHAR2,
      AC_LISTA_EVALUACION   OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_SU_PREF_INST_GUARDAR (
      PN_COD_SOL_EVA        IN                    NUMBER,
      PN_NRO_CURSO          IN                    NUMBER,
      PN_TIPO_TUMOR         IN                    NUMBER,
      PN_LUGAR_PROGRESION   IN                    NUMBER,
      PN_RESP_ALCANZADA     IN                    NUMBER,
      PN_CONDICION          IN                    NUMBER,
      PN_CUMPLE_PREF_INST   IN                    NUMBER,
      PV_OBSERVACION        IN                    VARCHAR2,
      PN_COD_ROL_USUARIO    IN                    NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO       IN                    VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO        IN                    NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_S_PREF_INST_CONSULTA (
      PV_COD_GRP_DIAG        IN                     VARCHAR2,
      PN_CONDICION           IN                     NUMBER,
      TC_PREFERENCIA_INSTI   OUT                    PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO       OUT                    NUMBER,
      PV_MSG_RESULTADO       OUT                    VARCHAR2
   );

   PROCEDURE SP_PFC_S_SUBCONDICION_CONSULTA (
      PN_COD_GRP_DIAG          IN                       NUMBER,
      PN_LINEA_TRATAMIENTO     IN                       NUMBER,
      PN_CONDICION_CANCER      IN                       NUMBER,
      TC_SUBCONDICION_CANCER   OUT                      PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO         OUT                      NUMBER,
      PV_MSG_RESULTADO         OUT                      VARCHAR2
   );

   PROCEDURE SP_PFC_SUI_PREF_INST_CONSULTA (
      PN_COD_SOL_EVA         IN                     NUMBER,
      PN_NRO_LINEA_TRA       IN                     NUMBER,
      PV_COD_GRP_DIAG        IN                     VARCHAR2,
      PN_CONDICION           OUT                    NUMBER,
      TC_CONDICION           OUT                    PCK_PFC_CONSTANTE.TYPCUR,
      TC_PREFERENCIA_INSTI   OUT                    PCK_PFC_CONSTANTE.TYPCUR,
      PN_NRO_CURSO           OUT                    NUMBER,
      PN_TIPO_TUMOR          OUT                    NUMBER,
      PN_LUGAR_PROGRESION    OUT                    NUMBER,
      PN_RESP_ALCANZADA      OUT                    NUMBER,
      PN_CUMPLE_PREF_INST    OUT                    NUMBER,
      PV_GRABAR              OUT                    VARCHAR2,
      PV_OBSERVACION         OUT                    VARCHAR2,
      PN_COD_RESULTADO       OUT                    NUMBER,
      PV_MSG_RESULTADO       OUT                    VARCHAR2
   );

   PROCEDURE SP_PFC_SI_PREF_INST (
      PN_COD_GRP_DIAG        IN                     NUMBER,
      PN_COND_CANCER         IN                     NUMBER,
      PN_LINEA_TRATAMIENTO   IN                     NUMBER,
      PN_SUBCOND_CANCER      IN                     NUMBER,
      PN_COD_MAC             IN                     NUMBER,
      PV_REGISTRO            OUT                    VARCHAR2,
      PN_COD_RESULTADO       OUT                    NUMBER,
      PV_MSG_RESULTADO       OUT                    VARCHAR2
   );

   PROCEDURE SP_PFC_SU_CHKLIST_REQ_GUARDAR (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PN_COD_ROL_USUARIO   IN                   NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO      IN                   VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO       IN                   NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   );

   PROCEDURE SP_PFC_SI_CHKLIST_REQ_CONSULTA (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PN_EDAD            IN                 NUMBER,
      AC_LISTA_HIST      OUT                PCK_PFC_CONSTANTE.TYPCUR,/*LISTAR HISTORIAL*/
      AC_LISTA_DOC       OUT                PCK_PFC_CONSTANTE.TYPCUR,/*LISTAR TIPO DE DOCUMENTOS*/
      PV_GRABAR          OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_SI_CHKLIST_CARGAR (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PN_COD_CHKLST_REQ    IN                   NUMBER,
      PN_COD_MAC           IN                   NUMBER,
      PN_TIPO_DOCUMENTO    IN                   NUMBER,
      PV_DESCRIPCION_DOC   IN                   VARCHAR2,
      PN_ESTADO_DOC        IN                   NUMBER,
      PV_URL_DESCARGA      IN                   VARCHAR2,
      PN_COD_ARCHIVO       IN                   NUMBER,
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   );

   PROCEDURE SP_PFC_SU_CHKLIST_ELIMINAR (
      PN_COD_CHKLST_REQ   IN                  NUMBER,
      PN_ESTADO_DOC       IN                  NUMBER,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   );

   PROCEDURE SP_PFC_S_CONDICION_BASAL_RESU (
      PN_SOL_EVA            IN                    NUMBER,
      PN_COD_MAC            IN                    NUMBER,
      PN_COD_GRP_DIAG       IN                    NUMBER,
      PV_ANTECEDENTES       OUT                   VARCHAR2,
      PN_ECOG               OUT                   NUMBER,
      PN_EXISTE_TOXICIDAD   OUT                   NUMBER,
      PN_TIPO_TOXICIDAD     OUT                   NUMBER,
      PV_GRABAR             OUT                   VARCHAR2,
      TC_LIST_RESULTADO     OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      TC_CMB_VALOR_FIJO     OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      TC_METASTASIS         OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_I_CONDICION_BASAL (
      PN_COD_SOL_EVA        IN                    NUMBER,
      PV_LINEA_LUGAR_META   IN                    VARCHAR2,
      PN_ECOG               IN                    NUMBER,
      PN_EXISTE_TOX         IN                    NUMBER,
      PN_TIPO_TOX           IN                    NUMBER,
      PV_RESULTADO_BASAL    IN                    VARCHAR2,
      PV_ANTECEDENTE        IN                    VARCHAR2,
      PN_COD_ROL_USUARIO    IN                    NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO       IN                    VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO        IN                    NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_S_ANALISIS_CONCL_CONSUL (
      PN_COD_SOL_EVA          IN                      NUMBER,
      PN_COD_MAC              IN                      NUMBER,
      PN_COD_GRP_DIAG         IN                      NUMBER,
      PN_CUMPLE_PREF_INST     OUT                     NUMBER,
      PV_DESCRIPCION_MAC      OUT                     VARCHAR2,
      TC_CUMPLE_TRATAMIENTO   OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_CUMPLE_CHKLIST_PER   OUT                     NUMBER,
      PV_DESCRIPCION_INDI     OUT                     VARCHAR2,
      TC_ANALISIS_CONCL       OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   );

   PROCEDURE SP_PFC_SIU_ANALISIS_CONCLUSION (
      PN_COD_REPORTE             IN                         NUMBER,
      PN_COD_SOL_EVA             IN                         NUMBER,
      PN_CUMPLE_CHKLIST_PER      IN                         NUMBER,
      PN_CUMPLE_PREF_INSTI       IN                         NUMBER,
      PN_COD_MAC                 IN                         NUMBER,
      PN_PERTINENCIA             IN                         NUMBER,
      PN_CONDICION_PAC           IN                         NUMBER,
      PN_TIEMPO_USO              IN                         NUMBER,
      PN_RESULTADO_AUTORIZADOR   IN                         NUMBER,
      PV_COMENTARIO              IN                         VARCHAR2,
      PN_FLAG_GRABAR             IN                         NUMBER,
      PN_COD_ROL_USUARIO         IN                         NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO            IN                         VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO             IN                         NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_RESULTADO           OUT                        NUMBER,
      PV_MSG_RESULTADO           OUT                        VARCHAR2
   );

   PROCEDURE SP_PFC_SIU_REGIST_CONTINUADOR (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PN_RESUL_AUTOR       IN                   NUMBER,
      PN_COD_RESPO_MONIT   IN                   NUMBER,
      PV_COMENTARIO        IN                   VARCHAR2,
      PN_COD_RESUL_MONIT   IN                   NUMBER,
      PV_NRO_TAREA_MONIT   IN                   VARCHAR2,
      PV_FEC_MONITOREO     IN                   VARCHAR2,
      PN_COD_ROL_USUARIO   IN                   NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO      IN                   VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO       IN                   NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_ROL_MON       IN                   NUMBER,/* CODIGO DE ROL MONITOREO*/
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   );

   PROCEDURE SP_PFC_SU_CONTINUADOR_DOCUMENT (
      PN_COD_SOL_EVA               IN                           NUMBER,
      PN_TIPO_MEDICAMENTO          IN                           NUMBER,
      PV_DESCRIPCION_MEDICAMENTO   IN                           VARCHAR2,
      PN_ESTADO                    IN                           NUMBER,
      PN_COD_RESULTADO             OUT                          NUMBER,
      PV_MSG_RESULTADO             OUT                          VARCHAR2
   );

   PROCEDURE SP_PFC_SI_CONTINUADOR_CONSULTA (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PN_EDAD            IN                 NUMBER,
      AC_LISTA_DOC       OUT                PCK_PFC_CONSTANTE.TYPCUR,/*LISTAR TIPO DE DOCUMENTOS*/
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_SIU_CONTINUADOR_CARGAR (
      PN_COD_SOL_EVA           IN                       NUMBER,
      PN_COD_CONTINUADOR_DOC   IN                       NUMBER,
      PN_TIPO_DOCUMENTO        IN                       NUMBER,
      PV_DESCRIPCION_DOC       IN                       VARCHAR2,
      PN_ESTADO_DOC            IN                       NUMBER,
      PN_COD_ARCHIVO           IN                       NUMBER,
      PV_FECHA_ESTADO          IN                       VARCHAR2,
      PN_COD_RESULTADO         OUT                      NUMBER,
      PV_MSG_RESULTADO         OUT                      VARCHAR2
   );

   PROCEDURE SP_PFC_U_CONTINUADOR_DOC_ELIM (
      PN_COD_CONTINUADOR_DOC   IN                       NUMBER,
      PN_ESTADO_DOC            IN                       NUMBER,
      PN_COD_RESULTADO         OUT                      NUMBER,
      PV_MSG_RESULTADO         OUT                      VARCHAR2
   );

   PROCEDURE SP_PFC_S_ROL_PER_PART_GRDIAG (
      PN_COD_ROL         IN                 NUMBER,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_I_CONDICION_BASAL_SAVE (
      PV_ANT_IMP         IN                 VARCHAR2,
      PN_LINEA_META      IN                 NUMBER,
      PN_LUGAR_META      IN                 NUMBER,
      PN_ECOG            IN                 NUMBER,
      PN_EXISTE_TOX      IN                 NUMBER,
      PN_TIPO_TOX        IN                 NUMBER,
      PN_GRABAR          IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_IU_CONFIGURACION_MARCA (
      PV_COD_MARCA_CONFIG_L   IN                      VARCHAR2,
      PN_ESTADO               IN                      NUMBER,
      PV_COD_MAC              IN                      NUMBER,
      PN_TIPO_MARCA           IN                      NUMBER,
      PN_COD_MARCA            IN                      NUMBER,
      PN_PERIOCIDAD_MIN       IN                      NUMBER,
      PN_PERIOCIDAD_MAX       IN                      NUMBER,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   );

   PROCEDURE SP_PFC_S_CONFIGURACION_MARCA (
      PN_COD_CONFIG_MARCA   IN                    NUMBER,
      TC_LIST               OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );
  /**/

   PROCEDURE SP_PFC_S_RPTA_BASAL_MARCADOR (
      PN_COD_MAC         IN                 NUMBER,
      PN_GRP_DIAG        IN                 NUMBER,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_I_SOL_EVA_LIDER_TUMOR (
      PN_COD_SOL_EVA        IN                    NUMBER,
      PV_FEC_EVA            IN                    VARCHAR2,
      PN_RESULTADO_EVA      IN                    NUMBER,
      PV_COMENTARIO         IN                    VARCHAR2,
      PN_COD_LIDER_TUMOR    IN                    NUMBER,
      PN_COD_ROL_USUARIO    IN                    NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PN_COD_USUARIO        IN                    NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_ROL_LI_TUMOR   IN                    NUMBER,/* CODIGO DE ROL MONITOREO*/
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_S_EVA_ROL_LIDER_TUMOR (
      PN_COD_ROL         IN                 NUMBER,
                                         /*PN_COD_DESC_SOL_EVA IN VARCHAR2,*/
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_I_PROG_REUNION_CMAC (
      PV_FEC_PROG_CMAC      IN                    VARCHAR2,
      PV_HORA_CMAC          IN                    VARCHAR2,
      PN_COD_DESC_SOL_EVA   IN                    VARCHAR2,
      PN_COD_ARCHIVO        IN                    NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_S_CASOS_EVALUACION_CMAC (
      PN_COD_EVALUACION   IN                  NUMBER,
                                           /* PN_COD_DESC_SOL_EVA   IN VARCHAR2,*/
      TC_LIST             OUT                 PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   );

   PROCEDURE SP_PFC_S_EVA_ROL_CMAC (
      PN_COD_ROL            IN                    NUMBER,
      PN_COD_DESC_SOL_EVA   IN                    VARCHAR2,
      TC_LIST               OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_S_FEC_EVALUACION_MAC (
      PN_FEC_MAC         IN                 VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      TC_LIST_PAR        OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_I_EVALUACION_CMAC (
      PV_EVALUACION         IN                    VARCHAR2,
      PV_PARTICIPANTE_MAC   IN                    VARCHAR2,
      PV_COD_ESCAN          IN                    VARCHAR2,
      PV_FECHA_PROG         IN                    VARCHAR2,/**/
      PV_HORA_PROG          IN                    VARCHAR2,/**/
      PV_FECHA_ESTADO       IN                    VARCHAR2, /* FECHA DE REGISTRO DE LA EVALUACION */
      PN_COD_ROL_CMAC       IN                    NUMBER, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
      PN_COD_CMAC           IN                    NUMBER, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
      PN_COD_ACTA_FTP       IN                    NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );
   
   PROCEDURE SP_PFC_SD_ELIM_EVALUACION_CMAC (
      PN_COD_SOL_EVA        IN                    VARCHAR2,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_S_SEGUIMIENTO (
      PV_COD_EVALUACION   IN                  VARCHAR2,
      TC_LIST             OUT                 PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   );

   PROCEDURE SP_PFC_I_SEGUIMIENTO (
      PN_COD_EVALUACION        IN                       NUMBER,
      PV_ESTADO_EVALUACION     IN                       NUMBER,
      PV_FECHA_ESTADO          IN                       VARCHAR2,
      PV_ROL_RESP_ESTADO       IN                       NUMBER,
      PV_USR_RESP_ESTADO       IN                       NUMBER,
      PV_ROL_RESP_REG_ESTADO   IN                       NUMBER,
      PV_USR_RESP_REG_ESTADO   IN                       NUMBER,
      PN_COD_RESULTADO         OUT                      NUMBER,
      PV_MSG_RESULTADO         OUT                      VARCHAR2
   );

   PROCEDURE SP_PFC_S_EVALUACIONXCODIGO (
      PV_COD_DESC_SOL_EVA   IN                    VARCHAR2,
      AC_LISTADETALLE       OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_S_COD_EVALUACION_MAC (
      PV_COD_EVA         IN                 VARCHAR2,
      PV_COD_PROG_CMAC   IN                 VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_U_EST_SOL_EVA (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PV_EST_SOL_EVAL    IN                 NUMBER,/* ESTADO DE SOLICITUD EVAL*/
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_U_COD_ENVIO_SOL_EVA (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PN_COD_ENVIO       IN                 VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_PARTICIPANTE_GRP_DIAG (
      PV_COD_GRP_DIAG     IN                  VARCHAR2,
      PN_COD_ROL          IN                  NUMBER,
      LISTAPARTICIPANTE   OUT                 PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   );

   PROCEDURE SP_PFC_S_PARTICIPANTE (
      PN_COD_ROL         IN                 NUMBER,
      PV_COD_GRP_DIAG    IN                 VARCHAR2,
      PN_P_RANGO_EDAD    IN                 NUMBER,
      PN_COD_USUARIO     IN                 NUMBER,
      AC_LISTA           OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_SOL_EVA_COD_ENVIO (
      PV_COD_SOL_EVA      IN                  NUMBER,
      CODIGOENVIOSOLEVA   OUT                 PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   );

   PROCEDURE SP_PFC_S_CHECKLIST_REQUISITO (
      PN_COD_SOL_EVA     IN                 NUMBER,
      AC_LISTA           OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_ACTA        OUT                NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_U_PARAMS_CORREO_EVA (
      PN_COD_SOL_EVA                  IN                              NUMBER,
      PN_COD_ENVIO_ENV_LIDER_TUMOR    IN                              NUMBER,
      PN_EST_CORREO_ENV_LIDER_TUMOR   IN                              NUMBER,
      AC_LISTA                        OUT                             PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO                OUT                             NUMBER,
      PV_MSG_RESULTADO                OUT                             VARCHAR2
   );

   PROCEDURE SP_PFC_U_ACTUALIZAR_INFORME (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PN_COD_INFORME     IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );
   
   PROCEDURE SP_PFC_U_SOL_EVA_PTIPO (
      PN_COD_SOL_EVA                  IN                              NUMBER,
      PN_P_TIPO_EVA                   IN                              NUMBER,
      PN_COD_RESULTADO                OUT                             NUMBER,
      PV_MSG_RESULTADO                OUT                             VARCHAR2
   );
   
   PROCEDURE SP_PFC_SI_DOCUMENTO_CONSULTA (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PN_EDAD            IN                 NUMBER,
      AC_LISTA_HIST      OUT                PCK_PFC_CONSTANTE.TYPCUR,/*LISTAR HISTORIAL*/
      AC_LISTA_DOC       OUT                PCK_PFC_CONSTANTE.TYPCUR,/*LISTAR TIPO DE DOCUMENTOS*/
      PV_GRABAR          OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );
   
   PROCEDURE SP_PFC_SI_DOCUMENTO_CARGAR (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PN_COD_CHKLST_REQ    IN                   NUMBER,
      PN_COD_MAC           IN                   NUMBER,
      PN_TIPO_DOCUMENTO    IN                   NUMBER,
      PV_DESCRIPCION_DOC   IN                   VARCHAR2,
      PN_ESTADO_DOC        IN                   NUMBER,
      PV_URL_DESCARGA      IN                   VARCHAR2,
      PN_COD_ARCHIVO       IN                   NUMBER,
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   );
   
   PROCEDURE SP_PFC_SUI_DOCUMENTO_ELIMINAR (
      PN_COD_CHKLST_REQ   IN                  NUMBER,
      PN_ESTADO_DOC       IN                  NUMBER,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   );
   
   PROCEDURE SP_PFC_SU_DOC_REQ_GUARDAR (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PN_COD_ROL_USUARIO   IN                   NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO      IN                   VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO       IN                   NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   );
   
   PROCEDURE SP_PFC_U_ACTAESC_PROG_CMAC (
      PN_COD_PROGRAMACION_CMAC       IN                   NUMBER,
      PV_REPORTE_ACTA_ESCANEADA      IN                   VARCHAR2,
      PN_COD_RESULTADO               OUT                  NUMBER,
      PV_MSG_RESULTADO               OUT                  VARCHAR2
   );
   
   PROCEDURE SP_PFC_S_INFORME_AUTO_CMAC (
      PN_COD_SOL_EVA                 IN                   NUMBER,
      PN_COD_INFORME_AUTO            OUT                  NUMBER,
      PN_COD_ACTA_CMAC_FTP           OUT                  NUMBER,
      PV_COD_REP_ACTA_SCAN           OUT              VARCHAR2,
      PN_COD_RESULTADO               OUT                  NUMBER,
      PV_MSG_RESULTADO               OUT                  VARCHAR2
   );

END PCK_PFC_BANDEJA_EVALUACION;

/
--------------------------------------------------------
--  DDL for Package PCK_PFC_BANDEJA_MONITOREO
--------------------------------------------------------

  CREATE OR REPLACE  PACKAGE "ONTPFC"."PCK_PFC_BANDEJA_MONITOREO" AS

    PROCEDURE sp_pfc_s_bandeja_monitoreo (
        pv_cod_paciente            IN VARCHAR2,
        pn_estado_monitoreo        IN NUMBER,
        pv_cod_clinica             IN VARCHAR2,
        pv_fecha_monitoreo         IN VARCHAR2,
        pn_cod_resp_monitoreo      IN NUMBER,
        ac_listamonitoreo          OUT pck_pfc_constante.typcur,
        pn_cod_resultado           OUT NUMBER,
        pv_msg_resultado           OUT VARCHAR2
    );

    PROCEDURE sp_pfc_s_hist_lin_tratamiento (
        pn_cod_hist_linea_trat   IN NUMBER,
        pn_cod_afiliado          IN VARCHAR2,
        pn_cod_mac               IN NUMBER,
        ac_lista                 OUT SYS_REFCURSOR,
        pn_cod_resultado         OUT NUMBER,
        pv_msg_resultado         OUT VARCHAR2
    );

    PROCEDURE sp_pfc_s_listar_evolucion (
        pn_cod_sol_eva     IN NUMBER,
        ac_lista           OUT pck_pfc_constante.typcur,
        pn_cod_resultado   OUT NUMBER,
        pv_msg_resultado   OUT VARCHAR2
    );

    PROCEDURE sp_pfc_i_datos_evolucion (
        pv_nro_desc_evolucion    IN VARCHAR2,
        pn_cod_monitoreo         IN NUMBER,
        pn_cod_mac               IN NUMBER,
        pn_p_res_evolucion       IN NUMBER,
        pn_cod_hist_linea_trat   IN NUMBER,
        pv_fec_monitoreo         IN VARCHAR2,
        pn_p_tolerancia          IN NUMBER,
        pn_p_toxicidad           IN NUMBER,
        pn_p_grado               IN NUMBER,
        pn_p_resp_clinica        IN NUMBER,
        pn_p_aten_alerta         IN NUMBER,
        pv_existe_toxicidad      IN ontpfc.pfc_evolucion.existe_toxicidad%TYPE,
        pn_nro_evolucion         IN NUMBER,
        pv_marcadores            IN VARCHAR2,
        pn_estado                IN NUMBER,
        pv_usuario_crea          IN VARCHAR2,
        pn_p_estado_monitoreo    IN NUMBER,
        pn_cod_evolucion         OUT NUMBER,
        pn_cod_resultado         OUT NUMBER,
        pv_msg_resultado         OUT VARCHAR2
    );

    PROCEDURE sp_pfc_i_resultado_evolucion (
        pn_cod_evolucion             IN NUMBER,
        pn_p_res_evolucion           IN NUMBER,
        pn_cod_sol_evaluacion        IN NUMBER,
        pv_desc_cod_sol_evaluacion   IN VARCHAR2,
        pn_cod_monitoreo             IN NUMBER,
        pn_p_estado_monitoreo        IN NUMBER,
        pv_fec_monitoreo             IN VARCHAR2,
        pv_fec_prox_monitoreo        IN VARCHAR2,
        pn_p_motivo_inactivacion     IN NUMBER,
        pv_fec_inactivacion          IN VARCHAR2,
        pv_observacion               IN VARCHAR2,
        pv_usuario_modif             IN VARCHAR2,
        pn_estado                    IN NUMBER,
        pn_cod_hist_linea_trat       IN NUMBER,
        pn_cod_resultado             OUT NUMBER,
        pv_msg_resultado             OUT VARCHAR2
    );

    PROCEDURE sp_pfc_s_detalle_evolucion (
        pn_cod_evolucion   IN NUMBER,
        ac_lista           OUT SYS_REFCURSOR,
        pn_cod_resultado   OUT NUMBER,
        pv_msg_resultado   OUT VARCHAR2
    );

    PROCEDURE sp_pfc_s_listar_marcador (
        pn_cod_mac          IN NUMBER,
        pn_cod_grp_diag     IN NUMBER,
        pn_p_estado         IN NUMBER,
        ac_lista            OUT pck_pfc_constante.typcur,
        ac_lista_detalle    OUT pck_pfc_constante.typcur,
        pn_cod_resultado    OUT NUMBER,
        pv_msg_resultado    OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_u_datos_evolucion (
        pn_cod_evolucion        IN NUMBER,
        pn_cod_monitoreo        IN NUMBER,
        pv_fec_monitoreo        IN VARCHAR2,
        pn_p_tolerancia         IN NUMBER,
        pn_p_toxicidad          IN NUMBER,
        pn_p_grado              IN NUMBER,
        pn_p_resp_clinica       IN NUMBER,
        pn_p_aten_alerta        IN NUMBER,
        pv_existe_toxicidad     IN ontpfc.pfc_evolucion.existe_toxicidad%TYPE,
        pv_marcadores           IN VARCHAR2,
        pn_estado               IN NUMBER,
        pv_usuario_modif        IN VARCHAR2,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_s_historial_marcadores (
        pn_cod_sol_eva          IN NUMBER,
        ac_lista                OUT pck_pfc_constante.typcur,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_u_estado_monitoreo (
        pn_cod_monitoreo             IN NUMBER,
        pn_p_estado_monitoreo        IN NUMBER,
        pn_cod_resultado             OUT NUMBER,
        pv_msg_resultado             OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_i_seg_ejecutivo (
        pn_cod_monitoreo             IN NUMBER,
        pn_cod_ejecutivo_monitoreo   IN NUMBER,
        pn_nom_ejecutivo_monitoreo   IN VARCHAR2,
        pn_p_estado_seguimiento      IN NUMBER,
        pv_detalle_evento            IN VARCHAR2,
        pn_visto_resp_monitoreo      IN NUMBER,
        pv_fecha_registro            IN VARCHAR2,
        pv_usuario_crea              IN VARCHAR2,
        pn_p_estado_monitoreo        IN NUMBER,
        pn_cod_seg_ejecutivo         OUT NUMBER,
        pn_cod_resultado             OUT NUMBER,
        pv_msg_resultado             OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_s_seg_ejecutivo (
        pn_cod_monitoreo        IN NUMBER,
        ac_lista                OUT pck_pfc_constante.typcur,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_s_seg_pendientes (
        pn_cod_monitoreo        IN NUMBER,
        pn_pendientes           OUT NUMBER,
        pn_seguimiento          OUT NUMBER,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_u_seg_ejecutivo (
        pn_cod_monitoreo        IN NUMBER,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_s_ult_marcador (
        pn_cod_marcador         IN NUMBER,
        pn_cod_sol_eva          IN NUMBER,
        pn_cod_evolucion        IN NUMBER,
        ac_lista                OUT pck_pfc_constante.typcur,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_s_ult_monitoreo (
        pn_cod_linea_trat          IN NUMBER,
        pv_fecha                   IN VARCHAR2,
        pv_cod_grp_diag            IN VARCHAR2,
        ac_listamonitoreo          OUT pck_pfc_constante.typcur,
        pn_cod_resultado           OUT NUMBER,
        pv_msg_resultado           OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_s_participante (
      pn_cod_rol            IN        NUMBER,
      pn_cod_usuario        IN        NUMBER,
      pn_cod_participante   IN        NUMBER,
      ac_lista              OUT       PCK_PFC_CONSTANTE.TYPCUR,
      pn_cod_resultado      OUT       NUMBER,
      pv_msg_resultado      OUT       VARCHAR2
   );
   
   PROCEDURE sp_pfc_s_participante_det (
      pn_cod_rol            IN        NUMBER,
      pv_cod_grp_diag       IN        VARCHAR2,
      pn_p_rango_edad       IN        NUMBER,
      pn_cod_usuario        IN        NUMBER,
      pn_cod_participante   IN        NUMBER,
      ac_lista              OUT       PCK_PFC_CONSTANTE.TYPCUR,
      pn_cod_resultado      OUT       NUMBER,
      pv_msg_resultado      OUT       VARCHAR2
   );
   
   PROCEDURE sp_pfc_u_soleva_correo_cmac (
      pn_cod_envio          IN        NUMBER,
      pv_cod_plantilla      IN        VARCHAR2,
      pn_tipo_envio         IN        NUMBER,
      pn_flag_adjunto       IN        NUMBER,
      pv_ruta               IN        VARCHAR2,
      pv_usrapp             IN        VARCHAR2,
      pv_asunto             IN        VARCHAR2,
      pv_cuerpo             IN        VARCHAR2,
      pv_solicitudes        IN        VARCHAR2,
      pv_destinatario       IN        VARCHAR2,
      pn_cod_resultado      OUT       NUMBER,
      pv_msg_resultado      OUT       VARCHAR2
   );
   
   PROCEDURE sp_pfc_u_soleva_correo_lidtum (
      pn_cod_envio              IN        NUMBER,
      pv_cod_plantilla          IN        VARCHAR2,
      pn_tipo_envio             IN        NUMBER,
      pn_flag_adjunto           IN        NUMBER,
      pv_ruta                   IN        VARCHAR2,
      pv_usrapp                 IN        VARCHAR2,
      pv_asunto                 IN        VARCHAR2,
      pv_cuerpo                 IN        VARCHAR2,
      pn_cod_sol_eva            IN        NUMBER,
      pn_estado_correo_lidtum   IN        NUMBER,
      pv_destinatario           IN        VARCHAR2,
      pn_cod_resultado          OUT       NUMBER,
      pv_msg_resultado          OUT       VARCHAR2
   );
   
   PROCEDURE sp_pfc_s_correo_participante (
        pn_cod_envio            IN NUMBER,
        ac_lista                OUT pck_pfc_constante.typcur,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    );
    
    PROCEDURE sp_pfc_u_params_cmac_eva (
      PN_COD_SOL_EVA                  IN      NUMBER,
      PN_COD_ENVIO_ENV_MAC            IN      NUMBER,
      PN_EST_CORREO_ENV_CMAC          IN      NUMBER,
      AC_LISTA                        OUT     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO                OUT     NUMBER,
      PV_MSG_RESULTADO                OUT     VARCHAR2
   );
   
   PROCEDURE SP_PFC_S_SOLEVA_X_COD_ACTA (
        PN_COD_SOL_EVA          IN NUMBER,
        AC_LISTA                OUT pck_pfc_constante.typcur,
        PN_COD_RESULTADO        OUT NUMBER,
        PV_MSG_RESULTADO        OUT VARCHAR2
    );
    
    PROCEDURE SP_PFC_U_PARAMS_CMAC_EVA_V2 (
      PN_COD_ENVIO_ENV_MAC            IN      NUMBER,
      PN_EST_CORREO_ENV_CMAC          IN      NUMBER,
      AC_LISTA                        OUT     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO                OUT     NUMBER,
      PV_MSG_RESULTADO                OUT     VARCHAR2
   );
   
   PROCEDURE SP_PFC_S_COD_AUTO_PERT_SOL (
      PV_COD_SOL_EVA            IN      VARCHAR2,
      AC_LISTA                  OUT     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO          OUT     NUMBER,
      PV_MSG_RESULTADO          OUT     VARCHAR2
   );
   
   PROCEDURE SP_PFC_S_RESP_MON_PARTICIPANTE (
      PV_COD_GRP_DIAG       IN      VARCHAR2,
      PN_EDAD               IN      NUMBER,
      PN_COD_ROL            IN      NUMBER,
      AC_LISTA              OUT     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT     NUMBER,
      PV_MSG_RESULTADO      OUT     VARCHAR2
   );

END pck_pfc_bandeja_monitoreo;

/
--------------------------------------------------------
--  DDL for Package PCK_PFC_BANDEJA_PRELIMINAR
--------------------------------------------------------

  CREATE OR REPLACE  PACKAGE "ONTPFC"."PCK_PFC_BANDEJA_PRELIMINAR" AS
   PROCEDURE SP_PFC_S_BANDEJA_PRELIMINAR (
      PN_ID_SOL_PRELIMINAR           IN                             NUMBER,
      PV_PACIENTE                    IN                             VARCHAR2,
      PV_COD_SCG_SOLBEN              IN                             VARCHAR2,
      PV_TIPO_SCG_SOLBEN             IN                             VARCHAR2,
      PV_CLINICA                     IN                             VARCHAR2,
      PV_FECHA_INI_REGISTRO_SOL      IN                             VARCHAR2,
      PV_FECHA_FIN_REGISTRO_SOL      IN                             VARCHAR2,
      PV_ESTADO_SOL_PRE              IN                             VARCHAR2,
      PV_AUTORIZADOR_PERTENENCIA     IN                             VARCHAR2,
      PN_INDEX                       IN                             NUMBER,
      PN_LONGITUD                    IN                             NUMBER,
      PN_TOTAL                       OUT                            NUMBER,
      AC_LISTA_DETALLE_PREELIMINAR   OUT                            PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO               OUT                            NUMBER,
      PV_MSG_RESULTADO               OUT                            VARCHAR2
   );

   PROCEDURE SP_PFC_S_TIPO_CORREO (
      PN_CODTIPOCO       IN                 NUMBER,
      PV_ASUNTO          OUT                VARCHAR2,
      PV_CUERPO          OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_DET_PRELIMINAR_SCG (
      PN_COD_SOL_PRE     IN                 NUMBER,
      AC_INFOSOLBEN      OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_U_DETALLE_PRELIMINAR (
      PN_COD_SOL_PRE        IN                    NUMBER,
      PN_P_ESTADO_SOL_PRE   IN                    NUMBER,
      PN_COD_MAC            IN                    NUMBER,
      PV_FECHA_ACTUAL       IN                    VARCHAR2,
      PN_USUARIO            IN                    NUMBER,
      PV_ESTADO_SOL_PRE     OUT                   VARCHAR2,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_U_ESTADO_PRELIMINAR (
      PN_COD_SOL_PRE     IN                 NUMBER,
      PV_FECHA_ACTUAL    IN                 VARCHAR2,
      PN_USUARIO         IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_SI_SCG_SOLBEN_PREL (
      PV_COD_SCG_SOLBEN                IN                               VARCHAR2,
      PV_COD_CLINICA                   IN                               VARCHAR2,
      PV_COD_AFI_PACIENTE              IN                               VARCHAR2,
      PN_EDAD_PACIENTE                 IN                               NUMBER,
      PV_DES_CONTRATANTE               IN                               VARCHAR2,
      PV_DES_PLAN                      IN                               VARCHAR2,
      PV_FEC_AFILIACION                IN                               VARCHAR2,
      PV_COD_DIAGNOSTICO               IN                               VARCHAR2,
      PV_CMP_MEDICO                    IN                               VARCHAR2,
      PV_MEDICO_TRATANTE_PRESCRIPTOR   IN                               VARCHAR2,
      PV_FEC_RECETA                    IN                               VARCHAR2,
      PV_FEC_QUIMIO                    IN                               VARCHAR2,
      PV_FEC_HOSP_INICIO               IN                               VARCHAR2,
      PV_FEC_HOSP_FIN                  IN                               VARCHAR2,
      PV_FEC_SCG_SOLBEN                IN                               VARCHAR2,
      PV_HORA_SCG_SOLBEN               IN                               VARCHAR2,
      PV_TIPO_SCG_SOLBEN               IN                               VARCHAR2,
      PV_ESTADO_SCG                    IN                               VARCHAR2,
      PV_DESC_MEDICAMENTO              IN                               VARCHAR2,
      PV_DESC_ESQUEMA                  IN                               VARCHAR2,
      PN_TOTAL_PRESUPUESTO             IN                               NUMBER,
      PV_DESC_PROCEDIMIENTO            IN                               VARCHAR2,
      PV_PERSON_CONTACTO               IN                               VARCHAR2,
      PV_OBS_CLINICA                   IN                               VARCHAR2,
      PN_IND_VALIDA                    IN                               NUMBER,
      PV_TX_DATO_ADIC1                 IN                               VARCHAR2,
      PV_TX_DATO_ADIC2                 IN                               VARCHAR2,
      PV_TX_DATO_ADIC3                 IN                               VARCHAR2,
      PV_COD_GRP_DIAGNOSTICO           IN                               VARCHAR2,
      PV_FEC_ACTUAL                    IN                               VARCHAR2,
      PV_OUT_COD_SCG_SOLBEN            OUT                              VARCHAR2,
      PN_OUT_COD_SOL_PRE               OUT                              NUMBER,
      PV_OUT_FEC_SOL_PRE               OUT                              VARCHAR2,
      PV_OUT_HORA_SOL_PRE              OUT                              VARCHAR2,
      PN_OUT_COD_RESULTADO             OUT                              NUMBER,
      PV_OUT_MSG_RESULTADO             OUT                              VARCHAR2
   );

   PROCEDURE SP_PFC_U_SOLIC_PRELIMINAR (
      PN_COD_SOL_PRE        IN                    NUMBER,
      PN_P_ESTADO_SOL_PRE   IN                    NUMBER,
      PV_FECHA_ACTUAL       IN                    VARCHAR2,
      PN_USUARIO            IN                    NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

END PCK_PFC_BANDEJA_PRELIMINAR;

/
--------------------------------------------------------
--  DDL for Package PCK_PFC_CARGA_CONTROL_GASTO
--------------------------------------------------------

  CREATE OR REPLACE  PACKAGE "ONTPFC"."PCK_PFC_CARGA_CONTROL_GASTO" AS

    PROCEDURE SP_PFC_S_HIST_CONTROL_GASTO_PG (
        PV_FECHA_INICIO            IN DATE,
        PV_FECHA_FIN               IN DATE,
        PN_INDEX                   IN NUMBER,
      PN_LONGITUD                IN NUMBER,      
        TC_CARGA_ARCHIVO           OUT PCK_PFC_CONSTANTE.TYPCUR,
        PN_COD_RESULTADO           OUT NUMBER,
        PV_MSG_RESULTADO           OUT VARCHAR2
    );

    PROCEDURE SP_PFC_I_CONTROL_GASTO_CAB (
        PD_FECHA_HORA_CARGA                IN   DATE   , 
        PN_RESPONSABLE_CARGA               IN   NUMBER , 
        PN_P_ESTADO_CARGA                  IN   NUMBER , 
    PN_REGISTRO_TOTAL                  IN   NUMBER , 
    PN_REGISTRO_CARGADO                IN   NUMBER , 
    PN_REGISTRO_ERROR                  IN   NUMBER , 
    PN_VER_LOG                         IN   NUMBER , 
    PN_COD_ARCHIVO_LOG                 IN   NUMBER , 
    PN_DESCARGA                        IN   NUMBER , 
    PN_COD_ARCHIVO_DESCARGA            IN   NUMBER ,
        PD_FECHA_CREACION                  IN   DATE,
        PN_USUARIO_CREACION                IN   NUMBER,  
    PD_FECHA_MODIFICACION              IN   DATE,          
    PN_USUARIO_MODIFICACION            IN   NUMBER,                         
    PN_COD_HIST_CONTROL_GASTO      OUT  NUMBER , 
    PN_COD_RESULTADO             OUT  NUMBER,
    PV_MSG_RESULTADO             OUT  VARCHAR2
   );

  PROCEDURE SP_PFC_I_CONSUMOS (
                        PD_FECHA_HORA_CARGA                         IN   DATE   , 
            PN_RESPONSABLE_CARGA                        IN   NUMBER , 
            PN_P_ESTADO_CARGA                           IN   NUMBER , 
            PN_REGISTRO_TOTAL                           IN   NUMBER , 
            PN_REGISTRO_CARGADO                         IN   NUMBER , 
            PN_REGISTRO_ERROR                           IN   NUMBER , 
            PN_VER_LOG                                  IN   NUMBER , 
            PN_COD_ARCHIVO_LOG                          IN   NUMBER , 
            PN_DESCARGA                                 IN   NUMBER , 
            PN_COD_ARCHIVO_DESCARGA                     IN   NUMBER ,   
            PN_COD_HIST_CONTROL_GASTO               IN   NUMBER ,
            PV_COD_MAC                                IN   VARCHAR2 ,
            PV_TIPO_ENCUENTRO                         IN   VARCHAR2 , 
            PN_ENCUENTRO                        IN   NUMBER   ,
            PV_CODIGO_SAP                       IN   VARCHAR2 ,
            PV_DESCRIP_ACTIVO_MOLECULA          IN   VARCHAR2 ,
            PV_DESCRIP_PRESENT_GENERICO         IN   VARCHAR2 ,
            PV_PRESTACION                       IN   VARCHAR2 ,
            PV_COD_AFILIADO                     IN   VARCHAR2 ,
            PV_COD_PACIENTE                     IN   VARCHAR2 ,
            PV_NOMBRE_PACIENTE                  IN   VARCHAR2 ,
            PV_CLINICA                          IN   VARCHAR2 ,
            PD_FECHA_CONSUMO                    IN   DATE     ,          
            PV_DESCRIP_GRP_DIAG                 IN   VARCHAR2 ,
            PV_COD_DIAGNOSTICO                  IN   VARCHAR2 ,
            PV_DESCRIP_DIAGNOSTICO              IN   VARCHAR2 ,
            PV_LINEA_TRATAMIENTO                IN   NUMBER   ,
            PV_MEDICO_TRATANTE                  IN   VARCHAR2 ,
            PV_PLAN                             IN   VARCHAR2 ,
            PN_CANTIDAD_CONSUMO                 IN   NUMBER   ,
            PN_MONTO_CONSUMO                    IN   NUMBER   ,
                        PD_FECHA_CREACION                           IN   DATE,
            PN_USUARIO_CREACION                         IN   NUMBER,  
            PD_FECHA_MODIFICACION                       IN   DATE,          
            PN_USUARIO_MODIFICACION                     IN   NUMBER,                         
                        PN_COD_CONSUMO_MEDICAMENTO          OUT  NUMBER ,       
                        PN_COD_RESULTADO                OUT  NUMBER,
                        PV_MSG_RESULTADO                OUT  VARCHAR2
   );

  PROCEDURE SP_PFC_U_CONTROL_GASTO_CAB (
                        PN_COD_HIST_CONTROL_GASTO               IN   NUMBER , 
            PD_FECHA_HORA_CARGA                         IN   DATE   , 
            PN_RESPONSABLE_CARGA                        IN   NUMBER , 
            PN_P_ESTADO_CARGA                           IN   NUMBER , 
            PN_REGISTRO_TOTAL                           IN   NUMBER , 
            PN_REGISTRO_CARGADO                         IN   NUMBER , 
            PN_REGISTRO_ERROR                           IN   NUMBER , 
            PN_VER_LOG                                  IN   NUMBER , 
            PN_COD_ARCHIVO_LOG                          IN   NUMBER , 
            PN_DESCARGA                                 IN   NUMBER , 
            PN_COD_ARCHIVO_DESCARGA                     IN   NUMBER ,
                        PD_FECHA_CREACION                           IN   DATE,
            PN_USUARIO_CREACION                         IN   NUMBER,  
            PD_FECHA_MODIFICACION                       IN   DATE,          
            PN_USUARIO_MODIFICACION                     IN   NUMBER,                         
                        PN_COD_RESULTADO                OUT  NUMBER,
                        PV_MSG_RESULTADO                OUT  VARCHAR2
   );

   PROCEDURE SP_PFC_S_VALIDAR_TRATAMIENTO (
                        PV_COD_AFI_PACIENTE                         IN VARCHAR2,
            PV_COD_DIAGNOSTICO                          IN VARCHAR2,  
            PN_COD_MAC                                  IN VARCHAR2,
                        PN_LINEA_TRA                                IN NUMBER,
                        PN_TOTAL                                    OUT NUMBER
   );

   PROCEDURE SP_PFC_U_ACTUALIZAR_CONSUMO (
                        PV_COD_AFI_PACIENTE                         IN VARCHAR2,
            PV_COD_DIAGNOSTICO                          IN VARCHAR2,  
            PN_COD_MAC                                  IN NUMBER, 
            PN_CANT_ULTIMO_CONSUMO            IN NUMBER,
                        PN_GASTO_ULTIMO_CONSUMO                     IN NUMBER,
                        PD_FEC_ULTIMO_CONSUMO                       IN DATE,
                        PN_COD_RESULTADO                            OUT NUMBER,
                        PV_MSG_RESULTADO                            OUT VARCHAR2
   );

   PROCEDURE SP_PFC_S_VALIDAR_CARGA (
              PN_EXISTE_ARCHIVO             OUT NUMBER,
                        PV_MENSAJE                                  OUT VARCHAR2
   );
   
   PROCEDURE SP_PFC_U_ACTUALIZAR_ESTADO;

END PCK_PFC_CARGA_CONTROL_GASTO;

/
--------------------------------------------------------
--  DDL for Package PCK_PFC_CONFIGURACION_SISTEMA
--------------------------------------------------------

  CREATE OR REPLACE  PACKAGE "ONTPFC"."PCK_PFC_CONFIGURACION_SISTEMA" AS
   PROCEDURE SP_PFC_I_REGISTRO_MAC (
      PN_COD_MAC         IN                 NUMBER,
      PV_DESCRIPCION     IN                 VARCHAR2,
      PN_P_TIPO_MAC      IN                 NUMBER,
      PN_P_ESTADO_MAC    IN                 NUMBER,
      PD_FECHA_INSCRIP   IN                 DATE,
      PD_FEC_INICIO      IN                 DATE,
      PD_FEC_FIN         IN                 DATE,
      PV_FEC_CREACION    IN                 VARCHAR2,
      PN_USER_CREACION   IN                 NUMBER,
      PV_FEC_MODIFICA    IN                 VARCHAR2,
      PN_USER_MODIFICA   IN                 NUMBER,
      PN_COD_OUT_MAC     OUT                NUMBER,
      PV_COD_LARGO_MAC   OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_CONFIG_CHECKLIST (
      PN_COD_GRP_DIAG    IN                 NUMBER,
      PN_COD_MAC         IN                 NUMBER,
      OP_OBJCURSOR       OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_FILTRO_MAC (
      PV_P_COD           IN                 VARCHAR2,
      PV_P_DES           IN                 VARCHAR2,
      PV_P_COM           IN                 VARCHAR2,
      PV_TIPO            IN                 VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_I_REGISTRO_CHEKLIST (
      PN_CODMAC               IN                      NUMBER,
      PN_GRPDIAGNOSTICO       IN                      NUMBER,
      PN_CODCHKLISTINDI       IN                      NUMBER,
      PV_DESCRIPCION          IN                      VARCHAR2,
      PN_PESTADO              IN                      NUMBER,
      PD_FECHAINIVIGENCIA     IN                      DATE,
      PD_FECHAFINVIGENCIA     IN                      DATE,
      PN_OUT_CODCHKLISTINDI   OUT                     NUMBER,
      PV_OUT_CODCHKLARGO      OUT                     VARCHAR2,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   );

   PROCEDURE SP_PFC_S_LISTA_CRITINCLUSION (
      PN_CODCHKLISTINDI       IN                      NUMBER,
      PN_COD_MAC              IN                      NUMBER,
      PN_GRP_DIAG             IN                      NUMBER,
      TC_LIST                 OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_OUT_CODCHKLISTINDI   OUT                     NUMBER,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   );

   PROCEDURE SP_PFC_S_LISTA_CRITEXCLUSION (
      PN_CODCHKLISTINDI       IN                      NUMBER,
      PN_COD_MAC              IN                      NUMBER,
      PN_GRP_DIAG             IN                      NUMBER,
      TC_LIST                 OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_OUT_CODCHKLISTINDI   OUT                     NUMBER,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   );

   PROCEDURE SP_PFC_I_REGCRITEXCLUSION (
      PN_CODCHKLISTINDI     IN                    NUMBER,
      PN_COD_MAC            IN                    NUMBER,
      PN_GRP_DIAG           IN                    NUMBER,
      PN_CODEXCLUSION       IN                    NUMBER,
      PN_DESCRIPCRITERIO    IN                    VARCHAR2,
      PN_ESTADO             IN                    NUMBER,
      PN_OUT_CODEXCLUSION   OUT                   NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_I_REGCRITINCLUSION (
      PN_CODCHKLISTINDI     IN                    NUMBER,
      PN_COD_MAC            IN                    NUMBER,
      PN_GRP_DIAG           IN                    NUMBER,
      PN_CODINCLUSION       IN                    NUMBER,
      PN_DESCRIPCRITERIO    IN                    VARCHAR2,
      PN_ESTADO             IN                    NUMBER,
      PN_OUT_CODINCLUSION   OUT                   NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_I_REGISTROFICHA (
      PN_CODVERSION      IN                 NUMBER,
      PV_NOMBREARCHIVO   IN                 VARCHAR2,
      PN_ESTADO          IN                 NUMBER,
      PN_CODMAC          IN                 NUMBER,
      PN_CODIGO_FICHA    IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_LISTARFICHAS (
      PN_CODMAC          IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR
   );

   PROCEDURE SP_PFC_I_REGISTRO_COMP (
      PV_COD_VERSION           IN                       VARCHAR2,
      PV_NOMBRE_ARCHIVO        IN                       VARCHAR2,
      PN_ESTADO                IN                       NUMBER,
      PN_CODMAC                IN                       NUMBER,
      PN_CODIGO_ARCHIVO_COMP   IN                       NUMBER,
      PN_COD_RESULTADO         OUT                      NUMBER,
      PV_MSG_RESULTADO         OUT                      VARCHAR2
   );

   PROCEDURE SP_PFC_U_REGISTRO_COMP (
      PV_COD_VERSION       IN                   VARCHAR2,
      PN_COD_FILE_UPLOAD   IN                   NUMBER,
      PN_CODMAC            IN                   NUMBER,
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   );

   PROCEDURE SP_PFC_S_LISTARCOMP (
      PN_CODMAC          IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR
   );

   PROCEDURE SP_PFC_I_REGISTRO_PROD_ASOC (
      PN_CODMAC             IN                    NUMBER,
      PV_COD_PRODUCTO       IN                    VARCHAR2,
      PV_DESCRIPCION        IN                    VARCHAR2,
      PV_NOMBRE_COMERCIAL   IN                    VARCHAR2,
      PN_COD_ESTADO         IN                    NUMBER,
      PN_COD_LABORATORIO    IN                    NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_S_LISTAR_PROD_ASOC (
      PN_CODMAC          IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR
   );
    
    /* AUTHOR  : Jose.Reyes/MDP*/
  /* CREATED : 28/05/2019*/
  /* PURPOSE : Registro de Examenes Medico Marcadores (MAESTROS)                          */

   PROCEDURE SP_PFC_I_REG_EXAM_MED_MARCA (
      PN_COD_EXAMEN_MED      IN                     NUMBER,
      PV_DESCRIPCION         IN                     VARCHAR2,
      PN_COD_TIPO_EXAMEN     IN                     NUMBER,
      PN_COD_ESTADO          IN                     NUMBER,
      PV_USUARIO_CREA        IN                     VARCHAR2,
      PD_FECHA_CREA          IN                     DATE,
      PV_USUARIO_MODIF       IN                     VARCHAR2,
      PD_FECHA_MODIF         IN                     DATE,
      PN_COD_EXAMEN_MEDICO   OUT                    NUMBER,
      PN_COD_RESULTADO       OUT                    NUMBER,
      PV_MSG_RESULTADO       OUT                    VARCHAR2
   );
    
  /* AUTHOR  : Jose.Reyes/MDP*/
  /* CREATED : 28/05/2019*/
  /* PURPOSE : Registro de Examenes Medico Detalle (MAESTROS)    */

   PROCEDURE SP_PFC_I_REG_EXAM_MED_DET (
      PN_COD_EXAMEN_MED_DET     IN                        NUMBER,
      PN_COD_EXAMEN_MED         IN                        NUMBER,
      PN_COD_TIPO_INGRESO_RES   IN                        NUMBER,
      PV_UNIDAD_MEDIDA          IN                        VARCHAR2,
      PV_RANGO                  IN                        VARCHAR2,
      PV_VALOR_FIJO             IN                        VARCHAR2,
      PN_COD_ESTADO             IN                        NUMBER,
      PN_RANGO_MINIMO           IN                        NUMBER,
      PN_RANGO_MAXIMO           IN                        NUMBER,
      PV_USUARIO_CREA           IN                        VARCHAR2,
      PD_FECHA_CREA             IN                        DATE,
      PV_USUARIO_MODIF          IN                        VARCHAR2,
      PD_FECHA_MODIF            IN                        DATE,
      PN_COD_RESULTADO          OUT                       NUMBER,
      PV_MSG_RESULTADO          OUT                       VARCHAR2
   );
    
  /* AUTHOR  : Jose.Reyes/MDP*/
  /* CREATED : 28/05/2019*/
  /* PURPOSE : Consulta lista de Examenes Medico (MAESTROS)    */

   PROCEDURE SP_PFC_S_LISTAR_EXAMS_MED (
      PV_DESCRIPCION     IN                 VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      TC_LIST_DETALLE    OUT                PCK_PFC_CONSTANTE.TYPCUR
   );

   PROCEDURE SP_PFC_I_REGISTRO_MARCA (
      PN_COD_CONFIG_MARCA         IN                          NUMBER,
      PV_COD_CONFIG_MARCA_LARGO   IN                          VARCHAR2,
      PN_COD_EXAMEN_MED           IN                          NUMBER,
      PN_COD_MAC                  IN                          NUMBER,
      PN_COD_GRP_DIAG             IN                          NUMBER,
      PN_TIPO_MARCADOR            IN                          NUMBER,
      PN_PER_MAXIMA               IN                          NUMBER,
      PN_PER_MINIMA               IN                          NUMBER,
      PN_COD_ESTADO               IN                          NUMBER,
      PN_COD_RESULTADO            OUT                         NUMBER,
      PV_MSG_RESULTADO            OUT                         VARCHAR2
   );

   PROCEDURE SP_PFC_S_LISTAR_MARCADORES (
      PN_COD_MAC         IN                 NUMBER,
      PN_COD_GRP_DIAG    IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR
   );
   
   /* AUTHOR  : Jose.Reyes/MDP*/
   /* CREATED : 01/07/2019*/
   /* PURPOSE : Consulta lista de Participantes (MAESTROS)    */
    PROCEDURE SP_PFC_S_LISTAR_PARTICIPANTES (
      PV_APELLIDO_NOMBRE IN                 VARCHAR2,
      PN_COD_USUARIO     IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      TC_LIST_GRP        OUT                PCK_PFC_CONSTANTE.TYPCUR
   );
   
   /* AUTHOR  : Jose.Reyes/MDP*/
   /* CREATED : 02/07/2019*/
   /* PURPOSE : Elimina Detalle de Examen Medico    */
   PROCEDURE SP_PFC_D_EXAMEN_MED_DET (
      PN_COD_EXAMEN_MED  IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );
   
   PROCEDURE SP_PFC_I_U_PARTICIPANTES (
      PN_COD_PARTICIPANTE IN                 NUMBER,
      PN_COD_USUARIO      IN                 NUMBER,
      PV_ESTADO_PARTICIPANTE  IN             VARCHAR2,
      PV_CMP_MEDICO           IN             VARCHAR2,
      PV_NOMBRE_FIRMA         IN             VARCHAR2,
      PN_COD_ROL              IN             NUMBER,
      PN_COD_ARCHIVO_FIRMA    IN             NUMBER,
      PV_CORREO_ELECTRONICO   IN             VARCHAR2,
      PV_NOMBRES              IN             VARCHAR2,
      PV_APELLIDOS            IN             VARCHAR2,
      PN_P_ESTADO             IN             NUMBER,
      PN_COORDINADOR          IN             NUMBER,
      PN_COD_RESULTADO   OUT                 NUMBER,
      PV_MSG_RESULTADO   OUT                 VARCHAR2,
      PN_CODIGO_PARTICIPANTE   OUT           NUMBER
   );
   
   PROCEDURE SP_PFC_I_U_PARTICIPANTES_GRUPO (
      PN_COD_PARTICIPANTE IN                 NUMBER,
      PN_P_RANGO_EDAD     IN                 NUMBER,
      PV_COD_GRP_DIAG     IN                 VARCHAR2,
      PN_COD_RESULTADO   OUT                 NUMBER,
      PV_MSG_RESULTADO   OUT                 VARCHAR2
   );
   
    PROCEDURE SP_PFC_I_CHKLIST_INDICACION (
      PN_COD_CHKLIST_INDI       IN                    NUMBER,
      PN_COD_CHKLIST_INDI_LARGO IN                    VARCHAR2,
      PV_DESCRIPCION            IN                    VARCHAR2,
      PN_P_ESTADO               IN                    NUMBER,
      PN_COD_MAC                IN                    NUMBER,
      PN_COD_GRP_DIAG           IN                    NUMBER,
      PV_L_CRITERIO_INCLU       IN                    VARCHAR2,
      PV_L_CRITERIO_EXCLU       IN                    VARCHAR2,
      PN_COD_RESULTADO          OUT                   NUMBER,
      PV_MSG_RESULTADO          OUT                   VARCHAR2
   );
   
   PROCEDURE SP_PFC_D_GRUPO_PARTICPANTES (
      PN_COD_PARTICIPANTE  IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ); 
   
   /*
   AUTHOR  : CHRISTIAN ALTAMIRANO
   CREATED : 06/08/2019
   PURPOSE : Validacion de rango de edad para registro de de participante 
   */
   PROCEDURE SP_PFC_S_VALIDAR_R_PARTICIPA (
      PN_COD_GRUPO_DIAG   IN                 NUMBER,
      PN_COD_RANGO_EDAD   IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

END PCK_PFC_CONFIGURACION_SISTEMA;

/
--------------------------------------------------------
--  DDL for Package PCK_PFC_INDICADORES
--------------------------------------------------------

  CREATE OR REPLACE  PACKAGE "ONTPFC"."PCK_PFC_INDICADORES" 
AS
  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 14/05/2019
  -- PURPOSE : Reporte Indicadores de Proceso

PROCEDURE SP_PFC_S_INDICADORES_PROCESO (PN_FEC_MES     IN NUMBER,
                    PN_FEC_ANO     IN NUMBER,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2);
  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 14/05/2019
  -- PURPOSE : Validar Indicadores de Proceso Por MES y AÑO
PROCEDURE SP_PFC_S_VALIDAR_INDICADORES (PN_FEC_MES     IN NUMBER,
                    PN_FEC_ANO     IN NUMBER,
                                           PN_CAN_REGISTROS       OUT NUMBER,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2);  
 -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 28/06/2019
  -- PURPOSE : Validar Indicadores de Proceso Por MES y AÑO                                           
PROCEDURE SP_PFC_S_INDICADORES_PROC_XLS (PN_FEC_MES     IN NUMBER,
                     PN_FEC_ANO     IN NUMBER,
                                           OP_OBJCURSOR       OUT SYS_REFCURSOR,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2) ;                                      

PROCEDURE SP_PFC_S_INDICADORES_CONS_XLS (PN_FEC_MES     IN NUMBER,
                        PN_FEC_ANO     IN NUMBER,
                                            PN_TIPO        IN NUMBER, 
                                            OP_OBJCURSOR       OUT SYS_REFCURSOR,
                                            PN_COD_RESULTADO   OUT NUMBER,
                                            PV_MSG_RESULTADO   OUT VARCHAR2) ;

  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 08/07/2019
  -- PURPOSE : Generar Datos Reporte Solicitud de Autrorizaciones

PROCEDURE SP_PFC_I_REP_SOL_AUTOR (PN_FEC_MES     IN NUMBER,
                    PN_FEC_ANO     IN NUMBER,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2);  

  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED :  08/07/2019
  -- PURPOSE : Listar Reporte de Solicitudes de Autorizaciones                                            
PROCEDURE SP_PFC_S_REP_SOL_AUTOR (PN_FEC_MES     IN NUMBER,
                     PN_FEC_ANO     IN NUMBER,
                                           OP_OBJCURSOR       OUT SYS_REFCURSOR,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2) ;                                                    


 -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 12/07/2019
  -- PURPOSE : Generar Datos Reporte Solicitud de Monitoreo

PROCEDURE SP_PFC_I_REP_SOL_MONITOR (PN_FEC_MES     IN NUMBER,
                    PN_FEC_ANO     IN NUMBER,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2); 
                                           
 -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED :  12/07/2019
  -- PURPOSE : Listar Reporte de Solicitudes de Monitoreo                                            
PROCEDURE SP_PFC_S_REP_SOL_MONITOR (PN_FEC_MES     IN NUMBER,
                     PN_FEC_ANO     IN NUMBER,
                                           OP_OBJCURSOR       OUT SYS_REFCURSOR,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2) ;       

END PCK_PFC_INDICADORES;

/
--------------------------------------------------------
--  DDL for Package PCK_PFC_MAESTROS
--------------------------------------------------------

  CREATE OR REPLACE  PACKAGE "ONTPFC"."PCK_PFC_MAESTROS" AS

  /* TODO enter package declarations (types, exceptions, methods etc) here */
  -- AUTHOR  : AFLORES
  -- CREATED : 22/01/2019
  -- PURPOSE : INSERCION DE LOS VALORES DE EXAMEN MEDICO (MAESTROS)
  PROCEDURE SP_PFC_I_U_EXAMEN_MEDICO(
                                    PN_COD_EXAMEN_MED  IN  NUMBER,
                                    PN_COD_EX_MEDICO_L IN  VARCHAR2,
                                    PN_COD_ESTADO      IN  NUMBER,
                                    PV_DESC_EX         IN  VARCHAR2,
                                    PN_COD_TIP_EXAMEN  IN  NUMBER,
                                    PN_COD_TIP_RESUL   IN  NUMBER,
                                    PV_UNID_MED        IN  VARCHAR2,
                                    PV_DESC_RANGO1     IN  VARCHAR2,
                                    PV_DESC_RANGO2     IN  VARCHAR2,
                                    PV_COD_POS_VALORES IN  VARCHAR2,
                                    PV_TEXTO           IN  VARCHAR2,
                                    PN_COD_RESULTADO   OUT NUMBER,
                                    PV_MSG_RESULTADO   OUT VARCHAR2);

  -- AUTHOR  : AFLORES
  -- CREATED : 22/01/2019
  -- PURPOSE : Consulta  para el listado de examenes medico (MAESTROS)
  PROCEDURE SP_PFC_S_EXAMENMEDICO( PV_DESCRIPCION    IN VARCHAR2,
                                AC_LISTA_EXAMENES OUT PCK_PFC_CONSTANTE.TYPCUR,
                                PN_COD_RESULTADO  OUT NUMBER,
                                PV_MSG_RESULTADO  OUT VARCHAR2);
  -- AUTHOR  : PAVEL
  -- CREATED : 04/08/2019
  -- PURPOSE : Consulta para listados de MAC's segun filtro (MAESTROS)
  PROCEDURE SP_PFC_S_FILTRO_MAC(
                         PN_P_COD IN  VARCHAR2,
                         PN_P_DES IN  VARCHAR2,
                         PN_P_COM IN VARCHAR2,
                         TC_LIST          OUT PCK_PFC_CONSTANTE.TYPCUR,
                         PN_COD_RESULTADO OUT NUMBER,
                         PV_MSG_RESULTADO OUT VARCHAR2);

END PCK_PFC_MAESTROS;

/
--------------------------------------------------------
--  DDL for Package PCK_PFC_REPORTE_EVALUACION
--------------------------------------------------------

  CREATE OR REPLACE  PACKAGE "ONTPFC"."PCK_PFC_REPORTE_EVALUACION" AS

  -- AUTHOR  : CATALINA
  -- CREATED : 04/02/2019
  -- PURPOSE : Reporte Datos generales relacionados a una Solicitud de Evaluacion
 PROCEDURE SP_PFC_S_DET_EVALUACION_REPORT(PN_COD_SOL_EVA     IN NUMBER,
                                           OP_OBJCURSOR       OUT SYS_REFCURSOR,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2);

  PROCEDURE SP_PFC_S_LINEA_TRAT_PDF(       PN_COD_SOL_EVA      IN NUMBER,
                                           TC_LIST_LT          OUT PCK_PFC_CONSTANTE.TYPCUR,
                                           TC_LIST_MS          OUT PCK_PFC_CONSTANTE.TYPCUR,
                                           TC_LIST_LM          OUT PCK_PFC_CONSTANTE.TYPCUR,
                                           TC_LIST_CB          OUT PCK_PFC_CONSTANTE.TYPCUR,
                                           PN_COD_RESULTADO    OUT NUMBER,
                                           PV_MSG_RESULTADO    OUT VARCHAR2);

  PROCEDURE SP_PFC_S_PDF_ANALISIS_CONCLU(PN_COD_SOL_EVA                   IN NUMBER,
                                         AC_LIST                          OUT PCK_PFC_CONSTANTE.TYPCUR,
                                         PN_COD_RESULTADO                 OUT NUMBER,
                                         PV_MSG_RESULTADO                 OUT VARCHAR2);

  PROCEDURE SP_PFC_S_PDF_MEDICO_AUDITOR(
                                 PN_COD_SOL_EVA                   IN NUMBER,
                                 V_NOMBRE_COMPLETO                OUT VARCHAR2,
                                 PN_COD_RESULTADO                 OUT NUMBER,
                                 PV_MSG_RESULTADO                 OUT VARCHAR2);
                                 
  PROCEDURE SP_PFC_S_VALIDACION_PDF( PN_COD_EVA                   IN NUMBER,
                                    PN_COD_RESULTADO              OUT NUMBER,
                                    PV_MSG_RESULTADO              OUT VARCHAR2);

END PCK_PFC_REPORTE_EVALUACION;

/
--------------------------------------------------------
--  DDL for Package PCK_PFC_SOLBEN
--------------------------------------------------------

  CREATE OR REPLACE  PACKAGE "ONTPFC"."PCK_PFC_SOLBEN" IS
   PROCEDURE SP_PFC_SI_SCG_SOLBEN_PREL (
      PV_COD_SCG_SOLBEN                IN                               VARCHAR2,
      PV_COD_CLINICA                   IN                               VARCHAR2,
      PV_COD_AFI_PACIENTE              IN                               VARCHAR2,
      PN_EDAD_PACIENTE                 IN                               NUMBER,
      PV_DES_CONTRATANTE               IN                               VARCHAR2,
      PV_DES_PLAN                      IN                               VARCHAR2,
      PV_FEC_AFILIACION                IN                               VARCHAR2,
      PV_COD_DIAGNOSTICO               IN                               VARCHAR2,
      PV_CMP_MEDICO                    IN                               VARCHAR2,
      PV_MEDICO_TRATANTE_PRESCRIPTOR   IN                               VARCHAR2,
      PV_FEC_RECETA                    IN                               VARCHAR2,
      PV_FEC_QUIMIO                    IN                               VARCHAR2,
      PV_FEC_HOSP_INICIO               IN                               VARCHAR2,
      PV_FEC_HOSP_FIN                  IN                               VARCHAR2,
      PV_FEC_SCG_SOLBEN                IN                               VARCHAR2,
      PV_HORA_SCG_SOLBEN               IN                               VARCHAR2,
      PV_TIPO_SCG_SOLBEN               IN                               VARCHAR2,
      PV_ESTADO_SCG                    IN                               VARCHAR2,
      PV_DESC_MEDICAMENTO              IN                               VARCHAR2,
      PV_DESC_ESQUEMA                  IN                               VARCHAR2,
      PN_TOTAL_PRESUPUESTO             IN                               NUMBER,
      PV_DESC_PROCEDIMIENTO            IN                               VARCHAR2,
      PV_PERSON_CONTACTO               IN                               VARCHAR2,
      PV_OBS_CLINICA                   IN                               VARCHAR2,
      PN_IND_VALIDA                    IN                               NUMBER,
      PV_TX_DATO_ADIC1                 IN                               VARCHAR2,
      PV_TX_DATO_ADIC2                 IN                               VARCHAR2,
      PV_TX_DATO_ADIC3                 IN                               VARCHAR2,
      PV_FEC_ACTUAL                    IN                               VARCHAR2,
      PV_COD_GRP_DIAGNOS               IN                               VARCHAR2,
      PV_OUT_COD_SCG_SOLBEN            OUT                              VARCHAR2,
      PN_OUT_COD_SOL_PRE               OUT                              NUMBER,
      PV_OUT_FEC_SOL_PRE               OUT                              VARCHAR2,
      PV_OUT_HORA_SOL_PRE              OUT                              VARCHAR2,
      PN_OUT_COD_RESULTADO             OUT                              NUMBER,
      PV_OUT_MSG_RESULTADO             OUT                              VARCHAR2
   );

   PROCEDURE SP_PFC_SU_SOLBEN_ACTUALIZAR (
      PV_COD_SCG_SOLBEN     IN OUT                VARCHAR2,
      PV_COD_AFI_PACIENTE   IN                    VARCHAR2,
      PV_COD_ESTADO_SCG     IN                    NUMBER,
      PN_TIPO_OPERACION     IN                    NUMBER,
      PV_NRO_CG             IN                    VARCHAR2,
      PV_FEC_CG             IN                    VARCHAR2,
      PV_TX_DATO_ADIC1      IN OUT                VARCHAR2,
      PV_TX_DATO_ADIC2      IN OUT                VARCHAR2,
      PV_TX_DATO_ADIC3      IN OUT                VARCHAR2,
      PV_COD_SOL_EVAL       OUT                   VARCHAR2,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

END PCK_PFC_SOLBEN;

/
--------------------------------------------------------
--  DDL for Package PCK_PFC_UTIL
--------------------------------------------------------

  CREATE OR REPLACE  PACKAGE "ONTPFC"."PCK_PFC_UTIL" AS
   PROCEDURE SP_PFC_S_FILTRO_PARAMETRO (
      PV_COD_GRUPO       IN                 VARCHAR2,
      TC_LIST_PARAM      OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_S_FILTRO_USR_ROL (
      PN_CODIGO          IN                 VARCHAR,
      AC_LISTA           OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_S_ROL_RESPONSABLE (
      TC_LIST_ROL        OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_TIPO_CORREO (
      PN_COD_TIPO_CORREO   IN                   NUMBER,
      PV_ASUNTO            OUT                  VARCHAR2,
      PV_CUERPO            OUT                  VARCHAR2,
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   );

   PROCEDURE SP_PFC_CODIGO_LARGO (
      PN_COD             IN                 NUMBER,
      PN_LONGITUD        IN                 NUMBER,
      PV_COD_LARGO       OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_PARAM_VAL (
      PN_COD_GRUPO       IN                 NUMBER,
      PN_CODIGO          IN                 VARCHAR2,
      PN_VALUE           IN                 VARCHAR2,
      PN_COD_PARAMETRO   IN                 NUMBER,
      TC_LIST_PAR        OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_PARAMETRO (
      PN_COD_PARAMETRO   IN                 NUMBER,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

           
                                     
     /*PROCEDURE SP_PFC_I_PERSONA(TY_PERSONA          IN TO_PERSONA,
                                PN_COD_RESULTADO    OUT NUMBER,
                                PV_MSG_RESULTADO    OUT VARCHAR2);*/

   PROCEDURE SP_PFC_S_VALID_LIDER_TUMOR (
      PV_CMP_MED             IN                     VARCHAR2,
      PV_COD_GRP_DIAG        IN                     VARCHAR2,
      PN_COD_ROL_LIDER_TUM   OUT                    NUMBER,
      PN_COD_USR_LIDER_TUM   OUT                    NUMBER,
      PV_USR_LIDER_TUM       OUT                    VARCHAR2,
      PN_CMP_LIDER_TUMOR     OUT                    NUMBER,
      PN_COD_RESULTADO       OUT                    NUMBER,
      PV_MSG_RESULTADO       OUT                    VARCHAR2
   );

   PROCEDURE SP_PFC_SU_SOLBEN_ACTUALIZAR (
      PV_COD_SCG_SOLBEN     IN OUT                VARCHAR2,
      PV_COD_AFI_PACIENTE   IN                    VARCHAR2,
      PV_COD_ESTADO_SCG     IN                    VARCHAR2,
      PV_FEC_ESTADO_SCG     IN                    VARCHAR2,
      PV_NRO_CG             IN                    VARCHAR2,
      PV_FEC_CG             IN                    VARCHAR2,
      PV_TX_DATO_ADIC1      IN OUT                VARCHAR2,
      PV_TX_DATO_ADIC2      IN OUT                VARCHAR2,
      PV_TX_DATO_ADIC3      IN OUT                VARCHAR2,
      PV_COD_SOL_EVAL       OUT                   VARCHAR2,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   );

   PROCEDURE SP_PFC_COD_FECHA (
      PN_COD_FECHA       IN                 VARCHAR2,
      PN_COD             IN                 NUMBER,
      PN_LONGITUD        IN                 NUMBER,
      PV_COD_LARGO       OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_USUARIO_FARMACIA (
      PN_COD_ROL         IN                 NUMBER,
      PN_ESTADO_USR      IN                 NUMBER,
      TC_MIEMBRO_CMAC    OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   );

   PROCEDURE SP_PFC_S_CMP_MEDICO (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PV_CMP_MEDICO        OUT                  VARCHAR2,
      PV_MEDICO_TRATANTE   OUT                  VARCHAR2,
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   );

END PCK_PFC_UTIL;

/
