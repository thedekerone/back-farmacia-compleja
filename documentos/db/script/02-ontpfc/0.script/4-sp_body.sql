--------------------------------------------------------
--  DDL for Package Body PCK_PFC_BANDEJA_EVALUACION
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "ONTPFC"."PCK_PFC_BANDEJA_EVALUACION" AS

   -- AUTOR     : CATALINA
   -- CREADO    : 14/01/2019
   -- PROPOSITO : Lista un Detalle de Solicitud de Evaluacion

   PROCEDURE SP_PFC_S_DET_EVALUACION (
      PN_COD_SOL_EVA          IN                      NUMBER,
      TC_DETALLE              OUT                     SYS_REFCURSOR,
      PN_FLAG_VER_INFORME     OUT                     NUMBER,
      PV_REPORTE_PDF          OUT                     VARCHAR2,
      PN_FLAG_VER_ACTA_CMAC   OUT                     NUMBER,
      PV_REPORTE_ACTA_CMAC    OUT                     VARCHAR2,
      PN_COD_ROL_LIDER_TUM    OUT                     NUMBER,
      PN_COD_USR_LIDER_TUM    OUT                     NUMBER,
      PV_USR_LIDER_TUM        OUT                     VARCHAR2,
      PN_COD_ARCH_FICHA_TEC   OUT                     NUMBER,
      PN_COD_ARCH_COMPL_MED   OUT                     NUMBER,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   ) AS
      V_REPORTE_PDF          VARCHAR2(100);
      V_REPORTE_ACTA         VARCHAR2(100);
      V_REPORTE_ACTA_ESCAN   VARCHAR2(100);
      V_EXC_RESULTADO EXCEPTION;
      V_MENSAJE              VARCHAR2(500);
      V_CMP_MEDICO           VARCHAR2(20);
      V_COD_GRP_DIAG         VARCHAR2(20);
      V_COD_MAC              NUMBER(7);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'VALOR INICIAL';
      DBMS_OUTPUT.PUT_LINE('1');
      BEGIN
         SELECT
            SB.CMP_MEDICO,
            SB.COD_GRP_DIAG,
            PE.COD_MAC
         INTO
            V_CMP_MEDICO,
            V_COD_GRP_DIAG,
            V_COD_MAC
         FROM
            PFC_SOLICITUD_EVALUACION   SE
            INNER JOIN PFC_SOLICITUD_PRELIMINAR   PE ON SE.COD_SOL_PRE = PE.COD_SOL_PRE
            INNER JOIN PFC_SOLBEN                 SB ON SB.COD_SCG = PE.COD_SCG
         WHERE
            SE.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            PN_COD_RESULTADO   := 1;
            V_MENSAJE          := 'NO SE ENCONTRARON RESULTADOS EN LA TABLA SOLBEN';
            RAISE V_EXC_RESULTADO;
         WHEN OTHERS THEN
            PN_COD_RESULTADO   := -2;
            V_MENSAJE          := TO_CHAR(SQLCODE)
                         || ' : '
                         || SQLERRM;
            RAISE V_EXC_RESULTADO;
      END;

      OPEN TC_DETALLE FOR SELECT
                            S.COD_SCG_SOLBEN,
                            S.ESTADO_SCG,
                            (
                               SELECT
                                  P.NOMBRE
                               FROM
                                  PFC_PARAMETRO P
                               WHERE
                                  P.CODIGO = S.ESTADO_SCG AND
                                  P.COD_GRUPO = 5
                            ) AS DESCRIP_ESTADO_SOLBEN,
                            S.FEC_SCG_SOLBEN,
                            S.HORA_SCG_SOLBEN,
                            S.TIPO_SCG_SOLBEN,
                            (
                               SELECT
                                  P.NOMBRE
                               FROM
                                  PFC_PARAMETRO P
                               WHERE
                                  P.COD_PARAMETRO = S.TIPO_SCG_SOLBEN
                            ) AS DESCRIP_TIPO_SCG_SOLBEN,
                            S.COD_CLINICA,
                            S.MEDICO_TRATANTE_PRESCRIPTOR,
                            S.CMP_MEDICO,
                            S.FEC_RECETA,
                            S.FEC_QUIMIO,
                            S.FEC_HOSP_INICIO,
                            S.FEC_HOSP_FIN,
                            S.DESC_MEDICAMENTO,
                            S.DESC_ESQUEMA,
                            S.PERSON_CONTACTO,
                            S.TOTAL_PRESUPUESTO,
                            S.OBS_CLINICA,
                            S.EDAD_PACIENTE,
                            S.COD_DIAGNOSTICO,
                            S.DES_CONTRATANTE,
                            S.DES_PLAN,
                            S.COD_AFI_PACIENTE,
                            S.FEC_AFILIACION,
                            S.NRO_CG,
                            S.COD_GRP_DIAG,
                            M.COD_MAC_LARGO,
                            M.DESCRIPCION,
                            SE.P_ESTADO_SOL_EVA,
                            SE.P_TIPO_EVA,
                            (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = SE.P_ESTADO_SOL_EVA)
                            AS NOMBRE_ESTADO,
                            TO_CHAR(SE.FEC_SOL_EVA,'DD/MM/YYYY') AS FEC_SOL_EVA_ACT
                         FROM
                            PFC_SOLBEN                 S,
                            PFC_SOLICITUD_PRELIMINAR   SP,
                            PFC_SOLICITUD_EVALUACION   SE,
                            PFC_MAC                    M
                         WHERE
                            SP.COD_SCG = S.COD_SCG
                            AND SP.COD_SOL_PRE  = SE.COD_SOL_PRE
                            AND SP.COD_MAC      = M.COD_MAC
                            AND SE.COD_SOL_EVA  = PN_COD_SOL_EVA;

      BEGIN
         SELECT
            SE.COD_INFORME_AUTO
         INTO PV_REPORTE_PDF
         FROM
            PFC_SOLICITUD_EVALUACION SE
         WHERE
            SE.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            PV_REPORTE_PDF := NULL;
      END;

      BEGIN
         SELECT
            PR.COD_ROL,
            PR.COD_USUARIO,
            PR.NOMBRES
            || ' '
            || PR.APELLIDOS
         INTO
            PN_COD_ROL_LIDER_TUM,
            PN_COD_USR_LIDER_TUM,
            PV_USR_LIDER_TUM
         FROM
            ONTPFC.PFC_PARTICIPANTE              PR
            INNER JOIN ONTPFC.PFC_PARTICIPANTE_GRUPO_DIAG   PRGD ON PRGD.COD_PARTICIPANTE = PR.COD_PARTICIPANTE
         WHERE
            PRGD.COD_GRP_DIAG = V_COD_GRP_DIAG
            AND PR.CMP_MEDICO  = V_CMP_MEDICO
            AND PR.COD_ROL     = PCK_PFC_CONSTANTE.PN_CODIGO_LIDER_TUMOR;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            PN_COD_ROL_LIDER_TUM   := NULL;
            PN_COD_USR_LIDER_TUM   := NULL;
         WHEN OTHERS THEN
            PN_COD_RESULTADO   := 3;
            V_MENSAJE          := TO_CHAR(SQLCODE)
                         || ' : '
                         || SQLERRM;
            RAISE V_EXC_RESULTADO;
      END;

      IF PV_REPORTE_PDF IS NOT NULL THEN
         PN_FLAG_VER_INFORME := 1;
      ELSE
         PN_FLAG_VER_INFORME := 0;
      END IF;

      BEGIN
         SELECT
            PCM.COD_ACTA_FTP,
            PCM.REPORTE_ACTA_ESCANEADA
         INTO
            V_REPORTE_ACTA,
            V_REPORTE_ACTA_ESCAN
         FROM
            PFC_PROGRAMACION_CMAC_DET   PCD
            INNER JOIN PFC_PROGRAMACION_CMAC       PCM ON PCD.COD_PROGRAMACION_CMAC = PCM.COD_PROGRAMACION_CMAC
         WHERE
            PCD.COD_SOL_EVA = PN_COD_SOL_EVA;

         PN_FLAG_VER_ACTA_CMAC := 1;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_REPORTE_ACTA          := NULL;
            V_REPORTE_ACTA_ESCAN    := NULL;
            PN_FLAG_VER_ACTA_CMAC   := 0;
      END;
      
      V_REPORTE_ACTA         := TRIM(V_REPORTE_ACTA);
      V_REPORTE_ACTA_ESCAN   := TRIM(V_REPORTE_ACTA_ESCAN);
      DBMS_OUTPUT.PUT_LINE('V_REPORTE_ACTA : '||V_REPORTE_ACTA);
      DBMS_OUTPUT.PUT_LINE('V_REPORTE_ACTA_ESCAN : '||V_REPORTE_ACTA_ESCAN);
      IF NULLIF(V_REPORTE_ACTA,'') IS NULL AND NULLIF(V_REPORTE_ACTA_ESCAN,'') IS NULL THEN
         DBMS_OUTPUT.PUT_LINE('NULL : NULL');
         PN_FLAG_VER_ACTA_CMAC   := 0;
         PV_REPORTE_ACTA_CMAC    := NULL;
      ELSIF NULLIF(V_REPORTE_ACTA,'') IS NOT NULL AND NULLIF(V_REPORTE_ACTA_ESCAN,'') IS NULL THEN
         DBMS_OUTPUT.PUT_LINE('NOT NULL : NULL');
         PV_REPORTE_ACTA_CMAC    := V_REPORTE_ACTA;
         PN_FLAG_VER_ACTA_CMAC   := 1;
      ELSIF NULLIF(V_REPORTE_ACTA,'') IS NOT NULL AND NULLIF(V_REPORTE_ACTA_ESCAN,'') IS NOT NULL  THEN
         DBMS_OUTPUT.PUT_LINE('NOT NULL : NOT NULL');
         PV_REPORTE_ACTA_CMAC    := V_REPORTE_ACTA_ESCAN;
         PN_FLAG_VER_ACTA_CMAC   := 1;
      ELSIF NULLIF(V_REPORTE_ACTA,'') IS NULL  AND NULLIF(V_REPORTE_ACTA_ESCAN,'') IS NOT NULL THEN
         DBMS_OUTPUT.PUT_LINE('NULL : NOT NULL');
         PV_REPORTE_ACTA_CMAC    := V_REPORTE_ACTA_ESCAN;
         PN_FLAG_VER_ACTA_CMAC   := 1;
      ELSE
         DBMS_OUTPUT.PUT_LINE('NULL : NULL');
         PV_REPORTE_ACTA_CMAC    := NULL;
         PN_FLAG_VER_ACTA_CMAC   := 0;
      END IF;

      BEGIN
         SELECT
            FT.COD_FILE_UPLOAD
            INTO PN_COD_ARCH_FICHA_TEC
         FROM
            PFC_FICHA_TECNICA FT
         WHERE
            FT.COD_MAC = V_COD_MAC
            AND FT.FEC_FIN_VIG IS NULL
            AND FT.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO;
      EXCEPTION
         WHEN OTHERS THEN
            PN_COD_ARCH_FICHA_TEC := NULL;
      END;
      
      BEGIN 
         SELECT
            AC.CODIGO_ARCHIVO_COMP
            INTO PN_COD_ARCH_COMPL_MED
         FROM
            PFC_ARCHIVO_COMPLICACION AC
         WHERE
            AC.COD_MAC = V_COD_MAC
            AND AC.FEC_FIN_VIG IS NULL
            AND AC.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO;
      EXCEPTION
         WHEN OTHERS THEN
            PN_COD_ARCH_COMPL_MED := NULL;
      END;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA';
   EXCEPTION
      WHEN V_EXC_RESULTADO THEN
         PV_MSG_RESULTADO := V_MENSAJE;
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_DET_EVALUACION]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_DET_EVALUACION;

  /* AUTHOR  : CATALINA*/
  /* CREATED : 15/01/2019*/
  /* PURPOSE : HISTORIAL LINEAS DE TRATAMIENTO HISTORICO*/

   PROCEDURE SP_PFC_S_HIST_LINEA_TRATAMIENT (
      PV_COD_AFILIADO    IN                 VARCHAR2,
      OP_OBJCURSOR       OUT                SYS_REFCURSOR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      V_MENSAJE VARCHAR2(100);
      V_EXC_VARIABLE EXCEPTION;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF NULLIF(
         PV_COD_AFILIADO,
         ''
      ) IS NULL THEN
         V_MENSAJE := 'No cuenta con codigo afiliado';
         RAISE V_EXC_VARIABLE;
      END IF;

      OPEN OP_OBJCURSOR FOR SELECT
                              HT.LINEA_TRAT,
                              (
                                 SELECT
                                    MAC.DESCRIPCION
                                 FROM
                                    ONTPFC.PFC_MAC MAC
                                 WHERE
                                    MAC.COD_MAC = HT.COD_MAC
                              ) AS MEDICAMENTOSOLICITADO,
                              SE.COD_DESC_SOL_EVA,
                              HT.FEC_APROBACION,
                              HT.FEC_INICIO,
                              HT.FEC_FIN,
                              (
                                 SELECT
                                    PAR.NOMBRE
                                 FROM
                                    ONTPFC.PFC_PARAMETRO PAR
                                 WHERE
                                    PAR.COD_PARAMETRO = HT.P_CURSO
                              ) AS P_CURSO,
                              (
                                 SELECT
                                    PAR.NOMBRE
                                 FROM
                                    ONTPFC.PFC_PARAMETRO PAR
                                 WHERE
                                    PAR.COD_PARAMETRO = HT.P_TIPO_TUMOR
                              ) AS P_TIPO_TUMOR,
                              (
                                 SELECT
                                    PAR.NOMBRE
                                 FROM
                                    ONTPFC.PFC_PARAMETRO PAR
                                 WHERE
                                    PAR.COD_PARAMETRO = HT.P_RESP_ALCANZADA
                              ) AS P_RESP_ALCANZADA,
                              (
                                 SELECT
                                    PAR.NOMBRE
                                 FROM
                                    ONTPFC.PFC_PARAMETRO PAR
                                 WHERE
                                    PAR.COD_PARAMETRO = HT.P_ESTADO
                              ) AS DESC_P_ESTADO,
                              (
                                 SELECT
                                    PAR.NOMBRE
                                 FROM
                                    ONTPFC.PFC_PARAMETRO PAR
                                 WHERE
                                    PAR.COD_PARAMETRO = HT.P_MOTIVO_INACTIVACION
                              ) AS P_MOTIVO_INACTIVACION,
                              HT.MEDICO_TRATANTE    AS MEDICO_TRATANTE_PRESCRIPTOR,
                              HT.MONTO_AUTORIZADO   AS MONTO_AUTORIZADO,
                              HT.COD_AFILIADO       AS COD_AFI_PACIENTE,
                              HT.P_ESTADO           AS P_ESTADO
                           FROM
                              ONTPFC.PFC_HIST_LINEA_TRATAMIENTO   HT
                              LEFT JOIN ONTPFC.PFC_SOLICITUD_EVALUACION     SE ON SE.COD_SOL_EVA = HT.COD_SOL_EVA
                              LEFT JOIN ONTPFC.PFC_SOLICITUD_PRELIMINAR     P ON P.COD_SOL_PRE = SE.COD_SOL_PRE
                              LEFT JOIN ONTPFC.PFC_SOLBEN                   S ON S.COD_SCG = P.COD_SCG
                              LEFT JOIN ONTPFC.PFC_MAC                      MAC ON MAC.COD_MAC = HT.COD_MAC
                           WHERE
                              HT.COD_AFILIADO = PV_COD_AFILIADO
                           ORDER BY
                              HT.FEC_FIN DESC;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta exitosa';
   EXCEPTION
      WHEN V_EXC_VARIABLE THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_HIST_LINEA_TRAMIENTO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_HIST_LINEA_TRATAMIENT;

  /* AUTHOR     : CATALINA*/
  /* CREATED    : 16/01/2019*/
  /* MODIFICADO : JAZABACHE*/
  /* PURPOSE    : Registra Linea de Tratamiento Histórico*/

   PROCEDURE SP_PFC_SI_HIST_LINEA_TRATAMIEN (
      PV_COD_AFI_PACIENTE        IN                         VARCHAR2,
      PN_COD_SOL_EVA             IN                         NUMBER,
      PN_COD_MAC_SOL             IN                         NUMBER,
      PV_FEC_INICIO              IN                         VARCHAR2,
      PV_FEC_FIN                 IN                         VARCHAR2,
      PN_P_MOTIVO_INACTIVACION   IN                         NUMBER,
      PV_FEC_INACTIVACION        IN                         VARCHAR2,
      PN_P_CURSO                 IN                         NUMBER,
      PN_P_TIPO_TUMOR            IN                         NUMBER,
      PN_P_RESP_ALCANZADA        IN                         NUMBER,
      PN_P_LUGAR_PROGRESION      IN                         NUMBER,
      PV_CMP                     IN                         VARCHAR2,
      PV_MEDICO_TRATANTE         IN                         VARCHAR2,
      PN_COD_RESULTADO           OUT                        NUMBER,
      PV_MSG_RESULTADO           OUT                        VARCHAR2
   ) AS

      V_COD_HIST_LINEA_TRAT_SEQ   NUMBER;
      V_COD_HIST_LINEA_TRAT_MAX   NUMBER;
      V_LINEA_TRATAMIENTO         NUMBER := 0;
      V_INICIO                    NUMBER := 0;
      V_FEC_FIN                   NUMBER;
      V_FEC_FIN_REPETIDO EXCEPTION;
      CURSOR C_HIST_LINEA_TRAT IS
      SELECT
         HT.COD_HIST_LINEA_TRAT,
         HT.LINEA_TRAT,
         HT.FEC_FIN
      FROM
         PFC_HIST_LINEA_TRATAMIENTO HT
      WHERE
         HT.COD_AFILIADO = PV_COD_AFI_PACIENTE
      ORDER BY
         TO_DATE(
            HT.FEC_FIN,
            'dd/mm/yyyy'
         ) ASC;

   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      SELECT
         COUNT(1)
      INTO V_FEC_FIN
      FROM
         PFC_HIST_LINEA_TRATAMIENTO HLT
      WHERE
         HLT.COD_AFILIADO = PV_COD_AFI_PACIENTE
         AND HLT.FEC_FIN = TO_DATE(
            PV_FEC_FIN,
            'DD/MM/YYYY'
         );

      IF V_FEC_FIN > 0 THEN
         RAISE V_FEC_FIN_REPETIDO;
      END IF;
      SELECT
         SQ_PFC_HIS_LINE_TRAT_COD_HIS.NEXTVAL
      INTO V_COD_HIST_LINEA_TRAT_SEQ
      FROM
         DUAL;

      INSERT INTO PFC_HIST_LINEA_TRATAMIENTO (
         COD_HIST_LINEA_TRAT,
         COD_SOL_EVA,
         COD_MAC,
         FEC_APROBACION,
         FEC_INICIO,
         FEC_FIN,
         P_CURSO,
         P_TIPO_TUMOR,
         P_RESP_ALCANZADA,
         P_MOTIVO_INACTIVACION,
         FEC_INACTIVACION,
         CMP,
         MEDICO_TRATANTE,
         P_LUGAR_PROGRESION,
         P_ESTADO,
         COD_AFILIADO
      ) VALUES (
         V_COD_HIST_LINEA_TRAT_SEQ,
         NULL,
         PN_COD_MAC_SOL,
         TO_DATE(
            PV_FEC_INICIO,
            'DD/MM/YYYY'
         ),
         TO_DATE(
            PV_FEC_INICIO,
            'DD/MM/YYYY'
         ),
         TO_DATE(
            PV_FEC_FIN,
            'DD/MM/YYYY'
         ),
         PN_P_CURSO,
         PN_P_TIPO_TUMOR,
         PN_P_RESP_ALCANZADA,
         PN_P_MOTIVO_INACTIVACION,
         TO_DATE(
            PV_FEC_INACTIVACION,
            'DD/MM/YYYY'
         ),
         PV_CMP,
         PV_MEDICO_TRATANTE,
         PN_P_LUGAR_PROGRESION,
         46,
         PV_COD_AFI_PACIENTE
      );

      SELECT
         MAX(HLT.COD_HIST_LINEA_TRAT)
      INTO V_COD_HIST_LINEA_TRAT_MAX
      FROM
         ONTPFC.PFC_HIST_LINEA_TRATAMIENTO HLT;

      FOR V_ITEM IN C_HIST_LINEA_TRAT LOOP
         IF V_ITEM.LINEA_TRAT IS NOT NULL AND V_INICIO = 0 THEN
            V_LINEA_TRATAMIENTO := V_ITEM.LINEA_TRAT;
         END IF;

         IF ( V_ITEM.COD_HIST_LINEA_TRAT = V_COD_HIST_LINEA_TRAT_MAX ) OR V_INICIO = 1 THEN
            V_INICIO              := 1;
            V_LINEA_TRATAMIENTO   := V_LINEA_TRATAMIENTO + 1;
            UPDATE PFC_HIST_LINEA_TRATAMIENTO HLT
            SET
               HLT.LINEA_TRAT = V_LINEA_TRATAMIENTO
            WHERE
               HLT.COD_HIST_LINEA_TRAT = V_ITEM.COD_HIST_LINEA_TRAT;

         END IF;

      END LOOP;

      COMMIT;
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_INSERTAR_EXITO;
   EXCEPTION
      WHEN V_FEC_FIN_REPETIDO THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'LA FECHA FIN DEL PERIODO '
                             || PV_FEC_FIN
                             || ' YA SE ENCUENTRA REGISTRADA.';
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_SI_HIST_LINEA_TRATAMIEN]---------- '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SI_HIST_LINEA_TRATAMIEN;
  
  
  /* AUTHOR  : AFLORES*/
  /* CREATED : 14/01/2019*/
  /* MODIFY  : <ddiazr> 07/05/2019*/
  /* PURPOSE : Lista un solicitud de evaluacion en la Bandeja*/

   PROCEDURE SP_PFC_S_BANDEJA_EVALUACION (
      PV_COD_DESC_SOL_EVA          IN                           VARCHAR2,
      PV_COD_CLINICA               IN                           VARCHAR2,
      PV_COD_PACIENTE              IN                           VARCHAR2,
      PV_FECHA_INICIO              IN                           VARCHAR2,
      PV_FECHA_FIN                 IN                           VARCHAR2,
      PN_NRO_SCG_SOLBEN            IN                           VARCHAR2,
      PV_ESTADO_SOL_EVALUACION     IN                           VARCHAR2,
      PV_TIP_SCG_SOLBEN            IN                           VARCHAR2,
      PN_ROL_RESPONSABLE           IN                           NUMBER,
      PV_ESTADO_SCG_SOLBEN         IN                           VARCHAR2,
      PV_CORREO_LIDER_TUMOR        IN                           VARCHAR2,
      PN_NRO_CARTA_GARANTIA        IN                           VARCHAR2,
      PV_CORREO_CMAC               IN                           VARCHAR2,
      PN_AUTORIZADOR_PERTENENCIA   IN                           NUMBER,
      PV_TIPO_EVALUACION           IN                           VARCHAR2,
      PN_LIDER_TUMOR               IN                           NUMBER,
      PV_FEC_REUNION_CMAC          IN                           VARCHAR2,
      AC_LISTADETALLE              OUT                          PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO             OUT                          NUMBER,
      PV_MSG_RESULTADO             OUT                          VARCHAR2
   ) AS
      L_SQL_STMT VARCHAR2(4000);
   BEGIN
      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN AC_LISTADETALLE FOR SELECT
                                  EV.COD_SOL_EVA,
                                  EV.COD_DESC_SOL_EVA,
                                  SO.COD_SCG_SOLBEN,
                                  TIPOSCG.NOMBRE                AS TIPO_SCG_SOLBEN,
                                  SO.ESTADO_SCG,
                                  (
                                     SELECT
                                        P.NOMBRE
                                     FROM
                                        PFC_PARAMETRO P
                                     WHERE
                                        P.COD_GRUPO = 5
                                        AND P.CODIGO = SO.ESTADO_SCG
                                  ) AS ESTADOSGCSOLBEN,
                                  SO.NRO_CG,
                                  EV.FEC_SOL_EVA                AS FECHAEVALUACION,
                                  TIPOEVA.NOMBRE                AS TIPOEVALUACION,
                                  ESTAEVA.NOMBRE                AS ESTADOEVALUACION,
                                  CORLTUM.NOMBRE                AS CORREOLIDERTUMOR,
                                  CORRMAC.NOMBRE                AS CORREOCMAC,
                                  PC.FEC_REUNION                AS FECHACMAC,
                                  SP.COD_MAC,
                                  SO.COD_AFI_PACIENTE,
                                  SO.COD_CLINICA,
                                  SO.COD_DIAGNOSTICO,
                                  MAC.DESCRIPCION,
                                  ONTPFC.FN_PFC_S_VALIDACION(
                                     SO.CMP_MEDICO,
                                     SO.COD_GRP_DIAG
                                  ) AS CODIGOP,
                                  EV.P_ESTADO_SOL_EVA,
                                  EV.ESTADO_CORREO_ENV_CMAC,
                                  EV.ESTADO_CORREO_ENV_LIDER_TUMOR,
                                  EV.CODIGO_ENVIO_ENV_MAC,
                                  EV.CODIGO_ENVIO_ENV_LIDER_TUMOR,
                                  EV.CODIGO_ENVIO_ENV_ALER_MONIT,
                                  EV.P_ROL_RESP_PENDIENTE_EVA              AS ROL_RESPONSABLE,
                                  EV.COD_AUTO_PERTE                        AS AUTORIZADOR_PERTENENCIA,
                                  EV.COD_LIDER_TUMOR                       AS COD_LIDER_TUMOR,
                                  (CASE WHEN PR.NOMBRES IS NULL THEN '' ELSE PR.APELLIDOS || ', ' || PR.NOMBRES END) AS LIDER_TUMOR,
                                  --PR.APELLIDOS || ', ' || PR.NOMBRES       AS LIDER_TUMOR,
                                  EV.P_TIPO_EVA                            AS P_TIPO_EVA
                               FROM
                                  PFC_SOLICITUD_EVALUACION           EV
                                  INNER JOIN PFC_SOLICITUD_PRELIMINAR           SP ON EV.COD_SOL_PRE = SP.COD_SOL_PRE
                                  INNER JOIN PFC_SOLBEN                         SO ON SP.COD_SCG = SO.COD_SCG
                                  LEFT JOIN PFC_PARAMETRO                      TIPOSCG ON SO.TIPO_SCG_SOLBEN = TIPOSCG.COD_PARAMETRO
                                  LEFT JOIN PFC_PARAMETRO                      TIPOSCG ON SO.TIPO_SCG_SOLBEN = TIPOSCG.COD_PARAMETRO
                                  LEFT JOIN PFC_PARAMETRO                      ESTASCG ON ESTASCG.COD_PARAMETRO = SO.ESTADO_SCG
                                  LEFT JOIN PFC_PARAMETRO                      TIPOEVA ON TIPOEVA.COD_PARAMETRO = EV.P_TIPO_EVA
                                  LEFT JOIN PFC_PARAMETRO                      ESTAEVA ON ESTAEVA.COD_PARAMETRO = EV.P_ESTADO_SOL_EVA
                                  LEFT JOIN PFC_PARAMETRO                      CORLTUM ON CORLTUM.COD_PARAMETRO = EV.ESTADO_CORREO_ENV_LIDER_TUMOR
                                  LEFT JOIN PFC_PARAMETRO                      CORRMAC ON CORRMAC.COD_PARAMETRO = EV.ESTADO_CORREO_ENV_CMAC
                                  LEFT JOIN ONTPFC.PFC_PROGRAMACION_CMAC_DET   PCD ON EV.COD_SOL_EVA = PCD.COD_SOL_EVA
                                  LEFT JOIN ONTPFC.PFC_PROGRAMACION_CMAC       PC ON PC.COD_PROGRAMACION_CMAC = PCD.COD_PROGRAMACION_CMAC
                                  LEFT JOIN ONTPFC.PFC_PARTICIPANTE            PR ON EV.COD_LIDER_TUMOR = PR.COD_PARTICIPANTE
                                  INNER JOIN ONTPFC.PFC_MAC                     MAC ON SP.COD_MAC = MAC.COD_MAC
                               WHERE
                                  ( PV_COD_DESC_SOL_EVA IS NULL
                                    OR ( PV_COD_DESC_SOL_EVA IS NOT NULL
                                         AND EV.COD_DESC_SOL_EVA               = PV_COD_DESC_SOL_EVA ) )
                                  AND ( PV_COD_CLINICA IS NULL
                                        OR ( PV_COD_CLINICA IS NOT NULL
                                             AND SO.COD_CLINICA                    = PV_COD_CLINICA ) )
                                  AND ( PV_COD_PACIENTE IS NULL
                                        OR ( PV_COD_PACIENTE IS NOT NULL
                                             AND SO.COD_AFI_PACIENTE               = PV_COD_PACIENTE ) )
                                  AND ( PV_FECHA_INICIO IS NULL
                                        OR ( PV_FECHA_INICIO IS NOT NULL
                                             AND EV.FEC_SOL_EVA >= TO_DATE(
                                     PV_FECHA_INICIO,
                                     'DD/MM/YYYY'
                                  ) ) )
                                  AND ( PV_FECHA_FIN IS NULL
                                        OR ( PV_FECHA_FIN IS NOT NULL
                                             AND EV.FEC_SOL_EVA <= TO_DATE(
                                     PV_FECHA_FIN,
                                     'DD/MM/YYYY'
                                  ) + 1 ) )
                                  AND ( PN_NRO_SCG_SOLBEN IS NULL
                                        OR ( PN_NRO_SCG_SOLBEN IS NOT NULL
                                             AND SO.COD_SCG_SOLBEN                 = PN_NRO_SCG_SOLBEN ) )
                                  AND ( PV_ESTADO_SOL_EVALUACION IS NULL
                                        OR ( PV_ESTADO_SOL_EVALUACION IS NOT NULL
                                             AND EV.P_ESTADO_SOL_EVA               = PV_ESTADO_SOL_EVALUACION ) )
                                  AND ( PV_TIP_SCG_SOLBEN IS NULL
                                        OR ( PV_TIP_SCG_SOLBEN IS NOT NULL
                                             AND SO.TIPO_SCG_SOLBEN                = PV_TIP_SCG_SOLBEN ) )
                                  AND ( PV_ESTADO_SCG_SOLBEN IS NULL
                                        OR ( PV_ESTADO_SCG_SOLBEN IS NOT NULL
                                             AND SO.ESTADO_SCG                     = PV_ESTADO_SCG_SOLBEN ) )
                                  AND ( PV_CORREO_LIDER_TUMOR IS NULL
                                        OR ( PV_CORREO_LIDER_TUMOR IS NOT NULL
                                             AND EV.ESTADO_CORREO_ENV_LIDER_TUMOR  = PV_CORREO_LIDER_TUMOR ) )
                                  AND ( PN_NRO_CARTA_GARANTIA IS NULL
                                        OR ( PN_NRO_CARTA_GARANTIA IS NOT NULL
                                             AND SO.NRO_CG                         = PN_NRO_CARTA_GARANTIA ) )
                                  AND ( PV_CORREO_CMAC IS NULL
                                        OR ( PV_CORREO_CMAC IS NOT NULL
                                             AND EV.ESTADO_CORREO_ENV_CMAC         = PV_CORREO_CMAC ) )
                                  AND ( PV_TIPO_EVALUACION IS NULL
                                        OR ( PV_TIPO_EVALUACION IS NOT NULL
                                             AND EV.P_TIPO_EVA                     = PV_TIPO_EVALUACION ) )
                                  AND ( PV_FEC_REUNION_CMAC IS NULL
                                        OR ( PV_FEC_REUNION_CMAC IS NOT NULL
                                             AND PC.FEC_REUNION                    = PV_FEC_REUNION_CMAC ) )
                                  AND ( PN_AUTORIZADOR_PERTENENCIA IS NULL
                                        OR ( PN_AUTORIZADOR_PERTENENCIA IS NOT NULL
                                             AND EV.COD_AUTO_PERTE                 = PN_AUTORIZADOR_PERTENENCIA ) )
                                  AND ( PN_LIDER_TUMOR IS NULL
                                        OR ( PN_LIDER_TUMOR IS NOT NULL
                                             AND PR.COD_PARTICIPANTE                = PN_LIDER_TUMOR ) )
                                  AND ( PN_ROL_RESPONSABLE IS NULL
                                        OR ( PN_ROL_RESPONSABLE IS NOT NULL
                                             AND EV.P_ROL_RESP_PENDIENTE_EVA       = PN_ROL_RESPONSABLE ) )
                               ORDER BY
                                  EV.FEC_SOL_EVA ASC;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_BANDEJA_EVALUACION]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_BANDEJA_EVALUACION;

  /* AUTHOR  : AFLORES*/
  /* CREATED : 28/01/2019*/
  /* PURPOSE : Lista Inclucion e Exclucion para el Paso 4 de Evaluacion CMAC*/

   PROCEDURE SP_PFC_S_LISTA_CRITERIO (
      PN_COD_INDI        IN                 NUMBER,
      AC_LISTA_INC       OUT                PCK_PFC_CONSTANTE.TYPCUR,
      AC_LISTA_EXC       OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN AC_LISTA_INC FOR SELECT
                               INC.COD_CRITERIO_INCLU,
                               INC.ORDEN,
                               INC.DESCRIPCION
                            FROM
                               ONTPFC.PFC_CRITERIO_INCLUSION INC
                            WHERE
                               INC.COD_CHKLIST_INDI = PN_COD_INDI;

      OPEN AC_LISTA_EXC FOR SELECT
                              EXC.COD_CRITERIO_EXCLU,
                              EXC.ORDEN,
                              EXC.DESCRIPCION
                           FROM
                              ONTPFC.PFC_CRITERIO_EXCLUSION EXC
                           WHERE
                              EXC.COD_CHKLIST_INDI = PN_COD_INDI;

      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'Consulta Exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_LISTA_CRITERIO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_LISTA_CRITERIO;

  /* AUTHOR  : AFLORES*/
  /* CREATED : 29/01/2019*/
  /* PURPOSE : Lista las indicaciones*/

   PROCEDURE SP_PFC_S_LISTA_INDICADORES (
      PN_COD_EVA              IN                      NUMBER,
      PN_COD_MAC              IN                      NUMBER,
      PN_COD_GRP_DIAG         IN                      NUMBER,
      AC_LISTA_IND            OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      AC_LISTA_INC            OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      AC_LISTA_EXC            OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_CHKLIST_INDI     OUT                     NUMBER,
      AC_LISTA_INC_PRE        OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      AC_LISTA_EXC_PRE        OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_CUMPLE_CHKLIST_PER   OUT                     NUMBER,
      PV_COMENTARIO           OUT                     VARCHAR2,
      PV_GRABAR               OUT                     VARCHAR2,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   ) AS
      V_COD_SEV_CHKLIST_PAC NUMBER;
      V_EXC_COD_EVA EXCEPTION;
      V_EXC_COD_MAC_GRP_DIAG EXCEPTION;
   BEGIN
      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'valor inicial';
      BEGIN
         SELECT
            SCP.COD_SEV_CHKLIST_PAC,
            SCP.GRABAR
         INTO
            V_COD_SEV_CHKLIST_PAC,
            PV_GRABAR
         FROM
            PFC_SEV_CHECKLIST_PAC   SCP
            INNER JOIN PFC_MEDICAMENTO_NUEVO   MNU ON SCP.COD_SEV_CHKLIST_PAC = MNU.COD_SEV_CHK_PAC
         WHERE
            MNU.COD_SOL_EVA = PN_COD_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_COD_SEV_CHKLIST_PAC   := NULL;
            PV_GRABAR               := NULL;
      END;

      IF PN_COD_EVA IS NOT NULL THEN
         IF PN_COD_MAC IS NOT NULL AND PN_COD_GRP_DIAG IS NOT NULL THEN
            DBMS_OUTPUT.PUT_LINE('  -- MAC Y COD GRP DIAG RECONOCIDA --  ');
            OPEN AC_LISTA_IND FOR SELECT
                                     IND.COD_CHKLIST_INDI,
                                     IND.DESCRIPCION
                                  FROM
                                     PFC_CHKLIST_INDICACION IND
                                  WHERE
                                     IND.COD_MAC = PN_COD_MAC
                                     AND IND.P_ESTADO      = PCK_PFC_CONSTANTE.PN_ESTADO_VIGENTE_INDICACION
                                     AND IND.COD_GRP_DIAG  = PN_COD_GRP_DIAG;

            OPEN AC_LISTA_INC FOR SELECT
                                    INC.COD_CRITERIO_INCLU,
                                    IND.COD_CHKLIST_INDI,
                                    INC.DESCRIPCION
                                 FROM
                                    ONTPFC.PFC_CRITERIO_INCLUSION   INC,
                                    ONTPFC.PFC_CHKLIST_INDICACION   IND
                                 WHERE
                                    INC.COD_CHKLIST_INDI = IND.COD_CHKLIST_INDI
                                    AND IND.COD_MAC       = PN_COD_MAC
                                    AND IND.COD_GRP_DIAG  = PN_COD_GRP_DIAG
                                    AND INC.P_ESTADO      = PCK_PFC_CONSTANTE.PN_ESTADO_VIGENTE_INCLUSION
                                 ORDER BY
                                    IND.COD_CHKLIST_INDI,
                                    INC.COD_CRITERIO_INCLU;

            OPEN AC_LISTA_EXC FOR SELECT
                                    EXC.COD_CRITERIO_EXCLU,
                                    IND.COD_CHKLIST_INDI,
                                    EXC.DESCRIPCION
                                 FROM
                                    ONTPFC.PFC_CRITERIO_EXCLUSION   EXC,
                                    ONTPFC.PFC_CHKLIST_INDICACION   IND
                                 WHERE
                                    EXC.COD_CHKLIST_INDI = IND.COD_CHKLIST_INDI
                                    AND IND.COD_MAC       = PN_COD_MAC
                                    AND IND.COD_GRP_DIAG  = PN_COD_GRP_DIAG
                                    AND EXC.P_ESTADO      = PCK_PFC_CONSTANTE.PN_ESTADO_VIGENTE_EXCLUSION
                                 ORDER BY
                                    IND.COD_CHKLIST_INDI,
                                    EXC.COD_CRITERIO_EXCLU;

         ELSE
            RAISE V_EXC_COD_MAC_GRP_DIAG;
         END IF;
      ELSE
         RAISE V_EXC_COD_EVA;
      END IF;

      IF V_COD_SEV_CHKLIST_PAC IS NOT NULL AND PV_GRABAR = '1' THEN
         DBMS_OUTPUT.PUT_LINE('  -- REGISTRO YA FUE GRABADO --  ');
         SELECT
            CLI.COD_CHKLIST_INDI,
            CLP.P_CUMPLE_CHKLIST_PER,
            CLP.COMENTARIO
         INTO
            PN_COD_CHKLIST_INDI,
            PN_CUMPLE_CHKLIST_PER,
            PV_COMENTARIO
         FROM
            PFC_SEV_CHECKLIST_PAC    CLP
            INNER JOIN PFC_CHKLIST_INDICACION   CLI ON CLI.COD_CHKLIST_INDI = CLP.COD_CHKLIST_INDI
         WHERE
            CLP.COD_SEV_CHKLIST_PAC = V_COD_SEV_CHKLIST_PAC
            AND CLI.COD_MAC       = PN_COD_MAC
            AND CLI.COD_GRP_DIAG  = PN_COD_GRP_DIAG;

         OPEN AC_LISTA_INC_PRE FOR SELECT
                                     CPDI.COD_CRITERIO_INCLU,
                                     CPDI.P_CRITERIO_INCLU
                                  FROM
                                     PFC_SEV_CHECKLIST_PAC_DET_INC CPDI
                                  WHERE
                                     CPDI.COD_SEV_CHKLIST_PAC = V_COD_SEV_CHKLIST_PAC;

         OPEN AC_LISTA_EXC_PRE FOR SELECT
                                     CPDE.COD_CRITERIO_EXCLU,
                                     CPDE.P_CRITERIO_EXCLU
                                  FROM
                                     PFC_SEV_CHECKLIST_PAC_DET_EXC CPDE
                                  WHERE
                                     CPDE.COD_SEV_CHKLIST_PAC = V_COD_SEV_CHKLIST_PAC;

      END IF;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
   EXCEPTION
      WHEN V_EXC_COD_EVA THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'No cuenta con el codigo de Evaluación';
      WHEN V_EXC_COD_MAC_GRP_DIAG THEN
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := 'No cuenta con el codigo Mac y Codigo de grupo de diagnostico';
      WHEN NO_DATA_FOUND THEN
         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'No se encontraron criterios de inclusion y exclusion guardados';
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_LISTA_INDICADORES]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_LISTA_INDICADORES;


  /* AUTOR     : AFLORES*/
  /* CREADO    : 31/01/2019*/
  /* PROPOSITO : GUARDAR LOS CRITERIOS DE INCLUSION Y EXCLUSION  de CheckList PASO 4*/

   PROCEDURE SP_PFC_SI_CHKLIST_PAC (
      PN_COD_EVA              IN                      NUMBER,
      PN_COD_INDI             IN                      NUMBER,
      PN_COD_MAC              IN                      NUMBER,
      PN_COD_GRP_DIAG         IN                      NUMBER,
      PV_STR_INCLU            IN                      VARCHAR2,
      PV_STR_EXC              IN                      VARCHAR2,
      PN_CUMPLE_CHKLIST_PER   IN                      NUMBER,
      PV_COMENTARIO           IN                      VARCHAR2,
      PN_COD_ROL_USUARIO      IN                      NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO         IN                      VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO          IN                      NUMBER,/* CODIGO DE USUARIO*/
      PN_RESULTADO_S          OUT                     NUMBER,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   ) AS

      V_CHK_LIST            NUMBER(7);
      V_COD_EX_MED_SEQ      NUMBER(10);
      V_COD_CHKLIST         NUMBER(10);
      V_COD_INC             NUMBER(10);
      V_COD_EXC             NUMBER(10);
      V_CHK_L               NUMBER(10);
      V_EST_EVA             NUMBER(7);
      V_GRABAR              CHAR(1);
      V_EXC_ESTADO_SOL_EVA EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO_SEG   NUMBER; /* SEGUIMIENTO*/
      V_MSG_RESULTADO_SEG   VARCHAR2(4000); /* SEGUIMIENTO*/
      V_ESTADO_SOL_EVA1     NUMBER; /* SEGUIMIENTO*/
      CURSOR V_LIST_INC IS
      SELECT
         REGEXP_SUBSTR(
            LISTA,
            '([^,]+)',
            1,
            1,
            '',
            1
         ) AS LISTA_1,
         REGEXP_SUBSTR(
            LISTA,
            '([^,]+)',
            1,
            2,
            '',
            1
         ) AS LISTA_2
      FROM
         (
            SELECT
               REGEXP_SUBSTR(
                  PV_STR_INCLU,
                  '[^|]+',
                  1,
                  LEVEL
               ) AS LISTA
            FROM
               DUAL
            CONNECT BY
               REGEXP_SUBSTR(
                  PV_STR_INCLU,
                  '[^|]+',
                  1,
                  LEVEL
               ) IS NOT NULL
         );

      CURSOR V_LIST_EXC IS
      SELECT
         REGEXP_SUBSTR(
            LISTA,
            '([^,]+)',
            1,
            1,
            '',
            1
         ) AS LISTA_1,
         REGEXP_SUBSTR(
            LISTA,
            '([^,]+)',
            1,
            2,
            '',
            1
         ) AS LISTA_2
      FROM
         (
            SELECT
               REGEXP_SUBSTR(
                  PV_STR_EXC,
                  '[^|]+',
                  1,
                  LEVEL
               ) AS LISTA
            FROM
               DUAL
            CONNECT BY
               REGEXP_SUBSTR(
                  PV_STR_EXC,
                  '[^|]+',
                  1,
                  LEVEL
               ) IS NOT NULL
         );

   BEGIN
      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'valor inicial';
      BEGIN
         SELECT
            MED.COD_SEV_CHK_PAC,
            EV.P_ESTADO_SOL_EVA,
            PC.GRABAR
         INTO
            V_CHK_LIST,
            V_EST_EVA,
            V_GRABAR
         FROM
            PFC_MEDICAMENTO_NUEVO      MED,
            PFC_SOLICITUD_EVALUACION   EV,
            PFC_SEV_CHECKLIST_PAC      PC
         WHERE
            MED.COD_SOL_EVA = EV.COD_SOL_EVA
            AND MED.COD_SEV_CHK_PAC  = PC.COD_SEV_CHKLIST_PAC
            AND EV.COD_SOL_EVA       = PN_COD_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_CHK_LIST := NULL;
      END;

      IF V_CHK_LIST IS NULL THEN
         SELECT
            SQ_PFC_SEV_CHK_PER_PAC_COD_CHK.NEXTVAL
         INTO V_COD_EX_MED_SEQ
         FROM
            DUAL;

         INSERT INTO ONTPFC.PFC_SEV_CHECKLIST_PAC (
            COD_SEV_CHKLIST_PAC,
            COD_CHKLIST_INDI,
            COD_MAC,
            COD_GRP_DIAG,
            COMENTARIO,
            P_CUMPLE_CHKLIST_PER,
            GRABAR
         ) VALUES (
            V_COD_EX_MED_SEQ,
            PN_COD_INDI,
            PN_COD_MAC,
            PN_COD_GRP_DIAG,
            'null',
            '91',
            0
         );

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Insertó Correctamente';
         SELECT
            COD_SEV_CHKLIST_PAC
         INTO V_COD_CHKLIST
         FROM
            (
               SELECT
                  COD_SEV_CHKLIST_PAC
               FROM
                  ONTPFC.PFC_SEV_CHECKLIST_PAC
               WHERE
                  COD_SEV_CHKLIST_PAC = V_COD_EX_MED_SEQ
               ORDER BY
                  COD_SEV_CHKLIST_PAC DESC
            )
         WHERE
            ROWNUM = 1;

         IF V_COD_CHKLIST IS NOT NULL THEN
            UPDATE ONTPFC.PFC_MEDICAMENTO_NUEVO MED
            SET
               MED.COD_SEV_CHK_PAC = V_COD_CHKLIST
            WHERE
               MED.COD_SOL_EVA = PN_COD_EVA;

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'Se actualizó con exito';
         ELSE
            PN_COD_RESULTADO   := 1;
            PV_MSG_RESULTADO   := 'No se encuentra el codigo de checklist lo cual no se hizo la insercion';
         END IF;

         FOR V_ITEM IN V_LIST_INC LOOP
            SELECT
               COUNT(*)
            INTO V_COD_INC
            FROM
               PFC_SEV_CHECKLIST_PAC_DET_INC
            WHERE
               COD_CRITERIO_INCLU = V_ITEM.LISTA_1
               AND COD_SEV_CHKLIST_PAC = V_CHK_LIST;

            IF V_COD_INC = 0 THEN
               INSERT INTO ONTPFC.PFC_SEV_CHECKLIST_PAC_DET_INC (
                  COD_SEV_CHKLIST_PAC,
                  COD_CRITERIO_INCLU,
                  P_CRITERIO_INCLU
               ) VALUES (
                  V_COD_CHKLIST,
                  V_ITEM.LISTA_1,
                  V_ITEM.LISTA_2
               );

               PN_COD_RESULTADO   := 0;
               PV_MSG_RESULTADO   := 'Se realizó la insercion con éxito';
            ELSE
               PN_COD_RESULTADO   := 2;
               PV_MSG_RESULTADO   := 'Ya cuenta con codigo de inclusion';
            END IF;

         END LOOP;

         FOR V_ITEM_2 IN V_LIST_EXC LOOP
            SELECT
               COUNT(*)
            INTO V_COD_EXC
            FROM
               PFC_SEV_CHECKLIST_PAC_DET_EXC
            WHERE
               COD_CRITERIO_EXCLU = V_ITEM_2.LISTA_1
               AND COD_SEV_CHKLIST_PAC = V_CHK_LIST;

            IF V_COD_EXC = 0 THEN
               INSERT INTO ONTPFC.PFC_SEV_CHECKLIST_PAC_DET_EXC (
                  COD_SEV_CHKLIST_PAC,
                  COD_CRITERIO_EXCLU,
                  P_CRITERIO_EXCLU
               ) VALUES (
                  V_COD_CHKLIST,
                  V_ITEM_2.LISTA_1,
                  V_ITEM_2.LISTA_2
               );

               PN_COD_RESULTADO   := 0;
               PV_MSG_RESULTADO   := 'Se realizó la insercion con éxito';
            ELSE
               PN_COD_RESULTADO   := 3;
               PV_MSG_RESULTADO   := 'Ya cuenta con codigo de exclusion';
            END IF;

         END LOOP;

         UPDATE ONTPFC.PFC_SEV_CHECKLIST_PAC SVCK
         SET
            SVCK.COMENTARIO = PV_COMENTARIO,
            SVCK.P_CUMPLE_CHKLIST_PER = PN_CUMPLE_CHKLIST_PER,
            SVCK.GRABAR = 1
         WHERE
            SVCK.COD_SEV_CHKLIST_PAC = V_COD_CHKLIST;

         PN_RESULTADO_S     := PN_CUMPLE_CHKLIST_PER;
         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Se actualizó con exito la tabla PFC_SEV_CHECKLIST_PAC';
      ELSE
         BEGIN
            SELECT
               COD_SEV_CHKLIST_PAC
            INTO V_CHK_L
            FROM
               ONTPFC.PFC_SEV_CHECKLIST_PAC
            WHERE
               COD_SEV_CHKLIST_PAC = V_CHK_LIST;

         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               V_CHK_L := NULL;
         END;

         IF V_CHK_L IS NOT NULL THEN
            DELETE FROM ONTPFC.PFC_SEV_CHECKLIST_PAC_DET_INC
            WHERE
               COD_SEV_CHKLIST_PAC = V_CHK_L;

            DELETE FROM ONTPFC.PFC_SEV_CHECKLIST_PAC_DET_EXC
            WHERE
               COD_SEV_CHKLIST_PAC = V_CHK_L;

            FOR V_ITEM IN V_LIST_INC LOOP
               SELECT
                  COUNT(*)
               INTO V_COD_INC
               FROM
                  ONTPFC.PFC_SEV_CHECKLIST_PAC_DET_INC
               WHERE
                  COD_CRITERIO_INCLU = V_ITEM.LISTA_1
                  AND COD_SEV_CHKLIST_PAC = V_CHK_L;

               IF V_COD_INC = 0 THEN
                  INSERT INTO ONTPFC.PFC_SEV_CHECKLIST_PAC_DET_INC (
                     COD_SEV_CHKLIST_PAC,
                     COD_CRITERIO_INCLU,
                     P_CRITERIO_INCLU
                  ) VALUES (
                     V_CHK_L,
                     V_ITEM.LISTA_1,
                     V_ITEM.LISTA_2
                  );

                  PN_COD_RESULTADO   := 0;
                  PV_MSG_RESULTADO   := 'Se realizó la insercion con éxito';
               ELSE
                  PN_COD_RESULTADO   := 4;
                  PV_MSG_RESULTADO   := 'Ya cuenta con codigo de inclusion';
               END IF;

            END LOOP;

            FOR V_ITEM_2 IN V_LIST_EXC LOOP
               SELECT
                  COUNT(*)
               INTO V_COD_EXC
               FROM
                  PFC_SEV_CHECKLIST_PAC_DET_EXC
               WHERE
                  COD_CRITERIO_EXCLU = V_ITEM_2.LISTA_1
                  AND COD_SEV_CHKLIST_PAC = V_CHK_L;

               IF V_COD_EXC = 0 THEN
                  INSERT INTO ONTPFC.PFC_SEV_CHECKLIST_PAC_DET_EXC (
                     COD_SEV_CHKLIST_PAC,
                     COD_CRITERIO_EXCLU,
                     P_CRITERIO_EXCLU
                  ) VALUES (
                     V_CHK_L,
                     V_ITEM_2.LISTA_1,
                     V_ITEM_2.LISTA_2
                  );

                  PN_COD_RESULTADO   := 0;
                  PV_MSG_RESULTADO   := 'Se realizó la insercion con éxito';
               ELSE
                  PN_COD_RESULTADO   := 5;
                  PV_MSG_RESULTADO   := 'Ya cuenta con codigo de exclusion';
               END IF;

            END LOOP;

            UPDATE ONTPFC.PFC_SEV_CHECKLIST_PAC SVCK
            SET
               SVCK.COD_CHKLIST_INDI = PN_COD_INDI,
               SVCK.COD_MAC = PN_COD_MAC,
               SVCK.COD_GRP_DIAG = PN_COD_GRP_DIAG,
               SVCK.COMENTARIO = PV_COMENTARIO,
               SVCK.P_CUMPLE_CHKLIST_PER = PN_CUMPLE_CHKLIST_PER,
               SVCK.GRABAR = 1
            WHERE
               SVCK.COD_SEV_CHKLIST_PAC = V_CHK_LIST;

            PN_RESULTADO_S     := PN_CUMPLE_CHKLIST_PER;
            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'Se actualizó con exito la tabla PFC_SEV_CHECKLIST_PAC- ELSE';
         ELSE
            PN_RESULTADO_S     := PN_CUMPLE_CHKLIST_PER;
            PN_COD_RESULTADO   := 6;
            PV_MSG_RESULTADO   := 'No se encuentra el codigo de check list';
         END IF;

      END IF;

      IF PN_COD_EVA IS NOT NULL THEN
         UPDATE ONTPFC.PFC_SOLICITUD_EVALUACION EV
         SET
            EV.P_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_STATUS_PEND_PAS_CUATRO
         WHERE
            EV.COD_SOL_EVA = PN_COD_EVA;

         PN_RESULTADO_S     := PN_CUMPLE_CHKLIST_PER;
         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Se actualizó correctamente el estado';
      ELSE
         PN_RESULTADO_S     := PN_CUMPLE_CHKLIST_PER;
         PN_COD_RESULTADO   := 7;
         PV_MSG_RESULTADO   := 'No se puede actualizar el estado de evaluacion';
      END IF;
      
      UPDATE PFC_SOLICITUD_EVALUACION SEV
       SET
          SEV.COD_AUTO_PERTE = PN_COD_USUARIO,
          SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE
       WHERE
          SEV.COD_SOL_EVA = PN_COD_EVA;
    
       /* SEGUIMIENTO BEGIN */

      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA1
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA1 := NULL;
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;

      ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO(
         PN_COD_EVA,/* NUMBER*/
         V_ESTADO_SOL_EVA1,
         PV_FECHA_ESTADO,
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL DE USUARIO*/
         PN_COD_USUARIO,/* CODIGO DE USUARIO*/
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         PN_COD_USUARIO, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         V_COD_RESULTADO_SEG,/* NUMBER*/
         V_MSG_RESULTADO_SEG
      ); /* VARCHAR*/

      IF V_COD_RESULTADO_SEG <> 0 THEN
         RAISE V_COD_RESULTADO; /* EXCEPTION*/
      END IF;            
      /* SEGUIMIENTO END*/
      COMMIT;
   EXCEPTION
      WHEN V_COD_RESULTADO THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR, AL INSERTAR LA TABLA SEGUIMIENTO '
                              || '[[ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO]]'
                              || V_MSG_RESULTADO_SEG;
      WHEN V_EXC_ESTADO_SOL_EVA THEN
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := 'ERROR, NO SE ENCONTRO EL ESTADO DE LA SOLICITUD DE EVALUACION';
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SI_CHKLIST_PAC]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SI_CHKLIST_PAC;

   -- AUTHOR  : JAZABACHE
   -- CREATED : 16/01/2019
   -- PURPOSE : OBTENER HIST DE ULTIMA LINEA TRAT PARA EVA DEL AUTOR

   PROCEDURE SP_PFC_S_EVALUACION_AUTOR (
      PN_COD_AFI_PACIENTE   IN                    VARCHAR2,
      AC_LISTA_EVALUACION   OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS
   BEGIN
    OPEN AC_LISTA_EVALUACION FOR 
         
         SELECT HLT.COD_HIST_LINEA_TRAT, HLT.COD_MAC, HLT.LINEA_TRAT, HLT.P_ESTADO, HLT.FEC_INICIO, HLT.FEC_FIN,
                TO_CHAR(SEV.FEC_SOL_EVA,'DD/MM/YYYY') AS FEC_SOL_EVA, SO.COD_GRP_DIAG
           FROM (SELECT TT.COD_HIST_LINEA_TRAT,TT.COD_MAC,TT.LINEA_TRAT,TT.P_ESTADO,TT.FEC_INICIO,
                        TT.FEC_FIN,TT.FEC_APROBACION,TT.COD_SOL_EVA
                   FROM (SELECT HT.COD_HIST_LINEA_TRAT,
                        HT.COD_MAC,
                        HT.LINEA_TRAT,
                        HT.P_ESTADO,
                        HT.FEC_INICIO,
                        HT.FEC_FIN,
                        HT.FEC_APROBACION,
                        HT.COD_SOL_EVA
                   FROM PFC_HIST_LINEA_TRATAMIENTO   HT
                  WHERE HT.COD_AFILIADO = PN_COD_AFI_PACIENTE
                  ORDER BY HT.FEC_FIN DESC, HT.LINEA_TRAT DESC
                 ) TT WHERE ROWNUM = 1) HLT
          LEFT OUTER JOIN PFC_SOLICITUD_EVALUACION SEV
             ON HLT.COD_SOL_EVA = SEV.COD_SOL_EVA
          LEFT OUTER JOIN PFC_SOLICITUD_PRELIMINAR SPR
             ON SEV.COD_SOL_PRE = SPR.COD_SOL_PRE
          LEFT OUTER JOIN PFC_SOLBEN SO
             ON SO.COD_SCG = SPR.COD_SCG;
        

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_EVALUACION_AUTOR]] '|| TO_CHAR(SQLCODE)|| ' : '|| SQLERRM;
   END SP_PFC_S_EVALUACION_AUTOR;
   
   
  /* AUTOR     : JAZABACHE*/
  /* CREADO    : 19/01/2019*/
  /* PROPOSITO : GUARDAR CAMBIOS MEDICAMENTO NUEVO PASO 1*/

   PROCEDURE SP_PFC_SU_PREF_INST_GUARDAR (
      PN_COD_SOL_EVA        IN                    NUMBER,
      PN_NRO_CURSO          IN                    NUMBER,
      PN_TIPO_TUMOR         IN                    NUMBER,
      PN_LUGAR_PROGRESION   IN                    NUMBER,
      PN_RESP_ALCANZADA     IN                    NUMBER,
      PN_CONDICION          IN                    NUMBER,
      PN_CUMPLE_PREF_INST   IN                    NUMBER,
      PV_OBSERVACION        IN                    VARCHAR2,
      PN_COD_ROL_USUARIO    IN                    NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO       IN                    VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO        IN                    NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS

      V_COD_SEV_LIN_TRA     NUMBER;
      V_ESTADO_SOL_EVA      NUMBER;
      V_MENSAJE             VARCHAR2(200);
      V_EXC_VARIABLE EXCEPTION;
      V_COD_RESULTADO EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO_SEG   NUMBER; /* SEGUIMIENTO*/
      V_MSG_RESULTADO_SEG   VARCHAR2(4000); /* SEGUIMIENTO*/
      V_ESTADO_SOL_EVA1     NUMBER; /* SEGUIMIENTO*/
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF NULLIF(
         PN_COD_SOL_EVA,
         ''
      ) IS NULL THEN
         V_MENSAJE := 'ERROR, NO EXITE CODIGO DE SOLICITUD DE EVALUACION';
         RAISE V_EXC_VARIABLE;
      END IF;

      IF FN_PFC_S_VALIDACION_FECHA_HORA(PV_FECHA_ESTADO) = 1 THEN
         V_MENSAJE := 'ERROR, FORMATO DE FECHA Y HORA INCORRECTO';
         RAISE V_EXC_VARIABLE;
      END IF;

      DBMS_OUTPUT.PUT_LINE(LENGTH(PV_OBSERVACION));
      IF LENGTH(PV_OBSERVACION) > 300 THEN
         V_MENSAJE := 'LA OBSERVACIÓN SUPERÓ EL TAMAÑO MÁXIMO PERMITIDO DE 300 CARACTERES';
         RAISE V_EXC_VARIABLE;
      END IF;

      BEGIN
         SELECT
            MN.COD_SEV_LIN_TRA,
            SE.P_ESTADO_SOL_EVA
         INTO
            V_COD_SEV_LIN_TRA,
            V_ESTADO_SOL_EVA
         FROM
            PFC_SOLICITUD_EVALUACION   SE
            INNER JOIN PFC_MEDICAMENTO_NUEVO      MN ON SE.COD_SOL_EVA = MN.COD_SOL_EVA
         WHERE
            SE.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_COD_SEV_LIN_TRA := NULL;
      END;

      IF V_COD_SEV_LIN_TRA IS NULL THEN
         V_MENSAJE := 'ERROR, NO EXISTE CODIGO DE LINEA DE TRATAMIENTO / PREFERENCIA INSTITUCIONAL';
         RAISE V_EXC_VARIABLE;
      END IF;
      DBMS_OUTPUT.PUT_LINE('  -- ACTUALIZAR --  ');
      UPDATE PFC_SEV_LINEA_TRATAMIENTO SLT
      SET
         SLT.P_NRO_CURSO = PN_NRO_CURSO,
         SLT.P_TIPO_TUMOR = PN_TIPO_TUMOR,
         SLT.P_LUGAR_PROGRESION = PN_LUGAR_PROGRESION,
         SLT.P_RESP_ALCANZADA = PN_RESP_ALCANZADA,
         SLT.P_CUMPLE_PREF_INST = PN_CUMPLE_PREF_INST,
         SLT.P_CONDICION = PN_CONDICION,
         SLT.GRABAR = PCK_PFC_CONSTANTE.PV_GRABAR_PASO,
         SLT.DESCRIPCION = PV_OBSERVACION,
         SLT.USUARIO_MODIFICACION = PN_COD_USUARIO,
         SLT.FECHA_MODIFICACION = TO_DATE(
            PV_FECHA_ESTADO,
            'DD-MM-YYYY HH24:MI:SS'
         )
      WHERE
         SLT.COD_SEV_LIN_TRA = V_COD_SEV_LIN_TRA;

      DBMS_OUTPUT.PUT_LINE('  -- 2 --  ');
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
      DBMS_OUTPUT.PUT_LINE('  -- 3 --  ');
      IF V_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_EVA OR V_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_RES_MON
      THEN
         UPDATE PFC_SOLICITUD_EVALUACION SEV
         SET
            SEV.P_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_PASO1
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

         DBMS_OUTPUT.PUT_LINE('  -- ESTADO PENDIENTE PASO 1 --  ');
      END IF;
      
      UPDATE PFC_SOLICITUD_EVALUACION SEV
      SET
         SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
         SEV.COD_AUTO_PERTE = PN_COD_USUARIO
      WHERE
         SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
    /* SEGUIMIENTO BEGIN */

      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA1
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA1   := NULL;
            V_MENSAJE           := 'ERROR: NO SE ENCONTRO EL ESTADO DE LA SOLICITUD DE EVALUACION';
            RAISE V_EXC_VARIABLE;
      END;

      ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO(
         PN_COD_SOL_EVA,/* NUMBER*/
         V_ESTADO_SOL_EVA1,
         PV_FECHA_ESTADO,
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL DE USUARIO*/
         PN_COD_USUARIO,/* CODIGO DE USUARIO*/
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         PN_COD_USUARIO, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         V_COD_RESULTADO_SEG,/* NUMBER*/
         V_MSG_RESULTADO_SEG
      ); /* VARCHAR*/

      IF V_COD_RESULTADO_SEG <> 0 THEN
         RAISE V_COD_RESULTADO; /* EXCEPTION*/
      END IF;   
      /* SEGUIMIENTO END*/
      COMMIT;
   EXCEPTION
      WHEN V_EXC_VARIABLE THEN
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN V_COD_RESULTADO THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR, AL INSERTAR LA TABLA SEGUIMIENTO '
                              || '[[ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO]]' 
                              || V_MSG_RESULTADO_SEG;
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SU_PREF_INST_GUARDAR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SU_PREF_INST_GUARDAR;


  /* AUTOR     : JAZABACHE*/
  /* CREADO    : 11/03/2019*/
  /* PROPOSITO  : CONSULTAR LA PREFERENCIA INSTITUCIONAL DE LA MAC PASO 1*/

   PROCEDURE SP_PFC_S_PREF_INST_CONSULTA (
      PV_COD_GRP_DIAG        IN                     VARCHAR2,
      PN_CONDICION           IN                     NUMBER,
      TC_PREFERENCIA_INSTI   OUT                    PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO       OUT                    NUMBER,
      PV_MSG_RESULTADO       OUT                    VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN TC_PREFERENCIA_INSTI FOR SELECT
                                       PI.P_CONDICION_CANCER,
                                       PI.DETALLE_CONDICION,
                                       PI.PRESENTAC_NO_PERMITIDA,
                                       PI.P_TIPO_TRATAMIENTO,
                                       (
                                          SELECT
                                             P.NOMBRE
                                          FROM
                                             PFC_PARAMETRO P
                                          WHERE
                                             P.COD_PARAMETRO = PI.P_TIPO_TRATAMIENTO
                                       ) AS DESCRIP_TIPO_TRATA,
                                       PI.COD_MAC AS COD_TRATAMIENTO,
                                       (
                                          SELECT
                                             M.DESCRIPCION
                                          FROM
                                             PFC_MAC M
                                          WHERE
                                             M.COD_MAC = PI.COD_MAC
                                       ) AS DESCRIP_TRATAMIENTO
                                    FROM
                                       PFC_PREFERENCIA_INST PI
                                    WHERE
                                       PI.COD_GRP_DIAG = PV_COD_GRP_DIAG/*'010'*/
                                       AND PI.P_CONDICION_CANCER  = PN_CONDICION/*237*/
                                       AND PI.P_ESTADO            = PCK_PFC_CONSTANTE.PN_ESTADO_PREFE_INSTI;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_PREF_INST_CONSULTA]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_PREF_INST_CONSULTA;

  /* AUTOR     : JAZABACHE*/
  /* CREADO    : 11/03/2019*/
  /* PROPOSITO  : CONSULTAR LAS SUBCONDICIONES DE CANCER PASO 1*/

   PROCEDURE SP_PFC_S_SUBCONDICION_CONSULTA (
      PN_COD_GRP_DIAG          IN                       NUMBER,
      PN_LINEA_TRATAMIENTO     IN                       NUMBER,
      PN_CONDICION_CANCER      IN                       NUMBER,
      TC_SUBCONDICION_CANCER   OUT                      PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO         OUT                      NUMBER,
      PV_MSG_RESULTADO         OUT                      VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN TC_SUBCONDICION_CANCER FOR SELECT
                                         * /*(SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = PI.P_SUBCONDICION) AS NOMBRE*/
                                      FROM
                                         PFC_PREFERENCIA_INST PI
                                      WHERE
                                         PI.P_CONDICION_CANCER = PN_CONDICION_CANCER
                                         AND PI.COD_GRP_DIAG  = PN_COD_GRP_DIAG
                                         AND PI.P_ESTADO      = PCK_PFC_CONSTANTE.PN_ESTADO_PREFE_INSTI;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_SUBCONDICION_CONSULTA]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_SUBCONDICION_CONSULTA;


  /* AUTHOR  : JAZABACHE*/
  /* CREATED : 25/01/2019*/
  /* PURPOSE : PRECARGAR O REGISTRAR LINEA DE TRATAMIENTO PREFERENCIA INSTITUCIONAL PASO 1*/

   PROCEDURE SP_PFC_SUI_PREF_INST_CONSULTA (
      PN_COD_SOL_EVA         IN                     NUMBER,
      PN_NRO_LINEA_TRA       IN                     NUMBER,
      PV_COD_GRP_DIAG        IN                     VARCHAR2,
      PN_CONDICION           OUT                    NUMBER,
      TC_CONDICION           OUT                    PCK_PFC_CONSTANTE.TYPCUR,
      TC_PREFERENCIA_INSTI   OUT                    PCK_PFC_CONSTANTE.TYPCUR,
      PN_NRO_CURSO           OUT                    NUMBER,
      PN_TIPO_TUMOR          OUT                    NUMBER,
      PN_LUGAR_PROGRESION    OUT                    NUMBER,
      PN_RESP_ALCANZADA      OUT                    NUMBER,
      PN_CUMPLE_PREF_INST    OUT                    NUMBER,
      PV_GRABAR              OUT                    VARCHAR2,
      PV_OBSERVACION         OUT                    VARCHAR2,
      PN_COD_RESULTADO       OUT                    NUMBER,
      PV_MSG_RESULTADO       OUT                    VARCHAR2
   ) AS

      V_COD_SEV_LIN_TRA       NUMBER;
      V_COD_PREF_INST         NUMBER;
      V_NRO_LINEA_TRA         NUMBER;
      V_COD_MEDIC_NUEVO_SEQ   NUMBER;
      V_COD_LINEA_TRAT_SEQ    NUMBER;
      V_MENSAJE               VARCHAR2(100);
      V_J                     NUMBER;
      V_EXC_PARAMETRO EXCEPTION;
      CURSOR CURSOR_CONDICION IS
      SELECT DISTINCT
         P.COD_PARAMETRO,
         P.NOMBRE
      FROM
         PFC_PARAMETRO          P
         RIGHT JOIN PFC_PREFERENCIA_INST   PI ON PI.P_CONDICION_CANCER = P.COD_PARAMETRO
      WHERE
         PI.COD_GRP_DIAG = PV_COD_GRP_DIAG/*'006'*/
         OR P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_CONDICION_NO_REGISTRADO/*237*/
      ORDER BY
         COD_PARAMETRO ASC;

   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN TC_CONDICION FOR SELECT DISTINCT
                               P.COD_PARAMETRO,
                               P.NOMBRE
                            FROM
                               PFC_PARAMETRO          P
                               RIGHT JOIN PFC_PREFERENCIA_INST   PI ON PI.P_CONDICION_CANCER = P.COD_PARAMETRO
                            WHERE
                               PI.COD_GRP_DIAG = PV_COD_GRP_DIAG/*'006'*/
                               OR P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_CONDICION_NO_REGISTRADO/*237*/
                            ORDER BY
                               COD_PARAMETRO ASC;

      V_J                := 0;
      FOR V_ITEM IN CURSOR_CONDICION LOOP V_J := V_J + 1;
      END LOOP;
      IF V_J = 0 THEN
         V_MENSAJE := 'NO EXISTE LISTA DE CONDICION PARA ESTE CODIGO DE GRUPO DIAGNOSTICO : ' || PV_COD_GRP_DIAG;
         RAISE V_EXC_PARAMETRO;
      END IF;

      IF NULLIF(
         PN_COD_SOL_EVA,
         ''
      ) IS NULL THEN
         V_MENSAJE := 'NO EXITE CODIGO DE SOLICITUD DE EVALUACION';
         RAISE V_EXC_PARAMETRO;
      END IF;

      BEGIN
         SELECT
            LT.COD_SEV_LIN_TRA,
            LT.COD_PREF_INST,
            MN.NRO_LINEA_TRA,
            P_CONDICION,
            LT.P_CUMPLE_PREF_INST,
            LT.DESCRIPCION,
            LT.GRABAR
         INTO
            V_COD_SEV_LIN_TRA,
            V_COD_PREF_INST,
            V_NRO_LINEA_TRA,
            PN_CONDICION,
            PN_CUMPLE_PREF_INST,
            PV_OBSERVACION,
            PV_GRABAR
         FROM
            PFC_MEDICAMENTO_NUEVO       MN
            INNER JOIN PFC_SEV_LINEA_TRATAMIENTO   LT ON MN.COD_SEV_LIN_TRA = LT.COD_SEV_LIN_TRA
         WHERE
            MN.COD_SOL_EVA = PN_COD_SOL_EVA;/*16*/

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_COD_SEV_LIN_TRA     := NULL;
            PN_CONDICION          := NULL;
            PN_CUMPLE_PREF_INST   := NULL;
            PV_OBSERVACION        := NULL;
            PV_GRABAR             := NULL;
      END;

      IF V_COD_SEV_LIN_TRA IS NULL THEN
         SELECT
            SQ_PFC_MEDIC_NUEVO_COD_MEDIC.NEXTVAL
         INTO V_COD_MEDIC_NUEVO_SEQ
         FROM
            DUAL;

         INSERT INTO PFC_MEDICAMENTO_NUEVO (
            COD_MEDIC_NUEVO,
            COD_SOL_EVA,
            NRO_LINEA_TRA
         ) VALUES (
            V_COD_MEDIC_NUEVO_SEQ,
            PN_COD_SOL_EVA,
            PN_NRO_LINEA_TRA
         );

         SELECT
            SQ_PFC_SEV_LIN_TRA_COD_SEV_LIN.NEXTVAL
         INTO V_COD_LINEA_TRAT_SEQ
         FROM
            DUAL;

         INSERT INTO PFC_SEV_LINEA_TRATAMIENTO (
            COD_SEV_LIN_TRA,
            GRABAR
         ) VALUES (
            V_COD_LINEA_TRAT_SEQ,
            PCK_PFC_CONSTANTE.PV_CREAR_PASO
         );

         UPDATE PFC_MEDICAMENTO_NUEVO MN
         SET
            MN.COD_SEV_LIN_TRA = V_COD_LINEA_TRAT_SEQ
         WHERE
            MN.COD_MEDIC_NUEVO = V_COD_MEDIC_NUEVO_SEQ;

         BEGIN
            SELECT
               LT.P_NRO_CURSO,
               LT.P_TIPO_TUMOR,
               LT.P_LUGAR_PROGRESION,
               LT.P_RESP_ALCANZADA
            INTO
               PN_NRO_CURSO,
               PN_TIPO_TUMOR,
               PN_LUGAR_PROGRESION,
               PN_RESP_ALCANZADA
            FROM
               PFC_SEV_LINEA_TRATAMIENTO LT
            WHERE
               LT.COD_SEV_LIN_TRA = V_COD_LINEA_TRAT_SEQ;/*1122*/

         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               PN_NRO_CURSO          := NULL;
               PN_TIPO_TUMOR         := NULL;
               PN_LUGAR_PROGRESION   := NULL;
               PN_RESP_ALCANZADA     := NULL;
         END;

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_INSERTAR_EXITO;
      ELSE
         IF V_NRO_LINEA_TRA <> PN_NRO_LINEA_TRA THEN
            UPDATE PFC_MEDICAMENTO_NUEVO MN
            SET
               MN.NRO_LINEA_TRA = PN_NRO_LINEA_TRA
            WHERE
               MN.COD_SOL_EVA = PN_COD_SOL_EVA;

         END IF;

         OPEN TC_PREFERENCIA_INSTI FOR SELECT
                                         PI.P_CONDICION_CANCER,
                                         PI.DETALLE_CONDICION,
                                         PI.PRESENTAC_NO_PERMITIDA,
                                         PI.P_TIPO_TRATAMIENTO,
                                         (
                                            SELECT
                                               P.NOMBRE
                                            FROM
                                               PFC_PARAMETRO P
                                            WHERE
                                               P.COD_PARAMETRO = PI.P_TIPO_TRATAMIENTO
                                         ) AS DESCRIP_TIPO_TRATA,
                                         PI.COD_MAC AS COD_TRATAMIENTO,
                                         (
                                            SELECT
                                               M.DESCRIPCION
                                            FROM
                                               PFC_MAC M
                                            WHERE
                                               M.COD_MAC = PI.COD_MAC
                                         ) AS DESCRIP_TRATAMIENTO
                                      FROM
                                         PFC_PREFERENCIA_INST PI
                                      WHERE
                                         PI.COD_GRP_DIAG = PV_COD_GRP_DIAG/*'010'*/
                                         AND PI.P_CONDICION_CANCER  = PN_CONDICION/*237*/
                                         AND PI.P_ESTADO            = PCK_PFC_CONSTANTE.PN_ESTADO_PREFE_INSTI;

         BEGIN
            SELECT
               LT.P_NRO_CURSO,
               LT.P_TIPO_TUMOR,
               LT.P_LUGAR_PROGRESION,
               LT.P_RESP_ALCANZADA
            INTO
               PN_NRO_CURSO,
               PN_TIPO_TUMOR,
               PN_LUGAR_PROGRESION,
               PN_RESP_ALCANZADA
            FROM
               PFC_SEV_LINEA_TRATAMIENTO LT
            WHERE
               LT.COD_SEV_LIN_TRA = V_COD_SEV_LIN_TRA;/*1122*/

         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               PN_NRO_CURSO          := NULL;
               PN_TIPO_TUMOR         := NULL;
               PN_LUGAR_PROGRESION   := NULL;
               PN_RESP_ALCANZADA     := NULL;
         END;

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
      END IF;

   EXCEPTION
      WHEN V_EXC_PARAMETRO THEN
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SUI_PREF_INST_CONSULTA]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SUI_PREF_INST_CONSULTA;

  /* AUTHOR  : JAZABACHE*/
  /* CREATED : 16/01/2019*/
  /* PURPOSE : PREFERENCIA INSTITUCIONAL PASO 1 -- ELIMINAR*/

   PROCEDURE SP_PFC_SI_PREF_INST (
      PN_COD_GRP_DIAG        IN                     NUMBER,
      PN_COND_CANCER         IN                     NUMBER,
      PN_LINEA_TRATAMIENTO   IN                     NUMBER,
      PN_SUBCOND_CANCER      IN                     NUMBER,
      PN_COD_MAC             IN                     NUMBER,
      PV_REGISTRO            OUT                    VARCHAR2,
      PN_COD_RESULTADO       OUT                    NUMBER,
      PV_MSG_RESULTADO       OUT                    VARCHAR2
   ) AS
      PN_COD_PREF_INST NUMBER;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      SELECT
         PI.COD_PREF_INST
      INTO PN_COD_PREF_INST
      FROM
         PFC_PREFERENCIA_INST PI
      WHERE
         PI.P_CONDICION_CANCER = PN_COND_CANCER
         AND PI.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_PREFE_INSTI;

      IF PN_COD_PREF_INST IS NULL THEN
         BEGIN
            INSERT INTO PFC_PREFERENCIA_INST (
               P_ESTADO,
               COD_GRP_DIAG,
               P_CONDICION_CANCER,
               COD_MAC
            ) VALUES (
               PCK_PFC_CONSTANTE.PN_ESTADO_PREFE_INSTI,
               PN_COD_GRP_DIAG,
               PN_COND_CANCER,
               PN_COD_MAC
            );

            COMMIT;
            PV_REGISTRO        := '1';
            PN_COD_RESULTADO   := 1;
            PV_MSG_RESULTADO   := 'Registro Exitoso';
         EXCEPTION
            WHEN OTHERS THEN
               PN_COD_RESULTADO   := -1;
               PV_MSG_RESULTADO   := '[[SP_PFC_SI_PREF_INST]] '
                                   || TO_CHAR(SQLCODE)
                                   || ' : '
                                   || SQLERRM;
         END;
      ELSE
         PV_REGISTRO        := '0';
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'LOS DATOS YA SE ENCUENTRAN REGISTRADOS';
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         PV_REGISTRO        := '0';
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SI_PREF_INST]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SI_PREF_INST;

  /* AUTOR     : JAZABACHE*/
  /* CREADO    : 25/01/2019*/
  /* PROPOSITO : GUARDAR CAMBIOS CHECKLIST_REQUISITO PASO 2*/

   PROCEDURE SP_PFC_SU_CHKLIST_REQ_GUARDAR (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PN_COD_ROL_USUARIO   IN                   NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO      IN                   VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO       IN                   NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   ) AS

      V_COD_SEV_CHK_REQ_PAC   NUMBER;
      V_ESTADO_SOL_EVA        NUMBER;
      V_EXC_ESTADO_SOL_EVA EXCEPTION;
      V_COD_RESULTADO EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO_SEG     NUMBER; /* SEGUIMIENTO*/
      V_MSG_RESULTADO_SEG     VARCHAR2(4000); /* SEGUIMIENTO*/
      V_ESTADO_SOL_EVA1       NUMBER; /* SEGUIMIENTO*/
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA := NULL;
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;

      SELECT
         MAX(CR.COD_SEV_CHK_REQ_PAC)
      INTO V_COD_SEV_CHK_REQ_PAC
      FROM
         PFC_MEDICAMENTO_NUEVO       MN
         INNER JOIN PFC_SEV_CHECKLIST_REQ_PAC   CR ON CR.COD_SEV_CHK_REQ_PAC = MN.COD_SEV_CHK_REQ_PAC
      WHERE
         MN.COD_SOL_EVA = PN_COD_SOL_EVA;

      UPDATE PFC_SEV_CHECKLIST_REQ_PAC CR
      SET
         CR.GRABAR = '1'
      WHERE
         CR.COD_SEV_CHK_REQ_PAC = V_COD_SEV_CHK_REQ_PAC;

      IF V_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_PASO1 THEN
         UPDATE PFC_SOLICITUD_EVALUACION SEV
         SET
            SEV.P_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_PASO2
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      END IF;
      
      UPDATE PFC_SOLICITUD_EVALUACION SEV
      SET
         SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
         SEV.COD_AUTO_PERTE = PN_COD_USUARIO
      WHERE
         SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
    
     /* SEGUIMIENTO BEGIN */
      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA1
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA1 := NULL;
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;

      ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO(
         PN_COD_SOL_EVA,/* NUMBER*/
         V_ESTADO_SOL_EVA1,
         PV_FECHA_ESTADO,
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL DE USUARIO*/
         PN_COD_USUARIO,/* CODIGO DE USUARIO*/
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         PN_COD_USUARIO, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         V_COD_RESULTADO_SEG,/* NUMBER*/
         V_MSG_RESULTADO_SEG
      ); /* VARCHAR*/

      IF V_COD_RESULTADO_SEG <> 0 THEN
         RAISE V_COD_RESULTADO; /* EXCEPTION*/
      END IF;           
      /* SEGUIMIENTO END*/
      COMMIT;
   EXCEPTION
      WHEN V_COD_RESULTADO THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR, AL INSERTAR LA TABLA SEGUIMIENTO ' 
                               || '[[ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO]]' 
                               || V_MSG_RESULTADO_SEG;
      WHEN V_EXC_ESTADO_SOL_EVA THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR: NO SE ENCONTRO EL ESTADO DE LA SOLICITUD DE EVALUACION';
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SI_CHECKLIST_REQUISITO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SU_CHKLIST_REQ_GUARDAR;

   -- AUTHOR  : JAZABACHE
   -- CREATED : 25/01/2019
   -- PURPOSE : CONSULTAR CHECKLIST_REQUISITO

   PROCEDURE SP_PFC_SI_CHKLIST_REQ_CONSULTA (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PN_EDAD            IN                 NUMBER,
      AC_LISTA_HIST      OUT                PCK_PFC_CONSTANTE.TYPCUR,/*LISTAR HISTORIAL*/
      AC_LISTA_DOC       OUT                PCK_PFC_CONSTANTE.TYPCUR,/*LISTAR TIPO DE DOCUMENTOS*/
      PV_GRABAR          OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS

      PN_COD_PACIENTE             NUMBER;
      V_COD_MAC                   NUMBER;
      V_EDAD                      NUMBER;
      V_COD_SEV_CHK_REQ           NUMBER;
      V_COD_CHKLIST_REQ_PAC_SEQ   NUMBER;
      V_COD_CHKLIST_REQ_SEQ       NUMBER;
      V_DESC_DOCUMENTO            VARCHAR2(70);
      CURSOR V_LIST_TIPO_DOC IS
      SELECT
         PR.COD_PARAMETRO,
         PR.NOMBRE,
         PR.VALOR1
      FROM
         PFC_GRUPO       GR,
         PFC_PARAMETRO   PR
      WHERE
         GR.COD_GRUPO = PR.COD_GRUPO
         AND GR.COD_GRUPO  = 30
         AND GR.ESTADO     = '1'
         AND PR.ESTADO     = '1'
         AND PR.COD_PARAMETRO <> PCK_PFC_CONSTANTE.PN_CHKLIST_TIPO_DOC_OTROS;

   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      BEGIN
         SELECT
            S.COD_AFI_PACIENTE
         INTO PN_COD_PACIENTE
         FROM
            PFC_SOLBEN                 S
            INNER JOIN PFC_SOLICITUD_PRELIMINAR   SP ON S.COD_SCG = SP.COD_SCG
            INNER JOIN PFC_SOLICITUD_EVALUACION   SE ON SP.COD_SOL_PRE = SE.COD_SOL_PRE
         WHERE
            SE.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            PN_COD_PACIENTE := NULL;
      END;

      IF PN_COD_PACIENTE IS NOT NULL THEN
         DBMS_OUTPUT.PUT_LINE('PN_COD_PACIENTE IS NOT NULL');
         BEGIN
            SELECT
               CRP.COD_SEV_CHK_REQ_PAC,
               CRP.GRABAR
            INTO
               V_COD_SEV_CHK_REQ,
               PV_GRABAR
            FROM
               PFC_MEDICAMENTO_NUEVO       MN
               INNER JOIN PFC_SEV_CHECKLIST_REQ_PAC   CRP ON CRP.COD_SEV_CHK_REQ_PAC = MN.COD_SEV_CHK_REQ_PAC
            WHERE
               MN.COD_SOL_EVA = PN_COD_SOL_EVA;

         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               V_COD_SEV_CHK_REQ   := NULL;
               PV_GRABAR           := '0';
         END;

         IF V_COD_SEV_CHK_REQ IS NOT NULL THEN
            OPEN AC_LISTA_HIST FOR
                 SELECT
                      CHKR.COD_CHECKLIST_REQ,
                      MN.NRO_LINEA_TRA   AS LINEA_TRATAMIENTO,
                      MA.DESCRIPCION     AS DESCRIPCION_MAC,
                      CHKR.P_TIPO_DOCUMENTO,
                      TIPODOC.NOMBRE     AS NOMBRE_TIPO_DOCUMENTO,
                      CHKR.DESCRIPCION_DOCUMENTO,
                      CHKR.URL_DESCARGA,
                      TO_CHAR(
                         CHKR.FEC_CARGA,
                         'DD/MM/YYYY'
                      ) AS FEC_CARGA,
                      CHKR.COD_ARCHIVO
                   FROM
                      PFC_SOLICITUD_PRELIMINAR    SP
                      INNER JOIN PFC_MAC                     MA ON MA.COD_MAC = SP.COD_MAC
                      INNER JOIN PFC_SOLICITUD_EVALUACION    SE ON SP.COD_SOL_PRE = SE.COD_SOL_PRE
                      INNER JOIN PFC_MEDICAMENTO_NUEVO       MN ON SE.COD_SOL_EVA = MN.COD_SOL_EVA
                      INNER JOIN PFC_SEV_CHECKLIST_REQ_PAC   CRP ON CRP.COD_SEV_CHK_REQ_PAC = MN.COD_SEV_CHK_REQ_PAC
                      INNER JOIN PFC_CHECKLIST_REQUISITO     CHKR ON CHKR.COD_SEV_CHK_REQ_PAC = CRP.COD_SEV_CHK_REQ_PAC
                      LEFT JOIN PFC_PARAMETRO               TIPODOC ON CHKR.P_TIPO_DOCUMENTO = TIPODOC.COD_PARAMETRO
                   WHERE
                      SE.COD_SOL_EVA = PN_COD_SOL_EVA
                      AND CHKR.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR
                   ORDER BY
                      CHKR.COD_CHECKLIST_REQ DESC;

            OPEN AC_LISTA_DOC FOR
                 SELECT
                      CR.COD_CHECKLIST_REQ,
                      CR.P_TIPO_DOCUMENTO,
                      (
                         SELECT
                            PP.NOMBRE
                         FROM
                            PFC_PARAMETRO PP
                         WHERE
                            PP.COD_PARAMETRO = CR.P_TIPO_DOCUMENTO
                      ) AS NOMBRE_TIPO_DOCUMENTO,
                      CR.DESCRIPCION_DOCUMENTO,
                      CR.P_ESTADO,
                      (
                         SELECT
                            P.NOMBRE
                         FROM
                            PFC_PARAMETRO P
                         WHERE
                            P.COD_PARAMETRO = CR.P_ESTADO
                      ) AS DESCRIPCION_ESTADO,
                      CR.COD_ARCHIVO
                   FROM
                      PFC_CHECKLIST_REQUISITO     CR
                      INNER JOIN PFC_SEV_CHECKLIST_REQ_PAC   CRP ON CRP.COD_SEV_CHK_REQ_PAC = CR.COD_SEV_CHK_REQ_PAC
                      INNER JOIN PFC_MEDICAMENTO_NUEVO       MN ON MN.COD_SEV_CHK_REQ_PAC = CRP.COD_SEV_CHK_REQ_PAC
                      INNER JOIN PFC_SOLICITUD_EVALUACION    SEV ON SEV.COD_SOL_EVA = MN.COD_SOL_EVA
                   WHERE
                      SEV.COD_SOL_EVA = PN_COD_SOL_EVA
                      AND ( CR.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR
                            OR CR.P_ESTADO IS NULL
                            OR ( ( CR.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                   AND CR.P_TIPO_DOCUMENTO  = PCK_PFC_CONSTANTE.PN_TIPO_DOC_RECETA_MED )
                                 OR ( CR.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                      AND CR.P_TIPO_DOCUMENTO  = PCK_PFC_CONSTANTE.PN_TIPO_DOC_INFO_MED )
                                 OR ( CR.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                      AND CR.P_TIPO_DOCUMENTO  = PCK_PFC_CONSTANTE.PN_TIPO_DOC_EVA_GERIAT ) ) );

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
         ELSE
            SELECT
               PR.COD_MAC
            INTO V_COD_MAC
            FROM
               ONTPFC.PFC_SOLICITUD_EVALUACION   EV,
               ONTPFC.PFC_SOLICITUD_PRELIMINAR   PR
            WHERE
               EV.COD_SOL_PRE = PR.COD_SOL_PRE
               AND EV.COD_SOL_EVA = PN_COD_SOL_EVA;

            SELECT
               SQ_PFC_SEV_CHK_REQ_PAC_COD_CHK.NEXTVAL
            INTO V_COD_CHKLIST_REQ_PAC_SEQ
            FROM
               DUAL;

            INSERT INTO PFC_SEV_CHECKLIST_REQ_PAC (
               COD_SEV_CHK_REQ_PAC,
               GRABAR
            ) VALUES (
               V_COD_CHKLIST_REQ_PAC_SEQ,
               PV_GRABAR
            );

            DBMS_OUTPUT.PUT_LINE('SE INSERTO EN PFC_SEV_CHECKLIST_REQ_PAC : ' || V_COD_CHKLIST_REQ_PAC_SEQ);
            UPDATE PFC_MEDICAMENTO_NUEVO MN
            SET
               MN.COD_SEV_CHK_REQ_PAC = V_COD_CHKLIST_REQ_PAC_SEQ
            WHERE
               MN.COD_SOL_EVA = PN_COD_SOL_EVA;

            FOR V_ITEM IN V_LIST_TIPO_DOC LOOP
               DBMS_OUTPUT.PUT_LINE('>>>> ' || V_ITEM.VALOR1);
               V_EDAD := TO_NUMBER(V_ITEM.VALOR1);
               DBMS_OUTPUT.PUT_LINE('V_EDAD : '
                                    || V_EDAD
                                    || ' <<>> PN_EDAD : '
                                    || PN_EDAD);
               IF ( V_EDAD < PN_EDAD ) OR NULLIF(
                  V_EDAD,
                  ''
               ) IS NULL THEN
                  SELECT
                     SQ_PFC_CHK_REQ_COD_CHKLIST_REQ.NEXTVAL
                  INTO V_COD_CHKLIST_REQ_SEQ
                  FROM
                     DUAL;

                  IF V_ITEM.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_CHKLIST_TIPO_DOC_OTROS THEN
                     V_DESC_DOCUMENTO := NULL;
                  ELSE
                     V_DESC_DOCUMENTO := V_ITEM.NOMBRE;
                  END IF;

                  INSERT INTO PFC_CHECKLIST_REQUISITO (
                     COD_CHECKLIST_REQ,
                     COD_SEV_CHK_REQ_PAC,
                     COD_MAC,
                     P_TIPO_DOCUMENTO,
                     DESCRIPCION_DOCUMENTO,
                     ESTADO
                  ) VALUES (
                     V_COD_CHKLIST_REQ_SEQ,
                     V_COD_CHKLIST_REQ_PAC_SEQ,
                     V_COD_MAC,
                     V_ITEM.COD_PARAMETRO,
                     V_DESC_DOCUMENTO,
                     '1'
                  );

                  DBMS_OUTPUT.PUT_LINE('SE INSERTO EN PFC_CHECKLIST_REQUISITO : ' || V_COD_CHKLIST_REQ_SEQ);
               END IF;

            END LOOP;
            
            OPEN AC_LISTA_HIST FOR 
                 SELECT
                      CHKR.COD_CHECKLIST_REQ,
                      MN.NRO_LINEA_TRA   AS LINEA_TRATAMIENTO,
                      MA.DESCRIPCION     AS DESCRIPCION_MAC,
                      CHKR.P_TIPO_DOCUMENTO,
                      TIPODOC.NOMBRE     AS NOMBRE_TIPO_DOCUMENTO,
                      CHKR.DESCRIPCION_DOCUMENTO,
                      CHKR.URL_DESCARGA,
                      TO_CHAR(
                         CHKR.FEC_CARGA,
                         'DD/MM/YYYY'
                      ) AS FEC_CARGA,
                      CHKR.COD_ARCHIVO
                   FROM
                      PFC_SOLICITUD_PRELIMINAR    SP
                      INNER JOIN PFC_MAC                     MA ON MA.COD_MAC = SP.COD_MAC
                      INNER JOIN PFC_SOLICITUD_EVALUACION    SE ON SP.COD_SOL_PRE = SE.COD_SOL_PRE
                      INNER JOIN PFC_MEDICAMENTO_NUEVO       MN ON SE.COD_SOL_EVA = MN.COD_SOL_EVA
                      INNER JOIN PFC_SEV_CHECKLIST_REQ_PAC   CRP ON CRP.COD_SEV_CHK_REQ_PAC = MN.COD_SEV_CHK_REQ_PAC
                      INNER JOIN PFC_CHECKLIST_REQUISITO     CHKR ON CHKR.COD_SEV_CHK_REQ_PAC = CRP.COD_SEV_CHK_REQ_PAC
                      LEFT JOIN PFC_PARAMETRO               TIPODOC ON CHKR.P_TIPO_DOCUMENTO = TIPODOC.COD_PARAMETRO
                   WHERE
                      SE.COD_SOL_EVA = PN_COD_SOL_EVA
                      AND CHKR.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR
                   ORDER BY
                      CHKR.COD_CHECKLIST_REQ DESC;

                OPEN AC_LISTA_DOC FOR
                     SELECT
                          CR.COD_CHECKLIST_REQ,
                          CR.P_TIPO_DOCUMENTO,
                          (
                             SELECT
                                PP.NOMBRE
                             FROM
                                PFC_PARAMETRO PP
                             WHERE
                                PP.COD_PARAMETRO = CR.P_TIPO_DOCUMENTO
                          ) AS NOMBRE_TIPO_DOCUMENTO,
                          CR.DESCRIPCION_DOCUMENTO,
                          CR.P_ESTADO,
                          (
                             SELECT
                                P.NOMBRE
                             FROM
                                PFC_PARAMETRO P
                             WHERE
                                P.COD_PARAMETRO = CR.P_ESTADO
                          ) AS DESCRIPCION_ESTADO,
                          CR.COD_ARCHIVO
                       FROM
                          PFC_CHECKLIST_REQUISITO     CR
                          INNER JOIN PFC_SEV_CHECKLIST_REQ_PAC   CRP ON CRP.COD_SEV_CHK_REQ_PAC = CR.COD_SEV_CHK_REQ_PAC
                          INNER JOIN PFC_MEDICAMENTO_NUEVO       MN ON MN.COD_SEV_CHK_REQ_PAC = CRP.COD_SEV_CHK_REQ_PAC
                          INNER JOIN PFC_SOLICITUD_EVALUACION    SEV ON SEV.COD_SOL_EVA = MN.COD_SOL_EVA
                       WHERE
                          SEV.COD_SOL_EVA = PN_COD_SOL_EVA
                          AND ( CR.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR
                                OR CR.P_ESTADO IS NULL
                                OR ( ( CR.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                       AND CR.P_TIPO_DOCUMENTO  = PCK_PFC_CONSTANTE.PN_TIPO_DOC_RECETA_MED )
                                     OR ( CR.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                          AND CR.P_TIPO_DOCUMENTO  = PCK_PFC_CONSTANTE.PN_TIPO_DOC_INFO_MED )
                                     OR ( CR.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                          AND CR.P_TIPO_DOCUMENTO  = PCK_PFC_CONSTANTE.PN_TIPO_DOC_EVA_GERIAT ) ) );

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'SE CREARON LOS REGISTROS.';
         END IF;

      ELSE
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_SIN_RESULTADO;
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_CHKLIST_REQUISITO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SI_CHKLIST_REQ_CONSULTA;


/* AUTHOR  : JAZABACHE*/
  /* CREATED : 25/01/2019*/
  /* PURPOSE : CARGAR ARCHIVO CHECKLIST DE REQUISITO DEL PACIENTE*/

   PROCEDURE SP_PFC_SI_CHKLIST_CARGAR (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PN_COD_CHKLST_REQ    IN                   NUMBER,
      PN_COD_MAC           IN                   NUMBER,
      PN_TIPO_DOCUMENTO    IN                   NUMBER,
      PV_DESCRIPCION_DOC   IN                   VARCHAR2,
      PN_ESTADO_DOC        IN                   NUMBER,
      PV_URL_DESCARGA      IN                   VARCHAR2,
      PN_COD_ARCHIVO       IN                   NUMBER,
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   ) AS
      V_COD_SEV_CHK_REQ_PAC       NUMBER;
      V_COD_CHKLIST_REQ_SEQ       NUMBER;
      V_COD_CHKLIST_REQ_PAC_SEQ   NUMBER;
      V_COD_PARAMETRO_UPD         NUMBER;
      V_NOMBRE_UPD                VARCHAR2(100);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF PN_COD_CHKLST_REQ IS NULL THEN
         DBMS_OUTPUT.PUT_LINE('PN_COD_CHKLST_REQ IS NULL');
         BEGIN
            SELECT
               MN.COD_SEV_CHK_REQ_PAC
            INTO V_COD_SEV_CHK_REQ_PAC
            FROM
               PFC_SOLICITUD_EVALUACION    SE
               INNER JOIN PFC_MEDICAMENTO_NUEVO       MN ON SE.COD_SOL_EVA = MN.COD_SOL_EVA
               INNER JOIN PFC_SEV_CHECKLIST_REQ_PAC   CR ON MN.COD_SEV_CHK_REQ_PAC = CR.COD_SEV_CHK_REQ_PAC
            WHERE
               MN.COD_SOL_EVA = PN_COD_SOL_EVA;

         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               V_COD_SEV_CHK_REQ_PAC := NULL;
         END;

         DBMS_OUTPUT.PUT_LINE('--  1  --');
         IF V_COD_SEV_CHK_REQ_PAC IS NULL THEN
            DBMS_OUTPUT.PUT_LINE('V_COD_SEV_CHK_REQ_PAC IS NULL');
            SELECT
               SQ_PFC_SEV_CHK_REQ_PAC_COD_CHK.NEXTVAL
            INTO V_COD_CHKLIST_REQ_PAC_SEQ
            FROM
               DUAL;

            INSERT INTO PFC_SEV_CHECKLIST_REQ_PAC (
               COD_SEV_CHK_REQ_PAC,
               GRABAR
            ) VALUES (
               V_COD_CHKLIST_REQ_PAC_SEQ,
               '0'
            );

            SELECT
               SQ_PFC_CHK_REQ_COD_CHKLIST_REQ.NEXTVAL
            INTO V_COD_CHKLIST_REQ_SEQ
            FROM
               DUAL;

            INSERT INTO PFC_CHECKLIST_REQUISITO (
               COD_CHECKLIST_REQ,
               COD_SEV_CHK_REQ_PAC,
               COD_MAC,
               P_TIPO_DOCUMENTO,
               DESCRIPCION_DOCUMENTO,
               FEC_CARGA,
               URL_DESCARGA,
               P_ESTADO,
               COD_ARCHIVO
            ) VALUES (
               V_COD_CHKLIST_REQ_SEQ,
               V_COD_CHKLIST_REQ_PAC_SEQ,
               PN_COD_MAC,
               PN_TIPO_DOCUMENTO,
               PV_DESCRIPCION_DOC,
               SYSDATE,
               PV_URL_DESCARGA,
               PN_ESTADO_DOC,
               PN_COD_ARCHIVO
            );

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_INSERTAR_EXITO;
         ELSE
            DBMS_OUTPUT.PUT_LINE('--  2  --');
            DBMS_OUTPUT.PUT_LINE('V_COD_SEV_CHK_REQ_PAC IS NOT NULL');
            SELECT
               SQ_PFC_CHK_REQ_COD_CHKLIST_REQ.NEXTVAL
            INTO V_COD_CHKLIST_REQ_SEQ
            FROM
               DUAL;
        /* SOLO INSERTAMOS EL REGISTRO EN LA TABLA PFC_CHECKLIST_REQUISITO*/

            INSERT INTO PFC_CHECKLIST_REQUISITO (
               COD_CHECKLIST_REQ,
               COD_SEV_CHK_REQ_PAC,
               COD_MAC,
               P_TIPO_DOCUMENTO,
               DESCRIPCION_DOCUMENTO,
               FEC_CARGA,
               URL_DESCARGA,
               P_ESTADO,
               COD_ARCHIVO
            ) VALUES (
               V_COD_CHKLIST_REQ_SEQ,
               V_COD_SEV_CHK_REQ_PAC,
               PN_COD_MAC,
               PN_TIPO_DOCUMENTO,
               PV_DESCRIPCION_DOC,
               SYSDATE,
               PV_URL_DESCARGA,
               PN_ESTADO_DOC,
               PN_COD_ARCHIVO
            );

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_INSERTAR_EXITO;
         END IF;

      ELSE
         DBMS_OUTPUT.PUT_LINE('--  3  --');
         DBMS_OUTPUT.PUT_LINE('--  PN_TIPO_DOCUMENTO  : ' || PN_TIPO_DOCUMENTO);
         BEGIN
            SELECT
               P.COD_PARAMETRO,
               P.NOMBRE
            INTO
               V_COD_PARAMETRO_UPD,
               V_NOMBRE_UPD
            FROM
               PFC_PARAMETRO P
            WHERE
               P.COD_PARAMETRO = PN_TIPO_DOCUMENTO;

            IF PN_TIPO_DOCUMENTO = 87 THEN
               V_NOMBRE_UPD := PV_DESCRIPCION_DOC;
            END IF;
            DBMS_OUTPUT.PUT_LINE('--  V_NOMBRE_UPD  : ' || V_NOMBRE_UPD);
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               V_COD_PARAMETRO_UPD   := NULL;
               V_NOMBRE_UPD          := NULL;
         END;

         IF NULLIF(
            V_COD_PARAMETRO_UPD,
            ''
         ) IS NULL THEN
            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'DATOS INGRESADOS INCORRECTO';
         ELSE
            DBMS_OUTPUT.PUT_LINE('UPDATE PFC_CHECKLIST_REQUISITO CR');
            UPDATE PFC_CHECKLIST_REQUISITO CR
            SET
               CR.COD_MAC = PN_COD_MAC,
               CR.P_TIPO_DOCUMENTO = PN_TIPO_DOCUMENTO,
               CR.DESCRIPCION_DOCUMENTO = V_NOMBRE_UPD,
               CR.FEC_CARGA = SYSDATE,
               CR.URL_DESCARGA = PV_URL_DESCARGA,
               CR.P_ESTADO = PN_ESTADO_DOC,
               CR.COD_ARCHIVO = PN_COD_ARCHIVO
            WHERE
               CR.COD_CHECKLIST_REQ = PN_COD_CHKLST_REQ;

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
         END IF;

      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_SI_CHKLIST_CARGAR] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SI_CHKLIST_CARGAR;

  /* AUTHOR  : JAZABACHE*/
  /* CREATED : 25/01/2019*/
  /* PURPOSE : CONSULTAR ACTUALIZAR CHECKLIST ELIMINAR*/

   PROCEDURE SP_PFC_SU_CHKLIST_ELIMINAR (
      PN_COD_CHKLST_REQ   IN                  NUMBER,
      PN_ESTADO_DOC       IN                  NUMBER,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF PN_ESTADO_DOC <> PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR THEN
         UPDATE PFC_CHECKLIST_REQUISITO CRE
         SET
            CRE.P_ESTADO = PN_ESTADO_DOC,
            CRE.ESTADO = '1'
         WHERE
            CRE.COD_CHECKLIST_REQ = PN_COD_CHKLST_REQ;

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
      ELSE
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'El CODIGO DE ESTADO '
                             || PN_ESTADO_DOC
                             || ' ES INCORRECTO.';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SU_CHKLIST_ELIMINAR][PFC_CHECKLIST_REQUISITO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SU_CHKLIST_ELIMINAR;


   /* AUTHOR  : JAZABACHE*/
   /* CREATED : 25/01/2019*/
   /* PURPOSE : PRECARGA CONDICION BASAL DEL PACIENTE - PASO 3*/

   PROCEDURE SP_PFC_S_CONDICION_BASAL_RESU (
      PN_SOL_EVA            IN                    NUMBER,
      PN_COD_MAC            IN                    NUMBER,
      PN_COD_GRP_DIAG       IN                    NUMBER,
      PV_ANTECEDENTES       OUT                   VARCHAR2,
      PN_ECOG               OUT                   NUMBER,
      PN_EXISTE_TOXICIDAD   OUT                   NUMBER,
      PN_TIPO_TOXICIDAD     OUT                   NUMBER,
      PV_GRABAR             OUT                   VARCHAR2,
      TC_LIST_RESULTADO     OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      TC_CMB_VALOR_FIJO     OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      TC_METASTASIS         OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS

      V_SQ_COD_RESULT_BASAL_MARCA   NUMBER := NULL;
      V_SQ_COD_SEV_CON_BAS_PAC      NUMBER;
      V_COD_SEV_CON_PAC             NUMBER;
      V_CONTAR_CONFIG_MARCA         NUMBER;
      V_COD_RES_BAS_SQ              NUMBER;
      
      CURSOR C_CONFIG_MARCADOR IS
      SELECT
         CM.COD_CONFIG_MARCA
      FROM
         PFC_CONFIGURACION_MARCADOR   CM
         INNER JOIN PFC_EXAMEN_MEDICO_MARCADOR   EM ON CM.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
      WHERE
         CM.COD_MAC = PN_COD_MAC
         AND CM.COD_GRP_DIAG  = PN_COD_GRP_DIAG
         AND CM.P_ESTADO      = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
         AND EM.P_ESTADO      = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
         AND (CM.P_TIPO_MARCADOR = 66 OR CM.P_TIPO_MARCADOR = 125 OR CM.P_TIPO_MARCADOR = 127)
         AND 1                = (
            SELECT DISTINCT
               ( 1 )
            FROM
               PFC_EXAMEN_MEDICO_DETALLE EMD
            WHERE
               EMD.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
               AND EMD.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
         );

      CURSOR C_CONFIG_MARCADOR_EXAM_MED IS
      SELECT DISTINCT
         EMD.P_TIPO_INGRESO_RES,
         CM.COD_CONFIG_MARCA
      FROM
         PFC_CONFIGURACION_MARCADOR   CM
         INNER JOIN PFC_EXAMEN_MEDICO_MARCADOR   EM ON CM.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
         INNER JOIN PFC_EXAMEN_MEDICO_DETALLE    EMD ON EMD.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
      WHERE
         EM.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
         AND EMD.P_ESTADO            = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
         AND CM.P_ESTADO             = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
         AND CM.COD_MAC              = PN_COD_MAC
         AND CM.COD_GRP_DIAG         = PN_COD_GRP_DIAG
         AND (CM.P_TIPO_MARCADOR = 66 OR CM.P_TIPO_MARCADOR = 125 OR CM.P_TIPO_MARCADOR = 127)
         AND EMD.P_TIPO_INGRESO_RES  = PCK_PFC_CONSTANTE.PN_CODIGO_VARIABLES_FIJOS;

      CURSOR V_RESULTADO_CONFIG_MARCA IS
      SELECT
         A.COD_RESULTADO_MARCADOR,
         A.COD_CONFIG_MARCA AS COD_CONFIG_MARCA_RES,
         A.RESULTADO,
         B.COD_CONFIG_MARCA,
         B.COD_EXAMEN_MED
      FROM
         (
            SELECT
               RBM.COD_CONFIG_MARCA,
               RBM.COD_RESULTADO_MARCADOR,
               RBM.RESULTADO,
               RBM.FEC_RESULTADO
            FROM
               PFC_RESULTADO_BASAL_MARCADOR RBM
            WHERE
               RBM.COD_SEV_CON_PAC = V_COD_SEV_CON_PAC
         ) A
         FULL OUTER JOIN (
            SELECT
               CM.COD_CONFIG_MARCA,
               CM.COD_EXAMEN_MED
            FROM
               PFC_CONFIGURACION_MARCADOR   CM
               INNER JOIN PFC_EXAMEN_MEDICO_MARCADOR   EM ON CM.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
            WHERE
               CM.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
               AND EM.P_ESTADO      = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
               AND CM.COD_MAC       = PN_COD_MAC
               AND CM.COD_GRP_DIAG  = PN_COD_GRP_DIAG
               AND (CM.P_TIPO_MARCADOR = 66 OR CM.P_TIPO_MARCADOR = 125 OR CM.P_TIPO_MARCADOR = 127)
               AND 1                = (
                  SELECT DISTINCT
                     ( 1 )
                  FROM
                     PFC_EXAMEN_MEDICO_DETALLE EMD
                  WHERE
                     EMD.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
                     AND EMD.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
               )
         ) B ON A.COD_CONFIG_MARCA = B.COD_CONFIG_MARCA;

   BEGIN
      PN_COD_RESULTADO        := -1;
      PV_MSG_RESULTADO        := 'valor inicial';
      V_CONTAR_CONFIG_MARCA   := 0;
      SELECT
         COUNT(CM.COD_CONFIG_MARCA)
      INTO V_CONTAR_CONFIG_MARCA
      FROM
         PFC_CONFIGURACION_MARCADOR   CM
         INNER JOIN PFC_EXAMEN_MEDICO_MARCADOR   EM ON CM.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
      WHERE
         CM.COD_MAC = PN_COD_MAC
         AND CM.COD_GRP_DIAG  = PN_COD_GRP_DIAG
         AND CM.P_ESTADO      = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
         AND (CM.P_TIPO_MARCADOR = 66 OR CM.P_TIPO_MARCADOR = 125 OR CM.P_TIPO_MARCADOR = 127)
         AND EM.P_ESTADO      = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
         AND 1                = (
            SELECT DISTINCT
               ( 1 )
            FROM
               PFC_EXAMEN_MEDICO_DETALLE EME
            WHERE
               EME.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
               AND EME.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
         );

      DBMS_OUTPUT.PUT_LINE('COUNT >>> ' || V_CONTAR_CONFIG_MARCA);
      BEGIN
         SELECT
            MN.COD_SEV_CON_PAC
         INTO V_COD_SEV_CON_PAC
         FROM
            PFC_MEDICAMENTO_NUEVO MN
         WHERE
            MN.COD_SOL_EVA = PN_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_COD_SEV_CON_PAC := NULL;
      END;

      OPEN TC_CMB_VALOR_FIJO FOR SELECT
                                   EMD.COD_EXAMEN_MED_DET,
                                   EMD.COD_EXAMEN_MED,
                                   (
                                      SELECT
                                         P.NOMBRE
                                      FROM
                                         PFC_PARAMETRO P
                                      WHERE
                                         P.COD_PARAMETRO = EMD.P_TIPO_INGRESO_RES
                                         AND P.ESTADO = 1
                                   ) AS TIPO_INGRESO_RES,
                                   EMD.VALOR_FIJO
                                FROM
                                   PFC_CONFIGURACION_MARCADOR   CM
                                   INNER JOIN PFC_EXAMEN_MEDICO_MARCADOR   EM ON CM.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
                                   INNER JOIN PFC_EXAMEN_MEDICO_DETALLE    EMD ON EMD.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
                                WHERE
                                   EM.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
                                   AND EMD.P_ESTADO            = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
                                   AND CM.P_ESTADO             = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
                                   AND CM.COD_MAC              = PN_COD_MAC
                                   AND CM.COD_GRP_DIAG         = PN_COD_GRP_DIAG
                                   AND (CM.P_TIPO_MARCADOR = 66 OR CM.P_TIPO_MARCADOR = 125 OR CM.P_TIPO_MARCADOR = 127)
                                   AND EMD.P_TIPO_INGRESO_RES  = PCK_PFC_CONSTANTE.PN_CODIGO_VARIABLES_FIJOS;

      IF V_CONTAR_CONFIG_MARCA = 0 THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'NO SE ENCUENTRA ALGUNA CONFIGURACION DE LA MAC Y GRUPO DE DIAGNOSTICO.';
      ELSIF V_COD_SEV_CON_PAC IS NOT NULL THEN
         DBMS_OUTPUT.PUT_LINE('<<< CODIGO DE LA CONDICION BASAL DEL PACIENTE DIFERENTE DE NULL >>> ');
         FOR V_ITEM IN V_RESULTADO_CONFIG_MARCA LOOP
            IF V_ITEM.COD_CONFIG_MARCA IS NULL THEN
               DELETE PFC_RESULTADO_BASAL_MARCADOR RBM
               WHERE
                  RBM.COD_RESULTADO_MARCADOR = V_ITEM.COD_RESULTADO_MARCADOR;

               DBMS_OUTPUT.PUT_LINE('<<< SE BORRO EL REGISTRO : ' || V_ITEM.COD_RESULTADO_MARCADOR);
               UPDATE PFC_SEV_CONDICION_BASAL_PAC CBP
               SET
                  CBP.GRABAR = 0
               WHERE
                  CBP.COD_SEV_CON_PAC = V_COD_SEV_CON_PAC;

            END IF;

            IF V_ITEM.COD_RESULTADO_MARCADOR IS NULL THEN
               SELECT
                  SQ_PFC_RES_BAS_MAR_COD_RES_BAS.NEXTVAL
               INTO V_COD_RES_BAS_SQ
               FROM
                  DUAL;

               INSERT INTO PFC_RESULTADO_BASAL_MARCADOR (
                  COD_RESULTADO_MARCADOR,
                  COD_SEV_CON_PAC,
                  COD_CONFIG_MARCA
               ) VALUES (
                  V_COD_RES_BAS_SQ,
                  V_COD_SEV_CON_PAC,
                  V_ITEM.COD_CONFIG_MARCA
               );

               UPDATE PFC_SEV_CONDICION_BASAL_PAC CBP
               SET
                  CBP.GRABAR = 0
               WHERE
                  CBP.COD_SEV_CON_PAC = V_COD_SEV_CON_PAC;

               DBMS_OUTPUT.PUT_LINE('<<< SE INSERTO EL REGISTRO : ' || V_COD_RES_BAS_SQ);
            END IF;

         END LOOP;

         OPEN TC_LIST_RESULTADO FOR SELECT
                                      EM.COD_EXAMEN_MED,
                                      RBM.COD_RESULTADO_MARCADOR,
                                      CM.COD_CONFIG_MARCA,
                                      EM.DESCRIPCION,
                                      EMD.P_TIPO_INGRESO_RES,
                                      (
                                         SELECT
                                            P.NOMBRE
                                         FROM
                                            PFC_PARAMETRO P
                                         WHERE
                                            P.COD_PARAMETRO = EMD.P_TIPO_INGRESO_RES
                                      ) AS TIPO_INGRESO_RES,
                                      EMD.UNIDAD_MEDIDA,
                                      EMD.RANGO,
                                      RBM.RESULTADO,
                                      RBM.FEC_RESULTADO AS FEC_RESULTADO,
                                      EMD.RANGO_MINIMO,
                                      EMD.RANGO_MAXIMO
                                   FROM
                                      PFC_CONFIGURACION_MARCADOR     CM
                                      INNER JOIN PFC_EXAMEN_MEDICO_MARCADOR     EM ON CM.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
                                      INNER JOIN PFC_EXAMEN_MEDICO_DETALLE      EMD ON EMD.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
                                      INNER JOIN PFC_RESULTADO_BASAL_MARCADOR   RBM ON RBM.COD_CONFIG_MARCA = CM.COD_CONFIG_MARCA
                                   WHERE
                                      CM.COD_MAC = PN_COD_MAC
                                      AND CM.COD_GRP_DIAG        = PN_COD_GRP_DIAG
                                      AND EM.P_ESTADO            = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
                                      AND CM.P_ESTADO            = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
                                      AND (CM.P_TIPO_MARCADOR = 66 OR CM.P_TIPO_MARCADOR = 125 OR CM.P_TIPO_MARCADOR = 127)
                                      AND RBM.COD_SEV_CON_PAC    = V_COD_SEV_CON_PAC
                                      AND ( EMD.P_TIPO_INGRESO_RES <> PCK_PFC_CONSTANTE.PN_CODIGO_VARIABLES_FIJOS
                                            OR EMD.COD_EXAMEN_MED_DET  = (
                                         SELECT
                                            MIN(MD.COD_EXAMEN_MED_DET)
                                         FROM
                                            PFC_EXAMEN_MEDICO_DETALLE MD
                                         WHERE
                                            MD.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
                                            AND MD.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
                                      ) );

         OPEN TC_METASTASIS FOR
              SELECT
                  ME.COD_METASTASIS,
                  (SELECT
                        P.NOMBRE
                     FROM
                        PFC_PARAMETRO P
                     WHERE
                        P.COD_PARAMETRO = ME.P_LINEA_METASTASIS
                        AND P.ESTADO = 1
                  ) AS LINEA_METASTASIS,
                  ME.P_LINEA_METASTASIS,
                  (SELECT
                        P.NOMBRE
                     FROM
                        PFC_PARAMETRO P
                     WHERE
                        P.COD_PARAMETRO = ME.P_LUGAR_METASTASIS
                        AND P.ESTADO = 1
                  ) AS LUGAR_METASTASIS,
                  ME.P_LUGAR_METASTASIS,
                  ME.ESTADO
               FROM
                  PFC_METASTASIS ME
               WHERE
                  ME.COD_SEV_CON_PAC = V_COD_SEV_CON_PAC;

         BEGIN
            SELECT
               CBP.ANTECEDENTES_IMP,
               CBP.P_ECOG,
               CBP.P_EXISTE_TOXICIDAD,
               CBP.P_TIPO_TOXICIDAD,
               CBP.GRABAR
            INTO
               PV_ANTECEDENTES,
               PN_ECOG,
               PN_EXISTE_TOXICIDAD,
               PN_TIPO_TOXICIDAD,
               PV_GRABAR
            FROM
               PFC_SEV_CONDICION_BASAL_PAC CBP
            WHERE
               CBP.COD_SEV_CON_PAC = V_COD_SEV_CON_PAC;

         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               PV_ANTECEDENTES       := NULL;
               PN_ECOG               := NULL;
               PN_EXISTE_TOXICIDAD   := NULL;
               PN_TIPO_TOXICIDAD     := NULL;
         END;

         COMMIT;
         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
      ELSIF V_COD_SEV_CON_PAC IS NULL THEN
         PV_GRABAR          := '0';
         SELECT
            SQ_PFC_SEV_CON_BAS_PAC_COD_CON.NEXTVAL
         INTO V_SQ_COD_SEV_CON_BAS_PAC
         FROM
            DUAL;

         DBMS_OUTPUT.PUT_LINE(' SEQUENCE >> V_SQ_COD_SEV_CON_BAS_PAC : ' || V_SQ_COD_SEV_CON_BAS_PAC);
         INSERT INTO PFC_SEV_CONDICION_BASAL_PAC (
            COD_SEV_CON_PAC,
            GRABAR
         ) VALUES (
            V_SQ_COD_SEV_CON_BAS_PAC,
            PV_GRABAR
         );

         DBMS_OUTPUT.PUT_LINE('SE INSERTO');
         UPDATE PFC_MEDICAMENTO_NUEVO MN
         SET
            MN.COD_SEV_CON_PAC = V_SQ_COD_SEV_CON_BAS_PAC
         WHERE
            MN.COD_SOL_EVA = PN_SOL_EVA;

         FOR V_ITEM IN C_CONFIG_MARCADOR LOOP
            SELECT
               SQ_PFC_RES_BAS_MAR_COD_RES_BAS.NEXTVAL
            INTO V_SQ_COD_RESULT_BASAL_MARCA
            FROM
               DUAL;

            DBMS_OUTPUT.PUT_LINE('SEQUENCE >> V_COD_RESULT_BASAL_MARCA : '
                                 || V_SQ_COD_RESULT_BASAL_MARCA
                                 || ' :: '
                                 || V_SQ_COD_SEV_CON_BAS_PAC);
            INSERT INTO PFC_RESULTADO_BASAL_MARCADOR (
               COD_RESULTADO_MARCADOR,
               COD_SEV_CON_PAC,
               COD_CONFIG_MARCA
            ) VALUES (
               V_SQ_COD_RESULT_BASAL_MARCA,
               V_SQ_COD_SEV_CON_BAS_PAC,
               V_ITEM.COD_CONFIG_MARCA
            );

            DBMS_OUTPUT.PUT_LINE('SE INSERTO');
         END LOOP;

         COMMIT;
         OPEN TC_LIST_RESULTADO FOR
              SELECT
                       EM.COD_EXAMEN_MED,
                       RBM.COD_RESULTADO_MARCADOR,
                       CM.COD_CONFIG_MARCA,
                       EM.DESCRIPCION,
                       EMD.P_TIPO_INGRESO_RES,
                       (
                          SELECT
                             P.NOMBRE
                          FROM
                             PFC_PARAMETRO P
                          WHERE
                             P.COD_PARAMETRO = EMD.P_TIPO_INGRESO_RES
                             AND P.ESTADO = 1
                       ) AS TIPO_INGRESO_RES,
                       EMD.UNIDAD_MEDIDA,
                       EMD.RANGO,
                       RBM.RESULTADO,
                       TO_CHAR(
                          RBM.FEC_RESULTADO,
                          'DD/MM/YYYY'
                       ) AS FEC_RESULTADO,
                       EMD.RANGO_MINIMO,
                       EMD.RANGO_MAXIMO
                    FROM
                       PFC_CONFIGURACION_MARCADOR     CM
                       INNER JOIN PFC_EXAMEN_MEDICO_MARCADOR     EM ON CM.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
                       INNER JOIN PFC_EXAMEN_MEDICO_DETALLE      EMD ON EMD.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
                       INNER JOIN PFC_RESULTADO_BASAL_MARCADOR   RBM ON RBM.COD_CONFIG_MARCA = CM.COD_CONFIG_MARCA
                    WHERE
                       CM.COD_MAC = PN_COD_MAC
                       AND CM.COD_GRP_DIAG        = PN_COD_GRP_DIAG
                       AND (CM.P_TIPO_MARCADOR = 66 OR CM.P_TIPO_MARCADOR = 125 OR CM.P_TIPO_MARCADOR = 127)
                       AND EM.P_ESTADO            = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
                       AND EMD.P_ESTADO           = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
                       AND RBM.COD_SEV_CON_PAC    = V_SQ_COD_SEV_CON_BAS_PAC
                       AND ( EMD.P_TIPO_INGRESO_RES <> PCK_PFC_CONSTANTE.PN_CODIGO_VARIABLES_FIJOS
                             OR EMD.COD_EXAMEN_MED_DET  = (
                          SELECT
                             MIN(MD.COD_EXAMEN_MED_DET)
                          FROM
                             PFC_EXAMEN_MEDICO_DETALLE MD
                          WHERE
                             MD.COD_EXAMEN_MED = EM.COD_EXAMEN_MED
                       ) );

         OPEN TC_METASTASIS FOR SELECT
                                  ME.COD_METASTASIS,
                                  ME.P_LINEA_METASTASIS,
                                  ME.P_LUGAR_METASTASIS,
                                  ME.ESTADO
                               FROM
                                  PFC_METASTASIS ME
                               WHERE
                                  ME.COD_SEV_CON_PAC = V_COD_SEV_CON_PAC;
                                  
         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_INSERTAR_EXITO;
      ELSE
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_SIN_RESULTADO;
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_S_CONDICION_BASAL_RESU] : '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_CONDICION_BASAL_RESU;

 /* AUTHOR  : CQUISPE - JAZABACHE*/
  /* CREATED : 21/01/2019*/
  /* PROPOSITO : GUARDAR CAMBIOS DE CONDICION BASAL DEL PACIENTE - PASO 3*/

   PROCEDURE SP_PFC_I_CONDICION_BASAL (
      PN_COD_SOL_EVA        IN                    NUMBER,
      PV_LINEA_LUGAR_META   IN                    VARCHAR2,
      PN_ECOG               IN                    NUMBER,
      PN_EXISTE_TOX         IN                    NUMBER,
      PN_TIPO_TOX           IN                    NUMBER,
      PV_RESULTADO_BASAL    IN                    VARCHAR2,
      PV_ANTECEDENTE        IN                    VARCHAR2,
      PN_COD_ROL_USUARIO    IN                    NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO       IN                    VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO        IN                    NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS

      V_COD_METASTASIS_SEQ   NUMBER;
      V_COD_SEV_CON_PAC      NUMBER;
      V_EXC_COD_SEV_CON_PAC EXCEPTION;
      V_EXC_ESTADO_SOL_EVA EXCEPTION;
      V_COUNT_META           NUMBER;
      V_ESTADO_SOL_EVA       NUMBER;
      V_I                    NUMBER;
      V_J                    NUMBER;
      V_COD_RESULTADO EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO_SEG    NUMBER; /* SEGUIMIENTO*/
      V_MSG_RESULTADO_SEG    VARCHAR2(4000); /* SEGUIMIENTO*/
      V_ESTADO_SOL_EVA1      NUMBER; /* SEGUIMIENTO*/
      
      CURSOR TC_METASTASIS IS
      SELECT
         REGEXP_SUBSTR(
            METASTASIS,
            '([^,]+)',
            1,
            1,
            '',
            1
         ) AS V_LINEA_META,
         REGEXP_SUBSTR(
            METASTASIS,
            '([^,]+)',
            1,
            2,
            '',
            1
         ) AS V_LUGAR_LUGAR
      FROM
         (
            SELECT
               REGEXP_SUBSTR(
                  PV_LINEA_LUGAR_META,
                  '[^|]+',
                  1,
                  LEVEL
               ) AS METASTASIS
            FROM
               DUAL
            CONNECT BY
               REGEXP_SUBSTR(
                  PV_LINEA_LUGAR_META,
                  '[^|]+',
                  1,
                  LEVEL
               ) IS NOT NULL
         );

      CURSOR TC_RESULTADO_BASAL IS
      SELECT
         REGEXP_SUBSTR(
            RESULTADO_BASAL,
            '([^,]+)',
            1,
            1,
            '',
            1
         ) AS V_COD_RESULTADO_MARCADOR,
         REGEXP_SUBSTR(
            RESULTADO_BASAL,
            '([^,]+)',
            1,
            2,
            '',
            1
         ) AS V_RESULTADO,
         REGEXP_SUBSTR(
            RESULTADO_BASAL,
            '([^,]+)',
            1,
            3,
            '',
            1
         ) AS V_FEC_RESULTADO
      FROM
         (
            SELECT
               REGEXP_SUBSTR(
                  PV_RESULTADO_BASAL,
                  '[^|]+',
                  1,
                  LEVEL
               ) AS RESULTADO_BASAL
            FROM
               DUAL
            CONNECT BY
               REGEXP_SUBSTR(
                  PV_RESULTADO_BASAL,
                  '[^|]+',
                  1,
                  LEVEL
               ) IS NOT NULL
         );

   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      BEGIN
         SELECT
            CB.COD_SEV_CON_PAC
         INTO V_COD_SEV_CON_PAC
         FROM
            PFC_SEV_CONDICION_BASAL_PAC   CB
            INNER JOIN PFC_MEDICAMENTO_NUEVO         MN ON MN.COD_SEV_CON_PAC = CB.COD_SEV_CON_PAC
         WHERE
            MN.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_COD_SEV_CON_PAC := NULL;
      END;

      IF V_COD_SEV_CON_PAC IS NULL THEN
         RAISE V_EXC_COD_SEV_CON_PAC;
      END IF;
      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA := NULL;
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;

      V_I                := 0;
      FOR V_ITEM IN TC_METASTASIS LOOP V_I := V_I + 1;
      END LOOP;
      DBMS_OUTPUT.PUT_LINE('V_I : ' || V_I);
      V_J                := 0;
      FOR V_ITEM IN TC_RESULTADO_BASAL LOOP V_J := V_J + 1;
      END LOOP;
      DBMS_OUTPUT.PUT_LINE('V_J : ' || V_J);
      SELECT
         COUNT(1)
      INTO V_COUNT_META
      FROM
         PFC_METASTASIS ME
      WHERE
         ME.COD_SEV_CON_PAC = V_COD_SEV_CON_PAC;

      DBMS_OUTPUT.PUT_LINE('V_COUNT_META : ' || V_COUNT_META);
      IF V_COUNT_META > 0 THEN
         DELETE PFC_METASTASIS ME WHERE ME.COD_SEV_CON_PAC = V_COD_SEV_CON_PAC;
    
         IF (PV_LINEA_LUGAR_META IS NOT NULL) THEN
            FOR V_ITEM IN TC_METASTASIS LOOP
                SELECT SQ_PFC_COD_METASTASIS.NEXTVAL INTO V_COD_METASTASIS_SEQ FROM DUAL;
                
                INSERT INTO PFC_METASTASIS (COD_METASTASIS,P_LINEA_METASTASIS,P_LUGAR_METASTASIS,COD_SEV_CON_PAC,ESTADO) 
                VALUES (V_COD_METASTASIS_SEQ,V_ITEM.V_LINEA_META,V_ITEM.V_LUGAR_LUGAR,V_COD_SEV_CON_PAC,'1');
            END LOOP;
        END IF;
      ELSE
         DBMS_OUTPUT.PUT_LINE('4');
         FOR V_ITEM IN TC_METASTASIS LOOP
            SELECT SQ_PFC_COD_METASTASIS.NEXTVAL INTO V_COD_METASTASIS_SEQ FROM DUAL;

            DBMS_OUTPUT.PUT_LINE('V_COD_METASTASIS_SEQ : ' || V_COD_METASTASIS_SEQ);
            IF (PV_LINEA_LUGAR_META IS NOT NULL) THEN
                INSERT INTO PFC_METASTASIS (COD_METASTASIS,P_LINEA_METASTASIS,P_LUGAR_METASTASIS,COD_SEV_CON_PAC,ESTADO) 
                VALUES (V_COD_METASTASIS_SEQ,V_ITEM.V_LINEA_META,V_ITEM.V_LUGAR_LUGAR,V_COD_SEV_CON_PAC,'1');
            END IF;
         END LOOP;
      END IF;

      UPDATE PFC_SEV_CONDICION_BASAL_PAC CB
      SET
         CB.ANTECEDENTES_IMP = PV_ANTECEDENTE,
         CB.P_ECOG = PN_ECOG,
         CB.P_EXISTE_TOXICIDAD = PN_EXISTE_TOX,
         CB.P_TIPO_TOXICIDAD = PN_TIPO_TOX,
         CB.GRABAR = '1'
      WHERE
         CB.COD_SEV_CON_PAC = V_COD_SEV_CON_PAC;

      FOR V_ITEM IN TC_RESULTADO_BASAL LOOP UPDATE PFC_RESULTADO_BASAL_MARCADOR RBM
      SET
         RBM.RESULTADO = V_ITEM.V_RESULTADO,
         RBM.FEC_RESULTADO = TO_DATE(
            V_ITEM.V_FEC_RESULTADO,
            'DD/MM/YYYY'
         )
      WHERE
         RBM.COD_RESULTADO_MARCADOR = V_ITEM.V_COD_RESULTADO_MARCADOR;

      END LOOP;

      IF V_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_PASO2 THEN
         DBMS_OUTPUT.PUT_LINE('>> V_ESTADO_SOL_EVA : ' || V_ESTADO_SOL_EVA);
         UPDATE PFC_SOLICITUD_EVALUACION SEV
         SET
            SEV.P_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_PASO3
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      END IF;
      
      UPDATE PFC_SOLICITUD_EVALUACION SEV
      SET
         SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
         SEV.COD_AUTO_PERTE = PN_COD_USUARIO
      WHERE
         SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
    
    /* SEGUIMIENTO BEGIN */
      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA1
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA1 := NULL;
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;

      ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO(
         PN_COD_SOL_EVA,/* NUMBER*/
         V_ESTADO_SOL_EVA1,
         PV_FECHA_ESTADO,
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL DE USUARIO*/
         PN_COD_USUARIO,/* CODIGO DE USUARIO*/
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         PN_COD_USUARIO, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         V_COD_RESULTADO_SEG,/* NUMBER*/
         V_MSG_RESULTADO_SEG
      ); /* VARCHAR*/

      IF V_COD_RESULTADO_SEG <> 0 THEN
         RAISE V_COD_RESULTADO; /* EXCEPTION*/
      END IF;            
      /* SEGUIMIENTO END*/
      COMMIT;
   EXCEPTION
      WHEN V_COD_RESULTADO THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR, AL INSERTAR LA TABLA SEGUIMIENTO ' 
                              || '[[ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO]]' 
                              || V_MSG_RESULTADO_SEG;
      WHEN V_EXC_COD_SEV_CON_PAC THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR, NO SE ENCONTRO EL CODIGO DE LA SOLICITUD DE EVALUACION';
      WHEN V_EXC_ESTADO_SOL_EVA THEN
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := 'ERROR, NO SE ENCONTRO EL ESTADO DE LA SOLICITUD DE EVALUACION';
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_I_CONDICION_BASAL]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_I_CONDICION_BASAL;

  /* AUTOR     : JAZABACHE*/
  /* CREADO    : 14/03/2019*/
  /* PROPOSITO : PRECARGAR ANALISIS Y CONCLUSION - PASO 5*/

   PROCEDURE SP_PFC_S_ANALISIS_CONCL_CONSUL (
      PN_COD_SOL_EVA          IN                      NUMBER,
      PN_COD_MAC              IN                      NUMBER,
      PN_COD_GRP_DIAG         IN                      NUMBER,
      PN_CUMPLE_PREF_INST     OUT                     NUMBER,
      PV_DESCRIPCION_MAC      OUT                     VARCHAR2,
      TC_CUMPLE_TRATAMIENTO   OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_CUMPLE_CHKLIST_PER   OUT                     NUMBER,
      PV_DESCRIPCION_INDI     OUT                     VARCHAR2,
      TC_ANALISIS_CONCL       OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   ) AS

      V_PREF_INST_GRABAR     VARCHAR2(1);
      V_CHKLIST_PAC_GRABAR   VARCHAR2(1);
      V_COD_PREF_INST        NUMBER;
      V_CONDICION            NUMBER;
      V_MENSAJE              VARCHAR2(200);
      V_EXC_VARIABLE EXCEPTION;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF PN_COD_SOL_EVA IS NULL THEN
         V_MENSAJE := 'NO SE ENCUENTRA EL CODIGO DE LA SOLICITUD DE EVALUACION';
         RAISE V_EXC_VARIABLE;
      END IF;
      BEGIN
         SELECT
            SLT.P_CONDICION,
            SLT.P_CUMPLE_PREF_INST,
            SLT.GRABAR,
            SLT.DESCRIPCION
         INTO
            V_CONDICION,
            PN_CUMPLE_PREF_INST,
            V_PREF_INST_GRABAR,
            PV_DESCRIPCION_MAC
         FROM PFC_MEDICAMENTO_NUEVO MNU
        INNER JOIN PFC_SEV_LINEA_TRATAMIENTO SLT
           ON MNU.COD_SEV_LIN_TRA = SLT.COD_SEV_LIN_TRA
        WHERE MNU.COD_SOL_EVA = PN_COD_SOL_EVA;
            
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            PN_CUMPLE_PREF_INST   := NULL;
            V_PREF_INST_GRABAR    := NULL;
      END;

      BEGIN
         OPEN TC_CUMPLE_TRATAMIENTO FOR
              SELECT PI.COD_MAC,
                     (SELECT M.DESCRIPCION FROM PFC_MAC M WHERE M.COD_MAC = PI.COD_MAC) AS DESCRIPCION_MAC,
                     (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = PI.P_TIPO_TRATAMIENTO) AS TIPO_TRATAMIENTO
                FROM PFC_PREFERENCIA_INST PI
               WHERE PI.P_CONDICION_CANCER = V_CONDICION
                 AND PI.COD_GRP_DIAG = PN_COD_GRP_DIAG;
                 
      EXCEPTION
         WHEN OTHERS THEN
            V_MENSAJE := 'ERROR, NO SE PUDO OBTENER EL TRATAMIENTO';
            RAISE V_EXC_VARIABLE;
      END;
      
      IF V_PREF_INST_GRABAR = '0' THEN
         V_MENSAJE := 'NO SE GRABO EN LINEA DE TRATAMIENTO/ PREFERENCIAS INSTITUCIONALES - PASO 1';
         RAISE V_EXC_VARIABLE;
      END IF;
      
      BEGIN
         SELECT
            SCP.P_CUMPLE_CHKLIST_PER,
            SCP.GRABAR,
            (SELECT IND.DESCRIPCION
               FROM PFC_CHKLIST_INDICACION IND
              WHERE IND.COD_CHKLIST_INDI = SCP.COD_CHKLIST_INDI
                AND IND.COD_MAC       = PN_COD_MAC
                AND IND.COD_GRP_DIAG  = PN_COD_GRP_DIAG
            ) AS DESCRIPCION
         INTO
            PN_CUMPLE_CHKLIST_PER,
            V_CHKLIST_PAC_GRABAR,
            PV_DESCRIPCION_INDI
         FROM
            PFC_MEDICAMENTO_NUEVO   MNU
            INNER JOIN PFC_SEV_CHECKLIST_PAC   SCP ON SCP.COD_SEV_CHKLIST_PAC = MNU.COD_SEV_CHK_PAC
         WHERE
            MNU.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            PN_CUMPLE_CHKLIST_PER   := NULL;
            V_CHKLIST_PAC_GRABAR    := NULL;
         WHEN OTHERS THEN
            V_MENSAJE := 'SE ENCONTRARON MAS DE UN REGISTRO DE CHECKLIST PACIENTE PARA EL CODIGO MAC : '
                         || PN_COD_MAC
                         || ' Y CODIGO DE GRUPO DE DIAGNOSTICO : '
                         || PN_COD_GRP_DIAG;
            RAISE V_EXC_VARIABLE;
      END;

      IF V_CHKLIST_PAC_GRABAR = '0' THEN
         V_MENSAJE := 'NO SE GRABO EN CHECKLIST DEL PACIENTE - PASO 4';
         RAISE V_EXC_VARIABLE;
      END IF;
      OPEN TC_ANALISIS_CONCL FOR
           SELECT
                AC.COD_SEV_ANA_CON,
                AC.P_CUMPLE_CHKLIST_PER,
                AC.P_CUMPLE_PREF_INSTI,
                AC.COD_MAC,
                (SELECT M.DESCRIPCION FROM PFC_MAC M WHERE M.COD_MAC = AC.COD_MAC) AS MEDICAMENTO_PREFERIDO,
                AC.P_PERTINENCIA,
                AC.P_CONDICION_PACIENTE,
                AC.P_TIEMPO_USO,
                AC.GRABAR
             FROM
                PFC_MEDICAMENTO_NUEVO         MN
                INNER JOIN PFC_SEV_ANALISIS_CONCLUSION   AC ON MN.COD_SEV_ANA_CON = AC.COD_SEV_ANA_CON
             WHERE
                MN.COD_SOL_EVA = PN_COD_SOL_EVA;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
   EXCEPTION
      WHEN V_EXC_VARIABLE THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_ANALISIS_CONCLUSION]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_ANALISIS_CONCL_CONSUL;
   

   -- AUTOR     : JAZABACHE
   -- CREADO    : 25/01/2019
   -- PROPOSITO : GUARDAR CAMBIOS ANALISIS CONCLUSION PASO 5

   PROCEDURE SP_PFC_SIU_ANALISIS_CONCLUSION (
      PN_COD_REPORTE             IN                         NUMBER,
      PN_COD_SOL_EVA             IN                         NUMBER,
      PN_CUMPLE_CHKLIST_PER      IN                         NUMBER,
      PN_CUMPLE_PREF_INSTI       IN                         NUMBER,
      PN_COD_MAC                 IN                         NUMBER,
      PN_PERTINENCIA             IN                         NUMBER,
      PN_CONDICION_PAC           IN                         NUMBER,
      PN_TIEMPO_USO              IN                         NUMBER,
      PN_RESULTADO_AUTORIZADOR   IN                         NUMBER,
      PV_COMENTARIO              IN                         VARCHAR2,
      PN_FLAG_GRABAR             IN                         NUMBER,
      PN_COD_ROL_USUARIO         IN                         NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO            IN                         VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO             IN                         NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_RESULTADO           OUT                        NUMBER,
      PV_MSG_RESULTADO           OUT                        VARCHAR2
   ) AS
      V_COD_SEV_ANA_CON           NUMBER;
      V_GRABAR                    VARCHAR2(1);
      V_COD_SEV_ANA_CON_PAC_SEQ   NUMBER;
      V_ESTADO_SOL_EVA            NUMBER;
      V_EXC_COD_SOL_EVA EXCEPTION;
      V_EXC_ESTADO_SOL_EVA EXCEPTION;
      V_COD_MONITOREO_SEQ         NUMBER;
      PV_COD_LARGO1               VARCHAR2(20);
      PN_COD_RESULTADO1           NUMBER;
      PV_MSG_RESULTADO1           VARCHAR2(200);
      V_COD_MONITOREO             NUMBER;
      V_FECHA_ESTADO              VARCHAR2(40);
      V_CMP_MEDICO                VARCHAR2(20);
      V_MEDICO_TRATANTE           VARCHAR2(200);
      V_COD_RESULTADO_SOLB        NUMBER;
      V_MSG_RESULTADO_SOLB        VARCHAR(500);
      V_COD_HIST_LINEA_TRAT_MAX   NUMBER;
      V_CODIGO_RESULTADO          NUMBER;
      V_MENSAJE                   VARCHAR2(200);
      V_COUNT_LIN_TRA             NUMBER;
      V_VALOR_PROX_MONIT          NUMBER;
      V_FECHA_PROX_MONIT          DATE;
      V_FECHA_APROBACION          DATE;
      V_NRO_LINEA_TRA             NUMBER;
      V_MAC_SOLICITADA            NUMBER;
      V_COD_AFI_PACIENTE          VARCHAR2(20);
      V_COD_SCG                   NUMBER;
      V_COD_GRP_DIAG              VARCHAR2(5);
      V_COD_ROL_LIDER_TUM         NUMBER;
      V_COD_USR_LIDER_TUM         NUMBER; 
      V_USR_LIDER_TUM             VARCHAR2(200);
      V_CMP_LIDER_TUMOR           NUMBER;
      V_COD_RESULTADO_LT          NUMBER;
      V_MSG_RESULTADO_LT          VARCHAR2(4000);
      V_EXC_LINEA_TRAT EXCEPTION;
      V_EXC_COD_MONITOREO EXCEPTION;
      V_COD_RESULTADO EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO_SEG         NUMBER; /* SEGUIMIENTO*/
      V_MSG_RESULTADO_SEG         VARCHAR2(4000); /* SEGUIMIENTO*/
      V_ESTADO_SOL_EVA1           NUMBER; /* SEGUIMIENTO*/
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      SELECT
         TO_CHAR(
            SYSDATE -(5 / 24),
            'DD/MM/YYYY HH24:MI:SS'
         )
      INTO V_FECHA_ESTADO
      FROM
         DUAL;

      IF PN_COD_SOL_EVA IS NULL THEN
         RAISE V_EXC_COD_SOL_EVA;
      END IF;
      IF PN_FLAG_GRABAR = 1 THEN
         UPDATE PFC_MEDICAMENTO_NUEVO
         SET
            COD_REPORTE = PN_COD_REPORTE
         WHERE
            COD_SOL_EVA = PN_COD_SOL_EVA;

      END IF;

      BEGIN
         SELECT
            M.COD_MONITOREO
         INTO V_COD_MONITOREO
         FROM
            PFC_MONITOREO M
         WHERE
            M.COD_SOL_EVA = PN_COD_SOL_EVA;

         RAISE V_EXC_COD_MONITOREO;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_COD_MONITOREO := NULL;
      END;

      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA := NULL;
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;

      BEGIN
         SELECT
            MNU.COD_SEV_ANA_CON,
            ANC.GRABAR
         INTO
            V_COD_SEV_ANA_CON,
            V_GRABAR
         FROM
            PFC_SEV_ANALISIS_CONCLUSION   ANC
            INNER JOIN PFC_MEDICAMENTO_NUEVO         MNU ON ANC.COD_SEV_ANA_CON = MNU.COD_SEV_ANA_CON
         WHERE
            MNU.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_GRABAR            := NULL;
            V_COD_SEV_ANA_CON   := NULL;
      END;

      IF V_COD_SEV_ANA_CON IS NOT NULL THEN
         IF V_GRABAR <> '1' THEN
            UPDATE PFC_SEV_ANALISIS_CONCLUSION SAC
            SET
               SAC.GRABAR = '1'
            WHERE
               SAC.COD_SEV_ANA_CON = V_COD_SEV_ANA_CON;

         END IF;

         UPDATE PFC_SEV_ANALISIS_CONCLUSION SAC
         SET
            SAC.P_CUMPLE_CHKLIST_PER = PN_CUMPLE_CHKLIST_PER,
            SAC.P_CUMPLE_PREF_INSTI = PN_CUMPLE_PREF_INSTI,
            SAC.COD_MAC = PN_COD_MAC,
            SAC.P_PERTINENCIA = PN_PERTINENCIA,
            SAC.P_CONDICION_PACIENTE = PN_CONDICION_PAC,
            SAC.P_TIEMPO_USO = PN_TIEMPO_USO,
            SAC.P_RESULTADO_AUTORIZADOR = PN_RESULTADO_AUTORIZADOR,
            SAC.COMENTARIO = PV_COMENTARIO,
            SAC.FEC_APROBACION = SYSDATE
         WHERE
            SAC.COD_SEV_ANA_CON = V_COD_SEV_ANA_CON;
            
         UPDATE PFC_SOLICITUD_EVALUACION SEV
         SET
            SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
            SEV.COD_AUTO_PERTE = PN_COD_USUARIO                
         WHERE SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
      ELSE
         SELECT
            SQ_PFC_SEV_ANALISIS_CONCLUSION.NEXTVAL
         INTO V_COD_SEV_ANA_CON_PAC_SEQ
         FROM
            DUAL;

         INSERT INTO PFC_SEV_ANALISIS_CONCLUSION (
            COD_SEV_ANA_CON,
            P_CUMPLE_CHKLIST_PER,
            P_CUMPLE_PREF_INSTI,
            COD_MAC,
            P_PERTINENCIA,
            P_CONDICION_PACIENTE,
            P_TIEMPO_USO,
            P_RESULTADO_AUTORIZADOR,
            COMENTARIO,
            GRABAR
         ) VALUES (
            V_COD_SEV_ANA_CON_PAC_SEQ,
            PN_CUMPLE_CHKLIST_PER,
            PN_CUMPLE_PREF_INSTI,
            PN_COD_MAC,
            PN_PERTINENCIA,
            PN_CONDICION_PAC,
            PN_TIEMPO_USO,
            PN_RESULTADO_AUTORIZADOR,
            PV_COMENTARIO,
            '1'
         );

         UPDATE PFC_MEDICAMENTO_NUEVO MNU
         SET
            MNU.COD_SEV_ANA_CON = V_COD_SEV_ANA_CON_PAC_SEQ
         WHERE
            MNU.COD_SOL_EVA = PN_COD_SOL_EVA;
      END IF;
    
     /* SEGUIMIENTO BEGIN */

      BEGIN
         SELECT SEV.P_ESTADO_SOL_EVA, SPR.COD_MAC, SPR.COD_SCG
           INTO V_ESTADO_SOL_EVA1, V_MAC_SOLICITADA, V_COD_SCG
           FROM PFC_SOLICITUD_EVALUACION SEV
          INNER JOIN PFC_SOLICITUD_PRELIMINAR SPR
             ON SEV.COD_SOL_PRE = SPR.COD_SOL_PRE
          WHERE SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA1 := NULL;
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;      

      IF PN_FLAG_GRABAR = 0 AND V_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_PASO4 THEN
         UPDATE PFC_SOLICITUD_EVALUACION SEV
         SET
            SEV.P_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_PASO5,
            SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
            SEV.COD_AUTO_PERTE = PN_COD_USUARIO
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      ELSIF PN_FLAG_GRABAR = 1 THEN
         SELECT SO.COD_GRP_DIAG
             INTO V_COD_GRP_DIAG
             FROM PFC_SOLICITUD_EVALUACION SEV
            INNER JOIN PFC_SOLICITUD_PRELIMINAR SPR
               ON SEV.COD_SOL_PRE = SPR.COD_SOL_PRE
            INNER JOIN PFC_SOLBEN SO
               ON SO.COD_SCG = SPR.COD_SCG
            WHERE SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
      
         UPDATE PFC_SOLICITUD_EVALUACION SEV
         SET         
            SEV.P_ESTADO_SOL_EVA = PN_RESULTADO_AUTORIZADOR,
            SEV.P_EVALUACION_AUTORIZADOR = PN_RESULTADO_AUTORIZADOR,
            SEV.FEC_EVALUACION_AUTORIZADOR = TO_DATE(V_FECHA_ESTADO,'DD/MM/YYYY HH24:MI:SS')
         WHERE SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
         COMMIT;
         
         PCK_PFC_UTIL.SP_PFC_S_CMP_MEDICO(
            PN_COD_SOL_EVA,
            V_CMP_MEDICO,
            V_MEDICO_TRATANTE,
            V_COD_RESULTADO_SOLB,
            V_MSG_RESULTADO_SOLB
         );
         
         IF PN_RESULTADO_AUTORIZADOR = PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ THEN
            UPDATE PFC_SOLICITUD_EVALUACION SEV
            SET
               --SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
               SEV.P_ROL_RESP_PENDIENTE_EVA = NULL,
               SEV.COD_AUTO_PERTE = PN_COD_USUARIO,
               SEV.FEC_FINALIZAR_ESTADO = TO_DATE(V_FECHA_ESTADO,'DD/MM/YYYY HH24:MI:SS')
            WHERE SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
            BEGIN
              SELECT S.COD_AFI_PACIENTE
                INTO V_COD_AFI_PACIENTE
                FROM PFC_SOLBEN S
               WHERE S.COD_SCG = V_COD_SCG;
            EXCEPTION
              WHEN OTHERS THEN
                V_MENSAJE := 'ERROR AL OBTENER EL CODIGO DE AFILIADO DEL PACIENTE';
                V_CODIGO_RESULTADO := 6;
            END;
            SELECT SQ_PFC_MONITOREO_COD_MONITOREO.NEXTVAL
              INTO V_COD_MONITOREO_SEQ
              FROM DUAL;

            ONTPFC.PCK_PFC_UTIL.SP_PFC_CODIGO_LARGO(
               PN_COD_SOL_EVA,
               PCK_PFC_CONSTANTE.PN_COD_LARGO_MONITOREO,
               PV_COD_LARGO1,
               PN_COD_RESULTADO1,
               PV_MSG_RESULTADO1
            );

            SELECT TO_NUMBER(P.VALOR1)
              INTO V_VALOR_PROX_MONIT
              FROM PFC_PARAMETRO P
            WHERE P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_CODIGO_PROX_MONIT;

            SELECT
               TO_DATE(TO_CHAR(TO_DATE(PV_FECHA_ESTADO,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY'),'DD/MM/YYYY') + V_VALOR_PROX_MONIT,
               TO_DATE(TO_CHAR(TO_DATE(PV_FECHA_ESTADO,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY'),'DD/MM/YYYY')
            INTO
               V_FECHA_PROX_MONIT,
               V_FECHA_APROBACION
            FROM
               DUAL;

            INSERT INTO PFC_MONITOREO (
               COD_MONITOREO,
               COD_DESC_MONITOREO,
               COD_SOL_EVA,
               P_ESTADO_MONITOREO,
               FEC_PROX_MONITOREO
            ) VALUES (
               V_COD_MONITOREO_SEQ,
               PV_COD_LARGO1
               || '-'
               || PCK_PFC_CONSTANTE.PV_INCIO_TAREA_MONIT,
               PN_COD_SOL_EVA,
               PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_MONITOREO,
               V_FECHA_PROX_MONIT
            );
            
            SELECT COUNT(1)
              INTO V_COUNT_LIN_TRA
              FROM PFC_HIST_LINEA_TRATAMIENTO LT
            WHERE LT.COD_SOL_EVA = PN_COD_SOL_EVA;

            IF V_COD_RESULTADO_SOLB = 0 AND V_COUNT_LIN_TRA = 0 THEN               
               SELECT SQ_PFC_HIS_LINE_TRAT_COD_HIS.NEXTVAL
                 INTO V_COD_HIST_LINEA_TRAT_MAX
               FROM DUAL;
               
               BEGIN
                  SELECT MN.NRO_LINEA_TRA
                    INTO V_NRO_LINEA_TRA
                    FROM PFC_MEDICAMENTO_NUEVO MN
                   WHERE MN.COD_SOL_EVA = PN_COD_SOL_EVA;
                   
               EXCEPTION
                  WHEN OTHERS THEN
                     V_NRO_LINEA_TRA := 1;
               END;
               
               BEGIN
                  INSERT INTO PFC_HIST_LINEA_TRATAMIENTO (
                     COD_HIST_LINEA_TRAT,
                     COD_SOL_EVA,
                     COD_MAC,
                     LINEA_TRAT,
                     FEC_APROBACION,
                     FEC_INICIO,
                     P_ESTADO,
                     CMP,
                     MEDICO_TRATANTE,
                     COD_AFILIADO,
                     FEC_EMISION,
                     COD_AUDITOR_EVAL
                  ) VALUES (
                     V_COD_HIST_LINEA_TRAT_MAX,
                     PN_COD_SOL_EVA,
                     V_MAC_SOLICITADA,
                     V_NRO_LINEA_TRA,
                     V_FECHA_APROBACION,
                     V_FECHA_APROBACION,
                     PCK_PFC_CONSTANTE.PN_ESTADO_LIN_TRA_ACTIVO,
                     V_CMP_MEDICO,
                     V_MEDICO_TRATANTE,
                     V_COD_AFI_PACIENTE,
                     TO_DATE(V_FECHA_ESTADO, 'DD/MM/YYYY  HH24:MI:SS'),
                     PN_COD_USUARIO
                  );

               EXCEPTION
                  WHEN OTHERS THEN
                     V_CODIGO_RESULTADO   := 5;
                     V_MENSAJE            := 'NO SE CREO LA NUEVA LINEA DE TRATAMIENTO';
                     RAISE V_EXC_LINEA_TRAT;
               END;

            ELSE
               V_CODIGO_RESULTADO   := 5;
               V_MENSAJE            := 'NO SE PUDIERON OBTENER LOS DATOS DE SOLBEN FARMACIA';
               RAISE V_EXC_LINEA_TRAT;
            END IF;

         ELSIF PN_RESULTADO_AUTORIZADOR = PCK_PFC_CONSTANTE.PN_ESTADO_OBSERVADO_AUTORIZ THEN
         
            PCK_PFC_UTIL.SP_PFC_S_VALID_LIDER_TUMOR(
               V_CMP_MEDICO,
               V_COD_GRP_DIAG,
               V_COD_ROL_LIDER_TUM,
               V_COD_USR_LIDER_TUM,
               V_USR_LIDER_TUM,
               V_CMP_LIDER_TUMOR,
               V_COD_RESULTADO_LT,
               V_MSG_RESULTADO_LT
            );
            
            IF V_COD_RESULTADO_LT = 0 THEN
               UPDATE PFC_SOLICITUD_EVALUACION SEV
               SET
                  SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_CMAC,
                  SEV.COD_AUTO_PERTE = PN_COD_USUARIO
               WHERE SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
            ELSE
               UPDATE PFC_SOLICITUD_EVALUACION SEV
               SET
                  SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_LID_TUMOR,
                  SEV.COD_AUTO_PERTE = PN_COD_USUARIO,
                  SEV.COD_LIDER_TUMOR = V_COD_USR_LIDER_TUM
               WHERE SEV.COD_SOL_EVA = PN_COD_SOL_EVA;            
            END IF;
         ELSIF PN_RESULTADO_AUTORIZADOR = PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ THEN             
            UPDATE PFC_SOLICITUD_EVALUACION SEV
            SET
               --SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
               SEV.P_ROL_RESP_PENDIENTE_EVA = NULL,
               SEV.COD_AUTO_PERTE = PN_COD_USUARIO,
               SEV.FEC_FINALIZAR_ESTADO = TO_DATE(V_FECHA_ESTADO,'DD/MM/YYYY HH24:MI:SS')
            WHERE SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
         
         END IF;
         
      END IF;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_INSERTAR_EXITO;
      
      ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO(
         PN_COD_SOL_EVA,/* NUMBER*/
         V_ESTADO_SOL_EVA1,
         V_FECHA_ESTADO,
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL DE USUARIO*/
         PN_COD_USUARIO,/* CODIGO DE USUARIO*/
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         PN_COD_USUARIO, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         V_COD_RESULTADO_SEG,/* NUMBER*/
         V_MSG_RESULTADO_SEG
      ); /* VARCHAR*/

      IF V_COD_RESULTADO_SEG <> 0 THEN
         RAISE V_COD_RESULTADO; /* EXCEPTION*/
      END IF;            
      /* SEGUIMIENTO END    */
      COMMIT;
   EXCEPTION
      WHEN V_COD_RESULTADO THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR, AL INSERTAR LA TABLA SEGUIMIENTO '
                           || '[[ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO]]' 
                           || V_MSG_RESULTADO_SEG;
      WHEN V_EXC_COD_SOL_EVA THEN
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := 'NO SE ENCUENTRA EL CODIGO DE LA SOLICITUD DE EVALUACION';
         
      WHEN V_EXC_ESTADO_SOL_EVA THEN
         PN_COD_RESULTADO   := 3;
         PV_MSG_RESULTADO   := 'ERROR, NO SE ENCONTRO EL ESTADO DE LA SOLICITUD DE EVALUACION';
         
      WHEN V_EXC_COD_MONITOREO THEN
         PN_COD_RESULTADO   := 4;
         PV_MSG_RESULTADO   := 'ERROR, YA SE ENCUENTRA REGISTRADA UNA TAREA DE MONITOREO CON ESTA EVALUACION';
         
      WHEN V_EXC_LINEA_TRAT THEN
         ROLLBACK;
         PN_COD_RESULTADO   := V_CODIGO_RESULTADO;
         PV_MSG_RESULTADO   := V_MENSAJE;
         
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SIU_ANALISIS_CONCLUSION]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM
                             || 'LINEA : '
                             || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;

   END SP_PFC_SIU_ANALISIS_CONCLUSION;


   -- AUTHOR  : JAZABACHE
   -- CREATED : 22/03/2019
   -- PURPOSE : REGISTRAR MEDICAMENTO CONTINUADOR

   PROCEDURE SP_PFC_SIU_REGIST_CONTINUADOR (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PN_RESUL_AUTOR       IN                   NUMBER,
      PN_COD_RESPO_MONIT   IN                   NUMBER,
      PV_COMENTARIO        IN                   VARCHAR2,
      PN_COD_RESUL_MONIT   IN                   NUMBER,
      PV_NRO_TAREA_MONIT   IN                   VARCHAR2,
      PV_FEC_MONITOREO     IN                   VARCHAR2,
      PN_COD_ROL_USUARIO   IN                   NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO      IN                   VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO       IN                   NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_ROL_MON       IN                   NUMBER,/* CODIGO DE ROL MONITOREO*/
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   ) AS

      V_TIPO_DOCUMENTO          NUMBER;
      V_DESCRIPCION_DOCUMENTO   VARCHAR2(100);
      V_ESTADO                  NUMBER;
      V_COD_CONTINUADOR         NUMBER;
      V_COD_CONTINUADOR_SEQ     NUMBER;
      V_MENSAJE                 VARCHAR2(200);
      V_EXC_ERROR               EXCEPTION;
      V_EXC_ESTADO_SOL_EVA      EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO           EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO_SEG       NUMBER; /* SEGUIMIENTO*/
      V_MSG_RESULTADO_SEG       VARCHAR2(4000); /* SEGUIMIENTO*/
      V_ESTADO_SOL_EVA1         NUMBER; /* SEGUIMIENTO*/
    

   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      DBMS_OUTPUT.PUT_LINE('INICIO >> ');
      DBMS_OUTPUT.PUT_LINE('DSDSDSD >> ');
      
      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA1
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA1 := NULL;
            V_MENSAJE := 'ERROR, NO SE ENCONTRO LA SOLICITUD DE EVALUACION';
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;
      
      BEGIN
         SELECT
            CON.COD_CONTINUADOR
         INTO V_COD_CONTINUADOR
         FROM
            PFC_SOLICITUD_EVALUACION   SEV
            INNER JOIN PFC_CONTINUADOR  CON ON SEV.COD_SOL_EVA = CON.COD_SOL_EVA
         WHERE
            CON.COD_SOL_EVA = PN_COD_SOL_EVA;

         DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR => ' || V_COD_CONTINUADOR);
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
           V_COD_CONTINUADOR := NULL;
           V_MENSAJE := 'SE PROCEDE A CREAR EL REGISTRO DEL MEDICAMENTO CONTINUADOR';
         WHEN OTHERS THEN
           V_COD_CONTINUADOR := NULL;
           V_MENSAJE := '[[SP_PFC_SIU_REGIST_CONTINUADOR]] '|| TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
           RAISE V_EXC_ERROR;
      END;

      CASE
         
         WHEN V_COD_CONTINUADOR IS NOT NULL THEN
            DBMS_OUTPUT.PUT_LINE('CASE WHEN V_COD_CONTINUADOR => ' || V_COD_CONTINUADOR);
            BEGIN
              
               UPDATE ONTPFC.PFC_CONTINUADOR
               SET
                  P_RESULTADO_AUTORIZADOR = PN_RESUL_AUTOR,
                  COD_RESPONSABLE_MONITOREO = PN_COD_RESPO_MONIT,
                  COMENTARIO = PV_COMENTARIO,
                  P_RESULTADO_MONITOREO = PN_COD_RESUL_MONIT,
                  NRO_TAREA_MONITOREO = PV_NRO_TAREA_MONIT,
                  FEC_MONITOREO = TO_DATE(PV_FEC_MONITOREO,'DD/MM/YYYY'),
                  GRABAR = '1'
               WHERE
                  COD_CONTINUADOR = V_COD_CONTINUADOR;

            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  
                  DBMS_OUTPUT.PUT_LINE('NO_DATA_FOUND  PFC_CONTINUADOR_DOCUMENTO');
            END;

            IF NULLIF(PN_RESUL_AUTOR,'') IS NULL THEN
              UPDATE ONTPFC.PFC_SOLICITUD_EVALUACION SE
                 SET SE.P_ESTADO_SOL_EVA = 14,
                     SE.FEC_FINALIZAR_ESTADO = SYSDATE,
                     SE.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE
               WHERE SE.COD_SOL_EVA = PN_COD_SOL_EVA;
            ELSE
              UPDATE ONTPFC.PFC_SOLICITUD_EVALUACION SE
                 SET SE.P_ESTADO_SOL_EVA = PN_RESUL_AUTOR,
                     SE.FEC_FINALIZAR_ESTADO = SYSDATE,
                     SE.P_ROL_RESP_PENDIENTE_EVA = NULL
               WHERE SE.COD_SOL_EVA = PN_COD_SOL_EVA;
            END IF;
            

         WHEN V_COD_CONTINUADOR IS NULL THEN
           
            SELECT SQ_PFC_CONTINUADOR_COD_CONT.NEXTVAL
              INTO V_COD_CONTINUADOR_SEQ
              FROM DUAL;
             
            INSERT INTO ONTPFC.PFC_CONTINUADOR (
               COD_CONTINUADOR,
               COD_SOL_EVA,
               P_RESULTADO_AUTORIZADOR,
               COD_RESPONSABLE_MONITOREO,
               COMENTARIO,
               P_RESULTADO_MONITOREO,
               NRO_TAREA_MONITOREO,
               FEC_MONITOREO,
               GRABAR
            ) VALUES (
               V_COD_CONTINUADOR_SEQ,
               PN_COD_SOL_EVA,
               PN_RESUL_AUTOR,
               PN_COD_RESPO_MONIT,
               PV_COMENTARIO,
               PN_COD_RESUL_MONIT,
               PV_NRO_TAREA_MONIT,
               TO_DATE(PV_FEC_MONITOREO,'DD/MM/YYYY'),
               '1'
            );
            
            IF NULLIF(PN_RESUL_AUTOR,'') IS NULL THEN
              UPDATE ONTPFC.PFC_SOLICITUD_EVALUACION SE
                 SET SE.P_ESTADO_SOL_EVA = 14,
                     SE.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
                     SE.COD_AUTO_PERTE = PN_COD_USUARIO,
                     SE.FEC_FINALIZAR_ESTADO = SYSDATE
               WHERE SE.COD_SOL_EVA = PN_COD_SOL_EVA;
            ELSE
              UPDATE ONTPFC.PFC_SOLICITUD_EVALUACION SE
                 SET SE.P_ESTADO_SOL_EVA = PN_RESUL_AUTOR,
                     SE.P_ROL_RESP_PENDIENTE_EVA = NULL,
                     SE.COD_AUTO_PERTE = PN_COD_USUARIO,
                     SE.FEC_FINALIZAR_ESTADO = SYSDATE
               WHERE SE.COD_SOL_EVA = PN_COD_SOL_EVA;
            END IF;

      END CASE;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'SE REGISTRARON CON EXITO';

 /* SEGUIMIENTO BEGIN */

      ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO(
         PN_COD_SOL_EVA,/* NUMBER*/
         V_ESTADO_SOL_EVA1,
         PV_FECHA_ESTADO,
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL DE USUARIO*/
         PN_COD_USUARIO,/* CODIGO DE USUARIO*/
         PN_COD_ROL_MON, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         PN_COD_RESPO_MONIT, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         V_COD_RESULTADO_SEG,/* NUMBER*/
         V_MSG_RESULTADO_SEG
      ); /* VARCHAR*/

      IF V_COD_RESULTADO_SEG <> 0 THEN
         RAISE V_COD_RESULTADO; /* EXCEPTION*/
      END IF;            
      /* SEGUIMIENTO END */
      COMMIT;
   EXCEPTION
      WHEN V_COD_RESULTADO THEN
         ROLLBACK;
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR, AL INSERTAR LA TABLA SEGUIMIENTO '
                              || '[[ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO]]' 
                              || V_MSG_RESULTADO_SEG;
      WHEN V_EXC_ESTADO_SOL_EVA THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR, NO SE ENCONTRO EL ESTADO DE LA SOLICITUD DE EVALUACION';
      WHEN V_EXC_ERROR THEN
         PN_COD_RESULTADO   := -2;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SIU_REGIST_CONTINUADOR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SIU_REGIST_CONTINUADOR;
   
   
  /* AUTOR     : JAZABACHE*/
  /* CREADO    : 25/01/2019*/
  /* PROPOSITO : PRECARGA MEDICAMENTO CONTINUADOR ELIMINAR SP*/

   PROCEDURE SP_PFC_SU_CONTINUADOR_DOCUMENT (
      PN_COD_SOL_EVA               IN                           NUMBER,
      PN_TIPO_MEDICAMENTO          IN                           NUMBER,
      PV_DESCRIPCION_MEDICAMENTO   IN                           VARCHAR2,
      PN_ESTADO                    IN                           NUMBER,
      PN_COD_RESULTADO             OUT                          NUMBER,
      PV_MSG_RESULTADO             OUT                          VARCHAR2
   ) AS
      V_COD_CONTINUADOR   NUMBER;
      V_ESTADO            NUMBER;
      V_COD_CONTIN        NUMBER;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      CASE
         WHEN PN_TIPO_MEDICAMENTO <> PCK_PFC_CONSTANTE.PN_CHKLIST_TIPO_DOC_OTROS THEN
            BEGIN
               SELECT
                  CO.COD_CONTINUADOR
               INTO V_COD_CONTINUADOR
               FROM
                  PFC_CONTINUADOR             CO
                  INNER JOIN PFC_CONTINUADOR_DOCUMENTO   CD ON CO.COD_CONTINUADOR = CD.COD_CONTINUADOR
               WHERE
                  CO.COD_SOL_EVA = PN_COD_SOL_EVA
                  AND CD.P_TIPO_DOCUMENTO = PN_TIPO_MEDICAMENTO;

               DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR XXXX => ' || V_COD_CONTINUADOR);
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  V_COD_CONTINUADOR := NULL;
                  DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR IS NULL');
            END;

            CASE
               WHEN V_COD_CONTINUADOR IS NOT NULL AND PN_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR THEN
                  DBMS_OUTPUT.PUT_LINE('PN_ESTADO => ' || PN_ESTADO);
                  DBMS_OUTPUT.PUT_LINE('P_TIPO_DOCUMENTO  => ' || PN_TIPO_MEDICAMENTO);
                  DBMS_OUTPUT.PUT_LINE('COD_CONTINUADOR  => ' || V_COD_CONTINUADOR);
                  UPDATE PFC_CONTINUADOR_DOCUMENTO PCD
                  SET
                     PCD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR,
                     PCD.DESCRIPCION_DOCUMENTO = PV_DESCRIPCION_MEDICAMENTO
                  WHERE
                     PCD.COD_CONTINUADOR = V_COD_CONTINUADOR
                     AND PCD.P_TIPO_DOCUMENTO = PN_TIPO_MEDICAMENTO;

                  DBMS_OUTPUT.PUT_LINE('OK  => ');
               WHEN V_COD_CONTINUADOR IS NOT NULL AND PN_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR THEN
                  UPDATE PFC_CONTINUADOR_DOCUMENTO PCD
                  SET
                     PCD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR,
                     PCD.DESCRIPCION_DOCUMENTO = NULL
                  WHERE
                     PCD.COD_CONTINUADOR = V_COD_CONTINUADOR
                     AND PCD.P_TIPO_DOCUMENTO       = PN_TIPO_MEDICAMENTO
                     AND PCD.DESCRIPCION_DOCUMENTO  = PV_DESCRIPCION_MEDICAMENTO;

               ELSE
                  PN_COD_RESULTADO   := 0;
                  PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_SIN_RESULTADO;
            END CASE;

            PN_COD_RESULTADO   := 1;
            PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
         WHEN PN_TIPO_MEDICAMENTO = PCK_PFC_CONSTANTE.PN_CHKLIST_TIPO_DOC_OTROS THEN
            DBMS_OUTPUT.PUT_LINE('TIPO DE DOCUMENTO OTROS');
            BEGIN
               SELECT
                  CO.COD_CONTINUADOR,
                  P_ESTADO
               INTO
                  V_COD_CONTINUADOR,
                  V_ESTADO
               FROM
                  PFC_CONTINUADOR             CO
                  INNER JOIN PFC_CONTINUADOR_DOCUMENTO   CD ON CO.COD_CONTINUADOR = CD.COD_CONTINUADOR
               WHERE
                  CO.COD_SOL_EVA = PN_COD_SOL_EVA
                  AND CD.DESCRIPCION_DOCUMENTO  = PV_DESCRIPCION_MEDICAMENTO
                  AND CD.P_TIPO_DOCUMENTO       = PN_TIPO_MEDICAMENTO;

            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  V_COD_CONTINUADOR := NULL;
                  DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR ' || V_COD_CONTINUADOR);
            END;

            CASE
               WHEN V_COD_CONTINUADOR IS NOT NULL THEN
                  IF PN_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR AND V_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR
                  THEN

              /*UPDATE PFC_CONTINUADOR_DOCUMENTO PCD
              SET PCD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR,
              PCD.DESCRIPCION_DOCUMENTO = NULL
              WHERE PCD.COD_CONTINUADOR = V_COD_CONTINUADOR
              AND PCD.P_TIPO_DOCUMENTO = PN_TIPO_MEDICAMENTO
              AND PCD.DESCRIPCION_DOCUMENTO = PV_DESCRIPCION_MEDICAMENTO;*/
                     DELETE PFC_CONTINUADOR_DOCUMENTO PCD
                     WHERE
                        PCD.COD_CONTINUADOR = V_COD_CONTINUADOR
                        AND PCD.P_TIPO_DOCUMENTO       = PN_TIPO_MEDICAMENTO
                        AND PCD.DESCRIPCION_DOCUMENTO  = PV_DESCRIPCION_MEDICAMENTO;

                     PN_COD_RESULTADO   := 1;
                     PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ELIMINAR_EXITO;
                  ELSIF PN_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR AND V_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                  THEN
                     PN_COD_RESULTADO   := 0;
                     PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_SIN_RESULTADO;
                  END IF;
               ELSE
                  DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR ' || V_COD_CONTINUADOR);
                  BEGIN
                     SELECT
                        CO.COD_CONTINUADOR
                     INTO V_COD_CONTIN
                     FROM
                        PFC_CONTINUADOR CO
                     WHERE
                        CO.COD_SOL_EVA = PN_COD_SOL_EVA;

                     DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR ' || V_COD_CONTINUADOR);
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        V_COD_CONTINUADOR := NULL;
                  END;

                  IF V_COD_CONTIN IS NOT NULL AND PV_DESCRIPCION_MEDICAMENTO IS NOT NULL AND PN_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR

                  THEN
                     DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR 111');
                     DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR 111 > '
                                          || V_COD_CONTINUADOR
                                          || ' : '
                                          || PN_TIPO_MEDICAMENTO
                                          || ' : '
                                          || PV_DESCRIPCION_MEDICAMENTO
                                          || ' : '
                                          || PN_ESTADO);

                     INSERT INTO PFC_CONTINUADOR_DOCUMENTO (
                        COD_CONTINUADOR,
                        P_TIPO_DOCUMENTO,
                        DESCRIPCION_DOCUMENTO,
                        P_ESTADO
                     ) VALUES (
                        V_COD_CONTIN,
                        PN_TIPO_MEDICAMENTO,
                        PV_DESCRIPCION_MEDICAMENTO,
                        PN_ESTADO
                     );

                     PN_COD_RESULTADO   := 1;
                     PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_INSERTAR_EXITO;
                     DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR 12345678');
                  ELSE
                     DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR 222');
                     PN_COD_RESULTADO   := 0;
                     PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_SIN_RESULTADO;
                  END IF;

            END CASE;

      END CASE;

      DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR 333');
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SU_CONTINUADOR_DOCUMENT]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SU_CONTINUADOR_DOCUMENT;
  
  /* AUTHOR  : JAZABACHE*/
  /* CREATED : 25/01/2019*/
  /* PURPOSE : PRECARGA MEDICAMENTO CONTINUADOR ORIGINAL*/

   PROCEDURE SP_PFC_SI_CONTINUADOR_CONSULTA (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PN_EDAD            IN                 NUMBER,
      AC_LISTA_DOC       OUT                PCK_PFC_CONSTANTE.TYPCUR,/*LISTAR TIPO DE DOCUMENTOS*/
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS

      V_EDAD                      NUMBER;
      V_COD_CONTINUADOR           NUMBER;
      V_COD_CONTINUADOR_SEQ       NUMBER;
      V_COD_CONTINUADOR_DOC_SEQ   NUMBER;
      V_MENSAJE                   VARCHAR2(100);
      V_DESCRIP_DOCUMENTO         VARCHAR(100);
      V_EXC_CAMPO_OBLIGATORIO EXCEPTION;
      CURSOR V_LIST_TIPO_DOC IS
      SELECT
         PR.COD_PARAMETRO,
         PR.NOMBRE,
         PR.VALOR1
      FROM
         PFC_GRUPO       GR,
         PFC_PARAMETRO   PR
      WHERE
         GR.COD_GRUPO = PR.COD_GRUPO
         AND GR.COD_GRUPO  = 30
         AND PR.COD_PARAMETRO <> 85
         AND GR.ESTADO     = '1'
         AND PR.ESTADO     = '1';

   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      CASE
         WHEN NULLIF(
            PN_COD_SOL_EVA,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CODIGO DE LA SOLICITUD DE EVALUACION OBLIGATORIO';
            RAISE V_EXC_CAMPO_OBLIGATORIO;
         WHEN NULLIF(
            PN_EDAD,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'LA EDAD ES UN DATO OBLIGATORIO A INGRESAR';
            RAISE V_EXC_CAMPO_OBLIGATORIO;
         ELSE
            V_MENSAJE := NULL;
      END CASE;

      BEGIN
         SELECT
            CO.COD_CONTINUADOR
         INTO V_COD_CONTINUADOR
         FROM
            PFC_SOLICITUD_EVALUACION   SE
            INNER JOIN PFC_CONTINUADOR            CO ON SE.COD_SOL_EVA = CO.COD_SOL_EVA
         WHERE
            SE.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_COD_CONTINUADOR := NULL;
      END;

      IF V_COD_CONTINUADOR IS NOT NULL THEN
         DBMS_OUTPUT.PUT_LINE('V_COD_CONTINUADOR IS NOT NULL');
         OPEN AC_LISTA_DOC FOR SELECT
                                  CD.COD_CONTINUADOR_DOC,
                                  CD.P_TIPO_DOCUMENTO,
                                  (
                                     SELECT
                                        PP.NOMBRE
                                     FROM
                                        PFC_PARAMETRO PP
                                     WHERE
                                        PP.COD_PARAMETRO = CD.P_TIPO_DOCUMENTO
                                  ) AS NOMBRE_TIPO_DOCUMENTO,
                                  CD.DESCRIPCION_DOCUMENTO,
                                  CD.P_ESTADO,
                                  (
                                     SELECT
                                        P.NOMBRE
                                     FROM
                                        PFC_PARAMETRO P
                                     WHERE
                                        P.COD_PARAMETRO = CD.P_ESTADO
                                  ) AS DESCRIPCION_ESTADO
                               FROM
                                  PFC_CONTINUADOR_DOCUMENTO   CD
                                  INNER JOIN PFC_CONTINUADOR             CO ON CD.COD_CONTINUADOR = CO.COD_CONTINUADOR
                               WHERE
                                  CO.COD_SOL_EVA = PN_COD_SOL_EVA
                                  AND ( CD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR
                                        OR CD.P_ESTADO IS NULL
                                        OR ( ( CD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                               AND CD.P_TIPO_DOCUMENTO  = 84 )
                                             OR ( CD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                                  AND CD.P_TIPO_DOCUMENTO  = 85 )
                                             OR ( CD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                                  AND CD.P_TIPO_DOCUMENTO  = 86 ) ) );

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
      ELSE
         SELECT
            SQ_PFC_CONTINUADOR_COD_CONT.NEXTVAL
         INTO V_COD_CONTINUADOR_SEQ
         FROM
            DUAL;

         INSERT INTO PFC_CONTINUADOR (
            COD_CONTINUADOR,
            COD_SOL_EVA,
            GRABAR
         ) VALUES (
            V_COD_CONTINUADOR_SEQ,
            PN_COD_SOL_EVA,
            '0'
         );

         DBMS_OUTPUT.PUT_LINE('SE INSERTO EN PFC_CONTINUADOR : ' || V_COD_CONTINUADOR_SEQ);
         FOR V_ITEM IN V_LIST_TIPO_DOC LOOP
            V_EDAD := TO_NUMBER(V_ITEM.VALOR1);
            DBMS_OUTPUT.PUT_LINE('V_EDAD : '
                                 || V_EDAD
                                 || ' <<>> PN_EDAD : '
                                 || PN_EDAD);
            IF ( V_EDAD < PN_EDAD ) OR NULLIF(
               V_EDAD,
               ''
            ) IS NULL THEN
               SELECT
                  SQ_PFC_CONT_DOC_COD_CONT_DOC.NEXTVAL
               INTO V_COD_CONTINUADOR_DOC_SEQ
               FROM
                  DUAL;

               IF V_ITEM.COD_PARAMETRO = 87 THEN
                  V_DESCRIP_DOCUMENTO := NULL;
               ELSE
                  V_DESCRIP_DOCUMENTO := V_ITEM.NOMBRE;
               END IF;

               INSERT INTO PFC_CONTINUADOR_DOCUMENTO (
                  COD_CONTINUADOR_DOC,
                  COD_CONTINUADOR,
                  P_TIPO_DOCUMENTO,
                  DESCRIPCION_DOCUMENTO
               ) VALUES (
                  V_COD_CONTINUADOR_DOC_SEQ,
                  V_COD_CONTINUADOR_SEQ,
                  V_ITEM.COD_PARAMETRO,
                  V_DESCRIP_DOCUMENTO
               );

            END IF;

         END LOOP;

         COMMIT;
         OPEN AC_LISTA_DOC FOR SELECT
                                  CD.COD_CONTINUADOR_DOC,
                                  CD.P_TIPO_DOCUMENTO,
                                  (
                                     SELECT
                                        PP.NOMBRE
                                     FROM
                                        PFC_PARAMETRO PP
                                     WHERE
                                        PP.COD_PARAMETRO = CD.P_TIPO_DOCUMENTO
                                  ) AS NOMBRE_TIPO_DOCUMENTO,
                                  CD.DESCRIPCION_DOCUMENTO,
                                  CD.P_ESTADO,
                                  (
                                     SELECT
                                        P.NOMBRE
                                     FROM
                                        PFC_PARAMETRO P
                                     WHERE
                                        P.COD_PARAMETRO = CD.P_ESTADO
                                  ) AS DESCRIPCION_ESTADO
                               FROM
                                  PFC_CONTINUADOR_DOCUMENTO   CD
                                  INNER JOIN PFC_CONTINUADOR             CO ON CD.COD_CONTINUADOR = CO.COD_CONTINUADOR
                               WHERE
                                  CO.COD_SOL_EVA = PN_COD_SOL_EVA
                                  AND ( CD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR
                                        OR CD.P_ESTADO IS NULL
                                        OR ( ( CD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                               AND CD.P_TIPO_DOCUMENTO  = 84 )
                                             OR ( CD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                                  AND CD.P_TIPO_DOCUMENTO  = 85 )
                                             OR ( CD.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                                  AND CD.P_TIPO_DOCUMENTO  = 86 ) ) );

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'SE CREARON LOS REGISTROS.';
      END IF;

   EXCEPTION
      WHEN V_EXC_CAMPO_OBLIGATORIO THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SI_CONTINUADOR_CONSULTA]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SI_CONTINUADOR_CONSULTA;

 
  /* AUTOR     : JAZABACHE*/
  /* CREADO    : 25/01/2019*/
  /* PROPOSITO : CARGAR DOCUMENTO MEDICAMENTO CONTINUADOR*/

   PROCEDURE SP_PFC_SIU_CONTINUADOR_CARGAR (
      PN_COD_SOL_EVA           IN                       NUMBER,
      PN_COD_CONTINUADOR_DOC   IN                       NUMBER,
      PN_TIPO_DOCUMENTO        IN                       NUMBER,
      PV_DESCRIPCION_DOC       IN                       VARCHAR2,
      PN_ESTADO_DOC            IN                       NUMBER,
      PN_COD_ARCHIVO           IN                       NUMBER,
      PV_FECHA_ESTADO          IN                       VARCHAR2,
      PN_COD_RESULTADO         OUT                      NUMBER,
      PV_MSG_RESULTADO         OUT                      VARCHAR2
   ) AS
      V_COD_CONTINUADOR           NUMBER;
      V_COD_CONTINUADOR_DOC_SEQ   NUMBER;
      V_COD_CONTINUADOR_SEQ       NUMBER;
      V_COD_PARAMETRO_UPD         NUMBER;
      V_NOMBRE_UPD                VARCHAR2(100);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF NULLIF(
         PN_COD_CONTINUADOR_DOC,
         ''
      ) IS NULL THEN
         BEGIN
            SELECT
               CO.COD_CONTINUADOR
            INTO V_COD_CONTINUADOR
            FROM
               PFC_SOLICITUD_EVALUACION   SE
               INNER JOIN PFC_CONTINUADOR            CO ON SE.COD_SOL_EVA = CO.COD_SOL_EVA
            WHERE
               CO.COD_SOL_EVA = PN_COD_SOL_EVA;

         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               V_COD_CONTINUADOR := NULL;
         END;

         IF V_COD_CONTINUADOR IS NULL THEN
            SELECT
               SQ_PFC_CONTINUADOR_COD_CONT.NEXTVAL
            INTO V_COD_CONTINUADOR_SEQ
            FROM
               DUAL;

            INSERT INTO PFC_CONTINUADOR (
               COD_CONTINUADOR,
               GRABAR
            ) VALUES (
               V_COD_CONTINUADOR_SEQ,
               '0'
            );

            SELECT
               SQ_PFC_CONT_DOC_COD_CONT_DOC.NEXTVAL
            INTO V_COD_CONTINUADOR_DOC_SEQ
            FROM
               DUAL;

            INSERT INTO PFC_CONTINUADOR_DOCUMENTO (
               COD_CONTINUADOR_DOC,
               COD_CONTINUADOR,
               P_TIPO_DOCUMENTO,
               DESCRIPCION_DOCUMENTO,
               FEC_CARGA,
               P_ESTADO,
               COD_ARCHIVO
            ) VALUES (
               V_COD_CONTINUADOR_DOC_SEQ,
               V_COD_CONTINUADOR_SEQ,
               PN_TIPO_DOCUMENTO,
               PV_DESCRIPCION_DOC,
               PV_FECHA_ESTADO,
               PN_ESTADO_DOC,
               PN_COD_ARCHIVO
            );

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_INSERTAR_EXITO;
         ELSE
            DBMS_OUTPUT.PUT_LINE('--  2  --');
            DBMS_OUTPUT.PUT_LINE('COD_CONTINUADOR IS NOT NULL');
            SELECT
               SQ_PFC_CONT_DOC_COD_CONT_DOC.NEXTVAL
            INTO V_COD_CONTINUADOR_DOC_SEQ
            FROM
               DUAL;
            /* SOLO INSERTAMOS EL REGISTRO EN LA TABLA PFC_CONTINUADOR_DOCUMENTO*/

            INSERT INTO PFC_CONTINUADOR_DOCUMENTO (
               COD_CONTINUADOR_DOC,
               COD_CONTINUADOR,
               P_TIPO_DOCUMENTO,
               DESCRIPCION_DOCUMENTO,
               FEC_CARGA,
               P_ESTADO,
               COD_ARCHIVO
            ) VALUES (
               V_COD_CONTINUADOR_DOC_SEQ,
               V_COD_CONTINUADOR,
               PN_TIPO_DOCUMENTO,
               PV_DESCRIPCION_DOC,
               TO_DATE(
                  PV_FECHA_ESTADO,
                  'DD/MM/YYYY HH24:MI:SS'
               ),
               PN_ESTADO_DOC,
               PN_COD_ARCHIVO
            );

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_INSERTAR_EXITO;
         END IF;

      ELSE
         BEGIN
            SELECT
               P.COD_PARAMETRO,
               P.NOMBRE
            INTO
               V_COD_PARAMETRO_UPD,
               V_NOMBRE_UPD
            FROM
               PFC_PARAMETRO P
            WHERE
               P.COD_PARAMETRO = PN_TIPO_DOCUMENTO;

            IF PN_TIPO_DOCUMENTO = 87 THEN
               V_NOMBRE_UPD := PV_DESCRIPCION_DOC;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               V_COD_PARAMETRO_UPD   := NULL;
               V_NOMBRE_UPD          := NULL;
         END;

         IF NULLIF(
            V_COD_PARAMETRO_UPD,
            ''
         ) IS NULL THEN
            PN_COD_RESULTADO   := 1;
            PV_MSG_RESULTADO   := 'DATOS INGRESADOS INCORRECTO';
         ELSE
            DBMS_OUTPUT.PUT_LINE('UPDATE PFC_CONTINUADOR_DOCUMENTO CR');
            UPDATE PFC_CONTINUADOR_DOCUMENTO CD
            SET
               CD.P_TIPO_DOCUMENTO = PN_TIPO_DOCUMENTO,
               CD.DESCRIPCION_DOCUMENTO = V_NOMBRE_UPD,
               CD.FEC_CARGA = TO_DATE(
                  PV_FECHA_ESTADO,
                  'DD/MM/YYYY HH24:MI:SS'
               ),
               CD.P_ESTADO = PN_ESTADO_DOC
            WHERE
               CD.COD_CONTINUADOR_DOC = PN_COD_CONTINUADOR_DOC;

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
         END IF;

      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_SI_CONTINUADOR_CARGAR] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SIU_CONTINUADOR_CARGAR;
  
  /* AUTHOR  : JAZABACHE*/
  /* CREATED : 25/01/2019*/
  /* PURPOSE : ELIMINAR DOCUMENTO - MEDICAMENTO CONTINUADOR*/

   PROCEDURE SP_PFC_U_CONTINUADOR_DOC_ELIM (
      PN_COD_CONTINUADOR_DOC   IN                       NUMBER,
      PN_ESTADO_DOC            IN                       NUMBER,
      PN_COD_RESULTADO         OUT                      NUMBER,
      PV_MSG_RESULTADO         OUT                      VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF PN_ESTADO_DOC <> PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR THEN
         UPDATE PFC_CONTINUADOR_DOCUMENTO CD
         SET
            CD.P_ESTADO = PN_ESTADO_DOC,
            CD.ESTADO = '1'
         WHERE
            CD.COD_CONTINUADOR_DOC = PN_COD_CONTINUADOR_DOC;

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
      ELSE
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'El CODIGO DE ESTADO '
                             || PN_ESTADO_DOC
                             || ' ES INCORRECTO.';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_U_CONTINUADOR_DOC_ELIM][PFC_CONTINUADOR_DOCUMENTO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_U_CONTINUADOR_DOC_ELIM;
  
  /* AUTHOR  : CATALINA*/
  /* CREATED : 18/01/2019*/
  /* PURPOSE : Listar a los Responsable de Monitoreo según el grupo diagnóstico de la Solicitud de Evaluación*/

   PROCEDURE SP_PFC_S_ROL_PER_PART_GRDIAG (
      PN_COD_ROL         IN                 NUMBER,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) IS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';

    /*OPEN TC_LIST FOR
      SELECT us.cod_usuario, us.usuario
        FROM pfc_participante         p,
             -pfc_grupo_diagnostico    gd,
             ONTPPA.ppa_usuario us,
             ONTPPA.ppa_usr_rol ur
       WHERE p.cod_grp_diag = gd.cod_grp_diag
         AND us.cod_usuario = ur.cod_usuario
         AND p.cod_usuario = ur.cod_usuario
         AND p.cod_usuario = us.cod_usuario
         AND ur.estado = PCK_PFC_CONSTANTE.PV_STATUS_USER_ROL_ACTIVE
         AND us.estado = PCK_PFC_CONSTANTE.PV_STATUS_USER_ACTIVE
         AND p.estado_participante =
             PCK_PFC_CONSTANTE.PV_STATUS_PART_ACTIVE
         AND ur.cod_rol = PN_COD_ROL;*/
      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_ROL_PER_PART_GRDIAG]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_ROL_PER_PART_GRDIAG;

  /* AUTHOR  : CATALINA*/
  /* CREATED : 21/01/2019*/
  /* PURPOSE :*/

   PROCEDURE SP_PFC_I_CONDICION_BASAL_SAVE (
      PV_ANT_IMP         IN                 VARCHAR2,
      PN_LINEA_META      IN                 NUMBER,
      PN_LUGAR_META      IN                 NUMBER,
      PN_ECOG            IN                 NUMBER,
      PN_EXISTE_TOX      IN                 NUMBER,
      PN_TIPO_TOX        IN                 NUMBER,
      PN_GRABAR          IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      V_COD_SEV_CON_PAC_SEQ   NUMBER;
      V_COD_METASTASIS_SEQ    NUMBER;
    /*v_cod_sev_con_pac_count             NUMBER;*/
      V_COD_SEV_CON_PAC       NUMBER;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      SELECT
         SQ_PFC_COD_SEV_CON_PAC.NEXTVAL
      INTO V_COD_SEV_CON_PAC_SEQ
      FROM
         DUAL;

      SELECT
         SQ_PFC_COD_METASTASIS.NEXTVAL
      INTO V_COD_METASTASIS_SEQ
      FROM
         DUAL;

      INSERT INTO PFC_SEV_CONDICION_BASAL_PAC (
         COD_SEV_CON_PAC,
         ANTECEDENTES_IMP,
         P_ECOG,
         P_EXISTE_TOXICIDAD,
         P_TIPO_TOXICIDAD,
         GRABAR
      ) VALUES (
         V_COD_SEV_CON_PAC_SEQ,
         PV_ANT_IMP,
         PN_ECOG,
         PN_EXISTE_TOX,
         PN_TIPO_TOX,
         PN_GRABAR
      );

      SELECT
         MAX(COD_SEV_CON_PAC)
      INTO V_COD_SEV_CON_PAC
      FROM
         PFC_SEV_CONDICION_BASAL_PAC;

      INSERT INTO PFC_METASTASIS M (
         M.COD_METASTASIS,
         M.P_LINEA_METASTASIS,
         M.P_LUGAR_METASTASIS,
         M.COD_SEV_CON_PAC
      ) VALUES (
         V_COD_METASTASIS_SEQ,
         PN_LINEA_META,
         PN_LUGAR_META,
         V_COD_SEV_CON_PAC
      );

      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'Inserción exitosa: Se registró condición basal del paciente';
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_I_CONDICION_BASAL]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_I_CONDICION_BASAL_SAVE;

  /* AUTHOR  : CATALINA*/
  /* CREATED : 22/01/2019*/
  /* PURPOSE : Insertar registros en el maestro de Configuracion de Marcadores*/

   PROCEDURE SP_PFC_IU_CONFIGURACION_MARCA (
      PV_COD_MARCA_CONFIG_L   IN                      VARCHAR2,
      PN_ESTADO               IN                      NUMBER,
      PV_COD_MAC              IN                      NUMBER,
      PN_TIPO_MARCA           IN                      NUMBER,
      PN_COD_MARCA            IN                      NUMBER,
      PN_PERIOCIDAD_MIN       IN                      NUMBER,
      PN_PERIOCIDAD_MAX       IN                      NUMBER,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   ) AS
      V_COD_EXAMEN_MED_SEQ         NUMBER;
      V_COD_CONFIG_MARCA_L_COUNT   NUMBER;
    /*v_cod_config_marca_l   VARCHAR2(50);*/
    /*v_cod_mac_vch            VARCHAR2(50);*/
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      SELECT
         SQ_PFC_COD_EXAMEN_MED.NEXTVAL
      INTO V_COD_EXAMEN_MED_SEQ
      FROM
         DUAL;
    /*SELECT seq_cod_metastasis.NEXTVAL INTO v_cod_metastasis_seq FROM dual;*/

      SELECT
         COUNT(*)
      INTO V_COD_CONFIG_MARCA_L_COUNT
      FROM
         PFC_CONFIGURACION_MARCADOR CM
      WHERE
         CM.COD_CONFIG_MARCA_LARGO = PV_COD_MARCA_CONFIG_L;

      IF ( V_COD_CONFIG_MARCA_L_COUNT != 0 ) THEN
         INSERT INTO PFC_CONFIGURACION_MARCADOR (
            COD_CONFIG_MARCA,
            COD_EXAMEN_MED,
         /*GRUPO_DIAGNOSTICO,*/
            P_TIPO_MARCADOR,
            P_PER_MAXIMA,
            P_PER_MINIMA,
            P_ESTADO,
            COD_MAC,
            COD_CONFIG_MARCA_LARGO
         ) VALUES (
            V_COD_EXAMEN_MED_SEQ,
            PN_COD_MARCA,
         /*1,*/
            PN_TIPO_MARCA,
            PN_PERIOCIDAD_MAX,
            PN_PERIOCIDAD_MIN,
            PN_ESTADO,
            PV_COD_MAC,
            PV_COD_MARCA_CONFIG_L
         );

         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'Inserción exitosa: Se registró configuración de marcador';
      ELSE
         UPDATE PFC_CONFIGURACION_MARCADOR MC
         SET
            MC.COD_EXAMEN_MED = PN_COD_MARCA,
             /*mc.GRUPO_DIAGNOSTICO = 2,*/
            MC.P_TIPO_MARCADOR = PN_TIPO_MARCA,
            MC.P_PER_MAXIMA = PN_PERIOCIDAD_MAX,
            MC.P_PER_MINIMA = P_PER_MINIMA,
            MC.P_ESTADO = PN_ESTADO,
            MC.COD_MAC = PV_COD_MAC
         WHERE
            COD_CONFIG_MARCA = V_COD_CONFIG_MARCA_L_COUNT;

         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := 'Se actualizo el registró configuración de marcador';
      END IF;

      COMMIT;
    /*SELECT MAX(COD_SEV_CON_PAC) INTO v_cod_sev_con_pac FROM pfc_sev_condicion_basal_pac;*/

    /*SELECT COUNT(*) INTO v_cod_mac_count
    FROM PFC_MAC_MAESTRO m, PFC_CONFIGURACION_MARCADOR cm
    WHERE  cm.cod_mac = m.cod_mac;

    IF(v_cod_mac_count != 0) THEN
    SELECT m.DESCRIPCION
      INTO v_descripcion_mac
     FROM PFC_MAC_MAESTRO m,PFC_CONFIGURACION_MARCADOR cm
     WHERE cm.cod_mac = m.cod_mac ;

    SELECT m.COD_MAC_VCH
      INTO v_cod_mac_vch
     FROM PFC_MAC_MAESTRO m,PFC_CONFIGURACION_MARCADOR cm
     WHERE cm.cod_mac = m.cod_mac ;
    PV_COD_MAC_L := v_cod_mac_vch;
    PV_DESCRICION_MAC := v_descripcion_mac;
    PN_COD_RESULTADO := 1;
    PV_MSG_RESULTADO := 'Inserción exitosa: Se registró configuración de marcador';
    END IF; */
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_I_CONFIGURACION_MARCA]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_IU_CONFIGURACION_MARCA;

  /* AUTHOR  : CATALINA*/
  /* CREATED : XX/XX/XXXX*/
  /* PURPOSE : Configuracion Marcador Maestro*/

   PROCEDURE SP_PFC_S_CONFIGURACION_MARCA (
      PN_COD_CONFIG_MARCA   IN                    NUMBER,
      TC_LIST               OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) IS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN TC_LIST FOR SELECT
                          P1.NOMBRE,
                          P2.NOMBRE,
                          P3.NOMBRE,
                          P4.NOMBRE
                       FROM
                          PFC_CONFIGURACION_MARCADOR   CM
                          INNER JOIN PFC_PARAMETRO                P1 ON P1.COD_PARAMETRO = 66
                          INNER JOIN PFC_PARAMETRO                P2 ON P2.COD_PARAMETRO = 68
                          INNER JOIN PFC_PARAMETRO                P3 ON P3.COD_PARAMETRO = 69
                          INNER JOIN PFC_PARAMETRO                P4 ON P4.COD_PARAMETRO = 64
                       WHERE
                          CM.COD_CONFIG_MARCA = PN_COD_CONFIG_MARCA;

      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_CONFIGURACION_MARCA]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_CONFIGURACION_MARCA;

  /* AUTHOR  : CATALINA*/
  /* CREATED : 24/01/2019*/
  /* PURPOSE : Salida de la tabla Resulta Basal Marcador*/

   PROCEDURE SP_PFC_S_RPTA_BASAL_MARCADOR (
      PN_COD_MAC         IN                 NUMBER,
      PN_GRP_DIAG        IN                 NUMBER,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) IS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN TC_LIST FOR SELECT
                          CM.COD_CONFIG_MARCA,
                          CM.COD_CONFIG_MARCA_LARGO,
                          EXM.COD_EXAMEN_MED,
                          EXM.COD_EXAMEN_MED_LARGO,
                          EXM.DESCRIPCION,
             /*exm.rango,*/
             /*exm.unidad_medida,*/
                          P1.NOMBRE AS NOMBRE_TIPOEXMED/*,
             p2.nombre                 as nombre_TipoIngrRes,
             p3.nombre                 as nombrePosibleValor*/
                       FROM
                          PFC_CONFIGURACION_MARCADOR   CM, PFC_EXAMEN_MEDICO_MARCADOR   EXM
                          INNER JOIN PFC_PARAMETRO                P1 ON P1.COD_PARAMETRO = EXM.P_TIPO_EXAMEN
       /*INNER JOIN pfc_parametro p2
          on p2.cod_parametro = exm.p_tipo_ingreso_res*/
        /*LEFT JOIN pfc_parametro p3
          on p3.cod_parametro = exm.p_posible_valor*/
                       WHERE
                          EXM.COD_EXAMEN_MED = CM.COD_EXAMEN_MED
         /*AND cm.grupo_diagnostico = PV_GRUP_DIAG*/
                          AND CM.COD_MAC = PN_COD_MAC;

      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_RPTA_BASAL_MARCADOR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_RPTA_BASAL_MARCADOR;

   -- AUTHOR  : JAZABACHE
   -- CREATED : 29/01/2019
   -- PURPOSE : Inserta la evaluacion del resultado final del lider tumor

   PROCEDURE SP_PFC_I_SOL_EVA_LIDER_TUMOR (
      PN_COD_SOL_EVA        IN                    NUMBER,
      PV_FEC_EVA            IN                    VARCHAR2,
      PN_RESULTADO_EVA      IN                    NUMBER,
      PV_COMENTARIO         IN                    VARCHAR2,
      PN_COD_LIDER_TUMOR    IN                    NUMBER,/* CODIGO LIDER DE TUMOR*/
      PN_COD_ROL_USUARIO    IN                    NUMBER,/* CODIGO ROL USUARIO*/
      PN_COD_USUARIO        IN                    NUMBER,/* CODIGO USUARIO*/
      PN_COD_ROL_LI_TUMOR   IN                    NUMBER,/* CODIGO ROL ROL LIDER DE TUMOR*/
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) IS

      V_COD_SEV_LIDER_TUM_SEQ     NUMBER;
      V_CANTIDAD                  NUMBER;
      V_COD_HIST_LINEA_TRAT_MAX   NUMBER;
      V_COUNT_LIN_TRA             NUMBER;
      V_CMP_MEDICO                VARCHAR2(20);
      V_MEDICO_TRATANTE           VARCHAR2(100);
      V_COD_RESULTADO_SOLB        NUMBER;
      V_MSG_RESULTADO_SOLB        VARCHAR2(200);
      V_VALOR_PROX_MONIT          NUMBER;
      V_FECHA_PROX_MONIT          DATE;
      V_FECHA_APROBACION          DATE;
      V_COD_MONITOREO_SEQ         NUMBER;
      PV_COD_LARGO1               VARCHAR2(20);
      PN_COD_RESULTADO1           NUMBER;
      PV_MSG_RESULTADO1           VARCHAR(200);
      V_CODIGO_RESULTADO          NUMBER;
      V_NRO_LINEA_TRA             NUMBER;
      V_MAC_SOLICITADA            NUMBER;
      V_COD_SCG                   NUMBER;
      V_COD_AFI_PACIENTE          VARCHAR2(20);
      V_EXC_LINEA_TRAT            EXCEPTION;
      V_EXC_ESTADO_SOL_EVA        EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO_SEG         NUMBER; /* SEGUIMIENTO*/
      V_MSG_RESULTADO_SEG         VARCHAR2(4000); /* SEGUIMIENTO*/
      V_ESTADO_SOL_EVA1           NUMBER;
      V_EXC_CAMPO_OBLIG EXCEPTION;
      V_MENSAJE                   VARCHAR2(200);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      CASE
         WHEN NULLIF(
            PV_COMENTARIO,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO COMENTARIO OBLIGATORIO';
            RAISE V_EXC_CAMPO_OBLIG;
         WHEN NULLIF(
            PV_FEC_EVA,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO FECHA DE EVALUACION OBLIGATORIO';
            RAISE V_EXC_CAMPO_OBLIG;
         WHEN NULLIF(
            PN_RESULTADO_EVA,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO RESULTADO DE EVALUACION OBLIGATORIO';
            RAISE V_EXC_CAMPO_OBLIG;
         WHEN LENGTH(PV_COMENTARIO) > PCK_PFC_CONSTANTE.PN_LONG_COMENTARIO_LIDER_TUM THEN
            V_MENSAJE := 'CAMPO COMENTARIO SUPERA LA LONGITUD MAXIMA DE '
                         || PCK_PFC_CONSTANTE.PN_LONG_COMENTARIO_LIDER_TUM
                         || ' CARACTERES';
            RAISE V_EXC_CAMPO_OBLIG;
         ELSE
            V_MENSAJE := 'CAMPOS CORRECTOS';
      END CASE;

      SELECT
         COUNT(*)
      INTO V_CANTIDAD
      FROM
         ONTPFC.PFC_EVA_LIDER_TUM
      WHERE
         COD_SOL_EVA = PN_COD_SOL_EVA;

      IF V_CANTIDAD = 0 THEN
        
         BEGIN
            SELECT SPR.COD_MAC, SPR.COD_SCG
              INTO V_MAC_SOLICITADA, V_COD_SCG
              FROM PFC_SOLICITUD_EVALUACION SEV
             INNER JOIN PFC_SOLICITUD_PRELIMINAR SPR
                ON SEV.COD_SOL_PRE = SPR.COD_SOL_PRE
             WHERE SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

         EXCEPTION
            WHEN OTHERS THEN
               V_MENSAJE := 'ERROR AL OBTENER LA MAC SOLICITADA';
               RAISE V_EXC_CAMPO_OBLIG;
         END;

         SELECT
            SQ_PFC_COD_SEV_LIDER_TUM.NEXTVAL
         INTO V_COD_SEV_LIDER_TUM_SEQ
         FROM
            DUAL;

         INSERT INTO PFC_EVA_LIDER_TUM (
            COD_EVA_LIDER_TUM,
            FECHA_EVA,
            P_RESULTADO_EVA,
            COMENTARIO,
            COD_SOL_EVA,
            COD_LIDER_TUMOR,
            USUARIO_CREACION,
            FECHA_CREACION
         ) VALUES (
            V_COD_SEV_LIDER_TUM_SEQ,
            TO_DATE(PV_FEC_EVA,'DD-MM-YYYY hh24:mi:ss'),
            PN_RESULTADO_EVA,
            PV_COMENTARIO,
            PN_COD_SOL_EVA,
            PN_COD_LIDER_TUMOR,
            PN_COD_USUARIO,
            TO_DATE(PV_FEC_EVA,'DD-MM-YYYY hh24:mi:ss')
         );

         UPDATE PFC_SOLICITUD_EVALUACION SEV
         SET
            SEV.P_ESTADO_SOL_EVA = PN_RESULTADO_EVA,
            SEV.P_EVALUACION_LIDER_TUMOR = PN_RESULTADO_EVA,
            SEV.FEC_EVALUACION_LIDER_TUMOR = TO_DATE(PV_FEC_EVA,'DD/MM/YYYY HH24:MI:SS')
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

         IF PN_RESULTADO_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T THEN
           
            UPDATE PFC_SOLICITUD_EVALUACION SEV
            SET
               SEV.FEC_FINALIZAR_ESTADO = TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS'),
               --SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_CMAC,
               SEV.P_ROL_RESP_PENDIENTE_EVA = NULL,
               SEV.COD_AUTO_PERTE = PN_COD_USUARIO,
               SEV.COD_LIDER_TUMOR = PN_COD_LIDER_TUMOR
            WHERE SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
            
            BEGIN
              SELECT S.COD_AFI_PACIENTE
                INTO V_COD_AFI_PACIENTE
                FROM PFC_SOLBEN S
               WHERE S.COD_SCG = V_COD_SCG;
            EXCEPTION
              WHEN OTHERS THEN
                V_MENSAJE := 'ERROR AL OBTENER EL CODIGO DE AFILIADO DEL PACIENTE';
                V_CODIGO_RESULTADO := 6;
            END;
            
            SELECT
               SQ_PFC_MONITOREO_COD_MONITOREO.NEXTVAL
            INTO V_COD_MONITOREO_SEQ
            FROM
               DUAL;

            ONTPFC.PCK_PFC_UTIL.SP_PFC_CODIGO_LARGO(
               PN_COD_SOL_EVA,
               PCK_PFC_CONSTANTE.PN_COD_LARGO_MONITOREO,
               PV_COD_LARGO1,
               PN_COD_RESULTADO1,
               PV_MSG_RESULTADO1
            );

            SELECT TO_NUMBER(P.VALOR1)
              INTO V_VALOR_PROX_MONIT
              FROM PFC_PARAMETRO P
             WHERE P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_CODIGO_PROX_MONIT;

            SELECT
               TO_DATE(TO_CHAR(TO_DATE(PV_FEC_EVA,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY'),'DD/MM/YYYY') + V_VALOR_PROX_MONIT,
               TO_DATE(TO_CHAR(TO_DATE(PV_FEC_EVA,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY'),'DD/MM/YYYY')
            INTO
               V_FECHA_PROX_MONIT,
               V_FECHA_APROBACION
            FROM
               DUAL;

            INSERT INTO PFC_MONITOREO (
               COD_MONITOREO,
               COD_DESC_MONITOREO,
               COD_SOL_EVA,
               P_ESTADO_MONITOREO,
               FEC_PROX_MONITOREO
            ) VALUES (
               V_COD_MONITOREO_SEQ,
               PV_COD_LARGO1
               || '-'
               || PCK_PFC_CONSTANTE.PV_INCIO_TAREA_MONIT,
               PN_COD_SOL_EVA,
               PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_MONITOREO,
               V_FECHA_PROX_MONIT
            );

            PCK_PFC_UTIL.SP_PFC_S_CMP_MEDICO(
               PN_COD_SOL_EVA,
               V_CMP_MEDICO,
               V_MEDICO_TRATANTE,
               V_COD_RESULTADO_SOLB,
               V_MSG_RESULTADO_SOLB
            );
            SELECT COUNT(1)
              INTO V_COUNT_LIN_TRA
              FROM PFC_HIST_LINEA_TRATAMIENTO LT
             WHERE LT.COD_SOL_EVA = PN_COD_SOL_EVA;

            IF V_COUNT_LIN_TRA = 0 THEN
               SELECT
                  SQ_PFC_HIS_LINE_TRAT_COD_HIS.NEXTVAL
               INTO V_COD_HIST_LINEA_TRAT_MAX
               FROM
                  DUAL;

               BEGIN
                  SELECT
                     MN.NRO_LINEA_TRA
                  INTO V_NRO_LINEA_TRA
                  FROM
                     PFC_MEDICAMENTO_NUEVO MN
                  WHERE
                     MN.COD_SOL_EVA = PN_COD_SOL_EVA;

               EXCEPTION
                  WHEN OTHERS THEN
                     V_NRO_LINEA_TRA := 1;
               END;

               BEGIN
                  INSERT INTO PFC_HIST_LINEA_TRATAMIENTO (
                     COD_HIST_LINEA_TRAT,
                     COD_SOL_EVA,
                     COD_MAC,
                     LINEA_TRAT,
                     FEC_APROBACION,
                     FEC_INICIO,
                     P_ESTADO,
                     CMP,
                     MEDICO_TRATANTE,
                     COD_AFILIADO,
                     FEC_EMISION,
                     COD_AUDITOR_EVAL
                  ) VALUES (
                     V_COD_HIST_LINEA_TRAT_MAX,
                     PN_COD_SOL_EVA,
                     V_MAC_SOLICITADA,
                     V_NRO_LINEA_TRA,
                     V_FECHA_APROBACION,
                     V_FECHA_APROBACION,
                     PCK_PFC_CONSTANTE.PN_ESTADO_LIN_TRA_ACTIVO,
                     V_CMP_MEDICO,
                     V_MEDICO_TRATANTE,
                     V_COD_AFI_PACIENTE,
                     TO_DATE(PV_FEC_EVA,'DD/MM/YYYY HH24:MI:SS'),
                     PN_COD_USUARIO
                  );

               EXCEPTION
                  WHEN OTHERS THEN
                     V_CODIGO_RESULTADO   := 5;
                     V_MENSAJE            := 'NO SE CREO LA NUEVA LINEA DE TRATAMIENTO';
                     RAISE V_EXC_LINEA_TRAT;
               END;

            END IF;
         ELSIF PN_RESULTADO_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_OBSERVADO_LIDER_T THEN
           UPDATE PFC_SOLICITUD_EVALUACION SEV
           SET
              SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_CMAC,
              SEV.COD_AUTO_PERTE = PN_COD_USUARIO,
              SEV.COD_LIDER_TUMOR = PN_COD_LIDER_TUMOR
           WHERE
              SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
         ELSE
           UPDATE PFC_SOLICITUD_EVALUACION SEV
           SET
              SEV.FEC_FINALIZAR_ESTADO = TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS'),
              --SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
              SEV.P_ROL_RESP_PENDIENTE_EVA = NULL,
              SEV.COD_AUTO_PERTE = PN_COD_USUARIO,
              SEV.COD_LIDER_TUMOR = PN_COD_LIDER_TUMOR
           WHERE
              SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
         END IF;

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'REGISTRO EXITOSO';
      ELSE
         UPDATE PFC_EVA_LIDER_TUM LT
         SET
            LT.FECHA_EVA = TO_DATE(
               PV_FEC_EVA,
               'DD-MM-YYYY hh24:mi:ss'
            ),
            LT.P_RESULTADO_EVA = PN_RESULTADO_EVA,
            LT.COMENTARIO = PV_COMENTARIO,
            LT.COD_LIDER_TUMOR = PN_COD_LIDER_TUMOR,
            LT.USUARIO_MODIFICACION = PN_COD_LIDER_TUMOR,
            LT.FECHA_MODIFICACION = TO_DATE(PV_FEC_EVA,'DD-MM-YYYY hh24:mi:ss')
         WHERE
            LT.COD_SOL_EVA = PN_COD_SOL_EVA;
            
         IF PN_RESULTADO_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_OBSERVADO_LIDER_T THEN            
            UPDATE PFC_SOLICITUD_EVALUACION SEV
            SET
               SEV.P_ESTADO_SOL_EVA = PN_RESULTADO_EVA,
               SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_CMAC,
               SEV.COD_AUTO_PERTE = PN_COD_USUARIO,
               SEV.COD_LIDER_TUMOR = PN_COD_LIDER_TUMOR
            WHERE
               SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
         ELSE 
            UPDATE PFC_SOLICITUD_EVALUACION SEV
            SET
               SEV.FEC_FINALIZAR_ESTADO = TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS'),
               --SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
               SEV.P_ROL_RESP_PENDIENTE_EVA = NULL,
               SEV.COD_AUTO_PERTE = PN_COD_USUARIO,
               SEV.COD_LIDER_TUMOR = PN_COD_LIDER_TUMOR,
               SEV.P_ESTADO_SOL_EVA = PN_RESULTADO_EVA
            WHERE
               SEV.COD_SOL_EVA = PN_COD_SOL_EVA;
         END IF;

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'ACTUALIZACION EXITOSO';
      END IF;

/* SEGUIMIENTO BEGIN */

      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA1
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA1 := NULL;
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;

      INSERT INTO PFC_LOG_SEGUIMIENTO VALUES (
         'CODIG 111',
         '|PN_COD_SOL_EVA : '
         || PN_COD_SOL_EVA
         || ' | V_ESTADO_SOL_EVA1 : '
         || V_ESTADO_SOL_EVA1
         || ' |PV_FEC_EVA: '
         || PV_FEC_EVA
         || ' |PN_COD_ROL_LI_TUMOR : '
         || PN_COD_ROL_LI_TUMOR
         || ' |PN_COD_LIDER_TUMOR : '
         || PN_COD_LIDER_TUMOR
         || ' |PN_COD_ROL_USUARIO : '
         || PN_COD_ROL_USUARIO
         || ' |PN_COD_USUARIO : '
         || PN_COD_USUARIO
      );

      COMMIT;
      ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO(
         PN_COD_SOL_EVA,/* NUMBER*/
         V_ESTADO_SOL_EVA1,
         PV_FEC_EVA,
         PN_COD_ROL_LI_TUMOR, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         PN_COD_LIDER_TUMOR, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL DE USUARIO*/
         PN_COD_USUARIO,/* CODIGO DE USUARIO*/
         V_COD_RESULTADO_SEG,/* NUMBER*/
         V_MSG_RESULTADO_SEG
      ); /* VARCHAR*/

      IF V_COD_RESULTADO_SEG <> 0 THEN
         RAISE V_COD_RESULTADO; /* EXCEPTION*/
      END IF;            
      /* SEGUIMIENTO END */
      COMMIT;
   EXCEPTION
      WHEN V_COD_RESULTADO THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR, AL INSERTAR LA TABLA SEGUIMIENTO ' 
                              || '[[ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO]]' 
                              || V_MSG_RESULTADO_SEG;
      WHEN V_EXC_ESTADO_SOL_EVA THEN
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := 'ERROR, NO SE ENCONTRO EL ESTADO DE LA SOLICITUD DE EVALUACION';
      WHEN V_EXC_CAMPO_OBLIG THEN
         PN_COD_RESULTADO   := 3;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN V_EXC_LINEA_TRAT THEN
         ROLLBACK;
         PN_COD_RESULTADO   := V_CODIGO_RESULTADO;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_I_SOL_EVA_LIDER_TUMOR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_I_SOL_EVA_LIDER_TUMOR;

  /* AUTHOR  : CATALINA*/
  /* CREATED : 29/01/2019*/
  /* PURPOSE : Listar a los Responsable de Lider Tumor según el grupo diagnóstico de la Solicitud de Evaluación y diferente del Medico Tratante*/

   PROCEDURE SP_PFC_S_EVA_ROL_LIDER_TUMOR (
      PN_COD_ROL         IN                 NUMBER,
                                         /*PN_COD_DESC_SOL_EVA IN VARCHAR2,*/
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) IS
    /*v_cmp_participante VARCHAR2(50);*/
      V_CMP_SOLBEN VARCHAR2(50);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      SELECT
         S.CMP_MEDICO
      INTO V_CMP_SOLBEN
      FROM
         PFC_SOLBEN                 S,
         PFC_SOLICITUD_PRELIMINAR   PL,
         PFC_SOLICITUD_EVALUACION   EVA
      WHERE
         PL.COD_SOL_PRE = EVA.COD_SOL_PRE
         AND S.COD_SCG = PL.COD_SCG
       /*AND eva.COD_DESC_SOL_EVA = PN_COD_DESC_SOL_EVA*/;

    /*OPEN TC_LIST FOR
      SELECT us.cod_usuario, us.usuario
        FROM pfc_participante         p,
             pfc_grupo_diagnostico    gd,
             ONTPPA.ppa_usuario us,
             ONTPPA.ppa_usr_rol ur
       WHERE p.cod_grp_diag = gd.cod_grp_diag
         AND us.cod_usuario = ur.cod_usuario
         AND p.cod_usuario = ur.cod_usuario
         AND p.cod_usuario = us.cod_usuario
         AND ur.estado = PCK_PFC_CONSTANTE.PV_STATUS_USER_ROL_ACTIVE
         AND us.estado = PCK_PFC_CONSTANTE.PV_STATUS_USER_ACTIVE
         AND p.estado_participante =
             PCK_PFC_CONSTANTE.PV_STATUS_PART_ACTIVE
         AND ur.cod_rol = PN_COD_ROL
         and p.cmp_medico != v_cmp_solben;*/

      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_EVA_ROL_LIDER_TUMOR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_EVA_ROL_LIDER_TUMOR;


   -- AUTHOR  : AFLORES*/
   -- CREATED : 30/01/2019*/
   -- PURPOSE : Inserta Programa de reunion CMAC.*/

   PROCEDURE SP_PFC_I_PROG_REUNION_CMAC (
      PV_FEC_PROG_CMAC      IN                    VARCHAR2,
      PV_HORA_CMAC          IN                    VARCHAR2,
      PN_COD_DESC_SOL_EVA   IN                    VARCHAR2,
      PN_COD_ARCHIVO        IN                    NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) IS

      V_COD_PROG_CMAC_SEQ       NUMBER;
      V_COD_ACTA_CMAC           NUMBER;
      V_COD_PROG_CMAC_DET_SEQ   NUMBER;
      PV_COD_LARGO1             VARCHAR2(3000);
      PN_COD_RESULTADO1         NUMBER;
      PV_MSG_RESULTADO1         VARCHAR2(3000);
      V_CONTAR_PROG_CMAC        NUMBER;
      V_CONTADOR                NUMBER;
      V_PRG_CMAC                NUMBER;
      V_COD_PRG_DETALLE         NUMBER;
      PV_COD_LARGO              VARCHAR2(3000);
      V_COD_RESULTADO           VARCHAR2(3000);
      V_MSG_RESULTADO           VARCHAR2(3000);
      V_FECHA_ANIO              VARCHAR2(5);
      V_COD_PROGRAMACION_CMAC   NUMBER;
      V_EXC_VARIABLE            EXCEPTION;
      V_MENSAJE                 VARCHAR2(100);
      
      CURSOR V_LIST_EVA IS
      SELECT
         REGEXP_SUBSTR(LISTA,'([^,]+)',1,1,'',1) AS LISTA_1
      FROM (SELECT REGEXP_SUBSTR( PN_COD_DESC_SOL_EVA,'[^,]+',1, LEVEL) AS LISTA
            FROM DUAL
            CONNECT BY REGEXP_SUBSTR(PN_COD_DESC_SOL_EVA,'[^,]+',1,LEVEL) IS NOT NULL);
      
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      
      BEGIN
      
      SELECT COUNT(PC.COD_PROGRAMACION_CMAC)
        INTO V_CONTAR_PROG_CMAC
        FROM PFC_PROGRAMACION_CMAC PC
       WHERE PC.FEC_REUNION = TO_DATE(PV_FEC_PROG_CMAC, 'DD/MM/YYYY')
         AND PC.P_ESTADO_PROG_CMAC = 1;
      
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          V_CONTAR_PROG_CMAC := NULL;
      END;
      
      IF V_CONTAR_PROG_CMAC > 0 THEN
        V_MENSAJE := 'YA SE ENCUENTRA PROGRAMADA PARA LA FECHA : '|| PV_FEC_PROG_CMAC;
        RAISE V_EXC_VARIABLE;
      END IF;
      
      SELECT SQ_PFC_COD_ACTA_CMAC.NEXTVAL
        INTO V_COD_ACTA_CMAC
        FROM DUAL;

      SELECT TO_CHAR(TO_DATE(PV_FEC_PROG_CMAC,'DD/MM/YYYY'),'YYYY')
        INTO V_FECHA_ANIO
        FROM DUAL;
        
      ONTPFC.PCK_PFC_UTIL.SP_PFC_COD_FECHA(
         V_FECHA_ANIO,
         V_COD_ACTA_CMAC,
         PCK_PFC_CONSTANTE.PV_CODIGO_LONGITUD_CMAC,/* CODIGO LONGITUD 10*/
         PV_COD_LARGO,
         V_COD_RESULTADO,
         V_MSG_RESULTADO
      );
      
      SELECT SQ_PFC_COD_PROG_CMAC.NEXTVAL
        INTO V_COD_PROG_CMAC_SEQ
        FROM DUAL;

      INSERT INTO ONTPFC.PFC_PROGRAMACION_CMAC (
         COD_PROGRAMACION_CMAC,
         FEC_REUNION,
         HORA_REUNION,
         P_ESTADO_PROG_CMAC,
         COD_ACTA,
         COD_ARCHIVO_PROG
      ) VALUES (
         V_COD_PROG_CMAC_SEQ,
         TO_DATE(PV_FEC_PROG_CMAC,'DD/MM/YYYY'),
         PV_HORA_CMAC,
         1,
         PV_COD_LARGO,
         PN_COD_ARCHIVO
      );
      
      FOR V_ITEM IN V_LIST_EVA LOOP
            
        SELECT SQ_PFC_COD_PROG_CMAC_DET.NEXTVAL
          INTO V_COD_PROG_CMAC_DET_SEQ
          FROM DUAL;
            
       /* ONTPFC.PCK_PFC_UTIL.SP_PFC_CODIGO_LARGO(v_cod_prog_cmac_det_seq,10,PV_COD_LARGO1,PN_COD_RESULTADO1,PV_MSG_RESULTADO1);*/

        INSERT INTO ONTPFC.PFC_PROGRAMACION_CMAC_DET (
           COD_PROG_CMAC_DET,
           COD_PROGRAMACION_CMAC,
           COD_SOL_EVA
        ) VALUES (
           V_COD_PROG_CMAC_DET_SEQ,
           V_COD_PROG_CMAC_SEQ,
           V_ITEM.LISTA_1
        );

        PN_COD_RESULTADO       := 0;
        PV_MSG_RESULTADO       := 'Se grabó correctamente ';
            
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN V_EXC_VARIABLE THEN
         PN_COD_RESULTADO  := 1;
         PV_MSG_RESULTADO  := V_MENSAJE;
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_I_PROG_REUNION_CMAC]] ' || TO_CHAR(SQLCODE)
                             || ' : '|| SQLERRM;
   END SP_PFC_I_PROG_REUNION_CMAC;



  /* AUTHOR  : CATALINA*/
  /* CREATED : 30/01/2019*/
  /* PURPOSE : Listar casos a Evaluar por el Autorizador CMAC*/

   PROCEDURE SP_PFC_S_CASOS_EVALUACION_CMAC (
      PN_COD_EVALUACION   IN                  NUMBER,
                                           /* PN_COD_DESC_SOL_EVA IN VARCHAR2,*/
      TC_LIST             OUT                 PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   ) IS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF PN_COD_EVALUACION IS NOT NULL THEN
         OPEN TC_LIST FOR SELECT
                             EV.COD_DESC_SOL_EVA,
                             'JESUS AZABACHE' AS PACIENTE,
                             'LEUCEMIA' AS DIAGNOSTICO,
                             SO.COD_DIAGNOSTICO,
                             (
                                SELECT
                                   MA.COD_MAC_LARGO
                                FROM
                                   PFC_MAC MA
                                WHERE
                                   MA.COD_MAC_LARGO = SP.COD_MAC
                             ) AS MEDICAMENTO
                          FROM
                             ONTPFC.PFC_SOLICITUD_EVALUACION    EV
                             INNER JOIN ONTPFC.PFC_SOLICITUD_PRELIMINAR    SP ON EV.COD_SOL_PRE = SP.COD_SOL_PRE
                             INNER JOIN ONTPFC.PFC_SOLBEN                  SO ON SP.COD_SCG = SO.COD_SCG
                             LEFT OUTER JOIN ONTPFC.PFC_PROGRAMACION_CMAC_DET   PCD ON EV.COD_SOL_EVA = PCD.COD_SOL_EVA
                             LEFT OUTER JOIN ONTPFC.PFC_PROGRAMACION_CMAC       PC ON PC.COD_PROGRAMACION_CMAC = PCD.COD_PROGRAMACION_CMAC
                             LEFT OUTER JOIN ONTPFC.PFC_PARTICIPANTE            PR ON SO.CMP_MEDICO = PR.CMP_MEDICO
                          WHERE
                             EV.COD_SOL_EVA = PN_COD_EVALUACION;

         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'Consulta exitosa';
      ELSE
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := 'No cuenta con codigo de evaluacion';
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_CASOS_EVALUACION_CMAC]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_CASOS_EVALUACION_CMAC;

  /* AUTOR     : CATALINA*/
  /* CREADO    : 29/01/2019*/
  /* PROPOSITO : Listar Responsable de CMAC*/

   PROCEDURE SP_PFC_S_EVA_ROL_CMAC (
      PN_COD_ROL            IN                    NUMBER,
      PN_COD_DESC_SOL_EVA   IN                    VARCHAR2,
      TC_LIST               OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) IS
    /*v_cmp_participante VARCHAR2(50);*/
      V_CMP_SOLBEN VARCHAR2(50);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      SELECT
         S.CMP_MEDICO
      INTO V_CMP_SOLBEN
      FROM
         PFC_SOLBEN                 S,
         PFC_SOLICITUD_PRELIMINAR   PL,
         PFC_SOLICITUD_EVALUACION   EVA
      WHERE
         PL.COD_SOL_PRE = EVA.COD_SOL_PRE
         AND S.COD_SCG             = PL.COD_SCG
         AND EVA.COD_DESC_SOL_EVA  = PN_COD_DESC_SOL_EVA;

    /*OPEN TC_LIST FOR
      SELECT us.cod_usuario, us.usuario
        FROM pfc_participante         p,
             pfc_grupo_diagnostico    gd,
             ONTPPA.ppa_usuario us,
             ONTPPA.ppa_usr_rol ur
       WHERE p.cod_grp_diag = gd.cod_grp_diag
         AND us.cod_usuario = ur.cod_usuario
         AND p.cod_usuario = ur.cod_usuario
         AND p.cod_usuario = us.cod_usuario
         AND ur.estado = PCK_PFC_CONSTANTE.PV_STATUS_USER_ROL_ACTIVE
         AND us.estado = PCK_PFC_CONSTANTE.PV_STATUS_USER_ACTIVE
         AND p.estado_participante =
             PCK_PFC_CONSTANTE.PV_STATUS_PART_ACTIVE
         AND ur.cod_rol = PN_COD_ROL
         and p.cmp_medico = v_cmp_solben;*/

      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_EVA_ROL_CMAC]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_EVA_ROL_CMAC;

   -- AUTOR     : AFLORES
   -- CREADO    : 20/03/2019
   -- PROPOSITO : Trae los datos de evaluacion con la fecha que fueron programados por cmac.

   PROCEDURE SP_PFC_S_FEC_EVALUACION_MAC (
      PN_FEC_MAC         IN                 VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      TC_LIST_PAR        OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) IS

   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF NULLIF(
         PN_FEC_MAC,
         ''
      ) IS NOT NULL THEN
         OPEN TC_LIST FOR 
              SELECT
                 SO.CMP_MEDICO,
                 EV.COD_DESC_SOL_EVA,
                 SP.COD_MAC,
                 SO.COD_AFI_PACIENTE,
                 SO.COD_DIAGNOSTICO,
                 (SELECT M.DESCRIPCION FROM PFC_MAC M WHERE M.COD_MAC = SP.COD_MAC
                 AND M.P_ESTADO_MAC = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO) AS
                 DESCRIPCION,
                 MAC.HORA_REUNION,
                 EV.COD_SOL_EVA,
                 DET.COD_GRABADO,
                 MAC.COD_ACTA,
                 DET.OBSERVACION,
                 (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = DET.COD_RES_EVALUACION) AS
                 DESC_EVALUACION,
                 DET.COD_RES_EVALUACION,
                 PC.COD_PROGRAMACION_CMAC,
                 PC.COD_ACTA_FTP,
                 PC.FEC_REUNION,
                 PC.REPORTE_ACTA_ESCANEADA
              FROM
                 ONTPFC.PFC_SOLICITUD_EVALUACION    EV
                 INNER JOIN ONTPFC.PFC_SOLICITUD_PRELIMINAR    SP ON EV.COD_SOL_PRE = SP.COD_SOL_PRE
                 INNER JOIN ONTPFC.PFC_SOLBEN                  SO ON SP.COD_SCG = SO.COD_SCG
                 LEFT OUTER JOIN ONTPFC.PFC_PROGRAMACION_CMAC_DET   PCD ON EV.COD_SOL_EVA = PCD.COD_SOL_EVA
                 LEFT OUTER JOIN ONTPFC.PFC_PROGRAMACION_CMAC       PC ON PC.COD_PROGRAMACION_CMAC = PCD.COD_PROGRAMACION_CMAC
                 INNER JOIN ONTPFC.PFC_PROGRAMACION_CMAC_DET   DET ON DET.COD_SOL_EVA = EV.COD_SOL_EVA
                 INNER JOIN ONTPFC.PFC_PROGRAMACION_CMAC       MAC ON MAC.COD_PROGRAMACION_CMAC = DET.COD_PROGRAMACION_CMAC
              WHERE
                 MAC.FEC_REUNION = TO_DATE(PN_FEC_MAC,'DD/MM/YYYY')
              ORDER BY
                 EV.COD_DESC_SOL_EVA ASC;
                 

         OPEN TC_LIST_PAR FOR SELECT DISTINCT
                                PAR.COD_USUARIO_CMAC
                             FROM
                                ONTPFC.PFC_PARTICIPANTES_CMAC   PAR
                                INNER JOIN PFC_PROGRAMACION_CMAC           DET ON DET.COD_PROGRAMACION_CMAC = PAR.COD_PROGRAMACION_CMAC
                             WHERE
                                DET.FEC_REUNION = TO_DATE(
                                   PN_FEC_MAC,
                                   'DD/MM/YYYY'
                                );

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Consulta exitosa';
      ELSE
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'no ingreso la fecha';
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_EVA_ROL_CMAC]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_FEC_EVALUACION_MAC;

  -- AUTHOR  : AFLORES
  -- CREATED : 30/01/2019
  -- PURPOSE : Inserta Evaluacion del CMAC

  PROCEDURE SP_PFC_I_EVALUACION_CMAC (
      PV_EVALUACION         IN                    VARCHAR2,
      PV_PARTICIPANTE_MAC   IN                    VARCHAR2,
      PV_COD_ESCAN          IN                    VARCHAR2,
      PV_FECHA_PROG         IN                    VARCHAR2,/**/
      PV_HORA_PROG          IN                    VARCHAR2,/**/
      PV_FECHA_ESTADO       IN                    VARCHAR2, /* FECHA DE REGISTRO DE LA EVALUACION */
      PN_COD_ROL_CMAC       IN                    NUMBER, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
      PN_COD_CMAC           IN                    NUMBER, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
      PN_COD_ACTA_FTP       IN                    NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) IS

      V_CONTADOR                NUMBER(7);
      V_COD_PRG_CMAC            NUMBER(10);
      V_COD_PARTICIPANTE        NUMBER;
      V_COD_PROGRAMACION_DET    NUMBER;
      V_COD_PROG_CMAC_SEQ       NUMBER;
      V_COD_PROG_CMAC_DET_SEQ   NUMBER;
      V_COD_RESULTADO           EXCEPTION; /* SEGUIMIENTO}*/
      V_EXC_VARIABLE            EXCEPTION;
      V_MENSAJE                 VARCHAR2(100);
      V_COD_PROGRAMACION_CMAC   NUMBER;
      V_COD_ROL_USR_CMAC        NUMBER;
      V_COD_USR_CMAC            NUMBER;
      V_COD_SOL_EVA             NUMBER;
      V_COD_AFI_PACIENTE        VARCHAR2(20);
      V_MAC_SOLICITADA          NUMBER;
      V_COD_MONITOREO_SEQ       NUMBER;
      V_COD_LARGO1              VARCHAR2(20);
      V_COD_RESULTADO1          NUMBER;
      V_MSG_RESULTADO1          VARCHAR2(100);
      V_VALOR_PROX_MONIT        NUMBER;
      V_FECHA_PROX_MONIT        DATE;
      V_FECHA_APROBACION        DATE;
      V_NRO_LINEA_TRA           NUMBER;
      V_COD_HIST_LINEA_TRAT_MAX NUMBER;
      V_CMP_MEDICO              VARCHAR2(20);
      V_MEDICO_TRATANTE         VARCHAR2(100);
      V_COD_RESULTADO_SOLB      NUMBER;
      V_MSG_RESULTADO_SOLB      VARCHAR2(100);
      V_CODIGO_RESULTADO        NUMBER;
      V_CONTAR_PARTICIPANTE     NUMBER;
      V_EXC_LINEA_TRAT          EXCEPTION;
      V_COD_RESULTADO_SEG       NUMBER; /* SEGUIMIENTO*/
      V_MSG_RESULTADO_SEG       VARCHAR2(4000); /* SEGUIMIENTO*/
      V_COD_EVALUACION          NUMBER; /*SEGUIMIENTO*/
      V_COD_COD_ESTADO          NUMBER; /*SEGUIMIENTO*/
      
      
      CURSOR V_LIST_EVA IS
        SELECT
           REGEXP_SUBSTR(LISTA,'([^,]+)',1,1,'',1) AS LISTA_1,
           REGEXP_SUBSTR(LISTA,'([^,]+)',1,2,'',1) AS LISTA_2,
           REGEXP_SUBSTR(LISTA,'([^,]+)',1,3,'',1) AS LISTA_3
        FROM
           (SELECT REGEXP_SUBSTR(PV_EVALUACION,'[^|]+',1,LEVEL) AS LISTA
              FROM DUAL
           CONNECT BY REGEXP_SUBSTR(PV_EVALUACION,'[^|]+',1,LEVEL) IS NOT NULL);

      CURSOR V_LIST_MIENBRO_CMAC IS
        SELECT REGEXP_SUBSTR(LISTA,'([^,]+)',1,1,'',1) AS LISTA_1
          FROM (SELECT REGEXP_SUBSTR(PV_PARTICIPANTE_MAC,'[^,]+',1,LEVEL) AS LISTA
          FROM DUAL
              CONNECT BY
                 REGEXP_SUBSTR(PV_PARTICIPANTE_MAC,'[^,]+',1,LEVEL) IS NOT NULL
           );

   BEGIN
     
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      
      BEGIN
        
        SELECT PC.COD_PROGRAMACION_CMAC
          INTO V_COD_PROGRAMACION_CMAC
          FROM PFC_PROGRAMACION_CMAC PC
         WHERE PC.FEC_REUNION = TO_DATE(PV_FECHA_PROG, 'DD/MM/YYYY')
           AND PC.P_ESTADO_PROG_CMAC = 1;
           
        UPDATE PFC_PROGRAMACION_CMAC PC
           SET PC.COD_ACTA_FTP = PN_COD_ACTA_FTP
         WHERE PC.COD_PROGRAMACION_CMAC = V_COD_PROGRAMACION_CMAC;
        COMMIT;
         
      EXCEPTION
        WHEN OTHERS THEN
          V_COD_PRG_CMAC := NULL;
          V_MENSAJE := 'ERROR EN LA PROGRAMACION DE LA EVALUACION CMAC, VERIFICAR LA FECHA : '||PV_FECHA_PROG;
          RAISE V_EXC_VARIABLE;
      END;
      
      FOR V_ITEM IN V_LIST_EVA LOOP

         SELECT COUNT(*)
           INTO V_CONTADOR
           FROM ONTPFC.PFC_PROGRAMACION_CMAC_DET DET
          WHERE DET.COD_SOL_EVA = V_ITEM.LISTA_1 AND DET.COD_GRABADO  IS NULL;
          
          V_COD_SOL_EVA :=  V_ITEM.LISTA_1;

         IF V_CONTADOR > 0 THEN
           
            UPDATE ONTPFC.PFC_PROGRAMACION_CMAC_DET DET
               SET DET.OBSERVACION = V_ITEM.LISTA_3,
                   DET.COD_RES_EVALUACION = V_ITEM.LISTA_2,
                   DET.COD_GRABADO = PCK_PFC_CONSTANTE.PV_CODIGO_GRABAR_OK /* grabado correcto*/
             WHERE DET.COD_SOL_EVA = V_COD_SOL_EVA;
             
             UPDATE ONTPFC.PFC_SOLICITUD_EVALUACION SE
               SET SE.P_ESTADO_SOL_EVA = V_ITEM.LISTA_2,
                   SE.FEC_FINALIZAR_ESTADO = TO_DATE(PV_FECHA_ESTADO, 'DD/MM/YYYY  HH24:MI:SS'),
                   --SE.P_ROL_RESP_PENDIENTE_EVA = PN_COD_ROL_CMAC
                   SE.P_ROL_RESP_PENDIENTE_EVA = NULL
             WHERE SE.COD_SOL_EVA = V_COD_SOL_EVA;
             --
             DBMS_OUTPUT.PUT_LINE('ESTADO DE LA SOLICITUD DE EVALUACION  : ' || V_ITEM.LISTA_2);
             DBMS_OUTPUT.PUT_LINE('ESTADO DE LA SOLICITUD DE LAS CONSTANTES : ' || PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ);
             
             IF V_ITEM.LISTA_2 = PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC THEN
               DBMS_OUTPUT.PUT_LINE('FUERON APROBADOS LA SOLICITUD : ' || V_COD_SOL_EVA);
                SELECT SO.COD_AFI_PACIENTE, PR.COD_MAC
                  INTO V_COD_AFI_PACIENTE, V_MAC_SOLICITADA
                  FROM PFC_SOLICITUD_EVALUACION SE
                 INNER JOIN PFC_SOLICITUD_PRELIMINAR PR
                    ON SE.COD_SOL_PRE = PR.COD_SOL_PRE
                 INNER JOIN PFC_SOLBEN SO
                    ON SO.COD_SCG = PR.COD_SCG
                 WHERE SE.COD_SOL_EVA = V_COD_SOL_EVA;
                 
                -- CODIGO DE MONITOREO
                SELECT SQ_PFC_MONITOREO_COD_MONITOREO.NEXTVAL
                  INTO V_COD_MONITOREO_SEQ
                  FROM DUAL;
                  DBMS_OUTPUT.PUT_LINE('V_COD_MONITOREO_SEQ : ' || V_COD_MONITOREO_SEQ);
                
                -- CODIGO LARGO DE MONITOREO
                ONTPFC.PCK_PFC_UTIL.SP_PFC_CODIGO_LARGO(
                   V_COD_SOL_EVA,
                   PCK_PFC_CONSTANTE.PN_COD_LARGO_MONITOREO,
                   V_COD_LARGO1,
                   V_COD_RESULTADO1,
                   V_MSG_RESULTADO1
                );
                
                -- PARAMETRO DE CONTROL DE PROXIMO MONITOREO
                SELECT TO_NUMBER(P.VALOR1)
                  INTO V_VALOR_PROX_MONIT
                  FROM PFC_PARAMETRO P
                 WHERE P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_CODIGO_PROX_MONIT;
                 
                DBMS_OUTPUT.PUT_LINE('V_VALOR_PROX_MONIT : ' || V_VALOR_PROX_MONIT);
                SELECT TO_DATE(TO_CHAR(TO_DATE(PV_FECHA_ESTADO,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY'),'DD/MM/YYYY') + V_VALOR_PROX_MONIT,
                       TO_DATE(TO_CHAR(TO_DATE(PV_FECHA_ESTADO,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY'),'DD/MM/YYYY')
                  INTO V_FECHA_PROX_MONIT,
                       V_FECHA_APROBACION
                  FROM DUAL;
                  
                 BEGIN
                    SELECT MN.NRO_LINEA_TRA
                      INTO V_NRO_LINEA_TRA
                      FROM PFC_MEDICAMENTO_NUEVO MN
                     WHERE MN.COD_SOL_EVA = V_COD_SOL_EVA;
                 EXCEPTION
                    WHEN OTHERS THEN
                       V_NRO_LINEA_TRA := 1;
                 END;
                 
                 
                INSERT INTO PFC_MONITOREO (
                   COD_MONITOREO,
                   COD_DESC_MONITOREO,
                   COD_SOL_EVA,
                   P_ESTADO_MONITOREO,
                   FEC_PROX_MONITOREO
                ) VALUES (
                   V_COD_MONITOREO_SEQ,
                   V_COD_LARGO1|| '-' || PCK_PFC_CONSTANTE.PV_INCIO_TAREA_MONIT,
                   V_COD_SOL_EVA,
                   PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_MONITOREO,
                   V_FECHA_PROX_MONIT
                );
                
                DBMS_OUTPUT.PUT_LINE('V_COD_MONITOREO_SEQ : ' || V_COD_MONITOREO_SEQ);
                DBMS_OUTPUT.PUT_LINE('COD_DESC_MONITOREO : ' || V_COD_LARGO1|| '-' || PCK_PFC_CONSTANTE.PV_INCIO_TAREA_MONIT);
                
                PCK_PFC_UTIL.SP_PFC_S_CMP_MEDICO(
                   V_COD_SOL_EVA,
                   V_CMP_MEDICO,
                   V_MEDICO_TRATANTE,
                   V_COD_RESULTADO_SOLB,
                   V_MSG_RESULTADO_SOLB
                );
                
                -- CODIGO DE LINEA DE TRATAMIENTO
                SELECT SQ_PFC_HIS_LINE_TRAT_COD_HIS.NEXTVAL
                  INTO V_COD_HIST_LINEA_TRAT_MAX
                  FROM DUAL;
                  
                  DBMS_OUTPUT.PUT_LINE('V_COD_HIST_LINEA_TRAT_MAX : ' || V_COD_HIST_LINEA_TRAT_MAX);
                  
                BEGIN
                  INSERT INTO PFC_HIST_LINEA_TRATAMIENTO (
                     COD_HIST_LINEA_TRAT,
                     COD_SOL_EVA,
                     COD_MAC,
                     LINEA_TRAT,
                     FEC_APROBACION,
                     FEC_INICIO,
                     P_ESTADO,
                     CMP,
                     MEDICO_TRATANTE,
                     COD_AFILIADO,
                     FEC_EMISION,
                     COD_AUDITOR_EVAL
                  ) VALUES (
                     V_COD_HIST_LINEA_TRAT_MAX,
                     V_COD_SOL_EVA,
                     V_MAC_SOLICITADA,
                     V_NRO_LINEA_TRA,
                     V_FECHA_APROBACION,
                     V_FECHA_APROBACION,
                     PCK_PFC_CONSTANTE.PN_ESTADO_LIN_TRA_ACTIVO,
                     V_CMP_MEDICO,
                     V_MEDICO_TRATANTE,
                     V_COD_AFI_PACIENTE,
                     TO_DATE(PV_FECHA_ESTADO, 'DD/MM/YYYY  HH24:MI:SS'),
                     PN_COD_CMAC
                  );

               
                EXCEPTION
                  WHEN OTHERS THEN
                     V_CODIGO_RESULTADO   := 5;
                     V_MENSAJE            := 'NO SE CREO LA NUEVA LINEA DE TRATAMIENTO';
                     RAISE V_EXC_LINEA_TRAT;
                END;
             
             END IF;
             
             BEGIN
                V_COD_ROL_USR_CMAC   := PCK_PFC_CONSTANTE.PV_CODIGO_ROL_MIENBRO_CMAC;
                V_COD_USR_CMAC       := NULL;
                V_COD_EVALUACION     := TO_NUMBER(V_ITEM.LISTA_1);
                V_COD_COD_ESTADO     := TO_NUMBER(V_ITEM.LISTA_2);
                ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO(
                   V_COD_EVALUACION,/* NUMBER*/
                   V_COD_COD_ESTADO,
                   PV_FECHA_ESTADO,
                   V_COD_ROL_USR_CMAC, /* CODIGO DE ROL DE USUARIO*/
                   V_COD_USR_CMAC,/* CODIGO DE USUARIO*/
                   PN_COD_ROL_CMAC, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
                   PN_COD_CMAC, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
                   V_COD_RESULTADO_SEG,/* NUMBER*/
                   V_MSG_RESULTADO_SEG
                ); /* VARCHAR*/

                IF V_COD_RESULTADO_SEG <> 0 THEN
                   RAISE V_COD_RESULTADO; /* EXCEPTION*/
                END IF;
             EXCEPTION
                WHEN V_COD_RESULTADO THEN
                   PN_COD_RESULTADO   := 1;
                   PV_MSG_RESULTADO   := 'ERROR, AL INSERTAR LA TABLA SEGUIMIENTO [[ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO]]'
                   || V_MSG_RESULTADO_SEG;
             END;
             
             DBMS_OUTPUT.PUT_LINE('<<<  FIN  >>> ');
                 /*ACTUALIZA REGISTRO DE PDF*/
            
                 /*-------------------------POSIBLE CAIDA*/
            
            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'SE GRABO CORRECTAMENTE';
            
            
         END IF;
         
         

      END LOOP;
      
      BEGIN
        SELECT COUNT(1)
          INTO V_CONTAR_PARTICIPANTE
          FROM PFC_PARTICIPANTES_CMAC PC
         WHERE PC.COD_PROGRAMACION_CMAC = V_COD_PROGRAMACION_CMAC;
      EXCEPTION
        WHEN OTHERS THEN
          V_CONTAR_PARTICIPANTE := NULL;
      END;
      
      IF V_CONTAR_PARTICIPANTE = 0 THEN
        
        FOR V_ITEM1 IN V_LIST_MIENBRO_CMAC LOOP
          
           SELECT SQ_PFC_PARTICIPANTE_CMAC.NEXTVAL
             INTO V_COD_PARTICIPANTE
             FROM DUAL;

           INSERT INTO ONTPFC.PFC_PARTICIPANTES_CMAC (
              COD_PARTICIPANTE_CMAC,
              COD_PROGRAMACION_CMAC,
              COD_USUARIO_CMAC
           ) VALUES (
              V_COD_PARTICIPANTE,
              V_COD_PROGRAMACION_CMAC,
              V_ITEM1.LISTA_1
           );

        END LOOP;
      END IF;
      
      
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Se grabo correctamente';
      COMMIT;
   EXCEPTION
      WHEN V_EXC_VARIABLE THEN
        PN_COD_RESULTADO    := 1;
        PV_MSG_RESULTADO    := V_MENSAJE;
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_I_EVALUACION_CMAC]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_I_EVALUACION_CMAC;

   -- AUTOR     : JAZABACHE
   -- CREACION  : 11/05/2019
   -- PROPOSITO : ELIMINAR REGISTRO DE LA PROGRAMACION CMAC EN LA EVALUACION

   PROCEDURE SP_PFC_SD_ELIM_EVALUACION_CMAC (
      PN_COD_SOL_EVA        IN                    VARCHAR2,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) IS

   V_COD_PROG_CMAC_DET      NUMBER;
   V_EXC_VARIABLE           EXCEPTION;
   V_MENSAJE                VARCHAR2(100);
   V_EXC_AGREGADO           EXCEPTION;
   
   BEGIN
     
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      
        
      BEGIN
          
        SELECT PCD.COD_PROG_CMAC_DET
          INTO V_COD_PROG_CMAC_DET
          FROM PFC_PROGRAMACION_CMAC_DET PCD
         WHERE PCD.COD_SOL_EVA = PN_COD_SOL_EVA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          V_MENSAJE := 'SOLICITUD DE EVALUACION AGREGADA, FUE ELIMINADA';
          RAISE V_EXC_AGREGADO;
        WHEN OTHERS THEN
          V_MENSAJE := 'ERROR AL ELIMINAR REGISTRO DEL CODIGO DE SOLICITUD DE EVALUACION : '||PN_COD_SOL_EVA;
          RAISE V_EXC_VARIABLE;
      END;
        
        
      DELETE PFC_PROGRAMACION_CMAC_DET PCD
       WHERE PCD.COD_PROG_CMAC_DET = V_COD_PROG_CMAC_DET;
         
      PN_COD_RESULTADO    := 0;
      PV_MSG_RESULTADO    := PCK_PFC_CONSTANTE.PV_MENSAJE_ELIMINAR_EXITO;
        
   EXCEPTION
      WHEN V_EXC_AGREGADO THEN
        PN_COD_RESULTADO    := 0;
        PV_MSG_RESULTADO    := V_MENSAJE;
      WHEN V_EXC_VARIABLE THEN
        PN_COD_RESULTADO    := 1;
        PV_MSG_RESULTADO    := V_MENSAJE;
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SD_ELIM_EVALUACION_CMAC]] '|| TO_CHAR(SQLCODE)|| ' : '|| SQLERRM;
   
   END SP_PFC_SD_ELIM_EVALUACION_CMAC;
  
  /* AUTHOR  : AFLORES*/
  /* CREATED : 26/03/2019*/
  /* PURPOSE : Consulta de seguimiento de la evaluacion*/

   PROCEDURE SP_PFC_S_SEGUIMIENTO (
      PV_COD_EVALUACION   IN                  VARCHAR2,
      TC_LIST             OUT                 PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   ) IS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN TC_LIST FOR SELECT
                          SEG.COD_SEGUIMIENTO,
                          (
                             SELECT
                                PP.NOMBRE
                             FROM
                                PFC_PARAMETRO PP
                             WHERE
                                PP.COD_PARAMETRO = SEG.ESTADO_SOL_EVALUACION
                          ) AS ESTADOEVALUACION,
                          SEG.FEC_ESTADO,
                          SEG.ROL_RESP_ESTADO,
                          SEG.USR_RESP_ESTADO,
                          SEG.ROL_RESP_REG_ESTADO,
                          SEG.USR_RESP_REG_ESTADO
                       FROM
                          ONTPFC.PFC_SEGUIMIENTO SEG
                       WHERE
                          SEG.COD_SOL_EVALUACION = PV_COD_EVALUACION
                       ORDER BY
                          SEG.ESTADO_SOL_EVALUACION DESC,
                          SEG.FEC_ESTADO DESC;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_SEGUIMIENTO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_SEGUIMIENTO;
  
  /* AUTHOR  : AFLORES*/
  /* CREATED : 30/01/2019*/
  /* PURPOSE : INSERTA LA TABLA DE SEGUIMIENTO*/

   PROCEDURE SP_PFC_I_SEGUIMIENTO (
      PN_COD_EVALUACION        IN                       NUMBER,
      PV_ESTADO_EVALUACION     IN                       NUMBER,
      PV_FECHA_ESTADO          IN                       VARCHAR2,
      PV_ROL_RESP_ESTADO       IN                       NUMBER,
      PV_USR_RESP_ESTADO       IN                       NUMBER,
      PV_ROL_RESP_REG_ESTADO   IN                       NUMBER,
      PV_USR_RESP_REG_ESTADO   IN                       NUMBER,
      PN_COD_RESULTADO         OUT                      NUMBER,
      PV_MSG_RESULTADO         OUT                      VARCHAR2
   ) IS
      V_CONTADOR NUMBER(7);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF NULLIF(
         PN_COD_EVALUACION,
         ''
      ) IS NOT NULL THEN
         SELECT
            ONTPFC.SQ_PFC_SEGUIMIENTO_COD_SEG.NEXTVAL
         INTO V_CONTADOR
         FROM
            DUAL;

         INSERT INTO ONTPFC.PFC_SEGUIMIENTO (
            COD_SEGUIMIENTO,
            ESTADO_SOL_EVALUACION,
            FEC_ESTADO,
            ROL_RESP_ESTADO,
            USR_RESP_ESTADO,
            ROL_RESP_REG_ESTADO,
            USR_RESP_REG_ESTADO,
            COD_SOL_EVALUACION
         ) VALUES (
            V_CONTADOR,
            PV_ESTADO_EVALUACION,
            TO_DATE(
               PV_FECHA_ESTADO,
               'DD-MM-YYYY hh24:mi:ss'
            ),
            PV_ROL_RESP_ESTADO,
            PV_USR_RESP_ESTADO,
            PV_ROL_RESP_REG_ESTADO,
            PV_USR_RESP_REG_ESTADO,
            PN_COD_EVALUACION
         );

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Se grabo correctamente';
      ELSE
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'El codigo de evaluacio es nulo';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_I_SEGUIMIENTO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_I_SEGUIMIENTO;
  
   -- AUTHOR  : AFLORES
   -- CREATED : 14/01/2019
   -- PURPOSE : Lista un solicitud de evaluacion por el codigo para programacion Cmac

   PROCEDURE SP_PFC_S_EVALUACIONXCODIGO (
      PV_COD_DESC_SOL_EVA   IN                    VARCHAR2,
      AC_LISTADETALLE       OUT                   PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN AC_LISTADETALLE FOR SELECT
                                  EV.COD_SOL_EVA,
                                  EV.COD_DESC_SOL_EVA,
                                  SO.COD_SCG_SOLBEN,
                                  (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = SO.TIPO_SCG_SOLBEN) AS TIPO_SCG_SOLBEN,
                                  SO.ESTADO_SCG,
                                  (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_GRUPO = 5 AND P.CODIGO = SO.ESTADO_SCG) AS ESTADOSGCSOLBEN,
                                  SO.NRO_CG,
                                  EV.FEC_SOL_EVA   AS FECHAEVALUACION,
                                  (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = EV.P_TIPO_EVA)
                                  AS TIPOEVALUACION,
                                  (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = EV.P_ESTADO_SOL_EVA)
                                  AS ESTADOEVALUACION,
                                  (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = EV.ESTADO_CORREO_ENV_LIDER_TUMOR)
                                  AS CORREOLIDERTUMOR,
                                  (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = EV.ESTADO_CORREO_ENV_CMAC)
                                  AS CORREOCMAC,
                                  PC.FEC_REUNION   AS FECHACMAC,
                                  EV.P_ROL_RESP_PENDIENTE_EVA   AS ROL_RESPONSABLE,
                                  EV.COD_AUTO_PERTE             AS AUTORIZADOR_PERTENENCIA,
                                  EV.COD_LIDER_TUMOR            AS COD_LIDER_TUMOR,
                                  PR.APELLIDOS || ', ' || PR.NOMBRES  AS LIDER_TUMOR,
                                  SP.COD_MAC,
                                  SO.COD_AFI_PACIENTE,
                                  SO.COD_CLINICA,
                                  SO.COD_DIAGNOSTICO,
                                  MAC.DESCRIPCION,
                                  ONTPFC.FN_PFC_S_VALIDACION(
                                     SO.CMP_MEDICO,
                                     SO.COD_GRP_DIAG
                                  ) AS CODIGOP,
                                  EV.P_ESTADO_SOL_EVA,
                                  EV.ESTADO_CORREO_ENV_CMAC,
                                  EV.ESTADO_CORREO_ENV_LIDER_TUMOR,
                                  EV.CODIGO_ENVIO_ENV_MAC,
                                  EV.CODIGO_ENVIO_ENV_LIDER_TUMOR,
                                  EV.CODIGO_ENVIO_ENV_ALER_MONIT,
                                  EV.P_TIPO_EVA    AS P_TIPO_EVA
                               FROM
                                  ONTPFC.PFC_SOLICITUD_EVALUACION    EV
                                  INNER JOIN ONTPFC.PFC_SOLICITUD_PRELIMINAR    SP ON EV.COD_SOL_PRE = SP.COD_SOL_PRE
                                  INNER JOIN ONTPFC.PFC_SOLBEN                  SO ON SP.COD_SCG = SO.COD_SCG
                                  LEFT OUTER JOIN ONTPFC.PFC_PROGRAMACION_CMAC_DET   PCD ON EV.COD_SOL_EVA = PCD.COD_SOL_EVA
                                  LEFT OUTER JOIN ONTPFC.PFC_PROGRAMACION_CMAC       PC ON PC.COD_PROGRAMACION_CMAC = PCD.COD_PROGRAMACION_CMAC
                                  LEFT OUTER JOIN ONTPFC.PFC_PARTICIPANTE            PR ON SO.CMP_MEDICO = PR.CMP_MEDICO
                                  INNER JOIN ONTPFC.PFC_MAC                     MAC ON SP.COD_MAC = MAC.COD_MAC
                               WHERE
                                  1 = 1
                                  AND EV.COD_DESC_SOL_EVA = TO_NUMBER(PV_COD_DESC_SOL_EVA);

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta Exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_EVALUACIONXCODIGO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_EVALUACIONXCODIGO;

 
   -- AUTOR     : AFLORES
   -- CREADO    : 29/03/2019
   -- PROPOSITO : Trae los datos de evaluacion con el codigo para el registro de evaluacion Cmac.

   PROCEDURE SP_PFC_S_COD_EVALUACION_MAC (
      PV_COD_EVA         IN                 VARCHAR2,
      PV_COD_PROG_CMAC   IN                 VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) IS
      V_EXC_VARIABLE            EXCEPTION;
      V_MENSAJE                 VARCHAR2(100);
      V_COD_EVA                 VARCHAR2(20);
      V_CODIGO                  NUMBER;
      V_CODIGO_PROG_CMAC        NUMBER;
      V_AUXILIAR                NUMBER;
      V_COD_PROG_CMAC_DET_SEQ   NUMBER;
      
   BEGIN
     
      PN_COD_RESULTADO          := -1;
      PV_MSG_RESULTADO          := 'valor inicial';
      V_CODIGO_PROG_CMAC        := NULL;
      V_AUXILIAR                := NULL;
      
      IF NULLIF(PV_COD_EVA,'') IS NULL THEN
         V_CODIGO    := 1;
         V_MENSAJE   := 'No ingreso el Codigo de Solicitud';
         RAISE V_EXC_VARIABLE;
      ELSIF SUBSTR(PV_COD_EVA,1,1) <> '0' THEN
         DBMS_OUTPUT.PUT_LINE('DIFERENTE DE CERO : '||PV_COD_EVA);
         V_COD_EVA := LPAD(PV_COD_EVA,PCK_PFC_CONSTANTE.PN_COD_LARGO,'0');
      ELSE
         V_COD_EVA := PV_COD_EVA;
      END IF;
      
        
        SELECT COUNT(1)
          INTO V_CODIGO_PROG_CMAC
          FROM PFC_PROGRAMACION_CMAC_DET 
         WHERE cod_programacion_cmac = PV_COD_PROG_CMAC 
           AND COD_SOL_EVA = V_COD_EVA;
           
      
      SELECT COUNT(1)
        INTO V_AUXILIAR
        FROM ONTPFC.PFC_SOLICITUD_EVALUACION    EV
       WHERE EV.COD_DESC_SOL_EVA = V_COD_EVA;
       
      IF V_AUXILIAR <> 0 THEN
      
            IF V_CODIGO_PROG_CMAC = 0 THEN
            
                SELECT SQ_PFC_COD_PROG_CMAC_DET.NEXTVAL
                INTO V_COD_PROG_CMAC_DET_SEQ
                FROM DUAL;
                      
                INSERT INTO ONTPFC.PFC_PROGRAMACION_CMAC_DET (
                  COD_PROG_CMAC_DET,
                  COD_PROGRAMACION_CMAC,
                  COD_SOL_EVA
                ) VALUES (
                    V_COD_PROG_CMAC_DET_SEQ,
                    TO_NUMBER(PV_COD_PROG_CMAC),
                    TO_NUMBER(PV_COD_EVA)
                );
                
            END IF;
            
            OPEN TC_LIST FOR 
                 SELECT
                     EV.COD_DESC_SOL_EVA,
                     SP.COD_MAC,
                     SO.COD_AFI_PACIENTE,
                     SO.COD_DIAGNOSTICO,
                     (SELECT M.DESCRIPCION FROM PFC_MAC M WHERE M.COD_MAC = SP.COD_MAC
                      AND M.P_ESTADO_MAC = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO) AS
                      DESCRIPCION,
                     PC.HORA_REUNION,
                     EV.COD_SOL_EVA,
                     PCD.COD_GRABADO,
                     PC.COD_ACTA,
                     PCD.OBSERVACION,
                     (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = PCD.COD_RES_EVALUACION) AS
                     DESC_EVALUACION,
                     PCD.COD_RES_EVALUACION,
                     EV.P_ESTADO_SOL_EVA
                  FROM
                     ONTPFC.PFC_SOLICITUD_EVALUACION    EV
                     INNER JOIN ONTPFC.PFC_SOLICITUD_PRELIMINAR    SP ON EV.COD_SOL_PRE = SP.COD_SOL_PRE
                     INNER JOIN ONTPFC.PFC_SOLBEN                  SO ON SP.COD_SCG = SO.COD_SCG
                     LEFT OUTER JOIN ONTPFC.PFC_PROGRAMACION_CMAC_DET   PCD ON EV.COD_SOL_EVA = PCD.COD_SOL_EVA
                     LEFT OUTER JOIN ONTPFC.PFC_PROGRAMACION_CMAC       PC ON PC.COD_PROGRAMACION_CMAC = PCD.COD_PROGRAMACION_CMAC
                     --LEFT OUTER JOIN ONTPFC.PFC_PARTICIPANTE            PR ON SO.CMP_MEDICO = PR.CMP_MEDICO
                     --INNER JOIN ONTPFC.PFC_MAC                     MAC ON SP.COD_MAC = MAC.COD_MAC
                  WHERE
                     EV.COD_DESC_SOL_EVA = V_COD_EVA
                  ORDER BY
                     EV.COD_DESC_SOL_EVA ASC;
                     
          PN_COD_RESULTADO   := 0;
          PV_MSG_RESULTADO   := 'CONSULTA EXITOSA';
      ELSE
        
          PN_COD_RESULTADO   := 0;
          PV_MSG_RESULTADO   := 'NO SE ENCONTRARON DATOS CON EL CODIGO INGRESADO';
      END IF;
                         

      
   EXCEPTION
      WHEN V_EXC_VARIABLE THEN
         PN_COD_RESULTADO   := V_CODIGO;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_COD_EVALUACION_MAC]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_COD_EVALUACION_MAC;
   
  
  /* AUTOR     : RROCA*/
  /* CREADO    : 20/05/2019*/
  /* PROPOSITO : Cambia el estado de la solicitud de evaluación a PENDIENTE DE RESPUESTA DE MONITOREO*/

   PROCEDURE SP_PFC_U_EST_SOL_EVA (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PV_EST_SOL_EVAL    IN                 NUMBER,/* ESTADO DE SOLICITUD EVAL*/
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) IS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      UPDATE PFC_SOLICITUD_EVALUACION SEV
      SET
         SEV.P_ESTADO_SOL_EVA = PV_EST_SOL_EVAL,
         SEV.FECHA_MODIFICACION = SYSDATE
      WHERE
         SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Actualizacion exitosa: Se actualizo el estado de la evaluación';
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_U_EST_SOL_EVA]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_U_EST_SOL_EVA;
  
  
  
   /* AUTOR     : RROCA*/
  /* CREADO    : 28/05/2019*/
  /* PROPOSITO : ACTUALIZA EL CODIGO DE ENVIO DEL CORREO A  LA SOLICITUD DE EVALUACION*/

   PROCEDURE SP_PFC_U_COD_ENVIO_SOL_EVA (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PN_COD_ENVIO       IN                 VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) IS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      UPDATE PFC_SOLICITUD_EVALUACION SEV
      SET
         SEV.CODIGO_ENVIO_ENV_ALER_MONIT = PN_COD_ENVIO
      WHERE
         SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'Se actualizo el codigo de envio a la evaluación: ' || PN_COD_SOL_EVA;
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_U_COD_ENVIO_SOL_EVA]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_U_COD_ENVIO_SOL_EVA;
  
  /* AUTOR     : RROCA*/
  /* CREADO    : 28/05/2019*/
  /* PROPOSITO : Obtiene los participantes por grupo de diagnostico*/

   PROCEDURE SP_PFC_S_PARTICIPANTE_GRP_DIAG (
      PV_COD_GRP_DIAG     IN                  VARCHAR2,
      PN_COD_ROL          IN                  NUMBER,
      LISTAPARTICIPANTE   OUT                 PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN LISTAPARTICIPANTE FOR SELECT
                                    PGD.COD_PARTICIPANTE,
                                    PART.NOMBRES,
                                    PART.APELLIDOS,
                                    PART.CORREO_ELECTRONICO,
                                    PAR.COD_PARAMETRO,
                                    PAR.VALOR1,
                                    PAR.ESTADO
                                 FROM
                                    PFC_PARTICIPANTE_GRUPO_DIAG   PGD
                                    INNER JOIN PFC_PARAMETRO                 PAR ON PGD.P_RANGO_EDAD = PAR.COD_PARAMETRO
                                    INNER JOIN PFC_PARTICIPANTE              PART ON PGD.COD_PARTICIPANTE = PART.COD_PARTICIPANTE
                                 WHERE
                                    PGD.COD_GRP_DIAG = PV_COD_GRP_DIAG
                                    AND PART.COD_ROL = PN_COD_ROL;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta Exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_PARTICIPANTE_GRP_DIAG]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_PARTICIPANTE_GRP_DIAG; 
  
  /* AUTOR     : RROCA*/
  /* CREADO    : 30/05/2019*/
  /* PROPOSITO : Obtiene los participantes de farmacia*/

   PROCEDURE SP_PFC_S_PARTICIPANTE (
      PN_COD_ROL         IN                 NUMBER,
      PV_COD_GRP_DIAG    IN                 VARCHAR2,
      PN_P_RANGO_EDAD    IN                 NUMBER,
      PN_COD_USUARIO     IN                 NUMBER,
      AC_LISTA           OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      L_SQL_STMT      VARCHAR2(32767);
      L_CONDICIONES   VARCHAR2(4000);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'No se puede obtener la lista';
      IF ( PN_COD_ROL IS NOT NULL ) THEN
         L_CONDICIONES := L_CONDICIONES
                          || ' AND PAR.COD_ROL = '
                          || PN_COD_ROL
                          || '';
      END IF;

      IF ( PV_COD_GRP_DIAG IS NOT NULL ) THEN
         L_CONDICIONES := L_CONDICIONES
                          || ' AND PGD.COD_GRP_DIAG = '''
                          || PV_COD_GRP_DIAG
                          || '''';
      END IF;

      IF ( PN_P_RANGO_EDAD IS NOT NULL ) THEN
         L_CONDICIONES := L_CONDICIONES
                          || ' AND PGD.P_RANGO_EDAD = '
                          || PN_P_RANGO_EDAD
                          || '';
      END IF;

      IF ( PN_COD_USUARIO IS NOT NULL ) THEN
         L_CONDICIONES := L_CONDICIONES
                          || ' AND PAR.COD_USUARIO = '
                          || PN_COD_USUARIO
                          || '';
      END IF;

      L_SQL_STMT         := '
            SELECT
            PAR.COD_PARTICIPANTE_LARGO AS "COD_PARTICIPANTE_LARGO",
            PAR.COD_ROL AS "COD_ROL",
            PAR.COD_PARTICIPANTE AS "COD_PARTICIPANTE", 
            PAR.NOMBRES AS "NOMBRES", 
            PAR.APELLIDOS AS "APELLIDOS", 
            PAR.CORREO_ELECTRONICO AS "CORREO_ELECTRONICO",
            PAR.CMP_MEDICO AS "CMP_MEDICO",
            PAR.COD_USUARIO AS "COD_USUARIO"
            FROM PFC_PARTICIPANTE PAR
            LEFT JOIN PFC_PARTICIPANTE_GRUPO_DIAG PGD ON PGD.COD_PARTICIPANTE = PAR.COD_PARTICIPANTE
            WHERE PAR.P_ESTADO = 8 '

      || L_CONDICIONES;
      OPEN AC_LISTA FOR L_SQL_STMT;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_PARTICIPANTE]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_PARTICIPANTE;
  
  
  /* AUTOR     : RROCA*/
  /* CREADO    : 30/05/2019*/
  /* PROPOSITO : Obtiene el codigo de envio de la solicitud              */

   PROCEDURE SP_PFC_S_SOL_EVA_COD_ENVIO (
      PV_COD_SOL_EVA      IN                  NUMBER,
      CODIGOENVIOSOLEVA   OUT                 PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN CODIGOENVIOSOLEVA FOR SELECT
                                    SOLEVA.COD_SOL_EVA,
                                    SOLEVA.P_ESTADO_SOL_EVA,
                                    SOLEVA.CODIGO_ENVIO_ENV_ALER_MONIT
                                 FROM
                                    PFC_SOLICITUD_EVALUACION SOLEVA
                                 WHERE
                                    SOLEVA.COD_SOL_EVA = PV_COD_SOL_EVA;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta Exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_SOL_EVA_COD_ENVIO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_SOL_EVA_COD_ENVIO;

   PROCEDURE SP_PFC_S_CHECKLIST_REQUISITO (
      PN_COD_SOL_EVA     IN                 NUMBER,
      AC_LISTA           OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_ACTA        OUT                NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      
      SELECT SOLEVA.COD_INFORME_AUTO INTO PN_COD_ACTA 
      FROM ONTPFC.PFC_SOLICITUD_EVALUACION SOLEVA
      WHERE SOLEVA.COD_SOL_EVA = PN_COD_SOL_EVA;
      
      OPEN AC_LISTA FOR SELECT
                           DOCEVA.COD_ARCHIVO   AS "COD_ARCHIVO",
                           DOCEVA.COD_SOL_EVA     AS "COD_SOL_EVA"
                        FROM
                           ONTPFC.PFC_DOCUMENTO_EVALUACION     DOCEVA
                        WHERE
                           DOCEVA.COD_SOL_EVA = PN_COD_SOL_EVA;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Listado exitoso';
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_S_CHECKLIST_REQUISITO] : '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_CHECKLIST_REQUISITO;

   PROCEDURE SP_PFC_U_PARAMS_CORREO_EVA (
      PN_COD_SOL_EVA                  IN                              NUMBER,
      PN_COD_ENVIO_ENV_LIDER_TUMOR    IN                              NUMBER,
      PN_EST_CORREO_ENV_LIDER_TUMOR   IN                              NUMBER,
      AC_LISTA                        OUT                             PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO                OUT                             NUMBER,
      PV_MSG_RESULTADO                OUT                             VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      UPDATE ONTPFC.PFC_SOLICITUD_EVALUACION
      SET
         ESTADO_CORREO_ENV_LIDER_TUMOR = PN_EST_CORREO_ENV_LIDER_TUMOR,
         CODIGO_ENVIO_ENV_LIDER_TUMOR = PN_COD_ENVIO_ENV_LIDER_TUMOR
      WHERE
         COD_SOL_EVA = PN_COD_SOL_EVA;

      COMMIT;
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Actualizacion exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_U_PARAMS_CORREO_EVA] : '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_U_PARAMS_CORREO_EVA;

   PROCEDURE SP_PFC_U_ACTUALIZAR_INFORME (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PN_COD_INFORME     IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      V_MENSAJE VARCHAR2(100);
      V_EXC_VARIABLE EXCEPTION;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF NULLIF(
         PN_COD_INFORME,
         ''
      ) IS NULL THEN
         V_MENSAJE := 'No se envio el código del Informe';
         RAISE V_EXC_VARIABLE;
      END IF;

      UPDATE PFC_SOLICITUD_EVALUACION
      SET
         COD_INFORME_AUTO = PN_COD_INFORME
      WHERE
         COD_SOL_EVA = PN_COD_SOL_EVA;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Actualización exitosa';
   EXCEPTION
      WHEN V_EXC_VARIABLE THEN
         PN_COD_RESULTADO   := -2;
         PV_MSG_RESULTADO   := '[SP_PFC_U_ACTUALIZAR_INFORME] : ' || V_MENSAJE;
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_U_ACTUALIZAR_INFORME] : '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_U_ACTUALIZAR_INFORME;
   
    PROCEDURE SP_PFC_U_SOL_EVA_PTIPO (
      PN_COD_SOL_EVA                  IN                              NUMBER,
      PN_P_TIPO_EVA                   IN                              NUMBER,
      PN_COD_RESULTADO                OUT                             NUMBER,
      PV_MSG_RESULTADO                OUT                             VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      
      UPDATE ONTPFC.PFC_SOLICITUD_EVALUACION SET P_TIPO_EVA = PN_P_TIPO_EVA
      WHERE COD_SOL_EVA = PN_COD_SOL_EVA;

      COMMIT;
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Actualizacion exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_U_PARAMS_CORREO_EVA] : ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
   END SP_PFC_U_SOL_EVA_PTIPO;
   
   -- AUTOR      : JAZABACHE
   -- CREACION   : 08/05/2019
   -- PROPOSITO  : CONSULTAR DOCUMENTO PARA CHECKLIST REQUISITO Y MEDICAMENTO CONTINUADOR

   PROCEDURE SP_PFC_SI_DOCUMENTO_CONSULTA (
      PN_COD_SOL_EVA     IN                 NUMBER,
      PN_EDAD            IN                 NUMBER,
      AC_LISTA_HIST      OUT                PCK_PFC_CONSTANTE.TYPCUR,/*LISTAR HISTORIAL*/
      AC_LISTA_DOC       OUT                PCK_PFC_CONSTANTE.TYPCUR,/*LISTAR TIPO DE DOCUMENTOS*/
      PV_GRABAR          OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS

      PV_COD_PACIENTE             NUMBER;
      V_COD_MAC                   NUMBER;
      V_EDAD                      NUMBER;
      V_COD_SEV_CHK_REQ           NUMBER;
      V_COD_CHKLIST_REQ_PAC_SEQ   NUMBER;
      V_COD_CHKLIST_REQ_SEQ       NUMBER;
      V_DESC_DOCUMENTO            VARCHAR2(70);
      V_NUM_DOCUMENTO             NUMBER;
      V_COD_PACIENTE              VARCHAR(20);
      V_COD_DOC_EVA_SEQ           NUMBER;
      V_EXC_VARIABLE              EXCEPTION;
      V_COD_SOL_EVA               NUMBER;
      V_FLAG_TIPO_EVA             NUMBER;
      V_MENSAJE                   VARCHAR2(200);
      V_COD_CONTINUADOR_SEQ       NUMBER;
      V_COD_CONTINUADOR           NUMBER;
      
      CURSOR V_LIST_TIPO_DOC IS
        SELECT
           PR.COD_PARAMETRO,
           PR.NOMBRE,
           PR.VALOR1
        FROM
           PFC_GRUPO       GR,
           PFC_PARAMETRO   PR
        WHERE
           GR.COD_GRUPO = PR.COD_GRUPO
           AND GR.COD_GRUPO  = 30
           AND GR.ESTADO     = '1'
           AND PR.ESTADO     = '1'
           AND PR.COD_PARAMETRO <> PCK_PFC_CONSTANTE.PN_CHKLIST_TIPO_DOC_OTROS;
           
       CURSOR V_LIST_TIPO_DOC_CON IS
         SELECT
           PR.COD_PARAMETRO,
           PR.NOMBRE,
           PR.VALOR1
        FROM
           PFC_GRUPO       GR,
           PFC_PARAMETRO   PR
        WHERE
           GR.COD_GRUPO = PR.COD_GRUPO
           AND GR.COD_GRUPO  = 30
           AND GR.ESTADO     = '1'
           AND PR.ESTADO     = '1'
           AND PR.COD_PARAMETRO <> 85
           AND PR.COD_PARAMETRO <> PCK_PFC_CONSTANTE.PN_CHKLIST_TIPO_DOC_OTROS;
       
       
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      DBMS_OUTPUT.PUT_LINE('DATA 1...');
      CASE
        WHEN NULLIF(PN_COD_SOL_EVA,'') IS NULL THEN
          V_MENSAJE := 'NO SE ENCONTRO LA SOLICITUD DE EVALUACION';
          RAISE V_EXC_VARIABLE;
        WHEN NULLIF(PN_EDAD,'') IS NULL THEN
          V_MENSAJE := 'NO SE ENCONTRO LA EDAD DEL PACIENTE';
          RAISE V_EXC_VARIABLE;
        ELSE
          V_MENSAJE:= 'CONSULTA CORRECTA';
      END CASE;
          
      BEGIN
        
        SELECT SE.COD_SOL_EVA
          INTO V_COD_SOL_EVA
          FROM PFC_SOLICITUD_EVALUACION SE
         INNER JOIN PFC_MEDICAMENTO_NUEVO MN
            ON SE.COD_SOL_EVA = MN.COD_SOL_EVA
         WHERE SE.COD_SOL_EVA = PN_COD_SOL_EVA;
         
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
         V_COD_SOL_EVA := NULL;
      END;
      
      IF V_COD_SOL_EVA IS NOT NULL THEN
           V_FLAG_TIPO_EVA := 1; --MEDICAMENTO NUEVO
      ELSE
        V_FLAG_TIPO_EVA := 2; --MEDICAMENTO CONTINUADOR
      END IF;
        
      BEGIN
        SELECT CO.COD_CONTINUADOR
          INTO V_COD_CONTINUADOR
          FROM PFC_CONTINUADOR CO
         WHERE CO.COD_SOL_EVA = PN_COD_SOL_EVA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          V_COD_CONTINUADOR :=NULL;
      END;
        
        IF V_COD_CONTINUADOR IS NOT NULL THEN
           V_FLAG_TIPO_EVA := 2; --MEDICAMENTO CONTINUADOR
        END IF;
            
      DBMS_OUTPUT.PUT_LINE('DATA 2...');
      BEGIN
         SELECT S.COD_AFI_PACIENTE
           INTO V_COD_PACIENTE
           FROM PFC_SOLBEN S
          INNER JOIN PFC_SOLICITUD_PRELIMINAR   SP ON S.COD_SCG = SP.COD_SCG
          INNER JOIN PFC_SOLICITUD_EVALUACION   SE ON SP.COD_SOL_PRE = SE.COD_SOL_PRE
          WHERE SE.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN OTHERS THEN
            V_MENSAJE := 'ERROR, NO SE ENCONTRO EL CODIGO DEL PACIENTE';
            RAISE V_EXC_VARIABLE;
      END;

      BEGIN
        SELECT COUNT(1)
          INTO V_NUM_DOCUMENTO
          FROM PFC_DOCUMENTO_EVALUACION DE
         WHERE DE.COD_SOL_EVA = PN_COD_SOL_EVA;
         DBMS_OUTPUT.PUT_LINE('V_NUM_DOCUMENTO : '||V_NUM_DOCUMENTO);
      EXCEPTION
        WHEN OTHERS THEN
           V_NUM_DOCUMENTO := NULL;
           V_MENSAJE := 'ERROR, NO SE ENCONTRO LA SOLICITUD DE EVALUACION';
           RAISE V_EXC_VARIABLE;
      END;
               
         /*IF V_NUM_DOCUMENTO > 0 THEN*/
         
         /*ELSE*/
      SELECT PR.COD_MAC
        INTO V_COD_MAC
        FROM ONTPFC.PFC_SOLICITUD_EVALUACION   EV,
             ONTPFC.PFC_SOLICITUD_PRELIMINAR   PR
       WHERE
         EV.COD_SOL_PRE = PR.COD_SOL_PRE
         AND EV.COD_SOL_EVA = PN_COD_SOL_EVA;
         
            DBMS_OUTPUT.PUT_LINE('V_COD_MAC : '||V_COD_MAC);
            --SELECT * FROM PFC_MEDICAMENTO_NUEVO MN WHERE MN.COD_SOL_EVA = 224
            DBMS_OUTPUT.PUT_LINE('V_FLAG_TIPO_EVA : '|| V_FLAG_TIPO_EVA);
            IF V_FLAG_TIPO_EVA = 1 THEN
               
               DBMS_OUTPUT.PUT_LINE('1. V_NUM_DOCUMENTO : '||V_NUM_DOCUMENTO);
               IF V_NUM_DOCUMENTO = 0 THEN
                 
                  UPDATE PFC_MEDICAMENTO_NUEVO MN
                     SET MN.CHK_REQ_PAC_GRABAR = '0'
                   WHERE MN.COD_SOL_EVA = PN_COD_SOL_EVA;
                 
                  FOR V_ITEM IN V_LIST_TIPO_DOC LOOP
                     DBMS_OUTPUT.PUT_LINE('>>>> ' || V_ITEM.VALOR1);
                     V_EDAD := TO_NUMBER(V_ITEM.VALOR1);
                     DBMS_OUTPUT.PUT_LINE('V_EDAD : '|| V_EDAD|| ' <<>> PN_EDAD : '|| PN_EDAD);
                     
                     IF ( V_EDAD < PN_EDAD ) OR NULLIF(V_EDAD,'') IS NULL  THEN

                        SELECT SQ_PFC_DOC_EVA_COD_DOC_EVA.NEXTVAL
                          INTO V_COD_DOC_EVA_SEQ
                          FROM DUAL;
                        
                        INSERT INTO PFC_DOCUMENTO_EVALUACION (
                           COD_DOCUMENTO_EVALUACION,
                           COD_SOL_EVA,
                           COD_AFI_PACIENTE,
                           COD_MAC,
                           P_TIPO_DOCUMENTO,
                           DESCRIPCION_DOCUMENTO,
                           ESTADO
                        ) VALUES (
                           V_COD_DOC_EVA_SEQ,
                           PN_COD_SOL_EVA,
                           V_COD_PACIENTE,
                           V_COD_MAC,
                           V_ITEM.COD_PARAMETRO,
                           V_ITEM.NOMBRE,
                           '1'
                        );

                        DBMS_OUTPUT.PUT_LINE('SE INSERTO EN PFC_DOCUMENTO_EVALUACION : ' || V_COD_DOC_EVA_SEQ);
                     END IF;

                  END LOOP;
               
               END IF;
               
               SELECT MN.CHK_REQ_PAC_GRABAR
                 INTO PV_GRABAR
                 FROM PFC_MEDICAMENTO_NUEVO MN
                WHERE MN.COD_SOL_EVA = PN_COD_SOL_EVA;
                DBMS_OUTPUT.PUT_LINE('2. PV_GRABAR MEDICAMENTO NUEVO : '||PV_GRABAR);
                
            ELSIF V_FLAG_TIPO_EVA = 2 THEN
              
              
              IF V_COD_CONTINUADOR IS NULL THEN
                
                SELECT SQ_PFC_CONTINUADOR_COD_CONT.NEXTVAL
                  INTO V_COD_CONTINUADOR_SEQ
                  FROM DUAL;
                  
                INSERT INTO PFC_CONTINUADOR (COD_CONTINUADOR, COD_SOL_EVA, GRABAR)
                VALUES (V_COD_CONTINUADOR_SEQ, PN_COD_SOL_EVA, '0');
                COMMIT;
                
              END IF;
              
              IF V_NUM_DOCUMENTO = 0 THEN
              
                 UPDATE PFC_CONTINUADOR C
                    SET C.GRABAR = '0'
                  WHERE C.COD_SOL_EVA = PN_COD_SOL_EVA;
                
                  FOR V_ITEM IN V_LIST_TIPO_DOC_CON LOOP
                     DBMS_OUTPUT.PUT_LINE('>>>> ' || V_ITEM.VALOR1);
                     V_EDAD := TO_NUMBER(V_ITEM.VALOR1);
                     DBMS_OUTPUT.PUT_LINE('V_EDAD : '|| V_EDAD|| ' <<>> PN_EDAD : '|| PN_EDAD);
                       
                     IF ( V_EDAD < PN_EDAD ) OR NULLIF(V_EDAD,'') IS NULL  THEN

                        SELECT SQ_PFC_DOC_EVA_COD_DOC_EVA.NEXTVAL
                          INTO V_COD_DOC_EVA_SEQ
                          FROM DUAL;
                          
                        INSERT INTO PFC_DOCUMENTO_EVALUACION (
                           COD_DOCUMENTO_EVALUACION,
                           COD_SOL_EVA,
                           COD_AFI_PACIENTE,
                           COD_MAC,
                           P_TIPO_DOCUMENTO,
                           DESCRIPCION_DOCUMENTO,
                           ESTADO
                        ) VALUES (
                           V_COD_DOC_EVA_SEQ,
                           PN_COD_SOL_EVA,
                           V_COD_PACIENTE,
                           V_COD_MAC,
                           V_ITEM.COD_PARAMETRO,
                           V_ITEM.NOMBRE,
                           '1'
                        );

                        DBMS_OUTPUT.PUT_LINE('SE INSERTO EN PFC_DOCUMENTO_EVALUACION CONTINUADOR: ' || V_COD_DOC_EVA_SEQ);
                     END IF;

                  END LOOP;
                  
              END IF;
              
              BEGIN
                SELECT C.GRABAR
                  INTO PV_GRABAR
                  FROM PFC_CONTINUADOR C
                 WHERE C.COD_SOL_EVA = PN_COD_SOL_EVA;
              EXCEPTION
                WHEN NO_DATA_FOUND THEN
                  PV_GRABAR := NULL;
              END;
              
            END IF;
            
            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'SE CREARON LOS REGISTROS.';
            
         /*END IF;*/
         
         OPEN AC_LISTA_DOC FOR SELECT
                  DE.COD_DOCUMENTO_EVALUACION,
                  DE.P_TIPO_DOCUMENTO,
                  (SELECT PP.NOMBRE FROM PFC_PARAMETRO PP WHERE PP.COD_PARAMETRO = DE.P_TIPO_DOCUMENTO) AS NOMBRE_TIPO_DOCUMENTO,
                  DE.DESCRIPCION_DOCUMENTO,
                  DE.P_ESTADO,
                  (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = DE.P_ESTADO ) AS DESCRIPCION_ESTADO,
                  DE.COD_ARCHIVO
               FROM PFC_SOLICITUD_EVALUACION SE
                  INNER JOIN PFC_DOCUMENTO_EVALUACION DE ON DE.COD_SOL_EVA = SE.COD_SOL_EVA
               WHERE
                  SE.COD_SOL_EVA = PN_COD_SOL_EVA
                  AND DE.ESTADO = '1'
                  AND ( DE.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR
                        OR DE.P_ESTADO IS NULL
                        OR ( ( DE.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                               AND DE.P_TIPO_DOCUMENTO  = PCK_PFC_CONSTANTE.PN_TIPO_DOC_RECETA_MED )
                             OR ( DE.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                  AND DE.P_TIPO_DOCUMENTO  = PCK_PFC_CONSTANTE.PN_TIPO_DOC_INFO_MED )
                             OR ( DE.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_ELIMINAR
                                  AND DE.P_TIPO_DOCUMENTO  = PCK_PFC_CONSTANTE.PN_TIPO_DOC_EVA_GERIAT ) ) );
         
         IF V_FLAG_TIPO_EVA = 1 THEN
           
           OPEN AC_LISTA_HIST FOR
               SELECT
                  DE.COD_DOCUMENTO_EVALUACION,
                  MN.NRO_LINEA_TRA   AS LINEA_TRATAMIENTO,
                  (SELECT M.DESCRIPCION FROM PFC_MAC M WHERE M.COD_MAC = SP.COD_MAC) AS DESCRIPCION_MAC,
                  DE.P_TIPO_DOCUMENTO,
                  (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = DE.P_TIPO_DOCUMENTO) AS NOMBRE_TIPO_DOCUMENTO,
                  DE.DESCRIPCION_DOCUMENTO,
                  TO_CHAR(DE.FEC_CARGA,'DD/MM/YYYY') AS FEC_CARGA,
                  DE.COD_ARCHIVO
               FROM
                  PFC_SOLICITUD_PRELIMINAR SP
                  INNER JOIN PFC_SOLICITUD_EVALUACION    SE   ON SP.COD_SOL_PRE = SE.COD_SOL_PRE
                  INNER JOIN PFC_MEDICAMENTO_NUEVO       MN   ON SE.COD_SOL_EVA = MN.COD_SOL_EVA
                  INNER JOIN PFC_DOCUMENTO_EVALUACION    DE   ON DE.COD_SOL_EVA = SE.COD_SOL_EVA
               WHERE
                  DE.COD_AFI_PACIENTE = V_COD_PACIENTE
                  AND DE.P_ESTADO = PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR
               ORDER BY
                  DE.P_TIPO_DOCUMENTO , DE.COD_DOCUMENTO_EVALUACION DESC;
         
         END IF;
         
      COMMIT;
   EXCEPTION
      WHEN V_EXC_VARIABLE THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SI_DOCUMENTO_CONSULTA]] '|| TO_CHAR(SQLCODE)|| ' : '|| SQLERRM;
   END SP_PFC_SI_DOCUMENTO_CONSULTA;


   -- AUTHOR  : JAZABACHE
   -- CREATED : 25/01/2019
   -- PURPOSE : CARGAR ARCHIVO CHECKLIST DE REQUISITO DEL PACIENTE Y DOCUMENTO CONTINUADOR

   PROCEDURE SP_PFC_SI_DOCUMENTO_CARGAR (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PN_COD_CHKLST_REQ    IN                   NUMBER,
      PN_COD_MAC           IN                   NUMBER,
      PN_TIPO_DOCUMENTO    IN                   NUMBER,
      PV_DESCRIPCION_DOC   IN                   VARCHAR2,
      PN_ESTADO_DOC        IN                   NUMBER,
      PV_URL_DESCARGA      IN                   VARCHAR2,
      PN_COD_ARCHIVO       IN                   NUMBER,
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   ) AS
      
      V_COD_DOC_EVA_SEQ           NUMBER;
      V_MENSAJE                   VARCHAR2(100);
      V_COD_AFI_PACIENTE          VARCHAR2(20);
      V_EXC_VARIABLE              EXCEPTION;
      V_EXC_ERROR                 EXCEPTION;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
                 
      IF PN_ESTADO_DOC = 89 THEN
        V_MENSAJE := 'EL ESTADO NO CORRESPONDE A LA CARGA DEL ARCHIVO';
        RAISE V_EXC_VARIABLE;
      END IF;
       
        IF NULLIF(PN_COD_CHKLST_REQ,'') IS NULL THEN
             
             BEGIN
                SELECT SO.COD_AFI_PACIENTE
                  INTO V_COD_AFI_PACIENTE
                  FROM PFC_SOLICITUD_EVALUACION SE
                 INNER JOIN PFC_SOLICITUD_PRELIMINAR PE ON SE.COD_SOL_PRE = PE.COD_SOL_PRE
                 INNER JOIN PFC_SOLBEN SO ON PE.COD_SCG = SO.COD_SCG
                 WHERE SE.COD_SOL_EVA = PN_COD_SOL_EVA;
             EXCEPTION
               WHEN NO_DATA_FOUND THEN
                 V_MENSAJE := 'NO SE ENCONTRO EL AFILIADO ASOCIADO A LA SOLICITUD';
                 RAISE V_EXC_VARIABLE;
               WHEN OTHERS THEN
                 V_MENSAJE := '[ERROR CODIGO AFILIADO] '|| TO_CHAR(SQLCODE)|| ' : '|| SQLERRM;
                 RAISE V_EXC_ERROR;
             END;
        
                SELECT SQ_PFC_DOC_EVA_COD_DOC_EVA.NEXTVAL
                  INTO V_COD_DOC_EVA_SEQ
                  FROM DUAL;

                INSERT INTO PFC_DOCUMENTO_EVALUACION (
                   COD_DOCUMENTO_EVALUACION,
                   COD_SOL_EVA,
                   COD_AFI_PACIENTE,
                   COD_MAC,
                   P_TIPO_DOCUMENTO,
                   DESCRIPCION_DOCUMENTO,
                   FEC_CARGA,
                   ESTADO,
                   P_ESTADO,
                   COD_ARCHIVO
                ) VALUES (
                   V_COD_DOC_EVA_SEQ,
                   PN_COD_SOL_EVA,
                   V_COD_AFI_PACIENTE,
                   PN_COD_MAC,
                   PN_TIPO_DOCUMENTO,
                   PV_DESCRIPCION_DOC,
                   SYSDATE,
                   '1',
                   PN_ESTADO_DOC,
                   PN_COD_ARCHIVO
                );

          PN_COD_RESULTADO   := 0;
          PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_INSERTAR_EXITO;
        
        ELSE
           DBMS_OUTPUT.PUT_LINE('--  3  --');
           DBMS_OUTPUT.PUT_LINE('--  PN_TIPO_DOCUMENTO  : ' || PN_TIPO_DOCUMENTO);
           
          
          UPDATE PFC_DOCUMENTO_EVALUACION DE
             SET DE.COD_ARCHIVO = PN_COD_ARCHIVO,
                 DE.FEC_CARGA = SYSDATE,
                 DE.P_ESTADO = 88
           WHERE DE.COD_DOCUMENTO_EVALUACION = PN_COD_CHKLST_REQ;
          
          PN_COD_RESULTADO   := 0;
          PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;

        END IF;

      COMMIT;
   EXCEPTION
      WHEN V_EXC_VARIABLE THEN
         PN_COD_RESULTADO  := 1;
         PV_MSG_RESULTADO  := V_MENSAJE;
         
      WHEN V_EXC_ERROR THEN
         PN_COD_RESULTADO  := -2;
         PV_MSG_RESULTADO  := V_MENSAJE;
         
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_SI_CHKLIST_CARGAR] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SI_DOCUMENTO_CARGAR;


   -- AUTOR      : JAZABACHE
   -- CREADO     : 25/01/2019
   -- PROPOSITO  : CONSULTAR ACTUALIZAR ELIMINAR DOCUMENTO CHECKLIST Y CONTINUADOR

   PROCEDURE SP_PFC_SUI_DOCUMENTO_ELIMINAR (
      PN_COD_CHKLST_REQ   IN                  NUMBER,
      PN_ESTADO_DOC       IN                  NUMBER,
      PN_COD_RESULTADO    OUT                 NUMBER,
      PV_MSG_RESULTADO    OUT                 VARCHAR2
   ) AS
   V_COD_SOL_EVA                NUMBER;
   V_COD_AFI_PACIENTE           VARCHAR2(20);
   V_COD_MAC                    NUMBER;
   V_P_TIPO_DOCUMENTO           NUMBER;
   V_DESCRIPCION_DOCUMENTO      VARCHAR2(100);
   V_FEC_CARGA                  DATE;
   V_P_ESTADO                   NUMBER;
   V_COD_ARCHIVO                NUMBER;
   V_USUARIO_CREACION           NUMBER;
   V_FECHA_CREACION             DATE;
   V_USUARIO_MODIFICACION       NUMBER;
   V_FECHA_MODIFICACION         DATE;
   V_COD_DOC_EVA_SEQ            NUMBER;
   
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF PN_ESTADO_DOC <> PCK_PFC_CONSTANTE.PN_CHKLIST_BOTON_CARGAR THEN
        
         SELECT DE.COD_SOL_EVA, DE.COD_AFI_PACIENTE,DE.COD_MAC, DE.P_TIPO_DOCUMENTO, 
                DE.DESCRIPCION_DOCUMENTO, DE.FEC_CARGA, DE.P_ESTADO, DE.COD_ARCHIVO,
                DE.USUARIO_CREACION, DE.FECHA_CREACION, DE.USUARIO_MODIFICACION, DE.FECHA_MODIFICACION
           INTO V_COD_SOL_EVA, V_COD_AFI_PACIENTE, V_COD_MAC, V_P_TIPO_DOCUMENTO,
                V_DESCRIPCION_DOCUMENTO, V_FEC_CARGA, V_P_ESTADO, V_COD_ARCHIVO,
                V_USUARIO_CREACION, V_FECHA_CREACION, V_USUARIO_MODIFICACION, V_FECHA_MODIFICACION
           FROM PFC_DOCUMENTO_EVALUACION DE
          WHERE DE.COD_DOCUMENTO_EVALUACION = PN_COD_CHKLST_REQ;
          
         
         IF V_P_TIPO_DOCUMENTO <> PCK_PFC_CONSTANTE.PN_CHKLIST_TIPO_DOC_OTROS THEN
            
             UPDATE PFC_DOCUMENTO_EVALUACION DEV
                SET DEV.P_ESTADO = PN_ESTADO_DOC,
                    DEV.COD_ARCHIVO = NULL
              WHERE DEV.COD_DOCUMENTO_EVALUACION = PN_COD_CHKLST_REQ;
                     
             SELECT SQ_PFC_DOC_EVA_COD_DOC_EVA.NEXTVAL
               INTO V_COD_DOC_EVA_SEQ
               FROM DUAL;

             INSERT INTO PFC_DOCUMENTO_EVALUACION(COD_DOCUMENTO_EVALUACION, COD_SOL_EVA, COD_AFI_PACIENTE, COD_MAC,
                         P_TIPO_DOCUMENTO, DESCRIPCION_DOCUMENTO, FEC_CARGA, ESTADO, P_ESTADO, COD_ARCHIVO,
                         USUARIO_CREACION, FECHA_CREACION, USUARIO_MODIFICACION, FECHA_MODIFICACION)
                  VALUES (V_COD_DOC_EVA_SEQ, V_COD_SOL_EVA, V_COD_AFI_PACIENTE, V_COD_MAC,
                         V_P_TIPO_DOCUMENTO, V_DESCRIPCION_DOCUMENTO, V_FEC_CARGA, '0', PN_ESTADO_DOC, V_COD_ARCHIVO,
                         V_USUARIO_CREACION, V_FECHA_CREACION, V_USUARIO_MODIFICACION, V_FECHA_MODIFICACION);
         
         ELSE
           -- PARA TIPO DE DOCUMENTOS OTROS
            UPDATE PFC_DOCUMENTO_EVALUACION DEV
               SET DEV.P_ESTADO = PN_ESTADO_DOC,
                   DEV.ESTADO = '0'
             WHERE DEV.COD_DOCUMENTO_EVALUACION = PN_COD_CHKLST_REQ;
            
         END IF;

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
      ELSE
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'El CODIGO DE ESTADO '
                             || PN_ESTADO_DOC
                             || ' ES INCORRECTO.';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SUI_DOCUMENTO_ELIMINAR]: '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SUI_DOCUMENTO_ELIMINAR;
   
   -- AUTOR     : JAZABACHE
   -- CREADO    : 25/01/2019
   -- PROPOSITO : GUARDAR CAMBIOS CHECKLIST_REQUISITO PASO 2

   PROCEDURE SP_PFC_SU_DOC_REQ_GUARDAR (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PN_COD_ROL_USUARIO   IN                   NUMBER,/* CODIGO DE ROL DE USUARIO*/
      PV_FECHA_ESTADO      IN                   VARCHAR2, /* FECHA DE ESTADO*/
      PN_COD_USUARIO       IN                   NUMBER,/* CODIGO DE USUARIO*/
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   ) AS

      V_COD_SEV_CHK_REQ_PAC   NUMBER;
      V_ESTADO_SOL_EVA        NUMBER;
      V_EXC_ESTADO_SOL_EVA EXCEPTION;
      V_COD_RESULTADO EXCEPTION; /* SEGUIMIENTO*/
      V_COD_RESULTADO_SEG     NUMBER; /* SEGUIMIENTO*/
      V_MSG_RESULTADO_SEG     VARCHAR2(4000); /* SEGUIMIENTO*/
      V_ESTADO_SOL_EVA1       NUMBER; /* SEGUIMIENTO*/
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA := NULL;
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;
         
      UPDATE PFC_MEDICAMENTO_NUEVO  MN
         SET MN.CHK_REQ_PAC_GRABAR = '1'
       WHERE MN.COD_SOL_EVA = PN_COD_SOL_EVA;

      IF V_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_PASO1 THEN
        
         UPDATE PFC_SOLICITUD_EVALUACION SEV
         SET
            SEV.P_ESTADO_SOL_EVA = PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_PASO2
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      END IF;
      
      UPDATE PFC_SOLICITUD_EVALUACION SEV
       SET
          SEV.COD_AUTO_PERTE = PN_COD_USUARIO,
          SEV.P_ROL_RESP_PENDIENTE_EVA = PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE
       WHERE
          SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_ACTUALIZAR_EXITO;
    
     /* SEGUIMIENTO BEGIN */
      BEGIN
         SELECT
            SEV.P_ESTADO_SOL_EVA
         INTO V_ESTADO_SOL_EVA1
         FROM
            PFC_SOLICITUD_EVALUACION SEV
         WHERE
            SEV.COD_SOL_EVA = PN_COD_SOL_EVA;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_ESTADO_SOL_EVA1 := NULL;
            RAISE V_EXC_ESTADO_SOL_EVA;
      END;

      ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO(
         PN_COD_SOL_EVA,/* NUMBER*/
         V_ESTADO_SOL_EVA1,
         PV_FECHA_ESTADO,
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL DE USUARIO*/
         PN_COD_USUARIO,/* CODIGO DE USUARIO*/
         PN_COD_ROL_USUARIO, /* CODIGO DE ROL D USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         PN_COD_USUARIO, /* CODIGO DE USUARIO RESPONSABLE DE REGISTRO DE ESTADO*/
         V_COD_RESULTADO_SEG,/* NUMBER*/
         V_MSG_RESULTADO_SEG
      ); /* VARCHAR*/

      IF V_COD_RESULTADO_SEG <> 0 THEN
         RAISE V_COD_RESULTADO; /* EXCEPTION*/
      END IF;           
      /* SEGUIMIENTO END*/
      COMMIT;
   EXCEPTION
      WHEN V_COD_RESULTADO THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR, AL INSERTAR LA TABLA SEGUIMIENTO [[ONTPFC.PCK_PFC_BANDEJA_EVALUACION.SP_PFC_I_SEGUIMIENTO]]' 
         || V_MSG_RESULTADO_SEG;
      WHEN V_EXC_ESTADO_SOL_EVA THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'ERROR: NO SE ENCONTRO EL ESTADO DE LA SOLICITUD DE EVALUACION';
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SU_DOC_REQ_GUARDAR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SU_DOC_REQ_GUARDAR;
   
   PROCEDURE SP_PFC_U_ACTAESC_PROG_CMAC (
      PN_COD_PROGRAMACION_CMAC       IN                   NUMBER,
      PV_REPORTE_ACTA_ESCANEADA      IN                   VARCHAR2,
      PN_COD_RESULTADO               OUT                  NUMBER,
      PV_MSG_RESULTADO               OUT                  VARCHAR2
   ) AS

   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'VALOR INICIAL';
      
      UPDATE ONTPFC.PFC_PROGRAMACION_CMAC SET REPORTE_ACTA_ESCANEADA = PV_REPORTE_ACTA_ESCANEADA 
      WHERE COD_PROGRAMACION_CMAC = PN_COD_PROGRAMACION_CMAC;
      
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'EXITO';
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_U_ACTAESC_PROG_CMAC]] ' || TO_CHAR(SQLCODE) || ' : '  || SQLERRM;
   END SP_PFC_U_ACTAESC_PROG_CMAC;
   
   PROCEDURE SP_PFC_S_INFORME_AUTO_CMAC (
      PN_COD_SOL_EVA                 IN                   NUMBER,
      PN_COD_INFORME_AUTO            OUT                  NUMBER,
      PN_COD_ACTA_CMAC_FTP           OUT                  NUMBER,
      PV_COD_REP_ACTA_SCAN           OUT              VARCHAR2,
      PN_COD_RESULTADO               OUT                  NUMBER,
      PV_MSG_RESULTADO               OUT                  VARCHAR2
   ) AS

   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'VALOR INICIAL';
      
      SELECT SOLEVA.COD_INFORME_AUTO, PC.COD_ACTA_FTP, PC.REPORTE_ACTA_ESCANEADA INTO PN_COD_INFORME_AUTO, PN_COD_ACTA_CMAC_FTP, PV_COD_REP_ACTA_SCAN
      FROM ONTPFC.PFC_SOLICITUD_EVALUACION SOLEVA
      LEFT JOIN ONTPFC.PFC_PROGRAMACION_CMAC_DET PCD ON PCD.COD_SOL_EVA = SOLEVA.COD_SOL_EVA
      LEFT JOIN ONTPFC.PFC_PROGRAMACION_CMAC PC ON PC.COD_PROGRAMACION_CMAC = PCD.COD_PROGRAMACION_CMAC
      WHERE SOLEVA.COD_SOL_EVA = PN_COD_SOL_EVA;
      
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'EXITO';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_INFORME_AUTO_CMAC]] ' || TO_CHAR(SQLCODE) || ' : '  || SQLERRM;
   END SP_PFC_S_INFORME_AUTO_CMAC;

END PCK_PFC_BANDEJA_EVALUACION;

/
--------------------------------------------------------
--  DDL for Package Body PCK_PFC_BANDEJA_MONITOREO
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "ONTPFC"."PCK_PFC_BANDEJA_MONITOREO" AS

    PROCEDURE sp_pfc_s_bandeja_monitoreo (
        pv_cod_paciente            IN VARCHAR2,
        pn_estado_monitoreo        IN NUMBER,
        pv_cod_clinica             IN VARCHAR2,
        pv_fecha_monitoreo         IN VARCHAR2,
        pn_cod_resp_monitoreo      IN NUMBER,
        ac_listamonitoreo          OUT pck_pfc_constante.typcur,
        pn_cod_resultado           OUT NUMBER,
        pv_msg_resultado           OUT VARCHAR2
    ) AS
        l_sql_stmt       VARCHAR2(32767);
        l_condiciones    VARCHAR2(4000);
        l_cond_resp_mon  VARCHAR2(4000);
        l_cond_grp       VARCHAR2(4000);
        l_tiene_conf     NUMBER;
        l_cod_grp_diag   NUMBER;
        l_edad_minimo    NUMBER;
        l_edad_maximo    NUMBER;
        CURSOR ac_condicion IS
            SELECT pgd.cod_grp_diag, pgd.v_edad_min, pgd.v_edad_max FROM pfc_participante par 
            INNER JOIN (
                select pgd.cod_participante, pgd.cod_grp_diag, pgd.p_rango_edad, parms.valor1, regexp_substr(parms.valor1,'([^-]+)',1,1,'',1) AS v_edad_min, regexp_substr(parms.valor1,'([^-]+)',1,2,'',1) AS v_edad_max
                from pfc_participante_grupo_diag pgd
                inner join pfc_parametro parms on parms.cod_parametro = pgd.p_rango_edad
            ) pgd ON pgd.cod_participante = par.cod_participante 
            WHERE par.p_estado=8 AND par.cod_usuario = pn_cod_resp_monitoreo AND pgd.cod_grp_diag IS NOT NULL;
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'No se puede obtener la lista';
        
        IF ( pv_cod_paciente <> '000' ) THEN
            l_condiciones := l_condiciones || ' AND sol.cod_afi_paciente = ''' || pv_cod_paciente || ''' ';
        END IF;

        IF ( pn_estado_monitoreo <> 0 ) THEN
            l_condiciones := l_condiciones || ' AND mon.p_estado_monitoreo = ' || pn_estado_monitoreo || ' ';
        END IF;

        IF ( pv_cod_clinica <> '000' ) THEN
            l_condiciones := l_condiciones || ' AND sol.cod_clinica = ''' || pv_cod_clinica || ''' ';
        END IF;
        
        IF ( pv_fecha_monitoreo is not null) THEN
            l_condiciones := l_condiciones || ' AND mon.fec_prox_monitoreo <= TO_DATE('''||pv_fecha_monitoreo||''',''DD/MM/YY'') ';
        END IF;
        
        IF ( pn_cod_resp_monitoreo <> 0 ) THEN
            --SOLO LISTA EL CODIGO INGRESADO
            l_cond_resp_mon := '
            (select par.apellidos||'', ''||par.nombres from ontpfc.pfc_participante par where par.cod_usuario = '||pn_cod_resp_monitoreo||' and par.p_estado = 8 and rownum <= 1) as "NOM_RESPONSABLEMONITOREO",' 
            || pn_cod_resp_monitoreo || ' as "COD_RESPONSABLEMONITOREO",';
            
            --ACTUALIZA LA SENTENCIA PARA UN FILTRO POR COD_RESP_MONITOREO
            l_cond_grp := ' AND (1=2';
            FOR v_item IN ac_condicion LOOP
                l_cond_grp := l_cond_grp || ' OR (sol.edad_paciente >= ' || v_item.v_edad_min || ' AND sol.edad_paciente <= ' || v_item.v_edad_max || ' AND sol.cod_grp_diag = ' || v_item.cod_grp_diag || ')';    
            END LOOP;
            l_cond_grp := l_cond_grp || ')';   
        ELSE
            --LISTA EL CODIGO DE RESP MONITOREO ASIGNADO DE ACUERDO A LA EDAD Y GRUPO DIAGNOSTICO

            l_cond_resp_mon := '
            (select par.apellidos||'', ''||par.nombres from ontpfc.pfc_participante par
            inner join (
                select pgd.cod_participante, pgd.cod_grp_diag, pgd.p_rango_edad, parms.valor1, regexp_substr(parms.valor1,''([^-]+)'',1,1,'''',1) AS v_edad_min, regexp_substr(parms.valor1,''([^-]+)'',1,2,'''',1) AS v_edad_max
                from pfc_participante_grupo_diag pgd
                inner join pfc_parametro parms on parms.cod_parametro = pgd.p_rango_edad) pgd 
            on pgd.cod_participante = par.cod_participante
            where par.p_estado='||ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_PARTICIP_ACTIVO||' and pgd.cod_grp_diag = sol.cod_grp_diag and sol.edad_paciente >= pgd.v_edad_min and sol.edad_paciente <= pgd.v_edad_max
            and rownum <= 1
            ) as "NOM_RESPONSABLEMONITOREO",
            (select par.cod_usuario from ontpfc.pfc_participante par
            inner join (
                select pgd.cod_participante, pgd.cod_grp_diag, pgd.p_rango_edad, parms.valor1, regexp_substr(parms.valor1,''([^-]+)'',1,1,'''',1) AS v_edad_min, regexp_substr(parms.valor1,''([^-]+)'',1,2,'''',1) AS v_edad_max
                from pfc_participante_grupo_diag pgd
                inner join pfc_parametro parms on parms.cod_parametro = pgd.p_rango_edad) pgd 
            on pgd.cod_participante = par.cod_participante
            where par.p_estado='||ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_PARTICIP_ACTIVO||' and pgd.cod_grp_diag = sol.cod_grp_diag and sol.edad_paciente >= pgd.v_edad_min and sol.edad_paciente <= pgd.v_edad_max
            and rownum <= 1
            ) as "COD_RESPONSABLEMONITOREO",';
        END IF;

        l_sql_stmt := '
        select 
        mon.cod_monitoreo as "COD_MONITOREO",
        mon.cod_desc_monitoreo as "COD_DESC_MONITOREO", 
        solev.cod_desc_sol_eva as "COD_DESC_SOL_EVA",
        histlin.fec_aprobacion as "FECHAAPROBACION",
        sol.cod_scg_solben as "COD_SCG_SOLBEN",
        sol.nro_cg as "NRO_CG",
        sol.cod_afi_paciente as "COD_AFILIADO",
        sol.cod_diagnostico as "COD_DIAGNOSTICO",
        sol.cod_grp_diag as "COD_GRUPO_DIAGNOSTICO",
        sol.medico_tratante_prescriptor as "MEDICO_TRATANTE",
        sol.edad_paciente as "EDAD_PACIENTE",
        mac.descripcion as "NOM_MEDICAMENTO",
        mac.cod_mac as "COD_MEDICAMENTO",
        histlin.linea_trat as "LINEA_TRAT",
        histlin.fec_inicio as "FEC_INI_LINEA_TRAT",
        sol.cod_afi_paciente as "COD_PACIENTE",
        sol.cod_clinica as "COD_CLINICA",
        mon.p_estado_monitoreo as "COD_ESTADOMONITOREO",
        (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = mon.p_estado_monitoreo) AS "DESC_ESTADOMONITOREO",
        mon.fec_prox_monitoreo as "FECHAPROXIMOMONITOREO",'
        || l_cond_resp_mon ||
        'solev.cod_sol_eva as "COD_SOL_EVA",
        histlin.cod_hist_linea_trat as "COD_HIST_LINEA_TRAT",
        evo.cod_evolucion as "COD_EVOLUCION",
        solev.cod_informe_auto as "COD_INFORME_AUTO"
        from ontpfc.pfc_monitoreo mon
        inner join ontpfc.pfc_solicitud_evaluacion solev on solev.cod_sol_eva=mon.cod_sol_eva
        inner join ontpfc.pfc_solicitud_preliminar solpre on solpre.cod_sol_pre=solev.cod_sol_pre
        inner join ontpfc.pfc_solben sol on sol.cod_scg=solpre.cod_scg
        inner join ontpfc.pfc_mac mac on mac.cod_mac = solpre.cod_mac
        inner join ontpfc.pfc_hist_linea_tratamiento histlin on histlin.cod_sol_eva=mon.cod_sol_eva
        left join ontpfc.pfc_continuador cont on cont.cod_sol_eva = solev.cod_sol_eva
        left join ontpfc.pfc_evolucion evo on evo.cod_monitoreo = mon.cod_monitoreo
        where 1 = 1' || l_condiciones || l_cond_grp ||' order by FECHAAPROBACION asc';
        OPEN ac_listamonitoreo FOR l_sql_stmt;

        DBMS_OUTPUT.put_line(l_sql_stmt);
        pn_cod_resultado := 0;
        pv_msg_resultado := 'EXITO';
    EXCEPTION
        WHEN OTHERS THEN
            pn_cod_resultado := -1;
            pv_msg_resultado := '[[PCK_PFC_BANDEJA_MONITOREO]] | codigo error: ' || sqlcode || ' | mensaje error: ' || sqlerrm;
    END sp_pfc_s_bandeja_monitoreo;
    
    PROCEDURE sp_pfc_s_hist_lin_tratamiento (
        pn_cod_hist_linea_trat  IN NUMBER,
        pn_cod_afiliado         IN VARCHAR2,
        pn_cod_mac              IN NUMBER,
        ac_lista                OUT SYS_REFCURSOR,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    ) AS
        l_sql_stmt       VARCHAR2(32767);
        l_condiciones    VARCHAR2(4000);
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'No se puede obtener la lista';
        
        IF ( pn_cod_hist_linea_trat is not null ) THEN
            l_condiciones := l_condiciones || ' AND ht.cod_hist_linea_trat = ' || pn_cod_hist_linea_trat || '';
        END IF;
        
        IF ( pn_cod_afiliado is not null ) THEN
            l_condiciones := l_condiciones || ' AND ht.cod_afiliado = ''' || pn_cod_afiliado || '''';
        END IF;

        IF ( pn_cod_mac is not null ) THEN
            l_condiciones := l_condiciones || ' AND ht.cod_mac = ' || pn_cod_mac || '';
        END IF;
        
        l_sql_stmt := '
            SELECT
            ht.cod_hist_linea_trat AS "COD_LINEA_TRATAMIENTO",
            ht.linea_trat         AS "LINEA_TRAT",
            mac.descripcion       AS "MEDICAMENTOSOLICITADO",
            se.cod_desc_sol_eva   AS "COD_DESC_SOL_EVA",
            ht.fec_aprobacion     AS "FEC_APROBACION",
            ht.fec_inicio         AS "FEC_INICIO",
            ht.fec_fin            AS "FEC_FIN",
            (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = ht.p_curso) AS "P_CURSO",
            (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = ht.p_tipo_tumor) AS "P_TIPO_TUMOR",
            (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = ht.p_resp_alcanzada) AS "P_RESP_ALCANZADA",
            (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = ht.p_estado ) AS "P_ESTADO",
            (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = ht.p_motivo_inactivacion) AS "P_MOTIVO_INACTIVACION",
            ht.medico_tratante    AS "MEDICO_TRATANTE_PRESCRIPTOR",
            s.total_presupuesto   AS "MONTO_AUTORIZADO",
            s.cod_scg_solben      AS "NRO_SCG_SOLBEN",
            s.fec_scg_solben      AS "FEC_SCG_SOLBEN",
            se.cod_sol_eva        AS "NRO_INFORME",
            ht.fec_emision        AS "FEC_EMISION",
            ht.cod_auditor_eval   AS "COD_AUDITOR_EVALUACION",
            s.nro_cg              AS "NRO_CG_SOLBEN",
            s.fec_cg              AS "FEC_CG_SOLBEN",
            se.cod_informe_auto   AS "COD_INFORME_AUTO"
            FROM ontpfc.pfc_hist_linea_tratamiento ht
            LEFT JOIN ontpfc.pfc_solicitud_evaluacion se ON se.cod_sol_eva=ht.cod_sol_eva
            LEFT JOIN ontpfc.pfc_solicitud_preliminar p ON p.cod_sol_pre = se.cod_sol_pre
            LEFT JOIN ontpfc.pfc_solben s ON s.cod_scg = p.cod_scg
            LEFT JOIN ontpfc.pfc_mac mac ON mac.cod_mac = ht.cod_mac
            WHERE 1 = 1' || l_condiciones || 'ORDER BY ht.fec_aprobacion desc, ht.cod_hist_linea_trat desc';
            
            OPEN ac_lista FOR l_sql_stmt;
        
            pn_cod_resultado := 0;
            pv_msg_resultado := 'Consulta exitosa';
            
    EXCEPTION
        WHEN OTHERS THEN
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[[SP_PFC_S_HIST_LIN_TRATAMIENTO]] ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_s_hist_lin_tratamiento;

    PROCEDURE sp_pfc_s_listar_evolucion (
        pn_cod_sol_eva      IN NUMBER,
        ac_lista            OUT pck_pfc_constante.typcur,
        pn_cod_resultado    OUT NUMBER,
        pv_msg_resultado    OUT VARCHAR2
    ) AS
        l_sql_stmt   VARCHAR2(32767);
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'No se puede obtener la lista';
        OPEN ac_lista FOR 
        SELECT 
        evo.cod_evolucion as "COD_EVOLUCION",
        evo.nro_desc_evolucion as "NRO_DESC_EVOLUCION",
        evo.cod_monitoreo as "COD_MONITOREO",
        mon.cod_desc_monitoreo as "COD_DESC_MONITOREO",
        evo.cod_mac as "COD_MAC",
        evo.p_res_evolucion as "P_RES_EVOLUCION",
        evo.cod_hist_linea_trat as "COD_HIST_LINEA_TRAT",
        mon.fec_monitoreo as "FEC_MONITOREO",
        mon.fec_prox_monitoreo as "FEC_PROX_MONITOREO",
        evo.p_motivo_inactivacion as "P_MOTIVO_INACTIVACION",
        evo.fec_inactivacion as "FEC_INACTIVACION",
        evo.observacion as "OBSERVACION",
        evo.p_tolerancia as "P_TOLERANCIA",
        (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = evo.p_tolerancia) AS "DESC_TOLERANCIA",
        evo.p_toxicidad as "P_TOXICIDAD",
        (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = evo.p_toxicidad) AS "DESC_TOXICIDAD",
        evo.p_grado as "P_GRADO",
        (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = evo.p_grado) AS "DESC_GRADO",
        evo.p_resp_clinica as "P_RESP_CLINICA",
        (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = evo.p_resp_clinica) AS "DESC_RESP_CLINICA",
        evo.p_aten_alerta as "P_ATEN_ALERTA",
        (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = evo.p_aten_alerta) AS "DESC_ATEN_ALERTA",
        evo.existe_toxicidad as "EXISTE_TOXICIDAD",
        evo.fec_ultimo_consumo as "F_ULTIMO_CONSUMO",
        evo.cant_ultimo_consumo as "ULTIMA_CANT_CONSUMIDA",
        mon.p_estado_monitoreo as "P_ESTADO_MONITOREO"
        FROM ONTPFC.pfc_evolucion evo 
        inner join ONTPFC.pfc_monitoreo mon on mon.cod_monitoreo = evo.cod_monitoreo
        WHERE mon.cod_sol_eva = pn_cod_sol_eva order by evo.cod_monitoreo asc;

        pn_cod_resultado := 0;
        pv_msg_resultado := 'EXITO';
    EXCEPTION
        WHEN no_data_found THEN
            pn_cod_resultado := 1;
            pv_msg_resultado := '[[SP_PFC_S_LISTAR_EVOLUCION]] | codigo error: ' || sqlcode || ' | mensaje error: ' || sqlerrm;
        WHEN OTHERS THEN
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[[SP_PFC_S_LISTAR_EVOLUCION]] | codigo error: ' || sqlcode || ' | mensaje error: ' || sqlerrm;
    END sp_pfc_s_listar_evolucion;
  
    PROCEDURE sp_pfc_i_datos_evolucion (
        pv_nro_desc_evolucion        IN VARCHAR2,
        pn_cod_monitoreo             IN NUMBER,
        pn_cod_mac                   IN NUMBER,
        pn_p_res_evolucion           IN NUMBER,
        pn_cod_hist_linea_trat       IN NUMBER,
        pv_fec_monitoreo             IN VARCHAR2,
        pn_p_tolerancia              IN NUMBER,
        pn_p_toxicidad               IN NUMBER,
        pn_p_grado                   IN NUMBER,
        pn_p_resp_clinica            IN NUMBER,
        pn_p_aten_alerta             IN NUMBER,
        pv_existe_toxicidad          IN ONTPFC.PFC_EVOLUCION.EXISTE_TOXICIDAD%type,
        pn_nro_evolucion             IN NUMBER,
        pv_marcadores                IN VARCHAR2,
        pn_estado                    IN NUMBER,
        pv_usuario_crea              IN VARCHAR2,
        pn_p_estado_monitoreo        IN NUMBER,
        pn_cod_evolucion             OUT NUMBER,
        pn_cod_resultado             OUT NUMBER,
        pv_msg_resultado             OUT VARCHAR2
    ) AS

        v_cod_evo_marcador   NUMBER;
        v_count_evolucion      NUMBER;
        v_j                    NUMBER;

        CURSOR tc_res_marcadores IS 
        SELECT
          regexp_substr(res_marcadores,'([^,]+)',1,1,'',1) AS v_cod_marcador,
          regexp_substr(res_marcadores,'([^,]+)',1,2,'',1) AS v_cod_resultado,
          regexp_substr(res_marcadores,'([^,]+)',1,3,'',1) AS v_resultado,
          regexp_substr(res_marcadores,'([^,]+)',1,4,'',1) AS v_fec_resultado,
          regexp_substr(res_marcadores,'([^,]+)',1,5,'',1) AS v_tiene_reg_hc,
          regexp_substr(res_marcadores,'([^,]+)',1,6,'',1) AS v_p_per_minima,
          regexp_substr(res_marcadores,'([^,]+)',1,7,'',1) AS v_p_per_maxima,
          regexp_substr(res_marcadores,'([^,]+)',1,8,'',1) AS v_p_tipo_ingreso_res,
          regexp_substr(res_marcadores,'([^,]+)',1,9,'',1) AS v_desc_per_minima,
          regexp_substr(res_marcadores,'([^,]+)',1,10,'',1) AS v_desc_per_maxima,
          regexp_substr(res_marcadores,'([^,]+)',1,11,'',1) AS v_descripcion,
          regexp_substr(res_marcadores,'([^,]+)',1,12,'',1) AS v_unidad_medida
        FROM (
        SELECT regexp_substr(pv_marcadores,'[^|]+',1,level) 
        AS res_marcadores 
        FROM dual CONNECT BY regexp_substr(pv_marcadores,'[^|]+',1,level) IS NOT NULL);

    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'valor inicial';

        v_j := 0;
        FOR v_item IN tc_res_marcadores LOOP
            v_j := v_j + 1;
        END LOOP;
        dbms_output.put_line('V_J : ' || v_j);
        
        
        IF v_j > 0 THEN
            SELECT ontpfc.SQ_PFC_COD_EVOLUCION.NEXTVAL INTO pn_cod_evolucion FROM DUAL;
            
            INSERT INTO ontpfc.pfc_evolucion (cod_evolucion,nro_desc_evolucion,cod_monitoreo,
            cod_mac,p_res_evolucion,cod_hist_linea_trat,
            p_tolerancia,p_toxicidad,p_grado,p_resp_clinica,p_aten_alerta,
            existe_toxicidad,nro_evolucion,estado,usuario_crea,fecha_crea) 
            VALUES (
            pn_cod_evolucion,pv_nro_desc_evolucion,pn_cod_monitoreo,
            pn_cod_mac,pn_p_res_evolucion,pn_cod_hist_linea_trat,
            pn_p_tolerancia,pn_p_toxicidad,pn_p_grado,pn_p_resp_clinica,pn_p_aten_alerta,
            pv_existe_toxicidad,pn_nro_evolucion,pn_estado,pv_usuario_crea,SYSDATE);
            
            UPDATE ontpfc.pfc_monitoreo SET p_estado_monitoreo = pn_p_estado_monitoreo, fec_monitoreo = TO_DATE(pv_fec_monitoreo,'DD/MM/YYYY') where cod_monitoreo = pn_cod_monitoreo;
      
            FOR v_item IN tc_res_marcadores LOOP
                SELECT ontpfc.SQ_PFC_COD_EVOL_MARCADOR.NEXTVAL INTO v_cod_evo_marcador FROM dual;
                IF v_item.v_tiene_reg_hc = '0' THEN
                    INSERT INTO ONTPFC.PFC_EVOLUCION_MARCADOR(
                    COD_EVOLUCION_MARCADOR,COD_EVOLUCION,COD_MARCADOR,COD_RESULTADO,RESULTADO,
                    FEC_RESULTADO,TIENE_REG_HC,P_PER_MINIMA,P_PER_MAXIMA,
                    P_TIPO_INGRESO_RES,DESC_PER_MINIMA,DESC_PER_MAXIMA,DESCRIPCION,UNIDAD_MEDIDA,USUARIO_CREA,FECHA_CREA)
                    VALUES(
                    v_cod_evo_marcador,pn_cod_evolucion,v_item.v_cod_marcador,v_item.v_cod_resultado,v_item.v_resultado,
                    TO_DATE(v_item.v_fec_resultado,'DD/MM/YYYY'),v_item.v_tiene_reg_hc,v_item.v_p_per_minima,v_item.v_p_per_maxima,
                    v_item.v_p_tipo_ingreso_res,v_item.v_desc_per_minima,v_item.v_desc_per_maxima,v_item.v_descripcion,v_item.v_unidad_medida,pv_usuario_crea,SYSDATE);
                ELSE
                    INSERT INTO ONTPFC.PFC_EVOLUCION_MARCADOR(
                    COD_EVOLUCION_MARCADOR,COD_EVOLUCION,COD_MARCADOR,COD_RESULTADO,RESULTADO,
                    FEC_RESULTADO,TIENE_REG_HC,P_PER_MINIMA,P_PER_MAXIMA,
                    P_TIPO_INGRESO_RES,DESC_PER_MINIMA,DESC_PER_MAXIMA,DESCRIPCION,UNIDAD_MEDIDA,USUARIO_CREA,FECHA_CREA)
                    VALUES(
                    v_cod_evo_marcador,pn_cod_evolucion,v_item.v_cod_marcador,null,null,
                    null,v_item.v_tiene_reg_hc,v_item.v_p_per_minima,v_item.v_p_per_maxima,
                    v_item.v_p_tipo_ingreso_res,v_item.v_desc_per_minima,v_item.v_desc_per_maxima,v_item.v_descripcion,v_item.v_unidad_medida,pv_usuario_crea,SYSDATE);
                END IF;
            END LOOP;
            pn_cod_resultado := 0;
            pv_msg_resultado := 'Registro exitoso';
            COMMIT;
        ELSE
            pn_cod_resultado := 1;
            pv_msg_resultado := 'No se encontro registros de marcador';
            ROLLBACK;
        END IF;
    
        EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[SP_PFC_I_DATOS_EVOLUCION] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_i_datos_evolucion;
    
    PROCEDURE sp_pfc_i_resultado_evolucion (
        pn_cod_evolucion             IN NUMBER,
        pn_p_res_evolucion           IN NUMBER,
        pn_cod_sol_evaluacion        IN NUMBER,
        pv_desc_cod_sol_evaluacion   IN VARCHAR2,
        pn_cod_monitoreo             IN NUMBER,
        pn_p_estado_monitoreo        IN NUMBER,
        pv_fec_monitoreo             IN VARCHAR2,
        pv_fec_prox_monitoreo        IN VARCHAR2,
        pn_p_motivo_inactivacion     IN NUMBER,
        pv_fec_inactivacion          IN VARCHAR2,
        pv_observacion               IN VARCHAR2,
        pv_usuario_modif             IN VARCHAR2,
        pn_estado                    IN NUMBER,
        pn_cod_hist_linea_trat       IN NUMBER,
        pn_cod_resultado             OUT NUMBER,
        pv_msg_resultado             OUT VARCHAR2
    ) AS
        l_cont_monitoreo             NUMBER;
        l_adic                      VARCHAR2(200);
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'No se pudo registrar los resultados de evolucion';
        
        IF pn_p_res_evolucion = ONTPFC.PCK_PFC_CONSTANTE.PN_RES_EVOLUCION_ACTIVO THEN
            --ACTIVO CREAR NUEVO MONITOREO--
            UPDATE ontpfc.pfc_evolucion 
            SET 
            p_res_evolucion = pn_p_res_evolucion,
            observacion = pv_observacion,
            estado=pn_estado,
            usuario_modif = pv_usuario_modif,
            fecha_modif= SYSDATE
            WHERE cod_evolucion=pn_cod_evolucion;
            
            UPDATE ontpfc.pfc_monitoreo SET  p_estado_monitoreo = pn_p_estado_monitoreo, fec_monitoreo = TO_DATE(pv_fec_monitoreo,'DD/MM/YYYY') WHERE cod_monitoreo=pn_cod_monitoreo;
            
            SELECT COUNT(cod_monitoreo) INTO l_cont_monitoreo FROM ontpfc.pfc_monitoreo WHERE cod_sol_eva = pn_cod_sol_evaluacion;
            INSERT INTO ontpfc.pfc_monitoreo(cod_monitoreo,cod_desc_monitoreo,cod_sol_eva,p_estado_monitoreo,fec_prox_monitoreo) VALUES(ontpfc.SQ_PFC_MONITOREO_COD_MONITOREO.NEXTVAL,pv_desc_cod_sol_evaluacion||'-'||lpad(l_cont_monitoreo+1, 3, 0),pn_cod_sol_evaluacion,ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_MONITOREO,TO_DATE(pv_fec_prox_monitoreo,'DD/MM/YYYY'));
        ELSE
            --INACTIVO FALTA SETEAR A INACTIVO LA LINEA DE TRATAMIENTO--
            UPDATE ontpfc.pfc_evolucion 
            SET 
            p_res_evolucion = pn_p_res_evolucion,
            p_motivo_inactivacion = pn_p_motivo_inactivacion,
            fec_inactivacion = TO_DATE(pv_fec_inactivacion,'DD/MM/YYYY'),
            observacion = pv_observacion,
            estado=pn_estado,
            usuario_modif = pv_usuario_modif,
            fecha_modif= SYSDATE
            WHERE cod_evolucion=pn_cod_evolucion;
            
            UPDATE ontpfc.pfc_monitoreo SET  p_estado_monitoreo = pn_p_estado_monitoreo, fec_monitoreo = TO_DATE(pv_fec_monitoreo,'DD/MM/YYYY') WHERE cod_monitoreo=pn_cod_monitoreo;
            UPDATE ontpfc.pfc_hist_linea_tratamiento SET p_motivo_inactivacion = pn_p_motivo_inactivacion, fec_inactivacion = TO_DATE(pv_fec_inactivacion,'DD/MM/YYYY'),p_estado=ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_LIN_TRA_INACTIVO WHERE cod_hist_linea_trat = pn_cod_hist_linea_trat;--46 
        END IF;
        
        pn_cod_resultado := 0;
        pv_msg_resultado := 'Registro de resultados exitoso '|| l_adic;
        COMMIT;
        
        EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[SP_PFC_I_RESULTADO_EVOLUCION] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_i_resultado_evolucion;

    PROCEDURE sp_pfc_s_detalle_evolucion (
        pn_cod_evolucion        IN NUMBER,
        ac_lista                OUT SYS_REFCURSOR,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    ) AS
    
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'No se puede obtener la lista';
        
        OPEN ac_lista FOR 
        SELECT 
        evomar.cod_evolucion_marcador as "COD_EVOLUCION_MARCADOR",
        evomar.cod_evolucion as "COD_EVOLUCION",
        evomar.cod_marcador as "COD_MARCADOR",
        evomar.cod_resultado as "COD_RESULTADO",
        evomar.resultado as "RESULTADO",
        evomar.fec_resultado as "FEC_RESULTADO",
        evomar.tiene_reg_hc as "TIENE_REG_HC",
        evomar.p_per_minima as "P_PER_MINIMA",
        evomar.p_per_maxima as "P_PER_MAXIMA",
        evomar.p_tipo_ingreso_res as "P_TIPO_INGRESO_RES",
        evomar.desc_per_minima as "DESC_PER_MINIMA",
        evomar.desc_per_maxima as "DESC_PER_MAXIMA",
        evomar.usuario_crea as "USUARIO_CREA",
        evomar.fecha_crea as "FECHA_CREA"
        FROM ONTPFC.pfc_evolucion_marcador evomar
        WHERE evomar.cod_evolucion = pn_cod_evolucion;
        
        pn_cod_resultado := 0;
        pv_msg_resultado := 'Consulta exitosa';
            
    EXCEPTION
        WHEN OTHERS THEN
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[[SP_PFC_S_DETALLE_EVOLUCION]] ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_s_detalle_evolucion;
    
    PROCEDURE sp_pfc_s_listar_marcador (
        pn_cod_mac          IN NUMBER,
        pn_cod_grp_diag     IN NUMBER,
        pn_p_estado         IN NUMBER,
        ac_lista            OUT pck_pfc_constante.typcur,
        ac_lista_detalle    OUT pck_pfc_constante.typcur,
        pn_cod_resultado    OUT NUMBER,
        pv_msg_resultado    OUT VARCHAR2
    ) AS
        l_sql_stmt       VARCHAR2(32767);
        l_condiciones    VARCHAR2(4000);
        
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'No se puede obtener la lista';
        
        IF ( pn_cod_mac is not null ) THEN
            l_condiciones := l_condiciones || ' and cm.cod_mac = ' || pn_cod_mac;
        END IF;
        
        IF ( pn_cod_grp_diag is not null ) THEN
            l_condiciones := l_condiciones || ' and cm.cod_grp_diag = ' || pn_cod_grp_diag;
        END IF;
        
        IF ( pn_p_estado is not null ) THEN
            l_condiciones := l_condiciones || ' and cm.p_estado = ' || pn_p_estado ;
        END IF;
        
        l_sql_stmt := '
        select 
        distinct (cm.cod_config_marca) as "COD_CONFIG_MARCA",
        cm.cod_examen_med as "COD_MARCADOR",
        exm.descripcion as "DESCRIPCION",
        emd.rango_minimo as "RANGO_MINIMO",
        emd.rango_maximo as "RANGO_MAXIMO",
        cm.p_per_minima as "P_PER_MINIMA",
        cm.p_per_maxima as "P_PER_MAXIMA",
        (SELECT p.valor1 FROM ontpfc.pfc_parametro p WHERE p.cod_parametro = cm.p_per_minima) AS "VAL_PER_MINIMA",
        (SELECT p.valor1 FROM ontpfc.pfc_parametro p WHERE p.cod_parametro = cm.p_per_maxima) AS "VAL_PER_MAXIMA",
        (SELECT p.nombre FROM ontpfc.pfc_parametro p WHERE p.cod_parametro = cm.p_per_minima) AS "DESC_PER_MINIMA",
        (SELECT p.nombre FROM ontpfc.pfc_parametro p WHERE p.cod_parametro = cm.p_per_maxima) AS "DESC_PER_MAXIMA",
        emd.rango as "RANGO",
        emd.p_tipo_ingreso_res as "P_TIPO_INGRESO_RES",
        (SELECT p.nombre FROM ontpfc.pfc_parametro p WHERE p.cod_parametro = emd.p_tipo_ingreso_res) AS "DESC_TIPO_INGRESO_RES",
        emd.unidad_medida as "UNIDAD_MEDIDA"
        from ontpfc.pfc_configuracion_marcador cm
        inner join ontpfc.pfc_examen_medico_marcador exm on exm.cod_examen_med=cm.cod_examen_med
        inner join ontpfc.pfc_examen_medico_detalle emd on emd.cod_examen_med=exm.cod_examen_med
        where cm.p_tipo_marcador = '||ONTPFC.PCK_PFC_CONSTANTE.PN_TIPO_MARC_EVAL_MONIT||' ' || l_condiciones;--66
        OPEN ac_lista FOR l_sql_stmt;
        
        l_sql_stmt := '
        select 
        emd.cod_examen_med as "COD_MARCADOR",
        emd.cod_examen_med_det as "COD_DET_MARCADOR",
        emd.valor_fijo as "VALOR_FIJO"
        from ontpfc.pfc_examen_medico_detalle emd
        where emd.cod_examen_med in (
        select distinct(cm.cod_examen_med) from ontpfc.pfc_configuracion_marcador cm
        inner join ontpfc.pfc_examen_medico_marcador exm on exm.cod_examen_med=cm.cod_examen_med
        inner join ontpfc.pfc_examen_medico_detalle emd on emd.cod_examen_med = exm.cod_examen_med
        where cm.p_tipo_marcador = '|| ONTPFC.PCK_PFC_CONSTANTE.PN_TIPO_MARC_EVAL_MONIT ||' '|| l_condiciones || ')';--66 
        OPEN ac_lista_detalle FOR l_sql_stmt;
        
        pn_cod_resultado := 0;
        pv_msg_resultado := 'EXITO';
    EXCEPTION
        WHEN no_data_found THEN
            pn_cod_resultado := 1;
            pv_msg_resultado := '[[SP_PFC_S_LISTAR_MARCADOR]] | codigo error: ' || sqlcode || ' | mensaje error: ' || sqlerrm;
        WHEN OTHERS THEN
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[[SP_PFC_S_LISTAR_MARCADOR]] | codigo error: ' || sqlcode || ' | mensaje error: ' || sqlerrm;
    END sp_pfc_s_listar_marcador;
    
    PROCEDURE sp_pfc_u_datos_evolucion (
        pn_cod_evolucion             IN NUMBER,
        pn_cod_monitoreo             IN NUMBER,
        pv_fec_monitoreo             IN VARCHAR2,
        pn_p_tolerancia              IN NUMBER,
        pn_p_toxicidad               IN NUMBER,
        pn_p_grado                   IN NUMBER,
        pn_p_resp_clinica            IN NUMBER,
        pn_p_aten_alerta             IN NUMBER,
        pv_existe_toxicidad          IN ONTPFC.PFC_EVOLUCION.EXISTE_TOXICIDAD%type,
        pv_marcadores                IN VARCHAR2,
        pn_estado                    IN NUMBER,
        pv_usuario_modif             IN VARCHAR2,
        pn_cod_resultado             OUT NUMBER,
        pv_msg_resultado             OUT VARCHAR2
    ) AS

        v_cod_evo_marcador   NUMBER;
        v_count_evolucion      NUMBER;
        v_j                    NUMBER;

        CURSOR tc_res_marcadores IS 
        SELECT
          regexp_substr(res_marcadores,'([^,]+)',1,1,'',1) AS v_cod_evolucion_marcador,
          regexp_substr(res_marcadores,'([^,]+)',1,2,'',1) AS v_cod_resultado,
          regexp_substr(res_marcadores,'([^,]+)',1,3,'',1) AS v_resultado,
          regexp_substr(res_marcadores,'([^,]+)',1,4,'',1) AS v_fec_resultado,
          regexp_substr(res_marcadores,'([^,]+)',1,5,'',1) AS v_tiene_reg_hc
        FROM (
        SELECT regexp_substr(pv_marcadores,'[^|]+',1,level) 
        AS res_marcadores 
        FROM dual CONNECT BY regexp_substr(pv_marcadores,'[^|]+',1,level) IS NOT NULL);

    BEGIN
        pn_cod_resultado := -1;
        pv_msg_resultado := 'Valor inicial';

        v_j := 0;
        FOR v_item IN tc_res_marcadores LOOP
            v_j := v_j + 1;
        END LOOP;
        
        IF v_j > 0 THEN
            UPDATE ontpfc.pfc_evolucion SET 
            p_tolerancia = pn_p_tolerancia,
            p_toxicidad = pn_p_toxicidad,
            p_grado = pn_p_grado,
            p_resp_clinica = pn_p_resp_clinica,
            p_aten_alerta = pn_p_aten_alerta,
            existe_toxicidad = pv_existe_toxicidad,
            estado = pn_estado,
            usuario_modif = pv_usuario_modif,
            fecha_modif = SYSDATE
            WHERE 
            cod_evolucion = pn_cod_evolucion;
            
            UPDATE ontpfc.pfc_monitoreo SET fec_monitoreo = TO_DATE(pv_fec_monitoreo,'DD/MM/YYYY') where cod_monitoreo = pn_cod_monitoreo;
      
            FOR v_item IN tc_res_marcadores LOOP
                IF v_item.v_tiene_reg_hc = '0' THEN
                    UPDATE ontpfc.pfc_evolucion_marcador SET
                    cod_resultado = v_item.v_cod_resultado,
                    resultado = v_item.v_resultado,
                    fec_resultado = TO_DATE(v_item.v_fec_resultado,'DD/MM/YYYY'),
                    tiene_reg_hc = v_item.v_tiene_reg_hc,
                    usuario_modif = pv_usuario_modif,
                    fecha_modif= SYSDATE
                    WHERE
                    cod_evolucion_marcador = v_item.v_cod_evolucion_marcador;
                ELSE
                    UPDATE ontpfc.pfc_evolucion_marcador SET
                    cod_resultado = null,
                    resultado = null,
                    fec_resultado = null,
                    tiene_reg_hc = v_item.v_tiene_reg_hc,
                    usuario_modif = pv_usuario_modif,
                    fecha_modif= SYSDATE
                    WHERE
                    cod_evolucion_marcador = v_item.v_cod_evolucion_marcador;
                END IF;
            END LOOP;
            pn_cod_resultado := 0;
            pv_msg_resultado := 'Actualizacion exitosa';
            COMMIT;
        ELSE
            pn_cod_resultado := 1;
            pv_msg_resultado := 'No se encontro registros de marcador';
            ROLLBACK;
        END IF;
    
        EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[SP_PFC_U_DATOS_EVOLUCION] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_u_datos_evolucion;
    
    PROCEDURE sp_pfc_s_historial_marcadores (
        pn_cod_sol_eva          IN NUMBER,
        ac_lista                OUT pck_pfc_constante.typcur,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    ) AS
        
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'No se puede obtener la lista';
        
        OPEN ac_lista FOR 
        select 
        evomar.cod_marcador as "COD_MARCADOR",
        evo.cod_evolucion as "COD_EVOLUCION",
        evomar.descripcion as "DESCRIPCION",
        evomar.desc_per_minima as "DESC_PER_MINIMA",
        evomar.desc_per_maxima as "DESC_PER_MAXIMA",
        evomar.cod_resultado as "COD_RESULTADO",
        evomar.resultado as "RESULTADO",
        evomar.fec_resultado as "FEC_RESULTADO",
        evomar.tiene_reg_hc as "TIENE_REG_HC",
        evomar.p_tipo_ingreso_res as "P_TIPO_INGRESO_RES",
        evomar.unidad_medida as "UNIDAD_MEDIDA"
        from ontpfc.pfc_evolucion evo
        inner join ontpfc.pfc_evolucion_marcador evomar on evomar.cod_evolucion=evo.cod_evolucion
        inner join ontpfc.pfc_monitoreo mon on mon.cod_monitoreo = evo.cod_monitoreo
        where mon.cod_sol_eva=pn_cod_sol_eva;
        
        pn_cod_resultado := 0;
        pv_msg_resultado := 'EXITO';
    EXCEPTION
        WHEN no_data_found THEN
            pn_cod_resultado := 1;
            pv_msg_resultado := '[[SP_PFC_S_HISTORIAL_MARCADORORES]] | codigo error: ' || sqlcode || ' | mensaje error: ' || sqlerrm;
        WHEN OTHERS THEN
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[[SP_PFC_S_HISTORIAL_MARCADORORES]] | codigo error: ' || sqlcode || ' | mensaje error: ' || sqlerrm;
    END sp_pfc_s_historial_marcadores;
    
    PROCEDURE sp_pfc_u_estado_monitoreo (
        pn_cod_monitoreo             IN NUMBER,
        pn_p_estado_monitoreo        IN NUMBER,
        pn_cod_resultado             OUT NUMBER,
        pv_msg_resultado             OUT VARCHAR2
    ) AS
        l_cont_monitoreo             NUMBER;

    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'No se pudo actualizar el estado monitoreo';
        
        UPDATE ontpfc.pfc_monitoreo
        SET
        p_estado_monitoreo = pn_p_estado_monitoreo
        WHERE cod_monitoreo=pn_cod_monitoreo;

        pn_cod_resultado := 0;
        pv_msg_resultado := 'Actualizacion de estado exitoso';
        COMMIT;
        
        EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[SP_PFC_U_ESTADO_MONITOREO] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_u_estado_monitoreo;
    
    PROCEDURE sp_pfc_i_seg_ejecutivo (
        pn_cod_monitoreo             IN NUMBER,
        pn_cod_ejecutivo_monitoreo   IN NUMBER,
        pn_nom_ejecutivo_monitoreo   IN VARCHAR2,
        pn_p_estado_seguimiento      IN NUMBER,
        pv_detalle_evento            IN VARCHAR2,
        pn_visto_resp_monitoreo      IN NUMBER,
        pv_fecha_registro            IN VARCHAR2,
        pv_usuario_crea              IN VARCHAR2,
        pn_p_estado_monitoreo        IN NUMBER,
        pn_cod_seg_ejecutivo         OUT NUMBER,
        pn_cod_resultado             OUT NUMBER,
        pv_msg_resultado             OUT VARCHAR2
    ) AS
    
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'valor inicial';

        SELECT sq_pfc_cod_seg_ejecutivo.nextval INTO pn_cod_seg_ejecutivo FROM dual;

        IF pn_p_estado_seguimiento = ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_SEG_EJEC_PENDIENTE THEN--242
            INSERT INTO ontpfc.pfc_seg_ejecutivo (cod_seg_ejecutivo,cod_monitoreo,cod_ejecutivo_monitoreo,nom_ejecutivo_monitoreo,
            fecha_registro,p_estado_seguimiento,detalle_evento,visto_resp_monitoreo,usuario_crea,fecha_crea)
            VALUES (pn_cod_seg_ejecutivo,pn_cod_monitoreo,pn_cod_ejecutivo_monitoreo,pn_nom_ejecutivo_monitoreo,
            TO_DATE(pv_fecha_registro,'dd/mm/yyyy hh24:mi:ss'),pn_p_estado_seguimiento,pv_detalle_evento,pn_visto_resp_monitoreo,pv_usuario_crea,SYSDATE);
        ELSE
            INSERT INTO ontpfc.pfc_seg_ejecutivo (cod_seg_ejecutivo,cod_monitoreo,cod_ejecutivo_monitoreo,nom_ejecutivo_monitoreo,
            fecha_registro,p_estado_seguimiento,detalle_evento,visto_resp_monitoreo,usuario_crea,fecha_crea)
            VALUES (pn_cod_seg_ejecutivo,pn_cod_monitoreo,pn_cod_ejecutivo_monitoreo,pn_nom_ejecutivo_monitoreo,
            TO_DATE(pv_fecha_registro,'dd/mm/yyyy hh24:mi:ss'),pn_p_estado_seguimiento,pv_detalle_evento,pn_visto_resp_monitoreo,pv_usuario_crea,SYSDATE);
            
            UPDATE ontpfc.pfc_monitoreo SET p_estado_monitoreo = pn_p_estado_monitoreo WHERE cod_monitoreo = pn_cod_monitoreo;
        END IF;
        
        pn_cod_resultado := 0;
        pv_msg_resultado := 'Registro exitoso';
        COMMIT;
        
        EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[SP_PFC_I_SEG_EJECUTIVO] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_i_seg_ejecutivo;
    
    PROCEDURE sp_pfc_s_seg_ejecutivo (
        pn_cod_monitoreo        IN NUMBER,
        ac_lista                OUT pck_pfc_constante.typcur,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    ) AS
    
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'valor inicial';

        OPEN ac_lista FOR 
        SELECT
        segeje.cod_seg_ejecutivo as "COD_SEG_EJECUTIVO",
        segeje.cod_monitoreo as "COD_MONITOREO",
        segeje.cod_ejecutivo_monitoreo as "COD_EJECUTIVO_MONITOREO",
        segeje.nom_ejecutivo_monitoreo as "NOM_EJECUTIVO_MONITOREO",
        segeje.fecha_registro as "FECHA_REGISTRO",
        segeje.p_estado_seguimiento as "P_ESTADO_SEGUIMIENTO",
        (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = segeje.p_estado_seguimiento) AS "DESC_ESTADO_SEGUIMIENTO",
        segeje.detalle_evento as "DETALLE_EVENTO",
        segeje.visto_resp_monitoreo as "VISTO_RESP_MONITOREO"
        FROM ontpfc.pfc_seg_ejecutivo segeje 
        WHERE segeje.cod_monitoreo = pn_cod_monitoreo
        ORDER BY segeje.cod_seg_ejecutivo desc;

        pn_cod_resultado := 0;
        pv_msg_resultado := 'Listado exitoso';
        
        EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[SP_PFC_S_SEG_EJECUTIVO] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_s_seg_ejecutivo;
    
    PROCEDURE sp_pfc_s_seg_pendientes (
        pn_cod_monitoreo        IN NUMBER,
        pn_pendientes           OUT NUMBER,
        pn_seguimiento          OUT NUMBER,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    ) AS
    
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'valor inicial';

        SELECT count(segeje.cod_seg_ejecutivo) INTO pn_pendientes FROM ontpfc.pfc_seg_ejecutivo segeje 
        WHERE segeje.cod_monitoreo = pn_cod_monitoreo and segeje.visto_resp_monitoreo = 0;
        
        SELECT count(segeje.cod_seg_ejecutivo) INTO pn_seguimiento FROM ontpfc.pfc_seg_ejecutivo segeje 
        WHERE segeje.cod_monitoreo = pn_cod_monitoreo;

        pn_cod_resultado := 0;
        pv_msg_resultado := 'Busqueda exitosa';
        
        EXCEPTION
        WHEN OTHERS THEN
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[SP_PFC_S_SEG_PENDIENTES] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_s_seg_pendientes;
    
    PROCEDURE sp_pfc_u_seg_ejecutivo (
        pn_cod_monitoreo        IN NUMBER,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    ) AS
    
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'valor inicial';

        UPDATE ontpfc.pfc_seg_ejecutivo segeje SET segeje.visto_resp_monitoreo = 1
        WHERE segeje.cod_monitoreo = pn_cod_monitoreo and segeje.visto_resp_monitoreo = 0;

        pn_cod_resultado := 0;
        pv_msg_resultado := 'Actualizacion exitosa';
        
        EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[SP_PFC_U_SEG_EJECUTIVO] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_u_seg_ejecutivo;
    
    PROCEDURE sp_pfc_s_ult_marcador (
        pn_cod_marcador         IN NUMBER,
        pn_cod_sol_eva          IN NUMBER,
        pn_cod_evolucion        IN NUMBER,
        ac_lista                OUT pck_pfc_constante.typcur,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    ) AS
    
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'valor inicial';
        
        IF (pn_cod_evolucion IS NOT NULL) THEN
            OPEN ac_lista FOR
            SELECT 
            mar.cod_marcador as "COD_MARCADOR",
            null as "COD_EVOLUCION",
            mar.descripcion as "DESCRIPCION",
            mar.desc_per_minima as "DESC_PER_MINIMA",
            mar.desc_per_maxima as "DESC_PER_MAXIMA",
            mar.cod_resultado as "COD_RESULTADO",
            mar.resultado as "RESULTADO",
            mar.fec_resultado as "FEC_RESULTADO",
            mar.tiene_reg_hc as "TIENE_REG_HC",
            mar.p_tipo_ingreso_res as "P_TIPO_INGRESO_RES",
            mar.unidad_medida as "UNIDAD_MEDIDA"
            FROM ontpfc.pfc_evolucion_marcador mar
            WHERE cod_evolucion_marcador =(
                SELECT MAX(evomar.cod_evolucion_marcador) from ontpfc.pfc_evolucion_marcador evomar
                INNER JOIN ontpfc.pfc_evolucion evo on evo.cod_evolucion = evomar.cod_evolucion
                INNER JOIN ontpfc.pfc_monitoreo mon on mon.cod_monitoreo = evo.cod_monitoreo
                WHERE evomar.cod_marcador = pn_cod_marcador and mon.cod_sol_eva = pn_cod_sol_eva and evomar.tiene_reg_hc = '0' and evo.cod_evolucion != pn_cod_evolucion);
        ELSE
            OPEN ac_lista FOR
            SELECT 
            mar.cod_marcador as "COD_MARCADOR",
            null as "COD_EVOLUCION",
            mar.descripcion as "DESCRIPCION",
            mar.desc_per_minima as "DESC_PER_MINIMA",
            mar.desc_per_maxima as "DESC_PER_MAXIMA",
            mar.cod_resultado as "COD_RESULTADO",
            mar.resultado as "RESULTADO",
            mar.fec_resultado as "FEC_RESULTADO",
            mar.tiene_reg_hc as "TIENE_REG_HC",
            mar.p_tipo_ingreso_res as "P_TIPO_INGRESO_RES",
            mar.unidad_medida as "UNIDAD_MEDIDA"
            FROM ontpfc.pfc_evolucion_marcador mar
            WHERE cod_evolucion_marcador =(
                SELECT MAX(evomar.cod_evolucion_marcador) from ontpfc.pfc_evolucion_marcador evomar
                INNER JOIN ontpfc.pfc_evolucion evo on evo.cod_evolucion = evomar.cod_evolucion
                INNER JOIN ontpfc.pfc_monitoreo mon on mon.cod_monitoreo = evo.cod_monitoreo
                WHERE evomar.cod_marcador = pn_cod_marcador and mon.cod_sol_eva = pn_cod_sol_eva and evomar.tiene_reg_hc = '0');
        END IF;

        pn_cod_resultado := 0;
        pv_msg_resultado := 'Busqueda exitosa';
        
        EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[SP_PFC_S_ULT_MARCADOR] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_s_ult_marcador;
    
    PROCEDURE sp_pfc_s_ult_monitoreo (
        pn_cod_linea_trat          IN NUMBER,
        pv_fecha                   IN VARCHAR2,
        pv_cod_grp_diag            IN VARCHAR2,
        ac_listamonitoreo          OUT pck_pfc_constante.typcur,
        pn_cod_resultado           OUT NUMBER,
        pv_msg_resultado           OUT VARCHAR2
    ) AS
        
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'No se puede obtener la lista';
        
        OPEN ac_listamonitoreo FOR 
        select * from(
            select 
            mon.cod_monitoreo as "COD_MONITOREO",
            mon.cod_desc_monitoreo as "DESC_COD_MONITOREO",
            sol.edad_paciente as "EDAD_PACIENTE",
            mac.descripcion as "NOM_MEDICAMENTO",
            mac.cod_mac as "COD_MEDICAMENTO",
            mon.p_estado_monitoreo as "COD_ESTADOMONITOREO",
            (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = mon.p_estado_monitoreo) AS "DESC_ESTADOMONITOREO",
            TO_CHAR(mon.fec_monitoreo,'DD/MM/YYYY') as "FEC_MONITOREO",
            TO_CHAR(mon.fec_prox_monitoreo, 'DD/MM/YYYY') as "FEC_PROX_MONITOREO",
            (select par.apellidos||', '||par.nombres from ontpfc.pfc_participante par
                inner join (
                    select pgd.cod_participante, pgd.cod_grp_diag, pgd.p_rango_edad, parms.valor1, regexp_substr(parms.valor1,'([^-]+)',1,1,'',1) AS v_edad_min, regexp_substr(parms.valor1,'([^-]+)',1,2,'',1) AS v_edad_max
                    from pfc_participante_grupo_diag pgd
                    inner join pfc_parametro parms on parms.cod_parametro = pgd.p_rango_edad) pgd 
                on pgd.cod_participante = par.cod_participante
                where par.p_estado=ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_PARTICIP_ACTIVO and pgd.cod_grp_diag = pv_cod_grp_diag and sol.edad_paciente >= pgd.v_edad_min and sol.edad_paciente <= pgd.v_edad_max--8 
                and rownum <= 1
            ) as "NOM_RESPONSABLEMONITOREO",
            (select par.cod_usuario from ontpfc.pfc_participante par
                inner join (
                    select pgd.cod_participante, pgd.cod_grp_diag, pgd.p_rango_edad, parms.valor1, regexp_substr(parms.valor1,'([^-]+)',1,1,'',1) AS v_edad_min, regexp_substr(parms.valor1,'([^-]+)',1,2,'',1) AS v_edad_max
                    from pfc_participante_grupo_diag pgd
                    inner join pfc_parametro parms on parms.cod_parametro = pgd.p_rango_edad) pgd 
                on pgd.cod_participante = par.cod_participante
                where par.p_estado=ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_PARTICIP_ACTIVO and pgd.cod_grp_diag = pv_cod_grp_diag and sol.edad_paciente >= pgd.v_edad_min and sol.edad_paciente <= pgd.v_edad_max--8
                and rownum <= 1
            ) as "COD_RESPONSABLEMONITOREO",
            histlin.cod_hist_linea_trat as "COD_HIST_LINEA_TRAT",
            evo.cod_evolucion as "COD_EVOLUCION",
            evo.p_res_evolucion as "P_RES_EVOLUCION",
            (SELECT par.nombre FROM ontpfc.pfc_parametro par WHERE par.cod_parametro = evo.p_res_evolucion) AS "DESC_RES_EVOLUCION"
            from ontpfc.pfc_monitoreo mon
            inner join ontpfc.pfc_solicitud_evaluacion solev on solev.cod_sol_eva=mon.cod_sol_eva
            inner join ontpfc.pfc_solicitud_preliminar solpre on solpre.cod_sol_pre=solev.cod_sol_pre
            inner join ontpfc.pfc_solben sol on sol.cod_scg=solpre.cod_scg
            inner join ontpfc.pfc_mac mac on mac.cod_mac = solpre.cod_mac
            inner join ontpfc.pfc_hist_linea_tratamiento histlin on histlin.cod_sol_eva=mon.cod_sol_eva
            left join ontpfc.pfc_evolucion evo on evo.cod_monitoreo = mon.cod_monitoreo
            where 
            histlin.cod_hist_linea_trat = pn_cod_linea_trat and 
            mon.fec_monitoreo <= TO_DATE(pv_fecha,'DD/MM/YYYY') and mon.p_estado_monitoreo = ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_ATENDIDO_MONITOREO
            order by mon.fec_monitoreo desc, mon.cod_monitoreo desc)
        where rownum < 2;

        pn_cod_resultado := 0;
        pv_msg_resultado := 'EXITO';
    EXCEPTION
        WHEN OTHERS THEN
            pn_cod_resultado := -1;
            pv_msg_resultado := '[[SP_PFC_S_ULT_MONITOREO]] | codigo error: ' || sqlcode || ' | mensaje error: ' || sqlerrm;
    END sp_pfc_s_ult_monitoreo;
    
    PROCEDURE sp_pfc_s_participante (
      pn_cod_rol            IN        NUMBER,
      pn_cod_usuario        IN        NUMBER,
      pn_cod_participante   IN        NUMBER,
      ac_lista              OUT       PCK_PFC_CONSTANTE.TYPCUR,
      pn_cod_resultado      OUT       NUMBER,
      pv_msg_resultado      OUT       VARCHAR2
   ) AS
      l_sql_stmt      VARCHAR2(32767);
      l_condiciones   VARCHAR2(4000);
   BEGIN
      pn_cod_resultado   := -1;
      pv_msg_resultado   := 'No se puede obtener la lista';
      
      IF ( pn_cod_rol IS NOT NULL ) THEN
         l_condiciones := l_condiciones || ' and par.cod_rol = ' || pn_cod_rol || '';
      END IF;

      IF ( pn_cod_usuario IS NOT NULL ) THEN
         l_condiciones := l_condiciones || ' and par.cod_usuario = ' || pn_cod_usuario || '';
      END IF;
      
      IF ( pn_cod_participante IS NOT NULL ) THEN
         l_condiciones := l_condiciones || ' and par.cod_participante = ' || pn_cod_participante || '';
      END IF;

      l_sql_stmt := '
            select
            par.cod_participante_largo as "COD_PARTICIPANTE_LARGO",
            par.cod_rol as "COD_ROL",
            par.cod_participante as "COD_PARTICIPANTE", 
            par.nombres as "NOMBRES", 
            par.apellidos as "APELLIDOS", 
            par.correo_electronico as "CORREO_ELECTRONICO",
            par.cmp_medico as "CMP_MEDICO",
            par.cod_usuario as "COD_USUARIO",
            null as "COD_GRP_DIAG",
            null as "P_RANGO_EDAD"
            from pfc_participante par
            where par.p_estado = 8' || l_condiciones;
      OPEN ac_lista FOR l_sql_stmt;

      pn_cod_resultado   := 0;
      pv_msg_resultado   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         pn_cod_resultado   := -1;
         pv_msg_resultado   := '[[SP_PFC_S_PARTICIPANTE]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
   END sp_pfc_s_participante;
   
   PROCEDURE sp_pfc_s_participante_det (
      pn_cod_rol            IN        NUMBER,
      pv_cod_grp_diag       IN        VARCHAR2,
      pn_p_rango_edad       IN        NUMBER,
      pn_cod_usuario        IN        NUMBER,
      pn_cod_participante   IN        NUMBER,
      ac_lista              OUT       PCK_PFC_CONSTANTE.TYPCUR,
      pn_cod_resultado      OUT       NUMBER,
      pv_msg_resultado      OUT       VARCHAR2
   ) AS
      l_sql_stmt      VARCHAR2(32767);
      l_condiciones   VARCHAR2(4000);
   BEGIN
      pn_cod_resultado   := -1;
      pv_msg_resultado   := 'No se puede obtener la lista';
      
      IF ( pn_cod_rol IS NOT NULL ) THEN
         l_condiciones := l_condiciones || ' and par.cod_rol = ' || pn_cod_rol || '';
      END IF;
      
      IF ( pv_cod_grp_diag IS NOT NULL ) THEN
         l_condiciones := l_condiciones || ' and pgd.cod_grp_diag = ''' || pv_cod_grp_diag || '''';
      END IF;
      
      IF ( pn_p_rango_edad IS NOT NULL ) THEN
         l_condiciones := l_condiciones || ' and pgd.p_rango_edad = ' || pn_p_rango_edad || '';
      END IF;

      IF ( pn_cod_usuario IS NOT NULL ) THEN
         l_condiciones := l_condiciones || ' and par.cod_usuario = ' || pn_cod_usuario || '';
      END IF;
      
      IF ( pn_cod_participante IS NOT NULL ) THEN
         l_condiciones := l_condiciones || ' and par.cod_participante = ' || pn_cod_participante || '';
      END IF;

      l_sql_stmt := '
            select
            par.cod_participante_largo as "COD_PARTICIPANTE_LARGO",
            par.cod_rol as "COD_ROL",
            par.cod_participante as "COD_PARTICIPANTE", 
            par.nombres as "NOMBRES", 
            par.apellidos as "APELLIDOS", 
            par.correo_electronico as "CORREO_ELECTRONICO",
            par.cmp_medico as "CMP_MEDICO",
            par.cod_usuario as "COD_USUARIO",
            pgd.cod_grp_diag as "COD_GRP_DIAG",
            pgd.p_rango_edad as "P_RANGO_EDAD"
            from pfc_participante par
            left join pfc_participante_grupo_diag pgd on pgd.cod_participante = par.cod_participante
            where par.p_estado = '|| ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_PARTICIP_ACTIVO ||' '|| l_condiciones;
      
      OPEN ac_lista FOR l_sql_stmt;

      pn_cod_resultado   := 0;
      pv_msg_resultado   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         pn_cod_resultado   := -1;
         pv_msg_resultado   := '[[SP_PFC_S_PARTICIPANTE_DET]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
   END sp_pfc_s_participante_det;
   
   PROCEDURE sp_pfc_u_soleva_correo_cmac (
      pn_cod_envio          IN        NUMBER,
      pv_cod_plantilla      IN        VARCHAR2,
      pn_tipo_envio         IN        NUMBER,
      pn_flag_adjunto       IN        NUMBER,
      pv_ruta               IN        VARCHAR2,
      pv_usrapp             IN        VARCHAR2,
      pv_asunto             IN        VARCHAR2,
      pv_cuerpo             IN        VARCHAR2,
      pv_solicitudes        IN        VARCHAR2,
      pv_destinatario        IN        VARCHAR2,
      pn_cod_resultado      OUT       NUMBER,
      pv_msg_resultado      OUT       VARCHAR2
   ) AS
      v_cod_correo_participante   NUMBER;
      CURSOR tc_sol_evalucion IS 
        SELECT
          regexp_substr(res_solicitudes,'([^,]+)',1,1,'',1) AS v_cod_sol_eva,
          regexp_substr(res_solicitudes,'([^,]+)',1,2,'',1) AS v_estado_correo_env_cmac
        FROM (
        SELECT regexp_substr(pv_solicitudes,'[^|]+',1,level) 
        AS res_solicitudes 
        FROM dual CONNECT BY regexp_substr(pv_solicitudes,'[^|]+',1,level) IS NOT NULL);
            
   BEGIN
      pn_cod_resultado   := -1;
      pv_msg_resultado   := 'No se pudo actualizar';
      
      SELECT ontpfc.SQ_PFC_COR_PART_COD_COR_PART.NEXTVAL INTO v_cod_correo_participante FROM DUAL;
      
      INSERT INTO ontpfc.pfc_correo_participante (cod_correo_participante,cod_envio,cod_plantilla,
      tipo_envio,flag_adjunto,ruta,usrapp,asunto,cuerpo,destinatario,fec_crea) 
      VALUES (v_cod_correo_participante,pn_cod_envio,pv_cod_plantilla,
      pn_tipo_envio,pn_flag_adjunto,pv_ruta,pv_usrapp,pv_asunto,pv_cuerpo,pv_destinatario,SYSDATE);
        
      FOR v_item IN tc_sol_evalucion LOOP
        UPDATE ontpfc.pfc_solicitud_evaluacion SET codigo_envio_env_mac = pn_cod_envio, estado_correo_env_cmac = v_item.v_estado_correo_env_cmac WHERE cod_sol_eva =  v_item.v_cod_sol_eva;
      END LOOP;
      
      COMMIT;
      pn_cod_resultado   := 0;
      pv_msg_resultado   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         pn_cod_resultado   := -1;
         pv_msg_resultado   := '[[SP_PFC_U_SOLEVA_CORREO_CMAC]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
         ROLLBACK;
   END sp_pfc_u_soleva_correo_cmac;
   
   PROCEDURE sp_pfc_u_soleva_correo_lidtum (
      pn_cod_envio              IN        NUMBER,
      pv_cod_plantilla          IN        VARCHAR2,
      pn_tipo_envio             IN        NUMBER,
      pn_flag_adjunto           IN        NUMBER,
      pv_ruta                   IN        VARCHAR2,
      pv_usrapp                 IN        VARCHAR2,
      pv_asunto                 IN        VARCHAR2,
      pv_cuerpo                 IN        VARCHAR2,
      pn_cod_sol_eva            IN        NUMBER,
      pn_estado_correo_lidtum   IN        NUMBER,
      pv_destinatario           IN        VARCHAR2,
      pn_cod_resultado          OUT       NUMBER,
      pv_msg_resultado          OUT       VARCHAR2
   ) AS
      v_cod_correo_participante   NUMBER;
      
   BEGIN
      pn_cod_resultado   := -1;
      pv_msg_resultado   := 'No se pudo actualizar';
      
      SELECT ontpfc.SQ_PFC_COR_PART_COD_COR_PART.NEXTVAL INTO v_cod_correo_participante FROM DUAL;
      
      INSERT INTO ontpfc.pfc_correo_participante (cod_correo_participante,cod_envio,cod_plantilla,
      tipo_envio,flag_adjunto,ruta,usrapp,asunto,cuerpo,destinatario,fec_crea) 
      VALUES (v_cod_correo_participante,pn_cod_envio,pv_cod_plantilla,
      pn_tipo_envio,pn_flag_adjunto,pv_ruta,pv_usrapp,pv_asunto,pv_cuerpo,pv_destinatario,SYSDATE);
        
      UPDATE ontpfc.pfc_solicitud_evaluacion SET codigo_envio_env_lider_tumor = pn_cod_envio, estado_correo_env_lider_tumor = pn_estado_correo_lidtum WHERE cod_sol_eva =  pn_cod_sol_eva;
      
      COMMIT;
      pn_cod_resultado   := 0;
      pv_msg_resultado   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         pn_cod_resultado   := -1;
         pv_msg_resultado   := '[[SP_PFC_U_SOLEVA_CORREO_CMAC]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
         ROLLBACK;
   END sp_pfc_u_soleva_correo_lidtum;
   
   PROCEDURE sp_pfc_s_correo_participante (
        pn_cod_envio            IN NUMBER,
        ac_lista                OUT pck_pfc_constante.typcur,
        pn_cod_resultado        OUT NUMBER,
        pv_msg_resultado        OUT VARCHAR2
    ) AS
    
    BEGIN
        pn_cod_resultado :=-1;
        pv_msg_resultado := 'valor inicial';

        OPEN ac_lista FOR
        SELECT
        cod_envio as "COD_ENVIO",
        cod_plantilla as "COD_PLANTILLA",
        tipo_envio as "TIPO_ENVIO",
        flag_adjunto as "FLAG ADJUNTO", 
        ruta as "RUTA",
        usrapp as "USRAPP",
        asunto as "ASUNTO",
        cuerpo as "CUERPO",
        destinatario as "DESTINATARIO"
        FROM ontpfc.pfc_correo_participante 
        WHERE cod_envio = pn_cod_envio;

        pn_cod_resultado := 0;
        pv_msg_resultado := 'Busqueda exitosa';
        
        EXCEPTION
        WHEN OTHERS THEN
            pn_cod_resultado :=-1;
            pv_msg_resultado := '[SP_PFC_S_CORREO_PARTICIPANTE] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END sp_pfc_s_correo_participante;
    
    PROCEDURE sp_pfc_u_params_cmac_eva (
      PN_COD_SOL_EVA                  IN      NUMBER,
      PN_COD_ENVIO_ENV_MAC            IN      NUMBER,
      PN_EST_CORREO_ENV_CMAC          IN      NUMBER,
      AC_LISTA                        OUT     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO                OUT     NUMBER,
      PV_MSG_RESULTADO                OUT     VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      UPDATE ONTPFC.PFC_SOLICITUD_EVALUACION
      SET
         ESTADO_CORREO_ENV_CMAC = PN_EST_CORREO_ENV_CMAC,
         CODIGO_ENVIO_ENV_MAC = PN_COD_ENVIO_ENV_MAC
      WHERE
         COD_SOL_EVA = PN_COD_SOL_EVA;
      
      COMMIT;
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Actualizacion exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_U_PARAMS_CMAC_EVA] : ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
   END sp_pfc_u_params_cmac_eva;
   
   PROCEDURE SP_PFC_S_SOLEVA_X_COD_ACTA (
        PN_COD_SOL_EVA          IN NUMBER,
        AC_LISTA                OUT pck_pfc_constante.typcur,
        PN_COD_RESULTADO        OUT NUMBER,
        PV_MSG_RESULTADO        OUT VARCHAR2
    ) AS
        V_COD_ACTA          VARCHAR2(50);
    BEGIN
        PN_COD_RESULTADO :=-1;
        PV_MSG_RESULTADO := 'valor inicial';

        SELECT PC.COD_ACTA INTO V_COD_ACTA
        FROM ONTPFC.PFC_SOLICITUD_EVALUACION SE
        INNER JOIN PFC_PROGRAMACION_CMAC_DET PCD ON PCD.COD_SOL_EVA = SE.COD_SOL_EVA
        INNER JOIN PFC_PROGRAMACION_CMAC PC ON PC.COD_PROGRAMACION_CMAC = PCD.COD_PROGRAMACION_CMAC
        WHERE SE.COD_SOL_EVA = PN_COD_SOL_EVA;
        
        IF (V_COD_ACTA IS NOT NULL) THEN
            OPEN AC_LISTA FOR
            SELECT
            SE.COD_SOL_EVA,
            SE.COD_DESC_SOL_EVA,
            SO.COD_AFI_PACIENTE,
            SO.COD_DIAGNOSTICO,
            MA.COD_MAC_LARGO,
            MA.DESCRIPCION AS "DESC_MAC",
            TO_CHAR(FEC_REUNION, 'DD/MM/YYYY')||' '||HORA_REUNION AS "FEC_REUNION"
            FROM ONTPFC.PFC_SOLICITUD_EVALUACION SE
            INNER JOIN ONTPFC.PFC_SOLICITUD_PRELIMINAR SP ON SP.COD_SOL_PRE = SE.COD_SOL_PRE
            INNER JOIN ONTPFC.PFC_SOLBEN SO ON SO.COD_SCG = SP.COD_SCG
            INNER JOIN ONTPFC.PFC_MAC MA ON MA.COD_MAC = SP.COD_MAC
            INNER JOIN ONTPFC.PFC_PROGRAMACION_CMAC_DET PCD ON PCD.COD_SOL_EVA = SE.COD_SOL_EVA
            INNER JOIN ONTPFC.PFC_PROGRAMACION_CMAC PC ON PC.COD_PROGRAMACION_CMAC = PCD.COD_PROGRAMACION_CMAC
            WHERE PC.COD_ACTA = V_COD_ACTA;
        END IF;
        
        PN_COD_RESULTADO := 0;
        PV_MSG_RESULTADO := 'BUSQUEDA EXITOSA';
        
        EXCEPTION
        WHEN OTHERS THEN
            PN_COD_RESULTADO :=-1;
            PV_MSG_RESULTADO := '[SP_PFC_S_SOLEVA_X_COD_ACTA] : ' || TO_CHAR(sqlcode) || ' : ' || sqlerrm;
    END SP_PFC_S_SOLEVA_X_COD_ACTA;
    
    PROCEDURE SP_PFC_U_PARAMS_CMAC_EVA_V2 (
      PN_COD_ENVIO_ENV_MAC            IN      NUMBER,
      PN_EST_CORREO_ENV_CMAC          IN      NUMBER,
      AC_LISTA                        OUT     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO                OUT     NUMBER,
      PV_MSG_RESULTADO                OUT     VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      UPDATE ONTPFC.PFC_SOLICITUD_EVALUACION
      SET
         ESTADO_CORREO_ENV_CMAC = PN_EST_CORREO_ENV_CMAC
      WHERE
         CODIGO_ENVIO_ENV_MAC = PN_COD_ENVIO_ENV_MAC;
      
      COMMIT;
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Actualizacion exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_U_PARAMS_CMAC_EVA_V2] : ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
   END SP_PFC_U_PARAMS_CMAC_EVA_V2;
   
   PROCEDURE SP_PFC_S_COD_AUTO_PERT_SOL (
      PV_COD_SOL_EVA            IN      VARCHAR2,
      AC_LISTA                  OUT     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO          OUT     NUMBER,
      PV_MSG_RESULTADO          OUT     VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'VALOR INICIAL';
      
      OPEN AC_LISTA FOR 
        select 
        soleva.cod_auto_perte as "COD_USUARIO"
        from ontpfc.pfc_solicitud_evaluacion soleva
        where soleva.cod_sol_eva in (
            SELECT regexp_substr(PV_COD_SOL_EVA,'[^|]+',1,level) AS COD_SOL_EVA
            FROM dual CONNECT BY regexp_substr(PV_COD_SOL_EVA,'[^|]+',1,level) IS NOT NULL);
        
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'LISTADO EXITOSO';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_S_COD_AUTO_PERT_SOL] : ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
   END SP_PFC_S_COD_AUTO_PERT_SOL;
   
   PROCEDURE SP_PFC_S_RESP_MON_PARTICIPANTE (
      PV_COD_GRP_DIAG       IN      VARCHAR2,
      PN_EDAD               IN      NUMBER,
      PN_COD_ROL            IN      NUMBER,
      AC_LISTA              OUT     PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO      OUT     NUMBER,
      PV_MSG_RESULTADO      OUT     VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'VALOR INICIAL';
      
      OPEN AC_LISTA FOR 
      SELECT 
      PAR.COD_PARTICIPANTE,
      PAR.COD_USUARIO,
      PAR.CMP_MEDICO
      FROM ONTPFC.PFC_PARTICIPANTE PAR
      INNER JOIN (
        SELECT PGD.COD_PARTICIPANTE, PGD.COD_GRP_DIAG, PGD.P_RANGO_EDAD, PARMS.VALOR1, REGEXP_SUBSTR(PARMS.VALOR1,'([^-]+)',1,1,'',1) AS V_EDAD_MIN, REGEXP_SUBSTR(PARMS.VALOR1,'([^-]+)',1,2,'',1) AS V_EDAD_MAX
        FROM PFC_PARTICIPANTE_GRUPO_DIAG PGD
        INNER JOIN PFC_PARAMETRO PARMS ON PARMS.COD_PARAMETRO = PGD.P_RANGO_EDAD) PGD
      ON PGD.COD_PARTICIPANTE = PAR.COD_PARTICIPANTE
      WHERE PAR.P_ESTADO=ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_PARTICIP_ACTIVO 
      AND PAR.COD_ROL = PN_COD_ROL 
      AND PGD.COD_GRP_DIAG = PV_COD_GRP_DIAG
      AND PN_EDAD >= PGD.V_EDAD_MIN 
      AND PN_EDAD <= PGD.V_EDAD_MAX
      AND ROWNUM <= 1;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'LISTADO EXITOSO';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[SP_PFC_S_RESP_MON_PARTICIPANTE] : ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
   END SP_PFC_S_RESP_MON_PARTICIPANTE;
    
END pck_pfc_bandeja_monitoreo;

/
--------------------------------------------------------
--  DDL for Package Body PCK_PFC_BANDEJA_PRELIMINAR
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "ONTPFC"."PCK_PFC_BANDEJA_PRELIMINAR" AS

  /* AUTHOR  : ALEX FLORES*/
  /* CREATED : 10/12/2018 03:18:57 P.M.*/
  /* MODIFIED: <DdiazR> 08/04/2019*/
  /* PURPOSE :*/

   PROCEDURE SP_PFC_S_BANDEJA_PRELIMINAR (
      PN_ID_SOL_PRELIMINAR           IN                             NUMBER,
      PV_PACIENTE                    IN                             VARCHAR2,
      PV_COD_SCG_SOLBEN              IN                             VARCHAR2,
      PV_TIPO_SCG_SOLBEN             IN                             VARCHAR2,
      PV_CLINICA                     IN                             VARCHAR2,
      PV_FECHA_INI_REGISTRO_SOL      IN                             VARCHAR2,
      PV_FECHA_FIN_REGISTRO_SOL      IN                             VARCHAR2,
      PV_ESTADO_SOL_PRE              IN                             VARCHAR2,
      PV_AUTORIZADOR_PERTENENCIA     IN                             VARCHAR2,
      PN_INDEX                       IN                             NUMBER,
      PN_LONGITUD                    IN                             NUMBER,
      PN_TOTAL                       OUT                            NUMBER,
      AC_LISTA_DETALLE_PREELIMINAR   OUT                            PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO               OUT                            NUMBER,
      PV_MSG_RESULTADO               OUT                            VARCHAR2
   ) AS
      L_SQL_STMT       VARCHAR2(1000);
      PV_AUTORIZADOR   VARCHAR2(20);
      V_INICIO         NUMBER;
      V_FIN            NUMBER;
   BEGIN
      V_INICIO           := PN_INDEX + 1;
      V_FIN              := PN_LONGITUD + PN_INDEX;
      
      /*IF NULLIF(PV_AUTORIZADOR_PERTENENCIA,'') IS NULL THEN
         PV_AUTORIZADOR := PV_AUTORIZADOR_PERTENENCIA;
      ELSE
         BEGIN
            SELECT US.COD_USUARIO_SOLBEN INTO PV_AUTORIZADOR FROM ONTPPA.PPA_USUARIO_SOLBEN US
            WHERE US.COD_USUARIO = PV_AUTORIZADOR_PERTENENCIA;

         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               PV_AUTORIZADOR := PV_AUTORIZADOR_PERTENENCIA;
         END;
      END IF;*/
      
      SELECT
         COUNT(A.COD_SOL_PRE)
      INTO PN_TOTAL
      FROM
         PFC_SOLICITUD_PRELIMINAR    A
         INNER JOIN PFC_SOLBEN                  B ON A.COD_SCG = B.COD_SCG
         LEFT JOIN PFC_PARAMETRO               TIPOSOL ON B.TIPO_SCG_SOLBEN = TIPOSOL.COD_PARAMETRO
         LEFT JOIN PFC_PARAMETRO               ESTAPRE ON A.P_ESTADO_SOL_PRE = ESTAPRE.COD_PARAMETRO
         LEFT JOIN ONTPPA.PPA_USUARIO_SOLBEN   SOLB ON B.TX_DATO_ADIC1 = SOLB.COD_USUARIO_SOLBEN
         INNER JOIN ONTPPA.PPA_USUARIO          USUA ON USUA.COD_USUARIO = SOLB.COD_USUARIO
      WHERE
         ( PN_ID_SOL_PRELIMINAR IS NULL
           OR ( PN_ID_SOL_PRELIMINAR IS NOT NULL
                AND A.COD_SOL_PRE = PN_ID_SOL_PRELIMINAR ) )
         AND ( PV_PACIENTE IS NULL
               OR ( PV_PACIENTE IS NOT NULL
                    AND B.COD_AFI_PACIENTE  = PV_PACIENTE ) )
         AND ( PV_COD_SCG_SOLBEN IS NULL
               OR ( PV_COD_SCG_SOLBEN IS NOT NULL
                    AND B.COD_SCG_SOLBEN = PV_COD_SCG_SOLBEN ) )
         AND ( PV_TIPO_SCG_SOLBEN IS NULL
               OR ( PV_TIPO_SCG_SOLBEN IS NOT NULL
                    AND B.TIPO_SCG_SOLBEN = PV_TIPO_SCG_SOLBEN ) )
         AND ( PV_CLINICA IS NULL
               OR ( PV_CLINICA IS NOT NULL
                    AND B.COD_CLINICA       = PV_CLINICA ) )
         AND ( PV_FECHA_INI_REGISTRO_SOL IS NULL
               OR ( PV_FECHA_INI_REGISTRO_SOL IS NOT NULL
                    AND A.FEC_SOL_PRE >= TO_DATE(
            PV_FECHA_INI_REGISTRO_SOL,
            'DD/MM/YYYY'
         ) ) )
         AND ( PV_FECHA_FIN_REGISTRO_SOL IS NULL
               OR ( PV_FECHA_FIN_REGISTRO_SOL IS NOT NULL
                    AND A.FEC_SOL_PRE <= ( TO_DATE(
            PV_FECHA_FIN_REGISTRO_SOL,
            'DD/MM/YYYY'
         ) + 1 ) ) )
         AND ( PV_ESTADO_SOL_PRE IS NULL
               OR ( PV_ESTADO_SOL_PRE IS NOT NULL
                    AND A.P_ESTADO_SOL_PRE = PV_ESTADO_SOL_PRE ) )
         AND ( PV_AUTORIZADOR_PERTENENCIA IS NULL
               OR ( PV_AUTORIZADOR_PERTENENCIA IS NOT NULL
                    AND B.TX_DATO_ADIC1 IN (
                        SELECT US.COD_USUARIO_SOLBEN FROM ONTPPA.PPA_USUARIO_SOLBEN US
                        WHERE US.COD_USUARIO = PV_AUTORIZADOR_PERTENENCIA
                    ) ) );

      OPEN AC_LISTA_DETALLE_PREELIMINAR FOR SELECT
                                              *
                                           FROM
                                              (
                                                 SELECT
                                                    ROW_NUMBER() OVER(
                                                       ORDER BY
                                                          A.FEC_SOL_PRE DESC
                                                    ) RN,
                                                    A.COD_SOL_PRE,
                                                    A.FEC_SOL_PRE        AS FECHA,
                                                    TO_CHAR(
                                                       CAST(A.FEC_SOL_PRE AS DATE),
                                                       'hh24:mi:ss'
                                                    ) AS HORA,
                                                    B.COD_SCG_SOLBEN,
                                                    B.FEC_SCG_SOLBEN,
                                                    B.COD_CLINICA        AS COD_CLINICA,
                                                    B.COD_AFI_PACIENTE   AS COD_PACIENTE,
                                                    TO_CHAR(
                                                       TO_DATE(
                                                          B.HORA_SCG_SOLBEN,
                                                          'hh24:mi:ss'
                                                       ),
                                                       'hh24:mi:ss'
                                                    ) AS HORA_SCG_SOLBEN,
                                                    TIPOSOL.NOMBRE       AS TIPO_SCG_SOLBEN,
                                                    ESTAPRE.NOMBRE       AS ESTADO_SOL_PRE,
                                                    USUA.APE_PAT
                                                    || ' '
                                                    || USUA.APE_MAT
                                                    || ', '
                                                    || USUA.NOMBRES AS MEDICO_TRATANTE_PRESCRIPTOR,
                                                    B.TX_DATO_ADIC1
                                                 FROM
                                                    PFC_SOLICITUD_PRELIMINAR    A
                                                    INNER JOIN PFC_SOLBEN                  B ON A.COD_SCG = B.COD_SCG
                                                    LEFT JOIN PFC_PARAMETRO               TIPOSOL ON B.TIPO_SCG_SOLBEN = TIPOSOL.COD_PARAMETRO
                                                    LEFT JOIN PFC_PARAMETRO               ESTAPRE ON A.P_ESTADO_SOL_PRE = ESTAPRE.COD_PARAMETRO
                                                    LEFT JOIN ONTPPA.PPA_USUARIO_SOLBEN   SOLB ON B.TX_DATO_ADIC1 = SOLB.COD_USUARIO_SOLBEN
                                                    INNER JOIN ONTPPA.PPA_USUARIO          USUA ON USUA.COD_USUARIO = SOLB.COD_USUARIO
                                                 WHERE
                                                    ( PN_ID_SOL_PRELIMINAR IS NULL
                                                      OR ( PN_ID_SOL_PRELIMINAR IS NOT NULL
                                                           AND A.COD_SOL_PRE = PN_ID_SOL_PRELIMINAR ) )
                                                    AND ( PV_PACIENTE IS NULL
                                                          OR ( PV_PACIENTE IS NOT NULL
                                                               AND B.COD_AFI_PACIENTE = PV_PACIENTE ) )
                                                    AND ( PV_COD_SCG_SOLBEN IS NULL
                                                          OR ( PV_COD_SCG_SOLBEN IS NOT NULL
                                                               AND B.COD_SCG_SOLBEN = PV_COD_SCG_SOLBEN ) )
                                                    AND ( PV_TIPO_SCG_SOLBEN IS NULL
                                                          OR ( PV_TIPO_SCG_SOLBEN IS NOT NULL
                                                               AND B.TIPO_SCG_SOLBEN = PV_TIPO_SCG_SOLBEN ) )
                                                    AND ( PV_CLINICA IS NULL
                                                          OR ( PV_CLINICA IS NOT NULL
                                                               AND B.COD_CLINICA = PV_CLINICA ) )
                                                    AND ( PV_FECHA_INI_REGISTRO_SOL IS NULL
                                                          OR ( PV_FECHA_INI_REGISTRO_SOL IS NOT NULL
                                                               AND A.FEC_SOL_PRE >= TO_DATE(PV_FECHA_INI_REGISTRO_SOL, 'DD/MM/YYYY') ) )
                                                    AND ( PV_FECHA_FIN_REGISTRO_SOL IS NULL
                                                          OR ( PV_FECHA_FIN_REGISTRO_SOL IS NOT NULL
                                                               AND A.FEC_SOL_PRE <= TO_DATE(PV_FECHA_FIN_REGISTRO_SOL, 'DD/MM/YYYY') + 1 ) )
                                                    AND ( PV_ESTADO_SOL_PRE IS NULL
                                                          OR ( PV_ESTADO_SOL_PRE IS NOT NULL
                                                               AND A.P_ESTADO_SOL_PRE = PV_ESTADO_SOL_PRE ) )
                                                    AND ( PV_AUTORIZADOR_PERTENENCIA IS NULL
                                                          OR ( PV_AUTORIZADOR_PERTENENCIA IS NOT NULL
                                                               AND B.TX_DATO_ADIC1 in (
                                                                    SELECT US.COD_USUARIO_SOLBEN FROM ONTPPA.PPA_USUARIO_SOLBEN US
                                                                    WHERE US.COD_USUARIO = PV_AUTORIZADOR_PERTENENCIA
                                                               ) ) )
                                              )
                                           WHERE
                                              RN BETWEEN V_INICIO AND V_FIN
                                           ORDER BY
                                              RN;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_BANDEJA_PRELIMINAR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_BANDEJA_PRELIMINAR;

  /* AUTHOR  : JESUS*/
  /* CREATED : XX/XX/XXXX*/
  /* PURPOSE :*/

   PROCEDURE SP_PFC_S_TIPO_CORREO (
      PN_CODTIPOCO       IN                 NUMBER,
      PV_ASUNTO          OUT                VARCHAR2,
      PV_CUERPO          OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      SELECT
         ASUNTO,
         CUERPO
      INTO
         PV_ASUNTO,
         PV_CUERPO
      FROM
         PFC_TIPO_CORREO TC
      WHERE
         TC.COD_TIPO_CORREO = PN_CODTIPOCO
         AND TC.ESTADO = '1';

      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'Consulta Exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_TIPO_CORREO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_TIPO_CORREO;

  /* AUTHOR  : CATALINA*/
  /* CREATED : 02/01/2019*/
  /* PURPOSE : Lista un SCG de una Solicitud Preliminar*/

   PROCEDURE SP_PFC_S_DET_PRELIMINAR_SCG (
      PN_COD_SOL_PRE     IN                 NUMBER,
      AC_INFOSOLBEN      OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN AC_INFOSOLBEN FOR SELECT
                                S.COD_SCG            AS COD_SOLBEN,
                                S.COD_SCG_SOLBEN     AS NRO_SOLBEN,
                                S.ESTADO_SCG         AS COD_EST_SOLBEN,
                                (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.CODIGO = S.ESTADO_SCG AND P.COD_GRUPO = 5) AS ESTADO_SOLBEN,
                                --ESTSOL.NOMBRE        AS ESTADO_SOLBEN,
                                S.FEC_SCG_SOLBEN,
                                S.HORA_SCG_SOLBEN,
                                S.TIPO_SCG_SOLBEN    AS COD_TIPO_SOLBEN,
                                (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = S.TIPO_SCG_SOLBEN) AS TIPO_SOLBEN,
                                --TIPOSOL.NOMBRE       AS TIPO_SOLBEN,
                                S.COD_CLINICA,
                                S.MEDICO_TRATANTE_PRESCRIPTOR,
                                S.CMP_MEDICO,
                                S.FEC_RECETA,
                                S.FEC_QUIMIO,
                                S.FEC_HOSP_INICIO,
                                S.FEC_HOSP_FIN,
                                S.DESC_MEDICAMENTO,
                                S.DESC_ESQUEMA,
                                S.PERSON_CONTACTO,
                                S.TOTAL_PRESUPUESTO,
                                S.OBS_CLINICA,
                                S.EDAD_PACIENTE,
                                S.COD_DIAGNOSTICO,
                                S.DES_CONTRATANTE,
                                S.DES_PLAN,
                                S.COD_AFI_PACIENTE,
                                S.FEC_AFILIACION,
                                P.P_ESTADO_SOL_PRE   AS COD_ESTADO_PRE,
                                (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = P.P_ESTADO_SOL_PRE) AS ESTADO_PRELIMINAR
                                --ESTPRE.NOMBRE        AS ESTADO_PRELIMINAR
                             FROM
                                PFC_SOLICITUD_PRELIMINAR   P
                                INNER JOIN PFC_SOLBEN                 S ON P.COD_SCG = S.COD_SCG
                                --LEFT JOIN PFC_PARAMETRO              ESTSOL ON ESTSOL.CODIGO = S.ESTADO_SCG
                                --LEFT JOIN PFC_PARAMETRO              TIPOSOL ON TIPOSOL.COD_PARAMETRO = S.TIPO_SCG_SOLBEN
                                --LEFT JOIN PFC_PARAMETRO              ESTPRE ON ESTPRE.COD_PARAMETRO = P.P_ESTADO_SOL_PRE
             /*AND sp.P_ESTADO_SOL_PRE = v_p_estado_sol_pre*/
                             WHERE
                                P.COD_SOL_PRE = PN_COD_SOL_PRE;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_DET_PRELIMINAR_SCG]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_DET_PRELIMINAR_SCG;

  /* AUTHOR  : CATALINA - JAZABACHE*/
  /* CREATED : 07/01/2019*/
  /* PURPOSE : Actualiza el codigo de solicitud preliminar*/

   PROCEDURE SP_PFC_U_DETALLE_PRELIMINAR (
      PN_COD_SOL_PRE        IN                    NUMBER,
      PN_P_ESTADO_SOL_PRE   IN                    NUMBER,
      PN_COD_MAC            IN                    NUMBER,
      PV_FECHA_ACTUAL       IN                    VARCHAR2,
      PN_USUARIO            IN                    NUMBER,
      PV_ESTADO_SOL_PRE     OUT                   VARCHAR2,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS

      V_COD_SOL_PRE_COUNT   NUMBER;
      V_COD_SOL_EVA         NUMBER;
      V_COD_LARGO           VARCHAR2(30);
      V_COD_RESULTADO       NUMBER;
      V_MSG_RESULTADO       VARCHAR2(50);
      V_FECHA_SOL_EVA       TIMESTAMP;
      V_FECHA_HORA_ACTUAL   DATE;
      V_MENSAJE             VARCHAR2(100);
      V_EXC_CAMPO EXCEPTION;
   BEGIN
      PN_COD_RESULTADO      := -1;
      PV_MSG_RESULTADO      := 'valor inicial';
      SELECT
         COUNT(*)
      INTO V_COD_SOL_PRE_COUNT
      FROM
         PFC_SOLICITUD_PRELIMINAR SP
      WHERE
         SP.COD_SOL_PRE = PN_COD_SOL_PRE;

      IF NULLIF(
         PV_FECHA_ACTUAL,
         ''
      ) IS NULL OR FN_PFC_S_VALIDACION_FECHA_HORA(PV_FECHA_ACTUAL) = 1 THEN
         V_MENSAJE := 'CAMPO FECHA ACTUAL OBLIGATORIO/FORMATO(DD-MM-YYYY HH24:MI:SS)';
         RAISE V_EXC_CAMPO;
      ELSIF NULLIF(
         PN_USUARIO,
         ''
      ) IS NULL THEN
         V_MENSAJE := 'CAMPO USUARIO OBLIGATORIO';
         RAISE V_EXC_CAMPO;
      END IF;

      V_FECHA_HORA_ACTUAL   := TO_DATE(
         PV_FECHA_ACTUAL,
         'DD/MM/YYYY HH24:MI:SS'
      );
      IF V_COD_SOL_PRE_COUNT != 0 THEN
         IF PN_P_ESTADO_SOL_PRE != PCK_PFC_CONSTANTE.PN_P_ESTADO_PENDIENTE THEN
            IF PN_P_ESTADO_SOL_PRE = PCK_PFC_CONSTANTE.PN_P_ESTADO_ATENDIDO OR PN_P_ESTADO_SOL_PRE = PCK_PFC_CONSTANTE.PN_P_ESTADO_PENDIENTE_INSCRIP
            THEN
               UPDATE PFC_SOLICITUD_PRELIMINAR SP
               SET
                  SP.P_ESTADO_SOL_PRE = PN_P_ESTADO_SOL_PRE,
                  SP.COD_MAC = PN_COD_MAC,
                  SP.FECHA_MODIFICACION = V_FECHA_HORA_ACTUAL,
                  SP.USUARIO_MODIFICACION = PN_USUARIO
               WHERE
                  SP.COD_SOL_PRE = PN_COD_SOL_PRE;

               SELECT
                  SQ_PFC_SOL_EVA_COD_SOL_EVA.NEXTVAL
               INTO V_COD_SOL_EVA
               FROM
                  DUAL;

               PCK_PFC_UTIL.SP_PFC_CODIGO_LARGO(
                  V_COD_SOL_EVA,
                  PCK_PFC_CONSTANTE.PN_COD_LARGO,
                  V_COD_LARGO,
                  V_COD_RESULTADO,
                  V_MSG_RESULTADO
               );
               IF V_COD_RESULTADO <> 0 THEN
                  V_COD_LARGO := TO_CHAR(V_COD_SOL_EVA);
               END IF;
               SELECT
                  TO_TIMESTAMP(
                     PV_FECHA_ACTUAL,
                     'DD/MM/YYYY HH24:MI:SS'
                  )
               INTO V_FECHA_SOL_EVA
               FROM
                  DUAL;

               INSERT INTO PFC_SOLICITUD_EVALUACION (
                  COD_SOL_EVA,
                  FEC_SOL_EVA,
                  P_ESTADO_SOL_EVA,
                  P_ROL_RESP_PENDIENTE_EVA,
                  COD_SOL_PRE,
                  COD_DESC_SOL_EVA,
                  FECHA_CREACION,
                  USUARIO_CREACION
               ) VALUES (
                  V_COD_SOL_EVA,
                  V_FECHA_SOL_EVA,
                  PCK_PFC_CONSTANTE.PN_ESTADO_PENDIENTE_EVA,
                  PCK_PFC_CONSTANTE.PN_ROL_AUTOR_PERTE,
                  PN_COD_SOL_PRE,
                  V_COD_LARGO,
                  V_FECHA_HORA_ACTUAL,
                  PN_USUARIO
               );

            ELSE
               UPDATE PFC_SOLICITUD_PRELIMINAR SP
               SET
                  SP.P_ESTADO_SOL_PRE = PN_P_ESTADO_SOL_PRE,
                  SP.FECHA_MODIFICACION = V_FECHA_HORA_ACTUAL,
                  SP.USUARIO_MODIFICACION = PN_USUARIO
               WHERE
                  SP.COD_SOL_PRE = PN_COD_SOL_PRE;

            END IF;

            SELECT
               PP.NOMBRE
            INTO PV_ESTADO_SOL_PRE
            FROM
               PFC_PARAMETRO PP
            WHERE
               PP.COD_PARAMETRO = PN_P_ESTADO_SOL_PRE;

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'Operación exitosa: Código de Solicitud Preliminar Actualizado';
         ELSE
            PN_COD_RESULTADO   := 1;
            PV_MSG_RESULTADO   := 'Error: No existe Estado de Solicitud Preliminar enviado';
         END IF;
      ELSE
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := 'Error: No existe Código de Solicitud Preliminar enviado';
      END IF;

   EXCEPTION
      WHEN V_EXC_CAMPO THEN
         PN_COD_RESULTADO   := -2;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_U_DETALLE_PRELIMINAR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_U_DETALLE_PRELIMINAR;

  /* AUTHOR  : CATALINA*/
  /* CREATED : XX/XX/XXXX*/
  /* PURPOSE : Actualiza el estado de la Solicitud Preliminar*/

   PROCEDURE SP_PFC_U_ESTADO_PRELIMINAR (
      PN_COD_SOL_PRE     IN                 NUMBER,
      PV_FECHA_ACTUAL    IN                 VARCHAR2,
      PN_USUARIO         IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      V_P_ESTADO_SOL_PRE    NUMBER;
      V_FECHA_HORA_ACTUAL   DATE;
      V_MENSAJE             VARCHAR2(100);
      V_EXC_CAMPO EXCEPTION;
   BEGIN
      PN_COD_RESULTADO      := -1;
      PV_MSG_RESULTADO      := 'valor inicial';
      IF NULLIF(
         PV_FECHA_ACTUAL,
         ''
      ) IS NULL OR FN_PFC_S_VALIDACION_FECHA_HORA(PV_FECHA_ACTUAL) = 1 THEN
         V_MENSAJE := 'CAMPO FECHA ACTUAL OBLIGATORIO/FORMATO(DD-MM-YYYY HH24:MI:SS)';
         RAISE V_EXC_CAMPO;
      ELSIF NULLIF(
         PN_USUARIO,
         ''
      ) IS NULL THEN
         V_MENSAJE := 'CAMPO USUARIO OBLIGATORIO';
         RAISE V_EXC_CAMPO;
      END IF;

      V_FECHA_HORA_ACTUAL   := TO_DATE(
         PV_FECHA_ACTUAL,
         'DD/MM/YYYY HH24:MI:SS'
      );
      SELECT
         SP.P_ESTADO_SOL_PRE
      INTO V_P_ESTADO_SOL_PRE
      FROM
         PFC_SOLICITUD_PRELIMINAR SP
      WHERE
         SP.COD_SOL_PRE = PN_COD_SOL_PRE;

      IF ( V_P_ESTADO_SOL_PRE = PCK_PFC_CONSTANTE.PN_P_ESTADO_PENDIENTE OR V_P_ESTADO_SOL_PRE = PCK_PFC_CONSTANTE.PN_P_ESTADO_PENDIENTE_INSCRIP

      ) THEN
         UPDATE PFC_SOLICITUD_PRELIMINAR SP
         SET
            SP.P_ESTADO_SOL_PRE = PCK_PFC_CONSTANTE.PN_P_ESTADO_PENDIENTE_INSCRIP,
            SP.FECHA_MODIFICACION = V_FECHA_HORA_ACTUAL,
            SP.USUARIO_MODIFICACION = PN_USUARIO
         WHERE
            SP.COD_SOL_PRE = PN_COD_SOL_PRE;

         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'Operación exitosa: Se actualizo a estado pendiente de inscripcion mac';
      ELSE
         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'No se pudo actualizar, no se acepta el estado ('
                             || V_P_ESTADO_SOL_PRE
                             || ')';
      END IF;

   EXCEPTION
      WHEN V_EXC_CAMPO THEN
         PN_COD_RESULTADO   := -2;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := 'Error: [[SP_PFC_U_ESTADO_PRELIMINAR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_U_ESTADO_PRELIMINAR;

  /* AUTOR  : CQUISPE - JAZABACHE*/
  /* CREADO : 01/12/2018*/
  /* PROPOSITO : REGISTRA EN LA TABLA SOLBEN Y TABLA PRELIMINAR*/

   PROCEDURE SP_PFC_SI_SCG_SOLBEN_PREL (
      PV_COD_SCG_SOLBEN                IN                               VARCHAR2,
      PV_COD_CLINICA                   IN                               VARCHAR2,
      PV_COD_AFI_PACIENTE              IN                               VARCHAR2,
      PN_EDAD_PACIENTE                 IN                               NUMBER,
      PV_DES_CONTRATANTE               IN                               VARCHAR2,
      PV_DES_PLAN                      IN                               VARCHAR2,
      PV_FEC_AFILIACION                IN                               VARCHAR2,
      PV_COD_DIAGNOSTICO               IN                               VARCHAR2,
      PV_CMP_MEDICO                    IN                               VARCHAR2,
      PV_MEDICO_TRATANTE_PRESCRIPTOR   IN                               VARCHAR2,
      PV_FEC_RECETA                    IN                               VARCHAR2,
      PV_FEC_QUIMIO                    IN                               VARCHAR2,
      PV_FEC_HOSP_INICIO               IN                               VARCHAR2,
      PV_FEC_HOSP_FIN                  IN                               VARCHAR2,
      PV_FEC_SCG_SOLBEN                IN                               VARCHAR2,
      PV_HORA_SCG_SOLBEN               IN                               VARCHAR2,
      PV_TIPO_SCG_SOLBEN               IN                               VARCHAR2,
      PV_ESTADO_SCG                    IN                               VARCHAR2,
      PV_DESC_MEDICAMENTO              IN                               VARCHAR2,
      PV_DESC_ESQUEMA                  IN                               VARCHAR2,
      PN_TOTAL_PRESUPUESTO             IN                               NUMBER,
      PV_DESC_PROCEDIMIENTO            IN                               VARCHAR2,
      PV_PERSON_CONTACTO               IN                               VARCHAR2,
      PV_OBS_CLINICA                   IN                               VARCHAR2,
      PN_IND_VALIDA                    IN                               NUMBER,
      PV_TX_DATO_ADIC1                 IN                               VARCHAR2,
      PV_TX_DATO_ADIC2                 IN                               VARCHAR2,
      PV_TX_DATO_ADIC3                 IN                               VARCHAR2,
      PV_COD_GRP_DIAGNOSTICO           IN                               VARCHAR2,
      PV_FEC_ACTUAL                    IN                               VARCHAR2,
      PV_OUT_COD_SCG_SOLBEN            OUT                              VARCHAR2,
      PN_OUT_COD_SOL_PRE               OUT                              NUMBER,
      PV_OUT_FEC_SOL_PRE               OUT                              VARCHAR2,
      PV_OUT_HORA_SOL_PRE              OUT                              VARCHAR2,
      PN_OUT_COD_RESULTADO             OUT                              NUMBER,
      PV_OUT_MSG_RESULTADO             OUT                              VARCHAR2
   ) IS

      V_COD_SCG_SEQ            NUMBER;
      V_COD_SCG_SOLBEN_COUNT   NUMBER;
      V_COD_SOL_PRE_SEQ        NUMBER;
      V_RANGO_TIEMPO           NUMBER;
      V_FEC_SOL_PRE            VARCHAR2(20);
      V_HORA_SOL_PRE           VARCHAR2(20);
      V_SOLBEN_CASE2_COUNT     NUMBER;
      V_SOLBEN_CASE3_COUNT     NUMBER;
      V_VALOR                  NUMBER;
      V_FECHA_RECETA           DATE;
      V_FECHA_QUIMIO           DATE;
      V_FECHA_AFILIACION       DATE;
      V_FECHA_SCG_SOLBEN       DATE;
      V_FECHA_HOSP_INI         DATE;
      V_FECHA_HOSP_FIN         DATE;
      V_FECHA_HORA_ACTUAL      DATE;
      V_FECHA_ACTUAL           DATE;
      V_EXC_FEC_ACTUAL EXCEPTION;
      V_EXC_CAMPO_VALIDA EXCEPTION;
        /*V_CODIGO                 NUMBER(1);*/
      V_TIPO_MEDICO            NUMBER;
      V_MENSAJE                VARCHAR2(500);
   BEGIN
      PN_OUT_COD_RESULTADO   := -1;
      PV_OUT_MSG_RESULTADO   := 'valor inicial';
      
        /*VALIDAR CAMPOS OBLIGATORIOS*/

      CASE
         WHEN NULLIF(
            PV_COD_SCG_SOLBEN,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO CODIGO SCG SOLBEN OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_COD_CLINICA,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO CODIGO CLINICA OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_COD_AFI_PACIENTE,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO CODIGO AFILIADO OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PN_EDAD_PACIENTE,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO EDAD PACIENTE OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_DES_CONTRATANTE,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO DESCRIPCION DEL CONTRATANTE OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_DES_PLAN,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO DESCRIPCION DEL PLAN OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_FEC_AFILIACION,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO FECHA DE AFILIACION ES OBLIGATORIO/FORMATO(DD-MM-YYYY)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_COD_DIAGNOSTICO,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO CODIGO DE DIAGNOSTICO OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_CMP_MEDICO,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO CMP MEDICO OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_MEDICO_TRATANTE_PRESCRIPTOR,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO MEDICO TRATANTE PRESCRIPTOR OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN ( PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_FARMACIA_COMPLEJA OR PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA
         ) AND NULLIF(
            PV_FEC_RECETA,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO FECHA RECETA ES OBLIGATORIO/FORMATO(DD-MM-YYYY) PARA EL CODIGO DE TIPO DE SOLICITUD : ' || PV_TIPO_SCG_SOLBEN
            ;
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA AND NULLIF(
            PV_FEC_QUIMIO,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO FECHA QUIMIO AMBULATORIA ES OBLIGATORIO/FORMATO(DD-MM-YYYY) PARA EL CODIGO DE TIPO DE SOLICITUD : '
            || PV_TIPO_SCG_SOLBEN;
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION AND NULLIF(
            PV_FEC_HOSP_INICIO,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO FECHA HOSPITALIZACION INICIO ES OBLIGATORIO/FORMATO(DD-MM-YYYY)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION AND NULLIF(
            PV_FEC_HOSP_FIN,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO FECHA HOSPITALIZACION FIN ES OBLIGATORIO/FORMATO(DD-MM-YYYY)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_FEC_SCG_SOLBEN,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO FECHA DE LA SOLICITUD DE CREACION SOLBEN ES OBLIGATORIO/FORMATO(DD-MM-YYYY)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_HORA_SCG_SOLBEN,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO HORA DE LA SOLICITUD DE CREACION SOLBEN OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN FN_PFC_S_VALIDACION_HORA(PV_HORA_SCG_SOLBEN) = 1 THEN
            V_MENSAJE := 'CAMPO HORA DE LA SOLICITUD DE CREACION SOLBEN NO CUMPLE CON EL FORMATO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_TIPO_SCG_SOLBEN,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO TIPO DE SOLICITUD OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN ( PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_FARMACIA_COMPLEJA OR PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION
         OR PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA ) = FALSE THEN
            V_MENSAJE := 'CODIGO DE TIPO DE SOLICITUD '
                         || PV_TIPO_SCG_SOLBEN
                         || ' ES DIFERENTE AL ESTABLECIDO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_ESTADO_SCG,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO ESTADO DE LA SOLICITUD DE CARTA DE GARANTIA SOLBEN OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PN_TOTAL_PRESUPUESTO,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO TOTAL PRESUPUESTO OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PN_IND_VALIDA,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO INDICE DE VALIDACION OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_FEC_ACTUAL,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'CAMPO FECHA ACTUAL OBLIGATORIO/FORMATO(DD-MM-YYYY  HH24:MI:SS)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN FN_PFC_S_VALIDACION_FECHA_HORA(PV_FEC_ACTUAL) = 1 THEN
            V_MENSAJE := 'ERROR EN EL TIPO DE FORMATO(DD-MM-YYYY HH24:MI:SS)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN LENGTH(PV_DES_PLAN) > 20 THEN
            V_MENSAJE := 'ERROR, SE SUPERA LA LONGITUD MAXIMA DEL CAMPO DES_PLAN';
            RAISE V_EXC_CAMPO_VALIDA;
          /*WHEN NULLIF(PV_TX_DATO_ADIC1, '') IS NULL THEN
            V_MENSAJE := 'CAMPO TX_DATO_ADIC1 ES OBLIGATORIO';
            RAISE V_EXC_CAMPO_VALIDA;*/
         WHEN LENGTH(PV_TX_DATO_ADIC1) > 20 THEN
            V_MENSAJE := 'ERROR, SE SUPERA LA LONGITUD MAXIMA DEL CAMPO TX_DATO_ADIC1';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN LENGTH(PV_TX_DATO_ADIC2) > 20 THEN
            V_MENSAJE := 'ERROR, SE SUPERA LA LONGITUD MAXIMA DEL CAMPO TX_DATO_ADIC2';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN LENGTH(PV_TX_DATO_ADIC3) > 20 THEN
            V_MENSAJE := 'ERROR, SE SUPERA LA LONGITUD MAXIMA DEL CAMPO TX_DATO_ADIC3';
            RAISE V_EXC_CAMPO_VALIDA;
         ELSE
            V_MENSAJE := NULL;
      END CASE;
        /*-----------*/
            /*LA VALIDACION DE FECHA SI ES IGUAL A 1 ES INCORRECTO*/
        /*-----------*/

      BEGIN
         IF NULLIF(
            PV_FEC_RECETA,
            ''
         ) IS NULL THEN
            V_FECHA_RECETA := NULL;
         ELSE
            V_FECHA_RECETA := TO_DATE(
               PV_FEC_RECETA,
               'DD/MM/YYYY'
            );
         END IF;

         IF NULLIF(
            PV_FEC_QUIMIO,
            ''
         ) IS NULL THEN
            V_FECHA_QUIMIO := NULL;
         ELSE
            V_FECHA_QUIMIO := TO_DATE(
               PV_FEC_QUIMIO,
               'DD/MM/YYYY'
            );
         END IF;

         IF NULLIF(
            PV_FEC_AFILIACION,
            ''
         ) IS NULL THEN
            V_FECHA_AFILIACION := NULL;
         ELSE
            V_FECHA_AFILIACION := TO_DATE(
               PV_FEC_AFILIACION,
               'DD/MM/YYYY'
            );
         END IF;

         IF NULLIF(
            PV_FEC_SCG_SOLBEN,
            ''
         ) IS NULL THEN
            V_FECHA_SCG_SOLBEN := NULL;
         ELSE
            V_FECHA_SCG_SOLBEN := TO_DATE(
               PV_FEC_SCG_SOLBEN,
               'DD/MM/YYYY'
            );
         END IF;

         IF NULLIF(
            PV_FEC_HOSP_INICIO,
            ''
         ) IS NULL THEN
            V_FECHA_HOSP_INI := NULL;
         ELSE
            V_FECHA_HOSP_INI := TO_DATE(
               PV_FEC_HOSP_INICIO,
               'DD/MM/YYYY'
            );
         END IF;

         IF NULLIF(
            PV_FEC_HOSP_FIN,
            ''
         ) IS NULL THEN
            V_FECHA_HOSP_FIN := NULL;
         ELSE
            V_FECHA_HOSP_FIN := TO_DATE(
               PV_FEC_HOSP_FIN,
               'DD/MM/YYYY'
            );
         END IF;

      EXCEPTION
         WHEN OTHERS THEN
            V_MENSAJE := 'ERROR EN EL FORMATO DE FECHA : ' || SQLERRM;
      END;

      BEGIN
         SELECT
            TO_DATE(
               PV_FEC_ACTUAL,
               'DD/MM/YYYY HH24:MI:SS'
            ),
            TO_DATE(
               TO_CHAR(
                  TO_DATE(
                     PV_FEC_ACTUAL,
                     'DD/MM/YYYY HH24:MI:SS'
                  ),
                  'DD/MM/YYYY'
               ),
               'DD/MM/YYYY'
            )
         INTO
            V_FECHA_HORA_ACTUAL,
            V_FECHA_ACTUAL
         FROM
            DUAL;

      EXCEPTION
         WHEN OTHERS THEN
            V_MENSAJE := 'ERROR EN LA FECHA ACTUAL : ' || SQLERRM;
            RAISE V_EXC_FEC_ACTUAL;
      END;

      BEGIN
         SELECT
            TO_NUMBER(
               P.VALOR1
            )
         INTO V_VALOR
         FROM
            PFC_PARAMETRO P
         WHERE
            P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_CODIGO_RANGO_TIEMPO_SOLBEN
            AND P.ESTADO = PCK_PFC_CONSTANTE.PV_ESTADO_PARAMETRO;


      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_VALOR     := 0;
            V_MENSAJE   := 'NO SE ENCONTRARON RESULTADOS PARA EL RANGO DE TIEMPO SOLBEN EN LOS PARAMETROS';
            RAISE V_EXC_CAMPO_VALIDA;
      END;
    
    /*VALIDA SI EL 'COD_SCG_SOLBEN' QUE ENVIA SOLBEN EXISTE EN LA TABLA SOLBEN*/

      SELECT
         COUNT(*)
      INTO V_COD_SCG_SOLBEN_COUNT
      FROM
         PFC_SOLBEN                 SB,
         PFC_SOLICITUD_PRELIMINAR   PRE
      WHERE
         SB.COD_SCG = PRE.COD_SCG
         AND SB.COD_SCG_SOLBEN = PV_COD_SCG_SOLBEN;
    
    /*VALIDATE RANGO DE TIEMPO */

      SELECT
         TRUNC(V_FECHA_ACTUAL) - TRUNC(V_FECHA_SCG_SOLBEN) AS RANGOTIEMPO
      INTO V_RANGO_TIEMPO
      FROM
         DUAL;

      IF PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_FARMACIA_COMPLEJA THEN
         V_TIPO_MEDICO := PCK_PFC_CONSTANTE.PN_CODIGO_MEDICO_PRESCRIPTOR;
      ELSIF PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA OR PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION
      THEN
         V_TIPO_MEDICO := PCK_PFC_CONSTANTE.PN_CODIGO_MEDICO_TRATANTE;
      END IF;
        
    /*GET NUEVO REGISTRO DE UNA SCG DE SOLBEN*/

      CASE
         WHEN V_COD_SCG_SOLBEN_COUNT = 0 THEN
            IF ( PN_IND_VALIDA = 0 ) THEN
               IF ( PV_COD_AFI_PACIENTE IS NULL AND PV_COD_DIAGNOSTICO IS NULL ) = FALSE THEN
                  SELECT
                     COUNT(*)
                  INTO V_SOLBEN_CASE2_COUNT
                  FROM
                     PFC_SOLBEN SB
                  WHERE
                     SB.COD_DIAGNOSTICO = PV_COD_DIAGNOSTICO
                     AND SB.COD_AFI_PACIENTE  = PV_COD_AFI_PACIENTE
                     AND SB.FEC_RECETA        = V_FECHA_RECETA
                     AND SB.IND_VALIDA        = PCK_PFC_CONSTANTE.PN_IND_VALIDA_ACTIVO
                     AND ( PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_FARMACIA_COMPLEJA
                           OR PV_TIPO_SCG_SOLBEN    = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA )
                     AND V_RANGO_TIEMPO > V_VALOR;

                  IF V_SOLBEN_CASE2_COUNT > 0 THEN
                     PN_OUT_COD_RESULTADO   := 2;
                     PV_OUT_MSG_RESULTADO   := 'YA EXISTE REGISTRADA OTRA SOLICITUD PRELIMINAR PARA LA LLAVE: FECHA RECETA, CÓDIGO DE DIAGNÓSTICO,CÓDIGO DE PACIENTE EN UN RANGO DE TIEMPO '
                                             || V_RANGO_TIEMPO
                                             || ' DÍAS';
                  ELSE
                     SELECT
                        COUNT(*)
                     INTO V_SOLBEN_CASE3_COUNT
                     FROM
                        PFC_SOLBEN SB
                     WHERE
                        SB.COD_DIAGNOSTICO = PV_COD_DIAGNOSTICO
                        AND SB.COD_AFI_PACIENTE  = PV_COD_AFI_PACIENTE
                        AND SB.IND_VALIDA        = PCK_PFC_CONSTANTE.PN_IND_VALIDA_ACTIVO
                        AND SB.TIPO_SCG_SOLBEN   = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION
                        AND V_RANGO_TIEMPO > V_VALOR;

                     IF V_SOLBEN_CASE3_COUNT > 0 THEN
                        PN_OUT_COD_RESULTADO   := 3;
                        PV_OUT_MSG_RESULTADO   := 'YA EXISTE REGISTRADA OTRA SOLICITUD PRELIMINAR PARA LA LLAVE: CÓDIGO DE DIAGNÓSTICO, CÓDIGO AFILIADO DE PACIENTE EN UN RANGO DE TIEMPO '
                                                || V_RANGO_TIEMPO
                                                || ' DÍAS';
                     ELSE
                        SELECT
                           SQ_PFC_COD_SOLBEN.NEXTVAL
                        INTO V_COD_SCG_SEQ
                        FROM
                           DUAL; /* SECUENCIADOR DE ID SOLBEN*/                            
                     /*INSERTAR REGISTRO EN LA TABLA SOLBEN*/

                        INSERT INTO PFC_SOLBEN (
                           COD_SCG,
                           COD_SCG_SOLBEN,
                           COD_CLINICA,
                           COD_AFI_PACIENTE,
                           EDAD_PACIENTE,
                           DES_CONTRATANTE,
                           DES_PLAN,
                           COD_DIAGNOSTICO,
                           CMP_MEDICO,
                           FEC_QUIMIO,
                           FEC_HOSP_INICIO,
                           FEC_HOSP_FIN,
                           TIPO_SCG_SOLBEN,
                           ESTADO_SCG,
                           DESC_MEDICAMENTO,
                           DESC_ESQUEMA,
                           TOTAL_PRESUPUESTO,
                           DESC_PROCEDIMIENTO,
                           PERSON_CONTACTO,
                           OBS_CLINICA,
                           IND_VALIDA,
                           FEC_SCG_SOLBEN,
                           HORA_SCG_SOLBEN,
                           TX_DATO_ADIC1,
                           TX_DATO_ADIC2,
                           TX_DATO_ADIC3,
                           FEC_RECETA,
                           MEDICO_TRATANTE_PRESCRIPTOR,
                           FEC_AFILIACION,
                           COD_GRP_DIAG,
                           P_TIPO_MEDICO
                        ) VALUES (
                           V_COD_SCG_SEQ,
                           PV_COD_SCG_SOLBEN,
                           PV_COD_CLINICA,
                           PV_COD_AFI_PACIENTE,
                           PN_EDAD_PACIENTE,
                           PV_DES_CONTRATANTE,
                           PV_DES_PLAN,
                           PV_COD_DIAGNOSTICO,
                           PV_CMP_MEDICO,
                           V_FECHA_QUIMIO,/*CAMBIAR VARIABLE NO OBLIG*/
                           V_FECHA_HOSP_INI,/*CAMBIAR VARIABLE NO OBLIG*/
                           V_FECHA_HOSP_FIN,/*CAMBIAR VARIABLE NO OBLIG*/
                           PV_TIPO_SCG_SOLBEN,
                           PV_ESTADO_SCG,
                           PV_DESC_MEDICAMENTO,
                           PV_DESC_ESQUEMA,
                           PN_TOTAL_PRESUPUESTO,
                           PV_DESC_PROCEDIMIENTO,
                           PV_PERSON_CONTACTO,
                           PV_OBS_CLINICA,
                           PN_IND_VALIDA,
                           V_FECHA_SCG_SOLBEN,/*CAMBIAR VARIABLE SI OBLIG*/
                           PV_HORA_SCG_SOLBEN,
                           PV_TX_DATO_ADIC1,
                           PV_TX_DATO_ADIC2,
                           PV_TX_DATO_ADIC3,
                           V_FECHA_RECETA,/*CAMBIAR VARIABLE NO OBLIG*/
                           PV_MEDICO_TRATANTE_PRESCRIPTOR,
                           V_FECHA_AFILIACION,/*CAMBIAR VARIABLE SI OBLIG*/
                           PV_COD_GRP_DIAGNOSTICO,
                           V_TIPO_MEDICO
                        );

                        SELECT
                           SQ_PFC_SOL_PRE_COD_SOL_PRE.NEXTVAL
                        INTO V_COD_SOL_PRE_SEQ
                        FROM
                           DUAL; /* SECUENCIADOR DE ID PRELIMINAR*/

                        INSERT INTO PFC_SOLICITUD_PRELIMINAR (
                           COD_SOL_PRE,
                           P_ESTADO_SOL_PRE,
                           COD_SCG,
                           FEC_SOL_PRE
                        ) VALUES (
                           V_COD_SOL_PRE_SEQ,
                           PCK_PFC_CONSTANTE.PN_P_ESTADO_PENDIENTE, /* P_ESTADO_SOL_PRE 'PENDIENTE'*/
                           V_COD_SCG_SEQ,
                           V_FECHA_HORA_ACTUAL
                        );

                        SELECT
                           TO_CHAR(
                              CAST(FEC_SOL_PRE AS DATE),
                              'DD/MM/YYYY'
                           ) AS FECHA,
                           TO_CHAR(
                              CAST(FEC_SOL_PRE AS DATE),
                              'HH24:MI:SS'
                           ) AS HORA
                        INTO
                           V_FEC_SOL_PRE,
                           V_HORA_SOL_PRE
                        FROM
                           PFC_SOLICITUD_PRELIMINAR SP
                        WHERE
                           SP.COD_SCG = V_COD_SCG_SEQ;

                        PV_OUT_COD_SCG_SOLBEN   := PV_COD_SCG_SOLBEN;
                        PN_OUT_COD_SOL_PRE      := V_COD_SOL_PRE_SEQ;
                        PV_OUT_FEC_SOL_PRE      := V_FEC_SOL_PRE;
                        PV_OUT_HORA_SOL_PRE     := V_HORA_SOL_PRE;
                        PN_OUT_COD_RESULTADO    := 0;
                        PV_OUT_MSG_RESULTADO    := 'INSERCIÓN EXITOSA: SE REGISTRÓ LA SOLICITUD PRELIMINAR CORRECTAMENTE';
                     END IF;

                  END IF;

               ELSE        
               /*SALIDA CASO 4*/
                  PN_OUT_COD_RESULTADO   := 4;
                  PV_OUT_MSG_RESULTADO   := 'LA PERSONA NO ESTÁ REGISTRADO COMO PACIENTE EN EL ONCOSYS';
               END IF;

            ELSE
               SELECT
                  SQ_PFC_COD_SOLBEN.NEXTVAL
               INTO V_COD_SCG_SEQ
               FROM
                  DUAL; /* SECUENCIADOR DE ID SOLBEN*/                            
                     /*INSERTAR REGISTRO EN LA TABLA SOLBEN*/

               INSERT INTO PFC_SOLBEN (
                  COD_SCG,
                  COD_SCG_SOLBEN,
                  COD_CLINICA,
                  COD_AFI_PACIENTE,
                  EDAD_PACIENTE,
                  DES_CONTRATANTE,
                  DES_PLAN,
                  COD_DIAGNOSTICO,
                  CMP_MEDICO,
                  FEC_QUIMIO,
                  FEC_HOSP_INICIO,
                  FEC_HOSP_FIN,
                  TIPO_SCG_SOLBEN,
                  ESTADO_SCG,
                  DESC_MEDICAMENTO,
                  DESC_ESQUEMA,
                  TOTAL_PRESUPUESTO,
                  DESC_PROCEDIMIENTO,
                  PERSON_CONTACTO,
                  OBS_CLINICA,
                  IND_VALIDA,
                  FEC_SCG_SOLBEN,
                  HORA_SCG_SOLBEN,
                  TX_DATO_ADIC1,
                  TX_DATO_ADIC2,
                  TX_DATO_ADIC3,
                  FEC_RECETA,
                  MEDICO_TRATANTE_PRESCRIPTOR,
                  FEC_AFILIACION,
                  COD_GRP_DIAG,
                  P_TIPO_MEDICO
               ) VALUES (
                  V_COD_SCG_SEQ,
                  PV_COD_SCG_SOLBEN,
                  PV_COD_CLINICA,
                  PV_COD_AFI_PACIENTE,
                  PN_EDAD_PACIENTE,
                  PV_DES_CONTRATANTE,
                  PV_DES_PLAN,
                  PV_COD_DIAGNOSTICO,
                  PV_CMP_MEDICO,
                  V_FECHA_QUIMIO,/*CAMBIAR VARIABLE NO OBLIG*/
                  V_FECHA_HOSP_INI,/*CAMBIAR VARIABLE NO OBLIG*/
                  V_FECHA_HOSP_FIN,/*CAMBIAR VARIABLE NO OBLIG*/
                  PV_TIPO_SCG_SOLBEN,
                  PV_ESTADO_SCG,
                  PV_DESC_MEDICAMENTO,
                  PV_DESC_ESQUEMA,
                  PN_TOTAL_PRESUPUESTO,
                  PV_DESC_PROCEDIMIENTO,
                  PV_PERSON_CONTACTO,
                  PV_OBS_CLINICA,
                  PN_IND_VALIDA,
                  V_FECHA_SCG_SOLBEN,/*CAMBIAR VARIABLE SI OBLIG*/
                  PV_HORA_SCG_SOLBEN,
                  PV_TX_DATO_ADIC1,
                  PV_TX_DATO_ADIC2,
                  PV_TX_DATO_ADIC3,
                  V_FECHA_RECETA,/*CAMBIAR VARIABLE NO OBLIG*/
                  PV_MEDICO_TRATANTE_PRESCRIPTOR,
                  V_FECHA_AFILIACION,/*CAMBIAR VARIABLE SI OBLIG*/
                  PV_COD_GRP_DIAGNOSTICO,
                  V_TIPO_MEDICO
               );

               SELECT
                  SQ_PFC_SOL_PRE_COD_SOL_PRE.NEXTVAL
               INTO V_COD_SOL_PRE_SEQ
               FROM
                  DUAL; /* SECUENCIADOR DE ID PRELIMINAR*/

               INSERT INTO PFC_SOLICITUD_PRELIMINAR (
                  COD_SOL_PRE,
                  P_ESTADO_SOL_PRE,
                  COD_SCG,
                  FEC_SOL_PRE
               ) VALUES (
                  V_COD_SOL_PRE_SEQ,
                  PCK_PFC_CONSTANTE.PN_P_ESTADO_PENDIENTE, /* P_ESTADO_SOL_PRE 'PENDIENTE'*/
                  V_COD_SCG_SEQ,
                  V_FECHA_HORA_ACTUAL
               );

               SELECT
                  TO_CHAR(
                     CAST(FEC_SOL_PRE AS DATE),
                     'DD/MM/YYYY'
                  ) AS FECHA,
                  TO_CHAR(
                     CAST(FEC_SOL_PRE AS DATE),
                     'HH24:MI:SS'
                  ) AS HORA
               INTO
                  V_FEC_SOL_PRE,
                  V_HORA_SOL_PRE
               FROM
                  PFC_SOLICITUD_PRELIMINAR SP
               WHERE
                  SP.COD_SCG = V_COD_SCG_SEQ;

               PV_OUT_COD_SCG_SOLBEN   := PV_COD_SCG_SOLBEN;
               PN_OUT_COD_SOL_PRE      := V_COD_SOL_PRE_SEQ;
               PV_OUT_FEC_SOL_PRE      := V_FEC_SOL_PRE;
               PV_OUT_HORA_SOL_PRE     := V_HORA_SOL_PRE;
               PN_OUT_COD_RESULTADO    := 0;
               PV_OUT_MSG_RESULTADO    := 'INSERCIÓN EXITOSA: SE REGISTRÓ LA SOLICITUD PRELIMINAR CORRECTAMENTE';
            END IF;
         WHEN V_COD_SCG_SOLBEN_COUNT > 0 THEN
      
        /*SALIDA CASO 1*/
            IF V_COD_SCG_SOLBEN_COUNT <> 0 AND ( PN_IND_VALIDA = 1 OR PN_IND_VALIDA = 0 ) THEN
               PN_OUT_COD_RESULTADO   := 1;
               PV_OUT_MSG_RESULTADO   := 'YA EXISTE REGISTRADA OTRA SOLICITUD PRELIMINAR PARA EL MISMO NÚMERO DE SOLICITUD SOLBEN';
            END IF;
      END CASE;

      COMMIT;
   EXCEPTION
      WHEN V_EXC_CAMPO_VALIDA THEN
         PN_OUT_COD_RESULTADO   := 6;
         PV_OUT_MSG_RESULTADO   := V_MENSAJE;
      WHEN V_EXC_FEC_ACTUAL THEN
         PN_OUT_COD_RESULTADO   := -2;
         PV_OUT_MSG_RESULTADO   := '[[SP_PFC_SI_SCG_SOLBEN_PREL]] ' || V_MENSAJE;
      WHEN OTHERS THEN
         PN_OUT_COD_RESULTADO   := -1;
         PV_OUT_MSG_RESULTADO   := '[[SP_PFC_SI_SCG_SOLBEN_PREL]] '
                                 || ' : '
                                 || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE
                                 || ' : '
                                 || TO_CHAR(SQLCODE)
                                 || ' : '
                                 || SQLERRM;

         ROLLBACK;
   END SP_PFC_SI_SCG_SOLBEN_PREL;

   PROCEDURE SP_PFC_U_SOLIC_PRELIMINAR (
      PN_COD_SOL_PRE        IN                    NUMBER,
      PN_P_ESTADO_SOL_PRE   IN                    NUMBER,
      PV_FECHA_ACTUAL       IN                    VARCHAR2,
      PN_USUARIO            IN                    NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      UPDATE PFC_SOLICITUD_PRELIMINAR SP
      SET
         SP.P_ESTADO_SOL_PRE = PN_P_ESTADO_SOL_PRE,
         SP.FECHA_MODIFICACION = TO_DATE(
            PV_FECHA_ACTUAL,
            'DD/MM/YYYY'
         ),
         SP.USUARIO_MODIFICACION = PN_USUARIO
      WHERE
         SP.COD_SOL_PRE = PN_COD_SOL_PRE;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Actualizacion exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK;
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_U_SOLIC_PRELIMINAR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_U_SOLIC_PRELIMINAR;

END PCK_PFC_BANDEJA_PRELIMINAR;

/
--------------------------------------------------------
--  DDL for Package Body PCK_PFC_CARGA_CONTROL_GASTO
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "ONTPFC"."PCK_PFC_CARGA_CONTROL_GASTO" AS

    /* AUTOR     : PEDRO OLIVAS */
    /* CREADO    : 12/06/2019 */
    /* PROPOSITO : LISTA LOS REGISTROS DE LA TABLA PFC_HIST_CONTROL_GASTO */
    /* VERSION   : 1.0 */
    PROCEDURE SP_PFC_S_HIST_CONTROL_GASTO_PG (
        PV_FECHA_INICIO            IN DATE,
        PV_FECHA_FIN               IN DATE,
        PN_INDEX                   IN NUMBER,
      PN_LONGITUD                IN NUMBER,      
        TC_CARGA_ARCHIVO           OUT PCK_PFC_CONSTANTE.TYPCUR,
        PN_COD_RESULTADO           OUT NUMBER,
        PV_MSG_RESULTADO           OUT VARCHAR2
    ) AS
        V_INICIO                   NUMBER;
        V_FIN                      NUMBER;
    BEGIN
      V_INICIO           := PN_INDEX + 1;
      V_FIN              := PN_LONGITUD + PN_INDEX;

      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'Valor Inicial';

      OPEN TC_CARGA_ARCHIVO FOR 
      SELECT * FROM 
           (  
               SELECT 
                    ROW_NUMBER() OVER(
                         ORDER BY
                            CG.FECHA_HORA_CARGA DESC
                    ) RN,
                            CG.COD_HIST_CONTROL_GASTO,
                  to_char(CG.FECHA_HORA_CARGA, 'DD/MM/YYYY') as FECHA_HORA_CARGA,
                            to_char(CG.FECHA_HORA_CARGA,'HH24:MI:SS')  HORA_CARGA,
                            CG.RESPONSABLE_CARGA,
                            CG.P_ESTADO_CARGA   ESTADO_CARGA,
                   (SELECT NVL(P.NOMBRE,'') FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = CG.P_ESTADO_CARGA) AS DES_ESTADO_CARGA,
                   CG.REGISTRO_TOTAL,
                   CG.REGISTRO_CARGADO,
                   CG.REGISTRO_ERROR,
                   CG.VER_LOG,
                   CG.DESCARGA,
                   CG.COD_ARCHIVO_LOG,
                   CG.COD_ARCHIVO_DESCARGA
              FROM PFC_HIST_CONTROL_GASTO CG
              WHERE
                   (TRUNC(CG.FECHA_HORA_CARGA) >= PV_FECHA_INICIO  AND  TRUNC(CG.FECHA_HORA_CARGA) <= PV_FECHA_FIN)
              ORDER BY CG.FECHA_HORA_CARGA DESC
           )
           WHERE
           (PN_LONGITUD=0)  OR    
           (RN BETWEEN V_INICIO AND V_FIN)  
           ORDER BY RN;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'LISTA TOTAL!';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_HIST_CONTROL_GASTO_PAG]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_HIST_CONTROL_GASTO_PG;

    /* AUTOR     : PEDRO OLIVAS */
    /* CREADO    : 12/06/2019 */
    /* PROPOSITO : INSERTA LOS REGISTROS EN LA TABLA PFC_HIST_CONTROL_GASTO */
    /* VERSION   : 1.0 */
    PROCEDURE SP_PFC_I_CONTROL_GASTO_CAB (
            PD_FECHA_HORA_CARGA                IN   DATE   , 
            PN_RESPONSABLE_CARGA               IN   NUMBER , 
            PN_P_ESTADO_CARGA                  IN   NUMBER , 
            PN_REGISTRO_TOTAL                  IN   NUMBER , 
            PN_REGISTRO_CARGADO                IN   NUMBER , 
            PN_REGISTRO_ERROR                  IN   NUMBER , 
            PN_VER_LOG                         IN   NUMBER , 
            PN_COD_ARCHIVO_LOG                 IN   NUMBER , 
            PN_DESCARGA                        IN   NUMBER , 
            PN_COD_ARCHIVO_DESCARGA            IN   NUMBER ,
                        PD_FECHA_CREACION                  IN   DATE,
            PN_USUARIO_CREACION                IN   NUMBER,  
            PD_FECHA_MODIFICACION              IN   DATE,          
            PN_USUARIO_MODIFICACION            IN   NUMBER,                         
            PN_COD_HIST_CONTROL_GASTO      OUT  NUMBER , 
                        PN_COD_RESULTADO             OUT  NUMBER,
                        PV_MSG_RESULTADO             OUT  VARCHAR2
   ) AS

      C_EST_EN_PROCESO  CONSTANT NUMBER(4) := 252;
      C_EST_CON_ERROR   CONSTANT NUMBER(4) := 253;
      C_EST_CARGADO     CONSTANT NUMBER(4) := 254;

      V_COD_HIST_CONTROL_GASTO NUMBER;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';

        SELECT
           NVL(MAX(COD_HIST_CONTROL_GASTO),0)
           INTO V_COD_HIST_CONTROL_GASTO
         FROM
        PFC_HIST_CONTROL_GASTO;

         PN_COD_HIST_CONTROL_GASTO     := V_COD_HIST_CONTROL_GASTO + 1;

          INSERT INTO PFC_HIST_CONTROL_GASTO
          (
            COD_HIST_CONTROL_GASTO      ,
            FECHA_HORA_CARGA                ,
            RESPONSABLE_CARGA               ,
            P_ESTADO_CARGA                  ,
            REGISTRO_TOTAL                  ,
            REGISTRO_CARGADO                ,
            REGISTRO_ERROR                  ,
            VER_LOG                         ,
            COD_ARCHIVO_LOG                 ,
            DESCARGA                        ,
            COD_ARCHIVO_DESCARGA            ,
            FECHA_CREACION                  ,
            USUARIO_CREACION                ,
            FECHA_MODIFICACION              ,
            USUARIO_MODIFICACION
          )
          VALUES
          (          
            PN_COD_HIST_CONTROL_GASTO      ,
            SYSDATE                            ,       
            PN_RESPONSABLE_CARGA               ,
            C_EST_EN_PROCESO                   ,
            PN_REGISTRO_TOTAL                  ,
            PN_REGISTRO_CARGADO                ,
            PN_REGISTRO_ERROR                  ,
            PN_VER_LOG                         ,
            PN_COD_ARCHIVO_LOG                 ,
            PN_DESCARGA                        ,
            PN_COD_ARCHIVO_DESCARGA            ,
            SYSDATE                            ,         
            PN_USUARIO_CREACION                ,
            SYSDATE                            ,      
            PN_USUARIO_MODIFICACION
          );

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Registro Exitoso!';

        COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_I_CONTROL_GASTO_CAB]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_CONTROL_GASTO_CAB;

    /* AUTOR     : PEDRO OLIVAS */
    /* CREADO    : 12/06/2019 */
    /* PROPOSITO : INSERTA LOS REGISTROS EN LA TABLA PFC_CONSUMO_MEDICAMENTO */
    /* VERSION   : 1.0 */
    PROCEDURE SP_PFC_I_CONSUMOS (

            PD_FECHA_HORA_CARGA                IN   DATE   , 
            PN_RESPONSABLE_CARGA               IN   NUMBER , 
            PN_P_ESTADO_CARGA                  IN   NUMBER , 
            PN_REGISTRO_TOTAL                  IN   NUMBER , 
            PN_REGISTRO_CARGADO                IN   NUMBER , 
            PN_REGISTRO_ERROR                  IN   NUMBER , 
            PN_VER_LOG                         IN   NUMBER , 
            PN_COD_ARCHIVO_LOG                 IN   NUMBER , 
            PN_DESCARGA                        IN   NUMBER , 
            PN_COD_ARCHIVO_DESCARGA            IN   NUMBER ,   
            -------------------------------------------------
            PN_COD_HIST_CONTROL_GASTO           IN NUMBER ,
            PV_COD_MAC                          IN VARCHAR2 ,
            PV_TIPO_ENCUENTRO                   IN VARCHAR2 , 
            PN_ENCUENTRO                        IN NUMBER   ,
            PV_CODIGO_SAP                       IN VARCHAR2 ,
            PV_DESCRIP_ACTIVO_MOLECULA          IN VARCHAR2 ,
            PV_DESCRIP_PRESENT_GENERICO         IN VARCHAR2 ,
            PV_PRESTACION                       IN VARCHAR2 ,
            PV_COD_AFILIADO                     IN VARCHAR2 ,
            PV_COD_PACIENTE                     IN VARCHAR2 ,
            PV_NOMBRE_PACIENTE                  IN VARCHAR2 ,
            PV_CLINICA                          IN VARCHAR2 ,
            PD_FECHA_CONSUMO                    IN DATE     ,          
            PV_DESCRIP_GRP_DIAG                 IN VARCHAR2 ,
            PV_COD_DIAGNOSTICO                  IN VARCHAR2 ,
            PV_DESCRIP_DIAGNOSTICO              IN VARCHAR2 ,
            PV_LINEA_TRATAMIENTO                IN NUMBER   ,
            PV_MEDICO_TRATANTE                  IN VARCHAR2 ,
            PV_PLAN                             IN VARCHAR2 ,
            PN_CANTIDAD_CONSUMO                 IN NUMBER   ,
            PN_MONTO_CONSUMO                    IN NUMBER   ,
            --------------------------------------------------------
            PD_FECHA_CREACION                           IN DATE,
            PN_USUARIO_CREACION                         IN NUMBER,  
            PD_FECHA_MODIFICACION                       IN DATE,          
            PN_USUARIO_MODIFICACION                     IN NUMBER,                         
            --------------------------------------------------------
            PN_COD_CONSUMO_MEDICAMENTO                          OUT   NUMBER ,        
            PN_COD_RESULTADO                        OUT   NUMBER,
            PV_MSG_RESULTADO                        OUT   VARCHAR2                                            
   ) AS           

      C_EST_EN_PROCESO  CONSTANT NUMBER(4) := 252;
      C_EST_CON_ERROR   CONSTANT NUMBER(4) := 253;
      C_EST_CARGADO     CONSTANT NUMBER(4) := 254;   
   V_COD_CONSUMO_MEDICAMENTO NUMBER;
   BEGIN                                                                                                   
      PN_COD_RESULTADO   := -1;                                                                            
      PV_MSG_RESULTADO   := 'valor inicial';                                                               

        SELECT
            NVL(MAX(COD_CONSUMO_MEDICAMENTO),0)
            INTO V_COD_CONSUMO_MEDICAMENTO
        FROM
        PFC_CONSUMO_MEDICAMENTO;

         PN_COD_CONSUMO_MEDICAMENTO     := V_COD_CONSUMO_MEDICAMENTO + 1;                                       


          INSERT INTO  PFC_CONSUMO_MEDICAMENTO                                                             
          (                                                                                                
              COD_HIST_CONTROL_GASTO    ,                                        
              COD_MAC                   ,                              
              TIPO_ENCUENTRO            ,                                 
              ENCUENTRO                 ,                               
              CODIGO_SAP                ,                                 
              DESCRIP_ACTIVO_MOLECULA   ,                                  
              DESCRIP_PRESENT_GENERICO  ,                                   
              PRESTACION                ,                                  
              COD_AFILIADO              ,                                  
              COD_PACIENTE              ,                                  
              NOMBRE_PACIENTE           ,                                  
              CLINICA                   ,                                
              FECHA_CONSUMO             ,                                            
              DESCRIP_GRP_DIAG          ,                                  
              COD_DIAGNOSTICO           ,                                    
              DESCRIP_DIAGNOSTICO       ,                                    
              LINEA_TRATAMIENTO         ,                                   
              MEDICO_TRATANTE           ,                                 
              PLAN                      ,                                   
              CANTIDAD_CONSUMO          ,                                       
              MONTO_CONSUMO             ,                                      
              COD_CONSUMO_MEDICAMENTO                                       
          )                                                                                                
          VALUES                                                                                           
          (                                                                                                
              PN_COD_HIST_CONTROL_GASTO    ,                                                               
              PV_COD_MAC                   ,                                                               
              PV_TIPO_ENCUENTRO            ,                                                               
              PN_ENCUENTRO                 ,                                                               
              PV_CODIGO_SAP                ,                                                               
              PV_DESCRIP_ACTIVO_MOLECULA   ,                                                               
              PV_DESCRIP_PRESENT_GENERICO  ,                                                               
              PV_PRESTACION                ,                                                               
              PV_COD_AFILIADO              ,                                                               
              PV_COD_PACIENTE              ,                                                               
              PV_NOMBRE_PACIENTE           ,                                                               
              PV_CLINICA                   ,                                                               
              PD_FECHA_CONSUMO             ,                                                               
              PV_DESCRIP_GRP_DIAG          ,                                                               
              PV_COD_DIAGNOSTICO           ,                                                               
              PV_DESCRIP_DIAGNOSTICO       ,                                                               
              PV_LINEA_TRATAMIENTO         ,                                                               
              PV_MEDICO_TRATANTE           ,                                                               
              PV_PLAN                      ,                                                               
              nvl(PN_CANTIDAD_CONSUMO,0)   ,                                                               
              PN_MONTO_CONSUMO             ,                                                               
              PN_COD_CONSUMO_MEDICAMENTO                                                                   
          );                                                                                               

         ---------------------------------------------------------------------------------------           

            UPDATE PFC_HIST_CONTROL_GASTO                                                                  
            SET                                                                                            
              RESPONSABLE_CARGA                 = PN_RESPONSABLE_CARGA    ,           
              P_ESTADO_CARGA                    = C_EST_EN_PROCESO        ,           
              REGISTRO_CARGADO                  = REGISTRO_CARGADO  + 1   ,                         
              USUARIO_CREACION                  = PN_USUARIO_CREACION     ,          
              FECHA_MODIFICACION                = SYSDATE,                
              USUARIO_MODIFICACION              = PN_USUARIO_MODIFICACION           
            WHERE                                                                                          
            COD_HIST_CONTROL_GASTO  = PN_COD_HIST_CONTROL_GASTO;                  
         ---------------------------------------------------------------------------------------           

         PN_COD_RESULTADO   := 0;                                                                          
         PV_MSG_RESULTADO   := 'Registro Exitoso!';                                                        

        COMMIT;                                                                                            

   EXCEPTION                                                                                               
      WHEN OTHERS THEN                                                                                     
         PN_COD_RESULTADO   := -1;                                                                         
         PV_MSG_RESULTADO   := '[[SP_PFC_I_CONSUMOS]] '                                                    
                             || TO_CHAR(SQLCODE)                                                           
                             || ' : '                                                                      
                             || SQLERRM;                                                                   
         ROLLBACK;                                                                                         

        UPDATE PFC_HIST_CONTROL_GASTO                                                                      
        SET                          
            P_ESTADO_CARGA    = C_EST_CON_ERROR,
            REGISTRO_ERROR    = PN_REGISTRO_ERROR + 1                                                       
        WHERE                                                                                              
        COD_HIST_CONTROL_GASTO  = PN_COD_HIST_CONTROL_GASTO;                      

        COMMIT;                                                                                            

   END SP_PFC_I_CONSUMOS;  
   
    /* AUTOR     : PEDRO OLIVAS */
    /* CREADO    : 12/06/2019 */
    /* PROPOSITO : ACTUALIZA LOS REGISTROS EN LA TABLA PFC_HIST_CONTROL_GASTO */
    /* VERSION   : 1.0 */
    PROCEDURE SP_PFC_U_CONTROL_GASTO_CAB (
                        PN_COD_HIST_CONTROL_GASTO      IN   NUMBER , 
            PD_FECHA_HORA_CARGA                IN   DATE   , 
            PN_RESPONSABLE_CARGA               IN   NUMBER , 
            PN_P_ESTADO_CARGA                  IN   NUMBER , 
            PN_REGISTRO_TOTAL                  IN   NUMBER , 
            PN_REGISTRO_CARGADO                IN   NUMBER , 
            PN_REGISTRO_ERROR                  IN   NUMBER , 
            PN_VER_LOG                         IN   NUMBER , 
            PN_COD_ARCHIVO_LOG                 IN   NUMBER , 
            PN_DESCARGA                        IN   NUMBER , 
            PN_COD_ARCHIVO_DESCARGA            IN   NUMBER ,
            ------------------------------------------------ 

                        PD_FECHA_CREACION                           IN DATE,
            PN_USUARIO_CREACION                         IN NUMBER,  
            PD_FECHA_MODIFICACION                       IN DATE,          
            PN_USUARIO_MODIFICACION                     IN NUMBER,                         

            --------------------------------------------------------                                      
            PN_COD_RESULTADO                  OUT   NUMBER,
            PV_MSG_RESULTADO                  OUT   VARCHAR2
   ) AS

      C_EST_EN_PROCESO  CONSTANT NUMBER(4) := 252;
      C_EST_CON_ERROR   CONSTANT NUMBER(4) := 253;
      C_EST_CARGADO     CONSTANT NUMBER(4) := 254;

      V_COD_HIST_CONTROL_GASTO NUMBER;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';

         ---------------------------------------------------------------------------------------

          UPDATE PFC_HIST_CONTROL_GASTO                                                                  
            SET                                                                                            
              FECHA_HORA_CARGA                  = DECODE(PD_FECHA_HORA_CARGA    ,null , FECHA_HORA_CARGA      , PD_FECHA_HORA_CARGA    ),                      
              RESPONSABLE_CARGA                 = DECODE(PN_RESPONSABLE_CARGA   ,null , RESPONSABLE_CARGA     , PN_RESPONSABLE_CARGA   ),              
              P_ESTADO_CARGA                    = DECODE(PN_P_ESTADO_CARGA      ,null , P_ESTADO_CARGA        , PN_P_ESTADO_CARGA      ),              
              REGISTRO_TOTAL                    = DECODE(PN_REGISTRO_TOTAL      ,null , REGISTRO_TOTAL        , PN_REGISTRO_TOTAL      ),           
              REGISTRO_CARGADO                  = DECODE(PN_REGISTRO_CARGADO    ,null , REGISTRO_CARGADO      , PN_REGISTRO_CARGADO    ),           
              REGISTRO_ERROR                    = DECODE(PN_REGISTRO_ERROR      ,null , REGISTRO_ERROR        , PN_REGISTRO_ERROR      ),            
              VER_LOG                           = DECODE(PN_VER_LOG             ,null , VER_LOG               , PN_VER_LOG             ),               
              COD_ARCHIVO_LOG                   = DECODE(PN_COD_ARCHIVO_LOG     ,null , COD_ARCHIVO_LOG       , PN_COD_ARCHIVO_LOG     ),              
              DESCARGA                          = DECODE(PN_DESCARGA            ,null , DESCARGA              , PN_DESCARGA            ),               
              COD_ARCHIVO_DESCARGA              = DECODE(PN_COD_ARCHIVO_DESCARGA,null , COD_ARCHIVO_DESCARGA  , PN_COD_ARCHIVO_DESCARGA),              
              FECHA_MODIFICACION                = SYSDATE, 
              USUARIO_MODIFICACION                = DECODE(PN_USUARIO_MODIFICACION,null , USUARIO_MODIFICACION  , PN_USUARIO_MODIFICACION)            
            WHERE                                                                                          
            COD_HIST_CONTROL_GASTO  = PN_COD_HIST_CONTROL_GASTO;   

         ---------------------------------------------------------------------------------------

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Registro Exitoso!';


        COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_U_CONTROL_GASTO_CAB]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_U_CONTROL_GASTO_CAB;   
    
    /* AUTOR     : PEDRO OLIVAS */
    /* CREADO    : 12/06/2019 */
    /* PROPOSITO : VALIDA QUE UN CONSUMO TENGA UNA LINEA DE TRATAMIENTO */
    /* VERSION   : 1.0 */
    PROCEDURE SP_PFC_S_VALIDAR_TRATAMIENTO (
        PV_COD_AFI_PACIENTE                         IN VARCHAR2,
        PV_COD_DIAGNOSTICO                          IN VARCHAR2,  
        PN_COD_MAC                                  IN VARCHAR2, 
        PN_LINEA_TRA                                IN NUMBER,
        PN_TOTAL                                    OUT NUMBER
    ) AS
        V_COD_SOL_EVA                               NUMBER;
        V_RESULT                                    NUMBER;
    BEGIN

        BEGIN
            SELECT T.COD_SOL_EVA, T.NRO_LINEA_TRA
            INTO V_COD_SOL_EVA, V_RESULT
            FROM (SELECT SE.COD_SOL_EVA, MN.NRO_LINEA_TRA,EV.COD_EVOLUCION
            FROM PFC_SOLBEN s 
            INNER JOIN PFC_SOLICITUD_PRELIMINAR p
            ON s.cod_scg = p.cod_scg
            INNER JOIN PFC_SOLICITUD_EVALUACION SE
            ON SE.COD_SOL_PRE = P.COD_SOL_PRE
            INNER JOIN PFC_MEDICAMENTO_NUEVO MN
            ON MN.COD_SOL_EVA = SE.COD_SOL_EVA
            INNER JOIN PFC_MONITOREO MO
            ON MN.COD_SOL_EVA = MO.COD_SOL_EVA
            INNER JOIN PFC_EVOLUCION EV
            ON MO.COD_MONITOREO = EV.COD_MONITOREO
            WHERE s.cod_afi_paciente = PV_COD_AFI_PACIENTE
            AND s.cod_diagnostico = PV_COD_DIAGNOSTICO
            AND p.cod_mac = PN_COD_MAC
            ORDER BY EV.COD_EVOLUCION DESC) T
            WHERE ROWNUM = 1;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            V_COD_SOL_EVA := NULL;
            V_RESULT      := NULL;
        WHEN OTHERS THEN
          V_COD_SOL_EVA := NULL;
          V_RESULT      := NULL;
        END;

        IF V_COD_SOL_EVA IS NULL THEN
            PN_TOTAL :=0;
        ELSE
            IF V_RESULT = PN_LINEA_TRA THEN
                PN_TOTAL := 1;
            ELSE
                PN_TOTAL := 0;
            END IF;
        END IF;

         EXCEPTION
          WHEN OTHERS THEN
             PN_TOTAL := -1;

    END SP_PFC_S_VALIDAR_TRATAMIENTO;
    
    /* AUTOR     : PEDRO OLIVAS */
    /* CREADO    : 12/06/2019 */
    /* PROPOSITO : ACTUALIZA LOS CAMPOS DE CONSUMO DE LA TABLA PFC_EVOLUCION DEL MODULO MONITOREO */
    /* VERSION   : 1.0 */
    PROCEDURE SP_PFC_U_ACTUALIZAR_CONSUMO (
      PV_COD_AFI_PACIENTE                         IN VARCHAR2,
    PV_COD_DIAGNOSTICO                          IN VARCHAR2,  
    PN_COD_MAC                                  IN NUMBER, 
    PN_CANT_ULTIMO_CONSUMO            IN NUMBER,
        PN_GASTO_ULTIMO_CONSUMO                     IN NUMBER,
        PD_FEC_ULTIMO_CONSUMO                       IN DATE,
        PN_COD_RESULTADO                            OUT NUMBER,
        PV_MSG_RESULTADO                            OUT VARCHAR2
     ) AS
        V_EVOLUCION                                 NUMBER;
     BEGIN
        PN_COD_RESULTADO := -1;
        PV_MSG_RESULTADO := 'Valor inicial';

       SELECT COD_EVOLUCION INTO V_EVOLUCION FROM (
      SELECT E.COD_EVOLUCION FROM PFC_MONITOREO M
      INNER JOIN PFC_EVOLUCION E
      ON M.COD_MONITOREO = E.COD_MONITOREO
      INNER JOIN PFC_SOLICITUD_EVALUACION SE
      ON SE.COD_SOL_EVA = M.COD_SOL_EVA
      INNER JOIN PFC_SOLICITUD_PRELIMINAR SP
      ON SP.COD_SOL_PRE = SE.COD_SOL_PRE
      INNER JOIN PFC_SOLBEN S
      ON S.COD_SCG = SP.COD_SCG
      WHERE S.COD_AFI_PACIENTE = PV_COD_AFI_PACIENTE
      AND S.COD_DIAGNOSTICO = PV_COD_DIAGNOSTICO
      AND E.COD_MAC = PN_COD_MAC
      ORDER BY E.COD_EVOLUCION DESC
     ) WHERE ROWNUM = 1;

         UPDATE PFC_EVOLUCION
         SET CANT_ULTIMO_CONSUMO = PN_CANT_ULTIMO_CONSUMO,
             GASTO_ULTIMO_CONSUMO = PN_GASTO_ULTIMO_CONSUMO,
             FEC_ULTIMO_CONSUMO = PD_FEC_ULTIMO_CONSUMO
         WHERE COD_EVOLUCION = V_EVOLUCION;

        PN_COD_RESULTADO := 0;
        PV_MSG_RESULTADO := 'Actualización exitosa';

        COMMIT;

        EXCEPTION
          WHEN OTHERS THEN
             PN_COD_RESULTADO   := -1;
             PV_MSG_RESULTADO   := '[[SP_PFC_U_ACTUALIZAR_CONSUMO]] '
                                 || TO_CHAR(SQLCODE)
                                 || ' : '
                                 || SQLERRM;
             ROLLBACK;

   END SP_PFC_U_ACTUALIZAR_CONSUMO;
     
     /* AUTOR     : PEDRO OLIVAS */
     /* CREADO    : 12/06/2019 */
     /* PROPOSITO : VALIDA SI YA EXISTE UN ARCHIVO DE CONSUMO PREVIAMENTE CARGADO EN EL MES DE CARGA */
     /* VERSION   : 1.0 */
   PROCEDURE SP_PFC_S_VALIDAR_CARGA (
    PN_EXISTE_ARCHIVO             OUT NUMBER,
        PV_MENSAJE                                  OUT VARCHAR2
   ) AS
    V_START_DATE                DATE;
    V_FINISH_DATE               DATE;
    V_PARAMETRO                 VARCHAR2(50);
        V_RESULT                                    NUMBER;
        V_DATAP                                     VARCHAR2(2000);
   BEGIN
     SELECT TO_DATE(to_char(TRUNC(SYSDATE,'MM'),'DD/MM/YYYY'),'DD/MM/YYYY') INTO V_START_DATE FROM DUAL;

     SELECT VALOR1 INTO V_PARAMETRO FROM PFC_PARAMETRO WHERE COD_GRUPO = 69 AND COD_PARAMETRO = 315;

     SELECT (V_START_DATE + V_PARAMETRO ) INTO V_FINISH_DATE FROM DUAL;

         SELECT COUNT(*) INTO V_RESULT FROM pfc_hist_control_gasto H   
         WHERE (TRUNC(H.FECHA_HORA_CARGA) >= V_START_DATE AND  TRUNC(H.FECHA_HORA_CARGA) < V_FINISH_DATE)
         AND H.P_ESTADO_CARGA = 254 AND H.REGISTRO_CARGADO > 0;

         IF V_RESULT = 0 THEN
            PN_EXISTE_ARCHIVO := 0;
         ELSE
            PN_EXISTE_ARCHIVO := 1;
         END IF;

         EXCEPTION
      WHEN OTHERS THEN
         PN_EXISTE_ARCHIVO := -1;
         PV_MENSAJE        := '[[SP_PFC_S_VALIDAR_CARGA]] '
                                 || TO_CHAR(SQLCODE)
                                 || ' : '
                                 || SQLERRM;

   END SP_PFC_S_VALIDAR_CARGA;
     
     PROCEDURE SP_PFC_U_ACTUALIZAR_ESTADO AS
        V_COD_HIST     NUMBER;
    BEGIN
    
        BEGIN
            SELECT cod_hist_control_gasto INTO V_COD_HIST FROM (
            SELECT H.cod_hist_control_gasto FROM pfc_hist_control_gasto H
            where H.P_ESTADO_CARGA = 252
            ORDER BY H.cod_hist_control_gasto DESC
            ) WHERE ROWNUM=1;  
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            V_COD_HIST := NULL;
        WHEN OTHERS THEN
          V_COD_HIST := NULL;
        END;
        
        IF V_COD_HIST IS NOT NULL THEN
            UPDATE pfc_hist_control_gasto SET P_ESTADO_CARGA = 253 WHERE COD_HIST_CONTROL_GASTO = V_COD_HIST;
            COMMIT;
        END IF;
                
    END SP_PFC_U_ACTUALIZAR_ESTADO;

END PCK_PFC_CARGA_CONTROL_GASTO;

/
--------------------------------------------------------
--  DDL for Package Body PCK_PFC_CONFIGURACION_SISTEMA
--------------------------------------------------------
CREATE OR REPLACE PACKAGE BODY "ONTPFC"."PCK_PFC_CONFIGURACION_SISTEMA" AS
    /* AUTOR     : DDIAZR*/
    /* CREADO    : 13/05/2019*/
    /* PROPOSITO : REGISTRO DE LA MAC*/

   PROCEDURE SP_PFC_I_REGISTRO_MAC (
      PN_COD_MAC         IN                 NUMBER,
      PV_DESCRIPCION     IN                 VARCHAR2,
      PN_P_TIPO_MAC      IN                 NUMBER,
      PN_P_ESTADO_MAC    IN                 NUMBER,
      PD_FECHA_INSCRIP   IN                 DATE,
      PD_FEC_INICIO      IN                 DATE,
      PD_FEC_FIN         IN                 DATE,
      PV_FEC_CREACION    IN                 VARCHAR2,
      PN_USER_CREACION   IN                 NUMBER,
      PV_FEC_MODIFICA    IN                 VARCHAR2,
      PN_USER_MODIFICA   IN                 NUMBER,
      PN_COD_OUT_MAC     OUT                NUMBER,
      PV_COD_LARGO_MAC   OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      V_COD_MAC NUMBER;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF ( PN_COD_MAC IS NULL ) THEN
         SELECT
            MAX(COD_MAC)
         INTO V_COD_MAC
         FROM
            PFC_MAC;
        
            /*pn_cod_out_mac := sq_pfc_mac_cod_mac.nextval;*/

         PN_COD_OUT_MAC     := V_COD_MAC + 1;
         PV_COD_LARGO_MAC   := LPAD(
            PN_COD_OUT_MAC,
            5,
            '0'
         );
         INSERT INTO PFC_MAC (
            COD_MAC,
            COD_MAC_LARGO,
            DESCRIPCION,
            P_TIPO_MAC,
            FEC_INSCRIPCION,
            FEC_INICIO_VIG,
            FEC_FIN_VIG,
            FEC_CREACION,
            USR_CREACION,
            FEC_MODIFICACION,
            USR_MODIFICACION,
            P_ESTADO_MAC
         ) VALUES (
            PN_COD_OUT_MAC,
            PV_COD_LARGO_MAC,
            PV_DESCRIPCION,
            PN_P_TIPO_MAC,
            PD_FECHA_INSCRIP,
            PD_FEC_INICIO,
            NULL,
            TO_TIMESTAMP(
               PV_FEC_CREACION,
               'DD-MM-YYYY HH24:MI:SS'
            ),
            PN_USER_CREACION,
            NULL,
            NULL,
            PN_P_ESTADO_MAC
         );

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Registro Exitoso!';
      ELSE
         UPDATE PFC_MAC
         SET
            DESCRIPCION = PV_DESCRIPCION,
            P_TIPO_MAC = PN_P_TIPO_MAC,
            P_ESTADO_MAC = PN_P_ESTADO_MAC,
            USR_MODIFICACION = PN_USER_MODIFICA,
            FEC_MODIFICACION = TO_TIMESTAMP(
               PV_FEC_MODIFICA,
               'DD-MM-YYYY HH24:MI:SS'
            )
         WHERE
            COD_MAC = PN_COD_MAC;

         IF ( PD_FEC_FIN IS NOT NULL ) THEN
            UPDATE PFC_MAC
            SET
               FEC_FIN_VIG = PD_FEC_FIN
            WHERE
               COD_MAC = PN_COD_MAC;

         END IF;

         PN_COD_RESULTADO   := 0;
         PN_COD_OUT_MAC     := PN_COD_MAC;
         PV_COD_LARGO_MAC   := LPAD(
            PN_COD_OUT_MAC,
            5,
            '0'
         );
         PV_MSG_RESULTADO   := 'Actualización Exitosa!!';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_i_registro_mac]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_REGISTRO_MAC;

   PROCEDURE SP_PFC_S_CONFIG_CHECKLIST (
      PN_COD_GRP_DIAG    IN                 NUMBER,
      PN_COD_MAC         IN                 NUMBER,
      OP_OBJCURSOR       OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'Valor Inicial';
      OPEN OP_OBJCURSOR FOR SELECT
                               C.COD_MAC,
                               C.COD_GRP_DIAG,
                               C.COD_CHKLIST_INDI         AS COD_INDICADOR,
                               C.COD_CHKLIST_INDI_LARGO   AS COD_INDICADOR_LARGO,
                               DESCRIPCION,
                               P_ESTADO                   AS COD_ESTADO,
                               P.NOMBRE                   AS ESTADO,
                               FECHA_INI_VIGENCIA,
                               FECHA_FIN_VIGENCIA
                            FROM
                               PFC_CHKLIST_INDICACION   C
                               LEFT JOIN PFC_PARAMETRO            P ON C.P_ESTADO = P.COD_PARAMETRO
                            WHERE
                               C.COD_MAC = PN_COD_MAC
                               AND C.COD_GRP_DIAG = PN_COD_GRP_DIAG;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'LISTA TOTAL!';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_s_config_checklist]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_CONFIG_CHECKLIST;

   PROCEDURE SP_PFC_S_FILTRO_MAC (
      PV_P_COD           IN                 VARCHAR2,
      PV_P_DES           IN                 VARCHAR2,
      PV_P_COM           IN                 VARCHAR2,
      PV_TIPO            IN                 VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      L_SQL_STMT VARCHAR2(4000);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'Valor Inicial';
      
      IF PV_TIPO = '1' AND NULLIF(PV_P_COM,'') IS NOT NULL THEN
          L_SQL_STMT         := 'SELECT 
                    M.COD_MAC,
                    M.COD_MAC_LARGO, 
                    PA.NOMBRE_COMERCIAL,
                    M.DESCRIPCION, 
                    M.P_TIPO_MAC,
                    TIPOP.nombre AS D_TIPO_MAC,
                    M.FEC_INSCRIPCION, 
                    M.FEC_INICIO_VIG, 
                    M.FEC_FIN_VIG, 
                    M.FEC_CREACION, 
                    M.USR_CREACION, 
                    M.FEC_MODIFICACION, 
                    M.USR_MODIFICACION, 
                    M.P_ESTADO_MAC,
                    ESTADOP.nombre AS D_ESTADO_MAC
                FROM PFC_MAC M 
                    LEFT JOIN PFC_PRODUCTO_ASOCIADO PA ON PA.COD_MAC = M.COD_MAC
                    LEFT JOIN PFC_PARAMETRO TIPOP ON M.p_tipo_mac = TIPOP.cod_parametro
                    LEFT JOIN PFC_PARAMETRO ESTADOP ON M.P_ESTADO_MAC = ESTADOP.cod_parametro
                WHERE 1 = 1 AND M.P_ESTADO_MAC = 8';
                    
          IF NULLIF(PV_P_COD,'') IS NULL THEN            
            L_SQL_STMT := L_SQL_STMT;            
          ELSE            
            L_SQL_STMT := L_SQL_STMT
                           || ' AND UPPER(M.COD_MAC_LARGO) LIKE UPPER('
                           || '''%'
                           || PV_P_COD
                           || '%'''
                           || ')';
          END IF;

          IF NULLIF(PV_P_DES,'') IS NULL THEN            
            L_SQL_STMT := L_SQL_STMT;
          ELSE            
            L_SQL_STMT := L_SQL_STMT
                           || ' AND UPPER(M.DESCRIPCION) LIKE UPPER('
                           || '''%'
                           || PV_P_DES
                           || '%'''
                           || ')';
          END IF;

          IF NULLIF(PV_P_COM,'') IS NULL THEN            
            L_SQL_STMT := L_SQL_STMT;            
          ELSE            
            L_SQL_STMT := L_SQL_STMT
                           || ' AND UPPER(PA.NOMBRE_COMERCIAL) LIKE UPPER('
                           || '''%'
                           || PV_P_COM
                           || '%'''
                           || ') AND PA.P_ESTADO = 8';
          END IF;
      
      ELSIF PV_TIPO = '1' THEN
        L_SQL_STMT := 'SELECT 
                    M.COD_MAC,
                    M.COD_MAC_LARGO,
                    '''' AS NOMBRE_COMERCIAL,
                    M.DESCRIPCION, 
                    M.P_TIPO_MAC,
                    (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = M.P_TIPO_MAC) AS D_TIPO_MAC,
                    M.FEC_INSCRIPCION, 
                    M.FEC_INICIO_VIG, 
                    M.FEC_FIN_VIG, 
                    M.FEC_CREACION, 
                    M.USR_CREACION, 
                    M.FEC_MODIFICACION, 
                    M.USR_MODIFICACION, 
                    M.P_ESTADO_MAC,
                    (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = M.P_ESTADO_MAC) AS D_ESTADO_MAC
                FROM PFC_MAC M
                WHERE 1 = 1 AND M.P_ESTADO_MAC = 8';
                    
        IF NULLIF(PV_P_COD,'') IS NULL THEN            
            L_SQL_STMT := L_SQL_STMT;            
          ELSE            
            L_SQL_STMT := L_SQL_STMT
                           || ' AND UPPER(M.COD_MAC_LARGO) LIKE UPPER('
                           || '''%'
                           || PV_P_COD
                           || '%'''
                           || ')';
          END IF;

          IF NULLIF(PV_P_DES,'') IS NULL THEN
            
            L_SQL_STMT := L_SQL_STMT;
          ELSE
            
            L_SQL_STMT := L_SQL_STMT
                           || ' AND UPPER(M.DESCRIPCION) LIKE UPPER('
                           || '''%'
                           || PV_P_DES
                           || '%'''
                           || ')';
          END IF;
      ELSE
        L_SQL_STMT := 'SELECT 
                    M.COD_MAC,
                    M.COD_MAC_LARGO,
                    '''' AS NOMBRE_COMERCIAL,
                    M.DESCRIPCION, 
                    M.P_TIPO_MAC,
                    (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = M.P_TIPO_MAC) AS D_TIPO_MAC,
                    M.FEC_INSCRIPCION, 
                    M.FEC_INICIO_VIG, 
                    M.FEC_FIN_VIG, 
                    M.FEC_CREACION, 
                    M.USR_CREACION, 
                    M.FEC_MODIFICACION, 
                    M.USR_MODIFICACION, 
                    M.P_ESTADO_MAC,
                    (SELECT P.NOMBRE FROM PFC_PARAMETRO P WHERE P.COD_PARAMETRO = M.P_ESTADO_MAC) AS D_ESTADO_MAC
                FROM PFC_MAC M
                WHERE 1 = 1';
                    
        IF NULLIF(PV_P_COD,'') IS NULL THEN            
            L_SQL_STMT := L_SQL_STMT;            
          ELSE            
            L_SQL_STMT := L_SQL_STMT
                           || ' AND UPPER(M.COD_MAC_LARGO) LIKE UPPER('
                           || '''%'
                           || PV_P_COD
                           || '%'''
                           || ')';
          END IF;

          IF NULLIF(PV_P_DES,'') IS NULL THEN
            
            L_SQL_STMT := L_SQL_STMT;
          ELSE
            
            L_SQL_STMT := L_SQL_STMT
                           || ' AND UPPER(M.DESCRIPCION) LIKE UPPER('
                           || '''%'
                           || PV_P_DES
                           || '%'''
                           || ')';
          END IF;        
      END IF;
          L_SQL_STMT         := L_SQL_STMT || ' ORDER BY M.COD_MAC ASC';
      OPEN TC_LIST FOR L_SQL_STMT;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta Exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_FILTRO_MAC]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_FILTRO_MAC;



   PROCEDURE SP_PFC_I_REGISTRO_CHEKLIST (
      PN_CODMAC               IN                      NUMBER,
      PN_GRPDIAGNOSTICO       IN                      NUMBER,
      PN_CODCHKLISTINDI       IN                      NUMBER,
      PV_DESCRIPCION          IN                      VARCHAR2,
      PN_PESTADO              IN                      NUMBER,
      PD_FECHAINIVIGENCIA     IN                      DATE,
      PD_FECHAFINVIGENCIA     IN                      DATE,
      PN_OUT_CODCHKLISTINDI   OUT                     NUMBER,
      PV_OUT_CODCHKLARGO      OUT                     VARCHAR2,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   ) AS
      V_COD_INDICADOR NUMBER;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'No registro, tampoco actualizo.';
      IF ( PN_CODCHKLISTINDI IS NULL ) THEN
         SELECT
            COUNT(*)
         INTO V_COD_INDICADOR
         FROM
            PFC_CHKLIST_INDICACION C
         WHERE
            C.COD_MAC = PN_CODMAC
            AND C.COD_GRP_DIAG = PN_GRPDIAGNOSTICO;

         PN_OUT_CODCHKLISTINDI   := V_COD_INDICADOR + 1;
         PV_OUT_CODCHKLARGO      := LPAD(
            PN_OUT_CODCHKLISTINDI,
            2,
            '0'
         );
         INSERT INTO PFC_CHKLIST_INDICACION (
            COD_CHKLIST_INDI,
            COD_CHKLIST_INDI_LARGO,
            DESCRIPCION,
            P_ESTADO,
            COD_MAC,
            COD_GRP_DIAG,
            FECHA_INI_VIGENCIA,
            FECHA_FIN_VIGENCIA
         ) VALUES (
            PN_OUT_CODCHKLISTINDI,
            PV_OUT_CODCHKLARGO,
            PV_DESCRIPCION,
            PN_PESTADO,
            PN_CODMAC,
            PN_GRPDIAGNOSTICO,
            PD_FECHAINIVIGENCIA,
            PD_FECHAFINVIGENCIA
         );

         PN_COD_RESULTADO        := 0;
         PV_MSG_RESULTADO        := 'Registro Exitoso!';
      ELSE
         UPDATE PFC_CHKLIST_INDICACION
         SET
            DESCRIPCION = PV_DESCRIPCION,
            P_ESTADO = PN_PESTADO,
            COD_MAC = PN_CODMAC,
            FECHA_INI_VIGENCIA = PD_FECHAINIVIGENCIA,
            FECHA_FIN_VIGENCIA = PD_FECHAFINVIGENCIA
         WHERE
            COD_CHKLIST_INDI = PN_CODCHKLISTINDI
            AND COD_MAC       = PN_CODMAC
            AND COD_GRP_DIAG  = PN_GRPDIAGNOSTICO;

         PN_COD_RESULTADO        := 0;
         PN_OUT_CODCHKLISTINDI   := PN_CODCHKLISTINDI;
         PV_OUT_CODCHKLARGO      := LPAD(
            PN_OUT_CODCHKLISTINDI,
            2,
            '0'
         );
         PV_MSG_RESULTADO        := 'Actualización Exitosa!!';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_i_registro_cheklist]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_I_REGISTRO_CHEKLIST;

   PROCEDURE SP_PFC_S_LISTA_CRITINCLUSION (
      PN_CODCHKLISTINDI       IN                      NUMBER,
      PN_COD_MAC              IN                      NUMBER,
      PN_GRP_DIAG             IN                      NUMBER,
      TC_LIST                 OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_OUT_CODCHKLISTINDI   OUT                     NUMBER,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA !';
      OPEN TC_LIST FOR SELECT
                          C.COD_CRITERIO_INCLU         AS COD_CRITERIO,
                          C.COD_CRITERIO_INCLU_LARGO   AS COD_CRITERIO_LARGO,
                          C.COD_CHKLIST_INDI           AS COD_INDICADOR,
                          C.DESCRIPCION,
                          C.ORDEN,
                          C.P_ESTADO,
                          P.NOMBRE                     AS ESTADO
                       FROM
                          PFC_CRITERIO_INCLUSION   C
                          LEFT JOIN PFC_PARAMETRO  P ON C.P_ESTADO = P.COD_PARAMETRO
                       WHERE
                          COD_CHKLIST_INDI = PN_CODCHKLISTINDI
                          AND COD_MAC       = PN_COD_MAC
                          AND COD_GRP_DIAG  = PN_GRP_DIAG;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA !';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_s_lista_critinclusion]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_LISTA_CRITINCLUSION;

   PROCEDURE SP_PFC_S_LISTA_CRITEXCLUSION (
      PN_CODCHKLISTINDI       IN                      NUMBER,
      PN_COD_MAC              IN                      NUMBER,
      PN_GRP_DIAG             IN                      NUMBER,
      TC_LIST                 OUT                     PCK_PFC_CONSTANTE.TYPCUR,
      PN_OUT_CODCHKLISTINDI   OUT                     NUMBER,
      PN_COD_RESULTADO        OUT                     NUMBER,
      PV_MSG_RESULTADO        OUT                     VARCHAR2
   ) AS
   BEGIN
      OPEN TC_LIST FOR SELECT
                          C.COD_CRITERIO_EXCLU         AS COD_CRITERIO,
                          C.COD_CRITERIO_EXCLU_LARGO   AS COD_CRITERIO_LARGO,
                          C.COD_CHKLIST_INDI           AS COD_INDICADOR,
                          C.DESCRIPCION,
                          C.ORDEN,
                          C.P_ESTADO,
                          P.NOMBRE                     AS ESTADO
                       FROM
                          PFC_CRITERIO_EXCLUSION  C
                          LEFT JOIN PFC_PARAMETRO  P ON C.P_ESTADO = P.COD_PARAMETRO
                       WHERE
                          COD_CHKLIST_INDI = PN_CODCHKLISTINDI
                          AND COD_MAC       = PN_COD_MAC
                          AND COD_GRP_DIAG  = PN_GRP_DIAG;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA !';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_s_lista_critexclusion]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_LISTA_CRITEXCLUSION;

   PROCEDURE SP_PFC_I_REGCRITEXCLUSION (
      PN_CODCHKLISTINDI     IN                    NUMBER,
      PN_COD_MAC            IN                    NUMBER,
      PN_GRP_DIAG           IN                    NUMBER,
      PN_CODEXCLUSION       IN                    NUMBER,
      PN_DESCRIPCRITERIO    IN                    VARCHAR2,
      PN_ESTADO             IN                    NUMBER,
      PN_OUT_CODEXCLUSION   OUT                   NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS
      V_COD_EXCLUSION NUMBER;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'No registro, tampoco actualizo.';
      IF ( PN_CODEXCLUSION IS NULL ) THEN
         SELECT
            COUNT(*)
         INTO V_COD_EXCLUSION
         FROM
            PFC_CRITERIO_EXCLUSION C
         WHERE
            C.COD_MAC = PN_COD_MAC
            AND C.COD_GRP_DIAG      = PN_GRP_DIAG
            AND C.COD_CHKLIST_INDI  = PN_CODCHKLISTINDI;
        
            /*pn_out_codexclusion := sq_pfc_crit_exc_cod_crit_exc.nextval;*/

         PN_OUT_CODEXCLUSION   := V_COD_EXCLUSION + 1;
         INSERT INTO PFC_CRITERIO_EXCLUSION (
            COD_CRITERIO_EXCLU,
            COD_CHKLIST_INDI,
            COD_CRITERIO_EXCLU_LARGO,
            DESCRIPCION,
            ORDEN,
            P_ESTADO,
            COD_MAC,
            COD_GRP_DIAG
         ) VALUES (
            PN_OUT_CODEXCLUSION,
            PN_CODCHKLISTINDI,
            LPAD(
               PN_OUT_CODEXCLUSION,
               2,
               '0'
            ),
            PN_DESCRIPCRITERIO,
            NULL,
            PN_ESTADO,
            PN_COD_MAC,
            PN_GRP_DIAG
         );

         PN_COD_RESULTADO      := 0;
         PV_MSG_RESULTADO      := 'Registro Exitoso!';
      ELSE
         UPDATE PFC_CRITERIO_EXCLUSION
         SET
            DESCRIPCION = PN_DESCRIPCRITERIO,
            P_ESTADO = PN_ESTADO
         WHERE
            COD_CRITERIO_EXCLU = PN_CODEXCLUSION
            AND COD_CHKLIST_INDI  = PN_CODCHKLISTINDI
            AND COD_MAC           = PN_COD_MAC
            AND COD_GRP_DIAG      = PN_GRP_DIAG;

         PN_COD_RESULTADO      := 0;
         PN_OUT_CODEXCLUSION   := PN_CODEXCLUSION;
         PV_MSG_RESULTADO      := 'Actualización Exitosa!!';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_i_regcritexclusion]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_REGCRITEXCLUSION;

   PROCEDURE SP_PFC_I_REGCRITINCLUSION (
      PN_CODCHKLISTINDI     IN                    NUMBER,
      PN_COD_MAC            IN                    NUMBER,
      PN_GRP_DIAG           IN                    NUMBER,
      PN_CODINCLUSION       IN                    NUMBER,
      PN_DESCRIPCRITERIO    IN                    VARCHAR2,
      PN_ESTADO             IN                    NUMBER,
      PN_OUT_CODINCLUSION   OUT                   NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS
      V_COD_INCLUSION NUMBER;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'No registro, tampoco actualizo.';
      IF ( PN_CODINCLUSION IS NULL ) THEN
         SELECT
            COUNT(*)
         INTO V_COD_INCLUSION
         FROM
            PFC_CRITERIO_INCLUSION C
         WHERE
            C.COD_MAC = PN_COD_MAC
            AND C.COD_GRP_DIAG      = PN_GRP_DIAG
            AND C.COD_CHKLIST_INDI  = PN_CODCHKLISTINDI;
        
            /*pn_out_codinclusion := sq_pfc_crit_inc_cod_crit_inc.nextval;*/

         PN_OUT_CODINCLUSION   := V_COD_INCLUSION + 1;
         INSERT INTO PFC_CRITERIO_INCLUSION (
            COD_CRITERIO_INCLU,
            COD_CHKLIST_INDI,
            COD_CRITERIO_INCLU_LARGO,
            DESCRIPCION,
            ORDEN,
            P_ESTADO,
            COD_MAC,
            COD_GRP_DIAG
         ) VALUES (
            PN_OUT_CODINCLUSION,
            PN_CODCHKLISTINDI,
            LPAD(
               PN_OUT_CODINCLUSION,
               2,
               '0'
            ),
            PN_DESCRIPCRITERIO,
            NULL,
            PN_ESTADO,
            PN_COD_MAC,
            PN_GRP_DIAG
         );

         PN_COD_RESULTADO      := 0;
         PV_MSG_RESULTADO      := 'Registro Exitoso!';
      ELSE
         UPDATE PFC_CRITERIO_INCLUSION
         SET
            DESCRIPCION = PN_DESCRIPCRITERIO,
            P_ESTADO = PN_ESTADO
         WHERE
            COD_CRITERIO_INCLU = PN_CODINCLUSION
            AND COD_CHKLIST_INDI  = PN_CODCHKLISTINDI
            AND COD_MAC           = PN_COD_MAC
            AND COD_GRP_DIAG      = PN_GRP_DIAG;

         PN_COD_RESULTADO      := 0;
         PN_OUT_CODINCLUSION   := PN_CODINCLUSION;
         PV_MSG_RESULTADO      := 'Actualización Exitosa!!';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_i_regcritinclusion]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_REGCRITINCLUSION;

   PROCEDURE SP_PFC_I_REGISTROFICHA (
      PN_CODVERSION      IN                 NUMBER,
      PV_NOMBREARCHIVO   IN                 VARCHAR2,
      PN_ESTADO          IN                 NUMBER,
      PN_CODMAC          IN                 NUMBER,
      PN_CODIGO_FICHA    IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      V_PN_COD_FICHA_TECNICA NUMBER;
      V_COD_VERSION_COUNT NUMBER;
   BEGIN
      
      IF ( PN_CODMAC IS NOT NULL ) THEN
           IF ( PN_CODIGO_FICHA IS NOT NULL  ) THEN
           
                    SELECT
                       COUNT(*)
                    INTO V_COD_VERSION_COUNT
                    FROM
                       PFC_FICHA_TECNICA
                    WHERE
                       COD_MAC = PN_CODMAC
                       AND COD_FILE_UPLOAD = PN_CODIGO_FICHA;
                       
                    IF V_COD_VERSION_COUNT != 0 THEN
                       PN_COD_RESULTADO   := -3;
                       PV_MSG_RESULTADO   := 'Código Ya Existe!';
                    ELSE
           
                                     
                          
                              UPDATE PFC_FICHA_TECNICA
                              SET
                                 FEC_FIN_VIG = SYSDATE - 1,
                                 P_ESTADO = 9
                              WHERE
                                 COD_MAC = PN_CODMAC
                                 AND TO_NUMBER( COD_VERSION) = ( TO_NUMBER(PN_CODVERSION) - 1 );
                               
                             
                             SELECT
                                COUNT(*)+1
                             INTO V_PN_COD_FICHA_TECNICA
                             FROM
                                PFC_FICHA_TECNICA
                             WHERE
                                COD_MAC = PN_CODMAC;
                    
                             INSERT INTO PFC_FICHA_TECNICA (
                                COD_FICHA_TECNICA,
                                COD_VERSION,
                                NOMBRE_ARCHIVO,
                                FEC_INICIO_VIG,
                                P_ESTADO,
                                COD_MAC,
                                COD_FILE_UPLOAD
                             ) VALUES (
                                V_PN_COD_FICHA_TECNICA,
                                LPAD(
                                   PN_CODVERSION,
                                   2,
                                   '0'
                                ),
                                PV_NOMBREARCHIVO,
                                SYSDATE,
                                PN_ESTADO,
                                PN_CODMAC,
                                PN_CODIGO_FICHA
                             );
                             PN_COD_RESULTADO   := 0;
                             PV_MSG_RESULTADO   := 'Registrado Correctamente!';
                             COMMIT;
                END IF;             
        
          END IF ;          
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_i_registroficha]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_REGISTROFICHA;

   PROCEDURE SP_PFC_S_LISTARFICHAS (
      PN_CODMAC          IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA !';
      OPEN TC_LIST FOR SELECT
                          FT.COD_MAC             AS COD_MAC,
                          FT.COD_FILE_UPLOAD   AS COD_FICHA,
                          FT.COD_VERSION,
                          FT.NOMBRE_ARCHIVO,
                          FT.FEC_INICIO_VIG      AS FECHA_INICIO,
                          FT.FEC_FIN_VIG         AS FECHA_FIN,
                          FT.P_ESTADO            AS COD_ESTADO,
                          P.NOMBRE               AS ESTADO
                       FROM
                          PFC_FICHA_TECNICA   FT
                          LEFT JOIN PFC_PARAMETRO       P ON P.COD_PARAMETRO = FT.P_ESTADO
                       WHERE
                          FT.COD_MAC = PN_CODMAC;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_s_listarfichas]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_LISTARFICHAS;

   PROCEDURE SP_PFC_I_REGISTRO_COMP (
      PV_COD_VERSION           IN                       VARCHAR2,
      PV_NOMBRE_ARCHIVO        IN                       VARCHAR2,
      PN_ESTADO                IN                       NUMBER,
      PN_CODMAC                IN                       NUMBER,
      PN_CODIGO_ARCHIVO_COMP   IN                       NUMBER,
      PN_COD_RESULTADO         OUT                      NUMBER,
      PV_MSG_RESULTADO         OUT                      VARCHAR2
   ) AS
      V_COD_ARCHIVO_COMP    NUMBER;
      V_COD_VERSION_COUNT   NUMBER;
      V_NOMBRE_MAC          VARCHAR2(80);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'Error en SP';
      IF ( PN_CODMAC IS NOT NULL ) THEN
         IF PN_CODIGO_ARCHIVO_COMP IS NOT NULL THEN
         
            SELECT
               COUNT(*)
            INTO V_COD_VERSION_COUNT
            FROM
               PFC_ARCHIVO_COMPLICACION
            WHERE
               COD_MAC = PN_CODMAC
               AND CODIGO_ARCHIVO_COMP = PN_CODIGO_ARCHIVO_COMP;
               
            IF V_COD_VERSION_COUNT != 0 THEN
               PN_COD_RESULTADO   := -3;
               PV_MSG_RESULTADO   := 'Código Ya Existe!';
            ELSE
         
         
               SELECT
                  COUNT(*)
               INTO V_COD_VERSION_COUNT
               FROM
                  PFC_ARCHIVO_COMPLICACION
               WHERE
                  COD_MAC = PN_CODMAC
                  AND TO_NUMBER( COD_VERSION) = ( TO_NUMBER(PV_COD_VERSION) - 1 );

              
                  UPDATE PFC_ARCHIVO_COMPLICACION
                  SET
                     FEC_FIN_VIG = SYSDATE - 1,
                     P_ESTADO = 9
                  WHERE
                     COD_MAC = PN_CODMAC
                     AND TO_NUMBER( COD_VERSION) = ( TO_NUMBER(PV_COD_VERSION) - 1 );
                    
                    
                    SELECT
                        COUNT(*) + 1
                       INTO V_COD_VERSION_COUNT
                       FROM
                          PFC_ARCHIVO_COMPLICACION ;
                     
                     INSERT INTO PFC_ARCHIVO_COMPLICACION (
                      COD_ARCHIVO_COMP,
                      COD_VERSION,
                      NOMBRE_ARCHIVO,
                      FEC_INICIO_VIG,
                      P_ESTADO,
                      COD_MAC,
                      CODIGO_ARCHIVO_COMP
                   ) VALUES (
                      V_COD_VERSION_COUNT,
                      PV_COD_VERSION,
                      PV_NOMBRE_ARCHIVO,
                      SYSDATE,
                      PN_ESTADO,
                      PN_CODMAC,
                      PN_CODIGO_ARCHIVO_COMP
                   );
                     
                   PN_COD_RESULTADO   := 0;
                   PV_MSG_RESULTADO   := 'Registro Exitoso .!';  
                   COMMIT;
                
             END IF;              
         END IF;
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_i_registro_comp]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_REGISTRO_COMP;

   PROCEDURE SP_PFC_U_REGISTRO_COMP (
      PV_COD_VERSION       IN                   VARCHAR2,
      PN_COD_FILE_UPLOAD   IN                   NUMBER,
      PN_CODMAC            IN                   NUMBER,
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'Error en SP';
      IF ( PN_CODMAC IS NOT NULL ) THEN
         UPDATE PFC_ARCHIVO_COMPLICACION
         SET
            CODIGO_ARCHIVO_COMP = PN_COD_FILE_UPLOAD
         WHERE
            COD_MAC = PN_CODMAC
            AND COD_ARCHIVO_COMP = TO_NUMBER(PV_COD_VERSION);

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Actualización Exitosa .!';
         COMMIT;
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_u_registro_comp]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_U_REGISTRO_COMP;

   PROCEDURE SP_PFC_S_LISTARCOMP (
      PN_CODMAC          IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA !';
      OPEN TC_LIST FOR SELECT
                          AC.COD_MAC               AS COD_MAC,
                          AC.COD_ARCHIVO_COMP      AS COD_ARCHIVO_COMP,
                          AC.COD_VERSION           AS COD_VERSION,
                          AC.NOMBRE_ARCHIVO        AS NOMBRE_ARCHIVO,
                          AC.FEC_INICIO_VIG        AS FECHA_INICIO,
                          AC.FEC_FIN_VIG           AS FECHA_FIN,
                          AC.P_ESTADO              AS COD_ESTADO,
                          P.NOMBRE                 AS ESTADO,
                          AC.CODIGO_ARCHIVO_COMP   AS CODIGO_ARCHIVO_COMP
                       FROM
                          PFC_ARCHIVO_COMPLICACION   AC
                          LEFT JOIN PFC_PARAMETRO              P ON P.COD_PARAMETRO = AC.P_ESTADO
                       WHERE
                          AC.COD_MAC = PN_CODMAC;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_s_listarComp]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_LISTARCOMP;

   PROCEDURE SP_PFC_I_REGISTRO_PROD_ASOC (
      PN_CODMAC             IN                    NUMBER,
      PV_COD_PRODUCTO       IN                    VARCHAR2,
      PV_DESCRIPCION        IN                    VARCHAR2,
      PV_NOMBRE_COMERCIAL   IN                    VARCHAR2,
      PN_COD_ESTADO         IN                    NUMBER,
      PN_COD_LABORATORIO    IN                    NUMBER,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS
      V_COD_PRODUCTO NUMBER;
   BEGIN
      IF ( PN_CODMAC IS NOT NULL ) THEN
         SELECT
            COUNT(*)
         INTO V_COD_PRODUCTO
         FROM
            PFC_PRODUCTO_ASOCIADO
         WHERE
            COD_PRODUCTO = PV_COD_PRODUCTO
            AND COD_MAC = PN_CODMAC;

         IF V_COD_PRODUCTO != 0 THEN
            UPDATE PFC_PRODUCTO_ASOCIADO
            SET
               COD_MAC = PN_CODMAC,
               DESCRIPCION_GENERICA = PV_DESCRIPCION,
               NOMBRE_COMERCIAL = PV_NOMBRE_COMERCIAL,
               P_ESTADO = PN_COD_ESTADO,
               P_LABORATORIO = PN_COD_LABORATORIO
            WHERE
               COD_PRODUCTO = PV_COD_PRODUCTO
               AND COD_MAC = PN_CODMAC;

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'Registro Actualizado Exitoso!';
         ELSE
            INSERT INTO PFC_PRODUCTO_ASOCIADO (
               COD_PRODUCTO,
               COD_MAC,
               DESCRIPCION_GENERICA,
               NOMBRE_COMERCIAL,
               P_ESTADO,
               P_LABORATORIO
            ) VALUES (
               PV_COD_PRODUCTO,
               PN_CODMAC,
               PV_DESCRIPCION,
               PV_NOMBRE_COMERCIAL,
               PN_COD_ESTADO,
               PN_COD_LABORATORIO
            );

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'Registro Exitoso .!';
            COMMIT;
         END IF;

      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_i_registro_prod_asoc]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_REGISTRO_PROD_ASOC;

   PROCEDURE SP_PFC_S_LISTAR_PROD_ASOC (
      PN_CODMAC          IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA !';
      OPEN TC_LIST FOR SELECT
                          A.COD_MAC,
                          A.COD_PRODUCTO,
                          A.DESCRIPCION_GENERICA,
                          A.NOMBRE_COMERCIAL,
                          A.P_LABORATORIO   AS COD_LABORATORIO,
                          B.NOMBRE          AS LABORATORIO,
                          A.P_ESTADO        AS COD_ESTADO,
                          C.NOMBRE          AS ESTADO
                       FROM
                          PFC_PRODUCTO_ASOCIADO   A,
                          PFC_PARAMETRO           B,
                          PFC_PARAMETRO           C
                       WHERE
                          A.P_LABORATORIO = B.COD_PARAMETRO
                          AND A.P_ESTADO  = C.COD_PARAMETRO
                          AND A.COD_MAC   = PN_CODMAC;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_s_listar_prod_asoc]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_LISTAR_PROD_ASOC;

   PROCEDURE SP_PFC_I_REG_EXAM_MED_MARCA (
      PN_COD_EXAMEN_MED      IN                     NUMBER,
      PV_DESCRIPCION         IN                     VARCHAR2,
      PN_COD_TIPO_EXAMEN     IN                     NUMBER,
      PN_COD_ESTADO          IN                     NUMBER,
      PV_USUARIO_CREA        IN                     VARCHAR2,
      PD_FECHA_CREA          IN                     DATE,
      PV_USUARIO_MODIF       IN                     VARCHAR2,
      PD_FECHA_MODIF         IN                     DATE,
      PN_COD_EXAMEN_MEDICO   OUT                    NUMBER,
      PN_COD_RESULTADO       OUT                    NUMBER,
      PV_MSG_RESULTADO       OUT                    VARCHAR2
   ) AS
      V_COD_EXAMEN_MED NUMBER;
   BEGIN
      IF ( PN_COD_EXAMEN_MED IS NULL ) THEN
         SELECT
            COUNT(*)
         INTO V_COD_EXAMEN_MED
         FROM
            PFC_EXAMEN_MEDICO_MARCADOR;

         PN_COD_EXAMEN_MEDICO   := V_COD_EXAMEN_MED + 1;
         INSERT INTO PFC_EXAMEN_MEDICO_MARCADOR (
            COD_EXAMEN_MED,
            COD_EXAMEN_MED_LARGO,
            DESCRIPCION,
            P_TIPO_EXAMEN,
            P_ESTADO,
            USUARIO_CREA,
            FECHA_CREA
         ) VALUES (
            PN_COD_EXAMEN_MEDICO,
            LPAD(
               PN_COD_EXAMEN_MEDICO,
               5,
               '0'
            ),
            PV_DESCRIPCION,
            PN_COD_TIPO_EXAMEN,
            PN_COD_ESTADO,
            PV_USUARIO_CREA,
            SYSDATE
         );

         PN_COD_RESULTADO       := 0;
         PV_MSG_RESULTADO       := 'Registro Exitoso .!';
      ELSE
         UPDATE PFC_EXAMEN_MEDICO_MARCADOR
         SET
            DESCRIPCION = PV_DESCRIPCION,
            P_TIPO_EXAMEN = PN_COD_TIPO_EXAMEN,
            P_ESTADO = PN_COD_ESTADO,
            USUARIO_MODIF = PV_USUARIO_MODIF,
            FECHA_MODIF = SYSDATE
         WHERE
            COD_EXAMEN_MED = PN_COD_EXAMEN_MED;

         PN_COD_EXAMEN_MEDICO := PN_COD_EXAMEN_MED;
         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Registro Actualizado .!';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_i_reg_exam_med_marca]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_REG_EXAM_MED_MARCA;

   PROCEDURE SP_PFC_I_REG_EXAM_MED_DET (
      PN_COD_EXAMEN_MED_DET     IN                        NUMBER,
      PN_COD_EXAMEN_MED         IN                        NUMBER,
      PN_COD_TIPO_INGRESO_RES   IN                        NUMBER,
      PV_UNIDAD_MEDIDA          IN                        VARCHAR2,
      PV_RANGO                  IN                        VARCHAR2,
      PV_VALOR_FIJO             IN                        VARCHAR2,
      PN_COD_ESTADO             IN                        NUMBER,
      PN_RANGO_MINIMO           IN                        NUMBER,
      PN_RANGO_MAXIMO           IN                        NUMBER,
      PV_USUARIO_CREA           IN                        VARCHAR2,
      PD_FECHA_CREA             IN                        DATE,
      PV_USUARIO_MODIF          IN                        VARCHAR2,
      PD_FECHA_MODIF            IN                        DATE,
      PN_COD_RESULTADO          OUT                       NUMBER,
      PV_MSG_RESULTADO          OUT                       VARCHAR2
   ) AS
      V_COD_EXAMEN_MED_DET NUMBER;
   BEGIN
      
         SELECT
            MAX(COD_EXAMEN_MED_DET)
         INTO V_COD_EXAMEN_MED_DET
         FROM
            PFC_EXAMEN_MEDICO_DETALLE;

         V_COD_EXAMEN_MED_DET   := V_COD_EXAMEN_MED_DET + 1;
         INSERT INTO PFC_EXAMEN_MEDICO_DETALLE (
            COD_EXAMEN_MED_DET,
            COD_EXAMEN_MED,
            P_TIPO_INGRESO_RES,
            UNIDAD_MEDIDA,
            RANGO,
            VALOR_FIJO,
            P_ESTADO,
            RANGO_MINIMO,
            RANGO_MAXIMO,
            USUARIO_CREA,
            FECHA_CREA
         ) VALUES (
            V_COD_EXAMEN_MED_DET,
            PN_COD_EXAMEN_MED,
            PN_COD_TIPO_INGRESO_RES,
            PV_UNIDAD_MEDIDA,
            PV_RANGO,
            PV_VALOR_FIJO,
            PN_COD_ESTADO,
            PN_RANGO_MINIMO,
            PN_RANGO_MAXIMO,
            PV_USUARIO_CREA,
            SYSDATE
         );

         PN_COD_RESULTADO       := 0;
         PV_MSG_RESULTADO       := 'Registro Exitoso .!';
     
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_i_reg_exam_med_det]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_REG_EXAM_MED_DET;

   PROCEDURE SP_PFC_S_LISTAR_EXAMS_MED (
      PV_DESCRIPCION     IN                 VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      TC_LIST_DETALLE    OUT                PCK_PFC_CONSTANTE.TYPCUR
   ) AS
   L_SQL_STMT VARCHAR2(4000);
   BEGIN
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA !';
      
       L_SQL_STMT:= 'SELECT
                          A.COD_EXAMEN_MED,
                          A.COD_EXAMEN_MED_LARGO,
                          A.DESCRIPCION,
                          A.P_TIPO_EXAMEN   AS COD_TIPO_EXAMEN,
                          C.NOMBRE          AS TIPO,
                          A.P_ESTADO        AS COD_ESTADO_EXAM_MED,
                          D.NOMBRE          AS ESTADO_EXAM_MED
                       FROM
                          PFC_EXAMEN_MEDICO_MARCADOR   A,
                          PFC_PARAMETRO                C,
                          PFC_PARAMETRO                D
                       WHERE
                          A.P_TIPO_EXAMEN = C.COD_PARAMETRO
                          AND A.P_ESTADO = D.COD_PARAMETRO';
                          
        -- Filtro por Descripcion
        IF PV_DESCRIPCION = '' OR PV_DESCRIPCION = 'null' THEN
            L_SQL_STMT := L_SQL_STMT ||   ' AND  1=1 ';
        ELSE
             L_SQL_STMT := L_SQL_STMT ||  ' AND A.DESCRIPCION LIKE UPPER ('||'''%'|| PV_DESCRIPCION||'%'''||')';
        END IF;                  
      
      L_SQL_STMT := L_SQL_STMT || ' ORDER BY a.COD_EXAMEN_MED DESC';
      
      OPEN TC_LIST FOR L_SQL_STMT;

      OPEN TC_LIST_DETALLE FOR SELECT
                                 B.COD_EXAMEN_MED,
                                 B.COD_EXAMEN_MED_DET,
                                 B.P_TIPO_INGRESO_RES   AS COD_TIPO_INGRESO_RES,
                                 E.NOMBRE               AS TIPO_INGRESO_RES,
                                 B.UNIDAD_MEDIDA,
                                 B.RANGO,
                                 B.VALOR_FIJO,
                                 B.P_ESTADO             AS COD_ESTADO_EXAM_MED_DET,
                                 F.NOMBRE               AS ESTADO_EXAM_MED_DET,
                                 B.RANGO_MINIMO,
                                 B.RANGO_MAXIMO
                              FROM
                                 PFC_EXAMEN_MEDICO_DETALLE   B,
                                 PFC_PARAMETRO               E,
                                 PFC_PARAMETRO               F
                              WHERE
                                 B.P_TIPO_INGRESO_RES = E.COD_PARAMETRO
                                 AND B.P_ESTADO = F.COD_PARAMETRO;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_s_listar_exams_med]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_LISTAR_EXAMS_MED;

   PROCEDURE SP_PFC_I_REGISTRO_MARCA (
      PN_COD_CONFIG_MARCA         IN                          NUMBER,
      PV_COD_CONFIG_MARCA_LARGO   IN                          VARCHAR2,
      PN_COD_EXAMEN_MED           IN                          NUMBER,
      PN_COD_MAC                  IN                          NUMBER,
      PN_COD_GRP_DIAG             IN                          NUMBER,
      PN_TIPO_MARCADOR            IN                          NUMBER,
      PN_PER_MAXIMA               IN                          NUMBER,
      PN_PER_MINIMA               IN                          NUMBER,
      PN_COD_ESTADO               IN                          NUMBER,
      PN_COD_RESULTADO            OUT                         NUMBER,
      PV_MSG_RESULTADO            OUT                         VARCHAR2
   ) AS
      V_COD_CONFIG_MARCA          NUMBER;
      V_COD_CONFIG_MARCA_EXISTE   NUMBER;
   BEGIN
      IF ( PN_COD_CONFIG_MARCA IS NULL ) THEN
         SELECT
            MAX(COD_CONFIG_MARCA) + 1
         INTO V_COD_CONFIG_MARCA
         FROM
            PFC_CONFIGURACION_MARCADOR;

         INSERT INTO PFC_CONFIGURACION_MARCADOR (
            COD_CONFIG_MARCA,
            COD_CONFIG_MARCA_LARGO,
            COD_EXAMEN_MED,
            COD_MAC,
            COD_GRP_DIAG,
            P_TIPO_MARCADOR,
            P_PER_MAXIMA,
            P_PER_MINIMA,
            P_ESTADO
         ) VALUES (
            V_COD_CONFIG_MARCA,
            LPAD(
               V_COD_CONFIG_MARCA,
               3,
               '0'
            ),
            PN_COD_EXAMEN_MED,
            PN_COD_MAC,
            PN_COD_GRP_DIAG,
            PN_TIPO_MARCADOR,
            PN_PER_MAXIMA,
            PN_PER_MINIMA,
            PN_COD_ESTADO
         );

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Registro Exitoso .!';
      ELSE
         SELECT
            COUNT(*)
         INTO V_COD_CONFIG_MARCA_EXISTE
         FROM
            PFC_CONFIGURACION_MARCADOR
         WHERE
            COD_CONFIG_MARCA = PN_COD_CONFIG_MARCA;

         IF V_COD_CONFIG_MARCA_EXISTE = 0 THEN
            PN_COD_RESULTADO   := -3;
            PV_MSG_RESULTADO   := 'Código NO Existe!';
         ELSE
            UPDATE PFC_CONFIGURACION_MARCADOR
            SET
               COD_EXAMEN_MED = PN_COD_EXAMEN_MED,
               COD_MAC = PN_COD_MAC,
               COD_GRP_DIAG = PN_COD_GRP_DIAG,
               P_TIPO_MARCADOR = PN_TIPO_MARCADOR,
               P_PER_MAXIMA = PN_PER_MAXIMA,
               P_PER_MINIMA = PN_PER_MINIMA,
               P_ESTADO = PN_COD_ESTADO
            WHERE
               COD_CONFIG_MARCA = PN_COD_CONFIG_MARCA;
         END IF;

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Registro Actualizado Exitoso .!';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_i_registro_marca]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_REGISTRO_MARCA;

   PROCEDURE SP_PFC_S_LISTAR_MARCADORES (
      PN_COD_MAC         IN                 NUMBER,
      PN_COD_GRP_DIAG    IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA !';
      OPEN TC_LIST FOR SELECT
                          A.COD_CONFIG_MARCA,
                          A.COD_CONFIG_MARCA_LARGO,
                          A.COD_EXAMEN_MED,
                          H.DESCRIPCION    AS DESCRIPCION_EXAMEN,
                          A.COD_MAC,
                          G.DESCRIPCION    AS DESCRIPCION_MAC,
                          A.COD_GRP_DIAG,
                          '' AS DESCRIPCION_GRUPO_DIAG,
                          A.P_TIPO_MARCADOR,
                          B.NOMBRE         AS MARCADOR,
                          A.P_PER_MAXIMA   AS COD_PER_MAXIMA,
                          C.NOMBRE         AS DESCRIPCION_PER_MAXIMA,
                          A.P_PER_MINIMA   AS COD_PER_MINIMA,
                          D.NOMBRE         AS DESCRIPCION_PER_MINIMA,
                          A.P_ESTADO       AS COD_ESTADO,
                          E.NOMBRE         AS ESTADO
                       FROM
                          PFC_CONFIGURACION_MARCADOR   A,
                          PFC_PARAMETRO                B,
                          PFC_PARAMETRO                C,
                          PFC_PARAMETRO                D,
                          PFC_PARAMETRO                E,
                          PFC_MAC                      G,
                          PFC_EXAMEN_MEDICO_MARCADOR   H
                       WHERE
                          A.COD_EXAMEN_MED = H.COD_EXAMEN_MED
                          AND A.COD_MAC          = G.COD_MAC
                          AND A.P_TIPO_MARCADOR  = B.COD_PARAMETRO
                          AND A.P_PER_MAXIMA     = C.COD_PARAMETRO
                          AND A.P_PER_MINIMA     = D.COD_PARAMETRO
                          AND A.P_ESTADO         = E.COD_PARAMETRO
                          AND A.COD_GRP_DIAG     = PN_COD_GRP_DIAG
                          AND A.COD_MAC          = PN_COD_MAC;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[sp_pfc_s_listar_marcadores]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_LISTAR_MARCADORES;
   
   /* AUTHOR  : Jose.Reyes/MDP*/
   /* CREATED : 01/07/2019*/
   /* PURPOSE : Consulta lista de Participantes (MAESTROS)    */
    PROCEDURE SP_PFC_S_LISTAR_PARTICIPANTES (
      PV_APELLIDO_NOMBRE IN                 VARCHAR2,
      PN_COD_USUARIO     IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      TC_LIST_GRP        OUT                PCK_PFC_CONSTANTE.TYPCUR
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'CONSULTA EXITOSA !';
      IF ( PN_COD_USUARIO IS NOT NULL ) THEN
         OPEN TC_LIST FOR select * from (select a.cod_participante, a.nombres, a.apellidos, a.cod_rol, a.correo_electronico, 
                      a.p_estado as codigo_estado, b.nombre as estado, a.codigo_archivo_firma , a.cod_usuario,
                      a.cod_participante_largo, a.coordinador , a.cmp_medico
                      from pfc_participante a, pfc_parametro b
                      where a.p_estado = b.cod_parametro AND A.COD_USUARIO = PN_COD_USUARIO AND A.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_PARTICIP_ACTIVO);
         OPEN TC_LIST_GRP FOR select a.cod_participante , cod_grp_diag, a.p_rango_edad, b.nombre as rango_edad
                                  from  pfc_participante_grupo_diag a, pfc_parametro b
                                  where a.p_rango_edad = b.cod_parametro (+);
      ELSE
        OPEN TC_LIST FOR select * from (select a.cod_participante, a.nombres, a.apellidos, a.cod_rol, a.correo_electronico, 
                      a.p_estado as codigo_estado, b.nombre as estado, a.codigo_archivo_firma , a.cod_usuario,
                      a.cod_participante_largo, a.coordinador , a.cmp_medico
                      from pfc_participante a, pfc_parametro b
                      where a.p_estado = b.cod_parametro
                            and ( a.nombres like '%' || PV_APELLIDO_NOMBRE || '%' or  
                                  a.apellidos like '%' || PV_APELLIDO_NOMBRE || '%') 
                                  UNION ALL
                                  select a.cod_participante, a.nombres, a.apellidos, a.cod_rol, a.correo_electronico, 
                      a.p_estado as codigo_estado, b.nombre as estado, a.codigo_archivo_firma , a.cod_usuario ,
                       a.cod_participante_largo, a.coordinador , a.cmp_medico
                      from pfc_participante a, pfc_parametro b
                      where a.p_estado = b.cod_parametro
                            and ( a.nombres is null or  
                                  a.apellidos is null))
                          order by cod_participante desc;
                                  
         OPEN TC_LIST_GRP FOR select a.cod_participante , cod_grp_diag, a.p_rango_edad, b.nombre as rango_edad
                                  from  pfc_participante_grupo_diag a, pfc_parametro b
                                  where a.p_rango_edad = b.cod_parametro (+);                        
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_LISTAR_PARTICIPANTES]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_LISTAR_PARTICIPANTES;
   
    /* AUTHOR  : Jose.Reyes/MDP*/
   /* CREATED : 02/07/2019*/
   /* PURPOSE : Elimina Detalle de Examen Medico    */
   PROCEDURE SP_PFC_D_EXAMEN_MED_DET (
      PN_COD_EXAMEN_MED  IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'ELIMINACION EXITOSA !';
      
      DELETE FROM PFC_EXAMEN_MEDICO_DETALLE WHERE COD_EXAMEN_MED = PN_COD_EXAMEN_MED;   
      COMMIT;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_D_EXAMEN_MED_DET]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_D_EXAMEN_MED_DET;
   
   /* AUTHOR  : Jose.Reyes/MDP*/
   /* CREATED : 02/07/2019*/
   /* PURPOSE : Registro de Participantes    */
   PROCEDURE SP_PFC_I_U_PARTICIPANTES (
      PN_COD_PARTICIPANTE IN                 NUMBER,
      PN_COD_USUARIO      IN                 NUMBER,
      PV_ESTADO_PARTICIPANTE  IN             VARCHAR2,
      PV_CMP_MEDICO           IN             VARCHAR2,
      PV_NOMBRE_FIRMA         IN             VARCHAR2,
      PN_COD_ROL              IN             NUMBER,
      PN_COD_ARCHIVO_FIRMA    IN             NUMBER,
      PV_CORREO_ELECTRONICO   IN             VARCHAR2,
      PV_NOMBRES              IN             VARCHAR2,
      PV_APELLIDOS            IN             VARCHAR2,
      PN_P_ESTADO             IN             NUMBER,
      PN_COORDINADOR          IN             NUMBER,
      PN_COD_RESULTADO   OUT                 NUMBER,
      PV_MSG_RESULTADO   OUT                 VARCHAR2,
      PN_CODIGO_PARTICIPANTE   OUT           NUMBER
   ) AS
      V_COD_PARTICIPANTE NUMBER;
   BEGIN
      IF ( PN_COD_PARTICIPANTE IS NULL ) THEN
         SELECT
           NVL( MAX(COD_PARTICIPANTE),0)
         INTO V_COD_PARTICIPANTE
         FROM
            PFC_PARTICIPANTE;

         V_COD_PARTICIPANTE   := V_COD_PARTICIPANTE + 1;
         PN_CODIGO_PARTICIPANTE :=V_COD_PARTICIPANTE;
         INSERT INTO PFC_PARTICIPANTE (
            COD_PARTICIPANTE,
            COD_PARTICIPANTE_LARGO,
            COD_USUARIO,
            ESTADO_PARTICIPANTE,
            CMP_MEDICO,
            NOMBRE_FIRMA,
            COD_ROL,
            CODIGO_ARCHIVO_FIRMA,
            CORREO_ELECTRONICO,
            NOMBRES,
            APELLIDOS,
            P_ESTADO,
            COORDINADOR
         ) VALUES (
            V_COD_PARTICIPANTE,
            LPAD(
               V_COD_PARTICIPANTE,
               5,
               '0'
            ),
            PN_COD_USUARIO,
            '1',
            PV_CMP_MEDICO,
            PV_NOMBRE_FIRMA,
            PN_COD_ROL,
            PN_COD_ARCHIVO_FIRMA,
            PV_CORREO_ELECTRONICO,
            PV_NOMBRES,
            PV_APELLIDOS,
            PN_P_ESTADO,
            PN_COORDINADOR
         );

         PN_COD_RESULTADO       := 0;
         PV_MSG_RESULTADO       := 'Registro Exitoso .!';
      ELSE
         UPDATE PFC_PARTICIPANTE
         SET
            COD_USUARIO = PN_COD_USUARIO,
            CMP_MEDICO = PV_CMP_MEDICO,
            NOMBRE_FIRMA = PV_NOMBRE_FIRMA,
            COD_ROL = PN_COD_ROL,
            CODIGO_ARCHIVO_FIRMA = PN_COD_ARCHIVO_FIRMA,
            CORREO_ELECTRONICO = PV_CORREO_ELECTRONICO,
            NOMBRES = PV_NOMBRES,
            APELLIDOS = PV_APELLIDOS,
            P_ESTADO = PN_P_ESTADO,
            COORDINADOR = PN_COORDINADOR
         WHERE
            COD_PARTICIPANTE = PN_COD_PARTICIPANTE;

         PN_CODIGO_PARTICIPANTE := PN_COD_PARTICIPANTE;
         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Registro Actualizado .!';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_I_U_PARTICIPANTES]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
    END SP_PFC_I_U_PARTICIPANTES;
    
    /* AUTHOR  : Jose.Reyes/MDP*/
   /* CREATED : 02/07/2019*/
   /* PURPOSE : Registro de Participantes -> Grupos Diagnosticos   */
   PROCEDURE SP_PFC_I_U_PARTICIPANTES_GRUPO (
      PN_COD_PARTICIPANTE IN                 NUMBER,
      PN_P_RANGO_EDAD     IN                 NUMBER,
      PV_COD_GRP_DIAG     IN                 VARCHAR2,
      PN_COD_RESULTADO   OUT                 NUMBER,
      PV_MSG_RESULTADO   OUT                 VARCHAR2
   ) AS
      V_COD_PARTICIPANTE_GRUPO NUMBER;
   BEGIN
   
            SELECT
            COUNT(*)
         INTO V_COD_PARTICIPANTE_GRUPO
         FROM
            PFC_PARTICIPANTE_GRUPO_DIAG
         WHERE  COD_PARTICIPANTE =  PN_COD_PARTICIPANTE
                 AND COD_GRP_DIAG = PV_COD_GRP_DIAG;
   
      IF ( V_COD_PARTICIPANTE_GRUPO = 0 ) THEN
      
    
         
                 INSERT INTO PFC_PARTICIPANTE_GRUPO_DIAG (
                    COD_PARTICIPANTE,
                    COD_GRP_DIAG,
                    P_RANGO_EDAD
                 ) VALUES (
                    PN_COD_PARTICIPANTE,
                    PV_COD_GRP_DIAG,
                    PN_P_RANGO_EDAD
                 );
        
                 PN_COD_RESULTADO       := 0;
                 PV_MSG_RESULTADO       := 'Registro Exitoso .!';
            
      ELSE
         UPDATE PFC_PARTICIPANTE_GRUPO_DIAG
         SET
            P_RANGO_EDAD = PN_P_RANGO_EDAD
         WHERE
            COD_PARTICIPANTE = PN_COD_PARTICIPANTE
            AND COD_GRP_DIAG = PV_COD_GRP_DIAG ;

         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Registro Actualizado .!';
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_I_U_PARTICIPANTES_GRUPO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
    END SP_PFC_I_U_PARTICIPANTES_GRUPO;
    
    PROCEDURE SP_PFC_I_CHKLIST_INDICACION (
      PN_COD_CHKLIST_INDI       IN                    NUMBER,
      PN_COD_CHKLIST_INDI_LARGO IN                    VARCHAR2,
      PV_DESCRIPCION            IN                    VARCHAR2,
      PN_P_ESTADO               IN                    NUMBER,
      PN_COD_MAC                IN                    NUMBER,
      PN_COD_GRP_DIAG           IN                    NUMBER,
      PV_L_CRITERIO_INCLU       IN                    VARCHAR2,
      PV_L_CRITERIO_EXCLU       IN                    VARCHAR2,
      PN_COD_RESULTADO          OUT                   NUMBER,
      PV_MSG_RESULTADO          OUT                   VARCHAR2
   ) AS
      --FORMA TABLA DE CRITERIOS DE INCLUSION
      PV_COD_CHKLIST_INDI    NUMBER;
      PV_COD_CRITERIO_INCLU    NUMBER;
      PV_COD_CRITERIO_EXCLU    NUMBER;
      CURSOR TC_L_CRITERIO_INCLU IS 
        SELECT
          regexp_substr(criterio_inclu,'([^$]+)',1,1,'',1) AS v_cod_criterio_inclu,
          regexp_substr(criterio_inclu,'([^$]+)',1,2,'',1) AS v_cod_chklist_indi,
          regexp_substr(criterio_inclu,'([^$]+)',1,3,'',1) AS v_cod_criterio_inclu_largo,
          regexp_substr(criterio_inclu,'([^$]+)',1,4,'',1) AS v_descripcion,
          regexp_substr(criterio_inclu,'([^$]+)',1,5,'',1) AS v_orden,
          regexp_substr(criterio_inclu,'([^$]+)',1,6,'',1) AS v_p_estado,
          regexp_substr(criterio_inclu,'([^$]+)',1,7,'',1) AS v_cod_mac,
          regexp_substr(criterio_inclu,'([^$]+)',1,8,'',1) AS v_cod_grp_diag
        FROM (
        SELECT regexp_substr(PV_L_CRITERIO_INCLU,'[^|]+',1,level) 
        AS criterio_inclu 
        FROM dual CONNECT BY regexp_substr(PV_L_CRITERIO_INCLU,'[^|]+',1,level) IS NOT NULL);
        
        --FORMA TABLA DE CRITERIOS DE EXCLUSION
        CURSOR TC_L_CRITERIO_EXCLU IS 
        SELECT
          regexp_substr(criterio_exclu,'([^$]+)',1,1,'',1) AS v_cod_criterio_exclu,
          regexp_substr(criterio_exclu,'([^$]+)',1,2,'',1) AS v_cod_chklist_indi,
          regexp_substr(criterio_exclu,'([^$]+)',1,3,'',1) AS v_cod_criterio_inclu_largo,
          regexp_substr(criterio_exclu,'([^$]+)',1,4,'',1) AS v_descripcion,
          regexp_substr(criterio_exclu,'([^$]+)',1,5,'',1) AS v_orden,
          regexp_substr(criterio_exclu,'([^$]+)',1,6,'',1) AS v_p_estado,
          regexp_substr(criterio_exclu,'([^$]+)',1,7,'',1) AS v_cod_mac,
          regexp_substr(criterio_exclu,'([^$]+)',1,8,'',1) AS v_cod_grp_diag
        FROM (
        SELECT regexp_substr(PV_L_CRITERIO_EXCLU,'[^|]+',1,level) 
        AS criterio_exclu 
        FROM dual CONNECT BY regexp_substr(PV_L_CRITERIO_EXCLU,'[^|]+',1,level) IS NOT NULL);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'NO REGISTRO';
      
      PV_COD_CHKLIST_INDI := PN_COD_CHKLIST_INDI;
      --SI ES NO NULL REGISTRA --> DATA FULL
      IF (PV_COD_CHKLIST_INDI IS NULL) THEN
         SELECT SQ_PFC_CHKLST_INDI_COD_CHKLST.nextval INTO PV_COD_CHKLIST_INDI FROM DUAL;
         
         INSERT INTO PFC_CHKLIST_INDICACION VALUES (
         PV_COD_CHKLIST_INDI, 
         PN_COD_CHKLIST_INDI_LARGO,
         PV_DESCRIPCION,
         PN_P_ESTADO,
         PN_COD_MAC,
         PN_COD_GRP_DIAG,
         SYSDATE,
         null);
         
         IF PV_L_CRITERIO_INCLU IS NOT NULL THEN
            FOR v_item IN TC_L_CRITERIO_INCLU LOOP
                SELECT SQ_PFC_CRIT_INC_COD_CRIT_INC.nextval INTO PV_COD_CRITERIO_INCLU  FROM DUAL; 
            
                INSERT INTO PFC_CRITERIO_INCLUSION VALUES (
                PV_COD_CRITERIO_INCLU,
                PV_COD_CHKLIST_INDI,
                lpad(v_item.v_orden, 2, 0),
                v_item.v_descripcion,
                v_item.v_orden,
                v_item.v_p_estado,
                v_item.v_cod_mac,
                v_item.v_cod_grp_diag);
            END LOOP;
         END IF;
         
         IF PV_L_CRITERIO_EXCLU IS NOT NULL THEN
            FOR v_item IN TC_L_CRITERIO_EXCLU LOOP
                SELECT SQ_PFC_CRIT_EXC_COD_CRIT_EXC.nextval INTO PV_COD_CRITERIO_EXCLU  FROM DUAL; 
            
                INSERT INTO PFC_CRITERIO_EXCLUSION VALUES (
                PV_COD_CRITERIO_EXCLU,
                PV_COD_CHKLIST_INDI,
                lpad(v_item.v_orden, 2, 0),
                v_item.v_descripcion,
                v_item.v_orden,
                v_item.v_p_estado,
                v_item.v_cod_mac,
                v_item.v_cod_grp_diag);
            END LOOP;
         END IF;
        PN_COD_RESULTADO      := 0;
        PV_MSG_RESULTADO      := 'Registro Exitoso!';
        
      ELSE
        IF (PN_P_ESTADO = ONTPFC.PCK_PFC_CONSTANTE.PN_ESTADO_VIGENTE_INDICACION) THEN
            UPDATE PFC_CHKLIST_INDICACION SET 
            DESCRIPCION = PV_DESCRIPCION,
            P_ESTADO = PN_P_ESTADO,
            COD_MAC = PN_COD_MAC,
            COD_GRP_DIAG = PN_COD_GRP_DIAG
            WHERE COD_CHKLIST_INDI = PN_COD_CHKLIST_INDI AND COD_MAC = PN_COD_MAC AND COD_GRP_DIAG = PN_COD_GRP_DIAG;
        ELSE
            UPDATE PFC_CHKLIST_INDICACION SET 
            DESCRIPCION = PV_DESCRIPCION,
            P_ESTADO = PN_P_ESTADO,
            COD_MAC = PN_COD_MAC,
            COD_GRP_DIAG = PN_COD_GRP_DIAG,
            FECHA_FIN_VIGENCIA = SYSDATE
            WHERE COD_CHKLIST_INDI = PN_COD_CHKLIST_INDI AND COD_MAC = PN_COD_MAC AND COD_GRP_DIAG = PN_COD_GRP_DIAG;
        END IF;
        
        IF PV_L_CRITERIO_INCLU IS NOT NULL THEN
            FOR v_item IN TC_L_CRITERIO_INCLU LOOP
                IF (v_item.v_cod_criterio_inclu = 0) THEN
                    SELECT SQ_PFC_CRIT_INC_COD_CRIT_INC.nextval INTO PV_COD_CRITERIO_INCLU  FROM DUAL;
                
                    INSERT INTO PFC_CRITERIO_INCLUSION VALUES (
                    PV_COD_CRITERIO_INCLU,
                    PN_COD_CHKLIST_INDI,
                    lpad(v_item.v_orden, 2, 0),
                    v_item.v_descripcion,
                    v_item.v_orden,
                    v_item.v_p_estado,
                    v_item.v_cod_mac,
                    v_item.v_cod_grp_diag);
                ELSE
                    UPDATE PFC_CRITERIO_INCLUSION SET
                    DESCRIPCION = v_item.v_descripcion,
                    ORDEN = v_item.v_orden,
                    P_ESTADO = v_item.v_p_estado,
                    COD_MAC = v_item.v_cod_mac,
                    COD_GRP_DIAG = v_item.v_cod_grp_diag
                    WHERE COD_CRITERIO_INCLU = v_item.v_cod_criterio_inclu;
                END IF;
            END LOOP;
        END IF;
        
        IF PV_L_CRITERIO_EXCLU IS NOT NULL THEN
            FOR v_item IN TC_L_CRITERIO_EXCLU LOOP
                IF (v_item.v_cod_criterio_exclu = 0) THEN
                    SELECT SQ_PFC_CRIT_EXC_COD_CRIT_EXC.nextval INTO PV_COD_CRITERIO_EXCLU  FROM DUAL; 
                
                    INSERT INTO PFC_CRITERIO_EXCLUSION VALUES (
                    PV_COD_CRITERIO_EXCLU,
                    PN_COD_CHKLIST_INDI,
                    lpad(v_item.v_orden, 2, 0),
                    v_item.v_descripcion,
                    v_item.v_orden,
                    v_item.v_p_estado,
                    v_item.v_cod_mac,
                    v_item.v_cod_grp_diag);
                ELSE
                    UPDATE PFC_CRITERIO_EXCLUSION SET
                    DESCRIPCION = v_item.v_descripcion,
                    ORDEN = v_item.v_orden,
                    P_ESTADO = v_item.v_p_estado,
                    COD_MAC = v_item.v_cod_mac,
                    COD_GRP_DIAG = v_item.v_cod_grp_diag
                    WHERE 
                    COD_CRITERIO_EXCLU = v_item.v_cod_criterio_exclu;
                END IF;
            END LOOP;
        END IF;
        PN_COD_RESULTADO      := 0;
        PV_MSG_RESULTADO      := 'Actualización Exitosa!!';
      
      END IF;
      COMMIT;
      
      EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_I_CHKLIST_INDICACION]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
         ROLLBACK;
   END SP_PFC_I_CHKLIST_INDICACION;
   
   
   /* AUTHOR  : Jose.Reyes/MDP*/
   /* CREATED : 08/07/2019*/
   /* PURPOSE : Elimina Detalle Grupo de Participante    */
   PROCEDURE SP_PFC_D_GRUPO_PARTICPANTES (
      PN_COD_PARTICIPANTE  IN                 NUMBER,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'ELIMINACION EXITOSA !';
      
      DELETE FROM PFC_PARTICIPANTE_GRUPO_DIAG WHERE COD_PARTICIPANTE = PN_COD_PARTICIPANTE;   
      COMMIT;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_D_GRUPO_PARTICPANTES]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_D_GRUPO_PARTICPANTES;
  
   
  -----------------------------------------------------------------------------------------
   PROCEDURE SP_PFC_S_VALIDAR_R_PARTICIPA (
      PN_COD_GRUPO_DIAG   IN                 NUMBER,
      PN_COD_RANGO_EDAD   IN                 NUMBER,
      PN_COD_RESULTADO   OUT                 NUMBER,
      PV_MSG_RESULTADO   OUT                 VARCHAR2
   ) AS
   
   V_CANT NUMBER;
   BEGIN
     
     SELECT COUNT(PGD.COD_PARTICIPANTE)
       INTO V_CANT
       FROM PFC_PARTICIPANTE_GRUPO_DIAG PGD
      WHERE  (PGD.COD_GRP_DIAG = PN_COD_GRUPO_DIAG AND PGD.P_RANGO_EDAD = PN_COD_RANGO_EDAD) OR
             (PGD.COD_GRP_DIAG = PN_COD_GRUPO_DIAG AND PCK_PFC_CONSTANTE.PN_RANGO_TODO = PN_COD_RANGO_EDAD) OR
             (PGD.COD_GRP_DIAG = PN_COD_GRUPO_DIAG AND PGD.P_RANGO_EDAD =PCK_PFC_CONSTANTE.PN_RANGO_TODO);
        
     IF V_CANT > 0 THEN
         PN_COD_RESULTADO:=1;
         PV_MSG_RESULTADO:='Ya se tiene un participante asignado con el mismo rango de edad.';
     ELSE
         PN_COD_RESULTADO:=0;
         PV_MSG_RESULTADO:='Correcto.';
     END IF; 
   
   END SP_PFC_S_VALIDAR_R_PARTICIPA;

END PCK_PFC_CONFIGURACION_SISTEMA;

/

--------------------------------------------------------
--  DDL for Package Body PCK_PFC_INDICADORES
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "ONTPFC"."PCK_PFC_INDICADORES" 
AS
  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 14/05/2019
  -- PURPOSE : Reporte Indicadores de Proceso

  PROCEDURE SP_PFC_S_INDICADORES_PROCESO (PN_FEC_MES     IN NUMBER,
                    PN_FEC_ANO     IN NUMBER,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2) AS
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    IF PN_FEC_MES IS NOT NULL  AND PN_FEC_ANO IS NOT NULL THEN
      FOR itemIndicador IN ( 
-- Indicador 001
-- Cantidad de MAC solicitadas en el mes y año 
      SELECT '001' AS Indicador , NULL AS Cantidad,
      (SELECT COUNT(*) FROM
      (SELECT DISTINCT P.COD_MAC
      FROM PFC_SOLICITUD_EVALUACION A
      INNER JOIN PFC_SOLICITUD_PRELIMINAR P
      ON A.COD_SOL_PRE = P.COD_SOL_PRE
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC))) AS Numerador,
      (SELECT count(distinct(trim(c.cod_afi_paciente))) 
      FROM PFC_SOLICITUD_EVALUACION A 
      INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
      INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
      '' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 002  
-- Porcentaje de solicitudes según estado final (APROBADO )    
      SELECT '002' AS Indicador , NULL AS Cantidad,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
      'APROBADO' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 002  
-- Porcentaje de solicitudes según estado final (RECHAZADO )     
      SELECT '002' AS Indicador , NULL AS Cantidad,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
       AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Numerador,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
      'RECHAZADO' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 003
-- Tiempo promedio (días) de resolución de solicitudes según estado (APROBADO )      
      SELECT '003' AS Indicador , NULL AS Cantidad,
      (SELECT NVL(SUM(TRUNC(A.FEC_FINALIZAR_ESTADO - B.FEC_SOL_PRE)), 0)
      FROM PFC_SOLICITUD_EVALUACION A 
      INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
      'APROBADO' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 003
-- Tiempo promedio (días) de resolución de solicitudes según estado (RECHAZADO )     
      SELECT '003' AS Indicador , NULL AS Cantidad,
      (SELECT NVL(SUM(TRUNC(A.FEC_FINALIZAR_ESTADO - B.FEC_SOL_PRE)), 0)
      FROM PFC_SOLICITUD_EVALUACION A 
      INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
       AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Numerador,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
      'RECHAZADO' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 004
-- Tiempo máximo (días) de resolución de solicitudes  según estado (APROBADO )  
      SELECT '004' AS Indicador,
      (SELECT NVL(MAX(TRUNC(A.FEC_FINALIZAR_ESTADO - B.FEC_SOL_PRE)), 0)
      FROM PFC_SOLICITUD_EVALUACION A 
      INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Cantidad,
      NULL AS Numerador,
      NULL AS Denominador,
      'APROBADO' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 004
-- Tiempo máximo (días) de resolución de solicitudes  según estado (RECHAZADO ) 
      SELECT '004' AS Indicador,
      (SELECT NVL(MAX(TRUNC(A.FEC_FINALIZAR_ESTADO - B.FEC_SOL_PRE)), 0)
      FROM PFC_SOLICITUD_EVALUACION A 
      INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Cantidad,
      NULL AS Numerador,
      NULL AS Denominador,
      'RECHAZADO' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 005
-- Tiempo mínimo (días) de resolución de solicitudes  según estado (APROBADO )      
      SELECT '005' AS Indicador,
      (SELECT NVL(MIN(TRUNC(A.FEC_FINALIZAR_ESTADO - B.FEC_SOL_PRE)), 0)
      FROM PFC_SOLICITUD_EVALUACION A 
      INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Cantidad,
      NULL AS Numerador,
      NULL AS Denominador,
      'APROBADO' AS Quiebre
      FROM DUAL
     
      UNION
-- Indicador 005
-- Tiempo máximo (días) de resolución de solicitudes  según estado (RECHAZADO )     
      SELECT '005' AS Indicador,
      (SELECT NVL(MIN(TRUNC(A.FEC_FINALIZAR_ESTADO - B.FEC_SOL_PRE)), 0)
      FROM PFC_SOLICITUD_EVALUACION A 
      INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Cantidad,
      NULL AS Numerador,
      NULL AS Denominador,
      'RECHAZADO' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 006
-- Porcentaje de solicitudes aprobadas por instancias(Autorizador de Pertinencias)     
      SELECT '006' AS Indicador , NULL AS Cantidad,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ)) AS Numerador,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
      'AUTORIZADOR DE PERTINENCIAS' AS Quiebre
      FROM DUAL
      UNION
-- Indicador 006
-- Porcentaje de solicitudes aprobadas por instancias(Líder tumor)     
      SELECT '006' AS Indicador , NULL AS Cantidad,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T)) AS Numerador,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
      'LÍDER DE TUMOR' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 006
-- Porcentaje de solicitudes aprobadas por instancias(CMAC)     
      SELECT '006' AS Indicador , NULL AS Cantidad,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
      'CMAC' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 007
-- Porcentaje de solicitudes rechazadas  por instancias(Autorizador de Pertinencias)     
      SELECT '007' AS Indicador , NULL AS Cantidad,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ)) AS Numerador,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
      'AUTORIZADOR DE PERTINENCIAS' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 007
-- Porcentaje de solicitudes rechazadas por instancias(Líder tumor)     
      SELECT '007' AS Indicador , NULL AS Cantidad,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T)) AS Numerador,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
      'LÍDER DE TUMOR' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 007
-- Porcentaje de solicitudes aprobadas por instancias(CMAC)     
      SELECT '007' AS Indicador , NULL AS Cantidad,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Numerador,
      (SELECT COUNT(*) 
      FROM PFC_SOLICITUD_EVALUACION A 
      WHERE
        EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
        AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
        AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
      'CMAC' AS Quiebre
      FROM DUAL
      
      UNION
-- Indicador 008
-- Porcentaje de solicitudes rechazadas según MAC      
      SELECT '008' AS Indicador , NULL AS Cantidad, NVL(A.Y, 0) AS Numerador, 
        (SELECT COUNT(*)
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador, M.DESCRIPCION AS Quiebre
      FROM PFC_MAC M
      LEFT JOIN (SELECT B.COD_MAC AS X, COUNT(B.COD_MAC) AS Y
        FROM PFC_SOLICITUD_EVALUACION A 
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)
        GROUP BY B.COD_MAC) A ON M.COD_MAC = A.X
      WHERE M.P_ESTADO_MAC = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO

      UNION
-- Indicador 009
-- Porcentaje de solicitudes aprobadas según MAC       
      SELECT '009' AS Indicador , NULL AS Cantidad, NVL(A.Y, 0) AS Numerador,
      (SELECT COUNT(*)
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador, M.DESCRIPCION AS Quiebre
      FROM PFC_MAC M
      LEFT JOIN (SELECT B.COD_MAC AS X, COUNT(B.COD_MAC) AS Y
        FROM PFC_SOLICITUD_EVALUACION A 
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)
        GROUP BY B.COD_MAC) A ON M.COD_MAC = A.X
       WHERE M.P_ESTADO_MAC = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
      
      UNION
-- Indicador 010
-- Porcentaje de solicitudes rechazadas según médico tratante 
      SELECT '010' AS Indicador , NULL AS Cantidad, NVL(A.Y, 0) AS Numerador, NVL(B.Y, 0) AS Denominador, ( B.X || ' - ' || B.Z )AS Quiebre
      FROM (SELECT C.CMP_MEDICO AS X, COUNT(C.CMP_MEDICO) AS Y, TRIM(C.MEDICO_TRATANTE_PRESCRIPTOR) AS Z
        FROM PFC_SOLICITUD_EVALUACION A 
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)
        GROUP BY C.CMP_MEDICO, TRIM(C.MEDICO_TRATANTE_PRESCRIPTOR)) B
      LEFT JOIN (SELECT C.CMP_MEDICO AS X, COUNT(C.CMP_MEDICO) AS Y
        FROM PFC_SOLICITUD_EVALUACION A 
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)
        GROUP BY C.CMP_MEDICO) A ON A.X = B.X
        
        UNION
-- Indicador 011
-- Porcentaje de solicitudes aprobadas según médico tratante  
      SELECT '011' AS Indicador , NULL AS Cantidad, NVL(A.Y, 0) AS Numerador, NVL(B.Y, 0) AS Denominador, ( B.X || ' - ' || B.Z )AS Quiebre
      FROM (SELECT C.CMP_MEDICO AS X, COUNT(C.CMP_MEDICO) AS Y, TRIM(C.MEDICO_TRATANTE_PRESCRIPTOR) AS Z
        FROM PFC_SOLICITUD_EVALUACION A 
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)
        GROUP BY C.CMP_MEDICO, TRIM(C.MEDICO_TRATANTE_PRESCRIPTOR)) B
      LEFT JOIN (SELECT C.CMP_MEDICO AS X, COUNT(C.CMP_MEDICO) AS Y
        FROM PFC_SOLICITUD_EVALUACION A 
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)
        GROUP BY C.CMP_MEDICO) A ON A.X = B.X

        UNION
-- Indicador 012
-- Porcentaje de MAC aprobadas según rango de edad (década : 0_10)
        SELECT '012' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          C.EDAD_PACIENTE BETWEEN 0 AND 10
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        '0-10' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 012
-- Porcentaje de MAC aprobadas según rango de edad (década : 11_20)
        SELECT '012' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          C.EDAD_PACIENTE BETWEEN 11 AND 20
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        '11-20' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 012
-- Porcentaje de MAC aprobadas según rango de edad (década : 21_30)
        SELECT '012' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          C.EDAD_PACIENTE BETWEEN 21 AND 30
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        '21-30' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 012
-- Porcentaje de MAC aprobadas según rango de edad (década : 31_40)
        SELECT '012' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          C.EDAD_PACIENTE BETWEEN 31 AND 40
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        '31-40' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 012
-- Porcentaje de MAC aprobadas según rango de edad (década : 41_50)
        SELECT '012' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          C.EDAD_PACIENTE BETWEEN 41 AND 50
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        '41-50' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 012
-- Porcentaje de MAC aprobadas según rango de edad (década : 51_60)
        SELECT '012' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          C.EDAD_PACIENTE BETWEEN 51 AND 60
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        '51-60' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 012
-- Porcentaje de MAC aprobadas según rango de edad (década : 61_70)
        SELECT '012' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          C.EDAD_PACIENTE BETWEEN 61 AND 70
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        '61-70' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 012
-- Porcentaje de MAC aprobadas según rango de edad (década : 71_80)
        SELECT '012' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          C.EDAD_PACIENTE BETWEEN 71 AND 80
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        '71-80' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 012
-- Porcentaje de MAC aprobadas según rango de edad (década : 81_90)
        SELECT '012' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          C.EDAD_PACIENTE BETWEEN 81 AND 90
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        '81-90' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 012
-- Porcentaje de MAC aprobadas según rango de edad (década : 91_100)
        SELECT '012' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
        INNER JOIN PFC_SOLBEN C ON B.COD_SCG = C.COD_SCG
        WHERE
          C.EDAD_PACIENTE BETWEEN 91 AND 100
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        '91-100' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 013
-- Porcentaje de cumplimiento de preferencias institucionales
        SELECT '013' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        INNER JOIN PFC_MEDICAMENTO_NUEVO B ON A.COD_SOL_EVA = B.COD_SOL_EVA
        INNER JOIN PFC_SEV_LINEA_TRATAMIENTO C ON B.COD_SEV_LIN_TRA = C.COD_SEV_LIN_TRA
        WHERE
          C.P_CUMPLE_PREF_INST = 1
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        NULL AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 014
-- Porcentaje de aprobaciones según tipo de indicación MAC
        SELECT '014' AS Indicador, NULL AS Cantidad, NVL(A.Y, 0) AS Numerador,
       (SELECT COUNT(*)
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                         PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)) AS Denominador,
        (M.DESCRIPCION || ': ' || I.DESCRIPCION) AS Quiebre
        FROM PFC_CHKLIST_INDICACION I
        INNER JOIN PFC_MAC M ON I.COD_MAC = M.COD_MAC
        LEFT JOIN 
          (SELECT C.COD_CHKLIST_INDI AS X, COUNT(C.COD_CHKLIST_INDI) AS Y
          FROM PFC_SOLICITUD_EVALUACION A
          INNER JOIN PFC_MEDICAMENTO_NUEVO B ON A.COD_SOL_EVA = B.COD_SOL_EVA
          INNER JOIN PFC_SEV_CHECKLIST_PAC C ON B.COD_SEV_CHK_PAC = C.COD_SEV_CHKLIST_PAC
          WHERE
            EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
            AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
            AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC)
          GROUP BY C.COD_CHKLIST_INDI) A ON I.COD_CHKLIST_INDI = A.X
        WHERE M.P_ESTADO_MAC = PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO
        
        UNION
-- Indicador 015
-- Porcentaje de solicitudes según condición (nuevo)  
        SELECT '015' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        WHERE
          A.P_TIPO_EVA = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_NUEVO
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
        'NUEVO' AS Quiebre
        FROM DUAL

        UNION
-- Indicador 015
-- Porcentaje de solicitudes según condición (continuador)  
        SELECT '015' AS Indicador , NULL AS Cantidad,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A
        WHERE
          A.P_TIPO_EVA = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_CONTINUADOR
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Numerador,
        (SELECT COUNT(*) 
        FROM PFC_SOLICITUD_EVALUACION A 
        WHERE
          EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
        'CONTINUADOR' AS Quiebre
        FROM DUAL
        
        UNION
-- Indicador 016
-- Tiempo promedio de evaluación según condición (Medicamento Nuevo)    
        SELECT '016' AS Indicador , NULL AS Cantidad,
          (SELECT NVL(SUM(TRUNC(A.FEC_FINALIZAR_ESTADO - B.FEC_SOL_PRE)), 0)
          FROM PFC_SOLICITUD_EVALUACION A 
          INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
          WHERE
          A.P_TIPO_EVA = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_NUEVO
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Numerador,
          (SELECT COUNT(*) 
          FROM PFC_SOLICITUD_EVALUACION A 
          WHERE
            EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
            AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
            AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                             PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                             PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
          'NUEVO' AS Quiebre
        FROM DUAL

        UNION
-- Indicador 016
-- Tiempo promedio de evaluación según condición (CONTINUADOR)
        SELECT '016' AS Indicador , NULL AS Cantidad,
          (SELECT NVL(SUM(TRUNC(A.FEC_FINALIZAR_ESTADO - B.FEC_SOL_PRE)), 0)
          FROM PFC_SOLICITUD_EVALUACION A 
          INNER JOIN PFC_SOLICITUD_PRELIMINAR B ON A.COD_SOL_PRE = B.COD_SOL_PRE
          WHERE
          A.P_TIPO_EVA = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_CONTINUADOR
          AND EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
          AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
          AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                           PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Numerador,
          (SELECT COUNT(*) 
          FROM PFC_SOLICITUD_EVALUACION A 
          WHERE
            EXTRACT ( MONTH FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
            AND EXTRACT ( YEAR FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
            AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                             PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                             PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)) AS Denominador,
          'CONTINUADOR' AS Quiebre
        FROM DUAL
      ) 

    LOOP
        
      INSERT INTO PFC_INDICADORES (INDICADOR, CANTIDAD, NUMERADOR, DENOMINADOR, QUIEBRE, FEC_MES , FEC_ANO , 
      USUARIO_CREACION, FECHA_CREACION ) VALUES (
        itemIndicador.INDICADOR, 
        (CASE
        WHEN itemIndicador.CANTIDAD IS NULL THEN 
          (CASE itemIndicador.DENOMINADOR
            WHEN 0 THEN 0
            ELSE (CASE 
                    WHEN itemIndicador.INDICADOR IN (PCK_PFC_CONSTANTE.PV_TRES, PCK_PFC_CONSTANTE.PV_CUATRO,
                      PCK_PFC_CONSTANTE.PV_CINCO, PCK_PFC_CONSTANTE.PV_DIECISEIS) THEN ROUND((itemIndicador.NUMERADOR/itemIndicador.DENOMINADOR), 2)
                    ELSE ROUND((itemIndicador.NUMERADOR/itemIndicador.DENOMINADOR), 4)*100
                  END)
          END)
        ELSE itemIndicador.CANTIDAD
        END), 
        itemIndicador.NUMERADOR, 
        itemIndicador.DENOMINADOR,
        itemIndicador.QUIEBRE, 
        PN_FEC_MES, 
        PN_FEC_ANO , 
        'ADMIN' , 
        SYSDATE) ;
      
    END LOOP; 
        commit;
    
      PN_COD_RESULTADO := 0;
      PV_MSG_RESULTADO := 'Consulta exitosa';
    ELSE
      PN_COD_RESULTADO := 1;
      PV_MSG_RESULTADO := 'MES ó AÑO son Nulos';
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      PN_COD_RESULTADO := -1;
      PV_MSG_RESULTADO := '[[SP_PFC_S_INDICADORES_PROCESO]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END SP_PFC_S_INDICADORES_PROCESO;

  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 14/05/2019
  -- PURPOSE : Validar Indicadores de Proceso Por MES y AÑO
  PROCEDURE SP_PFC_S_VALIDAR_INDICADORES (PN_FEC_MES     IN NUMBER,
                    PN_FEC_ANO     IN NUMBER,
                                           PN_CAN_REGISTROS   OUT NUMBER,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2) AS
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    IF PN_FEC_MES IS NOT NULL  AND PN_FEC_ANO IS NOT NULL THEN

       SELECT count(*) INTO PN_CAN_REGISTROS FROM PFC_INDICADORES 
      WHERE FEC_MES = PN_FEC_MES AND FEC_ANO = PN_FEC_ANO;

      PN_COD_RESULTADO := 0;
      PV_MSG_RESULTADO := 'Consulta exitosa';
    ELSE
      PN_COD_RESULTADO := 1;
      PV_MSG_RESULTADO := 'MES ó AÑO son Nulos';
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      PN_COD_RESULTADO := -1;
      PV_MSG_RESULTADO := '[[SP_PFC_S_VALIDAR_INDICADORES]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END SP_PFC_S_VALIDAR_INDICADORES;

  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 15/05/2019
  -- PURPOSE : Consulta de Indicador de Proceso Por MES y AÑO
  PROCEDURE SP_PFC_S_INDICADORES_PROC_XLS (PN_FEC_MES     IN NUMBER,
                    PN_FEC_ANO     IN NUMBER,
                                           OP_OBJCURSOR       OUT SYS_REFCURSOR,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2) AS
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    IF PN_FEC_MES IS NOT NULL  AND PN_FEC_ANO IS NOT NULL THEN
      OPEN OP_OBJCURSOR FOR
           SELECT INDICADOR, CANTIDAD, NUMERADOR, DENOMINADOR, (P.NOMBRE || (CASE WHEN QUIEBRE IS NULL THEN '' ELSE ' : ' END) || QUIEBRE) AS QUIEBRE, FEC_ANO, FEC_MES 
           FROM PFC_INDICADORES I
           INNER JOIN PFC_PARAMETRO P ON P.COD_GRUPO = 71 AND I.INDICADOR = P.CODIGO  
           WHERE FEC_MES = PN_FEC_MES AND FEC_ANO = PN_FEC_ANO
           ORDER BY INDICADOR, QUIEBRE;
      PN_COD_RESULTADO := 0;
      PV_MSG_RESULTADO := 'Consulta exitosa';
    ELSE
      PN_COD_RESULTADO := 1;
      PV_MSG_RESULTADO := 'MES ó AÑO son Nulos';
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      PN_COD_RESULTADO := -1;
      PV_MSG_RESULTADO := '[[SP_PFC_S_INDICADORES_PROC_XLS]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END SP_PFC_S_INDICADORES_PROC_XLS;


  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 28/06/2019
  -- PURPOSE : Consulta de Indicador de Consumo Por MES y AÑO
  PROCEDURE SP_PFC_S_INDICADORES_CONS_XLS ( PN_FEC_MES     IN NUMBER,
                                            PN_FEC_ANO     IN NUMBER,
                                            PN_TIPO         IN NUMBER,
                                            OP_OBJCURSOR       OUT SYS_REFCURSOR,
                                            PN_COD_RESULTADO   OUT NUMBER,
                                            PV_MSG_RESULTADO   OUT VARCHAR2) AS
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    IF PN_FEC_MES IS NOT NULL  AND PN_FEC_ANO IS NOT NULL THEN

      -- Query Reporte de Consumo Por MAC
      IF PN_TIPO = 1 THEN
          OPEN OP_OBJCURSOR FOR
               select A.cod_mac , D.descripcion, sum ( a.gasto_ultimo_consumo ) As GASTO_TOTAL,
                    count ( a.cod_evolucion ) As NRO_PACIENTES,
                      sum ( a.gasto_ultimo_consumo ) / count ( a.cod_evolucion )  as G_PAC,
                      COUNT(CASE WHEN C.p_tipo_eva = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_NUEVO  THEN 1 END) as NRO_NUEVOS,
                    COUNT(CASE WHEN C.p_tipo_eva = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_CONTINUADOR  THEN 1 END) as NRO_CONTINUADOR
                    from pfc_evolucion A, pfc_monitoreo B , pfc_solicitud_evaluacion C , pfc_mac D
                    Where A.cod_monitoreo = B.cod_monitoreo
                          AND B.cod_sol_eva = C.cod_sol_eva
                          AND A.cod_mac = D.cod_mac
                          AND EXTRACT(Year FROM A.fec_ultimo_consumo ) = PN_FEC_ANO
                          AND EXTRACT(Month FROM A.fec_ultimo_consumo ) = PN_FEC_MES
                    group by A.cod_mac , D.descripcion    ;   
      END IF;

      -- Query Reporte de Consumo Por Grupo Diagnostico y MAC
      IF PN_TIPO = 2 THEN
          OPEN OP_OBJCURSOR FOR
               select F.cod_grp_diag, A.cod_mac , D.descripcion, sum ( a.gasto_ultimo_consumo ) As GASTO_TOTAL,
                    count ( a.cod_evolucion ) As NRO_PACIENTES,
                      sum ( a.gasto_ultimo_consumo ) / count ( a.cod_evolucion )  as G_PAC,
                      COUNT(CASE WHEN C.p_tipo_eva = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_NUEVO  THEN 1 END) as NRO_NUEVOS,
                    COUNT(CASE WHEN C.p_tipo_eva = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_CONTINUADOR  THEN 1 END) as NRO_CONTINUADOR
                    from pfc_evolucion A, pfc_monitoreo B , pfc_solicitud_evaluacion C , 
                         pfc_mac D , pfc_solicitud_preliminar E, pfc_solben F
                    Where A.cod_monitoreo = B.cod_monitoreo
                          AND B.cod_sol_eva = C.cod_sol_eva
                          AND A.cod_mac = D.cod_mac
                          AND C.cod_sol_pre = E.cod_sol_pre
                          AND E.cod_scg = F.cod_scg
                          AND EXTRACT(Year FROM A.fec_ultimo_consumo ) = PN_FEC_ANO
                          AND EXTRACT(Month FROM A.fec_ultimo_consumo ) = PN_FEC_MES
                    group by F.cod_grp_diag , A.cod_mac , D.descripcion;   
      END IF;

       -- Query Reporte de Consumo Por Grupo Diagnostico , MAC y Linea de Tratamiento
      IF PN_TIPO = 3 THEN
          OPEN OP_OBJCURSOR FOR
               select F.cod_grp_diag, A.cod_mac , G.linea_trat as LINEA_TRATAMIENTO , D.descripcion, sum ( a.gasto_ultimo_consumo ) As GASTO_TOTAL,
                    count ( a.cod_evolucion ) As NRO_PACIENTES,
                      sum ( a.gasto_ultimo_consumo ) / count ( a.cod_evolucion )  as G_PAC,
                      COUNT(CASE WHEN C.p_tipo_eva = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_NUEVO  THEN 1 END) as NRO_NUEVOS,
                    COUNT(CASE WHEN C.p_tipo_eva = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_CONTINUADOR  THEN 1 END) as NRO_CONTINUADOR
                    from pfc_evolucion A, pfc_monitoreo B , pfc_solicitud_evaluacion C , 
                         pfc_mac D , pfc_solicitud_preliminar E, pfc_solben F, pfc_hist_linea_tratamiento G
                    Where A.cod_monitoreo = B.cod_monitoreo
                          AND B.cod_sol_eva = C.cod_sol_eva
                          AND A.cod_mac = D.cod_mac
                          AND C.cod_sol_pre = E.cod_sol_pre
                          AND E.cod_scg = F.cod_scg
                          AND C.cod_sol_eva = G.cod_sol_eva
                          AND A.cod_mac = G.cod_mac
                          AND EXTRACT(Year FROM A.fec_ultimo_consumo ) = PN_FEC_ANO
                          AND EXTRACT(Month FROM A.fec_ultimo_consumo ) = PN_FEC_MES
                    group by F.cod_grp_diag , A.cod_mac ,  G.linea_trat  , D.descripcion;   
      END IF;

       -- Query Reporte de Consumo Por Grupo Diagnostico , MAC , Linea de Tratamiento y Timepo de Uso
      IF PN_TIPO = 4 THEN
          OPEN OP_OBJCURSOR FOR
               select F.cod_grp_diag, A.cod_mac , G.linea_trat as LINEA_TRATAMIENTO , K.nombre as TIEMPO_USO,
                    D.descripcion, sum ( a.gasto_ultimo_consumo ) As GASTO_TOTAL,
                    count ( a.cod_evolucion ) As NRO_PACIENTES,
                      sum ( a.gasto_ultimo_consumo ) / count ( a.cod_evolucion )  as G_PAC,
                      COUNT(CASE WHEN C.p_tipo_eva = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_NUEVO  THEN 1 END) as NRO_NUEVOS,
                    COUNT(CASE WHEN C.p_tipo_eva = PCK_PFC_CONSTANTE.PN_TIPO_EVALUACION_CONTINUADOR  THEN 1 END) as NRO_CONTINUADOR
                    from pfc_evolucion A, pfc_monitoreo B , pfc_solicitud_evaluacion C , 
                         pfc_mac D , pfc_solicitud_preliminar E, pfc_solben F, pfc_hist_linea_tratamiento G,
                         pfc_medicamento_nuevo H, pfc_sev_analisis_conclusion I, pfc_parametro K
                    Where A.cod_monitoreo = B.cod_monitoreo
                          AND B.cod_sol_eva = C.cod_sol_eva
                          AND A.cod_mac = D.cod_mac
                          AND C.cod_sol_pre = E.cod_sol_pre
                          AND E.cod_scg = F.cod_scg
                          AND C.cod_sol_eva = G.cod_sol_eva
                          AND A.cod_mac = G.cod_mac
                          AND C.cod_sol_eva = H.cod_sol_eva
                          AND H.cod_sev_ana_con = I.cod_sev_ana_con
                          AND I.p_tiempo_uso = K.cod_parametro
                          AND EXTRACT(Year FROM A.fec_ultimo_consumo ) = PN_FEC_ANO
                          AND EXTRACT(Month FROM A.fec_ultimo_consumo ) = PN_FEC_MES
                    group by F.cod_grp_diag , A.cod_mac ,  G.linea_trat , K.nombre , D.descripcion;      

      END IF;


      PN_COD_RESULTADO := 0;
      PV_MSG_RESULTADO := 'Consulta exitosa';


    ELSE
      PN_COD_RESULTADO := 1;
      PV_MSG_RESULTADO := 'MES ó AÑO son Nulos';
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      PN_COD_RESULTADO := -1;
      PV_MSG_RESULTADO := '[[SP_PFC_S_INDICADORES_PROC_XLS]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END SP_PFC_S_INDICADORES_CONS_XLS;


  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 08/07/2019
  -- PURPOSE : Generar Datos Reporte Solicitud de Autrorizaciones

  PROCEDURE SP_PFC_I_REP_SOL_AUTOR (PN_FEC_MES     IN NUMBER,
                                  PN_FEC_ANO     IN NUMBER,
                                  PN_COD_RESULTADO   OUT NUMBER,
                                  PV_MSG_RESULTADO   OUT VARCHAR2) AS
  
  V_CANT_REG          NUMBER;
  
  CURSOR  V_ITEM_EVALUACION IS
      SELECT A.COD_DESC_SOL_EVA AS COD_SOL_EVA,
               (SELECT SUBSTR(C.COD_AFI_PACIENTE,-8,8) FROM DUAL) AS COD_PACIENTE,
               C.COD_AFI_PACIENTE , D.DESCRIPCION AS MEDICAMENTO , C.FEC_RECETA AS FECHA_RECETA,
               C.COD_DIAGNOSTICO AS CIE10, C.COD_GRP_DIAG , C.DES_PLAN AS PLAN, '' AS ESTADIO, '' AS TNM, 
               E.NRO_LINEA_TRA,
               C.COD_CLINICA, C.CMP_MEDICO, C.MEDICO_TRATANTE_PRESCRIPTOR, C.COD_SCG_SOLBEN,
               C.FEC_SCG_SOLBEN , C.HORA_SCG_SOLBEN,
               ( select nombre from pfc_parametro P where P.cod_parametro = C.TIPO_SCG_SOLBEN)  AS TIPO_SCG_SOLBEN,  
               (select nombre from pfc_parametro P where P.CODIGO = C.ESTADO_SCG AND P.COD_GRUPO = 5) AS ESTADO_SCG,
               LPAD(B.COD_SOL_PRE,10,'0') AS COD_SOL_PRE,
               B.FEC_SOL_PRE,
               TO_CHAR(B.FEC_SOL_PRE, 'HH24:MI:SS') AS HOR_SOL_PRE,
               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = B.P_ESTADO_SOL_PRE ) AS ESTADO_SOL_PRE,
               B.USUARIO_MODIFICACION AS USUARIO_EVA_PRE, A.FEC_SOL_EVA AS FEC_HORA_SOL_EVA ,
               C.DES_CONTRATANTE AS CONTRATANTE ,
               H.P_CUMPLE_PREF_INST,
               I.COD_CHKLIST_INDI, J.DESCRIPCION AS INDICACION_CHKLIST,
               I.P_CUMPLE_CHKLIST_PER, K.P_PERTINENCIA,
               K.P_CONDICION_PACIENTE ,
               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = K.P_CONDICION_PACIENTE ) AS DESCRIPCION_CONDICION_PACIENTE, 
               K.P_TIEMPO_USO , ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = K.P_TIEMPO_USO ) AS TIEMPO_USO,
               A.P_ESTADO_SOL_EVA , 
               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = A.P_ESTADO_SOL_EVA ) AS ESTADO_SOL_EVA ,
               A.P_ROL_RESP_PENDIENTE_EVA , 
               A.COD_AUTO_PERTE AS AUTORIZADOR_PERTINENCIA, K.FEC_APROBACION AS FEC_EVA_AUTO_PERTE, K.P_RESULTADO_AUTORIZADOR,
               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = K.P_RESULTADO_AUTORIZADOR ) as RESULTADO_AUTORIZADOR, 
               A.COD_LIDER_TUMOR , T.FECHA_EVA as FECHA_EVA_LIDER_TUMOR,
               T.P_RESULTADO_EVA , 
               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = T.P_RESULTADO_EVA ) AS RESULTADO_EVA_LIDER_TUMOR , 
               A.ESTADO_CORREO_ENV_CMAC AS CORREO_ENV_CMAC,
               X.FEC_REUNION AS FEC_REUNION_CMAC, Y.COD_RES_EVALUACION, 
               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = Y.COD_RES_EVALUACION ) AS RESULTADO_EVA_CMAC, 
               A.FEC_FINALIZAR_ESTADO , C.NRO_CG AS NRO_CARTA_GARANTIA,
               ( CASE A.FEC_EVALUACION_AUTORIZADOR 
                 WHEN NULL THEN NULL 
                 ELSE TRUNC(A.FEC_EVALUACION_AUTORIZADOR - A.FECHA_CREACION) END ) AS TIEMPO_AUTORIZADOR,
               ( CASE A.FEC_EVALUACION_LIDER_TUMOR 
                 WHEN NULL THEN NULL 
                 ELSE TRUNC(A.FEC_EVALUACION_LIDER_TUMOR - A.FEC_EVALUACION_AUTORIZADOR) END ) AS TIEMPO_LIDER,
               ( CASE  
                 WHEN A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC) THEN (
                  CASE A.FEC_EVALUACION_LIDER_TUMOR 
                  WHEN NULL THEN TRUNC(A.FEC_FINALIZAR_ESTADO - A.FEC_EVALUACION_AUTORIZADOR) 
                  ELSE TRUNC(A.FEC_FINALIZAR_ESTADO - A.FEC_EVALUACION_LIDER_TUMOR) END
                 ) 
                 ELSE NULL END ) AS TIEMPO_CMAC,
               TRUNC(A.FEC_FINALIZAR_ESTADO - A.FECHA_CREACION) AS TIEMPO_TOTAL
        FROM PFC_SOLICITUD_EVALUACION A, 
             PFC_SOLICITUD_PRELIMINAR B,
             PFC_SOLBEN C,
             PFC_MAC D,
             PFC_MEDICAMENTO_NUEVO E,
             PFC_SEV_LINEA_TRATAMIENTO H,
             PFC_SEV_CHECKLIST_PAC I,
             PFC_CHKLIST_INDICACION J,
             PFC_SEV_ANALISIS_CONCLUSION K,
             PFC_EVA_LIDER_TUM T,
             PFC_PROGRAMACION_CMAC X,
             PFC_PROGRAMACION_CMAC_DET Y
         WHERE A.COD_SOL_PRE = B.COD_SOL_PRE
              AND B.COD_SCG = C.COD_SCG
              AND B.COD_MAC = D.COD_MAC
              AND A.COD_SOL_EVA  = E.COD_SOL_EVA
              AND E.COD_SEV_LIN_TRA = H.COD_SEV_LIN_TRA
              AND E.COD_SEV_CHK_PAC = I.COD_SEV_CHKLIST_PAC
              AND I.COD_CHKLIST_INDI = J.COD_CHKLIST_INDI
              AND E.COD_SEV_ANA_CON = K.COD_SEV_ANA_CON
              AND A.COD_SOL_EVA = T.COD_SOL_EVA (+)
              AND X.COD_PROGRAMACION_CMAC (+) = Y.COD_PROGRAMACION_CMAC
              AND Y.COD_SOL_EVA (+) = A.COD_SOL_EVA
              AND EXTRACT(Year FROM A.FEC_FINALIZAR_ESTADO) = PN_FEC_ANO
              AND EXTRACT(Month FROM A.FEC_FINALIZAR_ESTADO) = PN_FEC_MES
              AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
             PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
             PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC);
  
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    DBMS_OUTPUT.put_line('PN_FEC_ANO : '||PN_FEC_ANO);
    DBMS_OUTPUT.put_line('PN_FEC_MES : '||PN_FEC_MES);
    IF PN_FEC_MES IS NOT NULL  AND PN_FEC_ANO IS NOT NULL THEN
    
       SELECT count(*) INTO V_CANT_REG
       FROM PFC_REPORTE_SOLIC_AUTORIZA 
       WHERE FEC_MES = PN_FEC_MES AND FEC_ANO = PN_FEC_ANO;
       DBMS_OUTPUT.put_line('V_CANT_REG : '||V_CANT_REG);
        IF V_CANT_REG = 0  THEN
           DBMS_OUTPUT.put_line('ENTRA : ');
                             
            FOR itemEvaluacion IN V_ITEM_EVALUACION LOOP

                INSERT INTO PFC_REPORTE_SOLIC_AUTORIZA (FEC_MES, FEC_ANO, COD_SOL_EVA, COD_PACIENTE, COD_AFILIADO, 
                            MEDICAMENTO,
                            FECHA_RECETA,
                            CIE10, GRUPO_DIAGNOSTICO, PLAN , ESTADIO, TNM ,LINEA ,
                            CLINICA , CMP, MEC_TRATANTE_PRESC, COD_SCG_SOLBEN,
                            FEC_REG_SCG_SOLBEN,
                            HOR_REG_SCG_SOLBEN,
                            TIPO_SCG_SOLBEN, ESTADO_SCG_SOLBEN , COD_SOL_PRE,
                            FEC_REG_SOL_PRE,
                            HOR_REG_SOL_PRE,
                            ESTADO_SOL_PRE, AUTOR_PERTINENCIA_SOL_PRE,
                            FEC_HOR_REG_SOL_EVA,
                            CONTRATANTE,CUMPLE_PREF_INST, INDIC_CHECK_LIST_PAC, CUMPLE_INDIC_CHECK_LIST_PAC, PERTINENCIA,
                            COND_PACIENTE, TIEMPO_USO, ESTADO_SOL_EVA, ROL_RESP_PEND_EVA, AUTOR_PERTINENCIA_SOL_EVA,
                            FEC_EVA_AUTOR_PERTI,
                            RESULTADO_EVA_AUTOR_PERTI , LIDER_TUMOR,
                            FEC_EVA_LIDER_TUMOR, 
                            RESULTADO_EVA_LIDER_TUMOR, CORREO_ENV_CMAC, FEC_REUNION_CMAC, RESULTADO_EVA_CMAC , NRO_CARTA_GARANTIA,
                            TIEMPO_AUTOR_PERTINENCIA_EVA, TIEMPO_LIDER_TUMOR, TIEMPO_CMAC, TIEMPO_TOTAL )
                     VALUES (PN_FEC_MES ,  PN_FEC_ANO , itemEvaluacion.COD_SOL_EVA, itemEvaluacion.COD_PACIENTE, itemEvaluacion.COD_AFI_PACIENTE,
                            itemEvaluacion.MEDICAMENTO,
                            itemEvaluacion.FECHA_RECETA,
                            itemEvaluacion.CIE10,itemEvaluacion.COD_GRP_DIAG, itemEvaluacion.PLAN, itemEvaluacion.ESTADIO , itemEvaluacion.TNM ,
                            itemEvaluacion.NRO_LINEA_TRA,
                            itemEvaluacion.COD_CLINICA,  itemEvaluacion.CMP_MEDICO ,
                            itemEvaluacion.MEDICO_TRATANTE_PRESCRIPTOR , itemEvaluacion.COD_SCG_SOLBEN , 
                            itemEvaluacion.FEC_SCG_SOLBEN,
                            itemEvaluacion.HORA_SCG_SOLBEN , 
                            itemEvaluacion.TIPO_SCG_SOLBEN, itemEvaluacion.ESTADO_SCG , itemEvaluacion.COD_SOL_PRE, 
                            itemEvaluacion.FEC_SOL_PRE,
                            itemEvaluacion.HOR_SOL_PRE,
                            itemEvaluacion.ESTADO_SOL_PRE, itemEvaluacion.USUARIO_EVA_PRE,
                            itemEvaluacion.FEC_HORA_SOL_EVA,
                            itemEvaluacion.CONTRATANTE, itemEvaluacion.P_CUMPLE_PREF_INST, 
                            itemEvaluacion.INDICACION_CHKLIST, itemEvaluacion.P_CUMPLE_CHKLIST_PER,
                            itemEvaluacion.P_PERTINENCIA, itemEvaluacion.DESCRIPCION_CONDICION_PACIENTE , 
                            itemEvaluacion.TIEMPO_USO , itemEvaluacion.ESTADO_SOL_EVA,
                            itemEvaluacion.P_ROL_RESP_PENDIENTE_EVA , itemEvaluacion.AUTORIZADOR_PERTINENCIA, 
                            itemEvaluacion.FEC_EVA_AUTO_PERTE,
                            itemEvaluacion.RESULTADO_AUTORIZADOR,
                            itemEvaluacion.COD_LIDER_TUMOR,
                            itemEvaluacion.FECHA_EVA_LIDER_TUMOR, 
                            itemEvaluacion.RESULTADO_EVA_LIDER_TUMOR, itemEvaluacion.CORREO_ENV_CMAC, 
                            itemEvaluacion.FEC_REUNION_CMAC , itemEvaluacion.RESULTADO_EVA_CMAC,
                            itemEvaluacion.NRO_CARTA_GARANTIA, itemEvaluacion.Tiempo_Autorizador,
                            itemEvaluacion.Tiempo_Lider, itemEvaluacion.TIEMPO_CMAC,
                            itemEvaluacion.TIEMPO_TOTAL) ;
                                    
            END LOOP; 
            commit;
                  
            PN_COD_RESULTADO := 0;
            -- PV_MSG_RESULTADO := 'Datos de Reporte Autorizaciones Generado';
            PV_MSG_RESULTADO :='Se generaron los reportes de manera exitosa';
        ELSE 
        
                               PN_COD_RESULTADO := 0;
                               -- PV_MSG_RESULTADO := 'Datos de Reporte Autorizaciones Generado Anteriormente';
                               PV_MSG_RESULTADO :='Los reportes ya fueron generados anteriormente';
        END IF;
    ELSE
      PN_COD_RESULTADO := 1;
      PV_MSG_RESULTADO := 'MES ó AÑO son Nulos';
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      PN_COD_RESULTADO := -1;
      PV_MSG_RESULTADO := '[[SP_PFC_I_REP_SOL_AUTOR]] LINEA : '
                             || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END SP_PFC_I_REP_SOL_AUTOR;
  

  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED :  08/07/2019
  -- PURPOSE : Listar Reporte de Solicitudes de Autorizaciones 
  PROCEDURE SP_PFC_S_REP_SOL_AUTOR (PN_FEC_MES     IN NUMBER,
                    PN_FEC_ANO     IN NUMBER,
                                           OP_OBJCURSOR       OUT SYS_REFCURSOR,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2) AS
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    IF PN_FEC_MES IS NOT NULL  AND PN_FEC_ANO IS NOT NULL THEN
      OPEN OP_OBJCURSOR FOR
           SELECT FEC_MES,FEC_ANO,COD_SOL_EVA,COD_PACIENTE,COD_AFILIADO,FECHA_AFILIACION,TIPO_AFILIACION,DOC_IDENTIDAD,
                  PACIENTE,SEXO,EDAD,MEDICAMENTO,FECHA_RECETA,CIE10,DIAGNOSTICO,GRUPO_DIAGNOSTICO,PLAN,ESTADIO,TNM,
                  LINEA,CLINICA,CMP,MEC_TRATANTE_PRESC,COD_SCG_SOLBEN,FEC_REG_SCG_SOLBEN,HOR_REG_SCG_SOLBEN,TIPO_SCG_SOLBEN,
                  ESTADO_SCG_SOLBEN,COD_SOL_PRE,FEC_REG_SOL_PRE,HOR_REG_SOL_PRE,ESTADO_SOL_PRE,AUTOR_PERTINENCIA_SOL_PRE,
                  FEC_HOR_REG_SOL_EVA,CONTRATANTE,
                  (CASE
                    WHEN CUMPLE_PREF_INST = '0'
                      THEN (SELECT P.NOMBRE FROM PFC_PARAMETRO P
                        WHERE P.COD_GRUPO = PCK_PFC_CONSTANTE.PN_CODIGO_CUMPLE_LINEA_TRAT AND P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_COD_LINEA_TRAT_NO)
                    WHEN CUMPLE_PREF_INST = '1'
                      THEN (SELECT P.NOMBRE FROM PFC_PARAMETRO P
                        WHERE P.COD_GRUPO = PCK_PFC_CONSTANTE.PN_CODIGO_CUMPLE_LINEA_TRAT AND P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_COD_LINEA_TRAT_SI)
                    WHEN CUMPLE_PREF_INST = '2'
                      THEN (SELECT P.NOMBRE FROM PFC_PARAMETRO P
                        WHERE P.COD_GRUPO = PCK_PFC_CONSTANTE.PN_CODIGO_CUMPLE_LINEA_TRAT AND P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_COD_LINEA_TRAT_NO_APLICA)
                   END) AS CUMPLE_PREF_INST,
                  INDIC_CHECK_LIST_PAC,
                  (CASE
                    WHEN CUMPLE_INDIC_CHECK_LIST_PAC = '0'
                      THEN (SELECT P.NOMBRE FROM PFC_PARAMETRO P
                        WHERE P.COD_GRUPO = PCK_PFC_CONSTANTE.PN_CODIGO_CUMPLE_LINEA_TRAT AND P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_COD_LINEA_TRAT_NO)
                    WHEN CUMPLE_INDIC_CHECK_LIST_PAC = '1'
                      THEN (SELECT P.NOMBRE FROM PFC_PARAMETRO P
                        WHERE P.COD_GRUPO = PCK_PFC_CONSTANTE.PN_CODIGO_CUMPLE_LINEA_TRAT AND P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_COD_LINEA_TRAT_SI)
                   END) AS CUMPLE_INDIC_CHECK_LIST_PAC,
                  (CASE
                    WHEN PERTINENCIA = '0'
                      THEN (SELECT P.NOMBRE FROM PFC_PARAMETRO P
                        WHERE P.COD_GRUPO = PCK_PFC_CONSTANTE.PN_CODIGO_CUMPLE_LINEA_TRAT AND P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_COD_LINEA_TRAT_NO)
                    WHEN PERTINENCIA = '1'
                      THEN (SELECT P.NOMBRE FROM PFC_PARAMETRO P
                        WHERE P.COD_GRUPO = PCK_PFC_CONSTANTE.PN_CODIGO_CUMPLE_LINEA_TRAT AND P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_COD_LINEA_TRAT_SI)
                   END) AS PERTINENCIA,
                  COND_PACIENTE,TIEMPO_USO,ESTADO_SOL_EVA,ROL_RESP_PEND_EVA,AUTOR_PERTINENCIA_SOL_EVA,
                  FEC_EVA_AUTOR_PERTI,RESULTADO_EVA_AUTOR_PERTI,
                  (SELECT PA.APELLIDOS||', '||PA.NOMBRES FROM PFC_PARTICIPANTE PA WHERE PA.COD_PARTICIPANTE = LIDER_TUMOR) AS LIDER_TUMOR,
                  FEC_EVA_LIDER_TUMOR,RESULTADO_EVA_LIDER_TUMOR,
                  (CASE
                    WHEN CORREO_ENV_CMAC = PCK_PFC_CONSTANTE.PN_CODIGO_CORREO_CMAC_SI THEN
                      'SI'
                    WHEN CORREO_ENV_CMAC = PCK_PFC_CONSTANTE.PN_CODIGO_CORREO_CMAC_NO THEN
                      'NO'
                    END
                  ) AS CORREO_ENV_CMAC,
                  FEC_REUNION_CMAC,RESULTADO_EVA_CMAC,NRO_CARTA_GARANTIA,TIEMPO_AUTOR_PERTINENCIA_EVA,
                  TIEMPO_LIDER_TUMOR,TIEMPO_CMAC,TIEMPO_TOTAL
           FROM PFC_REPORTE_SOLIC_AUTORIZA 
           WHERE FEC_MES = PN_FEC_MES AND FEC_ANO = PN_FEC_ANO;
    
      PN_COD_RESULTADO := 0;
      PV_MSG_RESULTADO := 'Consulta exitosa';
    ELSE
      PN_COD_RESULTADO := 1;
      PV_MSG_RESULTADO := 'MES ó AÑO son Nulos';
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      PN_COD_RESULTADO := -1;
      PV_MSG_RESULTADO := '[[SP_PFC_S_REP_SOL_AUTOR]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END SP_PFC_S_REP_SOL_AUTOR;


  -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED : 12/07/2019
  -- PURPOSE : Generar Datos Reporte Solicitud de Monitoreo

PROCEDURE SP_PFC_I_REP_SOL_MONITOR (PN_FEC_MES     IN NUMBER,
                    PN_FEC_ANO     IN NUMBER,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2) AS
  
  V_CANT_REG          NUMBER;
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    IF PN_FEC_MES IS NOT NULL  AND PN_FEC_ANO IS NOT NULL THEN
    
       SELECT count(*) INTO V_CANT_REG
       FROM PFC_REPORTE_SOLIC_MONITOREO 
       WHERE FEC_MES = PN_FEC_MES AND FEC_ANO = PN_FEC_ANO;
    
        IF V_CANT_REG = 0  THEN
                  FOR itemEvaluacion IN ( 
                  
                               -- Query Reporte General de Monitoreo

                       SELECT  M.COD_DESC_MONITOREO AS CODIGO_TAREA_MONITOREO,
                               (SELECT SUBSTR(C.COD_AFI_PACIENTE,-8,8) FROM DUAL) AS COD_PACIENTE,
                               C.COD_AFI_PACIENTE, 
                               D.DESCRIPCION AS MEDICAMENTO, 
                               C.COD_DIAGNOSTICO AS CIE10, C.COD_GRP_DIAG , C.DES_PLAN AS PLAN, '' AS ESTADIO, '' AS TNM,
                               E.NRO_LINEA_TRA,
                               C.COD_CLINICA, C.CMP_MEDICO, C.MEDICO_TRATANTE_PRESCRIPTOR, C.COD_SCG_SOLBEN,
                               C.TX_DATO_ADIC1 , A.FEC_SOL_EVA AS FEC_HORA_SOL_EVA ,
                               A.COD_DESC_SOL_EVA AS COD_SOL_EVA,
                               A.P_ESTADO_SOL_EVA , 
                               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = A.P_ESTADO_SOL_EVA ) AS ESTADO_SOL_EVA ,
                               A.COD_AUTO_PERTE AS AUTORIZADOR_PERTINENCIA, 
                               EV.NRO_DESC_EVOLUCION AS NRO_EVOLUCION,
                               --EV.USUARIO_CREA AS RESPONSABLE_MONITOREO,
                               -- RESPONSABLE DE MONITOREO DEBE SER SOLAMENTE 1, PARA EL RANGO DE EDAD; CONSIDERAR LA VALIDACION EL RANGO DE EDAD "TODOS" COMO UNICO POR GRUPO DE DIAGNOSTICO
                               (SELECT DISTINCT PAR.COD_USUARIO
                                FROM pfc_participante_grupo_diag PGD
                                INNER JOIN PFC_PARTICIPANTE PAR
                                ON PAR.COD_PARTICIPANTE = PGD.COD_PARTICIPANTE
                                INNER JOIN (SELECT P.COD_PARAMETRO,
                                SUBSTR(P.VALOR1,1,INSTR(P.VALOR1,'-',1,1)-1) AS MIN_VALOR,
                                SUBSTR(P.VALOR1,-(LENGTH(P.VALOR1)-INSTR(P.VALOR1,'-',1,1)),LENGTH(P.VALOR1)-INSTR(P.VALOR1,'-',1,1)) AS MAX_VALOR
                                FROM PFC_PARAMETRO P WHERE P.COD_GRUPO =66 AND P.ESTADO = PCK_PFC_CONSTANTE.PV_ESTADO_PARAMETRO) T
                                ON T.COD_PARAMETRO = PGD.P_RANGO_EDAD
                                WHERE
                                PAR.COD_ROL = PCK_PFC_CONSTANTE.PN_ROL_RESP_MONIT -- CONSTANTE
                                AND PAR.P_ESTADO = PCK_PFC_CONSTANTE.PN_ESTADO_PARTICIP_ACTIVO  -- CONSTANTE
                                AND PGD.COD_GRP_DIAG = C.COD_GRP_DIAG
                                AND T.MIN_VALOR < C.EDAD_PACIENTE
                                AND C.EDAD_PACIENTE < T.MAX_VALOR) AS RESPONSABLE_MONITOREO,
                                --
                               M.FEC_PROX_MONITOREO AS FECHA_PROG_MONITOREO, M.FEC_MONITOREO AS FECHA_REAL_MONITOREO,
                               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = M.P_ESTADO_MONITOREO ) AS ESTADO_MONITOREO,
                               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = EV.P_TOLERANCIA ) AS TOLERANCIA,
                               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = EV.P_TOXICIDAD ) AS TOXICIDAD,
                               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = EV.P_RESP_CLINICA ) AS RESPUESTA_CLINICA,
                               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = EV.P_ATEN_ALERTA ) AS ATENCION_ALERTA,
                               EV.FEC_ULTIMO_CONSUMO , EV.CANT_ULTIMO_CONSUMO ,
                               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = EV.P_RES_EVOLUCION ) AS RESULTADO_EVOLUCION,
                               EV.FEC_INACTIVACION , 
                               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = EV.P_MOTIVO_INACTIVACION ) AS MOTIVO_INACTIVACION,
                               SE.NOM_EJECUTIVO_MONITOREO ,
                               ( select nombre from pfc_parametro where pfc_parametro.cod_parametro = SE.P_ESTADO_SEGUIMIENTO ) AS ESTADO_SEGUIMIENTO,
                               SE.FECHA_REGISTRO AS FECHA_REGISTRO_SE, C.NRO_CG AS NRO_CARTA_GARANTIA,
                               ( SELECT LT.FEC_INICIO FROM PFC_HIST_LINEA_TRATAMIENTO LT WHERE LT.COD_SOL_EVA = A.COD_SOL_EVA ) AS FEC_INI_TRAT,
                               A.FEC_FINALIZAR_ESTADO
                       FROM PFC_SOLICITUD_EVALUACION A, 
                             PFC_SOLICITUD_PRELIMINAR B,
                             PFC_SOLBEN C,
                             PFC_MAC D,
                             PFC_MEDICAMENTO_NUEVO E,
                             PFC_SEV_LINEA_TRATAMIENTO H,
                             PFC_SEV_CHECKLIST_PAC I,
                             PFC_CHKLIST_INDICACION J,
                             PFC_SEV_ANALISIS_CONCLUSION K,
                             PFC_EVA_LIDER_TUM T,
                             PFC_PROGRAMACION_CMAC X,
                             PFC_PROGRAMACION_CMAC_DET Y,
                             PFC_MONITOREO M,
                             PFC_EVOLUCION EV,
                             PFC_SEG_EJECUTIVO SE
                         WHERE A.COD_SOL_PRE = B.COD_SOL_PRE
                              AND B.COD_SCG = C.COD_SCG
                              AND B.COD_MAC = D.COD_MAC
                              AND A.COD_SOL_EVA  = E.COD_SOL_EVA
                              AND E.COD_SEV_LIN_TRA = H.COD_SEV_LIN_TRA
                              AND E.COD_SEV_CHK_PAC = I.COD_SEV_CHKLIST_PAC
                              AND I.COD_CHKLIST_INDI = J.COD_CHKLIST_INDI
                              AND E.COD_SEV_ANA_CON = K.COD_SEV_ANA_CON
                              AND A.COD_SOL_EVA = T.COD_SOL_EVA (+)
                              AND X.COD_PROGRAMACION_CMAC (+) = Y.COD_PROGRAMACION_CMAC
                              AND Y.COD_SOL_EVA (+) = A.COD_SOL_EVA
                              AND M.COD_SOL_EVA = A.COD_SOL_EVA
                              AND M.COD_MONITOREO = EV.COD_MONITOREO (+)
                              AND M.COD_MONITOREO = SE.COD_MONITOREO (+)
                              AND EXTRACT(Year FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_ANO
                              AND EXTRACT(Month FROM A.FEC_FINALIZAR_ESTADO ) = PN_FEC_MES
                              AND A.P_ESTADO_SOL_EVA IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
                                  PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
                                  PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)
                             
                  )
              
                                LOOP
                                    
                                    INSERT INTO PFC_REPORTE_SOLIC_MONITOREO (FEC_MES, FEC_ANO, COD_SOL_EVA, COD_PACIENTE, COD_AFILIADO, 
                                                MEDICAMENTO, CODIGO_TAREA_MONITOREO, CIE10, GRUPO_DIAGNOSTICO, PLAN , 
                                                ESTADIO, TNM ,LINEA , CLINICA , CMP, MEC_TRATANTE_PRESC, COD_SCG_SOLBEN,
                                                ESTADO_SOL_EVA, AUTOR_PERTINENCIA_SOL_EVA,
                                                FEC_APROB_SOL_EVA ,
                                                NRO_CARTA_GARANTIA,
                                                FEC_INI_TRAT,
                                                NRO_EVOLUCION , COD_RESP_MONITOREO, 
                                                FEC_PROG_MONITOREO, FEC_REAL_MONITOREO, ESTADO_MONITOREO, TOLERANCIA, 
                                                TOXICIDAD, RESPUESTA_CLINICA, ATENCION_ALERTAS, FEC_ULTIMO_CONSUMO, CANT_ULTIMO_CONSUMO,
                                                RESULTADO_EVOLUCION, FEC_INACTIVACION, MOTIVO_INACTIVACION , EJECUTIVO_MONITOREO,
                                                ESTADO_SEGUIMIENTO, FECHA_REGISTRO_SE
                                                ) VALUES (PN_FEC_MES ,  PN_FEC_ANO , itemEvaluacion.COD_SOL_EVA, itemEvaluacion.COD_PACIENTE, itemEvaluacion.COD_AFI_PACIENTE,
                                                itemEvaluacion.MEDICAMENTO , itemEvaluacion.CODIGO_TAREA_MONITOREO , itemEvaluacion.CIE10 , 
                                                itemEvaluacion.COD_GRP_DIAG, itemEvaluacion.PLAN, itemEvaluacion.ESTADIO , itemEvaluacion.TNM ,
                                                itemEvaluacion.NRO_LINEA_TRA,
                                                itemEvaluacion.COD_CLINICA, itemEvaluacion.CMP_MEDICO,
                                                itemEvaluacion.MEDICO_TRATANTE_PRESCRIPTOR , itemEvaluacion.COD_SCG_SOLBEN , 
                                                itemEvaluacion.ESTADO_SOL_EVA,
                                                itemEvaluacion.AUTORIZADOR_PERTINENCIA, 
                                                itemEvaluacion.FEC_FINALIZAR_ESTADO,
                                                itemEvaluacion.NRO_CARTA_GARANTIA,
                                                itemEvaluacion.FEC_INI_TRAT,
                                                itemEvaluacion.NRO_EVOLUCION, itemEvaluacion.RESPONSABLE_MONITOREO, 
                                                itemEvaluacion.FECHA_PROG_MONITOREO, 
                                                itemEvaluacion.FECHA_REAL_MONITOREO, 
                                                itemEvaluacion.ESTADO_MONITOREO , itemEvaluacion.TOLERANCIA,
                                                itemEvaluacion.TOXICIDAD, itemEvaluacion.RESPUESTA_CLINICA, itemEvaluacion.ATENCION_ALERTA,
                                                itemEvaluacion.FEC_ULTIMO_CONSUMO, itemEvaluacion.CANT_ULTIMO_CONSUMO, 
                                                itemEvaluacion.RESULTADO_EVOLUCION,
                                                itemEvaluacion.FEC_INACTIVACION,
                                                itemEvaluacion.MOTIVO_INACTIVACION, itemEvaluacion.NOM_EJECUTIVO_MONITOREO,
                                                itemEvaluacion.ESTADO_SEGUIMIENTO,
                                                itemEvaluacion.FECHA_REGISTRO_SE);
                                    
                                END LOOP; 
                                commit;
                
                               PN_COD_RESULTADO := 0;
                               -- PV_MSG_RESULTADO := 'Datos de Reporte Monitoreo Generado';
                               PV_MSG_RESULTADO :='Se generaron los reportes de manera exitos';
        ELSE 
        
                               PN_COD_RESULTADO := 0;
                               -- PV_MSG_RESULTADO := 'Datos de Reporte Monitoreo Generado Anteriormente';
                               PV_MSG_RESULTADO :='Los reportes ya fueron generados anteriormente';
        END IF;
    ELSE
      PN_COD_RESULTADO := 1;
      PV_MSG_RESULTADO := 'MES ó AÑO son Nulos';
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      PN_COD_RESULTADO := -1;
      PV_MSG_RESULTADO := '[[SP_PFC_I_REP_SOL_MONITOR]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END SP_PFC_I_REP_SOL_MONITOR;

    -- AUTHOR  : JOSE.REYES/MDP
  -- CREATED :  12/07/2019
  -- PURPOSE : Listar Reporte de Solicitudes de Monitoreo                                            
PROCEDURE SP_PFC_S_REP_SOL_MONITOR  (PN_FEC_MES     IN NUMBER,
                    PN_FEC_ANO     IN NUMBER,
                                           OP_OBJCURSOR       OUT SYS_REFCURSOR,
                                           PN_COD_RESULTADO   OUT NUMBER,
                                           PV_MSG_RESULTADO   OUT VARCHAR2) AS
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    IF PN_FEC_MES IS NOT NULL  AND PN_FEC_ANO IS NOT NULL THEN
      OPEN OP_OBJCURSOR FOR
           SELECT FEC_MES,FEC_ANO,COD_SOL_EVA,COD_PACIENTE,COD_AFILIADO,FECHA_AFILIACION,TIPO_AFILIACION,
                  DOC_IDENTIDAD,PACIENTE,SEXO,EDAD,MEDICAMENTO, CIE10,DIAGNOSTICO,GRUPO_DIAGNOSTICO,PLAN,
                  ESTADIO,TNM,LINEA,CLINICA,CMP,MEC_TRATANTE_PRESC,COD_SCG_SOLBEN, ESTADO_SOL_EVA,
                  FEC_APROB_SOL_EVA,AUTOR_PERTINENCIA_SOL_EVA,NRO_CARTA_GARANTIA,
                  FEC_INI_TRAT,
                  NRO_EVOLUCION,
                  COD_RESP_MONITOREO, FEC_PROG_MONITOREO,FEC_REAL_MONITOREO,ESTADO_MONITOREO,TOLERANCIA,
                  TOXICIDAD,RESPUESTA_CLINICA,ATENCION_ALERTAS,FEC_ULTIMO_CONSUMO,CANT_ULTIMO_CONSUMO,
                  RESULTADO_EVOLUCION,
                  FEC_INACTIVACION,MOTIVO_INACTIVACION,EJECUTIVO_MONITOREO,ESTADO_SEGUIMIENTO,
                FECHA_REGISTRO_SE,CODIGO_TAREA_MONITOREO
           FROM   PFC_REPORTE_SOLIC_MONITOREO 
           WHERE  FEC_MES = PN_FEC_MES AND FEC_ANO = PN_FEC_ANO;
    
      PN_COD_RESULTADO := 0;
      PV_MSG_RESULTADO := 'Consulta exitosa';
    ELSE
      PN_COD_RESULTADO := 1;
      PV_MSG_RESULTADO := 'MES ó AÑO son Nulos';
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      PN_COD_RESULTADO := -1;
      PV_MSG_RESULTADO := '[[SP_PFC_S_REP_SOL_MONITOR]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END SP_PFC_S_REP_SOL_MONITOR;

END PCK_PFC_INDICADORES;

/
--------------------------------------------------------
--  DDL for Package Body PCK_PFC_MAESTROS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "ONTPFC"."PCK_PFC_MAESTROS" AS


  -- AUTHOR  : ALEX FLORES
  -- CREATED : 22/01/2019
  -- PURPOSE : INSERCION / ACTUALIZACION DE LOS VALORES DE EXAMEN MEDICO (MAESTROS)
   PROCEDURE SP_PFC_I_U_EXAMEN_MEDICO(
                                    PN_COD_EXAMEN_MED  IN NUMBER,
                                    PN_COD_EX_MEDICO_L IN VARCHAR2,
                                    PN_COD_ESTADO      IN NUMBER,
                                    PV_DESC_EX         IN VARCHAR2,
                                    PN_COD_TIP_EXAMEN  IN NUMBER,
                                    PN_COD_TIP_RESUL   IN NUMBER,
                                    PV_UNID_MED        IN VARCHAR2,
                                    PV_DESC_RANGO1     IN VARCHAR2,
                                    PV_DESC_RANGO2     IN VARCHAR2,
                                    PV_COD_POS_VALORES IN VARCHAR2,
                                    PV_TEXTO           IN VARCHAR2,
                                    PN_COD_RESULTADO   OUT NUMBER,
                                    PV_MSG_RESULTADO   OUT VARCHAR2) IS

    PV_CODIGO_LARGO  VARCHAR2(20);
    RANGO_COM        VARCHAR2(20);
    --v_cod_ex_med_seq VARCHAR2(20);
    BEGIN

    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';

    RANGO_COM := concat(PV_DESC_RANGO1,concat('-',PV_DESC_RANGO2));

    --select COUNT(*) INTO PV_CODIGO_LARGO from ONTPFC.pfc_examen_medico_marcador
    --WHERE COD_EXAMEN_MED_LARGO = PN_COD_EX_MEDICO_L ;

    IF PV_CODIGO_LARGO = 0 THEN

               -- SELECT SQ_PFC_EXA_MED_L_COD_EXA_MED_L.NEXTVAL INTO v_cod_ex_med_seq  FROM dual;

            INSERT INTO ONTPFC.PFC_EXAMEN_MEDICO_MARCADOR(
            COD_EXAMEN_MED,
            COD_EXAMEN_MED_LARGO,
            P_ESTADO,
            DESCRIPCION,
            P_TIPO_EXAMEN
            )
            VALUES (
            PN_COD_EXAMEN_MED,
            PN_COD_EX_MEDICO_L,--v_cod_ex_med_seq,
            PN_COD_ESTADO,
            PV_DESC_EX,
            PN_COD_TIP_EXAMEN);

            PN_COD_RESULTADO  := 1;
            PV_MSG_RESULTADO  := 'Se registró con exito';

           ELSE
            UPDATE ONTPFC.pfc_examen_medico_marcador exm
                SET
                    exm.P_ESTADO = PN_COD_ESTADO,
                    exm.DESCRIPCION = PV_DESC_EX,
                    exm.P_TIPO_EXAMEN = PN_COD_TIP_EXAMEN
                WHERE COD_EXAMEN_MED_LARGO = PN_COD_EX_MEDICO_L;

                PN_COD_RESULTADO  := 2;
            PV_MSG_RESULTADO  := 'Se actualizó con exito';
       END IF;
       COMMIT;

  EXCEPTION
     WHEN OTHERS
       THEN
         PN_COD_RESULTADO := -1;
         PV_MSG_RESULTADO := '[[SP_PFC_I_EXAMEN_MEDICO]] '|| TO_CHAR(SQLCODE) ||' : '||SQLERRM;
    END SP_PFC_I_U_EXAMEN_MEDICO;

  -- AUTHOR  : AFLORES
  -- CREATED : 22/01/2019
  -- PURPOSE : Consulta  para el listado de examenes medico (MAESTROS)
  PROCEDURE SP_PFC_S_EXAMENMEDICO( PV_DESCRIPCION    IN VARCHAR2,
                                AC_LISTA_EXAMENES OUT PCK_PFC_CONSTANTE.TYPCUR,
                                PN_COD_RESULTADO  OUT NUMBER,
                                PV_MSG_RESULTADO  OUT VARCHAR2)
  IS
  L_SQL_STMT VARCHAR2(4000);
  BEGIN

    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';

     L_SQL_STMT:='select
                  e.COD_EXAMEN_MED_LARGO as codigo,
                  e.DESCRIPCION          as descripcion,
                  p2.nombre              as tipoExamen,
                  p3.nombre              as tipoIngreso,
                  e.UNIDAD_MEDIDA        as unidadMedida,
                  e.RANGO                as rango,
                  p1.nombre              as estado
            from BDFARMACIACOMPLEJA.pfc_examen_medico_marcador e
            inner join BDFARMACIACOMPLEJA.pfc_parametro p2 on p2.id_parametro=e.P_TIPO_EXAMEN
            inner join BDFARMACIACOMPLEJA.pfc_parametro p3 on p3.id_parametro=e.P_TIPO_INGRESO_RES
            inner join BDFARMACIACOMPLEJA.pfc_parametro p1 on p1.id_parametro=e.p_estado ';

     -- Descripcion
        IF PV_DESCRIPCION = '' OR PV_DESCRIPCION = 'null' THEN
            L_SQL_STMT := L_SQL_STMT ||   ' WHERE 1=1 ';
        ELSE
             L_SQL_STMT := L_SQL_STMT ||  ' WHERE e.descripcion LIKE UPPER ('||'''%'|| PV_DESCRIPCION||'%'''||')';
        END IF;

      OPEN AC_LISTA_EXAMENES FOR L_SQL_STMT;

    PN_COD_RESULTADO := 1;
    PV_MSG_RESULTADO := 'Consulta Exitosa';

  EXCEPTION
     WHEN OTHERS
       THEN
         PN_COD_RESULTADO := -1;
         PV_MSG_RESULTADO := '[[SP_PFC_I_EXAMEN_MEDICO]] '|| TO_CHAR(SQLCODE) ||' : '||SQLERRM;
    END SP_PFC_S_EXAMENMEDICO;


  -- AUTHOR  : PAVEL
  -- CREATED : 04/08/2019
  -- PURPOSE : Consulta para listados de MAC's segun filtro (MAESTROS)
  PROCEDURE SP_PFC_S_FILTRO_MAC(
                         PN_P_COD IN VARCHAR2,
                         PN_P_DES IN VARCHAR2,
                         PN_P_COM IN VARCHAR2,
                         TC_LIST          OUT PCK_PFC_CONSTANTE.TYPCUR,
                         PN_COD_RESULTADO OUT NUMBER,
                         PV_MSG_RESULTADO OUT VARCHAR2) IS
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    OPEN TC_LIST FOR
        SELECT
            COD_MAC,
            COD_MAC_LARGO,
            DESCRIPCION,
            P_TIPO_MAC,
            TIPOP.nombre AS D_TIPO_MAC,
            FEC_INSCRIPCION,
            FEC_INICIO_VIG,
            FEC_FIN_VIG,
            FEC_CREACION,
            USR_CREACION,
            FEC_MODIFICACION,
            USR_MODIFICACION,
            P_ESTADO_MAC,
            ESTADOP.nombre AS D_ESTADO_MAC
        FROM PFC_MAC M LEFT JOIN PFC_PARAMETRO TIPOP ON M.p_tipo_mac = TIPOP.cod_parametro
            LEFT JOIN PFC_PARAMETRO ESTADOP ON M.P_ESTADO_MAC = ESTADOP.cod_parametro
        WHERE
        REPLACE(LOWER(COD_MAC_LARGO),' ','') LIKE REPLACE(LOWER('%'||PN_P_COD||'%'),' ','')
        AND
        REPLACE(LOWER(DESCRIPCION),' ','') LIKE REPLACE(LOWER('%'||PN_P_DES||'%'),' ','');
    PN_COD_RESULTADO := 1;
    PV_MSG_RESULTADO := 'Consulta Exitosa';
  EXCEPTION
    WHEN OTHERS THEN
      PN_COD_RESULTADO := -1;
      PV_MSG_RESULTADO := '[[SP_PFC_S_FILTRO_MAC]] ' || TO_CHAR(SQLCODE) || ' : ' ||
                          SQLERRM;
  END SP_PFC_S_FILTRO_MAC;

END PCK_PFC_MAESTROS;

/
--------------------------------------------------------
--  DDL for Package Body PCK_PFC_REPORTE_EVALUACION
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "ONTPFC"."PCK_PFC_REPORTE_EVALUACION" AS

  -- AUTHOR  : CATALINA
  -- CREATED : 04/02/2019
  -- PURPOSE : Reporte Datos generales relacionados a una Solicitud de Evaluacion

  PROCEDURE sp_pfc_s_det_evaluacion_report(pn_cod_sol_eva   IN NUMBER,
                                           op_objcursor     OUT SYS_REFCURSOR,
                                           pn_cod_resultado OUT NUMBER,
                                           pv_msg_resultado OUT VARCHAR2) AS
  BEGIN
    pn_cod_resultado := -1;
    pv_msg_resultado := 'valor inicial';
    IF pn_cod_sol_eva IS NOT NULL THEN
      OPEN op_objcursor FOR
        SELECT ev.cod_sol_eva,
               'RICARDO, QUISPE AZABACHE' AS nombre_apellido, --nombre y apellidos
               'Masculino' AS sexo, -- sexo
               s.des_plan,
               s.cod_diagnostico,
               'CANCER DE PROSTATA' AS descripciondiagnostico, --descripcion de diagnostico
               m.descripcion,
               sp.fec_sol_pre,
               s.cod_scg_solben,
               s.cod_afi_paciente,
               s.edad_paciente,
               s.medico_tratante_prescriptor,
               'TNM' AS estadioclinico, -- estadio clinico
               mn.nro_linea_tra
          FROM pfc_solicitud_evaluacion ev,
               pfc_solicitud_preliminar sp,
               pfc_solben               s,
               pfc_mac                  m,
               pfc_medicamento_nuevo    mn
         WHERE sp.cod_scg = s.cod_scg
           AND sp.cod_sol_pre = ev.cod_sol_pre
           AND mn.cod_sol_eva = ev.cod_sol_eva
           AND m.cod_mac = sp.cod_mac
           AND ev.cod_sol_eva = pn_cod_sol_eva;
    
      pn_cod_resultado := 0;
      pv_msg_resultado := 'Consulta exitosa';
    ELSE
      pn_cod_resultado := 1;
      pv_msg_resultado := 'No cuenta con el codigo de evaluacion';
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      pn_cod_resultado := -1;
      pv_msg_resultado := '[[SP_PFC_S_DET_EVALUACION_REPORT]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END sp_pfc_s_det_evaluacion_report;

  -- AUTHOR  : AFLORES
  -- CREATED : 04/02/2019
  -- PURPOSE : Lista las lineas de tratamiento de condición basal  del medicamento solicitado

  PROCEDURE sp_pfc_s_linea_trat_pdf(pn_cod_sol_eva   IN NUMBER,
                                    tc_list_lt       OUT pck_pfc_constante.typcur,
                                    tc_list_ms       OUT pck_pfc_constante.typcur,
                                    tc_list_lm       OUT pck_pfc_constante.typcur,
                                    tc_list_cb       OUT pck_pfc_constante.typcur,
                                    pn_cod_resultado OUT NUMBER,
                                    pv_msg_resultado OUT VARCHAR2) IS
    --v_cmp_participante VARCHAR2(50);
    v_cod_afi VARCHAR2(20);
  BEGIN
    pn_cod_resultado := -1;
    pv_msg_resultado := 'valor inicial';
    IF pn_cod_sol_eva IS NOT NULL THEN
      SELECT sl.cod_afi_paciente
        INTO v_cod_afi
        FROM ontpfc.pfc_solicitud_evaluacion ev,
             ontpfc.pfc_solicitud_preliminar pr,
             ontpfc.pfc_solben               sl
       WHERE ev.cod_sol_pre = pr.cod_sol_pre
         AND pr.cod_scg = sl.cod_scg
         AND ev.cod_sol_eva = pn_cod_sol_eva;
    
      IF v_cod_afi IS NOT NULL THEN
        OPEN tc_list_lt FOR
          SELECT ht.linea_trat,
                 ht.fec_inicio,
                 ht.fec_fin,
                 (SELECT pp.nombre
                    FROM pfc_parametro pp
                   WHERE pp.cod_parametro = ht.p_curso) AS p_curso,
                 (SELECT pp.nombre
                    FROM pfc_parametro pp
                   WHERE pp.cod_parametro = ht.p_resp_alcanzada) AS p_resp_alcanzada,
                 (SELECT pp.nombre
                    FROM pfc_parametro pp
                   WHERE pp.cod_parametro = ht.p_lugar_progresion) AS p_lugar_progresion,
                 --MOTIVO FIN
                 (SELECT pp.nombre
                    FROM pfc_parametro pp
                   WHERE pp.cod_parametro = ht.p_motivo_inactivacion) AS p_motivo_inactivacion
            FROM pfc_hist_linea_tratamiento ht,
                 pfc_solben                 s,
                 pfc_solicitud_preliminar   p,
                 pfc_solicitud_evaluacion   se
           WHERE ht.cod_sol_eva = se.cod_sol_eva
             AND se.cod_sol_pre = p.cod_sol_pre
             AND p.cod_scg = s.cod_scg
             AND s.cod_afi_paciente = v_cod_afi;
      
        OPEN tc_list_ms FOR
          SELECT scb.antecedentes_imp,
                 (SELECT pp.nombre
                    FROM pfc_parametro pp
                   WHERE pp.cod_parametro = scb.p_ecog) AS ecog,
                 (SELECT pp.nombre
                    FROM pfc_parametro pp
                   WHERE pp.cod_parametro = scb.p_existe_toxicidad) AS existe_toxicidad,
                 (SELECT pp.nombre
                    FROM pfc_parametro pp
                   WHERE pp.cod_parametro = scb.p_tipo_toxicidad) AS tipo_toxicidad
            FROM ontpfc.pfc_medicamento_nuevo       mn,
                 ontpfc.pfc_solicitud_evaluacion    ev,
                 ontpfc.pfc_sev_condicion_basal_pac scb
           WHERE mn.cod_sol_eva = ev.cod_sol_eva
             AND mn.cod_sev_con_pac = scb.cod_sev_con_pac
             AND ev.cod_sol_eva = pn_cod_sol_eva;
      
        OPEN tc_list_lm FOR
          SELECT (SELECT pp.nombre
                    FROM pfc_parametro pp
                   WHERE pp.cod_parametro = mt.p_linea_metastasis) AS linea_metastasis,
                 (SELECT pp.nombre
                    FROM pfc_parametro pp
                   WHERE pp.cod_parametro = mt.p_lugar_metastasis) AS lugar_metastasis
            FROM ontpfc.pfc_medicamento_nuevo       mn,
                 ontpfc.pfc_solicitud_evaluacion    ev,
                 ontpfc.pfc_sev_condicion_basal_pac scb,
                 ontpfc.pfc_metastasis              mt
           WHERE mn.cod_sol_eva = ev.cod_sol_eva
             AND mn.cod_sev_con_pac = scb.cod_sev_con_pac
             AND mn.cod_sev_con_pac = mt.cod_sev_con_pac
             AND ev.cod_sol_eva = pn_cod_sol_eva;
      
        OPEN tc_list_cb FOR
          SELECT emm.descripcion, rb.resultado, rb.fec_resultado
            FROM ontpfc.pfc_medicamento_nuevo        mn,
                 ontpfc.pfc_solicitud_evaluacion     ev,
                 ontpfc.pfc_sev_condicion_basal_pac  scb,
                 ontpfc.pfc_resultado_basal_marcador rb,
                 ontpfc.pfc_configuracion_marcador   cmb,
                 ontpfc.pfc_examen_medico_marcador   emm
           WHERE mn.cod_sol_eva = ev.cod_sol_eva
             AND mn.cod_sev_con_pac = scb.cod_sev_con_pac
             AND rb.cod_sev_con_pac = mn.cod_sev_con_pac
             AND cmb.cod_config_marca = rb.cod_config_marca
             AND emm.cod_examen_med = cmb.cod_examen_med
             AND ev.cod_sol_eva = pn_cod_sol_eva;
      
        pn_cod_resultado := 0;
        pv_msg_resultado := 'Consulta exitosa';
      ELSE
        pn_cod_resultado := 1;
        pv_msg_resultado := 'No cuenta con el codigo afiliado';
      END IF;
    
    ELSE
      pn_cod_resultado := 1;
      pv_msg_resultado := 'No cuenta con el codigo de evaluación';
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      pn_cod_resultado := -1;
      pv_msg_resultado := '[[SP_PFC_S_LINEA_TRAT_PDF]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END sp_pfc_s_linea_trat_pdf;

  PROCEDURE sp_pfc_s_pdf_analisis_conclu(pn_cod_sol_eva   IN NUMBER,
                                         ac_list          OUT pck_pfc_constante.typcur,
                                         pn_cod_resultado OUT NUMBER,
                                         pv_msg_resultado OUT VARCHAR2) AS

  BEGIN
    pn_cod_resultado := -1;
    pv_msg_resultado := 'valor inicial';
    OPEN ac_list FOR
      SELECT (SELECT ma.descripcion
                FROM pfc_mac ma
               WHERE ma.cod_mac = spr.cod_mac) AS medicamento_evaluado,
             (CASE WHEN sac.p_cumple_chklist_per = 1 THEN 'SI'  ELSE 'NO' END) AS p_cumple_chklist_per,
             /*(SELECT pa.nombre
                FROM pfc_parametro pa
               WHERE pa.cod_parametro = sac.p_cumple_chklist_per_pac) AS p_cumple_chklist_per_pac,*/
             (CASE WHEN sac.p_cumple_pref_insti = 1 THEN 'SI'  ELSE 'NO' END) AS p_cumple_pref_insti,
             /*(SELECT pa.nombre
                FROM pfc_parametro pa
               WHERE pa.cod_parametro = sac.p_cumple_prefe_insti) AS p_cumple_prefe_insti,*/
             cin.descripcion,
             (SELECT ma.descripcion
                FROM pfc_mac ma
               WHERE ma.cod_mac = sac.cod_mac) AS medicamento_preferido,
             (CASE WHEN sac.p_pertinencia = 1 THEN 'SI'  ELSE 'NO' END) AS pertinencia,
             /*(SELECT pa.nombre
                FROM pfc_parametro pa
               WHERE pa.cod_parametro = sac.p_pertinencia) AS pertinencia,*/
             (SELECT pa.nombre
                FROM pfc_parametro pa
               WHERE pa.cod_parametro = sac.p_tiempo_uso) AS tiempo_uso,
             (SELECT pa.nombre
                FROM pfc_parametro pa
               WHERE pa.cod_parametro = sac.p_condicion_paciente) AS condicion_paciente,
             (SELECT pa.nombre
                FROM pfc_parametro pa
               WHERE pa.cod_parametro = sac.p_resultado_autorizador) AS resultado_autorizador,
             chp.comentario,
             mn.nro_linea_tra
        FROM pfc_medicamento_nuevo mnu
       INNER JOIN pfc_sev_analisis_conclusion sac
          ON sac.cod_sev_ana_con = mnu.cod_sev_ana_con
       INNER JOIN pfc_sev_checklist_pac chp
          ON mnu.cod_sev_chk_pac = chp.cod_sev_chklist_pac
       INNER JOIN pfc_chklist_indicacion cin
          ON cin.cod_chklist_indi = chp.cod_chklist_indi
       INNER JOIN pfc_solicitud_evaluacion sev
          ON sev.cod_sol_eva = mnu.cod_sol_eva
       INNER JOIN pfc_solicitud_preliminar spr
          ON spr.cod_sol_pre = sev.cod_sol_pre
       INNER JOIN pfc_sev_linea_tratamiento ltr
          ON ltr.cod_sev_lin_tra = mnu.cod_sev_lin_tra
       INNER JOIN pfc_medicamento_nuevo mn
          ON mn.cod_sol_eva = sev.cod_sol_eva
       WHERE sev.cod_sol_eva = pn_cod_sol_eva;
  
    pn_cod_resultado := 0;
    pv_msg_resultado := 'CONSULTA CON EXITO';
  EXCEPTION
    WHEN OTHERS THEN
      pn_cod_resultado := -1;
      pv_msg_resultado := '[[SP_PFC_S_PDF_ANALISIS_CONCLU]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END sp_pfc_s_pdf_analisis_conclu;

  PROCEDURE sp_pfc_s_pdf_medico_auditor(pn_cod_sol_eva    IN NUMBER,
                                        v_nombre_completo OUT VARCHAR2,
                                        pn_cod_resultado  OUT NUMBER,
                                        pv_msg_resultado  OUT VARCHAR2) AS
    v_cod_participante NUMBER;
  BEGIN
    pn_cod_resultado := -1;
    pv_msg_resultado := 'valor inicial';
    BEGIN
      SELECT par.cod_usuario
        INTO v_cod_participante
        FROM pfc_solben sol
       INNER JOIN pfc_solicitud_preliminar pre
          ON sol.cod_scg = pre.cod_scg
       INNER JOIN pfc_solicitud_evaluacion eva
          ON eva.cod_sol_pre = pre.cod_sol_pre
       INNER JOIN pfc_participante_grupo_diag pgd
          ON pgd.cod_grp_diag = sol.cod_grp_diag
       INNER JOIN pfc_participante par
          ON pgd.cod_participante = par.cod_participante
       INNER JOIN pfc_participante pro
          ON pro.cod_participante = par.cod_participante
       WHERE pro.cod_rol = pck_pfc_constante.pn_codigo_rol_medico_auditor
         AND eva.cod_sol_eva = pn_cod_sol_eva;
    
    EXCEPTION
      WHEN no_data_found THEN
        v_cod_participante := NULL;
    END;
  
    ontppa.pck_ppa_usuario_rol.sp_ppa_s_persona(v_cod_participante,
                                                v_nombre_completo,
                                                pn_cod_resultado,
                                                pv_msg_resultado);
    pn_cod_resultado := 0;
    pv_msg_resultado := 'Consulta Exitosa';
  EXCEPTION
    WHEN OTHERS THEN
      pn_cod_resultado := -1;
      pv_msg_resultado := '[[SP_PFC_S_PDF_MEDICO_AUDITOR]] ' ||
                          TO_CHAR(sqlcode) || ' : ' || sqlerrm;
  END sp_pfc_s_pdf_medico_auditor;

  -- AUTHOR  : AFLORES
  -- CREATED : 04/02/2019
  -- PURPOSE : Lista las lineas de tratamiento de condición basal  del medicamento solicitado
  PROCEDURE SP_PFC_S_VALIDACION_PDF(PN_COD_EVA       IN NUMBER,
                                    PN_COD_RESULTADO OUT NUMBER,
                                    PV_MSG_RESULTADO OUT VARCHAR2) IS
    --v_cmp_participante VARCHAR2(50);
    V_COD_AFI     VARCHAR2(20);
    V_PASO_UNO    CHAR(1);
    V_PASO_DOS    CHAR(1);
    V_PASO_TRES   CHAR(1);
    V_PASO_CUATRO CHAR(1);
    V_PASO_CINCO  CHAR(1);
  
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
  
    IF PN_COD_EVA IS NOT NULL THEN
    
      SELECT LT.GRABAR, SCRP.GRABAR, CBP.GRABAR, CSP.GRABAR, AC.GRABAR
        INTO V_PASO_UNO,
             V_PASO_DOS,
             V_PASO_TRES,
             V_PASO_CUATRO,
             V_PASO_CINCO
        FROM ONTPFC.PFC_MEDICAMENTO_NUEVO       MN,
             ONTPFC.PFC_SOLICITUD_EVALUACION    EV,
             ONTPFC.PFC_SEV_LINEA_TRATAMIENTO   LT,
             ONTPFC.PFC_SEV_CHECKLIST_REQ_PAC   SCRP,
             ONTPFC.PFC_SEV_CONDICION_BASAL_PAC CBP,
             ONTPFC.PFC_SEV_CHECKLIST_PAC       CSP,
             ONTPFC.PFC_SEV_ANALISIS_CONCLUSION AC
       WHERE MN.COD_SOL_EVA = EV.COD_SOL_EVA
         AND LT.COD_SEV_LIN_TRA = MN.COD_SEV_LIN_TRA
         AND SCRP.COD_SEV_CHK_REQ_PAC = MN.COD_SEV_CHK_REQ_PAC
         AND CBP.COD_SEV_CON_PAC = MN.COD_SEV_CON_PAC
         AND CSP.COD_SEV_CHKLIST_PAC = MN.COD_SEV_CHK_PAC
         AND AC.COD_SEV_ANA_CON = MN.COD_SEV_ANA_CON
         AND EV.COD_SOL_EVA = PN_COD_EVA;
    
      IF V_PASO_UNO = 1 THEN
      
        IF V_PASO_DOS = 1 THEN
        
          IF V_PASO_TRES = 1 THEN
            IF V_PASO_CUATRO = 1 THEN
              IF V_PASO_CINCO = 1 THEN
                PN_COD_RESULTADO := 1;
                PV_MSG_RESULTADO := 'Se grabó correctamente los 5 pasos';
              ELSE
                PN_COD_RESULTADO := 0;
                PV_MSG_RESULTADO := 'No grabó el paso 5';
              END IF;
            
            ELSE
              PN_COD_RESULTADO := 0;
              PV_MSG_RESULTADO := 'No grabó el paso 4';
            END IF;
          
          ELSE
            PN_COD_RESULTADO := 0;
            PV_MSG_RESULTADO := 'No grabó el paso 3';
          END IF;
        
        ELSE
          PN_COD_RESULTADO := 0;
          PV_MSG_RESULTADO := 'No grabó el paso 2';
        END IF;
      
      ELSE
        PN_COD_RESULTADO := 0;
        PV_MSG_RESULTADO := 'No grabó el paso 1';
      END IF;
    
    ELSE
      PN_COD_RESULTADO := 0;
      PV_MSG_RESULTADO := 'No cuenta con el Codigo de Evaluacion';
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      PN_COD_RESULTADO := -1;
      PV_MSG_RESULTADO := '[[SP_PFC_S_VALIDACION_PDF]] ' ||
                          TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
    
  END SP_PFC_S_VALIDACION_PDF;

END pck_pfc_reporte_evaluacion;

/
--------------------------------------------------------
--  DDL for Package Body PCK_PFC_SOLBEN
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "ONTPFC"."PCK_PFC_SOLBEN" IS

  /* AUTOR  : CQUISPE - JAZABACHE*/
  /* CREADO : 01/12/2018*/
  /* PROPOSITO : REGISTRA EN LA TABLA SOLBEN Y TABLA PRELIMINAR*/

   PROCEDURE SP_PFC_SI_SCG_SOLBEN_PREL (
      PV_COD_SCG_SOLBEN                IN                               VARCHAR2,
      PV_COD_CLINICA                   IN                               VARCHAR2,
      PV_COD_AFI_PACIENTE              IN                               VARCHAR2,
      PN_EDAD_PACIENTE                 IN                               NUMBER,
      PV_DES_CONTRATANTE               IN                               VARCHAR2,
      PV_DES_PLAN                      IN                               VARCHAR2,
      PV_FEC_AFILIACION                IN                               VARCHAR2,
      PV_COD_DIAGNOSTICO               IN                               VARCHAR2,
      PV_CMP_MEDICO                    IN                               VARCHAR2,
      PV_MEDICO_TRATANTE_PRESCRIPTOR   IN                               VARCHAR2,
      PV_FEC_RECETA                    IN                               VARCHAR2,
      PV_FEC_QUIMIO                    IN                               VARCHAR2,
      PV_FEC_HOSP_INICIO               IN                               VARCHAR2,
      PV_FEC_HOSP_FIN                  IN                               VARCHAR2,
      PV_FEC_SCG_SOLBEN                IN                               VARCHAR2,
      PV_HORA_SCG_SOLBEN               IN                               VARCHAR2,
      PV_TIPO_SCG_SOLBEN               IN                               VARCHAR2,
      PV_ESTADO_SCG                    IN                               VARCHAR2,
      PV_DESC_MEDICAMENTO              IN                               VARCHAR2,
      PV_DESC_ESQUEMA                  IN                               VARCHAR2,
      PN_TOTAL_PRESUPUESTO             IN                               NUMBER,
      PV_DESC_PROCEDIMIENTO            IN                               VARCHAR2,
      PV_PERSON_CONTACTO               IN                               VARCHAR2,
      PV_OBS_CLINICA                   IN                               VARCHAR2,
      PN_IND_VALIDA                    IN                               NUMBER,
      PV_TX_DATO_ADIC1                 IN                               VARCHAR2,
      PV_TX_DATO_ADIC2                 IN                               VARCHAR2,
      PV_TX_DATO_ADIC3                 IN                               VARCHAR2,
      PV_FEC_ACTUAL                    IN                               VARCHAR2,
      PV_COD_GRP_DIAGNOS               IN                               VARCHAR2,
      PV_OUT_COD_SCG_SOLBEN            OUT                              VARCHAR2,
      PN_OUT_COD_SOL_PRE               OUT                              NUMBER,
      PV_OUT_FEC_SOL_PRE               OUT                              VARCHAR2,
      PV_OUT_HORA_SOL_PRE              OUT                              VARCHAR2,
      PN_OUT_COD_RESULTADO             OUT                              NUMBER,
      PV_OUT_MSG_RESULTADO             OUT                              VARCHAR2
   ) IS

      V_COD_SCG_SEQ            NUMBER;
      V_COD_SCG_SOLBEN_COUNT   NUMBER;
      V_COD_SOL_PRE_SEQ        NUMBER;
      V_RANGO_TIEMPO           NUMBER;
      V_FEC_SOL_PRE            VARCHAR2(20);
      V_HORA_SOL_PRE           VARCHAR2(20);
      V_SOLBEN_CASE2_COUNT     NUMBER;
      V_SOLBEN_CASE3_COUNT     NUMBER;
      V_VALOR                  NUMBER;
      V_FECHA_RECETA           DATE;
      V_FECHA_QUIMIO           DATE;
      V_FECHA_AFILIACION       DATE;
      V_FECHA_SCG_SOLBEN       DATE;
      V_FECHA_HOSP_INI         DATE;
      V_FECHA_HOSP_FIN         DATE;
      V_FECHA_HORA_ACTUAL      DATE;
      V_FECHA_ACTUAL           DATE;
      V_EXC_FEC_ACTUAL EXCEPTION;
      V_EXC_CAMPO_VALIDA EXCEPTION;
      V_FECHA_SCG_SOLBEN_EXIST DATE;
      V_COD_SOLICITUD_EXIST    NUMBER;
        /*V_CODIGO                 NUMBER(1);*/
      V_TIPO_MEDICO            NUMBER;
      V_MENSAJE                VARCHAR2(500);
      V_COD_SCG_PRE            NUMBER;
      V_COD_SOL_PRE            NUMBER;
      V_FEC_CREA_SOL           DATE;
      V_COD_SOL_PRE_EXIST_QFC  NUMBER;
      V_COD_SOL_PRE_EXIST_H    NUMBER;
               
   BEGIN
      PN_OUT_COD_RESULTADO   := -1;
      PV_OUT_MSG_RESULTADO   := 'valor inicial';
        
        /*VALIDAR CAMPOS OBLIGATORIOS*/
      CASE
         WHEN NULLIF( PV_COD_SCG_SOLBEN,  ''  ) IS NULL THEN
            V_MENSAJE := 'Campo "codScgSolben" solben obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF( PV_COD_CLINICA, '' ) IS NULL THEN
            V_MENSAJE := 'Campo "codClinica" obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF( PV_COD_AFI_PACIENTE, '') IS NULL THEN
            V_MENSAJE := 'Campo "codAfiPaciente" obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(PN_EDAD_PACIENTE,'' ) IS NULL THEN
            V_MENSAJE := 'campo edad paciente obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN PN_EDAD_PACIENTE < 0 OR PN_EDAD_PACIENTE > 120 THEN
            V_MENSAJE := 'campo "edadPaciente" debe estar comprendido entre 0 y 120';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(PV_DES_CONTRATANTE, '') IS NULL THEN
            V_MENSAJE := 'campo "desContratante" obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(PV_DES_PLAN,'') IS NULL THEN
            V_MENSAJE := 'campo "desPlan" obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(PV_FEC_AFILIACION,'') IS NULL THEN
            V_MENSAJE := 'Campo "fecAfiliacion" es obligatorio/formato(dd-mm-yyyy)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(PV_COD_DIAGNOSTICO, '' ) IS NULL THEN
            V_MENSAJE := 'Campo "codDiagnostico" obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(PV_CMP_MEDICO, '' ) IS NULL THEN
            V_MENSAJE := 'Campo "cmpMedico" obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(PV_MEDICO_TRATANTE_PRESCRIPTOR,  '' ) IS NULL THEN
            V_MENSAJE := 'Campo medico tratante prescriptor obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF( PV_TIPO_SCG_SOLBEN, '' ) IS NULL THEN
            V_MENSAJE := 'Campo "tipoScgSolben" obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN ( PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_FARMACIA_COMPLEJA OR PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION
         OR PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA ) = FALSE THEN
            V_MENSAJE := 'Código de tipo de solicitud "'
                         || PV_TIPO_SCG_SOLBEN
                         || '" es diferente al establecido';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN ( PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_FARMACIA_COMPLEJA OR PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA
         ) AND NULLIF(PV_FEC_RECETA,'') IS NULL THEN
            V_MENSAJE := 'Campo fecha receta es obligatorio/formato(dd-mm-yyyy) para el codigo de tipo de solicitud : ' || PV_TIPO_SCG_SOLBEN
            ;
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN ( PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_FARMACIA_COMPLEJA OR PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA
         ) AND FN_PFC_S_VALIDACION_FECHA(PV_FEC_RECETA) = 1 THEN
            V_MENSAJE := 'Campo fecha receta es obligatorio/formato(dd-mm-yyyy) formato incorrecto : ' || PV_FEC_RECETA;
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA AND NULLIF(
            PV_FEC_QUIMIO,'') IS NULL THEN
            V_MENSAJE := 'Campo fecha quimio ambulatoria es obligatorio/formato(dd-mm-yyyy) para el codigo de tipo de solicitud : '
            || PV_TIPO_SCG_SOLBEN;
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA 
           AND FN_PFC_S_VALIDACION_FECHA(PV_FEC_QUIMIO) = 1 THEN
            V_MENSAJE := 'Campo fecha quimio ambulatoria es obligatorio/formato(dd-mm-yyyy) formato incorrecto : '|| PV_FEC_QUIMIO;
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION AND NULLIF(
            PV_FEC_HOSP_INICIO,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'Campo fecha hospitalizacion inicio es obligatorio/formato(dd-mm-yyyy)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION AND NULLIF(
            PV_FEC_HOSP_FIN,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'Campo fecha hospitalizacion fin es obligatorio/formato(dd-mm-yyyy)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_FEC_SCG_SOLBEN,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'Campo fecha de la solicitud de creacion solben es obligatorio/formato(dd-mm-yyyy)';
            DBMS_OUTPUT.put_line('CAMPO FECHA DE LA SOLICITUD DE CREACION SOLBEN: '||PV_FEC_SCG_SOLBEN);
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_HORA_SCG_SOLBEN,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'Campo hora de la solicitud de creacion solben obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN FN_PFC_S_VALIDACION_HORA(PV_HORA_SCG_SOLBEN) = 1 THEN
            V_MENSAJE := 'Campo hora de la solicitud de creacion solben no cumple con el formato';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_ESTADO_SCG,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'Campo estado de la solicitud de carta de garantia solben obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PN_TOTAL_PRESUPUESTO,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'Campo total presupuesto obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PN_IND_VALIDA,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'Campo indice de validacion obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(
            PV_FEC_ACTUAL,
            ''
         ) IS NULL THEN
            V_MENSAJE := 'Campo fecha actual obligatorio/formato(dd-mm-yyyy  hh24:mi:ss)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN FN_PFC_S_VALIDACION_FECHA_HORA(PV_FEC_ACTUAL) = 1 THEN
          V_MENSAJE := 'Error en el tipo de formato(dd-mm-yyyy hh24:mi:ss)';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN LENGTH(PV_DES_PLAN) > 20 THEN
            V_MENSAJE := 'Error, se supera la longitud maxima del campo "desPlan"';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN NULLIF(PV_TX_DATO_ADIC1, '') IS NULL THEN
            V_MENSAJE := 'Campo "txDatoAdic1" es obligatorio';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN LENGTH(PV_TX_DATO_ADIC1) > 200 THEN
            V_MENSAJE := 'Error, se supera la longitud maxima del campo "txDatoAdic1"';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN LENGTH(PV_TX_DATO_ADIC2) > 200 THEN
            V_MENSAJE := 'Error, se supera la longitud maxima del campo "txDatoAdic2"';
            RAISE V_EXC_CAMPO_VALIDA;
         WHEN LENGTH(PV_TX_DATO_ADIC3) > 200 THEN
            V_MENSAJE := 'Error, se supera la longitud maxima del campo "txDatoAdic3"';
            RAISE V_EXC_CAMPO_VALIDA;
         ELSE
            V_MENSAJE := NULL;
      END CASE;
        /*-----------*/
            /*LA VALIDACION DE FECHA SI ES IGUAL A 1 ES INCORRECTO*/
        /*-----------*/

      BEGIN
         IF NULLIF( PV_FEC_RECETA, '' ) IS NULL THEN
            V_FECHA_RECETA := NULL;
         ELSE
            V_FECHA_RECETA := TO_DATE( PV_FEC_RECETA,'DD/MM/YYYY' );
         END IF;

         IF NULLIF( PV_FEC_QUIMIO, '' ) IS NULL THEN
            V_FECHA_QUIMIO := NULL;
         ELSE
            V_FECHA_QUIMIO := TO_DATE( PV_FEC_QUIMIO, 'DD/MM/YYYY');
         END IF;

         IF NULLIF( PV_FEC_AFILIACION,  '') IS NULL THEN
            V_FECHA_AFILIACION := NULL;
            V_MENSAJE := 'Campo fecha de afiliacion es obligatorio/formato(dd-mm-yyyy)';
            RAISE V_EXC_CAMPO_VALIDA;
         ELSE
            V_FECHA_AFILIACION := TO_DATE(PV_FEC_AFILIACION, 'DD/MM/YYYY' );
         END IF;
         DBMS_OUTPUT.put_line('------>');
         IF NULLIF( PV_FEC_SCG_SOLBEN,'') IS NULL THEN
            V_FECHA_SCG_SOLBEN := NULL;
            V_MENSAJE := 'Campo fecha de la solicitud de creacion solben es obligatorio/formato(dd-mm-yyyy)';
            DBMS_OUTPUT.put_line('FECHA--1>> PV_FEC_SCG_SOLBEN: '||PV_FEC_SCG_SOLBEN);
            
            RAISE V_EXC_CAMPO_VALIDA;
         ELSE
            V_FECHA_SCG_SOLBEN := TO_DATE( PV_FEC_SCG_SOLBEN, 'DD/MM/YYYY' );
            DBMS_OUTPUT.put_line('FECHA--2>> PV_FEC_SCG_SOLBEN: '||PV_FEC_SCG_SOLBEN);
         END IF;
         DBMS_OUTPUT.put_line('------>>>>>>');
         IF NULLIF(PV_FEC_HOSP_INICIO,'') IS NULL THEN
            V_FECHA_HOSP_INI := NULL;
         ELSE
            V_FECHA_HOSP_INI := TO_DATE( PV_FEC_HOSP_INICIO, 'DD/MM/YYYY' );
         END IF;

         IF NULLIF( PV_FEC_HOSP_FIN,  '' ) IS NULL THEN
            V_FECHA_HOSP_FIN := NULL;
         ELSE
            V_FECHA_HOSP_FIN := TO_DATE( PV_FEC_HOSP_FIN,'DD/MM/YYYY' );
         END IF;

      EXCEPTION
         WHEN OTHERS THEN
            V_MENSAJE := 'ERROR EN EL FORMATO DE FECHA : '|| DBMS_UTILITY.FORMAT_ERROR_BACKTRACE||' ' || SQLERRM;
            DBMS_OUTPUT.put_line('>>>>> V_MENSAJE: '||V_MENSAJE);
      END;

      BEGIN
         SELECT
            TO_DATE( PV_FEC_ACTUAL, 'DD/MM/YYYY HH24:MI:SS' ),
            TO_DATE( TO_CHAR( TO_DATE( PV_FEC_ACTUAL,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY'), 'DD/MM/YYYY' )
         INTO
            V_FECHA_HORA_ACTUAL,
            V_FECHA_ACTUAL
         FROM
            DUAL;

      EXCEPTION
         WHEN OTHERS THEN
            V_MENSAJE := 'ERROR EN LA FECHA ACTUAL : '|| DBMS_UTILITY.FORMAT_ERROR_BACKTRACE||' ' || SQLERRM;
            RAISE V_EXC_FEC_ACTUAL;
      END;

      
    /*VALIDA SI EL 'COD_SCG_SOLBEN' QUE ENVIA SOLBEN EXISTE EN LA TABLA SOLBEN*/

      SELECT
         COUNT(SB.COD_SCG)
      INTO V_COD_SCG_SOLBEN_COUNT
      FROM
         PFC_SOLBEN                 SB,
         PFC_SOLICITUD_PRELIMINAR   PRE
      WHERE
         SB.COD_SCG = PRE.COD_SCG
         AND SB.COD_SCG_SOLBEN = PV_COD_SCG_SOLBEN;
       
            
    /*VALIDATE RANGO DE TIEMPO */

      SELECT
         TRUNC(V_FECHA_ACTUAL) - TRUNC(V_FECHA_SCG_SOLBEN) AS RANGOTIEMPO
      INTO V_RANGO_TIEMPO
      FROM DUAL;

      IF PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_FARMACIA_COMPLEJA OR
        PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA THEN
         
         V_TIPO_MEDICO := PCK_PFC_CONSTANTE.PN_CODIGO_MEDICO_PRESCRIPTOR;
         BEGIN
           SELECT
              TO_NUMBER( P.VALOR1 )
           INTO V_VALOR
           FROM
              PFC_PARAMETRO P
           WHERE
              P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_RANGO_TIEM_SOLBEN_FC_QUIMIO
              AND P.ESTADO = PCK_PFC_CONSTANTE.PV_ESTADO_PARAMETRO;

         EXCEPTION
           WHEN NO_DATA_FOUND THEN
              V_VALOR     := 0;
              V_MENSAJE   := 'No se encontraron resultados para el rango de tiempo solben en los parametros';
              RAISE V_EXC_CAMPO_VALIDA;
         END;
    
      ELSIF PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION THEN
         
         V_TIPO_MEDICO := PCK_PFC_CONSTANTE.PN_CODIGO_MEDICO_TRATANTE;
         BEGIN
           SELECT
              TO_NUMBER( P.VALOR1 )
           INTO V_VALOR
           FROM
              PFC_PARAMETRO P
           WHERE
              P.COD_PARAMETRO = PCK_PFC_CONSTANTE.PN_RANGO_TIEMPO_SOLBEN_HOSPIT
              AND P.ESTADO = PCK_PFC_CONSTANTE.PV_ESTADO_PARAMETRO;

         EXCEPTION
           WHEN NO_DATA_FOUND THEN
              V_VALOR     := 0;
              V_MENSAJE   := 'No se encontraron resultados para el rango de tiempo solben en los parametros';
              RAISE V_EXC_CAMPO_VALIDA;
         END;
    
      END IF;
        
    /*GET NUEVO REGISTRO DE UNA SCG DE SOLBEN*/

      CASE
         WHEN V_COD_SCG_SOLBEN_COUNT = 0 THEN
            IF ( PV_COD_AFI_PACIENTE IS NULL AND PV_COD_DIAGNOSTICO IS NULL ) = FALSE THEN
               SELECT
                  COUNT(SB.COD_SCG)
               INTO V_SOLBEN_CASE2_COUNT
               FROM
                  PFC_SOLBEN SB
               WHERE
                  SB.COD_DIAGNOSTICO       = PV_COD_DIAGNOSTICO
                  AND SB.COD_AFI_PACIENTE  = PV_COD_AFI_PACIENTE
                  AND SB.FEC_RECETA        = V_FECHA_RECETA
                  --AND SB.IND_VALIDA        = PCK_PFC_CONSTANTE.PN_IND_VALIDA_ACTIVO
                  AND ( PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_FARMACIA_COMPLEJA
                        OR PV_TIPO_SCG_SOLBEN    = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA )
                  AND V_RANGO_TIEMPO <= PCK_PFC_CONSTANTE.PN_RANG_TIEM_SOLBEN_FC_QUIMIO;

               IF V_SOLBEN_CASE2_COUNT > 0 THEN
                 SELECT
                      SB.FEC_SCG_SOLBEN,
                      SB.COD_SCG_SOLBEN
                 INTO 
                      V_FECHA_SCG_SOLBEN_EXIST,
                      V_COD_SOLICITUD_EXIST
                 FROM
                    PFC_SOLBEN SB
                 WHERE
                    SB.COD_DIAGNOSTICO = PV_COD_DIAGNOSTICO
                    AND SB.COD_AFI_PACIENTE  = PV_COD_AFI_PACIENTE
                    AND SB.FEC_RECETA        = V_FECHA_RECETA
                    --AND SB.IND_VALIDA        = PCK_PFC_CONSTANTE.PN_IND_VALIDA_ACTIVO
                    AND ( PV_TIPO_SCG_SOLBEN = PCK_PFC_CONSTANTE.PN_TIPO_FARMACIA_COMPLEJA
                          OR PV_TIPO_SCG_SOLBEN    = PCK_PFC_CONSTANTE.PN_TIPO_QUIMIO_AMBULATORIA )
                    AND V_RANGO_TIEMPO <= PCK_PFC_CONSTANTE.PN_RANG_TIEM_SOLBEN_FC_QUIMIO
                    AND ROWNUM=1 
                    ORDER BY SB.FEC_SCG_SOLBEN DESC;
                 
                 SELECT MAX(SP.COD_SOL_PRE)
                   INTO V_COD_SOL_PRE_EXIST_QFC
                   FROM PFC_SOLICITUD_PRELIMINAR SP
                   INNER JOIN PFC_SOLBEN SO
                   ON SP.COD_SCG = SO.COD_SCG
                   WHERE SO.COD_SCG_SOLBEN = V_COD_SOLICITUD_EXIST;
                 
                  PN_OUT_COD_RESULTADO   := 3;
                  PV_OUT_MSG_RESULTADO   := 'Para la información ingresada ya se encuentra registrada la solicitud preliminar número ' ||
                                             V_COD_SOL_PRE_EXIST_QFC || ' con fecha de creación ' ||
                                             V_FECHA_SCG_SOLBEN_EXIST ||' en el Módulo de Farmacia Compleja.';
               ELSE
                  SELECT
                     COUNT(SB.COD_SCG)
                  INTO V_SOLBEN_CASE3_COUNT
                  FROM
                     PFC_SOLBEN SB
                  WHERE
                     SB.COD_DIAGNOSTICO = PV_COD_DIAGNOSTICO
                     AND SB.COD_AFI_PACIENTE  = PV_COD_AFI_PACIENTE
                     --AND SB.IND_VALIDA        = PCK_PFC_CONSTANTE.PN_IND_VALIDA_ACTIVO
                     AND SB.TIPO_SCG_SOLBEN   = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION
                     AND V_RANGO_TIEMPO <= PCK_PFC_CONSTANTE.PN_RANG_TIEM_SOLBEN_HOSPIT;

                  IF V_SOLBEN_CASE3_COUNT > 0 THEN
                    
                   SELECT
                        SB.FEC_SCG_SOLBEN,
                        SB.COD_SCG_SOLBEN
                     INTO 
                        V_FECHA_SCG_SOLBEN_EXIST,
                        V_COD_SOLICITUD_EXIST
                    FROM
                       PFC_SOLBEN SB
                    WHERE
                       SB.COD_DIAGNOSTICO = PV_COD_DIAGNOSTICO
                       AND SB.COD_AFI_PACIENTE  = PV_COD_AFI_PACIENTE
                       --AND SB.IND_VALIDA        = PCK_PFC_CONSTANTE.PN_IND_VALIDA_ACTIVO
                       AND SB.TIPO_SCG_SOLBEN   = PCK_PFC_CONSTANTE.PN_TIPO_HOSPITALIZACION
                       AND V_RANGO_TIEMPO <= PCK_PFC_CONSTANTE.PN_RANG_TIEM_SOLBEN_HOSPIT;
                       
                   SELECT MAX(SP.COD_SOL_PRE)
                     INTO V_COD_SOL_PRE_EXIST_H
                     FROM PFC_SOLICITUD_PRELIMINAR SP
                    INNER JOIN PFC_SOLBEN SO
                    ON SP.COD_SCG = SO.COD_SCG
                    WHERE SO.COD_SCG_SOLBEN = V_COD_SOLICITUD_EXIST;

                     PN_OUT_COD_RESULTADO   := 4;
                     PV_OUT_MSG_RESULTADO   := 'Para la información ingresada ya se encuentra registrada la solicitud preliminar número ' ||
                                             V_COD_SOL_PRE_EXIST_H || ' con fecha de creación ' ||
                                             V_FECHA_SCG_SOLBEN_EXIST ||' en el módulo de Farmacia Compleja.';
                  ELSE
                     SELECT
                        SQ_PFC_COD_SOLBEN.NEXTVAL
                     INTO V_COD_SCG_SEQ
                     FROM
                        DUAL; /* SECUENCIADOR DE ID SOLBEN*/
                            
                            /*INSERTAR REGISTRO EN LA TABLA SOLBEN*/
                     DBMS_OUTPUT.put_line('FECHA V_FECHA_SCG_SOLBEN: '||V_FECHA_SCG_SOLBEN);
                     INSERT INTO PFC_SOLBEN (
                        COD_SCG,
                        COD_SCG_SOLBEN,
                        COD_CLINICA,
                        COD_AFI_PACIENTE,
                        EDAD_PACIENTE,
                        DES_CONTRATANTE,
                        DES_PLAN,
                        COD_DIAGNOSTICO,
                        CMP_MEDICO,
                        FEC_QUIMIO,
                        FEC_HOSP_INICIO,
                        FEC_HOSP_FIN,
                        TIPO_SCG_SOLBEN,
                        ESTADO_SCG,
                        DESC_MEDICAMENTO,
                        DESC_ESQUEMA,
                        TOTAL_PRESUPUESTO,
                        DESC_PROCEDIMIENTO,
                        PERSON_CONTACTO,
                        OBS_CLINICA,
                        IND_VALIDA,
                        FEC_SCG_SOLBEN,
                        HORA_SCG_SOLBEN,
                        TX_DATO_ADIC1,
                        TX_DATO_ADIC2,
                        TX_DATO_ADIC3,
                        FEC_RECETA,
                        MEDICO_TRATANTE_PRESCRIPTOR,
                        FEC_AFILIACION,
                        P_TIPO_MEDICO,
                        COD_GRP_DIAG,
                        FECHA_CREACION
                     ) VALUES (
                        V_COD_SCG_SEQ,
                        PV_COD_SCG_SOLBEN,
                        PV_COD_CLINICA,
                        PV_COD_AFI_PACIENTE,
                        PN_EDAD_PACIENTE,
                        PV_DES_CONTRATANTE,
                        PV_DES_PLAN,
                        PV_COD_DIAGNOSTICO,
                        PV_CMP_MEDICO,
                        V_FECHA_QUIMIO,/*CAMBIAR VARIABLE NO OBLIG*/
                        V_FECHA_HOSP_INI,/*CAMBIAR VARIABLE NO OBLIG*/
                        V_FECHA_HOSP_FIN,/*CAMBIAR VARIABLE NO OBLIG*/
                        PV_TIPO_SCG_SOLBEN,
                        PV_ESTADO_SCG,
                        PV_DESC_MEDICAMENTO,
                        PV_DESC_ESQUEMA,
                        PN_TOTAL_PRESUPUESTO,
                        PV_DESC_PROCEDIMIENTO,
                        PV_PERSON_CONTACTO,
                        PV_OBS_CLINICA,
                        PN_IND_VALIDA,
                        V_FECHA_SCG_SOLBEN,/*CAMBIAR VARIABLE SI OBLIG*/
                        PV_HORA_SCG_SOLBEN,
                        PV_TX_DATO_ADIC1,
                        PV_TX_DATO_ADIC2,
                        PV_TX_DATO_ADIC3,
                        V_FECHA_RECETA,/*CAMBIAR VARIABLE NO OBLIG*/
                        PV_MEDICO_TRATANTE_PRESCRIPTOR,
                        V_FECHA_AFILIACION,/*CAMBIAR VARIABLE SI OBLIG*/
                        V_TIPO_MEDICO,
                        PV_COD_GRP_DIAGNOS,
                        SYSDATE
                     );

                     SELECT
                        SQ_PFC_SOL_PRE_COD_SOL_PRE.NEXTVAL
                     INTO V_COD_SOL_PRE_SEQ
                     FROM
                        DUAL; /* SECUENCIADOR DE ID PRELIMINAR*/
            
              /*INSERT REGISTRO A LA TABLA PRELIMINAR*/

                     INSERT INTO PFC_SOLICITUD_PRELIMINAR (
                        COD_SOL_PRE,
                        P_ESTADO_SOL_PRE,
                        COD_SCG,
                        FEC_SOL_PRE
                     ) VALUES (
                        V_COD_SOL_PRE_SEQ,
                        PCK_PFC_CONSTANTE.PN_P_ESTADO_PENDIENTE, /* P_ESTADO_SOL_PRE 'PENDIENTE'*/
                        V_COD_SCG_SEQ,
                        V_FECHA_HORA_ACTUAL
                     );
              /*SAVE CAMBIOS DE INSERCION EN LA TABLA SOLBEN Y PRELIMINAR*/
            
              /*CASTEO DE FEC_SOL_PRE*/

                     SELECT
                        TO_CHAR(
                           CAST(FEC_SOL_PRE AS DATE),
                           'DD/MM/YYYY'
                        ) AS FECHA,
                        TO_CHAR(
                           CAST(FEC_SOL_PRE AS DATE),
                           'HH24:MI:SS'
                        ) AS HORA
                     INTO
                        V_FEC_SOL_PRE,
                        V_HORA_SOL_PRE
                     FROM
                        PFC_SOLICITUD_PRELIMINAR SP
                     WHERE
                        SP.COD_SCG = V_COD_SCG_SEQ;
            
              /*SALIDA CASO 1*/

                     PV_OUT_COD_SCG_SOLBEN   := PV_COD_SCG_SOLBEN;
                     PN_OUT_COD_SOL_PRE      := V_COD_SOL_PRE_SEQ;
                     PV_OUT_FEC_SOL_PRE      := V_FEC_SOL_PRE;
                     PV_OUT_HORA_SOL_PRE     := V_HORA_SOL_PRE;
                     PN_OUT_COD_RESULTADO    := 0;
                     PV_OUT_MSG_RESULTADO    := 'No hay error. Se registró la solicitud preliminar correctamente';
                  END IF;

               END IF;

            ELSE
        
          /*SALIDA CASO 4*/
               PN_OUT_COD_RESULTADO   := 5;
               PV_OUT_MSG_RESULTADO   := 'El paciente "'
               || PV_COD_AFI_PACIENTE
               || '" no se encuentra registrado en Oncosys y/o el código de diagnóstico "'
               || PV_COD_DIAGNOSTICO 
               || '" no se encuentra registrado en el Oncosys.';
            END IF;
         WHEN V_COD_SCG_SOLBEN_COUNT > 0 THEN
            /*SALIDA CASO 1*/
            
            BEGIN
               SELECT
                  SB.COD_SCG,
                  PRE.COD_SOL_PRE,
                  SB.FECHA_CREACION
               INTO
                  V_COD_SCG_PRE,
                  V_COD_SOL_PRE,
                  V_FEC_CREA_SOL
               FROM
                  PFC_SOLBEN                 SB,
                  PFC_SOLICITUD_PRELIMINAR   PRE
               WHERE
                  SB.COD_SCG = PRE.COD_SCG
                  AND SB.COD_SCG_SOLBEN = PV_COD_SCG_SOLBEN;

               PN_OUT_COD_RESULTADO   := 2;
               PV_OUT_MSG_RESULTADO   := 'El número de Solicitud de Carta de Garantía "'
                                       || PV_COD_SCG_SOLBEN
                                       || '" ya se encuentra registrada en el Módulo de Farmacia Compleja con el número de solicitud preliminar "'
                                       || V_COD_SOL_PRE
                                       || ' con fecha de creación " '
                                       || TO_CHAR(V_FEC_CREA_SOL, 'DD/MM/YYYY')
                                       ||'"';
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                PN_OUT_COD_RESULTADO   := -3;
               PV_OUT_MSG_RESULTADO   := 'Error interno del servidor no se pudo obtener resultados del código de solicitud de CG Solben: '
                                       || PV_COD_SCG_SOLBEN;
              
            END;
      END CASE;

      COMMIT;
   EXCEPTION
      WHEN V_EXC_CAMPO_VALIDA THEN
         PN_OUT_COD_RESULTADO   := 8;
         PV_OUT_MSG_RESULTADO   := 'Error validación de datos: ' || V_MENSAJE;
      WHEN V_EXC_FEC_ACTUAL THEN
         PN_OUT_COD_RESULTADO   := -2;
         PV_OUT_MSG_RESULTADO   := '[[SP_GET_SCG_AND_INSERT_PREL]] ' || V_MENSAJE;
      WHEN OTHERS THEN
         PN_OUT_COD_RESULTADO   := -2;
         PV_OUT_MSG_RESULTADO   := '[[SP_GET_SCG_AND_INSERT_PREL]] '
                                 || ' : '
                                 || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE
                                 || ' : '
                                 || TO_CHAR(SQLCODE)
                                 || ' : '
                                 || SQLERRM;

         ROLLBACK;
   END SP_PFC_SI_SCG_SOLBEN_PREL;


  /* AUTOR     : CHRISTIAN ALTAMIRANO*/
  /* CREADO    : 18/01/2019*/
  /* PROPOSITO : ACTUALIZAR SERVICIO ACTUALIZAR*/

   PROCEDURE SP_PFC_SU_SOLBEN_ACTUALIZAR (
      PV_COD_SCG_SOLBEN     IN OUT                VARCHAR2,
      PV_COD_AFI_PACIENTE   IN                    VARCHAR2,
      PV_COD_ESTADO_SCG     IN                    NUMBER,
      PN_TIPO_OPERACION     IN                    NUMBER,
      PV_NRO_CG             IN                    VARCHAR2,
      PV_FEC_CG             IN                    VARCHAR2,
      PV_TX_DATO_ADIC1      IN OUT                VARCHAR2,
      PV_TX_DATO_ADIC2      IN OUT                VARCHAR2,
      PV_TX_DATO_ADIC3      IN OUT                VARCHAR2,
      PV_COD_SOL_EVAL       OUT                   VARCHAR2,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS

      V_COD_SCG          NUMBER;
      V_EXCEPTION        EXCEPTION;
      V_EXCEPTION_DATOS  EXCEPTION;
      V_EXC_FECHA        EXCEPTION;
      V_COD_SOL_EVA      NUMBER;
      V_MSG              VARCHAR2(200);
      V_CANT_SOL         NUMBER;
      V_ESTADO_SOL_EVA   NUMBER;
     BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      CASE
         WHEN NULLIF(PV_COD_SCG_SOLBEN, '') IS NULL THEN
            V_MSG := 'El codigo SCG es obligatorio.';
            RAISE V_EXCEPTION_DATOS;
         WHEN NULLIF(PV_COD_AFI_PACIENTE,'') IS NULL THEN
            V_MSG := 'El codigo de afiliado es obligatorio.';
            RAISE V_EXCEPTION_DATOS;
         WHEN NULLIF(PN_TIPO_OPERACION, '') IS NULL THEN
            V_MSG := 'El tipo de operación es obligatorio.';
            RAISE V_EXCEPTION_DATOS;
         WHEN PN_TIPO_OPERACION <> 1 AND PN_TIPO_OPERACION <> 2 THEN
            V_MSG := 'Tipo de operación no permitida. (1 y 2) Actual: '||PN_TIPO_OPERACION;
            RAISE V_EXCEPTION_DATOS;
         WHEN NULLIF(PV_COD_ESTADO_SCG, '') IS NULL THEN
            V_MSG := 'El codigo de estado de la carta garantía es obligatorio.';
            RAISE V_EXCEPTION_DATOS;
         WHEN PV_COD_ESTADO_SCG NOT IN ( 1,2,3,4,5, 6,7, 8) THEN
            V_MSG := 'Codigo de estado no permitido.';
            RAISE V_EXCEPTION_DATOS;
         ELSE
            V_MSG := 'Sin error en validaciones.';
      END CASE;
     
    SELECT COUNT(SOLB.COD_SCG_SOLBEN)
       INTO V_CANT_SOL
       FROM PFC_SOLBEN SOLB
      INNER JOIN PFC_SOLICITUD_PRELIMINAR SP
         ON SOLB.COD_SCG = SP.COD_SCG
      WHERE SOLB.COD_SCG_SOLBEN = PV_COD_SCG_SOLBEN;
        
     IF  V_CANT_SOL = 0 THEN
       PN_COD_RESULTADO  := 5;
       PV_MSG_RESULTADO  := 'El código de la Solicitud de CG Solben: '||PV_COD_SCG_SOLBEN||
                            ' no se encuentra registrada en el Módulo de Farmacia Compleja.';
       RETURN;
     END IF;
 
     SELECT COUNT(SOLB.COD_SCG_SOLBEN)
       INTO V_CANT_SOL
       FROM PFC_SOLBEN SOLB
      INNER JOIN PFC_SOLICITUD_PRELIMINAR SP
         ON SOLB.COD_SCG = SP.COD_SCG
      WHERE SOLB.COD_SCG_SOLBEN = PV_COD_SCG_SOLBEN
        AND SOLB.COD_AFI_PACIENTE = PV_COD_AFI_PACIENTE;
        
     IF  V_CANT_SOL = 0 THEN
       PN_COD_RESULTADO  := 5;
       PV_MSG_RESULTADO  := 'El código de la Solicitud de CG Solben: '||PV_COD_SCG_SOLBEN||
                            ' no tiene relacion con el codigo de Afiliado: '||PV_COD_AFI_PACIENTE||
                            ' en este envío en el Módulo de Farmacia Compleja.';
       DBMS_OUTPUT.PUT_LINE('SOLICITUD EN PRELIMINAR');
       RETURN;
     END IF;
     DBMS_OUTPUT.PUT_LINE('PASO ACA');

      BEGIN
         SELECT
            SOLB.COD_SCG,
            SE.COD_SOL_EVA,
            SE.P_ESTADO_SOL_EVA
         INTO
            V_COD_SCG,
            V_COD_SOL_EVA,
            V_ESTADO_SOL_EVA
         FROM
            PFC_SOLBEN                 SOLB
            INNER JOIN PFC_SOLICITUD_PRELIMINAR   SP ON SOLB.COD_SCG = SP.COD_SCG
            INNER JOIN PFC_SOLICITUD_EVALUACION   SE ON SE.COD_SOL_PRE = SP.COD_SOL_PRE
         WHERE
            SOLB.COD_SCG_SOLBEN = PV_COD_SCG_SOLBEN
            AND SOLB.COD_AFI_PACIENTE = PV_COD_AFI_PACIENTE;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_COD_SCG       := NULL;
            V_COD_SOL_EVA   := NULL;
         WHEN OTHERS THEN
            V_COD_SCG       := NULL;
            V_COD_SOL_EVA   := NULL;
      END;

    /*LA VALIDACION DE FECHA SI ES IGUAL A 1 ES INCORRECTO*/

      CASE
 
         WHEN FN_PFC_S_VALIDACION_FECHA_HORA(PV_FEC_CG) = 1 THEN
            V_MSG := 'Error al procesar fecha de carta garantía.';
            RAISE V_EXC_FECHA;
         ELSE
            V_MSG := 'Sin error en la validacion de formato de fecha.';
      END CASE;

      IF V_COD_SOL_EVA IS NULL THEN
         V_MSG := 'La Solicitud de CG continúa en evaluación en el Módulo de Farmacia Compleja.';
         RAISE V_EXCEPTION;
      ELSIF V_ESTADO_SOL_EVA NOT IN (PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_AUTORIZ, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_AUTORIZ,
            PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_LIDER_T, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_LIDER_T,
            PCK_PFC_CONSTANTE.PN_ESTADO_APROBADO_CMAC, PCK_PFC_CONSTANTE.PN_ESTADO_RECHAZADO_CMAC)  THEN
         V_MSG := 'La Solicitud de CG todavía se encuentra en proceso de evaluación en el Módulo de Farmacia Compleja';
         RAISE V_EXCEPTION;
      END IF;
    
      IF PN_TIPO_OPERACION = 2 THEN
         IF PV_COD_ESTADO_SCG = 6 THEN
            UPDATE PFC_SOLBEN S
            SET
               S.ESTADO_SCG = PV_COD_ESTADO_SCG,
               S.NRO_CG = PV_NRO_CG,
               S.FEC_CG = PV_FEC_CG,
               S.TX_DATO_ADIC1 = PV_TX_DATO_ADIC1,
               S.TX_DATO_ADIC2 = PV_TX_DATO_ADIC2,
               S.TX_DATO_ADIC3 = PV_TX_DATO_ADIC3,
               S.FECHA_MODIFICACION=SYSDATE
            WHERE
               S.COD_SCG = V_COD_SCG;
               
            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'Actualización Exitosa.';

         ELSE
            UPDATE PFC_SOLBEN S
            SET
               S.ESTADO_SCG = PV_COD_ESTADO_SCG,
               S.TX_DATO_ADIC1 = PV_TX_DATO_ADIC1,
               S.TX_DATO_ADIC2 = PV_TX_DATO_ADIC2,
               S.TX_DATO_ADIC3 = PV_TX_DATO_ADIC3,
               S.FECHA_MODIFICACION=SYSDATE
            WHERE
               S.COD_SCG = V_COD_SCG;
               
            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'Actualización Exitosa.';

         END IF;
      ELSIF PN_TIPO_OPERACION = 1 THEN
        IF V_COD_SOL_EVA IS NOT NULL THEN 
          SELECT  SOLEV.P_ESTADO_SOL_EVA 
          INTO PV_TX_DATO_ADIC1
          FROM  PFC_SOLICITUD_EVALUACION SOLEV
          WHERE SOLEV.COD_SOL_EVA=V_COD_SOL_EVA;
          
          IF PV_TX_DATO_ADIC1 IN (20,23,26) THEN
            PN_COD_RESULTADO   := 3;
            PV_MSG_RESULTADO   := 'La  Solicitud de CG se encuentra Aprobada en el Módulo de Farmacia Compleja.';
          ELSIF PV_TX_DATO_ADIC1 IN (21,24,27) THEN
            PN_COD_RESULTADO   := 4;
            PV_MSG_RESULTADO   := 'La  Solicitud de CG se encuentra Rechazada en el Módulo de Farmacia Compleja.';
          
          ELSIF PV_TX_DATO_ADIC1 IN (13,14,15,16,17,18,19) THEN
            PN_COD_RESULTADO   := 2;
            PV_MSG_RESULTADO   := 'La Solicitud de CG continúa en evaluación en el Módulo de Farmacia Compleja.';
        
          END IF;    
        
        END IF;   
      END IF;

      PV_COD_SOL_EVAL    := TO_CHAR(V_COD_SOL_EVA);
      
      COMMIT;
   EXCEPTION
      WHEN V_EXCEPTION_DATOS THEN
         PN_COD_RESULTADO   := 6;
         PV_MSG_RESULTADO   := 'ERROR DE VALIDACION DE DATOS: ' || V_MSG;
      WHEN V_EXC_FECHA THEN
         PN_COD_RESULTADO   := 3;
         PV_MSG_RESULTADO   := V_MSG;
      WHEN V_EXCEPTION THEN
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := V_MSG;
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SU_SOLBEN_ACTUALIZAR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SU_SOLBEN_ACTUALIZAR;

END PCK_PFC_SOLBEN;

/
--------------------------------------------------------
--  DDL for Package Body PCK_PFC_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "ONTPFC"."PCK_PFC_UTIL" AS

  /* AUTOR     : AFLORES*/
  /* CREADO    : 18/01/2019*/
  /* PROPOSITO : FILTRO DE PARAMETROS*/

   PROCEDURE SP_PFC_S_FILTRO_PARAMETRO (
      PV_COD_GRUPO       IN                 VARCHAR2,
      TC_LIST_PARAM      OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) IS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN TC_LIST_PARAM FOR SELECT
                                B.COD_PARAMETRO,
                                B.NOMBRE,
                                B.CODIGO,
                                B.VALOR1
                             FROM
                                ONTPFC.PFC_PARAMETRO   B,
                                ONTPFC.PFC_GRUPO       G
                             WHERE
                                B.COD_GRUPO = G.CODIGO
                                AND G.CODIGO  = PV_COD_GRUPO
                                AND G.ESTADO  = PCK_PFC_CONSTANTE.PV_ESTADO_GRUPO
                                AND B.ESTADO  = PCK_PFC_CONSTANTE.PV_ESTADO_PARAMETRO;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta Exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_FILTRO_PARAMETRO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM|| ' LINEA : '
                             || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
   END SP_PFC_S_FILTRO_PARAMETRO;

  /* AUTOR     : CATALINA*/
  /* CREADO    : 18/01/2019*/
  /* PROPOSITO : FILTRO DE USUARIO POR ROL*/

   PROCEDURE SP_S_FILTRO_USR_ROL (
      PN_CODIGO          IN                 VARCHAR,
      AC_LISTA           OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) IS
      PN_COD_RESUL    NUMBER;
      PV_MSG_RESUL    VARCHAR2(1000);
      PN_CODIGO_USR   NUMBER := NULL;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';

      /*ONTPPA.PCK_PPA_USUARIO_ROL.SP_PPA_S_AUTORIZADOR(PN_CODIGO,
                                                      PN_CODIGO_USR,
                                                      AC_LISTA,
                                                      PN_COD_RESUL,
                                                      PV_MSG_RESUL);*/
      PN_COD_RESULTADO   := PN_COD_RESUL;
      PV_MSG_RESULTADO   := PV_MSG_RESUL;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_S_FILTRO_AUDITOR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM
                             || PV_MSG_RESUL;

   END SP_S_FILTRO_USR_ROL;

  /* AUTOR     : JAZABACHE*/
  /* CREADO    : 22/03/2019*/
  /* PROPOSITO : OBTENER ROL POR RESPONSABLE*/

   PROCEDURE SP_S_ROL_RESPONSABLE (
      TC_LIST_ROL        OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) IS
      PN_COD_RESUL   NUMBER;
      PV_MSG_RESUL   VARCHAR2(1000);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';

      /*ONTPPA.PCK_PPA_USUARIO_ROL.SP_PPA_S_ROL_RESP      (   TC_LIST_ROL,
                                                            PN_COD_RESUL,
                                                            PV_MSG_RESUL);*/
      PN_COD_RESULTADO   := PN_COD_RESUL;
      PV_MSG_RESULTADO   := PV_MSG_RESUL;
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_S_FILTRO_AUDITOR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM
                             || PV_MSG_RESUL;

   END SP_S_ROL_RESPONSABLE;


  /* AUTOR     : CATALINA*/
  /* CREADO    : 18/01/2019*/
  /* PROPOSITO : TIPO DE CORREO*/

   PROCEDURE SP_PFC_S_TIPO_CORREO (
      PN_COD_TIPO_CORREO   IN                   NUMBER,
      PV_ASUNTO            OUT                  VARCHAR2,
      PV_CUERPO            OUT                  VARCHAR2,
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      SELECT
         TC.ASUNTO,
         TC.CUERPO
      INTO
         PV_ASUNTO,
         PV_CUERPO
      FROM
         PFC_TIPO_CORREO TC
      WHERE
         TC.COD_TIPO_CORREO = PN_COD_TIPO_CORREO
         AND TC.ESTADO = '1';

      PN_COD_RESULTADO   := 1;
      PV_MSG_RESULTADO   := 'Consulta Exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_TIPO_CORREO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_TIPO_CORREO;

  /* AUTOR     : JAZABACHE*/
  /* CREADO    : 18/01/2019*/
  /* PROPOSITO : OBTENER CODIGO LARGO SEGUN LONGITUD*/

   PROCEDURE SP_PFC_CODIGO_LARGO (
      PN_COD             IN                 NUMBER,
      PN_LONGITUD        IN                 NUMBER,
      PV_COD_LARGO       OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      V_COD_LONGITUD     VARCHAR2(50);
      V_CEROS_LONGITUD   NUMBER;
      I                  NUMBER := 1;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      V_CEROS_LONGITUD   := PN_LONGITUD - LENGTH(PN_COD);
      DBMS_OUTPUT.PUT_LINE('LONGITUD DE CEROS :> ' || V_CEROS_LONGITUD);
      V_COD_LONGITUD     := '';
      FOR I IN 1..V_CEROS_LONGITUD LOOP
         V_COD_LONGITUD := V_COD_LONGITUD || '0';
         DBMS_OUTPUT.PUT_LINE(V_COD_LONGITUD);
      END LOOP;

      PV_COD_LARGO       := V_COD_LONGITUD || PN_COD;
      DBMS_OUTPUT.PUT_LINE('FIN :> ' || PV_COD_LARGO);
      IF V_CEROS_LONGITUD >= 0 THEN
         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Consulta Exitosa';
      ELSE
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'EL CODIGO INGRESADO SUPERA LA LONGITUD MAXIMA';
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_CODIGO_LARGO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_CODIGO_LARGO;
  /* AUTOR     : AFLORES*/
  /* CREADO    : 18/01/2019*/
  /* PROPOSITO : PARAMETRO VALOR*/

   PROCEDURE SP_PFC_S_PARAM_VAL (
      PN_COD_GRUPO       IN                 NUMBER,
      PN_CODIGO          IN                 VARCHAR2,
      PN_VALUE           IN                 VARCHAR2,
      PN_COD_PARAMETRO   IN                 NUMBER,
      TC_LIST_PAR        OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      L_SQL_STMT VARCHAR2(4000);
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      L_SQL_STMT         := 'SELECT PA.COD_PARAMETRO,PA.NOMBRE,PA.CODIGO,PA.VALOR1
                    FROM ONTPFC.PFC_GRUPO GR ,ONTPFC.PFC_PARAMETRO PA
                    WHERE GR.COD_GRUPO = PA.COD_GRUPO'
      ;
      IF NULLIF(
         PN_COD_GRUPO,
         ''
      ) IS NULL THEN
         L_SQL_STMT := L_SQL_STMT || ' AND 1=1 ';
      ELSE
         L_SQL_STMT := L_SQL_STMT
                       || ' AND GR.COD_GRUPO = '
                       || PN_COD_GRUPO;
      END IF;

      IF NULLIF(
         PN_CODIGO,
         ''
      ) IS NULL THEN
         L_SQL_STMT := L_SQL_STMT || ' AND 1=1 ';
      ELSE
         L_SQL_STMT := L_SQL_STMT
                       || ' AND PA.CODIGO = '
                       || PN_CODIGO;
      END IF;

      IF NULLIF(
         PN_VALUE,
         'null'
      ) IS NULL THEN
         L_SQL_STMT := L_SQL_STMT || ' AND 1=1 ';
      ELSE
         L_SQL_STMT := L_SQL_STMT
                       || ' AND PA.VALOR1 = '
                       || PN_VALUE;
      END IF;

      IF NULLIF(
         PN_COD_PARAMETRO,
         ''
      ) IS NULL THEN
         L_SQL_STMT := L_SQL_STMT || ' AND 1=1 ';
      ELSE
         L_SQL_STMT := L_SQL_STMT
                       || ' AND PA.COD_PARAMETRO = '
                       || PN_COD_PARAMETRO;
      END IF;

      INSERT INTO ONTPFC.PFC_LOG_SEGUIMIENTO (
         CODIGO,
         DESCRIPCION
      ) VALUES (
         '02',
         L_SQL_STMT
      );

      COMMIT;
      OPEN TC_LIST_PAR FOR L_SQL_STMT;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta Exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_PARAM_VAL]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_PARAM_VAL;

  /* AUTOR     : AFLORES*/
  /* CREADO    : 18/01/2019*/
  /* PROPOSITO : PARAMETRO DINAMICO*/

   PROCEDURE SP_PFC_S_PARAMETRO (
      PN_COD_PARAMETRO   IN                 NUMBER,
      TC_LIST            OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      OPEN TC_LIST FOR SELECT
                          PA.COD_PARAMETRO,
                          PA.NOMBRE,
                          PA.VALOR1
                       FROM
                          ONTPFC.PFC_GRUPO       GR,
                          ONTPFC.PFC_PARAMETRO   PA
                       WHERE
                          GR.COD_GRUPO = PA.COD_GRUPO
                          AND PA.COD_PARAMETRO = PN_COD_PARAMETRO;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'Consulta exitosa';
   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_PARAMETRO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_PARAMETRO;

  /* AUTOR      : AFLORES*/
  /* MODIFICADO : JAZABACHE*/
  /* CREADO     : 15/03/2019*/
  /* PROPOSITO  : Validar que el médico tratante no es el mismo médico asignado con rol de Líder Tumor asignado según Grupo Diagnóstico.*/

   PROCEDURE SP_PFC_S_VALID_LIDER_TUMOR (
      PV_CMP_MED             IN                     VARCHAR2,
      PV_COD_GRP_DIAG        IN                     VARCHAR2,
      PN_COD_ROL_LIDER_TUM   OUT                    NUMBER,
      PN_COD_USR_LIDER_TUM   OUT                    NUMBER,
      PV_USR_LIDER_TUM       OUT                    VARCHAR2,
      PN_CMP_LIDER_TUMOR     OUT                    NUMBER,
      PN_COD_RESULTADO       OUT                    NUMBER,
      PV_MSG_RESULTADO       OUT                    VARCHAR2
   ) AS
      V_MENSAJE         VARCHAR2(200);
      V_COD_RESULTADO   NUMBER;
      V_EXC_RESULTADO EXCEPTION;
   BEGIN
      PN_COD_RESULTADO     := -1;
      PV_MSG_RESULTADO     := 'valor inicial';
      PN_CMP_LIDER_TUMOR   := 1;
      IF NULLIF(
         PV_CMP_MED,
         ''
      ) IS NULL AND NULLIF(
         PV_COD_GRP_DIAG,
         ''
      ) IS NULL THEN
         V_COD_RESULTADO   := 2;
         V_MENSAJE         := 'NO SE ENCUENTRA EL CAMPO CMP MEDICO O GRUPO DE DIAGNOSTICO';
         RAISE V_EXC_RESULTADO;
      END IF;

      BEGIN
         SELECT
            PR.COD_ROL,
            PR.COD_PARTICIPANTE,
            PR.NOMBRES
            || ' '
            || PR.APELLIDOS
         INTO
            PN_COD_ROL_LIDER_TUM,
            PN_COD_USR_LIDER_TUM,
            PV_USR_LIDER_TUM
         FROM
            ONTPFC.PFC_PARTICIPANTE              PR
            INNER JOIN ONTPFC.PFC_PARTICIPANTE_GRUPO_DIAG   PRGD ON PRGD.COD_PARTICIPANTE = PR.COD_PARTICIPANTE
         WHERE
            PRGD.COD_GRP_DIAG = PV_COD_GRP_DIAG
            AND PR.CMP_MEDICO  = PV_CMP_MED
            AND PR.COD_ROL     = PCK_PFC_CONSTANTE.PN_CODIGO_LIDER_TUMOR;

         PN_CMP_LIDER_TUMOR   := 0;
         PN_COD_RESULTADO     := 0;
         PV_MSG_RESULTADO     := 'EL CMP DEL MEDICO TRATANTE ES EL MISMO QUE EL LIDER DE TUMOR SEGUN EL GRUPO DE DIAGNOSTICO';
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            PN_CMP_LIDER_TUMOR   := 1;
            V_COD_RESULTADO      := 0;
            V_MENSAJE            := 'EL CMP DEL MEDICO TRATANTE ES DIFERENTE QUE EL LIDER DE TUMOR SEGUN EL GRUPO DE DIAGNOSTICO';
      END;

      IF PN_CMP_LIDER_TUMOR = 1 THEN
         BEGIN
            SELECT
               PR.COD_ROL,
               PR.COD_PARTICIPANTE,
               PR.NOMBRES
               || ' '
               || PR.APELLIDOS
            INTO
               PN_COD_ROL_LIDER_TUM,
               PN_COD_USR_LIDER_TUM,
               PV_USR_LIDER_TUM
            FROM
               ONTPFC.PFC_PARTICIPANTE              PR
               INNER JOIN ONTPFC.PFC_PARTICIPANTE_GRUPO_DIAG   PRGD ON PRGD.COD_PARTICIPANTE = PR.COD_PARTICIPANTE
            WHERE
               PRGD.COD_GRP_DIAG = PV_COD_GRP_DIAG
               AND PR.COD_ROL = PCK_PFC_CONSTANTE.PN_CODIGO_LIDER_TUMOR;

            PN_COD_RESULTADO   := 0;
            PV_MSG_RESULTADO   := 'CONSULTA EXITOSA';
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               PN_COD_RESULTADO   := 3;
               PV_MSG_RESULTADO   := 'NO SE ENCONTRARON RESULTADOS PARA EL LIDER DE TUMOR';
            WHEN TOO_MANY_ROWS THEN
               PN_COD_RESULTADO   := 4;
               PV_MSG_RESULTADO   := 'SE ENCONTRARON MAS DE UN REGISTRO PARA EL LIDER DE TUMOR';
            WHEN OTHERS THEN
               PN_COD_RESULTADO   := -2;
               PV_MSG_RESULTADO   := '[[SP_PFC_S_VALID_LIDER_TUMOR]] '
                                   || TO_CHAR(SQLCODE)
                                   || ' : '
                                   || SQLERRM;
         END;
      END IF;

   EXCEPTION
      WHEN V_EXC_RESULTADO THEN
         PN_COD_RESULTADO   := V_COD_RESULTADO;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_VALID_LIDER_TUMOR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_VALID_LIDER_TUMOR;


  /* AUTOR     : AFLORES*/
  /* CREADO    : 18/01/2019*/
  /* PROPOSITO : ACTUALIZAR SERVICIO ACTUALIZAR*/

   PROCEDURE SP_PFC_SU_SOLBEN_ACTUALIZAR (
      PV_COD_SCG_SOLBEN     IN OUT                VARCHAR2,
      PV_COD_AFI_PACIENTE   IN                    VARCHAR2,
      PV_COD_ESTADO_SCG     IN                    VARCHAR2,
      PV_FEC_ESTADO_SCG     IN                    VARCHAR2,
      PV_NRO_CG             IN                    VARCHAR2,
      PV_FEC_CG             IN                    VARCHAR2,
      PV_TX_DATO_ADIC1      IN OUT                VARCHAR2,
      PV_TX_DATO_ADIC2      IN OUT                VARCHAR2,
      PV_TX_DATO_ADIC3      IN OUT                VARCHAR2,
      PV_COD_SOL_EVAL       OUT                   VARCHAR2,
      PN_COD_RESULTADO      OUT                   NUMBER,
      PV_MSG_RESULTADO      OUT                   VARCHAR2
   ) AS

      V_COD_SCG          NUMBER;
      V_EXCEPTION EXCEPTION;
      V_EXC_COD_SOL_EVA EXCEPTION;
      V_EXC_FECHA EXCEPTION;
      V_COD_SOL_EVA      NUMBER;
      V_MSG              VARCHAR2(200);
      V_FEC_ESTADO_SCG   DATE;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      CASE
         WHEN NULLIF(
            PV_COD_SCG_SOLBEN,
            ''
         ) IS NULL THEN
            V_MSG := 'EL CODIGO SCG ES OBLIGATORIO';
            RAISE V_EXCEPTION;
         WHEN NULLIF(
            PV_COD_AFI_PACIENTE,
            ''
         ) IS NULL THEN
            V_MSG := 'EL CODIGO DE AFILIADO ES OBLIGATORIO';
            RAISE V_EXCEPTION;
         WHEN NULLIF(
            PV_COD_ESTADO_SCG,
            ''
         ) IS NULL THEN
            V_MSG := 'EL CODIGO DE ESTADO DE LA SCG ES OBLIGATORIO';
            RAISE V_EXCEPTION;
         WHEN NULLIF(
            PV_FEC_ESTADO_SCG,
            ''
         ) IS NULL THEN
            V_MSG := 'LA FECHA DEL ESTADO DE LA SCG ES OBLIGATORIO';
            RAISE V_EXCEPTION;
         ELSE
            V_MSG := 'NO HAY ERROR EN LA VALIDACION DE CAMPOS OBLIGATORIOS';
      END CASE;

      BEGIN
         SELECT
            S.COD_SCG,
            SE.COD_SOL_EVA
         INTO
            V_COD_SCG,
            V_COD_SOL_EVA
         FROM
            PFC_SOLBEN                 S
            INNER JOIN PFC_SOLICITUD_PRELIMINAR   SP ON S.COD_SCG = SP.COD_SCG
            INNER JOIN PFC_SOLICITUD_EVALUACION   SE ON SE.COD_SOL_PRE = SP.COD_SOL_PRE
         WHERE
            S.COD_SCG_SOLBEN = PV_COD_SCG_SOLBEN
            AND S.COD_AFI_PACIENTE = PV_COD_AFI_PACIENTE;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            V_COD_SCG       := NULL;
            V_COD_SOL_EVA   := NULL;
      END;

    /*LA VALIDACION DE FECHA SI ES IGUAL A 1 ES INCORRECTO*/

      CASE
         WHEN FN_PFC_S_VALIDACION_FECHA_HORA(PV_FEC_ESTADO_SCG) = 1 THEN
            V_MSG := 'FORMATO FECHA DE ESTADO SCG SOLBEN INCORRECTO.';
            RAISE V_EXC_FECHA;
         WHEN FN_PFC_S_VALIDACION_FECHA_HORA(PV_FEC_CG) = 1 THEN
            V_MSG := 'FORMATO FECHA DE CARTA DE GARANTIA.';
            RAISE V_EXC_FECHA;
         ELSE
            V_MSG := 'NO HAY ERROR EN LA VALIDACION DE FORMATO DE FECHA';
      END CASE;

      IF V_COD_SOL_EVA IS NULL THEN
         RAISE V_EXC_COD_SOL_EVA;
      END IF;
      V_FEC_ESTADO_SCG   := TO_DATE(
         PV_FEC_ESTADO_SCG,
         'DD-MM-YYYY HH24:MI:SS'
      );
      UPDATE PFC_SOLBEN S
      SET
         S.ESTADO_SCG = PV_COD_ESTADO_SCG,
         S.NRO_CG = PV_NRO_CG,
         S.FEC_CG = PV_FEC_CG,
         S.FEC_ESTADO_SCG = V_FEC_ESTADO_SCG,
         S.TX_DATO_ADIC1 = PV_TX_DATO_ADIC1,
         S.TX_DATO_ADIC2 = PV_TX_DATO_ADIC2,
         S.TX_DATO_ADIC3 = PV_TX_DATO_ADIC3
      WHERE
         S.COD_SCG = V_COD_SCG;

      PV_COD_SOL_EVAL    := TO_CHAR(V_COD_SOL_EVA);
      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := 'No hay Error';
      COMMIT;
   EXCEPTION
      WHEN V_EXC_FECHA THEN
         PN_COD_RESULTADO   := 3;
         PV_MSG_RESULTADO   := V_MSG;
      WHEN V_EXCEPTION THEN
         PN_COD_RESULTADO   := 2;
         PV_MSG_RESULTADO   := V_MSG;
      WHEN V_EXC_COD_SOL_EVA THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'NO EXISTE LA SOLICITUD DE EVALUACION PARA LA SCG SCG ENVIADA';
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_SU_SOLBEN_ACTUALIZAR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_SU_SOLBEN_ACTUALIZAR;

  /* AUTOR     : AFLORES*/
  /* CREADO    : 18/01/2019*/
  /* PROPOSITO : CODIGO FECHA*/

   PROCEDURE SP_PFC_COD_FECHA (
      PN_COD_FECHA       IN                 VARCHAR2,
      PN_COD             IN                 NUMBER,
      PN_LONGITUD        IN                 NUMBER,
      PV_COD_LARGO       OUT                VARCHAR2,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      V_COD_LONGITUD     VARCHAR2(50);
      V_CEROS_LONGITUD   NUMBER;
      I                  NUMBER := 1;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      V_CEROS_LONGITUD   := PN_LONGITUD - LENGTH(PN_COD_FECHA
                                               || '-'
                                               || PN_COD);
      DBMS_OUTPUT.PUT_LINE('LONGITUD DE CEROS :> ' || V_CEROS_LONGITUD);
      V_COD_LONGITUD     := '';
      FOR I IN 1..V_CEROS_LONGITUD LOOP
         V_COD_LONGITUD := V_COD_LONGITUD || '0';
         DBMS_OUTPUT.PUT_LINE(V_COD_LONGITUD);
      END LOOP;

      PV_COD_LARGO       := PN_COD_FECHA
                      || '-'
                      || V_COD_LONGITUD
                      || PN_COD;
      DBMS_OUTPUT.PUT_LINE('FIN :> ' || PV_COD_LARGO);
      IF V_CEROS_LONGITUD >= 0 THEN
         PN_COD_RESULTADO   := 0;
         PV_MSG_RESULTADO   := 'Consulta Exitosa';
      ELSE
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := 'EL CODIGO INGRESADO SUPERA LA LONGITUD MAXIMA';
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_CODIGO_LARGO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_COD_FECHA;

  /* AUTOR      : JAZABACHE*/
  /* CREADO     : 15/03/2019*/
  /* PROPOSITO  : LISTAR USUARIOS FARMACIA COMPLEJA SEGUN ROL ACTIVOS O INACTIVOS*/

   PROCEDURE SP_PFC_S_USUARIO_FARMACIA (
      PN_COD_ROL         IN                 NUMBER,
      PN_ESTADO_USR      IN                 NUMBER,
      TC_MIEMBRO_CMAC    OUT                PCK_PFC_CONSTANTE.TYPCUR,
      PN_COD_RESULTADO   OUT                NUMBER,
      PV_MSG_RESULTADO   OUT                VARCHAR2
   ) AS
      V_MENSAJE          VARCHAR2(200);
      V_EXC_OTHERS EXCEPTION;
      V_ESTADO_USUARIO   NUMBER;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      CASE
         WHEN PN_ESTADO_USR = PCK_PFC_CONSTANTE.PN_USUARIO_ACTIVO OR NULLIF(
            PN_ESTADO_USR,
            ''
         ) IS NULL THEN
            V_ESTADO_USUARIO := PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_ACTIVO;
         ELSE
            V_ESTADO_USUARIO := PCK_PFC_CONSTANTE.PN_ESTADO_CONFIG_MAC_INACTIVO;
      END CASE;

      BEGIN
         OPEN TC_MIEMBRO_CMAC FOR SELECT
                                     PA.COD_PARTICIPANTE,
                                     PA.NOMBRES
                                     || ' '
                                     || PA.APELLIDOS AS NOMBRE_APELLIDO,
                                     PA.CORREO_ELECTRONICO
                                  FROM
                                     PFC_PARTICIPANTE PA
                                  WHERE
                                     PA.COD_ROL = PN_COD_ROL
                                     AND PA.NOMBRES IS NOT NULL
                                     AND PA.APELLIDOS IS NOT NULL
                                     AND PA.P_ESTADO = V_ESTADO_USUARIO;

      EXCEPTION
         WHEN OTHERS THEN
            V_MENSAJE := '[[CURSOR LISTADO MIEMBROS CMAC]] '
                         || TO_CHAR(SQLCODE)
                         || ' : '
                         || SQLERRM;
            RAISE V_EXC_OTHERS;
      END;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
   EXCEPTION
      WHEN V_EXC_OTHERS THEN
         PN_COD_RESULTADO   := -2;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_VALID_LIDER_TUMOR]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_USUARIO_FARMACIA;
  
  /* AUTOR      : JAZABACHE*/
  /* CREADO     : 11/05/2019*/
  /* PROPOSITO  : OBTENER DATOS DE MEDICO TRATANTE SOLBEN*/

   PROCEDURE SP_PFC_S_CMP_MEDICO (
      PN_COD_SOL_EVA       IN                   NUMBER,
      PV_CMP_MEDICO        OUT                  VARCHAR2,
      PV_MEDICO_TRATANTE   OUT                  VARCHAR2,
      PN_COD_RESULTADO     OUT                  NUMBER,
      PV_MSG_RESULTADO     OUT                  VARCHAR2
   ) AS
      V_MENSAJE VARCHAR2(200);
      V_EXC_VARIABLE EXCEPTION;
   BEGIN
      PN_COD_RESULTADO   := -1;
      PV_MSG_RESULTADO   := 'valor inicial';
      IF NULLIF(
         PN_COD_SOL_EVA,
         ''
      ) IS NULL THEN
         V_MENSAJE := 'NO SE ENCUENTRA LA SOLICITUD DE EVALUACION';
         RAISE V_EXC_VARIABLE;
      END IF;

      SELECT
         SO.CMP_MEDICO,
         SO.MEDICO_TRATANTE_PRESCRIPTOR
      INTO
         PV_CMP_MEDICO,
         PV_MEDICO_TRATANTE
      FROM
         PFC_SOLICITUD_EVALUACION   SE
         INNER JOIN PFC_SOLICITUD_PRELIMINAR   PE ON SE.COD_SOL_PRE = PE.COD_SOL_PRE
         INNER JOIN PFC_SOLBEN                 SO ON PE.COD_SCG = SO.COD_SCG
      WHERE
         SE.COD_SOL_EVA = PN_COD_SOL_EVA;

      PN_COD_RESULTADO   := 0;
      PV_MSG_RESULTADO   := PCK_PFC_CONSTANTE.PV_MENSAJE_CONSULTAR_EXITO;
   EXCEPTION
      WHEN V_EXC_VARIABLE THEN
         PN_COD_RESULTADO   := 1;
         PV_MSG_RESULTADO   := V_MENSAJE;
      WHEN OTHERS THEN
         PN_COD_RESULTADO   := -1;
         PV_MSG_RESULTADO   := '[[SP_PFC_S_CMP_MEDICO]] '
                             || TO_CHAR(SQLCODE)
                             || ' : '
                             || SQLERRM;
   END SP_PFC_S_CMP_MEDICO;

END PCK_PFC_UTIL;

/


create or replace package body ONTPPA.PCK_PPA_CORREO AS

PROCEDURE SP_ONTPPA_S_OBTENER_PLANTILLA (
    PV_USRAPP IN VARCHAR2,
    PV_TIPO_ENVIO IN VARCHAR2,
    PV_FLAG_ADJUNTO IN VARCHAR2,
    PT_FECHA_PROGRAMADA IN TIMESTAMP,
    PN_COD_PLANTILLA IN NUMBER,
    K_CURSOR OUT SYS_REFCURSOR,
    PN_CODIGO OUT NUMBER,
    PV_MENSAJE OUT VARCHAR2,
    PN_CODIGO_ENVIO OUT NUMBER
)
    IS
        PV_CONDICIONES EXCEPTION;
        v_num_total_rows NUMBER;
        v_codigo_envio  NUMBER;
        V_CODIGO_CUENTA NUMBER;
        V_CODIGO_PLANTILLA NUMBER;
        V_CUERPO VARCHAR2(4000);
        V_COMPLETADO VARCHAR(10);
        V_COD_PLANILLA VARCHAR(1000);
    begin
        V_COD_PLANILLA:='';

        dbms_output.enable;
        DBMS_OUTPUT.PUT_LINE('Inicializando PCK');

        IF PV_USRAPP IS NULL THEN
             RAISE PV_CONDICIONES;
        END IF;

        IF LENGTH(PV_USRAPP) < 6 THEN
                 RAISE PV_CONDICIONES;
        END IF;

        V_COD_PLANILLA:='';
        select
            COD_PLANTILLA INTO V_COD_PLANILLA
        from
            ppa_plantilla
        WHERE
            usrapp = PV_USRAPP and
            cod_plantilla = pn_cod_plantilla;

        IF V_COD_PLANILLA IS NOT NULL THEN
            open K_CURSOR for
            select
                COD_PLANTILLA as codigoPlantilla,
                FLG_ADJUNTO as flagAdjunto,
                ASUNTO as asunto,
                CUERPO as cuerpo,
                FLG_HTML as flgHtml,
                USRAPP as usrapp
            from
                ppa_plantilla
            WHERE
                usrapp = PV_USRAPP and
                cod_plantilla = pn_cod_plantilla;

            DBMS_OUTPUT.PUT_LINE('Inicializando K_CURSOR');

            -- SELECT plantilla.cod_plantilla
            -- INTO V_CODIGO_PLANTILLA
            -- FROM PPA_PLANTILLA plantilla
            -- WHERE plantilla.usrapp = PV_USRAPP
            -- AND plantilla.cod_plantilla = ;

            V_CODIGO_PLANTILLA := pn_cod_plantilla;

            DBMS_OUTPUT.PUT_LINE('V_CODIGO_PLANTILLA: ' || V_CODIGO_PLANTILLA);

            SELECT cuenta.cod_cuenta INTO V_CODIGO_CUENTA
            FROM PPA_CUENTACORREO cuenta
            WHERE cuenta.usrapp = PV_USRAPP;

            DBMS_OUTPUT.PUT_LINE('V_CODIGO_CUENTA: ' || V_CODIGO_CUENTA);

            v_codigo_envio := envio_sequence.nextval + 1;

           DBMS_OUTPUT.PUT_LINE('v_codigo_envio: ' || v_codigo_envio);

            V_COMPLETADO  := '0'; -- 0: pendiente de envio , 1: enviado, 2: error al enviar

            insert into PPA_ENVIO_CORREO (cod_envio,
            estado_envio,fec_creacion,fec_entrada,fec_modificacion,fec_salida ,
            flg_adjunto, tipo_envio, usr_creacion, usr_modificacion,usrapp,
            cod_cuenta, cod_plantilla,fecha_programada, flg_completado) values ( v_codigo_envio  ,
            '0',SYSDATE,SYSDATE,SYSDATE,SYSDATE,
            PV_FLAG_ADJUNTO,PV_TIPO_ENVIO,'ADMIN','ADMIN',PV_USRAPP,
            V_CODIGO_CUENTA,v_codigo_plantilla,PT_FECHA_PROGRAMADA,V_COMPLETADO );

            PN_CODIGO_ENVIO := v_codigo_envio;

            PN_CODIGO := 1;
            PV_MENSAJE := 'Consulta Exitosa';

            COMMIT;
        ELSE
            PN_CODIGO := -2;
            PV_MENSAJE := 'CODIGO DE PLANILLA INVALIDO.';
        END IF;


     EXCEPTION
       WHEN PV_CONDICIONES THEN
        ROLLBACK;
        PN_CODIGO := -1;
        PV_MENSAJE := '[[SP_ONTPPA_S_OBTENER_PLANTILLA:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
        --DBMS_OUTPUT.PUT_LINE('PV_MENSAJE = ' || PV_MENSAJE);
       WHEN OTHERS THEN
        ROLLBACK;
        PN_CODIGO := -2;
        PV_MENSAJE := '[[SP_ONTPPA_S_OBTENER_PLANTILLA_2:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
       --DBMS_OUTPUT.PUT_LINE('PV_MENSAJE = ' || PV_MENSAJE);

    END;


PROCEDURE SP_PPA_I_INGRESAR_ENVIO (
PV_USRAPP IN VARCHAR2,
PV_FLAG_ADJUNTO IN VARCHAR2,
PV_TIPO_ENVIO IN VARCHAR2,
PV_ASUNTO IN VARCHAR2,
PV_CUERPO IN VARCHAR2,
PV_RUTA IN VARCHAR2,
PV_ID_PLANTILLA IN VARCHAR2,
PV_ID_ENVIO IN VARCHAR2,
PV_DESTINATARIO IN VARCHAR2,
PV_MENSAJE OUT VARCHAR2,
PN_CODIGO OUT NUMBER
)
    AS
    V_CODIGO_CUENTA number;
    codigo_envio  number;
    codigo_registro  number;
    codigo_envio_detalle  number;
    PV_CONDICIONES EXCEPTION;
    SIN_ADJUNTO EXCEPTION;
    V_TIENE_ADJUNTO number;
    V_ARCHIVO_PESO VARCHAR2(20);
    V_ARCHIVO_RUTA VARCHAR2(500);
    V_ARCHIVO_EXTENSION VARCHAR2(10);
    V_ARCHIVO_COMPLETO VARCHAR2(500);
    begin

        IF PV_USRAPP IS NULL THEN
         RAISE PV_CONDICIONES;
        END IF;

        IF PV_TIPO_ENVIO IS NULL THEN
         RAISE PV_CONDICIONES;
        END IF;

        IF PV_DESTINATARIO IS NULL THEN
         RAISE PV_CONDICIONES;
        END IF;


        IF PV_ASUNTO IS NULL THEN
         RAISE PV_CONDICIONES;
        END IF;

        IF PV_CUERPO IS NULL THEN
         RAISE PV_CONDICIONES;
        END IF;

         IF PV_ID_PLANTILLA IS NULL THEN
         RAISE PV_CONDICIONES;
        END IF;

         IF PV_FLAG_ADJUNTO IS NULL THEN
         RAISE PV_CONDICIONES;
        END IF;
        IF PV_FLAG_ADJUNTO = '1' THEN
            V_TIENE_ADJUNTO := 1;
            IF PV_RUTA IS NULL THEN
            RAISE SIN_ADJUNTO;
            END IF;
        ELSE
            V_TIENE_ADJUNTO := 0;
        END IF;


        SELECT cuenta.cod_cuenta
        INTO V_CODIGO_CUENTA
        FROM PPA_CUENTACORREO cuenta
        WHERE cuenta.usrapp = PV_USRAPP;

        IF V_CODIGO_CUENTA IS NULL THEN
            RAISE PV_CONDICIONES;
        END IF;

        codigo_envio_detalle := detalle_envio_sequence.nextval + 1;

        insert into PPA_detalle_envio (
        cod_detalleenvio,asunto,estado,
        fec_creacion,fec_modificacion,
        cuerpo,  usr_creacion, usr_modificacion,usrapp,cod_envio, destinatario)
        values (
        codigo_envio_detalle, PV_ASUNTO,'0',
        SYSDATE,SYSDATE,
        PV_CUERPO,'admin','admin1',PV_USRAPP,TO_NUMBER(PV_ID_ENVIO),PV_DESTINATARIO);

        update ppa_envio_correo
        set estado_envio = '1'
        where cod_envio= TO_NUMBER(PV_ID_ENVIO);

        --dbms_output.put_line('CASI EN EL LOOP');
        IF V_TIENE_ADJUNTO = 1 THEN
        -- dbms_output.put_line('CASI EN EL LOOP2');

            FOR i IN
              (SELECT level,
                trim(regexp_substr(PV_RUTA, '[^,]+', 1, LEVEL)) str
             FROM dual
               CONNECT BY regexp_substr(PV_RUTA , '[^,]+', 1, LEVEL) IS NOT NULL
             )
             LOOP
               -- do something
               -- dbms_output.put_line('Company code no.'||i.level||' = '||i.str);

               SELECT archivo.extension,
                      archivo.TAMANO,
                      archivo.ruta,
                      archivo.nom_archivo
                      INTO v_archivo_extension,
                           v_archivo_peso,
                           v_archivo_ruta,
                           v_archivo_completo
                        FROM PPA_archftp archivo
                        WHERE archivo.cod_archftp = TO_NUMBER(i.str);

                codigo_registro := "SQ_PPA_ADJUNTO_SEQUENCE".nextval;
                --dbms_output.put_line('CASI EN EL LOOP3');

                insert into PPA_adjunto (
                    COD_REGISTRO, COD_ADJUNTO, estado, extension, fec_creacion,
                    fec_modificacion, nombre_archivo, nombre_completo, peso, ruta,
                    usr_creacion, usr_modificacion, usrapp
                ) values (
                    codigo_registro, TO_NUMBER(i.str),'1', v_archivo_extension, sysdate,
                    sysdate, v_archivo_completo, v_archivo_ruta || v_archivo_completo, v_archivo_peso, v_archivo_ruta,
                    'admin', 'admin', PV_USRAPP
                );
                --dbms_output.put_line('CASI EN EL LOOP4');

                 insert into PPA_envio_adjunto ( COD_REGISTRO, COD_ENVIO)
                 VALUES (codigo_registro,TO_NUMBER(PV_ID_ENVIO));

             END LOOP;

        END IF;

        PV_MENSAJE := 'Proceso Correcto';
        PN_CODIGO := 1;

        COMMIT;

        EXCEPTION
        WHEN SIN_ADJUNTO THEN
        ROLLBACK;
        PN_CODIGO := -1;
        PV_MENSAJE := '[[SP_PPA_I_INGRESAR_ENVIO_:EX]] ' || 'SIN ADJUNTOS';
        WHEN PV_CONDICIONES THEN
        ROLLBACK;
        PN_CODIGO := -1;
        PV_MENSAJE := '[[SP_PPA_I_INGRESAR_ENVIO_1:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
        --DBMS_OUTPUT.PUT_LINE('PV_MENSAJE = ' || PV_MENSAJE);
        WHEN OTHERS THEN
        ROLLBACK;
        PN_CODIGO := -1;
        PV_MENSAJE := '[[SP_PPA_I_INGRESAR_ENVIO_2:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
       --DBMS_OUTPUT.PUT_LINE('PV_MENSAJE = ' || PV_MENSAJE);
    END;


 /*   PROCEDURE SP_ONTPPA_S_OBTENER_CORREOS (
    USRAPP IN VARCHAR2
    )
    as
    PV_CONDICIONES EXCEPTION;
    cenvios SYS_REFCURSOR;
    V_COD_ENVIO NUMBER;
    begin
      OPEN cenvios for SELECT envio.cod_envio FROM PPA_ENVIO_CORREO envio WHERE envio.estado_envio = '1' and envio.flg_adjunto = '1' and envio.flg_completado = 'NO' ORDER BY envio.fec_creacion desc;

      LOOP
      FETCH cenvios INTO V_COD_ENVIO;
      DBMS_OUTPUT.PUT_LINE(V_COD_ENVIO);
    EXIT WHEN cenvios%NOTFOUND;

     -- K_CURSOR :      CLOSE cenvios;
      END LOOP;
    END;*/


PROCEDURE SP_ONTPPA_S_OBTENER_ENVIOS (
    K_CURSOR OUT SYS_REFCURSOR
)
    IS
        LISTA_CORREO LIST_CAB_CORREO;
    BEGIN
    
        LISTA_CORREO:=LIST_CAB_CORREO();
        
        SELECT 
            codigoEnvio BULK COLLECT INTO LISTA_CORREO 
        FROM (
           SELECT 
                ENVIO.COD_ENVIO as codigoEnvio
           FROM 
                PPA_ENVIO_CORREO envio
           WHERE 
                ENVIO.estado_envio = '1'
                AND ENVIO.flg_completado = '0'
                AND ENVIO.fecha_programada <= SYSDATE
           ORDER BY ENVIO.COD_ENVIO ASC
        ) WHERE ROWNUM <=20;
        
        OPEN K_CURSOR FOR
           SELECT 
                envio.cod_envio as codigoEnvio,
                envio.flg_adjunto as flagAdjunto,
                envio.usrapp as usrApp
           FROM 
                PPA_ENVIO_CORREO envio
           WHERE 
                envio.cod_envio IN (SELECT * FROM TABLE(LISTA_CORREO));
        
        UPDATE PPA_ENVIO_CORREO
            SET ESTADO_ENVIO=3
        WHERE 
            COD_ENVIO IN(SELECT * FROM TABLE(LISTA_CORREO));
                
        COMMIT;
        
        EXCEPTION
            WHEN OTHERS THEN
            ROLLBACK;
            
END SP_ONTPPA_S_OBTENER_ENVIOS;


PROCEDURE SP_ONTPPA_S_OBTENER_ADJUNTOS (
    CODIGO_ENVIO IN NUMBER,
    PV_ADJUNTO OUT VARCHAR2
)
    IS
    V_ADJUNTOS varchar2(100);

    BEGIN

        SELECT SUBSTR (SYS_CONNECT_BY_PATH (COD_ADJUNTO , ','), 2) into V_ADJUNTOS
        FROM (
            SELECT
                ADJ.COD_ADJUNTO,
                ROW_NUMBER () OVER (ORDER BY ADJ.COD_ADJUNTO ) rn,
                COUNT (*) OVER () cnt
            FROM
                PPA_ENVIO_ADJUNTO  ENV
                INNER JOIN PPA_ADJUNTO ADJ  ON ENV.COD_REGISTRO=ADJ.COD_REGISTRO
            WHERE COD_ENVIO = CODIGO_ENVIO
            GROUP BY ADJ.COD_ADJUNTO
        )
        WHERE rn = cnt
        START WITH rn = 1
        CONNECT BY rn = PRIOR rn + 1;

        PV_ADJUNTO := V_ADJUNTOS;

    END;



PROCEDURE SP_ONTPPA_S_OBTENER_DETALLE (
COD_ENVIO IN NUMBER,
K_CURSOR OUT SYS_REFCURSOR
)
    IS
    BEGIN
       OPEN K_CURSOR FOR
       SELECT cod_detalleenvio as idDetalleEnvio,
       asunto as asunto,
       cuerpo as cuerpo,
       destinatario as destinatario,
       estado as estado
       FROM PPA_DETALLE_ENVIO
       WHERE COD_ENVIO = COD_ENVIO and destinatario IS NOT NULL AND ESTADO = '0';
    END;

PROCEDURE SP_ONTPPA_U_ACTUALIZAR_ENVIO (
    COD_DETALLE_ENVIO IN NUMBER,
    PV_ESTADO IN VARCHAR,
    PV_MENSAJE IN VARCHAR
) IS
 BEGIN


    UPDATE PPA_DETALLE_ENVIO
    SET ESTADO = PV_ESTADO, RESPUESTA=PV_MENSAJE
    WHERE COD_DETALLEENVIO = COD_DETALLE_ENVIO;

END;

PROCEDURE SP_ONTPPA_U_COMPLETAR_ENVIO (
PV_COD_ENVIO IN NUMBER,
PV_ESTADO IN VARCHAR,
PV_MENSAJE IN VARCHAR
) IS
 BEGIN

    UPDATE PPA_ENVIO_CORREO
    SET FLG_COMPLETADO = PV_ESTADO, RESPUESTA = PV_MENSAJE
    WHERE COD_ENVIO = PV_COD_ENVIO;

    IF PV_ESTADO='2' THEN
        UPDATE PPA_DETALLE_ENVIO
        SET ESTADO = PV_ESTADO, RESPUESTA=PV_MENSAJE
        WHERE COD_ENVIO = PV_COD_ENVIO;
    END IF;

    COMMIT;

    EXCEPTION
        WHEN OTHERS THEN
        ROLLBACK;

END;

 PROCEDURE SP_ONTPPA_S_OBTENER_CUENTA_COR(
    PV_USRAPP IN VARCHAR2,
    PV_MENSAJE OUT VARCHAR2,
    PN_CODIGO OUT NUMBER,
    K_CURSOR OUT SYS_REFCURSOR
 )AS
    PV_CONDICIONES EXCEPTION;

 BEGIN
    IF PV_USRAPP IS NULL or PV_USRAPP = '' THEN

        RAISE PV_CONDICIONES;
    END IF;

    OPEN K_CURSOR FOR
        SELECT
            cc.correo,
            cc.clave,
            cc.smtp,
            cc.usrapp,
            cc.usuario,
            sc.ip_servidor,
            sc.nombre_servidor,
            sc.mail_transport_protocol,
            sc.mail_smtp_starttls_enable,
            sc.mail_smtp_host,
            sc.mail_smtp_auth,
            sc.mail_debug,
            sc.mail_smtp_port
        FROM PPA_CUENTACORREO CC
        INNER JOIN PPA_SERVIDOR_CORREO SC ON cc.cod_servidor_correo=sc.cod_servidor_correo
        AND UPPER(cc.usrapp) = UPPER(PV_USRAPP);

        PV_MENSAJE := 'consulta exitosa';
        PN_CODIGO := 0;
        EXCEPTION
          WHEN PV_CONDICIONES THEN
            PN_CODIGO := -1;
            PV_MENSAJE := '[[SP_ONTPPA_S_OBTENER_CUENTA_COR:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
          WHEN OTHERS THEN
            PN_CODIGO := -1;
            PV_MENSAJE := '[[SP_ONTPPA_S_OBTENER_CUENTA_COR:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;


 END SP_ONTPPA_S_OBTENER_CUENTA_COR;

 PROCEDURE SP_ONTPPA_S_OBTENER_C_CORREOS(
    PV_MENSAJE OUT VARCHAR2,
    PN_CODIGO OUT NUMBER,
    K_CURSOR OUT SYS_REFCURSOR
 )AS
    PV_CONDICIONES EXCEPTION;

 BEGIN

    OPEN K_CURSOR FOR
        SELECT
            cc.correo,
            cc.clave,
            cc.smtp,
            cc.usrapp,
            cc.usuario,
            sc.ip_servidor,
            sc.nombre_servidor,
            sc.mail_transport_protocol,
            sc.mail_smtp_starttls_enable,
            sc.mail_smtp_host,
            sc.mail_smtp_auth,
            sc.mail_debug,
            sc.mail_smtp_port
        FROM PPA_CUENTACORREO CC
        INNER JOIN PPA_SERVIDOR_CORREO SC ON cc.cod_servidor_correo=sc.cod_servidor_correo
        WHERE USRAPP IS NOT NULL;

        PV_MENSAJE := 'consulta exitosa';
        PN_CODIGO := 1;
        
        EXCEPTION
        
          WHEN PV_CONDICIONES THEN
            PN_CODIGO := -1;
            PV_MENSAJE := '[[SP_ONTPPA_S_OBTENER_CUENTA_COR:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
        
          WHEN OTHERS THEN
            PN_CODIGO := -1;
            PV_MENSAJE := '[[SP_ONTPPA_S_OBTENER_CUENTA_COR:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;


 END SP_ONTPPA_S_OBTENER_C_CORREOS;

-- AUTOR     : RROCA
-- CREADO    : 20/05/2019
-- PROPOSITO : Obtiene el estado del envió del correo según la respuesta del proceso batch   -> 0 NO ENVIADO, 1 ENVIADO
 PROCEDURE SP_ONTPPA_S_OBTENER_ESTADO_ENV(
    PV_USRAPP               IN VARCHAR2, -- USUARIO DE APLICACION
    PN_COD_ENVIO            IN VARCHAR2,   -- CODIGO DE ENVIO
    OP_OBJCURSOR       OUT SYS_REFCURSOR, --LISTADO DE RESPUESTA
    PN_CODIGO               OUT NUMBER,
    PV_MENSAJE              OUT VARCHAR2)
    IS

  BEGIN

    OPEN OP_OBJCURSOR FOR
    SELECT
        COD_ENVIO,
        COD_DETALLEENVIO,
        DESTINATARIO,
        ESTADO,
        RESPUESTA
    FROM
        PPA_DETALLE_ENVIO
    WHERE
        COD_ENVIO=PN_COD_ENVIO
        AND USRAPP=PV_USRAPP;

    PN_CODIGO := 1;
    PV_MENSAJE := 'Consulta Exitosa';

    RETURN;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PN_CODIGO := -1;
      PV_MENSAJE := '[[SP_ONTPPA_S_OBTENER_ESTADO_ENV:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM || ' para el código de envio :' || PN_COD_ENVIO;

  END SP_ONTPPA_S_OBTENER_ESTADO_ENV;
  
      PROCEDURE SP_PPA_I_INGRESAR_REENVIO (
        PV_USRAPP IN VARCHAR2,
        PV_FLAG_ADJUNTO IN VARCHAR2,
        PV_TIPO_ENVIO IN VARCHAR2,
        PV_ASUNTO IN VARCHAR2,
        PV_CUERPO IN VARCHAR2,
        PV_RUTA IN VARCHAR2,
        PV_ID_PLANTILLA IN VARCHAR2,
        PV_ID_ENVIO IN VARCHAR2,
        PV_DESTINATARIO IN VARCHAR2,
        PV_MENSAJE OUT VARCHAR2,
        PN_CODIGO OUT NUMBER
    ) AS
        V_CODIGO_CUENTA number;
        codigo_envio  number;
        codigo_registro  number;
        codigo_envio_detalle  number;
        PV_CONDICIONES EXCEPTION;
        SIN_ADJUNTO EXCEPTION;
        V_TIENE_ADJUNTO number;
        V_ARCHIVO_PESO VARCHAR2(20);
        V_ARCHIVO_RUTA VARCHAR2(500);
        V_ARCHIVO_EXTENSION VARCHAR2(10);
        V_ARCHIVO_COMPLETO VARCHAR2(500);
        begin
    
            IF PV_USRAPP IS NULL THEN
             RAISE PV_CONDICIONES;
            END IF;
    
            IF PV_TIPO_ENVIO IS NULL THEN
             RAISE PV_CONDICIONES;
            END IF;
    
            IF PV_DESTINATARIO IS NULL THEN
             RAISE PV_CONDICIONES;
            END IF;
    
    
            IF PV_ASUNTO IS NULL THEN
             RAISE PV_CONDICIONES;
            END IF;
    
            IF PV_CUERPO IS NULL THEN
             RAISE PV_CONDICIONES;
            END IF;
    
             IF PV_ID_PLANTILLA IS NULL THEN
             RAISE PV_CONDICIONES;
            END IF;
    
             IF PV_FLAG_ADJUNTO IS NULL THEN
             RAISE PV_CONDICIONES;
            END IF;
            IF PV_FLAG_ADJUNTO = '1' THEN
                V_TIENE_ADJUNTO := 1;
                IF PV_RUTA IS NULL THEN
                RAISE SIN_ADJUNTO;
                END IF;
            ELSE
                V_TIENE_ADJUNTO := 0;
            END IF;
    
    
            SELECT cuenta.cod_cuenta
            INTO V_CODIGO_CUENTA
            FROM PPA_CUENTACORREO cuenta
            WHERE cuenta.usrapp = PV_USRAPP;
    
            IF V_CODIGO_CUENTA IS NULL THEN
                RAISE PV_CONDICIONES;
            END IF;
    
            codigo_envio_detalle := detalle_envio_sequence.nextval + 1;
    
            insert into PPA_detalle_envio (
            cod_detalleenvio,asunto,estado,
            fec_creacion,fec_modificacion,
            cuerpo,  usr_creacion, usr_modificacion,usrapp,cod_envio, destinatario)
            values (
            codigo_envio_detalle, PV_ASUNTO,'0',
            SYSDATE,SYSDATE,
            PV_CUERPO,'admin','admin1',PV_USRAPP,TO_NUMBER(PV_ID_ENVIO),PV_DESTINATARIO);
    
            update ppa_envio_correo
            set estado_envio = '1', FLG_COMPLETADO='0'
            where cod_envio= TO_NUMBER(PV_ID_ENVIO);
    
            --dbms_output.put_line('CASI EN EL LOOP');
            IF V_TIENE_ADJUNTO = 1 THEN
            -- dbms_output.put_line('CASI EN EL LOOP2');
    
                FOR i IN
                  (SELECT level,
                    trim(regexp_substr(PV_RUTA, '[^,]+', 1, LEVEL)) str
                 FROM dual
                   CONNECT BY regexp_substr(PV_RUTA , '[^,]+', 1, LEVEL) IS NOT NULL
                 )
                 LOOP
                   -- do something
                   -- dbms_output.put_line('Company code no.'||i.level||' = '||i.str);
    
                   SELECT archivo.extension,
                          archivo.TAMANO,
                          archivo.ruta,
                          archivo.nom_archivo
                          INTO v_archivo_extension,
                               v_archivo_peso,
                               v_archivo_ruta,
                               v_archivo_completo
                            FROM PPA_archftp archivo
                            WHERE archivo.cod_archftp = TO_NUMBER(i.str);
    
                    codigo_registro := "SQ_PPA_ADJUNTO_SEQUENCE".nextval;
                    --dbms_output.put_line('CASI EN EL LOOP3');
    
                    insert into PPA_adjunto (
                        COD_REGISTRO, COD_ADJUNTO, estado, extension, fec_creacion,
                        fec_modificacion, nombre_archivo, nombre_completo, peso, ruta,
                        usr_creacion, usr_modificacion, usrapp
                    ) values (
                        codigo_registro, TO_NUMBER(i.str),'1', v_archivo_extension, sysdate,
                        sysdate, v_archivo_completo, v_archivo_ruta || v_archivo_completo, v_archivo_peso, v_archivo_ruta,
                        'admin', 'admin', PV_USRAPP
                    );
                    --dbms_output.put_line('CASI EN EL LOOP4');
    
                     insert into PPA_envio_adjunto ( COD_REGISTRO, COD_ENVIO)
                     VALUES (codigo_registro,TO_NUMBER(PV_ID_ENVIO));
    
                 END LOOP;
    
            END IF;
    
            PV_MENSAJE := 'Proceso Correcto';
            PN_CODIGO := 1;
    
            COMMIT;
    
            EXCEPTION
            WHEN SIN_ADJUNTO THEN
            ROLLBACK;
            PN_CODIGO := -1;
            PV_MENSAJE := '[[SP_PPA_I_INGRESAR_REENVIO:EX]] ' || 'SIN ADJUNTOS';
            WHEN PV_CONDICIONES THEN
            ROLLBACK;
            PN_CODIGO := -1;
            PV_MENSAJE := '[[SP_PPA_I_INGRESAR_REENVIO:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
            --DBMS_OUTPUT.PUT_LINE('PV_MENSAJE = ' || PV_MENSAJE);
            WHEN OTHERS THEN
            ROLLBACK;
            PN_CODIGO := -1;
            PV_MENSAJE := '[[SP_PPA_I_INGRESAR_REENVIO:EX]] ' || TO_CHAR(SQLCODE) || ' : ' || SQLERRM;
           --DBMS_OUTPUT.PUT_LINE('PV_MENSAJE = ' || PV_MENSAJE);
        END;
  -------------------------------------------------------------
  /*
  Author: Chrisian Altamirano
  */      
  PROCEDURE SP_ONTPPA_S_PLANTILLA_X_CODIGO ( PN_COD_PLANTILLA IN NUMBER,
                                          OP_OBJCURSOR OUT SYS_REFCURSOR,
                                          PN_CODIGO OUT NUMBER,
                                          PV_MENSAJE OUT VARCHAR2 ) IS 
              
   BEGIN
      
     
      OPEN OP_OBJCURSOR FOR
            SELECT COD_PLANTILLA, 
                   FLG_ADJUNTO,   
                   ASUNTO,        
                   CUERPO
              FROM PPA_PLANTILLA
             WHERE COD_PLANTILLA = PN_COD_PLANTILLA;
       
      PN_CODIGO:=0;
      PV_MENSAJE:='Consulta exitosa.';
   END;                                        
end ONTPPA.PCK_PPA_CORREO;

/


CREATE OR REPLACE PACKAGE BODY ONTPPA.PCK_PPA_SEGURIDAD IS

  PROCEDURE SP_PPA_S_PERSONA_ROL(
    PV_COD_USUARIO        IN NUMBER,
    TC_LIST               OUT PCK_PPA_CONSTANTE.TYPCUR,
    PN_COD_RESULTADO      OUT NUMBER,
    PV_MSG_RESULTADO      OUT VARCHAR2
  )
  IS
  PN_COD_ROL  NUMBER;

  BEGIN

    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';

    SELECT MAX(R.COD_ROL) INTO PN_COD_ROL FROM ONTPPA.PPA_USER_ROL_APP UR
    INNER JOIN ONTPPA.PPA_ROL R ON R.COD_ROL = UR.COD_ROL
    INNER JOIN ONTPPA.PPA_APLICACION APP ON APP.COD_APLICACION=UR.COD_APLICACION
    WHERE UR.COD_USUARIO = PV_COD_USUARIO;

    OPEN TC_LIST FOR
      SELECT U.NOMBRES, U.APE_PAT, U.APE_MAT, R.COD_ROL, R.DESCRIPCION
      FROM ONTPPA.PPA_USUARIO U
      INNER JOIN ONTPPA.PPA_USER_ROL_APP UR ON U.COD_USUARIO = UR.COD_USUARIO
      INNER JOIN ONTPPA.PPA_ROL R ON R.COD_ROL = UR.COD_ROL
      WHERE U.COD_USUARIO = PV_COD_USUARIO
      AND U.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USR_ROL_ACTIVE
      AND UR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USR_ROL_ACTIVE
      AND R.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ROL_ACTIVE
      AND R.COD_ROL = PN_COD_ROL;

      PN_COD_RESULTADO := 0;
    PV_MSG_RESULTADO := 'Consulta Exitosa';

  EXCEPTION
    WHEN OTHERS
      THEN
        PN_COD_RESULTADO := -1;
        PV_MSG_RESULTADO := '[[SP_PPA_S_PERSONA_ROL]] '|| TO_CHAR(SQLCODE) ||' : '||SQLERRM;
  END SP_PPA_S_PERSONA_ROL;

------------------------------------------------------------
  PROCEDURE SP_PPA_S_USUARIO_OPCION(
    PV_COD_USUARIO        IN NUMBER,
    TC_LIST               OUT PCK_PPA_CONSTANTE.TYPCUR,
    PN_COD_RESULTADO      OUT NUMBER,
    PV_MSG_RESULTADO      OUT VARCHAR2
  )
  IS
  BEGIN

    OPEN TC_LIST FOR
      SELECT RECURSO.COD_RECURSO,
             RECURSO.TIPO_RECURSO,
             RECURSO.NOM_CORTO,
             '1' AS FLAG_ASIGNACION
      FROM ONTPPA.PPA_RECURSO RECURSO
      INNER JOIN ONTPPA.PPA_ASIGNACION_RECURSO AR
      ON AR.COD_RECURSO = RECURSO.COD_RECURSO
      INNER JOIN ONTPPA.PPA_USER_ROL_APP UR
      ON AR.COD_ROL = UR.COD_ROL
      INNER JOIN ONTPPA.PPA_USUARIO US
      ON UR.COD_USUARIO = US.COD_USUARIO
      INNER JOIN ONTPPA.PPA_APLICACION APP
      ON APP.COD_APLICACION = UR.COD_APLICACION
      INNER JOIN ONTPPA.PPA_ROL RO
      ON UR.COD_USUARIO = UR.COD_ROL
      WHERE AR.COD_ROL = PV_COD_USUARIO
      AND AR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ASIG_ROL_ACTIVE
      AND UR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ROL_ACTIVE
      AND US.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USUARIO_ACTIVE
      AND RO.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ROL_ACTIVE
      GROUP BY RECURSO.COD_RECURSO, RECURSO.TIPO_RECURSO, RECURSO.NOM_CORTO, AR.ESTADO
      ORDER BY RECURSO.COD_RECURSO;

    PN_COD_RESULTADO := 0;
    PV_MSG_RESULTADO := 'Consulta Exitosa';

  EXCEPTION
    WHEN OTHERS
      THEN
        PN_COD_RESULTADO := -1;
        PV_MSG_RESULTADO := '[[SP_PPA_S_USUARIO_ASIGNACION]] '|| TO_CHAR(SQLCODE) ||' : '||SQLERRM;
  END SP_PPA_S_USUARIO_OPCION;

--------------------------------------------------------------------

 PROCEDURE SP_PPA_S_MENU_OPCION(PV_COD_USUARIO         IN NUMBER,
                                PN_COD_APLICACION      IN NUMBER,
                                 TC_MENU               OUT PCK_PPA_CONSTANTE.TYPCUR,
                                 TC_OPCION             OUT PCK_PPA_CONSTANTE.TYPCUR,
                                 PN_COD_RESULTADO      OUT NUMBER,
                                 PV_MSG_RESULTADO      OUT VARCHAR2)
  IS
  BEGIN

    OPEN TC_MENU FOR
      SELECT DISTINCT RECURSO.COD_RECURSO AS COD_MENU,
                      RECURSO.NOM_CORTO,
                      RECURSO.NOM_LARGO,
                      RECURSO.URL,
                      RECURSO.RUTA_IMAGEN AS RUTA_IMG,
                      RECURSO.COD_PADRE AS MENU_PADRE
      FROM ONTPPA.PPA_RECURSO RECURSO
        INNER JOIN ONTPPA.PPA_ASIGNACION_RECURSO AR
        ON RECURSO.COD_RECURSO = AR.COD_RECURSO
        INNER JOIN ONTPPA.PPA_USER_ROL_APP UR
        ON AR.COD_ROL = UR.COD_ROL
        INNER JOIN ONTPPA.PPA_USUARIO US
        ON UR.COD_USUARIO = US.COD_USUARIO
        INNER JOIN ONTPPA.PPA_ROL RO
        ON RO.COD_ROL = UR.COD_ROL
        INNER JOIN ONTPPA.PPA_APLICACION AP
        ON AR.COD_APLICACION = AP.COD_APLICACION
      WHERE UR.COD_USUARIO = PV_COD_USUARIO
        AND AP.COD_APLICACION=PN_COD_APLICACION
        AND AR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ASIG_ROL_ACTIVE
        AND RECURSO.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_MENU_ACTIVE
        AND UR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USR_ROL_ACTIVE
        AND AP.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_APLICACION_ACTIVE
        AND US.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USUARIO_ACTIVE
        AND RO.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ROL_ACTIVE
        AND RECURSO.TIPO_RECURSO=PCK_PPA_SEGURIDAD.PN_RECURSO_MENU --Menu

      UNION

       SELECT DISTINCT RECURSO.COD_RECURSO AS COD_MENU,
                      RECURSO.NOM_CORTO,
                      RECURSO.NOM_LARGO,
                      RECURSO.URL,
                      RECURSO.RUTA_IMAGEN AS RUTA_IMG,
                      RECURSO.COD_PADRE AS MENU_PADRE
      FROM ONTPPA.PPA_RECURSO RECURSO
        INNER JOIN ONTPPA.PPA_ASIGNACION_RECURSO AR
        ON RECURSO.COD_RECURSO = AR.COD_RECURSO
        INNER JOIN ONTPPA.PPA_USER_ROL_APP UR
        ON AR.COD_ROL = UR.COD_ROL
        INNER JOIN ONTPPA.PPA_USUARIO US
        ON UR.COD_USUARIO = US.COD_USUARIO
        INNER JOIN ONTPPA.PPA_ROL RO
        ON RO.COD_ROL = UR.COD_ROL
        INNER JOIN ONTPPA.PPA_APLICACION AP
        ON AR.COD_APLICACION = AP.COD_APLICACION
      WHERE UR.COD_USUARIO = PV_COD_USUARIO
        AND AP.COD_APLICACION=PN_COD_APLICACION
        AND AR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ASIG_ROL_ACTIVE
        AND RECURSO.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_MENU_ACTIVE
        AND UR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USR_ROL_ACTIVE
        AND AP.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_APLICACION_ACTIVE
        AND US.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USUARIO_ACTIVE
        AND RO.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ROL_ACTIVE
        AND RECURSO.TIPO_RECURSO=PCK_PPA_SEGURIDAD.PN_RECURSO_SUB_MENU;  --SUBMENU




    OPEN TC_OPCION FOR
       SELECT DISTINCT RECURSO.COD_RECURSO AS COD_OPCION,
                      RECURSO.COD_PADRE  AS COD_MENU,
                      RECURSO.TIPO_RECURSO AS TIPO_OPCION,
                      RECURSO.NOM_CORTO,
                      '1' AS FLAG_ASIGNACION
        FROM ONTPPA.PPA_RECURSO RECURSO
        INNER JOIN ONTPPA.PPA_ASIGNACION_RECURSO AR
        ON RECURSO.COD_RECURSO = AR.COD_RECURSO
        INNER JOIN ONTPPA.PPA_USER_ROL_APP UR
        ON AR.COD_ROL = UR.COD_ROL
        INNER JOIN ONTPPA.PPA_USUARIO US
        ON UR.COD_USUARIO = US.COD_USUARIO
        INNER JOIN ONTPPA.PPA_ROL RO
        ON RO.COD_ROL = UR.COD_ROL
        INNER JOIN ONTPPA.PPA_APLICACION AP
        ON AR.COD_APLICACION = AP.COD_APLICACION
        WHERE UR.COD_USUARIO = PV_COD_USUARIO
        AND AR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ASIG_ROL_ACTIVE
        AND RECURSO.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_MENU_ACTIVE
        AND UR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USR_ROL_ACTIVE
        AND AP.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_APLICACION_ACTIVE
        AND US.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USUARIO_ACTIVE
        AND RO.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ROL_ACTIVE
        AND RECURSO.TIPO_RECURSO NOT IN (PCK_PPA_SEGURIDAD.PN_RECURSO_SUB_MENU,
                                         PCK_PPA_SEGURIDAD.PN_RECURSO_MENU)
        ORDER BY RECURSO.COD_RECURSO;


    PN_COD_RESULTADO := 0;
    PV_MSG_RESULTADO := 'Consulta Exitosa';

  EXCEPTION
    WHEN OTHERS
      THEN
        PN_COD_RESULTADO := -1;
        PV_MSG_RESULTADO := '[[SP_PPA_S_MENU_OPCION]] '|| TO_CHAR(SQLCODE) ||' : '||SQLERRM;

  END SP_PPA_S_MENU_OPCION;
-----------------------------------------------------------------------------

/*
 PROCEDURE SP_PPA_S_USUARIO_APLICACION(
    PV_COD_USUARIO        IN NUMBER,
    TC_LIST               OUT PCK_PPA_CONSTANTE.TYPCUR,
    PN_COD_RESULTADO      OUT NUMBER,
    PV_MSG_RESULTADO      OUT VARCHAR2
  )
  IS
  BEGIN

    OPEN TC_LIST FOR
      SELECT DISTINCT AP.COD_APLICACION,
                      AP.NOM_CORTO,
                      AP.NOM_LARGO
      FROM ONTPPA.PPA_OPCION OP
      INNER JOIN ONTPPA.PPA_ASIGNACION_ROL AR
      ON OP.COD_OPCION = AR.COD_OPCION
      INNER JOIN ONTPPA.PPA_USR_ROL UR
      ON AR.COD_ROL = UR.COD_ROL
      INNER JOIN ONTPPA.PPA_USUARIO US
      ON UR.COD_USUARIO = US.COD_USUARIO
      INNER JOIN ONTPPA.PPA_ROL RO
      ON RO.COD_ROL = UR.COD_ROL
      INNER JOIN ONTPPA.PPA_MENU ME
      ON OP.COD_MENU = ME.COD_MENU
      INNER JOIN ONTPPA.PPA_APLICACION AP
      ON ME.COD_APLICACION = AP.COD_APLICACION
      WHERE AR.COD_USUARIO = PV_COD_USUARIO
      AND AR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ASIG_ROL_ACTIVE
      AND ME.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_MENU_ACTIVE
      AND UR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USR_ROL_ACTIVE
      AND AP.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_APLICACION_ACTIVE
      AND US.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USUARIO_ACTIVE
      AND RO.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_ROL_ACTIVE;

    PN_COD_RESULTADO := 1;
    PV_MSG_RESULTADO := 'Consulta Exitosa';

  EXCEPTION
    WHEN OTHERS
      THEN
        PN_COD_RESULTADO := -1;
        PV_MSG_RESULTADO := '[[SP_PPA_S_USUARIO_APLICACION]] '|| TO_CHAR(SQLCODE) ||' : '||SQLERRM;
  END SP_PPA_S_USUARIO_APLICACION;
  */


  PROCEDURE SP_PPA_S_USUARIO_PERSONA(
    PV_USUARIO            IN VARCHAR2,
    PN_APLICACION          IN NUMBER,
    TC_LIST               OUT PCK_PPA_CONSTANTE.TYPCUR,
    PN_COD_RESULTADO      OUT NUMBER,
    PV_MSG_RESULTADO      OUT VARCHAR2
  )
  IS
  BEGIN
    OPEN TC_LIST FOR
      SELECT DISTINCT US.COD_USUARIO,
                      US.NOMBRES,
                      US.APE_PAT,
                      US.APE_MAT
        FROM ONTPPA.PPA_USUARIO US
       INNER JOIN ONTPPA.PPA_USER_ROL_APP UR
          ON UR.COD_USUARIO = US.COD_USUARIO
       WHERE US.USUARIO = UPPER(PV_USUARIO)
         AND UR.COD_APLICACION= PN_APLICACION
         AND US.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USUARIO_ACTIVE;

    PN_COD_RESULTADO := 0;
    PV_MSG_RESULTADO := 'Consulta Exitosa';

  EXCEPTION
    WHEN OTHERS
      THEN
        PN_COD_RESULTADO := -1;
        PV_MSG_RESULTADO := '[[SP_PPA_S_USUARIO_PERSONA]] '|| TO_CHAR(SQLCODE) ||' : '||SQLERRM;
  END SP_PPA_S_USUARIO_PERSONA;

-------------------------------------------------------------------------
 /*
 Author: Christian Altmairano
 */
  PROCEDURE SP_PPA_S_VALIDAR_CORREO( PV_CORREO       IN VARCHAR2,
                                     PN_APLICACION   IN NUMBER,
                                     PN_COD_USER     OUT NUMBER,
                                     PN_CODIGO       OUT NUMBER,
                                     PV_MENSAJE      OUT VARCHAR2) IS
  V_CANT NUMBER DEFAULT 0;                                   
  BEGIN
    SELECT COUNT(USR.CORREO)
      INTO V_CANT
      FROM PPA_USUARIO USR
         INNER JOIN PPA_USER_ROL_APP URA
            ON USR.COD_USUARIO = URA.COD_USUARIO
     WHERE URA.COD_APLICACION = PN_APLICACION
       AND UPPER(USR.CORREO) = UPPER(PV_CORREO)
       AND USR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USUARIO_ACTIVE
       AND ROWNUM <= 1;
                             
     IF V_CANT = 1 THEN
        SELECT  USR.COD_USUARIO
          INTO PN_COD_USER
          FROM PPA_USUARIO USR
             INNER JOIN PPA_USER_ROL_APP URA
                ON USR.COD_USUARIO = URA.COD_USUARIO
         WHERE URA.COD_APLICACION = PN_APLICACION
           AND UPPER(USR.CORREO) = UPPER(PV_CORREO)
           AND USR.ESTADO = PCK_PPA_CONSTANTE.PV_STATUS_USUARIO_ACTIVE
           AND ROWNUM <= 1;
           
        PN_CODIGO:=0;
        PV_MENSAJE:='VERIFICACION CORRECTA';
     ELSE
        PN_CODIGO:=1;
        --PV_MENSAJE:='El correo ingresado no existe para la aplicacion.';
         PV_MENSAJE:='CUENTA DE CORREO NO EXISTE PARA LA APLICACION';
        
     END IF;      
  END;


-----------------------------------------------------------------------------------
/*
  Author: Christian Altmirano
*/
 PROCEDURE SP_PPA_U_CONTRASENA( PV_NUEVA_CONTRA    IN  VARCHAR2,
                                 PN_COD_USUARIO    IN  NUMBER,
                                 PN_APLICACION     IN  NUMBER,
                                 PN_CODIGO         OUT NUMBER,
                                 PV_MENSAJE        OUT VARCHAR2) IS
 V_CANT NUMBER;
 E_VALIDACION EXCEPTION;                                 
 BEGIN
    SELECT COUNT(DISTINCT USR.CORREO)
      INTO V_CANT
      FROM PPA_USUARIO USR
         INNER JOIN PPA_USER_ROL_APP URA
            ON USR.COD_USUARIO = URA.COD_USUARIO
     WHERE URA.COD_APLICACION = PN_APLICACION
       AND USR.COD_USUARIO = PN_COD_USUARIO;
     
     IF V_CANT=0 THEN
       RAISE E_VALIDACION;
     ELSE
       UPDATE PPA_USUARIO USR
          SET USR.CLAVE = PV_NUEVA_CONTRA
        WHERE USR.COD_USUARIO = PN_COD_USUARIO;
     END IF;
     
     PN_CODIGO:= 0;
     PV_MENSAJE:='Transaccion satisfactoria.'; 
     COMMIT;  
 EXCEPTION
   WHEN  E_VALIDACION THEN
     PN_CODIGO:= 1;
     PV_MENSAJE:='Usuario no valido.';
     ROLLBACK;                             
 END;  

------------------------------------------------------------

 PROCEDURE SP_PPA_ISU_USUARIO(
    PV_USUARIO                   IN PPA_LOGIN.USUARIO%TYPE,
    PV_SYSDATETIME               IN VARCHAR2,
    PN_COD_APLICACION            IN NUMBER,
    PN_ESTADO                    OUT NUMBER,
    PN_COD_RESULTADO             OUT NUMBER,
    PV_MSG_RESULTADO             OUT VARCHAR2
  )
  AS
    l_max_int           NUMBER;
    l_max_bloq          NUMBER;
    l_tie_bloq          NUMBER;
    l_usu_exis_login    NUMBER;
    l_usu_exis_usuario  NUMBER;
    l_usu_bloq_temp     NUMBER;
    l_usu_bloq_def      NUMBER;
    l_num_int_usu       NUMBER;
    l_num_bloq_usu      NUMBER;
    -- PN_ESTADO - reintento:0, bloqueo:1 bloqueo definitivo:2 Usuario No existe: 3
  BEGIN
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    
    SELECT ABREVIATURA INTO l_max_int FROM PPA_PARAMETRO WHERE COD_PARAMETRO=139;
    SELECT ABREVIATURA INTO l_max_bloq FROM PPA_PARAMETRO WHERE COD_PARAMETRO=140;
    SELECT ABREVIATURA INTO l_tie_bloq FROM PPA_PARAMETRO WHERE COD_PARAMETRO=141;
    
    SELECT COUNT(1) INTO l_usu_exis_login FROM PPA_LOGIN WHERE USUARIO = PV_USUARIO AND COD_APLICACION = PN_COD_APLICACION;    
   
   IF l_usu_exis_login = 0 THEN
   
    SELECT COUNT(1) INTO l_usu_exis_usuario FROM PPA_USUARIO WHERE USUARIO = PV_USUARIO;
    IF l_usu_exis_usuario =0 THEN
        PN_ESTADO := 3;
    ELSE
        INSERT INTO PPA_LOGIN (cod_login, cod_aplicacion, usuario, num_intentos, fecha_intento, fecha_fin_bloqueo, num_bloqueos) 
        VALUES (PPA_LOGIN_SEQ.nextval,PN_COD_APLICACION,PV_USUARIO,1,to_date(PV_SYSDATETIME, 'DD/MM/YYYY HH24:MI:SS'),null,0);
        PN_ESTADO := 0;
    END IF;   
    
    ELSE
        SELECT COUNT(1) INTO l_usu_bloq_def FROM PPA_LOGIN WHERE USUARIO = PV_USUARIO AND ESTADO = 'I' 
        AND COD_APLICACION = PN_COD_APLICACION;
        IF l_usu_bloq_def = 0  THEN
        
            SELECT COUNT(1) INTO l_usu_bloq_temp FROM PPA_LOGIN WHERE USUARIO = PV_USUARIO AND ESTADO = 'A'
            AND COD_APLICACION = PN_COD_APLICACION
            AND to_date(PV_SYSDATETIME, 'DD/MM/YYYY HH24:MI:SS') >= FECHA_INTENTO
            AND to_date(PV_SYSDATETIME, 'DD/MM/YYYY HH24:MI:SS') <= FECHA_FIN_BLOQUEO;
                    
            IF l_usu_bloq_temp = 0 THEN
                SELECT NUM_INTENTOS INTO l_num_int_usu FROM PPA_LOGIN WHERE USUARIO = PV_USUARIO AND ESTADO = 'A'
                AND COD_APLICACION = PN_COD_APLICACION;
                
                IF l_num_int_usu < (l_max_int-1) THEN
                    UPDATE PPA_LOGIN 
                    SET NUM_INTENTOS = l_num_int_usu+1, 
                    FECHA_INTENTO = to_date(PV_SYSDATETIME, 'DD/MM/YYYY HH24:MI:SS') 
                    WHERE USUARIO = PV_USUARIO AND ESTADO = 'A' AND COD_APLICACION = PN_COD_APLICACION;
                    
                    PN_ESTADO := 0;
                ELSE
                    SELECT NUM_BLOQUEOS INTO l_num_bloq_usu FROM PPA_LOGIN WHERE USUARIO = PV_USUARIO AND ESTADO = 'A'
                    AND COD_APLICACION = PN_COD_APLICACION;
                    IF l_num_bloq_usu < (l_max_bloq-1) THEN
                        UPDATE PPA_LOGIN 
                        SET NUM_INTENTOS = 0,
                        FECHA_INTENTO = to_date(PV_SYSDATETIME, 'DD/MM/YYYY HH24:MI:SS'),
                        FECHA_FIN_BLOQUEO = to_date(PV_SYSDATETIME, 'DD/MM/YYYY HH24:MI:SS') + l_tie_bloq/24/60,
                        NUM_BLOQUEOS = l_num_bloq_usu+1
                        WHERE USUARIO = PV_USUARIO AND ESTADO = 'A' AND COD_APLICACION = PN_COD_APLICACION;
                        
                        PN_ESTADO := 1;
                    ELSE
                        UPDATE PPA_LOGIN 
                        SET ESTADO = 'I',
                        NUM_INTENTOS = l_num_int_usu+1,
                        FECHA_INTENTO = to_date(PV_SYSDATETIME, 'DD/MM/YYYY HH24:MI:SS'),
                        NUM_BLOQUEOS = l_num_bloq_usu+1
                        WHERE USUARIO = PV_USUARIO
                        AND COD_APLICACION = PN_COD_APLICACION;
                        
                        PN_ESTADO := 2;
                    END IF;
                    
                END IF;
                
                PN_ESTADO := 0;
            ELSE
                PN_ESTADO := 1;   
            END IF;
            
        ELSE
        PN_ESTADO := 2;
        END IF;
    END IF;
    
    COMMIT;
    PN_COD_RESULTADO := 0;
    PV_MSG_RESULTADO := 'Consulta Exitosa';

  EXCEPTION
    WHEN OTHERS
      THEN
        PN_COD_RESULTADO := -1;
        PV_MSG_RESULTADO := '[[SP_PPA_ISU_USUARIO]] '|| TO_CHAR(SQLCODE) ||' : '||SQLERRM;
  END SP_PPA_ISU_USUARIO;

------------------------------------------------------------

 PROCEDURE SP_PPA_U_USUARIO(
    PV_USUARIO                   IN PPA_LOGIN.USUARIO%TYPE,
    PV_SYSDATETIME               IN VARCHAR2,
    PN_COD_APLICACION            IN NUMBER,
    PN_COD_RESULTADO             OUT NUMBER,
    PV_MSG_RESULTADO             OUT VARCHAR2
  )
  AS
  BEGIN
    
    PN_COD_RESULTADO := -1;
    PV_MSG_RESULTADO := 'valor inicial';
    
    SP_PPA_S_USUARIO_BLOQUEADO(PV_USUARIO, PV_SYSDATETIME, PN_COD_RESULTADO,PV_MSG_RESULTADO);
    
    DBMS_OUTPUT.PUT_LINE('BLOQUEADO:' || PN_COD_RESULTADO);
    
    IF PN_COD_RESULTADO = 0 THEN
        
        UPDATE PPA_LOGIN 
        SET NUM_INTENTOS = 0,
        FECHA_INTENTO = to_date(PV_SYSDATETIME, 'DD/MM/YYYY HH24:MI:SS'),
        FECHA_FIN_BLOQUEO = NULL,
        NUM_BLOQUEOS = 0
        WHERE USUARIO = PV_USUARIO AND ESTADO = 'A' AND COD_APLICACION = PN_COD_APLICACION;
        
        PN_COD_RESULTADO := 0;
        PV_MSG_RESULTADO := 'CONSULTA EXITOSA';
    
    ELSE
    
        PN_COD_RESULTADO:= 3;
        PV_MSG_RESULTADO := 'USUARIO BLOQUEADO - NO SE PUEDE RESETEAR EL REINTENTO';
    
    END IF;
     
    COMMIT;


  EXCEPTION
    WHEN OTHERS
      THEN
        PN_COD_RESULTADO := -1;
        PV_MSG_RESULTADO := '[[SP_PPA_U_USUARIO]] '|| TO_CHAR(SQLCODE) ||' : '||SQLERRM;
  END SP_PPA_U_USUARIO;
  
  --------------------------------------------------------------------
 PROCEDURE SP_PPA_S_USUARIO_POR_TOKEN(
    PV_TOKEN                     IN VARCHAR2,
    PV_COD_USUARIO               OUT NUMBER,
    PV_DES_USUARIO               OUT VARCHAR2,
    PN_COD_RESULTADO             OUT NUMBER,
    PV_MSG_RESULTADO             OUT VARCHAR2
 ) IS
 
 V_USUARIO VARCHAR2(50);
 V_CANT_USUARIO_TOKEN NUMBER;
 V_COD_USUARIO NUMBER ;
 V_DES_USUARIO VARCHAR2(100);
 
 BEGIN
   
   SELECT COUNT(OAUTH.USER_NAME)
     INTO V_CANT_USUARIO_TOKEN
     FROM OAUTH_ACCESS_TOKEN OAUTH
    WHERE OAUTH.TOKEN_ID = PV_TOKEN;
 
   IF V_CANT_USUARIO_TOKEN > 0 THEN
     SELECT OAUTH.USER_NAME
       INTO V_USUARIO
       FROM OAUTH_ACCESS_TOKEN OAUTH
      WHERE OAUTH.TOKEN_ID = PV_TOKEN;
        
     DBMS_OUTPUT.PUT_LINE(V_USUARIO); 
     SELECT USU.COD_USUARIO, USU.USUARIO
       INTO V_COD_USUARIO, V_DES_USUARIO
       FROM PPA_USUARIO USU
      WHERE TRIM(USU.USUARIO) = TRIM(V_USUARIO);

     PV_COD_USUARIO:= V_COD_USUARIO;
     PV_DES_USUARIO:= V_DES_USUARIO;              
     PN_COD_RESULTADO:=0;
     PV_MSG_RESULTADO:='Consulta exitósa.';
       
   ELSE 
     PN_COD_RESULTADO:=1;
     PV_MSG_RESULTADO:='Datos no encontrados para el token ingresado.';
   END IF;
  
 END SP_PPA_S_USUARIO_POR_TOKEN;
 
  /*---------------------------------------------------------------------------
    Author  : Enrique Manuel Paiva Rodriguez
    Created : 02/08/2019 
    Purpose : Validacion de estado de usuario para obtener token 
---------------------------------------------------------------------------*/    
PROCEDURE SP_PPA_S_USUARIO_BLOQUEADO(
    PV_USUARIO                   IN VARCHAR2,
    PV_SYSDATETIME               IN VARCHAR2,
    PN_COD_RESULTADO             OUT NUMBER,
    PV_MSG_RESULTADO             OUT VARCHAR2
 ) IS
    N_CONTADOR NUMBER;
    BEGIN
        SELECT 
            COUNT(1) INTO N_CONTADOR
        FROM 
            PPA_LOGIN 
        WHERE 
            USUARIO = PV_USUARIO AND ESTADO = 'A' AND 
            FECHA_FIN_BLOQUEO IS NOT NULL AND
            NUM_BLOQUEOS > 0 AND 
            TO_DATE(PV_SYSDATETIME, 'DD/MM/YYYY HH24:MI:SS') <= FECHA_FIN_BLOQUEO;

        IF N_CONTADOR > 0 THEN
            PN_COD_RESULTADO := 1;
        ELSE
            PN_COD_RESULTADO:= 0;
        END IF;
        
        PV_MSG_RESULTADO:='CONSULTA EXITOSA';
        
 END SP_PPA_S_USUARIO_BLOQUEADO;

END PCK_PPA_SEGURIDAD;

/