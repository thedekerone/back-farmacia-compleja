CREATE OR REPLACE PACKAGE SISOPE.PCK_WS_CONTRATO_SERVICIO
AS

--01 CONSULTA AFILIADO
   PROCEDURE PRC_CONSULTA_AFI (
                               PN_INI         IN     NUMBER,   --01 REGISTRO INICIO
                               PN_FIN         IN     NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS      IN     VARCHAR2, --03 TIPO DE BUSQUEDA: 1. Por apellidos y nombres/2. Documento de identidad/3. C�digo de afiliado
                               PV_CODAFI      IN     VARCHAR2, --04 CODIGO DE AFILIADO
                               PV_TIPDOC      IN     VARCHAR2, --05 TIPO DE DOCUMENTO
                               PV_NUMDOC      IN     VARCHAR2, --06 NUMERO DE DOCUMENTO
                               PV_APEPAT      IN     VARCHAR2, --07 APELLIDO PATERNO
                               PV_APEMAT      IN     VARCHAR2, --08 APELLIDO MATERNO
                               PV_NOMAFI      IN     VARCHAR2, --09 NOMBRE DE AFILIADO
                               PV_VIGREG      IN     VARCHAR2, --10 VIGENCIA DE REISTRO 1 ACTIVO, 0 TODOS
                               PO_TOTAL          OUT NUMBER,   --11 TOTAL DE REGISTROS
                               PO_IDTRAN         OUT VARCHAR2, --12 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA         OUT VARCHAR2, --13 FECHA DE LA TRANSACCI�N
                               PO_CODRES         OUT VARCHAR2, --14 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES         OUT VARCHAR2, --15 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET      OUT SYS_REFCURSOR);

   FUNCTION FNC_ES_PACIENTE (PV_CODAFI  VARCHAR2)             --CODIGO DE AFILIADO
      RETURN VARCHAR2;

   FUNCTION FNC_DIRECCION (PV_CODPNA    VARCHAR2)                 --CODIGO DE PERSONA
      RETURN VARCHAR2;

--02 PLAN AFILIADO
   PROCEDURE PRC_PLAN_AFI (
                           PV_TIPBUS      IN     VARCHAR2,        --01 TIPO DE BUSQUEDA: 1. C�digo de afiliado/2. Documento de identidad/
                           PV_CODAFI      IN     VARCHAR2,        --02 CODIGO DE AFILIADO
                           PV_TIPDOC      IN     VARCHAR2,        --03 TIPO DE DOCUMENTO
                           PV_NUMDOC      IN     VARCHAR2,        --04 NUMERO DE DOCUMENTO
                           PO_IDTRAN         OUT VARCHAR2, --05 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                           PO_FECTRA         OUT VARCHAR2, --06 FECHA DE LA TRANSACCI�N
                           PO_CODRES         OUT VARCHAR2, --07 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                           PO_MENRES         OUT VARCHAR2, --08 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                           RESULTSET      OUT SYS_REFCURSOR);
--03 CUOTAS IMPAGAS

--04 NOMBRES AFILIADO
   PROCEDURE PRC_NOMBRES_AFI ( 
                               PN_INI           IN      NUMBER,   --01 REGISTRO INICIO        
                               PN_FIN           IN      NUMBER,   --02 REGISTRO FIN
                               PV_CODAFI        IN      VARCHAR2, --03 CODIGO DE AFILIADO
                               PO_TOTAL         OUT     NUMBER,   --04 TOTAL REGISTROS
                               PO_IDTRAN        OUT     VARCHAR2, --05 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA        OUT VARCHAR2,     --06 FECHA DE LA TRANSACCI�N
                               PO_CODRES        OUT     VARCHAR2, --07 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES        OUT     VARCHAR2, --08 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET        OUT     SYS_REFCURSOR);

--05 CONTACTO AFILIADO
   PROCEDURE PRC_CONTACTO_AFI( PV_CODAFI        IN      VARCHAR2, --01 CODIGO DE AFILIADO
                               PO_IDTRAN        OUT     VARCHAR2, --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA        OUT VARCHAR2,     --03 FECHA DE LA TRANSACCI�N
                               PO_CODRES        OUT     VARCHAR2, --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES        OUT     VARCHAR2, --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET        OUT     SYS_REFCURSOR);

--06 CONSULTA PACIENTE AFILIADO
   PROCEDURE PRC_PACIENTE_AFI (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA: 1. Por apellidos y nombres/2. Documento de identidad/3. C�digo de afiliado
                               PV_CODAFI            IN      VARCHAR2, --04 CODIGO DE AFILIADO
                               PV_TIPDOC            IN      VARCHAR2, --05 TIPO DE DOCUMENTO
                               PV_NUMDOC            IN      VARCHAR2, --06 NUMERO DE DOCUMENTO
                               PV_APEPAT            IN      VARCHAR2, --07 APELLIDO PATERNO
                               PV_APEMAT            IN      VARCHAR2, --08 APELLIDO MATERNO
                               PV_NOMAFI            IN      VARCHAR2, --09 NOMBRE DE AFILIADO
                               PO_TOTAL             OUT     NUMBER,   --10 TOTAL REGISTROS
                               PO_IDTRAN            OUT     VARCHAR2, --11 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT VARCHAR2,     --12 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --13 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --14 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR);


--07 LISTA CLINICAS
   PROCEDURE PRC_LISTA_CLINICAS (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA 1. TODAS LAS CLINICAS, 2. POR CODIGO, 3. POR DESCRIPCION
                               PV_CODCLI            IN      VARCHAR2, --04 CODIGO DE CLINICA O IPRESS
                               PV_NOMCLI            IN      VARCHAR2, --05 NOMBRE DE CLINICA O IPRESS
                               PO_TOTAL             OUT     NUMBER,   --06 REGISTRO TOTAL
                               PO_IDTRAN            OUT     VARCHAR2, --07 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT VARCHAR2,     --08 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --09 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --10 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR);

--08 LISTA DIAGNOSTICO
   PROCEDURE PRC_LISTA_DIAGNOSTICO (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA 1. POR CODIGO, 2. POR DESCRIPCION
                               PV_CODDIA            IN      VARCHAR2, --04 CODIGO DIAGNOSTICO
                               PV_NOMDIA            IN      VARCHAR2, --05 NOMBRE DIAGNOSTICO
                               PO_TOTAL             OUT     NUMBER,   --06 TOTAL REGISTROS
                               PO_IDTRAN            OUT     VARCHAR2, --07 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT VARCHAR2,     --08 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --09 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --10 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR);

--09 GRUPO DIAGNOSTICO
   PROCEDURE PRC_GRUPO_DIAGNOSTICO (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA 1. POR CODIGO, 2. POR DESCRIPCION
                               PV_CODGRU            IN      VARCHAR2, --04 CODIGO GRUPO DIAGNOSTICO
                               PV_NOMGRU            IN      VARCHAR2, --05 NOMBRE GRUPO DIAGNOSTICO
                               PO_TOTAL             OUT     NUMBER,   --06 REGISTRO TOTAL
                               PO_IDTRAN            OUT     VARCHAR2, --07 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT VARCHAR2,     --08 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --09 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --10 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR);

--10 MEDICO TRATANTE
   PROCEDURE PRC_MED_TRATANTE  (PV_CODAFI            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                PO_IDTRAN            OUT     VARCHAR2,   --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                PO_FECTRA            OUT VARCHAR2,       --03 FECHA DE LA TRANSACCI�N
                                PO_CODRES            OUT     VARCHAR2,   --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                PO_MENRES            OUT     VARCHAR2,   --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                RESULTSET            OUT     SYS_REFCURSOR);

--11 LISTA MEDICO TRATANTE
   PROCEDURE PRC_LIS_MED_TRATANTE  (PN_INI               IN      NUMBER,     --01 REGISTRO INICIO
                                    PN_FIN               IN      NUMBER,     --02 REGISTRO FIN
                                    PV_TIPBUS            IN      VARCHAR2,   --03 CODIGO DE AFILIADO
                                    PV_CODMED            IN      VARCHAR2,   --04 CODIGO DE AFILIADO
                                    PV_PATMED            IN      VARCHAR2,   --05 CODIGO DE AFILIADO
                                    PV_MATMED            IN      VARCHAR2,   --06 CODIGO DE AFILIADO
                                    PV_NOMMED            IN      VARCHAR2,   --07 CODIGO DE AFILIADO                                                                        
                                    PV_CODCMP            IN      VARCHAR2,   --08 CODIGO MEDICO DE PERU
                                    PO_TOTAL             OUT     NUMBER,     --09 REGISTRO TOTAL
                                    PO_IDTRAN            OUT     VARCHAR2,   --10 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                    PO_FECTRA            OUT VARCHAR2,       --11 FECHA DE LA TRANSACCI�N
                                    PO_CODRES            OUT     VARCHAR2,   --12 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                    PO_MENRES            OUT     VARCHAR2,   --13 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                    RESULTSET            OUT     SYS_REFCURSOR);

--13 EMPRESA CONTRATANTE AFILIADO
   PROCEDURE PRC_EMP_CONTRATANTE( PV_CODAFI        IN      VARCHAR2, --01 CODIGO DE AFILIADO
                                  PO_IDTRAN        OUT     VARCHAR2, --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                  PO_FECTRA        OUT VARCHAR2,     --03 FECHA DE LA TRANSACCI�N
                                  PO_CODRES        OUT     VARCHAR2, --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                  PO_MENRES        OUT     VARCHAR2, --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                  RESULTSET        OUT     SYS_REFCURSOR);

--14 HISTORIAL DIAGNOSTICO PACIENTE
   PROCEDURE PRC_DIAG_PACIENTE (PV_CODAFI            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                PO_IDTRAN            OUT     VARCHAR2,   --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                PO_FECTRA            OUT VARCHAR2,       --03 FECHA DE LA TRANSACCI�N
                                PO_CODRES            OUT     VARCHAR2,   --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                PO_MENRES            OUT     VARCHAR2,   --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                RESULTSET            OUT     SYS_REFCURSOR);

   FUNCTION FNC_LINEA (PV_LINEA IN OUT VARCHAR2, PV_CARACTER VARCHAR2)
      RETURN VARCHAR2;

--15 CONSULTA PREEXISTENCIA AFILIADO 04/04/2019
   PROCEDURE PRC_PREEXI_AFI (   PV_CODAFI            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                PO_IDTRAN            OUT     VARCHAR2,   --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                PO_FECTRA            OUT     VARCHAR2,   --03 FECHA DE LA TRANSACCI�N
                                PO_CODRES            OUT     VARCHAR2,   --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                PO_MENRES            OUT     VARCHAR2,   --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                RESULTSET            OUT     SYS_REFCURSOR);


--16 CONSULTA PREEXISTENCIA AFILIADO 04/04/2019
   PROCEDURE PRC_EXCLU_AFI (   PV_CODAFI            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                PO_IDTRAN            OUT     VARCHAR2,   --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                PO_FECTRA            OUT     VARCHAR2,   --03 FECHA DE LA TRANSACCI�N
                                PO_CODRES            OUT     VARCHAR2,   --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                PO_MENRES            OUT     VARCHAR2,   --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                RESULTSET            OUT     SYS_REFCURSOR);

  FUNCTION FNC_UBIGEO (PV_CODPER    VARCHAR2) RETURN VARCHAR2;


--17 CONSULTA LISTA ESPECIALIDAD HOMOLOGADA 11/04/2019
   PROCEDURE PRC_LIS_ESP_HOMO (  
                                PO_IDTRAN            OUT     VARCHAR2,   --01 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                PO_FECTRA            OUT     VARCHAR2,   --02 FECHA DE LA TRANSACCI�N
                                PO_CODRES            OUT     VARCHAR2,   --03 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                PO_MENRES            OUT     VARCHAR2,   --04 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                RESULTSET            OUT     SYS_REFCURSOR);

--18 CONSULTA LISTA PACIENTES DETALLE 16/04/2019
   PROCEDURE PRC_LIS_PACIENTE( PV_CODAFI        IN      VARCHAR2, --01 CODIGO DE AFILIADO
                                   PO_IDTRAN        OUT     VARCHAR2, --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                   PO_FECTRA        OUT VARCHAR2,     --03 FECHA DE LA TRANSACCI�N
                                   PO_CODRES        OUT     VARCHAR2, --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                   PO_MENRES        OUT     VARCHAR2, --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                   RESULTSET        OUT     SYS_REFCURSOR);

--19 CONSULTA LISTA CLINICAS POR CODIGOS RENIPRESS 16/04/2019
   PROCEDURE PRC_LIS_CLINICAS_RE (
                               PV_CODCLI            IN      VARCHAR2, --01 CODIGO DE CLINICA O IPRESS
                               PO_IDTRAN            OUT     VARCHAR2, --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT VARCHAR2,     --03 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR);

--20 ACTIVACION PACIENTE
   PROCEDURE PRC_ACTIVA_PACIENTE (
                               PV_CODAFI            IN      VARCHAR2, --01 CODIGO DE AFILIADO
                               PV_CODUSU            IN      VARCHAR2, --02 USUARIO DE CLINICA
                               PV_CODPAIS           IN      VARCHAR2, --03 CODIGO DE PAIS
                               PV_CODUBI            IN      VARCHAR2, --04 CODIGO UBIGEO
                               PV_ESTCIV            IN      VARCHAR2, --05 ESTADO CIVIL  --SE ENCUENTRA EL TT_290
                               PV_CODPRO            IN      VARCHAR2, --06 CODIGO DE PROCEDENCIA --SE ENCUENTRA EN LA TT_268
                               PV_CODOCU            IN      VARCHAR2, --07 CODIGO DE OCUPACION --SE ENCUENTRA EN LA TT_308
                               PV_CODSUC            IN      VARCHAR2, --08 CODIGO SUCURSAL --SE ENCUENTRA EN LA TT_253
                               PV_CODDIA            IN      VARCHAR2, --09 CIE10 (SERAN SEPARADOS POR COMAS(,))
                               PV_FECDIA            IN      VARCHAR2, --10 FECHA CIE10 (SERAN SEPARADOS POR COMAS(,))
                               PV_CODMED            IN      VARCHAR2, --11 MEDICOS TRATANTES (SERAN SEPARADOS POR COMAS(,))
                               PV_FECMED            IN      VARCHAR2, --12 FECHA MEDICO (SERAN SEPARADOS POR COMAS(,))
                               PO_IDTRAN            OUT     VARCHAR2, --13 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT     VARCHAR2, --14 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --15 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2  --16 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               );

--21 LISTA MEDICO TRATANTE
   PROCEDURE PRC_LIS_MED_TRA_COD   (PV_CODMED            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                    PO_TOTAL             OUT     NUMBER,     --02 TOTAL REGISTROS
                                    PO_IDTRAN            OUT     VARCHAR2,   --03 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                    PO_FECTRA            OUT     VARCHAR2,   --04 FECHA DE LA TRANSACCI�N
                                    PO_CODRES            OUT     VARCHAR2,   --05 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                    PO_MENRES            OUT     VARCHAR2,   --06 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                    RESULTSET            OUT     SYS_REFCURSOR);

--22 DIAGNOSTICO POR PACIENTE
   PROCEDURE PRC_DIAG_PACIENTE_DET   (  PV_CODAFI            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                    PO_IDTRAN            OUT     VARCHAR2,   --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                    PO_FECTRA            OUT     VARCHAR2,   --03 FECHA DE LA TRANSACCI�N
                                    PO_CODRES            OUT     VARCHAR2,   --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                    PO_MENRES            OUT     VARCHAR2,   --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                    RESULTSET            OUT     SYS_REFCURSOR);


--23 LISTA PROGRAMAS 14/05/2019
   PROCEDURE PRC_LISTA_PROGRAMA (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA 1. TODOS, 2. POR CODIGO, 3. POR DESCRIPCION
                               PV_CODPRO            IN      VARCHAR2, --04 CODIGO PROGRAMA
                               PV_NOMPRO            IN      VARCHAR2, --05 NOMBRE PROGRAMA
                               PO_TOTAL             OUT     NUMBER,   --06 TOTAL REGISTROS
                               PO_IDTRAN            OUT     VARCHAR2, --07 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT     VARCHAR2, --08 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --09 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --10 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR);

--24 LISTA PLANES 14/05/2019
   PROCEDURE PRC_LISTA_PLANES (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA 1. TODOS, 2. POR CODIGO, 3. POR DESCRIPCION
                               PV_CODPLA            IN      VARCHAR2, --04 CODIGO PLANES
                               PV_NOMPLA            IN      VARCHAR2, --05 NOMBRE PLANES
                               PO_TOTAL             OUT     NUMBER,   --06 TOTAL REGISTROS
                               PO_IDTRAN            OUT     VARCHAR2, --07 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT     VARCHAR2, --08 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --09 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --10 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR);

--25 LISTA DIAGNOSTICO 05/06/2019
   PROCEDURE PRC_LISTA_DIAGNOS_DET (
                               PV_CODDIA            IN      VARCHAR2, --01 CODIGO DIAGNOSTICO
                               PO_IDTRAN            OUT     VARCHAR2, --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT     VARCHAR2, --03 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR);

END PCK_WS_CONTRATO_SERVICIO;
/

