CREATE OR REPLACE PACKAGE BODY SISOPE.PCK_WS_CONTRATO_SERVICIO
AS

 /******************************************************************************
 Nombre de paquete: SISOPE.PCK_WS_CONTRATO_SERVICIO
 
 Ver fecha Autor Descripci�n
 --------- ---------- --------------- ------------------------------------
 1.0 01/03/2019 LRengifo 1. Creaci�n de paquete
 ******************************************************************************/

--01 CONSULTA AFILIADO
 PROCEDURE PRC_CONSULTA_AFI (
 PN_INI IN NUMBER, --01 REGISTRO INICIO
 PN_FIN IN NUMBER, --02 REGISTRO FIN
 PV_TIPBUS IN VARCHAR2, --03 TIPO DE BUSQUEDA: 1. Por apellidos y nombres/2. Documento de identidad/3. C�digo de afiliado
 PV_CODAFI IN VARCHAR2, --04 CODIGO DE AFILIADO
 PV_TIPDOC IN VARCHAR2, --05 TIPO DE DOCUMENTO
 PV_NUMDOC IN VARCHAR2, --06 NUMERO DE DOCUMENTO
 PV_APEPAT IN VARCHAR2, --07 APELLIDO PATERNO
 PV_APEMAT IN VARCHAR2, --08 APELLIDO MATERNO
 PV_NOMAFI IN VARCHAR2, --09 NOMBRE DE AFILIADO
 PV_VIGREG IN VARCHAR2, --10 VIGENCIA DE REISTRO 1 ACTIVO, 0 TODOS
 PO_TOTAL OUT NUMBER, --11 TOTAL DE REGISTROS
 PO_IDTRAN OUT VARCHAR2, --12 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
 PO_FECTRA OUT VARCHAR2, --13 FECHA DE LA TRANSACCI�N
 PO_CODRES OUT VARCHAR2, --14 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
 PO_MENRES OUT VARCHAR2, --15 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
 RESULTSET OUT SYS_REFCURSOR)
 IS
 PV_ERROR VARCHAR2 (4000) := NULL;
 PN_VALOR NUMBER := 0;
 DATO_ERR EXCEPTION;
 PV_USR VARCHAR2(20) := NULL;
 BEGIN

--PARA BUSQUEDAS DE TIPO 1 POR NOMBRES
 IF PV_TIPBUS = '1'
 THEN

 
 SELECT TRIM (USERENV ('SESSIONID'))
 INTO PV_USR
 FROM DUAL;

 BEGIN

 INSERT INTO SISOPE.CONTROL_AFI (CODAFI, CODUSR, PGRCODCIA, PGRCODSUC, PGRCODSAF, 
 PGRCODGFA, PGRCODPNA, PGRFECBAJ, PGRFECAFL, PGRFECCON, 
 PGRFECURE, PGRFECGFF, PGRCODCAT, PGRCODPRG, PNADESAPA, 
 PNADESAMA, PNANOMBRE, PNANOMBR2, PNAFECNAC, PNATIPDOC, 
 PNADOCIDE, PNACODSEX,GRPEMPGRP)
 SELECT '1'||PGRCODGFA||PGRCODPNA, PV_USR,
 PGRCODCIA,PGRCODSUC,PGRCODSAF,PGRCODGFA,PGRCODPNA,
 TO_CHAR(PGRFECBAJ,'DD/MM/RRRR'),TO_CHAR(PGRFECAFL,'DD/MM/RRRR'),TO_CHAR(PGRFECCON,'DD/MM/RRRR'),
 TO_CHAR(PGRFECURE,'DD/MM/RRRR'),TO_CHAR(PGRFECGFF,'DD/MM/RRRR'),
 PGRCODCAT,PGRCODPRG,PNADESAPA,PNADESAMA,PNANOMBRE,
 PNANOMBR2,TO_CHAR(PNAFECNAC,'DD/MM/RRRR'),
 PNATIPDOC,PNADOCIDE,PNACODSEX,NULL
 FROM PBFPOBGRP PBF, PNADATMAE PNA
 WHERE PBF.PGRCODCIA = PNA.PNACODCIA
 AND PBF.PGRCODSUC = PNA.PNACODSUC
 AND PBF.PGRCODPNA = PNA.PNACODPNA
 AND PNACODCIA = '001'
 AND PNACODSUC = '001'
 AND (PNADESAPA LIKE PV_APEPAT || '%'
 AND NVL (PNADESAMA, ' ') LIKE PV_APEMAT || '%'
 AND PNANOMBRE || ' %' || PNANOMBR2 LIKE
 REPLACE(PV_NOMAFI,' ', '%') || '%');
 COMMIT;
 
 INSERT INTO SISOPE.CONTROL_AFI (CODAFI, CODUSR, PGRCODCIA, PGRCODSUC, PGRCODSAF, 
 PGRCODGFA, PGRCODPNA, PGRFECBAJ, PGRFECAFL, PGRFECCON, 
 PGRFECURE, PGRFECGFF, PGRCODCAT, PGRCODPRG, PNADESAPA, 
 PNADESAMA, PNANOMBRE, PNANOMBR2, PNAFECNAC, PNATIPDOC, 
 PNADOCIDE, PNACODSEX,GRPEMPGRP)
 SELECT '2'||LPAD(GRPCODONC,8,'0')||GRPCODPNA,PV_USR,GRPCODCIA,GRPCODSUC,NULL,
 LPAD(GRPCODONC,8,'0'),GRPCODPNA,TO_CHAR(GRPFECBAJ,'DD/MM/RRRR'),TO_CHAR(GRPFECING,'DD/MM/RRRR'),TO_CHAR(GRPFECCON,'DD/MM/RRRR'),
 NULL, NULL,GRPCATGRP, NULL,PNADESAPA, 
 PNADESAMA, PNANOMBRE, PNANOMBR2, TO_CHAR(PNAFECNAC,'DD/MM/RRRR'), PNATIPDOC, 
 PNADOCIDE, PNACODSEX, GRPEMPGRP
 FROM GRPPOBGRP PBF, PNADATMAE PNA
 WHERE PBF.GRPCODCIA = PNA.PNACODCIA
 AND PBF.GRPCODSUC = PNA.PNACODSUC
 AND PBF.GRPCODPNA = PNA.PNACODPNA
 AND PNACODCIA = '001'
 AND PNACODSUC = '001'
 AND grpcodcia = '001'
 AND grpcodsuc = '001'
 AND (PNADESAPA LIKE PV_APEPAT || '%'
 AND NVL (PNADESAMA, ' ') LIKE PV_APEMAT || '%'
 AND PNANOMBRE || '%' || PNANOMBR2 LIKE
 REPLACE(PV_NOMAFI,' ','%') || '%')
 ;
 COMMIT;

 END;

 SELECT COUNT(1)
 INTO PN_VALOR
 FROM CONTROL_AFI 
 WHERE CODUSR=PV_USR
 AND ((PGRFECBAJ IS NULL AND PCK_WS_SITEDS_V10.FN_OBT_ESTADO (CODAFI,NULL, PGRCODSAF)=PV_VIGREG )OR '0'=PV_VIGREG)
 ;

 IF PN_VALOR < 1
 THEN
 RAISE DATO_ERR;
 END IF;

 SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
 TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
 INTO PO_IDTRAN, PO_FECTRA
 FROM DUAL;

 PO_CODRES := '0';
 PO_MENRES := 'DATOS CORRECTOS';
 PO_TOTAL := PN_VALOR;

 OPEN RESULTSET FOR
 

 SELECT 
 RN, --01 NUMERO
 TRIM(PNADESAPA) APEPAT, --02 APELLIDO PATERNO
 TRIM(PNADESAMA) APEMAT, --03 APELLIDO MATERNO
 TRIM(PNANOMBRE) NOMBR1, --04 NOMBRE 1
 TRIM(PNANOMBR2) NOMBR2, --05 OTROS NOMBRES
 CODAFI CODAFIR, --06 CODIGO DE AFILIADO
 DECODE(SUBSTR(CODAFI,1,1),'1','INDIVIDUAL','2','GRUPAL') TIPAFI, --07 TIPO AFILIADO
 DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
 '024',
 PNATIPDOC,
 'ABRDOCID'),
 NULL, 'DNI',
 PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
 '024',
 PNATIPDOC,
 'ABRDOCID'))
 TIPDOC, --08 TIPO DOCUMENTO IDENTIDAD
 PNADOCIDE NUMDOC, --09 NUMERO DE DOCUMENTO
 PGRFECAFL FECAFI, --10 FECHA DE INGRESO DEL AFILIADO
 PGRFECURE FECREN, --11 FECHA DE RENOVACION DEL AFILIADO
 PGRFECBAJ FECBAJ, --12 FECHA DE BAJA DEL AFILIADO
 PCK_WS_CONTRATO_SERVICIO.FNC_ES_PACIENTE ('1' || PGRCODGFA || PGRCODPNA) 
 ESPACI, --13 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
 PNAFECNAC FECNAC, --14 FECHA DE NACIMIENTO DEL AFILIADO
 FLOOR (MONTHS_BETWEEN (SYSDATE, TO_DATE(PNAFECNAC,'DD/MM/RRRR')) / 12)
 EDAAFI, --15 EDAD DEL AFILIADO
 DECODE (PNACODSEX, 'M', 'MASCULINO', 'F', 'FEMENINO')
 SEXAFI, --16 SEXO AFILIADO
 F_PER_TELEFONO ('001001', PGRCODPNA)
 TELAFI, --17 TELEFONO FIJO AFILIADO
 F_PER_CELULAR ('001001', PGRCODPNA)
 CELAFI, --18 CELULAR AFILIADO
 PCK_WS_CONTRATO_SERVICIO.FNC_DIRECCION (PGRCODPNA)
 DIRAFI, --19 DIRECCION AFILIADO
 FNC_OBTEN_UBIGEO (PGRCODPNA, 'DIST')
 DISAFI, --20 DISTRITO DEL AFILIADO
 REFAFI, --21 Referencia del afiliado
 F_PER_EMAIL ('001001', PGRCODPNA)
 EMAAFI, --22 CORREO ELECTRONICO AFILIADO
 PGRCODGFA GRUAFI, --23 GRUPO FAMILIAR DEL AFILIADO
 PCK_AFIL_RENO.FNC_DESC_PROGRAMA (PGRCODCIA || PGRCODSUC, PGRCODPRG)
 PROAFI, --24 PROGRAMA DEL AFILIADO
 PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL (PGRCODCIA || PGRCODSUC,
 '056',
 PGRCODCAT,
 'NOMCORTO')
 CATAFI, --25 CATEGOR�A DE AFILIADO
 PGRFECCON FECCON, --26 FECHA DE CONTINUIDAD DEL AFILIADO
 DECODE(SUBSTR(CODAFI,1,1),'1',PCK_WS_SITEDS_V10.FN_OBT_ESTADO (CODAFI,TO_DATE(PGRFECBAJ,'DD/MM/RRRR'), PGRCODSAF),
 '2',PCK_WS_SITEDS_V10.FN_OBT_ESTADO (CODAFI,TO_DATE(PGRFECBAJ,'DD/MM/RRRR'), PGRCODSAF))
 ESTAFI, --27 ESTADO AFILIADO
 PCK_AFIL_RENO.FNC_VAL_FCHACT_PAC (PGRCODCIA || PGRCODSUC,NULL,PGRCODPNA)
 FECACT, --28 FECHA DE ACTUALIZACI�N DEL PACIENTE
 DECODE(SUBSTR(CODAFI,1,1),'1',PCK_WS_SITEDS_V10.FN_OBT_DATOS_CONTRATANTE_IND(PGRCODPNA,'5'),
 '2',PCK_WS_SITEDS_V10.FN_OBT_DATOS_CONTRATANTE_GRP(PGRCODPNA,GRPEMPGRP,2))
 CONAFI, --29 CONTRATANTE
 PCK_WS_SITEDS_V10.FN_OBTIENE_PLAN(PGRCODGFA,PGRCODPNA,SUBSTR(CODAFI,1,1),GRPEMPGRP)
 CODPLAN, --30 CODIGO DE PLAN
 PCK_WS_SITEDS.FN_OBTIENE_PLAN_NOM(PGRCODGFA,PGRCODPNA,SUBSTR(CODAFI,1,1),GRPEMPGRP)
 NOMPLAN --31 NOMBRE PLAN
 
 FROM (
 SELECT 
 ROW_NUMBER() OVER (ORDER BY PNADESAPA,PNADESAMA, PNANOMBRE, PNANOMBR2) RN,
 CODAFI, CODUSR, PGRCODCIA, PGRCODSUC, PGRCODSAF, 
 PGRCODGFA, PGRCODPNA, PGRFECBAJ, PGRFECAFL, PGRFECCON, 
 PGRFECURE, PGRFECGFF, PGRCODCAT, PGRCODPRG, PNADESAPA, 
 PNADESAMA, PNANOMBRE, PNANOMBR2, PNAFECNAC, PNATIPDOC, 
 PNADOCIDE, PNACODSEX, GRPEMPGRP, TIPAFI, TIPDOC, ESPACI, EDAAFI, 
 SEXAFI, TELAFI, CELAFI, DIRAFI, DISAFI, REFAFI, EMAAFI, GRUAFI, 
 PROAFI, CATAFI, FECCON, ESTAFI, FECACT, CONAFI, CODPLAN, NOMPLAN
 FROM CONTROL_AFI 
 WHERE CODUSR=PV_USR
 AND ((PGRFECBAJ IS NULL AND PCK_WS_SITEDS_V10.FN_OBT_ESTADO (CODAFI,NULL, PGRCODSAF)=PV_VIGREG) OR '0'=PV_VIGREG)
 ) WHERE RN BETWEEN PN_INI AND PN_FIN ;

 
 DELETE SISOPE.CONTROL_AFI 
 WHERE CODUSR=PV_USR;
 COMMIT;
 
 
--PARA BUSQUEDAS DE TIPO 2 NUMERO DE DOCUMENTO 
 ELSIF PV_TIPBUS = '2'
 THEN
 SELECT SUM (CAN)
 INTO PN_VALOR
 FROM (SELECT COUNT (1) CAN
 FROM PBFPOBGRP PBF, PNADATMAE PNA
 WHERE PBF.PGRCODCIA = PNA.PNACODCIA
 AND PBF.PGRCODSUC = PNA.PNACODSUC
 AND PBF.PGRCODPNA = PNA.PNACODPNA
 AND PNACODCIA = '001'
 AND PNACODSUC = '001'
 AND PNADOCIDE LIKE PV_NUMDOC || '%'
 AND (PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('1'||PGRCODGFA||PGRCODPNA,PGRFECBAJ, PGRCODSAF)=PV_VIGREG OR '0'=PV_VIGREG)
 
 UNION
 SELECT COUNT (1) CAN
 FROM GRPPOBGRP PBF, PNADATMAE PNA
 WHERE PBF.GRPCODCIA = PNA.PNACODCIA
 AND PBF.GRPCODSUC = PNA.PNACODSUC
 AND PBF.GRPCODPNA = PNA.PNACODPNA
 AND PNACODCIA = '001'
 AND PNACODSUC = '001'
 AND grpcodcia = '001'
 AND grpcodsuc = '001'
 AND PNADOCIDE LIKE PV_NUMDOC || '%'
 AND (PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('2'||LPAD(GRPCODONC,8,'0')||GRPCODPNA,GRPFECBAJ, NULL)=PV_VIGREG OR '0'=PV_VIGREG)
 );

 IF PN_VALOR < 1
 THEN
 RAISE DATO_ERR;
 END IF;

 SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
 TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
 INTO PO_IDTRAN, PO_FECTRA
 FROM DUAL;

 PO_CODRES := '0';
 PO_MENRES := 'DATOS CORRECTOS';
 PO_TOTAL := PN_VALOR;

 OPEN RESULTSET FOR
 SELECT * FROM( 
 SELECT 
 ROW_NUMBER() OVER (ORDER BY APEPAT,APEMAT,NOMBR1,NOMBR2) RN,--01 NUMERO DE REGISTRO
 APEPAT, --02 APELLIDO PATERNO
 APEMAT, --03 APELLIDO MATERNO
 NOMBR1, --04 NOMBRE 1
 NOMBR2, --05 OTROS NOMBRES
 CODAFIR, --06 CODIGO DE AFILIADO
 TIPAFI, --07 TIPO AFILIADO
 TIPDOC, --08 TIPO DOCUMENTO IDENTIDAD
 NUMDOC, --09 NUMERO DE DOCUMENTO
 FECAFI, --10 FECHA DE INGRESO DEL AFILIADO
 FECREN, --11 FECHA DE RENOVACION DEL AFILIADO
 FECBAJ, --12 FECHA DE BAJA DEL AFILIADO
 ESPACI, --13 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
 FECNAC, --14 FECHA DE NACIMIENTO DEL AFILIADO
 EDAAFI, --15 EDAD DEL AFILIADO
 SEXAFI, --16 SEXO AFILIADO
 TELAFI, --17 TELEFONO FIJO AFILIADO
 CELAFI, --18 CELULAR AFILIADO
 DIRAFI, --19 DIRECCION AFILIADO
 DISAFI, --20 DISTRITO DEL AFILIADO
 REFAFI, --21 Referencia del afiliado
 EMAAFI, --22 CORREO ELECTRONICO AFILIADO
 GRUAFI, --23 GRUPO FAMILIAR DEL AFILIADO
 PROAFI, --24 PROGRAMA DEL AFILIADO
 CATAFI, --25 CATEGOR�A DE AFILIADO
 PGRFECCON, --26 FECHA DE CONTINUIDAD DEL AFILIADO
 ESTAFI, --27 ESTADO AFILIADO
 FECACT, --28 FECHA DE ACTUALIZACI�N DEL PACIENTE
 CONAFI, --29 CONTRATANTE
 CODPLAN, --30 CODIGO DE PLAN
 NOMPLAN --31 NOMBRE PLAN
 FROM(
 SELECT
 TRIM (PNADESAPA) APEPAT, --01 APELLIDO PATERNO
 TRIM (PNADESAMA) APEMAT, --02 APELLIDO MATERNO
 TRIM (PNANOMBRE) NOMBR1, --03 NOMBRE 1
 TRIM (PNANOMBR2) NOMBR2, --04 OTROS NOMBRES
 '1' || PGRCODGFA || PGRCODPNA CODAFIR, --05 CODIGO DE AFILIADO
 'INDIVIDUAL' TIPAFI, --06 TIPO AFILIADO
 DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
 '024',
 PNATIPDOC,
 'ABRDOCID'),
 NULL, 'DNI',
 PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
 '024',
 PNATIPDOC,
 'ABRDOCID'))
 TIPDOC, --07 TIPO DOCUMENTO IDENTIDAD
 PNADOCIDE NUMDOC, --08 NUMERO DE DOCUMENTO
 TO_CHAR(PGRFECAFL,'DD/MM/YYYY') FECAFI, --09 FECHA DE INGRESO DEL AFILIADO
 TO_CHAR(PGRFECURE,'DD/MM/YYYY') FECREN, --10 FECHA DE RENOVACION DEL AFILIADO
 TO_CHAR(PGRFECBAJ,'DD/MM/YYYY') FECBAJ, --11 FECHA DE BAJA DEL AFILIADO
 SISOPE.PCK_WS_CONTRATO_SERVICIO.
 FNC_ES_PACIENTE ('1' || PGRCODGFA || PGRCODPNA)
 ESPACI, --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
 TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC, --13 FECHA DE NACIMIENTO DEL AFILIADO
 FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
 EDAAFI, --14 EDAD DEL AFILIADO
 DECODE (PNACODSEX, 'M', 'MASCULINO', 'F', 'FEMENINO')
 SEXAFI, --15 SEXO AFILIADO
 F_PER_TELEFONO ('001001', PGRCODPNA) TELAFI, --16 TELEFONO FIJO AFILIADO
 F_PER_CELULAR ('001001', PGRCODPNA) CELAFI, --17 CELULAR AFILIADO
 PCK_WS_CONTRATO_SERVICIO.FNC_DIRECCION (PGRCODPNA) DIRAFI, --18 DIRECCION AFILIADO
 FNC_OBTEN_UBIGEO (PGRCODPNA, 'DIST') DISAFI, --19 DISTRITO DEL AFILIADO
 ' ' REFAFI, --20 Referencia del afiliado
 F_PER_EMAIL ('001001', PGRCODPNA) EMAAFI, --21 CORREO ELECTRONICO AFILIADO
 PGRCODGFA GRUAFI, --22 GRUPO FAMILIAR DEL AFILIADO
 PCK_AFIL_RENO.
 FNC_DESC_PROGRAMA (PGRCODCIA || PGRCODSUC, PGRCODPRG)
 PROAFI, --23 PROGRAMA DEL AFILIADO
 PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL (PGRCODCIA || PGRCODSUC,
 '056',
 PGRCODCAT,
 'NOMCORTO')
 CATAFI, --24 CATEGOR�A DE AFILIADO
 TO_CHAR(PGRFECCON,'DD/MM/YYYY') PGRFECCON, --25 FECHA DE CONTINUIDAD DEL AFILIADO
 PCK_WS_SITEDS_V10.
 FN_OBT_ESTADO ('1'||PGRCODGFA||PGRCODPNA, 
 PGRFECBAJ, PGRCODSAF)
 ESTAFI, --26 ESTADO AFILIADO
 PCK_AFIL_RENO.
 FNC_VAL_FCHACT_PAC (PGRCODCIA || PGRCODSUC,
 PGRFECGFF,
 PGRCODPNA)
 FECACT, --27 FECHA DE ACTUALIZACI�N DEL PACIENTE
 PCK_WS_SITEDS_V10.
 FN_OBT_DATOS_CONTRATANTE_IND(PGRCODPNA,'5') CONAFI, --28 CONTRATANTE
 PCK_WS_SITEDS_V10.
 FN_OBTIENE_PLAN(PGRCODGFA,PGRCODPNA,'1',NULL) CODPLAN, --29 CONTRATANTE
 PCK_WS_SITEDS.
 FN_OBTIENE_PLAN_NOM(PGRCODGFA,PGRCODPNA,'1',NULL) NOMPLAN --30 CONTRATANTE
 FROM (SELECT 
 PGRCODCIA,PGRCODSUC,PGRCODSAF,PGRCODGFA,PGRCODPNA,
 PGRFECBAJ,PGRFECAFL,PGRFECCON,PGRFECURE,PGRFECGFF,
 PGRCODCAT,PGRCODPRG,PNADESAPA,PNADESAMA,PNANOMBRE,
 PNANOMBR2,PNAFECNAC,PNATIPDOC,PNACODSEX,PNADOCIDE
 FROM PBFPOBGRP PBF, PNADATMAE PNA
 WHERE PBF.PGRCODCIA = PNA.PNACODCIA
 AND PBF.PGRCODSUC = PNA.PNACODSUC
 AND PBF.PGRCODPNA = PNA.PNACODPNA
 AND PNACODCIA = '001'
 AND PNACODSUC = '001'
 AND PNADOCIDE LIKE PV_NUMDOC || '%') PBF
 UNION ALL
 SELECT 
 TRIM (PNADESAPA) APEPAT, --01 APELLIDO PATERNO
 TRIM (PNADESAMA) APEMAT, --02 APELLIDO MATERNO 
 TRIM (PNANOMBRE) NOMBR1, --03 NOMBRE 1
 TRIM (PNANOMBR2) NOMBR2, --04 OTROS NOMBRES
 '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA 
 CODAFIR, --05 CODIGO DE AFILIADO
 'GRUPAL' TIPAFI, --06 TIPO AFILIADO
 DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
 '024',
 PNATIPDOC,
 'ABRDOCID'),
 NULL, 'DNI',
 PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
 '024',
 PNATIPDOC,
 'ABRDOCID'))
 TIPDOC, --07 TIPO DOCUMENTO IDENTIDAD
 PNADOCIDE NUMDOC, --08 NUMERO DE DOCUMENTO
 TO_CHAR(GRPFECING,'DD/MM/YYYY') FECAFI, --09 FECHA DE INGRESO DEL AFILIADO
 NULL FECREN, --10 FECHA DE RENOVACION DEL AFILIADO
 TO_CHAR(GRPFECBAJ,'DD/MM/YYYY') FECBAJ, --11 FECHA DE BAJA DEL AFILIADO
 PCK_WS_CONTRATO_SERVICIO.
 FNC_ES_PACIENTE (
 '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA)
 ESPACI, --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
 TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC, --13 FECHA DE NACIMIENTO DEL AFILIADO
 FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
 EDAAFI, --14 EDAD DEL AFILIADO
 DECODE (PNACODSEX, 'M', 'MASCULINO', 'F', 'FEMENINO')
 SEXAFI, --15 SEXO AFILIADO
 F_PER_TELEFONO ('001001', GRPCODPNA) TELAFI, --16 TELEFONO FIJO AFILIADO
 F_PER_CELULAR ('001001', GRPCODPNA) CELAFI, --17 CELULAR AFILIADO
 PCK_WS_CONTRATO_SERVICIO.FNC_DIRECCION (GRPCODPNA)
 DIRAFI, --18 DIRECCION AFILIADO
 FNC_OBTEN_UBIGEO (GRPCODPNA, 'DIST') DISAFI, --19 DISTRITO DEL AFILIADO
 ' ' REFAFI, --20 Referencia del afiliado
 F_PER_EMAIL ('001001', PNACODPNA) EMAAFI, --21 CORREO ELECTRONICO AFILIADO
 LPAD (GRPCODONC, 8, '0') GRUAFI, --22 GRUPO FAMILIAR DEL AFILIADO
 PCK_AFIL_RENO.
 FNC_DESC_PROGRAMA (
 GRPCODCIA || GRPCODSUC,
 (SELECT COD_PRG
 FROM ASE_EMPRESAPLAN
 WHERE EMPCODONC = GRPEMPGRP AND EST_RGSTR = 'A'))
 PROAFI, --23 PROGRAMA DEL AFILIADO
 PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL (GRPCODCIA || GRPCODSUC,
 '056',
 GRPCATGRP,
 'NOMCORTO')
 CATAFI, --24 CATEGOR�A DE AFILIADO
 TO_CHAR(GRPFECCON,'DD/MM/YYYY') PGRFECCON, --25 FECHA DE CONTINUIDAD DEL AFILIADO
 PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('2'||LPAD(GRPCODONC,8,'0')||GRPCODPNA, 
 GRPFECBAJ, NULL)
 ESTAFI, --26 ESTADO AFILIADO
 PCK_AFIL_RENO.
 FNC_VAL_FCHACT_PAC (GRPCODCIA || GRPCODSUC,
 NULL,
 GRPCODPNA)
 FECACT, --27 FECHA DE ACTUALIZACI�N DEL PACIENTE
 PCK_WS_SITEDS_V10.
 FN_OBT_DATOS_CONTRATANTE_GRP(PNACODPNA,GRPEMPGRP,2) CONAFI, --28 CONTRATANTE 
 PCK_WS_SITEDS_V10.
 FN_OBTIENE_PLAN(LPAD(GRPCODONC,8,0),GRPCODPNA,'2',GRPEMPGRP) CODPLAN, --29 CODIGO PLAN
 PCK_WS_SITEDS.
 FN_OBTIENE_PLAN_NOM(LPAD(GRPCODONC,8,0),GRPCODPNA,'2',GRPEMPGRP) NOMPLAN --30 CONTRATANTE 
 FROM (SELECT 
 GRPCODCIA,GRPCODSUC,GRPCODONC,GRPCODPNA,GRPFECBAJ,
 PNACODPNA,PNAFECNAC,GRPFAMGRP,GRPCATGRP,PNADOCIDE,
 PNATIPDOC,PNADESAPA,PNADESAMA,PNANOMBRE,PNANOMBR2,
 PNACODSEX,GRPFECCON,GRPFECING,GRPEMPGRP
 FROM GRPPOBGRP PBF, PNADATMAE PNA
 WHERE PBF.GRPCODCIA = PNA.PNACODCIA
 AND PBF.GRPCODSUC = PNA.PNACODSUC
 AND PBF.GRPCODPNA = PNA.PNACODPNA
 AND PNACODCIA = '001'
 AND PNACODSUC = '001'
 AND grpcodcia = '001'
 AND grpcodsuc = '001'
 AND PNADOCIDE LIKE PV_NUMDOC || '%') PBF
 ))
 WHERE RN BETWEEN PN_INI AND PN_FIN 
 AND (ESTAFI=PV_VIGREG OR '0'=PV_VIGREG)
 ORDER BY RN;

--PARA BUSQUEDAS DE TIPO 3 CODIGO DE AFILIADO
 ELSIF PV_TIPBUS = '3'
 THEN
 SELECT SUM (CAN)
 INTO PN_VALOR
 FROM (SELECT COUNT (1) CAN
 FROM PBFPOBGRP PBF, PNADATMAE PNA
 WHERE PBF.PGRCODCIA = PNA.PNACODCIA
 AND PBF.PGRCODSUC = PNA.PNACODSUC
 AND PBF.PGRCODPNA = PNA.PNACODPNA
 AND PNACODCIA = '001'
 AND PNACODSUC = '001'
 AND '1' = SUBSTR (PV_CODAFI, 1, 1)
 AND PGRCODGFA = SUBSTR (PV_CODAFI, 2, 8)
 AND PGRCODPNA = SUBSTR (PV_CODAFI, 10, 8)
 AND (PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('1'||PGRCODGFA||PGRCODPNA,PGRFECBAJ, PGRCODSAF)=PV_VIGREG OR '0'=PV_VIGREG)
 UNION
 SELECT COUNT (1) CAN
 FROM GRPPOBGRP PBF, PNADATMAE PNA
 WHERE PBF.GRPCODCIA = PNA.PNACODCIA
 AND PBF.GRPCODSUC = PNA.PNACODSUC
 AND PBF.GRPCODPNA = PNA.PNACODPNA
 AND PNACODCIA = '001'
 AND PNACODSUC = '001'
 AND grpcodcia = '001'
 AND grpcodsuc = '001'
 AND '2' = SUBSTR (PV_CODAFI, 1, 1)
 AND GRPCODONC = SUBSTR (PV_CODAFI, 2, 8)
 AND GRPCODPNA = SUBSTR (PV_CODAFI, 10, 8)
 AND (PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('2'||LPAD(GRPCODONC,8,'0')||GRPCODPNA,GRPFECBAJ, NULL)=PV_VIGREG OR '0'=PV_VIGREG)
 );

 IF PN_VALOR < 1
 THEN
 RAISE DATO_ERR;
 END IF;


 SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
 TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
 INTO PO_IDTRAN, PO_FECTRA
 FROM DUAL;

 PO_CODRES := '0';
 PO_MENRES := 'DATOS CORRECTOS';
 PO_TOTAL := PN_VALOR;


 OPEN RESULTSET FOR
 SELECT * FROM( 
 SELECT 
 ROW_NUMBER() OVER (ORDER BY APEPAT,APEMAT,NOMBR1,NOMBR2) RN,--01 NUMERO DE REGISTRO
 APEPAT, --02 APELLIDO PATERNO
 APEMAT, --03 APELLIDO MATERNO
 NOMBR1, --04 NOMBRE 1
 NOMBR2, --05 OTROS NOMBRES
 CODAFIR, --06 CODIGO DE AFILIADO
 TIPAFI, --07 TIPO AFILIADO
 TIPDOC, --08 TIPO DOCUMENTO IDENTIDAD
 NUMDOC, --09 NUMERO DE DOCUMENTO
 FECAFI, --10 FECHA DE INGRESO DEL AFILIADO
 FECREN, --11 FECHA DE RENOVACION DEL AFILIADO
 FECBAJ, --12 FECHA DE BAJA DEL AFILIADO
 ESPACI, --13 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
 FECNAC, --14 FECHA DE NACIMIENTO DEL AFILIADO
 EDAAFI, --15 EDAD DEL AFILIADO
 SEXAFI, --16 SEXO AFILIADO
 TELAFI, --17 TELEFONO FIJO AFILIADO
 CELAFI, --18 CELULAR AFILIADO
 DIRAFI, --19 DIRECCION AFILIADO
 DISAFI, --20 DISTRITO DEL AFILIADO
 REFAFI, --21 Referencia del afiliado
 EMAAFI, --22 CORREO ELECTRONICO AFILIADO
 GRUAFI, --23 GRUPO FAMILIAR DEL AFILIADO
 PROAFI, --24 PROGRAMA DEL AFILIADO
 CATAFI, --25 CATEGOR�A DE AFILIADO
 PGRFECCON, --26 FECHA DE CONTINUIDAD DEL AFILIADO
 ESTAFI, --27 ESTADO AFILIADO
 FECACT, --28 FECHA DE ACTUALIZACI�N DEL PACIENTE
 CONAFI, --29 CONTRATANTE
 CODPLAN, --30 CODIGO DE PLAN
 NOMPLAN --31 NOMBRE PLAN 
 FROM(
 SELECT
 TRIM (PNADESAPA) APEPAT, --01 APELLIDO PATERNO
 TRIM (PNADESAMA) APEMAT, --02 APELLIDO MATERNO
 TRIM (PNANOMBRE) NOMBR1, --03 NOMBRE 1
 TRIM (PNANOMBR2) NOMBR2, --04 OTROS NOMBRES
 '1' || PGRCODGFA || PGRCODPNA CODAFIR, --05 CODIGO DE AFILIADO
 'INDIVIDUAL' TIPAFI, --06 TIPO AFILIADO
 DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
 '024',
 PNATIPDOC,
 'ABRDOCID'),
 NULL, 'DNI',
 PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
 '024',
 PNATIPDOC,
 'ABRDOCID'))
 TIPDOC, --07 TIPO DOCUMENTO IDENTIDAD
 PNADOCIDE NUMDOC, --08 NUMERO DE DOCUMENTO
 TO_CHAR(PGRFECAFL,'DD/MM/YYYY') FECAFI, --09 FECHA DE INGRESO DEL AFILIADO
 TO_CHAR(PGRFECURE,'DD/MM/YYYY') FECREN, --10 FECHA DE RENOVACION DEL AFILIADO
 TO_CHAR(PGRFECBAJ,'DD/MM/YYYY') FECBAJ, --11 FECHA DE BAJA DEL AFILIADO
 PCK_WS_CONTRATO_SERVICIO.
 FNC_ES_PACIENTE ('1' || PGRCODGFA || PGRCODPNA)
 ESPACI, --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
 TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC, --13 FECHA DE NACIMIENTO DEL AFILIADO
 FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI,                                                --14 EDAD DEL AFILIADO
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                      SEXAFI,                                             --15 SEXO AFILIADO
                   F_PER_TELEFONO ('001001', PGRCODPNA) TELAFI,           --16 TELEFONO FIJO AFILIADO
                   F_PER_CELULAR ('001001', PGRCODPNA) CELAFI,            --17 CELULAR AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.FNC_DIRECCION (PGRCODPNA) DIRAFI, --18 DIRECCION AFILIADO
                   FNC_OBTEN_UBIGEO (PGRCODPNA, 'DIST') DISAFI,           --19 DISTRITO DEL AFILIADO
                   ' ' REFAFI,                                            --20 Referencia del afiliado
                   F_PER_EMAIL ('001001', PGRCODPNA) EMAAFI,              --21 CORREO ELECTRONICO AFILIADO
                   PGRCODGFA GRUAFI,                                      --22 GRUPO FAMILIAR DEL AFILIADO
                   PCK_AFIL_RENO.
                    FNC_DESC_PROGRAMA (PGRCODCIA || PGRCODSUC, PGRCODPRG)
                      PROAFI,                                             --23 PROGRAMA DEL AFILIADO
                   PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL (PGRCODCIA || PGRCODSUC,
                                                     '056',
                                                     PGRCODCAT,
                                                     'NOMCORTO')
                      CATAFI,                                            --24 CATEGOR�A DE AFILIADO
                   TO_CHAR(PGRFECCON,'DD/MM/YYYY') PGRFECCON,                                            --25 FECHA DE CONTINUIDAD DEL AFILIADO
                   PCK_WS_SITEDS_V10.
                    FN_OBT_ESTADO ('1'||PGRCODGFA||PGRCODPNA, 
                    PGRFECBAJ, PGRCODSAF)
                      ESTAFI,                                            --26 ESTADO AFILIADO
                     PCK_AFIL_RENO.
                       FNC_VAL_FCHACT_PAC (PGRCODCIA || PGRCODSUC,
                                           PGRFECGFF,
                                           PGRCODPNA)
                      FECACT,                                            --27 FECHA DE ACTUALIZACI�N DEL PACIENTE
                      PCK_WS_SITEDS_V10.
                      FN_OBT_DATOS_CONTRATANTE_IND(PGRCODPNA,'5') CONAFI,       --28 CONTRATANTE
                      PCK_WS_SITEDS_V10.
                      FN_OBTIENE_PLAN(PGRCODGFA,PGRCODPNA,'1',NULL) CODPLAN,       --29 CONTRATANTE
                      PCK_WS_SITEDS.
                      FN_OBTIENE_PLAN_NOM(PGRCODGFA,PGRCODPNA,'1',NULL) NOMPLAN       --30 CONTRATANTE
              FROM (SELECT                 
                           PGRCODCIA,PGRCODSUC,PGRCODSAF,PGRCODGFA,PGRCODPNA,
                           PGRFECBAJ,PGRFECAFL,PGRFECCON,PGRFECURE,PGRFECGFF,
                           PGRCODCAT,PGRCODPRG,PNADESAPA,PNADESAMA,PNANOMBRE,
                           PNANOMBR2,PNAFECNAC,PNATIPDOC,PNACODSEX,PNADOCIDE
                      FROM PBFPOBGRP PBF, PNADATMAE PNA
                     WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                           AND PBF.PGRCODSUC = PNA.PNACODSUC
                           AND PBF.PGRCODPNA = PNA.PNACODPNA
                           AND PNACODCIA = '001'
                           AND PNACODSUC = '001'
                           AND '1' = SUBSTR (PV_CODAFI, 1, 1)
                           AND PGRCODGFA = SUBSTR (PV_CODAFI, 2, 8)
                           AND PGRCODPNA = SUBSTR (PV_CODAFI, 10, 8)) PBF
            UNION ALL
            SELECT  
                      TRIM (PNADESAPA) APEPAT,                      --01 APELLIDO PATERNO
                      TRIM (PNADESAMA) APEMAT,                      --02 APELLIDO MATERNO    
                      TRIM (PNANOMBRE) NOMBR1,                      --03 NOMBRE 1
                      TRIM (PNANOMBR2) NOMBR2,                      --04 OTROS NOMBRES
                      '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA 
                      CODAFIR,                                      --05 CODIGO DE AFILIADO
                   'GRUPAL' TIPAFI,                                 --06 TIPO AFILIADO
                   DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'),
                           NULL, 'DNI',
                           PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'))
                      TIPDOC,                                      --07 TIPO DOCUMENTO IDENTIDAD
                   PNADOCIDE NUMDOC,                               --08 NUMERO DE DOCUMENTO
                   TO_CHAR(GRPFECING,'DD/MM/YYYY') FECAFI,                               --09 FECHA DE INGRESO DEL AFILIADO
                   NULL FECREN,                                    --10 FECHA DE RENOVACION DEL AFILIADO
                   TO_CHAR(GRPFECBAJ,'DD/MM/YYYY') FECBAJ,                               --11 FECHA DE BAJA DEL AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.
                    FNC_ES_PACIENTE (
                      '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA)
                      ESPACI,                                      --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC,                               --13 FECHA DE NACIMIENTO DEL AFILIADO
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI,                                         --14 EDAD DEL AFILIADO
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                      SEXAFI,                                      --15 SEXO AFILIADO
                   F_PER_TELEFONO ('001001', GRPCODPNA) TELAFI,    --16 TELEFONO FIJO AFILIADO
                   F_PER_CELULAR ('001001', GRPCODPNA) CELAFI,     --17 CELULAR AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.FNC_DIRECCION (GRPCODPNA)
                   DIRAFI,                                         --18 DIRECCION AFILIADO
                   FNC_OBTEN_UBIGEO (GRPCODPNA, 'DIST') DISAFI,    --19 DISTRITO DEL AFILIADO
                   ' ' REFAFI,                                     --20 Referencia del afiliado
                   F_PER_EMAIL ('001001', PNACODPNA) EMAAFI,       --21 CORREO ELECTRONICO AFILIADO
                   LPAD (GRPCODONC, 8, '0') GRUAFI,                --22 GRUPO FAMILIAR DEL AFILIADO
                   PCK_AFIL_RENO.
                    FNC_DESC_PROGRAMA (
                      GRPCODCIA || GRPCODSUC,
                      (SELECT COD_PRG
                         FROM ASE_EMPRESAPLAN
                        WHERE EMPCODONC = GRPEMPGRP AND EST_RGSTR = 'A'))
                      PROAFI,                                      --23 PROGRAMA DEL AFILIADO
                   PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL (GRPCODCIA || GRPCODSUC,
                                                     '056',
                                                     GRPCATGRP,
                                                     'NOMCORTO')
                      CATAFI,                                     --24 CATEGOR�A DE AFILIADO
                   TO_CHAR(GRPFECCON,'DD/MM/YYYY') PGRFECCON,                           --25 FECHA DE CONTINUIDAD DEL AFILIADO
                   PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('2'||LPAD(GRPCODONC,8,'0')||GRPCODPNA, 
                   GRPFECBAJ, NULL)
                      ESTAFI,                                     --26 ESTADO AFILIADO
                      PCK_AFIL_RENO.
                       FNC_VAL_FCHACT_PAC (GRPCODCIA || GRPCODSUC,
                                           NULL,
                                           GRPCODPNA)
                      FECACT,                                               --27 FECHA DE ACTUALIZACI�N DEL PACIENTE
                      PCK_WS_SITEDS_V10.
                      FN_OBT_DATOS_CONTRATANTE_GRP(PNACODPNA,GRPEMPGRP,2) CONAFI,  --28 CONTRATANTE                                     
                      PCK_WS_SITEDS_V10.
                      FN_OBTIENE_PLAN(LPAD(GRPCODONC,8,0),GRPCODPNA,'2',GRPEMPGRP) CODPLAN,  --29 CODIGO PLAN
                      PCK_WS_SITEDS.
                      FN_OBTIENE_PLAN_NOM(LPAD(GRPCODONC,8,0),GRPCODPNA,'2',GRPEMPGRP) NOMPLAN       --30 CONTRATANTE  
              FROM (SELECT                
                          GRPCODCIA,GRPCODSUC,GRPCODONC,GRPCODPNA,GRPFECBAJ,
                          PNACODPNA,PNAFECNAC,GRPFAMGRP,GRPCATGRP,PNADOCIDE,
                          PNATIPDOC,PNADESAPA,PNADESAMA,PNANOMBRE,PNANOMBR2,
                          PNACODSEX,GRPFECCON,GRPFECING,GRPEMPGRP
                      FROM GRPPOBGRP PBF, PNADATMAE PNA
                     WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                           AND PBF.GRPCODSUC = PNA.PNACODSUC
                           AND PBF.GRPCODPNA = PNA.PNACODPNA
                           AND PNACODCIA = '001'
                           AND PNACODSUC = '001'
                           AND GRPCODCIA = '001'
                           AND GRPCODSUC = '001'
                           AND '2' = SUBSTR (PV_CODAFI, 1, 1)
                           AND GRPCODONC = SUBSTR (PV_CODAFI, 2, 8)
                           AND GRPCODPNA = SUBSTR (PV_CODAFI, 10, 8)) PBF
                    ))
                    WHERE RN BETWEEN PN_INI AND PN_FIN  
                    AND (ESTAFI=PV_VIGREG OR '0'=PV_VIGREG)
                    ORDER BY RN;


      ELSE
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTE DATOS';

         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_CONSULTA_AFI',
                           PV_TIPBUS
                        || '-'
                        || PV_CODAFI
                        || '-'
                        || PV_TIPDOC
                        || '-'
                        || PV_NUMDOC
                        || '-'
                        || PV_APEPAT
                        || '-'
                        || PV_APEMAT
                        || '-'
                        || PV_NOMAFI);

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                  --10
                   NULL,                                                  --11
                   NULL,                                                  --12
                   NULL,                                                  --13
                   NULL,                                                  --14
                   NULL,                                                  --15
                   NULL,                                                  --16
                   NULL,                                                  --17
                   NULL,                                                  --18
                   NULL,                                                  --19
                   NULL,                                                  --20
                   NULL,                                                  --21
                   NULL,                                                  --22
                   NULL,                                                  --23
                   NULL,                                                  --24
                   NULL,                                                  --25
                   NULL,                                                  --26
                   NULL,                                                  --27
                   NULL                                                   --28
              FROM DUAL;
      END IF;
   EXCEPTION
      WHEN DATO_ERR
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                  --10
                   NULL,                                                  --11
                   NULL,                                                  --12
                   NULL,                                                  --13
                   NULL,                                                  --14
                   NULL,                                                  --15
                   NULL,                                                  --16
                   NULL,                                                  --17
                   NULL,                                                  --18
                   NULL,                                                  --19
                   NULL,                                                  --20
                   NULL,                                                  --21
                   NULL,                                                  --22
                   NULL,                                                  --23
                   NULL,                                                  --24
                   NULL,                                                  --25
                   NULL,                                                  --26
                   NULL,                                                  --27
                   NULL                                                   --28
              FROM DUAL;

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                  --10
                   NULL,                                                  --11
                   NULL,                                                  --12
                   NULL,                                                  --13
                   NULL,                                                  --14
                   NULL,                                                  --15
                   NULL,                                                  --16
                   NULL,                                                  --17
                   NULL,                                                  --18
                   NULL,                                                  --19
                   NULL,                                                  --20
                   NULL,                                                  --21
                   NULL,                                                  --22
                   NULL,                                                  --23
                   NULL,                                                  --24
                   NULL,                                                  --25
                   NULL,                                                  --26
                   NULL,                                                  --27
                   NULL                                                   --28
              FROM DUAL;

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;
      
      
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;


         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_CONSULTA_AFI',
                           PV_TIPBUS
                        || '-'
                        || PV_CODAFI
                        || '-'
                        || PV_TIPDOC
                        || '-'
                        || PV_NUMDOC
                        || '-'
                        || PV_APEPAT
                        || '-'
                        || PV_APEMAT
                        || '-'
                        || PV_NOMAFI);

         COMMIT;

         OPEN RESULTSET FOR
            SELECT 'A',                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                  --10
                   NULL,                                                  --11
                   NULL,                                                  --12
                   NULL,                                                  --13
                   NULL,                                                  --14
                   NULL,                                                  --15
                   NULL,                                                  --16
                   NULL,                                                  --17
                   NULL,                                                  --18
                   NULL,                                                  --19
                   NULL,                                                  --20
                   NULL,                                                  --21
                   NULL,                                                  --22
                   NULL,                                                  --23
                   NULL,                                                  --24
                   NULL,                                                  --25
                   NULL,                                                  --26
                   NULL,                                                  --27
                   NULL                                                   --28
              FROM DUAL;
              
                --DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                --COMMIT;
   END PRC_CONSULTA_AFI;

   FUNCTION FNC_ES_PACIENTE (PV_CODAFI VARCHAR2)              --CODIGO DE PERSONA
      RETURN VARCHAR2
   IS
      PN_VALOR   VARCHAR2 (10) := NULL;
   BEGIN
--      SELECT 'SI'
--        INTO PN_VALOR
--        FROM SISOPE.PCNDATMAE
--       WHERE     PCNCODCIA = '001'
--             AND PCNCODSUC = '001'
--             AND PCNCODIGO = SUBSTR (PV_CODAFI, 10, 8);

          SELECT 'SI'
            INTO PN_VALOR
            FROM PCNDATMAE PCN,
                 PCNSEGSIN PSEG,
                 PCNSEGSIN PSEG1
           WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                 AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                 AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                 AND PCN.PCNCODCIA =
                        PSEG1.PSSCODCIA
                 AND PCN.PCNCODSUC =
                        PSEG1.PSSCODSUC
                 AND PCN.PCNCODIGO =
                        PSEG1.PSSPCNCOD
                 AND PCN.PCNCODCIA = '001'
                 AND PCN.PCNCODSUC = '001'
                 AND PCN.PCNESTADO = '10'
                 AND PCNCODIGO = SUBSTR (PV_CODAFI, 10, 8)
                 AND TO_NUMBER (
                        NVL (PCN.PCNESTPCN, '0')) <
                        2
                 AND PSEG.PSSTIPATR = 'CON'
                 AND PSEG.PSSFECVIG IS NULL
                 AND PSEG1.PSSTIPATR = 'EST'
                 AND PSEG.PSSVALATR NOT IN
                        ('11', '12')
                 AND PSEG1.PSSFECVIG IS NULL;

      RETURN PN_VALOR;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 'NO';
      WHEN TOO_MANY_ROWS
      THEN
         RETURN 'NO';
   END FNC_ES_PACIENTE;

   FUNCTION FNC_DIRECCION (PV_CODPNA VARCHAR2)                 --CODIGO DE PERSONA
      RETURN VARCHAR2
   IS
      CURSOR C1
      IS
           SELECT AGPVALAGE, AGPFLGDEF, AGPESTREG
             FROM PBFAGEPER
            WHERE     AGPCODCIA = '001'
                  AND AGPCODSUC = '001'
                  AND AGPCODPER = PV_CODPNA
                  AND AGPTIELAG = '001'
         ORDER BY NVL (AGPESTREG, '0') DESC, NVL (AGPFLGDEF, '0') DESC;

      LV_DIRECCION   VARCHAR2 (250) := NULL;
   BEGIN
      LV_DIRECCION := NULL;

      FOR X IN C1
      LOOP
         IF NVL (TRIM (X.AGPFLGDEF), '0') = '1'
            AND NVL (TRIM (X.AGPESTREG), '0') = '1'
         THEN
            LV_DIRECCION := TRIM (X.AGPVALAGE);
            EXIT;
         ELSIF NVL (TRIM (X.AGPFLGDEF), '0') = '0'
               AND NVL (TRIM (X.AGPESTREG), '0') = '1'
         THEN
            LV_DIRECCION := TRIM (X.AGPVALAGE);
            EXIT;
         ELSIF NVL (TRIM (X.AGPFLGDEF), '0') = '1'
               AND NVL (TRIM (X.AGPESTREG), '0') = '0'
         THEN
            LV_DIRECCION := TRIM (X.AGPVALAGE);
            EXIT;
         ELSE
            LV_DIRECCION := TRIM (X.AGPVALAGE);
            EXIT;
         END IF;
      END LOOP;

      RETURN LV_DIRECCION;
   END FNC_DIRECCION;

   PROCEDURE PRC_PLAN_AFI (
                           PV_TIPBUS      IN     VARCHAR2,        --01 TIPO DE BUSQUEDA: 1. C�digo de afiliado/2. Documento de identidad/
                           PV_CODAFI      IN     VARCHAR2,        --02 CODIGO DE AFILIADO
                           PV_TIPDOC      IN     VARCHAR2,        --03 TIPO DE DOCUMENTO
                           PV_NUMDOC      IN     VARCHAR2,        --04 NUMERO DE DOCUMENTO
                           PO_IDTRAN         OUT VARCHAR2, --05 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                           PO_FECTRA         OUT VARCHAR2, --06 FECHA DE LA TRANSACCI�N
                           PO_CODRES         OUT VARCHAR2, --07 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                           PO_MENRES         OUT VARCHAR2, --08 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                           RESULTSET      OUT SYS_REFCURSOR)
   IS
      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;
   BEGIN

   IF  PV_TIPBUS='1' THEN

      IF SUBSTR (PV_CODAFI, 1, 1) = '1'
      THEN
         SELECT COUNT (1)
           INTO PN_VALOR
           FROM PBFPOBGRP
          WHERE     PGRCODCIA = '001'
                AND PGRCODSUC = '001'
                AND PGRCODGFA = SUBSTR (PV_CODAFI, 2, 8)
                AND PGRCODPNA = SUBSTR (PV_CODAFI, 10, 8);

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR
            SELECT PCK_WS_SITEDS_V10.FN_OBTIENE_PLAN (SUBSTR (PV_CODAFI, 2, 8),
                                                      SUBSTR (PV_CODAFI, 10, 8),
                                                      '1',
                                                      NULL)
                      CODPLAN,                             --01 CODIGO DE PLAN
                   (SELECT DES_PLAN
                      FROM ASE_PLAN
                     WHERE CODPLAN = PCK_WS_SITEDS_V10.
                                      FN_OBTIENE_PLAN (
                                        SUBSTR (PV_CODAFI, 2, 8),
                                        SUBSTR (PV_CODAFI, 10, 8),
                                        '1',
                                        NULL))
                      NOMPLAN,                              --02 NOMBRE DE PLAN
                      ' ' PESPLAN,                           --03 PESO PLAN
                      'INDIVIDUAL' TIPAFI,
                      'VIGENTE' ESTPLAN       
              FROM DUAL;
      ELSIF SUBSTR (PV_CODAFI, 1, 1) = '2'
      THEN
         SELECT COUNT (1)
           INTO PN_VALOR
           FROM GRPPOBGRP
          WHERE     GRPCODCIA = '001'
                AND GRPCODSUC = '001'
                AND GRPCODONC = SUBSTR (PV_CODAFI, 2, 8)
                AND GRPCODPNA = SUBSTR (PV_CODAFI, 10, 8);

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR
            SELECT PCK_WS_SITEDS_V10.FN_OBTIENE_PLAN (SUBSTR (PV_CODAFI, 2, 8),
                                                      SUBSTR (PV_CODAFI, 10, 8),
                                                      '2',
                                                      GRPEMPGRP)
                      CODPLAN,                             --01 CODIGO DE PLAN
                   (SELECT DES_PLAN
                      FROM ASE_PLAN
                     WHERE CODPLAN = PCK_WS_SITEDS_V10.
                                      FN_OBTIENE_PLAN (
                                        SUBSTR (PV_CODAFI, 2, 8),
                                        SUBSTR (PV_CODAFI, 10, 8),
                                        '2',
                                        GRPEMPGRP))
                      NOMPLAN,                              --02 NOMBRE DE PLAN
                      ' ' PESPLAN,                           --03 PESO PLAN
                      'GRUPAL' TIPAFI,
                      'VIGENTE' ESTPLAN
              FROM GRPPOBGRP
             WHERE     GRPCODCIA = '001'
                   AND GRPCODSUC = '001'
                   AND GRPCODONC = SUBSTR (PV_CODAFI, 2, 8)
                   AND GRPCODPNA = SUBSTR (PV_CODAFI, 10, 8);
      ELSE

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTE DATOS';

         OPEN RESULTSET FOR SELECT NULL, NULL, NULL FROM DUAL;
      END IF;
      
     ELSIF PV_TIPBUS='2' THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
            
            OPEN RESULTSET FOR
                     SELECT PCK_WS_SITEDS_V10.FN_OBTIENE_PLAN (PGRCODGFA, PGRCODPNA,'1', NULL)
                            CODPLAN,                          --01 CODIGO DE PLAN
                    (SELECT DES_PLAN
                       FROM ASE_PLAN
                      WHERE CODPLAN = PCK_WS_SITEDS_V10.FN_OBTIENE_PLAN (PGRCODGFA,PGRCODPNA,'1',NULL))
                            NOMPLAN,                          --02 NOMBRE DE PLAN
                        ' ' PESPLAN,                           --03 PESO PLAN  
                      'INDIVIDUAL' TIPAFI,
                      'VIGENTE' ESTPLAN
                       FROM PBFPOBGRP PBF, PNADATMAE PNA
                      WHERE PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND PNADOCIDE = PV_NUMDOC
                        AND PCK_WS_SITEDS_V10.FN_OBTIENE_PLAN (PGRCODGFA, PGRCODPNA,'1', NULL) IS NOT NULL
                      UNION
                     SELECT PCK_WS_SITEDS_V10.FN_OBTIENE_PLAN (LPAD(GRPCODONC,8,'0'), GRPCODPNA,'2', GRPEMPGRP)
                            CODPLAN,                          --01 CODIGO DE PLAN
                    (SELECT DES_PLAN
                       FROM ASE_PLAN
                      WHERE CODPLAN = PCK_WS_SITEDS_V10.FN_OBTIENE_PLAN (LPAD(GRPCODONC,8,'0'),GRPCODPNA,'2',GRPEMPGRP))
                            NOMPLAN,                          --02 NOMBRE DE PLAN
                        ' ' PESPLAN,                           --03 PESO PLAN                   
                      'GRUPAL' TIPAFI,
                      'VIGENTE' ESTPLAN
                   FROM GRPPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND grpcodcia = '001'
                        AND grpcodsuc = '001'
                        AND PNADOCIDE = PV_NUMDOC
                        AND PCK_WS_SITEDS_V10.FN_OBTIENE_PLAN (LPAD(GRPCODONC,8,'0'), GRPCODPNA,'2', GRPEMPGRP) IS NOT NULL;



     ELSE

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTE DATOS';

         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (PO_IDTRAN,
                      PO_FECTRA,
                      PV_ERROR,
                      'PRC_PLAN_AFI',
                      PV_CODAFI);

         COMMIT;

         OPEN RESULTSET FOR SELECT NULL, NULL, NULL FROM DUAL;

     END IF; 

   EXCEPTION
      WHEN DATO_ERR
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTE DATOS';

         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (PO_IDTRAN,
                      PO_FECTRA,
                      PV_ERROR,
                      'PRC_PLAN_AFI',
                      PV_CODAFI);

         COMMIT;

         OPEN RESULTSET FOR SELECT NULL, NULL, NULL FROM DUAL;

      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTE DATOS-'||PV_ERROR;


         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (PO_IDTRAN,
                      PO_FECTRA,
                      PV_ERROR,
                      'PRC_PLAN_AFI',
                      PV_CODAFI);

         COMMIT;

         OPEN RESULTSET FOR SELECT NULL, NULL,NULL FROM DUAL;

      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTE DATOS-'||PV_ERROR;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (PO_IDTRAN,
                      PO_FECTRA,
                      PV_ERROR,
                      'PRC_PLAN_AFI',
                      PV_CODAFI);

         COMMIT;

         OPEN RESULTSET FOR SELECT NULL, NULL, NULL FROM DUAL;
   END PRC_PLAN_AFI;

--03 CUOTAS IMPAGAS

--04 NOMBRES AFILIADO
   PROCEDURE PRC_NOMBRES_AFI ( 
                               PN_INI         IN  NUMBER,   --01 REGISTRO INICIO
                               PN_FIN         IN  NUMBER,   --02 REGISTRO FIN 
                               PV_CODAFI      IN  VARCHAR2, --03 CODIGO DE AFILIADO
                               PO_TOTAL       OUT NUMBER,   --04 TOTAL REGISTROS
                               PO_IDTRAN      OUT VARCHAR2, --05 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA      OUT VARCHAR2,     --06 FECHA DE LA TRANSACCI�N
                               PO_CODRES      OUT VARCHAR2, --07 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES      OUT VARCHAR2, --08 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET   OUT SYS_REFCURSOR)
   IS
      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER          := 0;
      PN_CONTAR  NUMBER          := 0;
      PN_RESER1  VARCHAR2(32767) := NULL;
      PN_RESER2  VARCHAR2(32767) := NULL;
      PV_USR     VARCHAR2(20)    := NULL;
      DATO_ERR   EXCEPTION;
    
    BEGIN


       SELECT LENGTH(PV_CODAFI) - LENGTH(REPLACE(PV_CODAFI,'|')) 
         INTO PN_CONTAR 
         FROM DUAL;
         
         PN_RESER1:=PV_CODAFI;
         
         PV_USR:=TRIM (USERENV ('SESSIONID'));
       
       FOR N  IN 1 .. PN_CONTAR LOOP

            PN_RESER2:=SISOPE.PCK_WS_CONTRATO_SERVICIO.FNC_LINEA (PN_RESER1, '|');

           INSERT INTO SISOPE.TMP_CODAFI
           VALUES(SUBSTR(PN_RESER1,1,17),PV_USR);

       END LOOP;
        
       COMMIT;
            
         SELECT SUM (CAN)
           INTO PN_VALOR
           FROM (SELECT COUNT (1) CAN
                   FROM PBFPOBGRP PBF, PNADATMAE PNA, TMP_CODAFI AFI
                  WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND '1'= SUBSTR(AFI.CODAFI,1,1)
                        AND PBF.PGRCODGFA = SUBSTR(AFI.CODAFI,2,8)
                        AND PBF.PGRCODPNA = SUBSTR(AFI.CODAFI,10,8)
                        AND CODUSR=PV_USR
                 UNION
                 SELECT COUNT (1) CAN
                   FROM GRPPOBGRP PBF, PNADATMAE PNA, TMP_CODAFI AFI
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND grpcodcia = '001'
                        AND grpcodsuc = '001'
                        AND '2'= SUBSTR(AFI.CODAFI,1,1)
                        AND LPAD(GRPCODONC,8,'0') = SUBSTR(AFI.CODAFI,2,8)
                        AND GRPCODPNA = SUBSTR(AFI.CODAFI,10,8)
                        AND CODUSR=PV_USR
                        );

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;


         OPEN RESULTSET FOR
            SELECT    
                   '1' || PGRCODGFA || PGRCODPNA CODAFIR, --01 CODIGO DE AFILIADO            
                   TRIM (PNADESAPA) PV_APEPAT,               --02 APELLIDO PATERNO
                   TRIM (PNADESAMA) PV_APEMAT,               --03 APELLIDO MATERNO
                   TRIM (PNANOMBRE) NOMBR1,               --04 PRIMER NOMBRE
                   TRIM (PNANOMBR2) NOMBR2,               --05 OTROS NOMBRES
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI,                                 --06 EDAD DEL AFILIADO
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                   SEXAFI,                                            --07 SEXO AFILIADO
                      PCK_WS_SITEDS_V10.
                      FN_OBTIENE_PLAN(PGRCODGFA,PGRCODPNA,'1',NULL) CODPLAN,       --29 CONTRATANTE
                      PCK_WS_SITEDS.
                      FN_OBTIENE_PLAN_NOM(PGRCODGFA,PGRCODPNA,'1',NULL) NOMPLAN,       --30 CONTRATANTE
                                         PCK_WS_SITEDS_V10.
                    FN_OBT_ESTADO ('1'||PGRCODGFA||PGRCODPNA, 
                    PGRFECBAJ, PGRCODSAF)
                      ESTAFI,                                            --26 ESTADO AFILIADO
                      PGRCODPNA CODPAC,
                      TO_CHAR(PGRFECAFL,'DD/MM/RRRR') FECAFL,
                      'INDIVIDUAL' TIPAFI,
                      PNADOCIDE DOCIDE
              FROM (SELECT                 
                          *
                      FROM PBFPOBGRP PBF, PNADATMAE PNA, TMP_CODAFI AFI
                     WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                           AND PBF.PGRCODSUC = PNA.PNACODSUC
                           AND PBF.PGRCODPNA = PNA.PNACODPNA
                           AND PNACODCIA = '001'
                           AND PNACODSUC = '001'
                           AND '1'= SUBSTR(AFI.CODAFI,1,1)
                           AND PBF.PGRCODGFA = SUBSTR(AFI.CODAFI,2,8)
                           AND PBF.PGRCODPNA = SUBSTR(AFI.CODAFI,10,8)
                           AND CODUSR=PV_USR
                           ) PBF
            UNION ALL
            SELECT    
                   '2'||LPAD(GRPCODONC,8,'0')||GRPCODPNA 
                   CODAFIR,                               --01 CODIGO DE AFILIADO            
                   TRIM (PNADESAPA) APEPAT,               --02 APELLIDO PATERNO
                   TRIM (PNADESAMA) APEMAT,               --03 APELLIDO MATERNO
                   TRIM (PNANOMBRE) NOMBR1,               --04 PRIMER NOMBRE
                   TRIM (PNANOMBR2) NOMBR2,               --05 OTROS NOMBRES
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI,                                --06 EDAD DEL AFILIADO
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                   SEXAFI,                                 --07 SEXO AFILIADO
                      PCK_WS_SITEDS_V10.
                      FN_OBTIENE_PLAN(LPAD(GRPCODONC,8,0),GRPCODPNA,'2',GRPEMPGRP) CODPLAN,  --29 CODIGO PLAN
                      PCK_WS_SITEDS.
                      FN_OBTIENE_PLAN_NOM(LPAD(GRPCODONC,8,0),GRPCODPNA,'2',GRPEMPGRP) NOMPLAN,       --30 CONTRATANTE 
                      PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('2'||LPAD(GRPCODONC,8,'0')||GRPCODPNA, 
                    GRPFECBAJ, NULL)
                      ESTAFI,                                            --26 ESTADO AFILIADO  
                      GRPCODPNA CODPAC,
                      TO_CHAR(GRPFECING,'DD/MM/RRRR') FECAFL,
                      'GRUPAL' TIPAFI,
                      PNADOCIDE DOCIDE
              FROM (SELECT
                          *
                      FROM GRPPOBGRP PBF, PNADATMAE PNA, TMP_CODAFI AFI
                     WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                           AND PBF.GRPCODSUC = PNA.PNACODSUC
                           AND PBF.GRPCODPNA = PNA.PNACODPNA
                           AND PNACODCIA = '001'
                           AND PNACODSUC = '001'
                           AND GRPCODCIA = '001'
                           AND GRPCODSUC = '001'
                           AND '2'= SUBSTR(AFI.CODAFI,1,1)
                           AND LPAD(GRPCODONC,8,'0') = SUBSTR(AFI.CODAFI,2,8)
                           AND GRPCODPNA = SUBSTR(AFI.CODAFI,10,8)
                           AND CODUSR=PV_USR
                           ) PBF;

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_NOMBRES_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL                                                    --5
                   FROM DUAL;

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_NOMBRES_AFI',
                        PV_CODAFI
                        );

         COMMIT;


         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL                                                    --5
                   FROM DUAL;
                   
                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;


      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_NOMBRES_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL                                                    --5
                   FROM DUAL;
   
                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;


    END PRC_NOMBRES_AFI;
                               
--05 CONTACTO AFILIADO
   PROCEDURE PRC_CONTACTO_AFI( PV_CODAFI      IN  VARCHAR2, --01 CODIGO DE AFILIADO
                               PO_IDTRAN      OUT VARCHAR2, --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA      OUT VARCHAR2,     --03 FECHA DE LA TRANSACCI�N
                               PO_CODRES      OUT VARCHAR2, --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES      OUT VARCHAR2, --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET   OUT SYS_REFCURSOR)
                               IS

      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;
 

   BEGIN

         SELECT SUM (CAN)
           INTO PN_VALOR
           FROM (SELECT COUNT (1) CAN
                   FROM PBFPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND '1' = SUBSTR (PV_CODAFI, 1, 1)
                        AND PGRCODGFA = SUBSTR (PV_CODAFI, 2, 8)
                        AND PGRCODPNA = SUBSTR (PV_CODAFI, 10, 8)
                 UNION
                 SELECT COUNT (1) CAN
                   FROM GRPPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND grpcodcia = '001'
                        AND grpcodsuc = '001'
                        AND '2' = SUBSTR (PV_CODAFI, 1, 1)
                        AND GRPCODONC = SUBSTR (PV_CODAFI, 2, 8)
                        AND GRPCODPNA = SUBSTR (PV_CODAFI, 10, 8));

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR
            SELECT    
                    TRIM (PNADESAPA) APEPAT,                                --01 APELLIDO PATERNO
                    TRIM (PNADESAMA) APEMAT,                                --02 APELLIDO MATERNO
                    TRIM (PNANOMBRE) NOMBR1,                                --03 PRIMER NOMBRE
                    TRIM (PNANOMBR2) NOMBR2,                                --04 OTROS NOMBRES
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) EDAAFI, --05 EDAD DEL AFILIADO
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                      SEXAFI,                                               --06 SEXO AFILIADO
                   F_PER_TELEFONO ('001001', PGRCODPNA) TELAFI,             --07 TELEFONO FIJO AFILIADO
                   F_PER_CELULAR ('001001', PgrCodPna) CELAFI,              --08 CELULAR AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.FNC_DIRECCION (PGRCODPNA)
                   DIRAFI,                                                  --09 DIRECCION AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.FNC_UBIGEO(PGRCODPNA) UBIGEO,--10 UBIGEO
                   FNC_OBTEN_UBIGEO (PGRCODPNA, 'DIST') DISAFI,             --11 DISTRITO DEL AFILIADO
                   FNC_OBTEN_UBIGEO (PGRCODPNA, 'PROV') PROAFI,             --12 PROVINICA DEL AFILIADO
                   FNC_OBTEN_UBIGEO (PGRCODPNA, 'DPTO') DEPAFI,             --13 DEPARTAMENTO DEL AFILIADO
                   ' ' REFAFI,                                              --14 Referencia del afiliado
                   F_PER_EMAIL ('001001', PGRCODPNA) EMAAFI                 --15 CORREO ELECTRONICO AFILIADO
              FROM (SELECT   
                           PGRCODCIA,PGRCODSUC,PGRCODSAF,PGRCODGFA,PGRCODPNA,
                           PGRFECBAJ,PGRFECAFL,PGRFECCON,PGRFECURE,PGRFECGFF,
                           PGRCODCAT,PGRCODPRG,PNADESAPA,PNADESAMA,PNANOMBRE,
                           PNANOMBR2,PNAFECNAC,PNATIPDOC,PNACODSEX,PNADOCIDE
                      FROM PBFPOBGRP PBF, PNADATMAE PNA
                     WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                           AND PBF.PGRCODSUC = PNA.PNACODSUC
                           AND PBF.PGRCODPNA = PNA.PNACODPNA
                           AND PNACODCIA = '001'
                           AND PNACODSUC = '001'
                           AND '1' = SUBSTR (PV_CODAFI, 1, 1)
                           AND PGRCODGFA = SUBSTR (PV_CODAFI, 2, 8)
                           AND PGRCODPNA = SUBSTR (PV_CODAFI, 10, 8)) PBF
            UNION ALL
            SELECT 
                    TRIM (PNADESAPA) APEPAT,                                --01 APELLIDO PATERNO
                    TRIM (PNADESAMA) APEMAT,                                --02 APELLIDO MATERNO
                    TRIM (PNANOMBRE) NOMBR1,                                --03 PRIMER NOMBRE
                    TRIM (PNANOMBR2) NOMBR2,                                --04 OTROS NOMBRES
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) EDAAFI, --05 EDAD DEL AFILIADO
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                      SEXAFI,                                               --06 SEXO AFILIADO
                   F_PER_TELEFONO ('001001', GRPCODPNA) TELAFI,             --07 TELEFONO FIJO AFILIADO
                   F_PER_CELULAR ('001001', GRPCODPNA) CELAFI,              --08 CELULAR AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.FNC_DIRECCION (GRPCODPNA)
                      DIRAFI,                                               --09 DIRECCION AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.FNC_UBIGEO(GRPCODPNA) UBIGEO,--10 UBIGEO
                   FNC_OBTEN_UBIGEO (GRPCODPNA, 'DIST') DISAFI,             --11 DISTRITO DEL AFILIADO
                   FNC_OBTEN_UBIGEO (GRPCODPNA, 'PROV') PROAFI,             --12 PROVINICA DEL AFILIADO
                   FNC_OBTEN_UBIGEO (GRPCODPNA, 'DPTO') DEPAFI,             --13 DEPARTAMENTO DEL AFILIADO
                   ' ' REFAFI,                                              --14 Referencia del afiliado
                   F_PER_EMAIL ('001001', PNACODPNA) EMAAFI                 --15 CORREO ELECTRONICO AFILIADO
              FROM (SELECT     
                          GRPCODCIA,GRPCODSUC,GRPCODONC,GRPCODPNA,GRPFECBAJ,
                          PNACODPNA,PNAFECNAC,GRPFAMGRP,GRPCATGRP,PNADOCIDE,
                          PNATIPDOC,PNADESAPA,PNADESAMA,PNANOMBRE,PNANOMBR2,
                          PNACODSEX,GRPFECCON,GRPFECING,GRPEMPGRP
                      FROM GRPPOBGRP PBF, PNADATMAE PNA
                     WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                           AND PBF.GRPCODSUC = PNA.PNACODSUC
                           AND PBF.GRPCODPNA = PNA.PNACODPNA
                           AND PNACODCIA = '001'
                           AND PNACODSUC = '001'
                           AND GRPCODCIA = '001'
                           AND GRPCODSUC = '001'
                           AND '2' = SUBSTR (PV_CODAFI, 1, 1)
                           AND GRPCODONC = SUBSTR (PV_CODAFI, 2, 8)
                           AND GRPCODPNA = SUBSTR (PV_CODAFI, 10, 8)) PBF;

    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_CONTACTO_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                   --10
                   NULL,                                                   --11
                   NULL,                                                   --12
                   NULL,                                                   --13
                   NULL                                                    --14
                   FROM DUAL;

      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_CONTACTO_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                   --10
                   NULL,                                                   --11
                   NULL,                                                   --12
                   NULL,                                                   --13
                   NULL                                                    --14
                   FROM DUAL;

                   
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_CONTACTO_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                   --10
                   NULL,                                                   --11
                   NULL,                                                   --12
                   NULL,                                                   --13
                   NULL                                                    --14
                   FROM DUAL;


    END PRC_CONTACTO_AFI;    

--06 PACIENTE AFILIADO
   PROCEDURE PRC_PACIENTE_AFI (
                               PN_INI         IN     NUMBER,   --01 REGISTRO INICIO
                               PN_FIN         IN     NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS      IN     VARCHAR2, --03 TIPO DE BUSQUEDA: 1. Por apellidos y nombres/2. Documento de identidad/3. C�digo de afiliado
                               PV_CODAFI      IN     VARCHAR2, --04 CODIGO DE AFILIADO
                               PV_TIPDOC      IN     VARCHAR2, --05 TIPO DE DOCUMENTO
                               PV_NUMDOC      IN     VARCHAR2, --06 NUMERO DE DOCUMENTO
                               PV_APEPAT      IN     VARCHAR2, --07 APELLIDO PATERNO
                               PV_APEMAT      IN     VARCHAR2, --08 APELLIDO MATERNO
                               PV_NOMAFI      IN     VARCHAR2, --09 NOMBRE DE AFILIADO
                               PO_TOTAL          OUT NUMBER,   --10 TOTAL REGISTROS
                               PO_IDTRAN         OUT VARCHAR2, --11 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA         OUT VARCHAR2,     --12 FECHA DE LA TRANSACCI�N
                               PO_CODRES         OUT VARCHAR2, --13 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES         OUT VARCHAR2, --14 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET      OUT SYS_REFCURSOR)
   IS
      PV_ERROR   VARCHAR2 (4000) := NULL;
      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;
   BEGIN

--PARA BUSQUEDAS DE TIPO 1 POR NOMBRES
      IF PV_TIPBUS = '1'
      THEN

         SELECT SUM (CAN)
           INTO PN_VALOR
           FROM(
         SELECT COUNT(1) CAN
           FROM (SELECT *
                   FROM PBFPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND (PNADESAPA LIKE PV_APEPAT || '%'
                             AND NVL (PNADESAMA, ' ') LIKE PV_APEMAT || '%'
                             AND PNANOMBRE || '%' || PNANOMBR2 LIKE
                                    PV_NOMAFI || '%')) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
              WHERE A.PGRCODCIA = B.PCNCODCIA
                AND A.PGRCODSUC = B.PCNCODSUC
                AND A.PGRCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
        UNION   
       SELECT COUNT(1) CAN
           FROM (SELECT *
                   FROM GRPPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND (PNADESAPA LIKE PV_APEPAT || '%'
                        AND NVL (PNADESAMA, ' ') LIKE PV_APEMAT || '%'
                        AND PNANOMBRE || '%' || PNANOMBR2 LIKE
                               PV_NOMAFI || '%')) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
          WHERE     A.GRPCODCIA = B.PCNCODCIA
                AND A.GRPCODSUC = B.PCNCODSUC
                AND A.GRPCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
                );
                

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

         OPEN RESULTSET FOR
            SELECT * FROM(            
            SELECT             
            ROW_NUMBER() OVER (ORDER BY APEPAT,APEMAT,NOMBR1,NOMBR2) RN,--01 NUMERO DE REGISTRO
                   APEPAT,                                              --02 APELLIDO PATERNO
                   APEMAT,                                              --03 APELLIDO MATERNO
                   NOMBR1,                                              --04 NOMBRE 1
                   NOMBR2,                                              --05 OTROS NOMBRES
                   CODAFIR,                                             --06 CODIGO DE AFILIADO
                   TIPAFI,                                              --07 TIPO AFILIADO
                   TIPDOC,                                              --08 TIPO DOCUMENTO IDENTIDAD
                   NUMDOC,                                              --09 NUMERO DE DOCUMENTO
                   FECAFI,                                              --10 FECHA DE INGRESO DEL AFILIADO
                   FECREN,                                              --11 FECHA DE RENOVACION DEL AFILIADO
                   FECBAJ,                                              --12 FECHA DE BAJA DEL AFILIADO
                   ESPACI,                                              --13 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   FECNAC,                                              --14 FECHA DE NACIMIENTO DEL AFILIADO
                   EDAAFI,                                              --15 EDAD DEL AFILIADO
                   CODPAC,                                              --16 CODIGO PACIENTE
                   SEXAFI,                                                --17 SEXO
                   ESTAFI
            FROM(
            SELECT
                   TRIM (PNADESAPA) APEPAT,                               --01 APELLIDO PATERNO
                   TRIM (PNADESAMA) APEMAT,                               --02 APELLIDO MATERNO
                   TRIM (PNANOMBRE) NOMBR1,                               --03 NOMBRE 1
                   TRIM (PNANOMBR2) NOMBR2,                               --04 OTROS NOMBRES
                   '1' || PGRCODGFA || PGRCODPNA CODAFIR,                 --05 CODIGO DE AFILIADO
                   'INDIVIDUAL' TIPAFI,                                   --06 TIPO AFILIADO
                   DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'),
                           NULL, 'DNI',
                           PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'))
                   TIPDOC,                                                --07 TIPO DOCUMENTO IDENTIDAD
                   PNADOCIDE NUMDOC,                                      --08 NUMERO DE DOCUMENTO
                   TO_CHAR(PGRFECAFL,'DD/MM/YYYY') FECAFI,                                      --09 FECHA DE INGRESO DEL AFILIADO
                   TO_CHAR(PGRFECURE,'DD/MM/YYYY') FECREN,                                      --10 FECHA DE RENOVACION DEL AFILIADO
                   TO_CHAR(PGRFECBAJ,'DD/MM/YYYY') FECBAJ,                                      --11 FECHA DE BAJA DEL AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.
                    FNC_ES_PACIENTE ('1' || PGRCODGFA || PGRCODPNA)
                      ESPACI,                                             --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC,                                      --13 FECHA DE NACIMIENTO DEL AFILIADO
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI,                                                 --14 EDAD DEL AFILIADO
                   PNACODPNA CODPAC,                               --15 CODIGO PACIENTE
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                   SEXAFI,                                          --16 SEXO
                    PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('1'||PGRCODGFA||PGRCODPNA, 
                    PGRFECBAJ, PGRCODSAF)
                      ESTAFI                                            --17 ESTADO AFILIADO                    
              FROM (SELECT                 
                           PGRCODCIA,PGRCODSUC,PGRCODSAF,PGRCODGFA,PGRCODPNA,
                           PGRFECBAJ,PGRFECAFL,PGRFECCON,PGRFECURE,PGRFECGFF,
                           PGRCODCAT,PGRCODPRG,PNADESAPA,PNADESAMA,PNANOMBRE,
                           PNANOMBR2,PNAFECNAC,PNATIPDOC,PNACODSEX,PNADOCIDE,
                           PNACODPNA
                   FROM PBFPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND (PNADESAPA LIKE PV_APEPAT || '%'
                             AND NVL (PNADESAMA, ' ') LIKE PV_APEMAT || '%'
                             AND PNANOMBRE || '%' || PNANOMBR2 LIKE
                                    PV_NOMAFI || '%')) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
              WHERE A.PGRCODCIA = B.PCNCODCIA
                AND A.PGRCODSUC = B.PCNCODSUC
                AND A.PGRCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
            UNION ALL
            SELECT    
                      TRIM (PNADESAPA) APEPAT,                      --01 APELLIDO PATERNO
                      TRIM (PNADESAMA) APEMAT,                      --02 APELLIDO MATERNO    
                      TRIM (PNANOMBRE) NOMBR1,                      --03 NOMBRE 1
                      TRIM (PNANOMBR2) NOMBR2,                      --04 OTROS NOMBRES
                      '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA 
                      CODAFIR,                                      --05 CODIGO DE AFILIADO
                   'GRUPAL' TIPAFI,                                 --06 TIPO AFILIADO
                   DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'),
                           NULL, 'DNI',
                           PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'))
                      TIPDOC,                                      --07 TIPO DOCUMENTO IDENTIDAD
                   PNADOCIDE NUMDOC,                               --08 NUMERO DE DOCUMENTO
                   TO_CHAR(GRPFECING,'DD/MM/YYYY') FECAFI,                               --09 FECHA DE INGRESO DEL AFILIADO
                   NULL FECREN,                                    --10 FECHA DE RENOVACION DEL AFILIADO
                   TO_CHAR(GRPFECBAJ,'DD/MM/YYYY') FECBAJ,                               --11 FECHA DE BAJA DEL AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.
                    FNC_ES_PACIENTE (
                      '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA)
                      ESPACI,                                      --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC,                               --13 FECHA DE NACIMIENTO DEL AFILIADO
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI,                                         --14 EDAD DEL AFILIADO
                   PNACODPNA CODPAC,                               --15 CODIGO PACIENTE
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                   SEXAFI,                                          --16 SEXO  
                   PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('2'||LPAD(GRPCODONC,8,'0')||GRPCODPNA, 
                   GRPFECBAJ, NULL)
                      ESTAFI                                     --17 ESTADO AFILIADO                   
              FROM (SELECT                
                          GRPCODCIA,GRPCODSUC,GRPCODONC,GRPCODPNA,GRPFECBAJ,
                          PNACODPNA,PNAFECNAC,GRPFAMGRP,GRPCATGRP,PNADOCIDE,
                          PNATIPDOC,PNADESAPA,PNADESAMA,PNANOMBRE,PNANOMBR2,
                          PNACODSEX,GRPFECCON,GRPFECING,GRPEMPGRP
                   FROM GRPPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND (PNADESAPA LIKE PV_APEPAT || '%'
                        AND NVL (PNADESAMA, ' ') LIKE PV_APEMAT || '%'
                        AND PNANOMBRE || '%' || PNANOMBR2 LIKE
                               PV_NOMAFI || '%')) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
          WHERE     A.GRPCODCIA = B.PCNCODCIA
                AND A.GRPCODSUC = B.PCNCODSUC
                AND A.GRPCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
                    ))
                    WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;
                    
--PARA BUSQUEDAS DE TIPO 2 NUMERO DE DOCUMENTO        
      ELSIF PV_TIPBUS = '2'
      THEN

         SELECT SUM (CAN)
           INTO PN_VALOR
           FROM(
         SELECT COUNT(1) CAN
           FROM (SELECT *
                   FROM PBFPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND PNADOCIDE LIKE PV_NUMDOC || '%'
                                    ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
              WHERE A.PGRCODCIA = B.PCNCODCIA
                AND A.PGRCODSUC = B.PCNCODSUC
                AND A.PGRCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
        UNION   
       SELECT COUNT(1) CAN
           FROM (SELECT *
                   FROM GRPPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND PNADOCIDE LIKE PV_NUMDOC || '%'
                               ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
          WHERE     A.GRPCODCIA = B.PCNCODCIA
                AND A.GRPCODSUC = B.PCNCODSUC
                AND A.GRPCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
                );
                

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

         OPEN RESULTSET FOR
            SELECT * FROM(            
            SELECT             
            ROW_NUMBER() OVER (ORDER BY APEPAT,APEMAT,NOMBR1,NOMBR2) RN,--01 NUMERO DE REGISTRO
                   APEPAT,                                              --02 APELLIDO PATERNO
                   APEMAT,                                              --03 APELLIDO MATERNO
                   NOMBR1,                                              --04 NOMBRE 1
                   NOMBR2,                                              --05 OTROS NOMBRES
                   CODAFIR,                                             --06 CODIGO DE AFILIADO
                   TIPAFI,                                              --07 TIPO AFILIADO
                   TIPDOC,                                              --08 TIPO DOCUMENTO IDENTIDAD
                   NUMDOC,                                              --09 NUMERO DE DOCUMENTO
                   FECAFI,                                              --10 FECHA DE INGRESO DEL AFILIADO
                   FECREN,                                              --11 FECHA DE RENOVACION DEL AFILIADO
                   FECBAJ,                                              --12 FECHA DE BAJA DEL AFILIADO
                   ESPACI,                                              --13 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   FECNAC,                                              --14 FECHA DE NACIMIENTO DEL AFILIADO
                   EDAAFI,                                              --15 EDAD DEL AFILIADO
                   CODPAC,                                              --16 CODIGO PACIENTE
                   SEXAFI,                                               --17 SEXO AFILIADO
                   ESTAFI                                               --18 ESTADO AFILIADO
            FROM(
            SELECT
                   TRIM (PNADESAPA) APEPAT,                               --01 APELLIDO PATERNO
                   TRIM (PNADESAMA) APEMAT,                               --02 APELLIDO MATERNO
                   TRIM (PNANOMBRE) NOMBR1,                               --03 NOMBRE 1
                   TRIM (PNANOMBR2) NOMBR2,                               --04 OTROS NOMBRES
                   '1' || PGRCODGFA || PGRCODPNA CODAFIR,                 --05 CODIGO DE AFILIADO
                   'INDIVIDUAL' TIPAFI,                                   --06 TIPO AFILIADO
                   DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'),
                           NULL, 'DNI',
                           PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'))
                   TIPDOC,                                                --07 TIPO DOCUMENTO IDENTIDAD
                   PNADOCIDE NUMDOC,                                      --08 NUMERO DE DOCUMENTO
                   TO_CHAR(PGRFECAFL,'DD/MM/YYYY') FECAFI,                                      --09 FECHA DE INGRESO DEL AFILIADO
                   TO_CHAR(PGRFECURE,'DD/MM/YYYY') FECREN,                                      --10 FECHA DE RENOVACION DEL AFILIADO
                   TO_CHAR(PGRFECBAJ,'DD/MM/YYYY') FECBAJ,                                      --11 FECHA DE BAJA DEL AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.
                    FNC_ES_PACIENTE ('1' || PGRCODGFA || PGRCODPNA)
                      ESPACI,                                             --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC,                                      --13 FECHA DE NACIMIENTO DEL AFILIADO
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI,                                                 --14 EDAD DEL AFILIADO
                   PNACODPNA CODPAC,                               --15 CODIGO PACIENTE
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                   SEXAFI,                                          --16 SEXO
                   PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('1'||PGRCODGFA||PGRCODPNA, 
                    PGRFECBAJ, PGRCODSAF)
                    ESTAFI                                            --17 ESTADO AFILIADO 
              FROM (SELECT                 
                           PGRCODCIA,PGRCODSUC,PGRCODSAF,PGRCODGFA,PGRCODPNA,
                           PGRFECBAJ,PGRFECAFL,PGRFECCON,PGRFECURE,PGRFECGFF,
                           PGRCODCAT,PGRCODPRG,PNADESAPA,PNADESAMA,PNANOMBRE,
                           PNANOMBR2,PNAFECNAC,PNATIPDOC,PNACODSEX,PNADOCIDE,
                           PNACODPNA
                   FROM PBFPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND PNADOCIDE LIKE PV_NUMDOC || '%'
                                    ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
              WHERE A.PGRCODCIA = B.PCNCODCIA
                AND A.PGRCODSUC = B.PCNCODSUC
                AND A.PGRCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
            UNION ALL
            SELECT    
                      TRIM (PNADESAPA) APEPAT,                      --01 APELLIDO PATERNO
                      TRIM (PNADESAMA) APEMAT,                      --02 APELLIDO MATERNO    
                      TRIM (PNANOMBRE) NOMBR1,                      --03 NOMBRE 1
                      TRIM (PNANOMBR2) NOMBR2,                      --04 OTROS NOMBRES
                      '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA 
                      CODAFIR,                                      --05 CODIGO DE AFILIADO
                   'GRUPAL' TIPAFI,                                 --06 TIPO AFILIADO
                   DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'),
                           NULL, 'DNI',
                           PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'))
                      TIPDOC,                                      --07 TIPO DOCUMENTO IDENTIDAD
                   PNADOCIDE NUMDOC,                               --08 NUMERO DE DOCUMENTO
                   TO_CHAR(GRPFECING,'DD/MM/YYYY') FECAFI,                               --09 FECHA DE INGRESO DEL AFILIADO
                   NULL FECREN,                                    --10 FECHA DE RENOVACION DEL AFILIADO
                   TO_CHAR(GRPFECBAJ,'DD/MM/YYYY') FECBAJ,                               --11 FECHA DE BAJA DEL AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.
                    FNC_ES_PACIENTE (
                      '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA)
                      ESPACI,                                      --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC,                               --13 FECHA DE NACIMIENTO DEL AFILIADO
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI,                                         --14 EDAD DEL AFILIADO
                   PNACODPNA CODPAC,                               --15 CODIGO PACIENTE
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                   SEXAFI,                                          --16 SEXO
                   PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('2'||LPAD(GRPCODONC,8,'0')||GRPCODPNA, 
                   GRPFECBAJ, NULL)
                   ESTAFI                                     --17 ESTADO AFILIADO                   
              FROM (SELECT                
                          GRPCODCIA,GRPCODSUC,GRPCODONC,GRPCODPNA,GRPFECBAJ,
                          PNACODPNA,PNAFECNAC,GRPFAMGRP,GRPCATGRP,PNADOCIDE,
                          PNATIPDOC,PNADESAPA,PNADESAMA,PNANOMBRE,PNANOMBR2,
                          PNACODSEX,GRPFECCON,GRPFECING,GRPEMPGRP
                   FROM GRPPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND PNADOCIDE LIKE PV_NUMDOC || '%'
                               ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
          WHERE     A.GRPCODCIA = B.PCNCODCIA
                AND A.GRPCODSUC = B.PCNCODSUC
                AND A.GRPCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
                    ))
                    WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;


--PARA BUSQUEDAS DE TIPO 3 CODIGO DE AFILIADO
      ELSIF PV_TIPBUS = '3'
      THEN


         SELECT SUM (CAN)
           INTO PN_VALOR
           FROM(
         SELECT COUNT(1) CAN
           FROM (SELECT *
                   FROM PBFPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND '1' = SUBSTR (PV_CODAFI, 1, 1)
                        AND PGRCODGFA = SUBSTR (PV_CODAFI, 2, 8)
                        AND PGRCODPNA = SUBSTR (PV_CODAFI, 10, 8)
                                    ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
              WHERE A.PGRCODCIA = B.PCNCODCIA
                AND A.PGRCODSUC = B.PCNCODSUC
                AND A.PGRCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
        UNION   
       SELECT COUNT(1) CAN
           FROM (SELECT *
                   FROM GRPPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND '2' = SUBSTR (PV_CODAFI, 1, 1)
                        AND GRPCODONC = SUBSTR (PV_CODAFI, 2, 8)
                        AND GRPCODPNA = SUBSTR (PV_CODAFI, 10, 8)
                               ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
          WHERE     A.GRPCODCIA = B.PCNCODCIA
                AND A.GRPCODSUC = B.PCNCODSUC
                AND A.GRPCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
                );

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

         OPEN RESULTSET FOR
            SELECT * FROM(            
            SELECT             
            ROW_NUMBER() OVER (ORDER BY APEPAT,APEMAT,NOMBR1,NOMBR2) RN,--01 NUMERO DE REGISTRO
                   APEPAT,                                              --02 APELLIDO PATERNO
                   APEMAT,                                              --03 APELLIDO MATERNO
                   NOMBR1,                                              --04 NOMBRE 1
                   NOMBR2,                                              --05 OTROS NOMBRES
                   CODAFIR,                                             --06 CODIGO DE AFILIADO
                   TIPAFI,                                              --07 TIPO AFILIADO
                   TIPDOC,                                              --08 TIPO DOCUMENTO IDENTIDAD
                   NUMDOC,                                              --09 NUMERO DE DOCUMENTO
                   FECAFI,                                              --10 FECHA DE INGRESO DEL AFILIADO
                   FECREN,                                              --11 FECHA DE RENOVACION DEL AFILIADO
                   FECBAJ,                                              --12 FECHA DE BAJA DEL AFILIADO
                   ESPACI,                                              --13 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   FECNAC,                                              --14 FECHA DE NACIMIENTO DEL AFILIADO
                   EDAAFI,                                              --15 EDAD DEL AFILIADO
                   CODPAC,                                              --16 CODIGO PACIENTE
                   SEXAFI,                                               --17 SEXO AFILIADO
                   ESTAFI                                               --18 ESTADO AFILIADO
            FROM(
            SELECT
                   TRIM (PNADESAPA) APEPAT,                               --01 APELLIDO PATERNO
                   TRIM (PNADESAMA) APEMAT,                               --02 APELLIDO MATERNO
                   TRIM (PNANOMBRE) NOMBR1,                               --03 NOMBRE 1
                   TRIM (PNANOMBR2) NOMBR2,                               --04 OTROS NOMBRES
                   '1' || PGRCODGFA || PGRCODPNA CODAFIR,                 --05 CODIGO DE AFILIADO
                   'INDIVIDUAL' TIPAFI,                                   --06 TIPO AFILIADO
                   DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'),
                           NULL, 'DNI',
                           PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'))
                   TIPDOC,                                                --07 TIPO DOCUMENTO IDENTIDAD
                   PNADOCIDE NUMDOC,                                      --08 NUMERO DE DOCUMENTO
                   TO_CHAR(PGRFECAFL,'DD/MM/YYYY') FECAFI,                                      --09 FECHA DE INGRESO DEL AFILIADO
                   TO_CHAR(PGRFECURE,'DD/MM/YYYY') FECREN,                                      --10 FECHA DE RENOVACION DEL AFILIADO
                   TO_CHAR(PGRFECBAJ,'DD/MM/YYYY') FECBAJ,                                      --11 FECHA DE BAJA DEL AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.
                    FNC_ES_PACIENTE ('1' || PGRCODGFA || PGRCODPNA)
                      ESPACI,                                             --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC,                                      --13 FECHA DE NACIMIENTO DEL AFILIADO
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI,                                                 --14 EDAD DEL AFILIADO
                   PNACODPNA CODPAC,                               --15 CODIGO PACIENTE
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                   SEXAFI,                                          --16 SEXO 
                   PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('1'||PGRCODGFA||PGRCODPNA, 
                    PGRFECBAJ, PGRCODSAF)
                      ESTAFI                                            --17 ESTADO AFILIADO
              FROM (SELECT                 
                           PGRCODCIA,PGRCODSUC,PGRCODSAF,PGRCODGFA,PGRCODPNA,
                           PGRFECBAJ,PGRFECAFL,PGRFECCON,PGRFECURE,PGRFECGFF,
                           PGRCODCAT,PGRCODPRG,PNADESAPA,PNADESAMA,PNANOMBRE,
                           PNANOMBR2,PNAFECNAC,PNATIPDOC,PNACODSEX,PNADOCIDE,
                           PNACODPNA
                   FROM PBFPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND '1' = SUBSTR (PV_CODAFI, 1, 1)
                        AND PGRCODGFA = SUBSTR (PV_CODAFI, 2, 8)
                        AND PGRCODPNA = SUBSTR (PV_CODAFI, 10, 8)
                                    ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
              WHERE A.PGRCODCIA = B.PCNCODCIA
                AND A.PGRCODSUC = B.PCNCODSUC
                AND A.PGRCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
            UNION ALL
            SELECT    
                      TRIM (PNADESAPA) APEPAT,                      --01 APELLIDO PATERNO
                      TRIM (PNADESAMA) APEMAT,                      --02 APELLIDO MATERNO    
                      TRIM (PNANOMBRE) NOMBR1,                      --03 NOMBRE 1
                      TRIM (PNANOMBR2) NOMBR2,                      --04 OTROS NOMBRES
                      '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA 
                      CODAFIR,                                      --05 CODIGO DE AFILIADO
                   'GRUPAL' TIPAFI,                                 --06 TIPO AFILIADO
                   DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'),
                           NULL, 'DNI',
                           PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'))
                      TIPDOC,                                      --07 TIPO DOCUMENTO IDENTIDAD
                   PNADOCIDE NUMDOC,                               --08 NUMERO DE DOCUMENTO
                   TO_CHAR(GRPFECING,'DD/MM/YYYY') FECAFI,                               --09 FECHA DE INGRESO DEL AFILIADO
                   NULL FECREN,                                    --10 FECHA DE RENOVACION DEL AFILIADO
                   TO_CHAR(GRPFECBAJ,'DD/MM/YYYY') FECBAJ,                               --11 FECHA DE BAJA DEL AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.
                    FNC_ES_PACIENTE (
                      '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA)
                      ESPACI,                                      --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC,                               --13 FECHA DE NACIMIENTO DEL AFILIADO
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI,                                         --14 EDAD DEL AFILIADO
                   PNACODPNA CODPAC,                               --15 CODIGO PACIENTE
                   DECODE (PNACODSEX,  'M', 'MASCULINO',  'F', 'FEMENINO')
                   SEXAFI,                                          --16 SEXO 
                   PCK_WS_SITEDS_V10.FN_OBT_ESTADO ('2'||LPAD(GRPCODONC,8,'0')||GRPCODPNA, 
                   GRPFECBAJ, NULL)
                      ESTAFI                                    --26 ESTADO AFILIADO
              FROM (SELECT                
                          GRPCODCIA,GRPCODSUC,GRPCODONC,GRPCODPNA,GRPFECBAJ,
                          PNACODPNA,PNAFECNAC,GRPFAMGRP,GRPCATGRP,PNADOCIDE,
                          PNATIPDOC,PNADESAPA,PNADESAMA,PNANOMBRE,PNANOMBR2,
                          PNACODSEX,GRPFECCON,GRPFECING,GRPEMPGRP
                   FROM GRPPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND '2' = SUBSTR (PV_CODAFI, 1, 1)
                        AND GRPCODONC = SUBSTR (PV_CODAFI, 2, 8)
                        AND GRPCODPNA = SUBSTR (PV_CODAFI, 10, 8)
                               ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
          WHERE     A.GRPCODCIA = B.PCNCODCIA
                AND A.GRPCODSUC = B.PCNCODSUC
                AND A.GRPCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
                    ))
                    WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;

      ELSE
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTE DATOS';

         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_PACIENTE_AFI',
                           PV_TIPBUS
                        || '-'
                        || PV_CODAFI
                        || '-'
                        || PV_TIPDOC
                        || '-'
                        || PV_NUMDOC
                        || '-'
                        || PV_APEPAT
                        || '-'
                        || PV_APEMAT
                        || '-'
                        || PV_NOMAFI);

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                  --10
                   NULL,                                                  --11
                   NULL,                                                  --12
                   NULL,                                                  --13
                   NULL,                                                  --14
                   NULL
              FROM DUAL;
      END IF;
   EXCEPTION
      WHEN DATO_ERR
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                  --10
                   NULL,                                                  --11
                   NULL,                                                  --12
                   NULL,                                                  --13
                   NULL,                                                  --14
                   NULL
              FROM DUAL;
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                  --10
                   NULL,                                                  --11
                   NULL,                                                  --12
                   NULL,                                                  --13
                   NULL,                                                  --14
                   NULL
              FROM DUAL;
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;


         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_PACIENTE_AFI',
                           PV_TIPBUS
                        || '-'
                        || PV_CODAFI
                        || '-'
                        || PV_TIPDOC
                        || '-'
                        || PV_NUMDOC
                        || '-'
                        || PV_APEPAT
                        || '-'
                        || PV_APEMAT
                        || '-'
                        || PV_NOMAFI);

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL,                                                   --6
                   NULL,                                                   --7
                   NULL,                                                   --8
                   NULL,                                                   --9
                   NULL,                                                  --10
                   NULL,                                                  --11
                   NULL,                                                  --12
                   NULL,                                                  --13
                   NULL,                                                  --14
                   NULL
              FROM DUAL;
   
   END PRC_PACIENTE_AFI;

--07 LISTA CLINICAS
   PROCEDURE PRC_LISTA_CLINICAS (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA 1. TODAS LAS CLINICAS, 2. POR CODIGO, 3. POR DESCRIPCION
                               PV_CODCLI            IN      VARCHAR2, --04 CODIGO DE CLINICA O IPRESS
                               PV_NOMCLI            IN      VARCHAR2, --05 NOMBRE DE CLINICA O IPRESS
                               PO_TOTAL             OUT     NUMBER,   --06 TOTAL REGISTROS
                               PO_IDTRAN            OUT     VARCHAR2, --07 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT VARCHAR2,     --08 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --09 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --10 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR)
                               IS
      PV_ERROR   VARCHAR2 (4000) := NULL;
      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;
 
   BEGIN
        
        IF    PV_TIPBUS='1' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM TEDEF10_RUCXIPRESS 
             WHERE TIPO='IP' 
               AND CODSU IS NOT NULL 
               AND CODRENIPRESS IS NOT NULL;

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT *FROM (
            SELECT 
            ROW_NUMBER() OVER (ORDER BY CODSU,DESABR) RN,               --01 NUMERO DE REGISTRO            
                  CODRENIPRESS CODCLI,                                         --02 CODIGO CLINICA O IPRESS
                  DESABR NOMCLI                                         --03 NOMBRE CLINICA O IPRESS
              FROM TEDEF10_RUCXIPRESS 
             WHERE TIPO='IP' 
               AND CODSU IS NOT NULL 
               AND CODRENIPRESS IS NOT NULL
               )
               WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;               
               
        
        ELSIF PV_TIPBUS='2' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM TEDEF10_RUCXIPRESS 
             WHERE TIPO='IP' 
               AND CODSU IS NOT NULL 
               AND CODRENIPRESS IS NOT NULL
               AND CODRENIPRESS LIKE PV_CODCLI||'%';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT *FROM (
            SELECT 
            ROW_NUMBER() OVER (ORDER BY CODSU,DESABR) RN,               --01 NUMERO DE REGISTRO            
                  CODRENIPRESS CODCLI,                                         --02 CODIGO CLINICA O IPRESS
                  DESABR NOMCLI                                         --03 NOMBRE CLINICA O IPRESS
              FROM TEDEF10_RUCXIPRESS 
             WHERE TIPO='IP' 
               AND CODSU IS NOT NULL 
               AND CODRENIPRESS IS NOT NULL
               AND CODRENIPRESS LIKE PV_CODCLI||'%'
               )
               WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN; 

        ELSIF PV_TIPBUS='3' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM TEDEF10_RUCXIPRESS 
             WHERE TIPO='IP' 
               AND CODSU IS NOT NULL 
               AND CODRENIPRESS IS NOT NULL
               AND UPPER(DESABR) LIKE '%'||PV_NOMCLI||'%';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT *FROM (
            SELECT 
            ROW_NUMBER() OVER (ORDER BY CODSU,DESABR) RN,               --01 NUMERO DE REGISTRO            
                  CODRENIPRESS CODCLI,                                         --02 CODIGO CLINICA O IPRESS
                  DESABR NOMCLI                                         --03 NOMBRE CLINICA O IPRESS
              FROM TEDEF10_RUCXIPRESS 
             WHERE TIPO='IP' 
               AND CODSU IS NOT NULL 
               AND CODRENIPRESS IS NOT NULL
               AND UPPER(DESABR) LIKE '%'||PV_NOMCLI||'%'
               )
               WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN; 

        ELSE

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
            
              OPEN RESULTSET FOR        
            SELECT NULL,
                   NULL,
                   NULL
                   FROM DUAL;
        
        END IF;
    EXCEPTION
      WHEN DATO_ERR
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL
              FROM DUAL;
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL
              FROM DUAL;
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;


         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LISTA_CLINICAS',
                           PV_TIPBUS
                        || '-'
                        || PV_CODCLI
                        || '-'
                        || PV_NOMCLI);

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL
              FROM DUAL;

    END PRC_LISTA_CLINICAS;                               

--08 LISTA DIAGNOSTICO
   PROCEDURE PRC_LISTA_DIAGNOSTICO (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA 1. POR CODIGO, 2. POR DESCRIPCION
                               PV_CODDIA            IN      VARCHAR2, --04 CODIGO DIAGNOSTICO
                               PV_NOMDIA            IN      VARCHAR2, --05 NOMBRE DIAGNOSTICO
                               PO_TOTAL             OUT     NUMBER,   --06 TOTAL REGISTROS
                               PO_IDTRAN            OUT     VARCHAR2, --07 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT VARCHAR2,     --08 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --09 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --10 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR)
                               IS
      PV_ERROR   VARCHAR2 (4000) := NULL;
      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;
      DATO_SIN_GRUPO   EXCEPTION;
      PN_VALOR1  NUMBER := 0;
      

    BEGIN
    
        IF    PV_TIPBUS='1' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM DGNDATMAE A,
                   ADMTABGEN B               
             WHERE DGNCODCIA=TBGCODCIA
               AND DGNCODSUC=TBGCODSUC
               AND DGNCODCIA='001'
               AND TBGTABCOD='464'
               AND DGNCODGRU=TBGDATCOD;

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT  
                    RN,                                 --01 NUMERO DE REGISTRO            
                    CODDIA,                             --02 CODIGO DIAGNOSTICO
                    NOMDIA,                             --03 NOMBRE DIAGNOSTICO
                    CODGRU,                             --04 CODIGO GRUPO DIAGNOSTICO
                    NOMGRU                              --05 NOMBRE GRUPO DIAGNOSTICO            
            FROM(
            SELECT  
            ROW_NUMBER() OVER (ORDER BY DGNDESCRI,TRIM(TBGDATSEG)) RN,    --01 NUMERO DE REGISTRO            
                    DGNCODREF   CODDIA,                             --02 CODIGO DIAGNOSTICO
                    DGNDESCRI   NOMDIA,                             --03 NOMBRE DIAGNOSTICO
                    DGNCODGRU   CODGRU,                             --04 CODIGO GRUPO DIAGNOSTICO
                    TRIM(TBGDATSEG) NOMGRU                          --05 NOMBRE GRUPO DIAGNOSTICO
              FROM DGNDATMAE A,
                   ADMTABGEN B               
             WHERE DGNCODCIA=TBGCODCIA
               AND DGNCODSUC=TBGCODSUC
               AND DGNCODCIA='001'
               AND TBGTABCOD='464'
               AND DGNCODGRU=TBGDATCOD
                )
            WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;
        
        ELSIF PV_TIPBUS='2' THEN

            SELECT COUNT(DGNCODREF)  --CODIGO DE DIAGNOSTICO
              INTO PN_VALOR
              FROM DGNDATMAE A,
                   ADMTABGEN B               
             WHERE DGNCODCIA=TBGCODCIA(+)
               AND DGNCODSUC=TBGCODSUC(+)
               AND DGNCODCIA='001'
               AND TBGTABCOD(+)='464'
               AND DGNCODGRU=TBGDATCOD(+)
               AND DGNCODREF LIKE PV_CODDIA;


            SELECT COUNT(TBGDATCOD) --CODIGO DE GRUPO DE DIAGNOSTICO
              INTO PN_VALOR1
              FROM DGNDATMAE A,
                   ADMTABGEN B               
             WHERE DGNCODCIA=TBGCODCIA(+)
               AND DGNCODSUC=TBGCODSUC(+)
               AND DGNCODCIA='001'
               AND TBGTABCOD(+)='464'
               AND DGNCODGRU=TBGDATCOD(+)
               AND DGNCODREF LIKE PV_CODDIA;

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;
         
         IF PN_VALOR1 < 1
         THEN
            RAISE DATO_SIN_GRUPO;
         END IF;


    IF PN_VALOR>0 AND PN_VALOR1>0 THEN
    
          SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT  
                    RN,                                 --01 NUMERO DE REGISTRO            
                    CODDIA,                             --02 CODIGO DIAGNOSTICO
                    NOMDIA,                             --03 NOMBRE DIAGNOSTICO
                    CODGRU,                             --04 CODIGO GRUPO DIAGNOSTICO
                    NOMGRU                              --05 NOMBRE GRUPO DIAGNOSTICO            
            FROM(
            SELECT  
            ROW_NUMBER() OVER (ORDER BY DGNDESCRI,TRIM(TBGDATSEG)) RN,    --01 NUMERO DE REGISTRO            
                    DGNCODREF   CODDIA,                             --02 CODIGO DIAGNOSTICO
                    DGNDESCRI   NOMDIA,                             --03 NOMBRE DIAGNOSTICO
                    DGNCODGRU   CODGRU,                             --04 CODIGO GRUPO DIAGNOSTICO
                    TRIM(TBGDATSEG) NOMGRU                          --05 NOMBRE GRUPO DIAGNOSTICO
              FROM DGNDATMAE A,
                   ADMTABGEN B               
             WHERE DGNCODCIA=TBGCODCIA
               AND DGNCODSUC=TBGCODSUC
               AND DGNCODCIA='001'
               AND TBGTABCOD='464'
               AND DGNCODGRU=TBGDATCOD
               AND DGNCODREF LIKE PV_CODDIA
                )
            WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;

    END IF;

        ELSIF PV_TIPBUS='3' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM DGNDATMAE A,
                   ADMTABGEN B               
             WHERE DGNCODCIA=TBGCODCIA
               AND DGNCODSUC=TBGCODSUC
               AND DGNCODCIA='001'
               AND TBGTABCOD='464'
               AND DGNCODGRU=TBGDATCOD
               AND DGNDESCRI LIKE '%'||PV_NOMDIA||'%';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

               OPEN RESULTSET FOR        
            SELECT  
                    RN,                                 --01 NUMERO DE REGISTRO            
                    CODDIA,                             --02 CODIGO DIAGNOSTICO
                    NOMDIA,                             --03 NOMBRE DIAGNOSTICO
                    CODGRU,                             --04 CODIGO GRUPO DIAGNOSTICO
                    NOMGRU                              --05 NOMBRE GRUPO DIAGNOSTICO            
            FROM(
            SELECT  
            ROW_NUMBER() OVER (ORDER BY DGNDESCRI,TRIM(TBGDATSEG)) RN,    --01 NUMERO DE REGISTRO            
                    DGNCODREF   CODDIA,                             --02 CODIGO DIAGNOSTICO
                    DGNDESCRI   NOMDIA,                             --03 NOMBRE DIAGNOSTICO
                    DGNCODGRU   CODGRU,                             --04 CODIGO GRUPO DIAGNOSTICO
                    TRIM(TBGDATSEG) NOMGRU                          --05 NOMBRE GRUPO DIAGNOSTICO
              FROM DGNDATMAE A,
                   ADMTABGEN B               
             WHERE DGNCODCIA=TBGCODCIA
               AND DGNCODSUC=TBGCODSUC
               AND DGNCODCIA='001'
               AND TBGTABCOD='464'
               AND DGNCODGRU=TBGDATCOD
               AND DGNDESCRI LIKE '%'||PV_NOMDIA||'%'
                )
            WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;

        ELSE

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
            
              OPEN RESULTSET FOR        
            SELECT NULL,
                   NULL
                   FROM DUAL;
        
        END IF;
    EXCEPTION
      WHEN DATO_SIN_GRUPO
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '2';
         PO_MENRES := 'DIAGNOSTICO SIN GRUPO';

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;



      WHEN DATO_ERR
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;


         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LISTA_DIAGNOSTICO',
                           PV_TIPBUS
                        || '-'
                        || PV_CODDIA
                        || '-'
                        || PV_NOMDIA);

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;

    END PRC_LISTA_DIAGNOSTICO;                               

--09 GRUPO DIAGNOSTICO
   PROCEDURE PRC_GRUPO_DIAGNOSTICO (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA 1. POR CODIGO, 2. POR DESCRIPCION
                               PV_CODGRU            IN      VARCHAR2, --04 CODIGO GRUPO DIAGNOSTICO
                               PV_NOMGRU            IN      VARCHAR2, --05 NOMBRE GRUPO DIAGNOSTICO
                               PO_TOTAL             OUT     NUMBER,   --06 TOTAL REGISTROS
                               PO_IDTRAN            OUT     VARCHAR2, --07 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT VARCHAR2,     --08 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --09 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --10 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR)
                               IS
      PV_ERROR   VARCHAR2 (4000) := NULL;
      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;

    BEGIN
    
        IF    PV_TIPBUS='1' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM ADMTABGEN 
             WHERE TBGCODCIA='001'
               AND TBGCODSUC='001'
               AND TBGTABCOD='464'; 

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        

                      SELECT * FROM (
                      SELECT           
                        ROW_NUMBER() OVER (ORDER BY CODGRU,NOMGRU) RN,      --01 NUMERO DE REGISTRO
                        CODGRU,                                             --02 CODIGO GRUPO DIAGNOSTICO
                        NOMGRU                                              --03 NOMBRE GRUPO DIAGNOSTICO
                        FROM(
                        SELECT  
                      DISTINCT             
                                TBGDATCOD   CODGRU,                             --02 CODIGO GRUPO DIAGNOSTICO
                                TRIM(TBGDATSEG)   NOMGRU                        --03 NOMBRE GRUPO DIAGNOSTICO
                          FROM ADMTABGEN 
                         WHERE TBGCODCIA='001'
                           AND TBGCODSUC='001'
                           AND TBGTABCOD='464'
                            )
                            )
                        WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;
        
        ELSIF PV_TIPBUS='2' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM ADMTABGEN 
             WHERE TBGCODCIA='001'
               AND TBGCODSUC='001'
               AND TBGTABCOD='464'
               AND TBGDATCOD LIKE PV_CODGRU||'%';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        

                      SELECT * FROM (
                      SELECT           
                        ROW_NUMBER() OVER (ORDER BY CODGRU,NOMGRU) RN,      --01 NUMERO DE REGISTRO
                        CODGRU,                                             --02 CODIGO GRUPO DIAGNOSTICO
                        NOMGRU                                              --03 NOMBRE GRUPO DIAGNOSTICO
                        FROM(
                        SELECT  
                      DISTINCT             
                                TBGDATCOD   CODGRU,                             --02 CODIGO GRUPO DIAGNOSTICO
                                TRIM(TBGDATSEG)   NOMGRU                        --03 NOMBRE GRUPO DIAGNOSTICO
                          FROM ADMTABGEN 
                         WHERE TBGCODCIA='001'
                           AND TBGCODSUC='001'
                           AND TBGTABCOD='464'
                           AND TBGDATCOD LIKE PV_CODGRU||'%'
                            )
                            )
                        WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;

        ELSIF PV_TIPBUS='3' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM ADMTABGEN 
             WHERE TBGCODCIA='001'
               AND TBGCODSUC='001'
               AND TBGTABCOD='464'
               AND TRIM(TBGDATSEG) LIKE '%'||PV_NOMGRU||'%';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        

                      SELECT * FROM (
                      SELECT           
                        ROW_NUMBER() OVER (ORDER BY CODGRU,NOMGRU) RN,      --01 NUMERO DE REGISTRO
                        CODGRU,                                             --02 CODIGO GRUPO DIAGNOSTICO
                        NOMGRU                                              --03 NOMBRE GRUPO DIAGNOSTICO
                        FROM(
                        SELECT  
                      DISTINCT             
                                TBGDATCOD   CODGRU,                             --02 CODIGO GRUPO DIAGNOSTICO
                                TRIM(TBGDATSEG)   NOMGRU                        --03 NOMBRE GRUPO DIAGNOSTICO
                          FROM ADMTABGEN 
                         WHERE TBGCODCIA='001'
                           AND TBGCODSUC='001'
                           AND TBGTABCOD='464'
                           AND TRIM(TBGDATSEG) LIKE '%'||PV_NOMGRU||'%'
                            )
                            )
                        WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;


        ELSE

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
            
              OPEN RESULTSET FOR        
            SELECT NULL,
                   NULL
                   FROM DUAL;
        
        END IF;
    EXCEPTION
      WHEN DATO_ERR
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;


         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LISTA_DIAGNOSTICO',
                           PV_TIPBUS
                        || '-'
                        || PV_CODGRU
                        || '-'
                        || PV_NOMGRU);

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;

    END PRC_GRUPO_DIAGNOSTICO;                               


--10 MEDICO TRATANTE
   PROCEDURE PRC_MED_TRATANTE  (PV_CODAFI            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                PO_IDTRAN            OUT     VARCHAR2,   --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                PO_FECTRA            OUT     VARCHAR2,   --03 FECHA DE LA TRANSACCI�N
                                PO_CODRES            OUT     VARCHAR2,   --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                PO_MENRES            OUT     VARCHAR2,   --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                RESULTSET            OUT     SYS_REFCURSOR) 
                                IS

      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;

   BEGIN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM PNADATMAE C,PCNDGNMAE A, DGNDATMAE B
              WHERE     PNACODCIA = PDGCODCIA
                    AND PNACODSUC = PDGCODSUC
                    AND PNACODPNA = PDGMEDCOD
                    AND PDGCODCIA = DGNCODCIA
                    AND PDGCODSUC = DGNCODSUC
                    AND PDGCODIGO = DGNCODIGO
                    AND PDGCODCIA = '001'
                    AND PDGCODSUC = '001'
                    AND DGNCODCIA = '001'
                    AND DGNCODSUC = '001'
                    AND PDGPCNCOD = SUBSTR (PV_CODAFI, 10, 8);          

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR
                SELECT 
                        PDGMEDCOD CODMED,           --01 CODIGOMT CODIGO DE MEDICO TRATANTE
                        CODCMPMED CMPTRA,                  --02 C�DIGO DE MEDICOS DEL PER�
                        TRIM(PNADESAPA) APEPAT,     --03 APELLIDO PATERNO DEL MEDICO TRATANTE
                        TRIM(PNADESAMA) APEMAT,     --04 APELLIDO MATERNO MEDICO TRATANTE
                        TRIM(PNANOMBRE)||' '||
                        TRIM(PNANOMBR2) NOMMED      --05 NOMBRES DEL MEDICO TRATANTE
                  FROM PNADATMAE C,PCNDGNMAE A, DGNDATMAE B, SOLBFINSTITUCIONES D
                  WHERE     PNACODCIA = PDGCODCIA
                        AND PNACODSUC = PDGCODSUC
                        AND PNACODPNA = PDGMEDCOD
                        AND PDGCODCIA = DGNCODCIA
                        AND PDGCODSUC = DGNCODSUC
                        AND PDGCODIGO = DGNCODIGO
                        AND PDGCODCIA = '001'
                        AND PDGCODSUC = '001'
                        AND DGNCODCIA = '001'
                        AND DGNCODSUC = '001'
                        AND PNACODPNA = D.PJUCODPJU(+)
                        AND PDGPCNCOD = SUBSTR (PV_CODAFI, 10, 8);
    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
         TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;
           
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIAG_PACIENTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                   --7
                   FROM DUAL;
 
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_EMP_CONTRATANTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                    --7
                   FROM DUAL;
                   
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIAG_PACIENTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                 --7
                   FROM DUAL;

    END PRC_MED_TRATANTE;

--11 MEDICO TRATANTE
   PROCEDURE PRC_LIS_MED_TRATANTE  (PN_INI               IN      NUMBER,     --01 REGISTRO INICIO
                                    PN_FIN               IN      NUMBER,     --02 REGISTRO FIN
                                    PV_TIPBUS            IN      VARCHAR2,   --03 CODIGO DE AFILIADO
                                    PV_CODMED            IN      VARCHAR2,   --04 CODIGO DE AFILIADO
                                    PV_PATMED            IN      VARCHAR2,   --05 CODIGO DE AFILIADO
                                    PV_MATMED            IN      VARCHAR2,   --06 CODIGO DE AFILIADO
                                    PV_NOMMED            IN      VARCHAR2,   --07 CODIGO DE AFILIADO                                                                        
                                    PV_CODCMP            IN      VARCHAR2,   --08 CODIGO MEDICO DE PERU
                                    PO_TOTAL             OUT     NUMBER,     --09 TOTAL REGISTROS
                                    PO_IDTRAN            OUT     VARCHAR2,   --10 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                    PO_FECTRA            OUT     VARCHAR2,   --11 FECHA DE LA TRANSACCI�N
                                    PO_CODRES            OUT     VARCHAR2,   --12 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                    PO_MENRES            OUT     VARCHAR2,   --13 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                    RESULTSET            OUT     SYS_REFCURSOR) 
                                    IS

      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;

   BEGIN

        IF PV_TIPBUS='1' THEN
        
            SELECT COUNT(1)
              INTO PN_VALOR
              FROM PNADATMAE A, SOLBFINSTITUCIONES B
              WHERE     PNACODCIA = PJUCODCIA
                    AND PNACODSUC = PJUCODSUC
                    AND PNACODPNA = PJUCODPJU
                    AND PNACODCIA='001' 
                    AND PNACODSUC='001'
                    AND PCK_MANT_PROV.FN_TIPO_PROV(PJUCODPJU)='MEDICO'
                    AND PNACODPNA = PV_CODMED;          

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

         OPEN RESULTSET FOR

                SELECT * FROM(
                SELECT 
                ROW_NUMBER() OVER (ORDER BY APEPAT,APEMAT,NOMMED) RN,  --01 NUMERO DE REGISTRO
                CODMED,                                                 --02 CODIGOMT CODIGO DE MEDICO TRATANTE
                CMPTRA,                                                 --03 C�DIGO DE MEDICOS DEL PER�
                APEPAT,                                                 --04 APELLIDO PATERNO DEL MEDICO TRATANTE
                APEMAT,                                                 --05 APELLIDO MATERNO MEDICO TRATANTE
                NOMMED                                                  --06 NOMBRES DEL MEDICO TRATANTE
                FROM
                (SELECT 
                        DISTINCT
                        PNACODPNA CODMED,           --01 CODIGOMT CODIGO DE MEDICO TRATANTE
                        CODCMPMED CMPTRA,                  --02 C�DIGO DE MEDICOS DEL PER�
                        TRIM(PNADESAPA) APEPAT,     --03 APELLIDO PATERNO DEL MEDICO TRATANTE
                        TRIM(PNADESAMA) APEMAT,     --04 APELLIDO MATERNO MEDICO TRATANTE
                        TRIM(PNANOMBRE)||' '||
                        TRIM(PNANOMBR2) NOMMED      --05 NOMBRES DEL MEDICO TRATANTE
              FROM PNADATMAE A, SOLBFINSTITUCIONES B
              WHERE     PNACODCIA = PJUCODCIA
                    AND PNACODSUC = PJUCODSUC
                    AND PNACODPNA = PJUCODPJU
                    AND PNACODCIA='001' 
                    AND PNACODSUC='001'
                    AND PCK_MANT_PROV.FN_TIPO_PROV(PJUCODPJU)='MEDICO'
                        AND PNACODPNA = PV_CODMED
                        )
                        )
                        WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;
       
        ELSIF PV_TIPBUS='2' THEN

        
            SELECT COUNT(1)
              INTO PN_VALOR
              FROM PNADATMAE A, SOLBFINSTITUCIONES B
              WHERE     PNACODCIA = PJUCODCIA
                    AND PNACODSUC = PJUCODSUC
                    AND PNACODPNA = PJUCODPJU
                    AND PNACODCIA='001' 
                    AND PNACODSUC='001'
                    AND PCK_MANT_PROV.FN_TIPO_PROV(PJUCODPJU)='MEDICO'
                    AND (PNADESAPA LIKE '%'||PV_PATMED || '%'
                         AND NVL (PNADESAMA, ' ') LIKE '%'||PV_MATMED || '%'
                         AND PNANOMBRE || '%' || PNANOMBR2 LIKE
                                '%'||REPLACE(PV_NOMMED,' ' , '%') || '%');
         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

         OPEN RESULTSET FOR

                SELECT * FROM(
                SELECT 
                ROW_NUMBER() OVER (ORDER BY APEPAT,APEMAT,NOMMED) RN,  --01 NUMERO DE REGISTRO
                CODMED,                                                 --02 CODIGOMT CODIGO DE MEDICO TRATANTE
                CMPTRA,                                                 --03 C�DIGO DE MEDICOS DEL PER�
                APEPAT,                                                 --04 APELLIDO PATERNO DEL MEDICO TRATANTE
                APEMAT,                                                 --05 APELLIDO MATERNO MEDICO TRATANTE
                NOMMED                                                  --06 NOMBRES DEL MEDICO TRATANTE
                FROM
                (SELECT 
                        DISTINCT
                        PNACODPNA CODMED,           --01 CODIGOMT CODIGO DE MEDICO TRATANTE
                        CODCMPMED CMPTRA,                  --02 C�DIGO DE MEDICOS DEL PER�
                        TRIM(PNADESAPA) APEPAT,     --03 APELLIDO PATERNO DEL MEDICO TRATANTE
                        TRIM(PNADESAMA) APEMAT,     --04 APELLIDO MATERNO MEDICO TRATANTE
                        TRIM(PNANOMBRE)||' '||
                        TRIM(PNANOMBR2) NOMMED      --05 NOMBRES DEL MEDICO TRATANTE
              FROM PNADATMAE A, SOLBFINSTITUCIONES B
              WHERE     PNACODCIA = PJUCODCIA
                    AND PNACODSUC = PJUCODSUC
                    AND PNACODPNA = PJUCODPJU
                    AND PNACODCIA='001' 
                    AND PNACODSUC='001'
                    AND PCK_MANT_PROV.FN_TIPO_PROV(PJUCODPJU)='MEDICO'
                        AND (PNADESAPA LIKE '%'||PV_PATMED || '%'
                             AND NVL (PNADESAMA, ' ') LIKE '%'||PV_MATMED || '%'
                             AND PNANOMBRE || ' %' || PNANOMBR2 LIKE
                                    '%'||REPLACE(PV_NOMMED,' ', '%') || '%')
                        )
                        )
                        WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;

        ELSIF PV_TIPBUS='3' THEN

        
            SELECT COUNT(1)
              INTO PN_VALOR
              FROM PNADATMAE A, SOLBFINSTITUCIONES B
              WHERE     PNACODCIA = PJUCODCIA
                    AND PNACODSUC = PJUCODSUC
                    AND PNACODPNA = PJUCODPJU
                    AND PNACODCIA='001' 
                    AND PNACODSUC='001'
                    AND PCK_MANT_PROV.FN_TIPO_PROV(PJUCODPJU)='MEDICO'
                    AND CODCMPMED=PV_CODCMP;
         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

         OPEN RESULTSET FOR

                SELECT * FROM(
                SELECT 
                ROW_NUMBER() OVER (ORDER BY APEPAT,APEMAT,NOMMED) RN,  --01 NUMERO DE REGISTRO
                CODMED,                                                 --02 CODIGOMT CODIGO DE MEDICO TRATANTE
                CMPTRA,                                                 --03 C�DIGO DE MEDICOS DEL PER�
                APEPAT,                                                 --04 APELLIDO PATERNO DEL MEDICO TRATANTE
                APEMAT,                                                 --05 APELLIDO MATERNO MEDICO TRATANTE
                NOMMED                                                  --06 NOMBRES DEL MEDICO TRATANTE
                FROM
                (SELECT 
                        DISTINCT
                        PNACODPNA CODMED,           --01 CODIGOMT CODIGO DE MEDICO TRATANTE
                        CODCMPMED CMPTRA,                  --02 C�DIGO DE MEDICOS DEL PER�
                        TRIM(PNADESAPA) APEPAT,     --03 APELLIDO PATERNO DEL MEDICO TRATANTE
                        TRIM(PNADESAMA) APEMAT,     --04 APELLIDO MATERNO MEDICO TRATANTE
                        TRIM(PNANOMBRE)||' '||
                        TRIM(PNANOMBR2) NOMMED      --05 NOMBRES DEL MEDICO TRATANTE
              FROM PNADATMAE A, SOLBFINSTITUCIONES B
              WHERE     PNACODCIA = PJUCODCIA
                    AND PNACODSUC = PJUCODSUC
                    AND PNACODPNA = PJUCODPJU
                    AND PNACODCIA='001' 
                    AND PNACODSUC='001'
                    AND PCK_MANT_PROV.FN_TIPO_PROV(PJUCODPJU)='MEDICO'
                        AND CODCMPMED=PV_CODCMP
                        )
                        )
                        WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;
        ELSE
            
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTE DATOS';

         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_MED_TRA',
                           PV_TIPBUS
                        || '-'
                        || PV_CODMED
                        || '-'
                        || PV_PATMED
                        || '-'
                        || PV_MATMED
                        || '-'
                        || PV_NOMMED);

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL,                                                   --4
                   NULL,                                                   --5
                   NULL                                                   --28
              FROM DUAL;            
        END IF;
                        
    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_MED_TRA',
                        PV_CODMED
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                   --7
                   FROM DUAL;
 
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_MED_TRA',
                        PV_CODMED
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                    --7
                   FROM DUAL;
                   
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_MED_TRA',
                        PV_CODMED
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL,NULL                                                 --7
                   FROM DUAL;

    END PRC_LIS_MED_TRATANTE;

--13 EMPRESA CONTRATANTE AFILIADO
   PROCEDURE PRC_EMP_CONTRATANTE( PV_CODAFI        IN      VARCHAR2, --01 CODIGO DE AFILIADO
                                  PO_IDTRAN        OUT     VARCHAR2, --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                  PO_FECTRA        OUT VARCHAR2,     --03 FECHA DE LA TRANSACCI�N
                                  PO_CODRES        OUT     VARCHAR2, --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                  PO_MENRES        OUT     VARCHAR2, --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                  RESULTSET        OUT     SYS_REFCURSOR) IS

      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;
 

   BEGIN

         SELECT SUM (CAN)
           INTO PN_VALOR
           FROM (
                 SELECT COUNT (1) CAN
                   FROM GRPPOBGRP PBF, PNADATMAE PNA
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND grpcodcia = '001'
                        AND grpcodsuc = '001'
                        AND '2' = SUBSTR (PV_CODAFI, 1, 1)
                        AND GRPCODONC = SUBSTR (PV_CODAFI, 2, 8)
                        AND GRPCODPNA = SUBSTR (PV_CODAFI, 10, 8));

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR
            SELECT 
                    PCK_WS_SITEDS_V10.
                    FN_OBT_DATOS_CONTRATANTE_GRP(NULL,GRPEMPGRP,'1') RUCCON,        --01 RUC CONTRATANTE 
                    PCK_WS_SITEDS_V10.
                    FN_OBT_DATOS_CONTRATANTE_GRP(NULL,GRPEMPGRP,'2') NOMCON        --01 RAZON SOCIAL
              FROM (SELECT     
                          GRPCODCIA,GRPCODSUC,GRPCODONC,GRPCODPNA,GRPFECBAJ,
                          PNACODPNA,PNAFECNAC,GRPFAMGRP,GRPCATGRP,PNADOCIDE,
                          PNATIPDOC,PNADESAPA,PNADESAMA,PNANOMBRE,PNANOMBR2,
                          PNACODSEX,GRPFECCON,GRPFECING,GRPEMPGRP
                      FROM GRPPOBGRP PBF, PNADATMAE PNA
                     WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                           AND PBF.GRPCODSUC = PNA.PNACODSUC
                           AND PBF.GRPCODPNA = PNA.PNACODPNA
                           AND PNACODCIA = '001'
                           AND PNACODSUC = '001'
                           AND GRPCODCIA = '001'
                           AND GRPCODSUC = '001'
                           AND '2' = SUBSTR (PV_CODAFI, 1, 1)
                           AND GRPCODONC = SUBSTR (PV_CODAFI, 2, 8)
                           AND GRPCODPNA = SUBSTR (PV_CODAFI, 10, 8)) PBF;

    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_CONTACTO_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL                                                   --9
                   FROM DUAL;
 
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_EMP_CONTRATANTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL                                                    --9
                   FROM DUAL;
                   
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_EMP_CONTRATANTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL                                                 --9
                   FROM DUAL;

    END PRC_EMP_CONTRATANTE;

--14 HISTORIAL DIAGNOSTICO PACIENTE
   PROCEDURE PRC_DIAG_PACIENTE (PV_CODAFI            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                PO_IDTRAN            OUT     VARCHAR2,   --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                PO_FECTRA            OUT VARCHAR2,       --03 FECHA DE LA TRANSACCI�N
                                PO_CODRES            OUT     VARCHAR2,   --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                PO_MENRES            OUT     VARCHAR2,   --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                RESULTSET            OUT     SYS_REFCURSOR) 
                                IS

      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;

   BEGIN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM PNADATMAE C,PCNDGNMAE A, DGNDATMAE B, SOLBFINSTITUCIONES D
              WHERE     PNACODCIA = PDGCODCIA
                    AND PNACODSUC = PDGCODSUC
                    AND PNACODPNA = PDGMEDCOD
                    AND PDGCODCIA = DGNCODCIA
                    AND PDGCODSUC = DGNCODSUC
                    AND PDGCODIGO = DGNCODIGO
                    AND PDGMEDCOD = PJUCODPJU(+)
                    AND PDGCODCIA = '001'
                    AND PDGCODSUC = '001'
                    AND DGNCODCIA = '001'
                    AND DGNCODSUC = '001'
                    AND PDGPCNCOD = SUBSTR (PV_CODAFI, 10, 8);          

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR
                SELECT 
                        DGNCODREF CODDIA,           --01 CODIGO DE DIAGNOSTICO
                        DGNDESCRI DESDIA,
                        TO_CHAR(PDGFECDGN,'DD/MM/YYYY') FECDIA,           --02 FECHA DEL DIAGNOSTICO
                        PDGMEDCOD CODMED,           --03 CODIGOMT CODIGO DE MEDICO TRATANTE
                        CODCMPMED CMPTRA,                  --04 C�DIGO DE MEDICOS DEL PER�
                        TRIM(PNADESAPA) APEPAT,     --05 APELLIDO PATERNO DEL MEDICO TRATANTE
                        TRIM(PNADESAMA) APEMAT,     --06 APELLIDO MATERNO MEDICO TRATANTE
                        TRIM(PNANOMBRE)||' '||
                        TRIM(PNANOMBR2) NOMMED      --07 NOMBRES DEL MEDICO TRATANTE
                  FROM PNADATMAE C,PCNDGNMAE A, DGNDATMAE B, SOLBFINSTITUCIONES D
                  WHERE     PNACODCIA = PDGCODCIA
                        AND PNACODSUC = PDGCODSUC
                        AND PNACODPNA = PDGMEDCOD
                        AND PDGMEDCOD = PJUCODPJU(+)
                        AND PDGCODCIA = DGNCODCIA
                        AND PDGCODSUC = DGNCODSUC
                        AND PDGCODIGO = DGNCODIGO
                        AND PDGCODCIA = '001'
                        AND PDGCODSUC = '001'
                        AND DGNCODCIA = '001'
                        AND DGNCODSUC = '001'
                        AND PDGPCNCOD = SUBSTR (PV_CODAFI, 10, 8);
    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIAG_PACIENTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                   --7
                   FROM DUAL;
 
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_EMP_CONTRATANTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                    --7
                   FROM DUAL;
                   
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIAG_PACIENTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                 --7
                   FROM DUAL;

    END PRC_DIAG_PACIENTE;

   FUNCTION FNC_LINEA (PV_LINEA IN OUT VARCHAR2, PV_CARACTER VARCHAR2)
      RETURN VARCHAR2
   IS
      LV_LINEA          VARCHAR2 (2000);
      LN_POS_COMA       NUMBER;
      RET      CONSTANT VARCHAR2 (1) := CHR (10);
      TAB      CONSTANT VARCHAR2 (1) := CHR (9);
      COMA     CONSTANT VARCHAR2 (1) := CHR (44);
      PUNTO    CONSTANT VARCHAR2 (1) := CHR (46);
      BLANCO   CONSTANT VARCHAR2 (1) := CHR (32);
      ASTER    CONSTANT VARCHAR2 (1) := CHR (42);
      PALOTE   CONSTANT VARCHAR2 (1) := CHR (124);
   BEGIN
      LN_POS_COMA := INSTR (PV_LINEA, PV_CARACTER);

      IF LN_POS_COMA = 0
      THEN
         LV_LINEA := PV_LINEA;
         PV_LINEA := NULL;
      ELSE
         LV_LINEA := SUBSTR (PV_LINEA, 1, LN_POS_COMA - 1);
      END IF;

      PV_LINEA := RTRIM (LTRIM (SUBSTR (PV_LINEA, LN_POS_COMA + 1)));

      RETURN (LV_LINEA);

   END FNC_LINEA;

--15 CONSULTA PREEXISTENCIA AFILIADO 04/04/2019
   PROCEDURE PRC_PREEXI_AFI (   PV_CODAFI            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                PO_IDTRAN            OUT     VARCHAR2,   --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                PO_FECTRA            OUT     VARCHAR2,   --03 FECHA DE LA TRANSACCI�N
                                PO_CODRES            OUT     VARCHAR2,   --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                PO_MENRES            OUT     VARCHAR2,   --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                RESULTSET            OUT     SYS_REFCURSOR) IS

      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;

   BEGIN
            
            SELECT COUNT(1)
              INTO PN_VALOR
              FROM ASE_PREEXISTENCIA A,
                   ASE_DIAGCIE10 B
             WHERE PRECODAFI = PV_CODAFI 
               AND A.COD_DIAG= B.COD_DIAG 
               AND PREESTPRE = '1';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR
            SELECT 
                    PRECODCOR SECPRE,                   --01 SECUENCIAL DE DIAGNOSTICO
                    A.COD_DIAG CODDIA,                  --02 CODIGO DE DIAGNOSTICO
                    DES_LARG_DIAG DESPRE                --03 DESCRIPCION DE DIAGNOSTICO
              FROM ASE_PREEXISTENCIA A,
                   ASE_DIAGCIE10 B
             WHERE PRECODAFI = PV_CODAFI
               AND A.COD_DIAG= B.COD_DIAG
               AND PREESTPRE = '1';
                   
                   
    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_PREEXI_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL                                                   --7
                   FROM DUAL;
 
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_PREEXI_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL                                                    --7
                   FROM DUAL;
                   
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_PREEXI_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL                                                 --7
                   FROM DUAL;

    END PRC_PREEXI_AFI;                                


--15 CONSULTA PREEXISTENCIA AFILIADO 04/04/2019
   PROCEDURE PRC_EXCLU_AFI (   PV_CODAFI            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                PO_IDTRAN            OUT     VARCHAR2,   --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                PO_FECTRA            OUT     VARCHAR2,   --03 FECHA DE LA TRANSACCI�N
                                PO_CODRES            OUT     VARCHAR2,   --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                PO_MENRES            OUT     VARCHAR2,   --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                RESULTSET            OUT     SYS_REFCURSOR) IS

      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;

   BEGIN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM ASE_EXCLUSION A,
                   ASE_DIAGCIE10 B
             WHERE EXCCODAFI = PV_CODAFI 
               AND A.COD_DIAG= B.COD_DIAG 
               AND EXCESTEXC = '1';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR
            SELECT 
                    EXCCODCOR SECEXC,                   --01 SECUENCIAL DE DIAGNOSTICO
                    A.COD_DIAG EXCDIA,                  --02 CODIGO DE DIAGNOSTICO
                    DES_LARG_DIAG EXCPRE                --03 DESCRIPCION DE DIAGNOSTICO
              FROM ASE_EXCLUSION A,
                   ASE_DIAGCIE10 B
             WHERE EXCCODAFI = PV_CODAFI 
               AND A.COD_DIAG= B.COD_DIAG 
               AND EXCESTEXC = '1';
                   
                   
    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_EXCLU_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL                                                   --7
                   FROM DUAL;
 
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_EXCLU_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL                                                    --7
                   FROM DUAL;
                   
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_EXCLU_AFI',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,
                   NULL                                                 --7
                   FROM DUAL;

    END PRC_EXCLU_AFI;                                

 FUNCTION FNC_UBIGEO (PV_CODPER    VARCHAR2)
   RETURN VARCHAR2
IS
   LV_UBI   VARCHAR2 (50);

BEGIN

      SELECT AGPCODUBI
        INTO LV_UBI
        FROM PBFAGEPER
       WHERE     AGPCODCIA = '001'
             AND AGPCODSUC = '001'
             AND AGPCODPER = PV_CODPER
             AND AGPTIELAG = '001'
             AND ROWNUM < 2;
             
      RETURN LV_UBI;
   
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'NULL';
   
   END FNC_UBIGEO;


--17 CONSULTA LISTA ESPECIALIDAD HOMOLOGADA 11/04/2019
   PROCEDURE PRC_LIS_ESP_HOMO (  
                                PO_IDTRAN            OUT     VARCHAR2,   --01 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                PO_FECTRA            OUT     VARCHAR2,   --02 FECHA DE LA TRANSACCI�N
                                PO_CODRES            OUT     VARCHAR2,   --03 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                PO_MENRES            OUT     VARCHAR2,   --04 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                RESULTSET            OUT     SYS_REFCURSOR) IS
      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;

   BEGIN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM ADMTABGEN 
             WHERE TBGCODCIA='001' AND TBGCODSUC='001' 
               AND TBGTABCOD='896'; 

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

        OPEN RESULTSET FOR
                SELECT  TBGDATCOD CODESP,            --01 CODIGO DE ESPECIALIDAD
                        TBGDATSEG DESESP             --02 DESCRIPCION ESPECIALIDAD
                  FROM ADMTABGEN 
                 WHERE TBGCODCIA='001' AND TBGCODSUC='001' 
                   AND TBGTABCOD='896';
                   
                   
    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_ESP_HOMO',
                        NULL
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT                                                    --1
                   NULL,
                   NULL                                                   --7
                   FROM DUAL;
 
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_ESP_HOMO',
                        NULL
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT                                                    --1
                   NULL,
                   NULL                                                    --7
                   FROM DUAL;
                   
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_ESP_HOMO',
                        NULL
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT                                                    --1
                   NULL,
                   NULL                                                 --7
                   FROM DUAL;
                   
    END PRC_LIS_ESP_HOMO;                               

--18 CONSULTA LISTA PACIENTES DETALLE 16/04/2019
   PROCEDURE PRC_LIS_PACIENTE( PV_CODAFI        IN      VARCHAR2, --01 CODIGO DE AFILIADO
                                   PO_IDTRAN        OUT     VARCHAR2, --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                   PO_FECTRA        OUT VARCHAR2,     --03 FECHA DE LA TRANSACCI�N
                                   PO_CODRES        OUT     VARCHAR2, --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                   PO_MENRES        OUT     VARCHAR2, --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                   RESULTSET        OUT     SYS_REFCURSOR)
   IS
      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER          := 0;
      PN_CONTAR  NUMBER          := 0;
      PN_RESER1  VARCHAR2(32767) := NULL;
      PN_RESER2  VARCHAR2(32767) := NULL;
      PV_USR     VARCHAR2(20)    := NULL;
      DATO_ERR   EXCEPTION;
    
    BEGIN


       SELECT LENGTH(PV_CODAFI) - LENGTH(REPLACE(PV_CODAFI,'|')) 
         INTO PN_CONTAR 
         FROM DUAL;
         
         PN_RESER1:=PV_CODAFI;
         
         PV_USR:=TRIM (USERENV ('SESSIONID'));
       
       FOR N  IN 1 .. PN_CONTAR LOOP

            PN_RESER2:=SISOPE.PCK_WS_CONTRATO_SERVICIO.FNC_LINEA (PN_RESER1, '|');

           INSERT INTO SISOPE.TMP_CODAFI
           VALUES(SUBSTR(PN_RESER1,1,17),PV_USR);

       END LOOP;
        
       COMMIT;

         SELECT SUM (CAN)
           INTO PN_VALOR
           FROM(
         SELECT COUNT(1) CAN
           FROM (SELECT *
                   FROM PBFPOBGRP PBF, PNADATMAE PNA, TMP_CODAFI AFI
                  WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND '1'= SUBSTR(AFI.CODAFI,1,1)
                        AND PBF.PGRCODGFA = SUBSTR(AFI.CODAFI,2,8)
                        AND PBF.PGRCODPNA = SUBSTR(AFI.CODAFI,10,8)
                        AND CODUSR=PV_USR
                                    ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
              WHERE A.PGRCODCIA = B.PCNCODCIA
                AND A.PGRCODSUC = B.PCNCODSUC
                AND A.PGRCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
        UNION   
       SELECT COUNT(1) CAN
           FROM (SELECT *
                   FROM GRPPOBGRP PBF, PNADATMAE PNA, TMP_CODAFI AFI
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND '2'= SUBSTR(AFI.CODAFI,1,1)
                        AND LPAD(GRPCODONC,8,'0') = SUBSTR(AFI.CODAFI,2,8)
                        AND GRPCODPNA = SUBSTR(AFI.CODAFI,10,8)
                        AND CODUSR=PV_USR                
                ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
          WHERE     A.GRPCODCIA = B.PCNCODCIA
                AND A.GRPCODSUC = B.PCNCODSUC
                AND A.GRPCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
                );


         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR
         SELECT 
                   CODAFI,                                              --01 CODIGO DE AFILIADO
                   APEPAT,                                              --02 APELLIDO PATERNO
                   APEMAT,                                              --03 APELLIDO MATERNO
                   NOMBR1,                                              --04 NOMBRE 1
                   NOMBR2,                                              --05 OTROS NOMBRES
                   TIPAFI,                                              --06 TIPO AFILIADO
                   TIPDOC,                                              --07 TIPO DOCUMENTO IDENTIDAD
                   NUMDOC,                                              --08 NUMERO DE DOCUMENTO
                   FECAFI,                                              --09 FECHA DE INGRESO DEL AFILIADO
                   FECREN,                                              --10 FECHA DE RENOVACION DEL AFILIADO
                   FECBAJ,                                              --11 FECHA DE BAJA DEL AFILIADO
                   ESPACI,                                              --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   FECNAC,                                              --13 FECHA DE NACIMIENTO DEL AFILIADO
                   EDAAFI                                               --14 EDAD DEL AFILIADO
            FROM(
            SELECT
                   CODAFI,
                   TRIM (PNADESAPA) APEPAT,                               --01 APELLIDO PATERNO
                   TRIM (PNADESAMA) APEMAT,                               --02 APELLIDO MATERNO
                   TRIM (PNANOMBRE) NOMBR1,                               --03 NOMBRE 1
                   TRIM (PNANOMBR2) NOMBR2,                               --04 OTROS NOMBRES
                   '1' || PGRCODGFA || PGRCODPNA CODAFIR,                 --05 CODIGO DE AFILIADO
                   'INDIVIDUAL' TIPAFI,                                   --06 TIPO AFILIADO
                   DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'),
                           NULL, 'DNI',
                           PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'))
                   TIPDOC,                                                --07 TIPO DOCUMENTO IDENTIDAD
                   PNADOCIDE NUMDOC,                                      --08 NUMERO DE DOCUMENTO
                   TO_CHAR(PGRFECAFL,'DD/MM/YYYY') FECAFI,                                      --09 FECHA DE INGRESO DEL AFILIADO
                   TO_CHAR(PGRFECURE,'DD/MM/YYYY') FECREN,                                      --10 FECHA DE RENOVACION DEL AFILIADO
                   TO_CHAR(PGRFECBAJ,'DD/MM/YYYY') FECBAJ,                                      --11 FECHA DE BAJA DEL AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.
                    FNC_ES_PACIENTE ('1' || PGRCODGFA || PGRCODPNA)
                      ESPACI,                                             --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC,                                      --13 FECHA DE NACIMIENTO DEL AFILIADO
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI                                                 --14 EDAD DEL AFILIADO
              FROM (SELECT                 
                           CODAFI,
                           PGRCODCIA,PGRCODSUC,PGRCODSAF,PGRCODGFA,PGRCODPNA,
                           PGRFECBAJ,PGRFECAFL,PGRFECCON,PGRFECURE,PGRFECGFF,
                           PGRCODCAT,PGRCODPRG,PNADESAPA,PNADESAMA,PNANOMBRE,
                           PNANOMBR2,PNAFECNAC,PNATIPDOC,PNACODSEX,PNADOCIDE
                   FROM PBFPOBGRP PBF, PNADATMAE PNA, TMP_CODAFI AFI
                  WHERE     PBF.PGRCODCIA = PNA.PNACODCIA
                        AND PBF.PGRCODSUC = PNA.PNACODSUC
                        AND PBF.PGRCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND '1'= SUBSTR(AFI.CODAFI,1,1)
                        AND PBF.PGRCODGFA = SUBSTR(AFI.CODAFI,2,8)
                        AND PBF.PGRCODPNA = SUBSTR(AFI.CODAFI,10,8)
                        AND CODUSR=PV_USR
                                    ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
              WHERE A.PGRCODCIA = B.PCNCODCIA
                AND A.PGRCODSUC = B.PCNCODSUC
                AND A.PGRCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
        UNION   
            SELECT    
                      CODAFI,
                      TRIM (PNADESAPA) APEPAT,                      --01 APELLIDO PATERNO
                      TRIM (PNADESAMA) APEMAT,                      --02 APELLIDO MATERNO    
                      TRIM (PNANOMBRE) NOMBR1,                      --03 NOMBRE 1
                      TRIM (PNANOMBR2) NOMBR2,                      --04 OTROS NOMBRES
                      '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA 
                      CODAFIR,                                      --05 CODIGO DE AFILIADO
                   'GRUPAL' TIPAFI,                                 --06 TIPO AFILIADO
                   DECODE (PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'),
                           NULL, 'DNI',
                           PQ_ADM_UTIL.FN_ADM_OBTIENETBLVAL ('001001',
                                                             '024',
                                                             PNATIPDOC,
                                                             'ABRDOCID'))
                      TIPDOC,                                      --07 TIPO DOCUMENTO IDENTIDAD
                   PNADOCIDE NUMDOC,                               --08 NUMERO DE DOCUMENTO
                   TO_CHAR(GRPFECING,'DD/MM/YYYY') FECAFI,                               --09 FECHA DE INGRESO DEL AFILIADO
                   NULL FECREN,                                    --10 FECHA DE RENOVACION DEL AFILIADO
                   TO_CHAR(GRPFECBAJ,'DD/MM/YYYY') FECBAJ,                               --11 FECHA DE BAJA DEL AFILIADO
                   PCK_WS_CONTRATO_SERVICIO.
                    FNC_ES_PACIENTE (
                      '2' || LPAD (GRPCODONC, 8, '0') || GRPCODPNA)
                      ESPACI,                                      --12 SE INDICAR� SI EL AFILIADO ES PACIENTE O NO CON UN SI/NO
                   TO_CHAR(PNAFECNAC,'DD/MM/YYYY') FECNAC,                               --13 FECHA DE NACIMIENTO DEL AFILIADO
                   FLOOR (MONTHS_BETWEEN (SYSDATE, PNAFECNAC) / 12) 
                   EDAAFI                                         --14 EDAD DEL AFILIADO
              FROM (SELECT                
                          CODAFI,
                          GRPCODCIA,GRPCODSUC,GRPCODONC,GRPCODPNA,GRPFECBAJ,
                          PNACODPNA,PNAFECNAC,GRPFAMGRP,GRPCATGRP,PNADOCIDE,
                          PNATIPDOC,PNADESAPA,PNADESAMA,PNANOMBRE,PNANOMBR2,
                          PNACODSEX,GRPFECCON,GRPFECING,GRPEMPGRP
                   FROM GRPPOBGRP PBF, PNADATMAE PNA, TMP_CODAFI AFI
                  WHERE     PBF.GRPCODCIA = PNA.PNACODCIA
                        AND PBF.GRPCODSUC = PNA.PNACODSUC
                        AND PBF.GRPCODPNA = PNA.PNACODPNA
                        AND PNACODCIA = '001'
                        AND PNACODSUC = '001'
                        AND '2'= SUBSTR(AFI.CODAFI,1,1)
                        AND LPAD(GRPCODONC,8,'0') = SUBSTR(AFI.CODAFI,2,8)
                        AND GRPCODPNA = SUBSTR(AFI.CODAFI,10,8)
                        AND CODUSR=PV_USR                
                ) A,
                (SELECT PCN.*, PSSTIPATR, PSSVALATR
                   FROM PCNDATMAE PCN, PCNSEGSIN PSEG
                  WHERE     PCN.PCNCODCIA = PSEG.PSSCODCIA
                        AND PCN.PCNCODSUC = PSEG.PSSCODSUC
                        AND PCN.PCNCODIGO = PSEG.PSSPCNCOD
                        AND PCN.PCNCODCIA = '001'
                        AND PCN.PCNCODSUC = '001'
                        AND PCN.PCNESTADO = '10'
                        AND TO_NUMBER (NVL (PCN.PCNESTPCN, '0')) < 2
                        AND PSSTIPATR = 'CON'
                        AND PSSFECVIG IS NULL) B
          WHERE     A.GRPCODCIA = B.PCNCODCIA
                AND A.GRPCODSUC = B.PCNCODSUC
                AND A.GRPCODPNA = B.PCNCODIGO
                AND PCNCODIGO IS NOT NULL
                );

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIAG_PACIENTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,                                                   
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                
                   FROM DUAL;                

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_EMP_CONTRATANTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,                                                   
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                
                   FROM DUAL;                
                   
                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_PACIENTE',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,                                                   
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                
                   FROM DUAL;                

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

    END PRC_LIS_PACIENTE;
                                       

--19 CONSULTA LISTA CLINICAS POR CODIGOS RENIPRESS 16/04/2019
   PROCEDURE PRC_LIS_CLINICAS_RE (
                               PV_CODCLI            IN      VARCHAR2, --01 CODIGO DE CLINICA O IPRESS
                               PO_IDTRAN            OUT     VARCHAR2, --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT VARCHAR2,     --03 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR)
   IS
      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER          := 0;
      PN_CONTAR  NUMBER          := 0;
      PN_RESER1  VARCHAR2(32767) := NULL;
      PN_RESER2  VARCHAR2(32767) := NULL;
      PV_USR     VARCHAR2(20)    := NULL;
      DATO_ERR   EXCEPTION;
    
    BEGIN


       SELECT LENGTH(PV_CODCLI) - LENGTH(REPLACE(PV_CODCLI,'|')) 
         INTO PN_CONTAR 
         FROM DUAL;
         
         PN_RESER1:=PV_CODCLI;
         
         PV_USR:=TRIM (USERENV ('SESSIONID'));
       
       FOR N  IN 1 .. PN_CONTAR LOOP

            PN_RESER2:=SISOPE.PCK_WS_CONTRATO_SERVICIO.FNC_LINEA (PN_RESER1, '|');

           INSERT INTO SISOPE.TMP_CODAFI
           VALUES(SUBSTR(PN_RESER1,1,8),PV_USR);

       END LOOP;
        
       COMMIT;

         SELECT COUNT (1)
           INTO PN_VALOR
           FROM TEDEF10_RUCXIPRESS A, TMP_CODAFI B
          WHERE     TIPO = 'IP'
                AND CODSU IS NOT NULL
                AND CODRENIPRESS IS NOT NULL
                AND CODRENIPRESS=CODAFI;

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR
                SELECT 
                        CODRENIPRESS CODCLI, --01 CODIGO CLINICA O IPRESS
                        DESABR NOMCLI        --02 NOMBRE CLINICA O IPRESS
                   FROM TEDEF10_RUCXIPRESS A, TMP_CODAFI B
                  WHERE     TIPO = 'IP'
                        AND CODSU IS NOT NULL
                        AND CODRENIPRESS IS NOT NULL
                        AND CODRENIPRESS=CODAFI;

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

   EXCEPTION
      WHEN DATO_ERR
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';

         OPEN RESULTSET FOR SELECT NULL, 
                                   NULL FROM DUAL;
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-' || PV_ERROR;

         OPEN RESULTSET FOR SELECT NULL, 
                                   NULL FROM DUAL;


                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-' || PV_ERROR;


         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (PO_IDTRAN,
                      PO_FECTRA,
                      PV_ERROR,
                      'PRC_LIS_CLINICAS_RE',
                       PV_CODCLI );

         COMMIT;

         OPEN RESULTSET FOR SELECT                                 --1
                                   NULL, 
                                   NULL FROM DUAL;       


                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

    END PRC_LIS_CLINICAS_RE;                               

--20 ACTIVACION PACIENTE
   PROCEDURE PRC_ACTIVA_PACIENTE (
                               PV_CODAFI            IN      VARCHAR2, --01 CODIGO DE AFILIADO
                               PV_CODUSU            IN      VARCHAR2, --02 USUARIO DE CLINICA
                               PV_CODPAIS           IN      VARCHAR2, --03 CODIGO DE PAIS
                               PV_CODUBI            IN      VARCHAR2, --04 CODIGO UBIGEO
                               PV_ESTCIV            IN      VARCHAR2, --05 ESTADO CIVIL  --SE ENCUENTRA EL TT_290
                               PV_CODPRO            IN      VARCHAR2, --06 CODIGO DE PROCEDENCIA --SE ENCUENTRA EN LA TT_268
                               PV_CODOCU            IN      VARCHAR2, --07 CODIGO DE OCUPACION --SE ENCUENTRA EN LA TT_308
                               PV_CODSUC            IN      VARCHAR2, --08 CODIGO SUCURSAL --SE ENCUENTRA EN LA TT_253
                               PV_CODDIA            IN      VARCHAR2, --09 CIE10 (SERAN SEPARADOS POR COMAS(,))
                               PV_FECDIA            IN      VARCHAR2, --10 FECHA CIE10 (SERAN SEPARADOS POR COMAS(,))
                               PV_CODMED            IN      VARCHAR2, --11 MEDICOS TRATANTES (SERAN SEPARADOS POR COMAS(,))
                               PV_FECMED            IN      VARCHAR2, --12 FECHA MEDICO (SERAN SEPARADOS POR COMAS(,))
                               PO_IDTRAN            OUT     VARCHAR2, --13 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT     VARCHAR2, --14 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --15 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2  --16 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               ) IS
    BEGIN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

        PO_CODRES:='0';
        PO_MENRES:='GRABACION EXITOSA';

    END PRC_ACTIVA_PACIENTE;

--21 LISTA MEDICO TRATANTE
   PROCEDURE PRC_LIS_MED_TRA_COD   (PV_CODMED            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                    PO_TOTAL             OUT     NUMBER,     --02 TOTAL REGISTROS
                                    PO_IDTRAN            OUT     VARCHAR2,   --03 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                    PO_FECTRA            OUT     VARCHAR2,   --04 FECHA DE LA TRANSACCI�N
                                    PO_CODRES            OUT     VARCHAR2,   --05 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                    PO_MENRES            OUT     VARCHAR2,   --06 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                    RESULTSET            OUT     SYS_REFCURSOR) IS

      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER          := 0;
      PN_CONTAR  NUMBER          := 0;
      PN_RESER1  VARCHAR2(32767) := NULL;
      PN_RESER2  VARCHAR2(32767) := NULL;
      PV_USR     VARCHAR2(20)    := NULL;
      DATO_ERR   EXCEPTION;
    
    BEGIN

       SELECT LENGTH(PV_CODMED) - LENGTH(REPLACE(PV_CODMED,'|')) 
         INTO PN_CONTAR 
         FROM DUAL;
         
         PN_RESER1:=PV_CODMED;
         
         PV_USR:=TRIM (USERENV ('SESSIONID'));
       
       FOR N  IN 1 .. PN_CONTAR LOOP

            PN_RESER2:=SISOPE.PCK_WS_CONTRATO_SERVICIO.FNC_LINEA (PN_RESER1, '|');

           INSERT INTO SISOPE.TMP_CODAFI
           VALUES(SUBSTR(PN_RESER1,1,8),PV_USR);

       END LOOP;
        
       COMMIT;
            
         SELECT COUNT (1)
           INTO PN_VALOR
              FROM PNADATMAE A, SOLBFINSTITUCIONES B,TMP_CODAFI AFI
              WHERE     PNACODCIA = PJUCODCIA
                    AND PNACODSUC = PJUCODSUC
                    AND PNACODPNA = PJUCODPJU
                    AND PNACODCIA='001' 
                    AND PNACODSUC='001'
                    AND PCK_MANT_PROV.FN_TIPO_PROV(PJUCODPJU)='MEDICO'
                    AND PNACODPNA =CODAFI;          

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

         OPEN RESULTSET FOR

                SELECT 
                CODMED,                                                 --01 CODIGOMT CODIGO DE MEDICO TRATANTE
                CMPTRA,                                                 --02 C�DIGO DE MEDICOS DEL PER�
                APEPAT,                                                 --03 APELLIDO PATERNO DEL MEDICO TRATANTE
                APEMAT,                                                 --04 APELLIDO MATERNO MEDICO TRATANTE
                NOMMED                                                  --05 NOMBRES DEL MEDICO TRATANTE
                FROM
                (SELECT 
                        DISTINCT
                        PNACODPNA CODMED,           --01 CODIGOMT CODIGO DE MEDICO TRATANTE
                        CODCMPMED CMPTRA,                  --02 C�DIGO DE MEDICOS DEL PER�
                        TRIM(PNADESAPA) APEPAT,     --03 APELLIDO PATERNO DEL MEDICO TRATANTE
                        TRIM(PNADESAMA) APEMAT,     --04 APELLIDO MATERNO MEDICO TRATANTE
                        TRIM(PNANOMBRE)||' '||
                        TRIM(PNANOMBR2) NOMMED      --05 NOMBRES DEL MEDICO TRATANTE
              FROM PNADATMAE A, SOLBFINSTITUCIONES B,TMP_CODAFI AFI
              WHERE     PNACODCIA = PJUCODCIA
                    AND PNACODSUC = PJUCODSUC
                    AND PNACODPNA = PJUCODPJU
                    AND PNACODCIA='001' 
                    AND PNACODSUC='001'
                    AND PCK_MANT_PROV.FN_TIPO_PROV(PJUCODPJU)='MEDICO'
                    AND PNACODPNA = AFI.CODAFI
                    );

      DELETE SISOPE.TMP_CODAFI
       WHERE CODUSR = PV_USR;
      COMMIT;


    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_MED_TRA_COD',
                        PV_CODMED
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT 
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                   --7
                   FROM DUAL;

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_MED_TRA_COD',
                        PV_CODMED
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT 
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                    --7
                   FROM DUAL;

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;
                 
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LIS_MED_TRA_COD',
                        PV_CODMED
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT 
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                 --7
                   FROM DUAL;

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;
      
   END PRC_LIS_MED_TRA_COD;                                  

--22 DIAGNOSTICO POR PACIENTE
   PROCEDURE PRC_DIAG_PACIENTE_DET( PV_CODAFI            IN      VARCHAR2,   --01 CODIGO DE AFILIADO
                                    PO_IDTRAN            OUT     VARCHAR2,   --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                                    PO_FECTRA            OUT     VARCHAR2,   --03 FECHA DE LA TRANSACCI�N
                                    PO_CODRES            OUT     VARCHAR2,   --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                                    PO_MENRES            OUT     VARCHAR2,   --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                                    RESULTSET            OUT     SYS_REFCURSOR) IS

      PV_ERROR   VARCHAR2 (4000) := NULL;

      PN_VALOR   NUMBER          := 0;
      PN_CONTAR  NUMBER          := 0;
      PN_RESER1  VARCHAR2(32767) := NULL;
      PN_RESER2  VARCHAR2(32767) := NULL;
      PV_USR     VARCHAR2(20)    := NULL;
      DATO_ERR   EXCEPTION;
    
    BEGIN
--
--       SELECT LENGTH(PV_CODAFI) - LENGTH(REPLACE(PV_CODAFI,'|')) 
--         INTO PN_CONTAR 
--         FROM DUAL;
--         
--         PN_RESER1:=PV_CODAFI;
--         
--         PV_USR:=TRIM (USERENV ('SESSIONID'));
--       
--       FOR N  IN 1 .. PN_CONTAR+1 LOOP
--
--            PN_RESER2:=SISOPE.PCK_WS_CONTRATO_SERVICIO.FNC_LINEA (PN_RESER1, '|');
--
--           INSERT INTO SISOPE.TMP_CODAFI
--           VALUES(SUBSTR(PN_RESER1,1,17),PV_USR);
--
--       END LOOP;
--        
--       COMMIT;

         SELECT COUNT (1)
           INTO PN_VALOR
                   FROM PCNDGNMAE A, DGNDATMAE B,ADMTABGEN C --, TMP_CODAFI D
                  WHERE PDGCODCIA=DGNCODCIA AND PDGCODSUC=DGNCODSUC 
                    AND PDGCODIGO=DGNCODIGO AND PDGPCNCOD=SUBSTR(PV_CODAFI,10,8)
                    AND DGNCODCIA=TBGCODCIA AND DGNCODSUC=TBGCODSUC 
                    AND TBGTABCOD = '464'    
                    AND TBGDATCOD=DGNCODGRU;
                    --AND CODUSR=PV_USR;          

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;


         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

         OPEN RESULTSET FOR    

              SELECT 
                   DGNCODREF CODDIA,         --01 CODIGO DIAGNOSTICO
                   DGNDESCRI NOMDIA,         --02 NOMBRE DIAGNOSTICO
                   DGNCODGRU CODGRU,         --03 CODIGO GRUPO
                   TRIM (TBGDATSEG) NOMGRU   --04 NOMBRE GRUPO
                   FROM PCNDGNMAE A, DGNDATMAE B,ADMTABGEN C--, TMP_CODAFI D
                  WHERE PDGCODCIA=DGNCODCIA AND PDGCODSUC=DGNCODSUC 
                    AND PDGCODIGO=DGNCODIGO AND PDGPCNCOD=SUBSTR(PV_CODAFI,10,8)
                    AND DGNCODCIA=TBGCODCIA AND DGNCODSUC=TBGCODSUC 
                    AND TBGTABCOD = '464'    
                    AND TBGDATCOD=DGNCODGRU;
                    --AND CODUSR=PV_USR; 


--                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
--                COMMIT;

    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIA_PAC_DET',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT 
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                   --7
                   FROM DUAL;

--                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
--                COMMIT;

      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIA_PAC_DET',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT 
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                    --7
                   FROM DUAL;

--                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
--                COMMIT;

      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIA_PAC_DET',
                        PV_CODAFI
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT 
                   NULL,
                   NULL,
                   NULL,
                   NULL                                                 --7
                   FROM DUAL;

--                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
--                COMMIT;
        
    END PRC_DIAG_PACIENTE_DET;                                 

--23 LISTA PROGRAMAS 14/05/2019
   PROCEDURE PRC_LISTA_PROGRAMA (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA 1. TODOS, 2. POR CODIGO, 3. POR DESCRIPCION
                               PV_CODPRO            IN      VARCHAR2, --04 CODIGO PROGRAMA
                               PV_NOMPRO            IN      VARCHAR2, --05 NOMBRE PROGRAMA
                               PO_TOTAL             OUT     NUMBER,   --06 TOTAL REGISTROS
                               PO_IDTRAN            OUT     VARCHAR2, --07 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT     VARCHAR2, --08 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --09 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --10 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR) IS

      PV_ERROR   VARCHAR2 (4000) := NULL;
      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;

    BEGIN
    
        IF    PV_TIPBUS='1' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM PRGDATMAE A               
             WHERE PRGCODCIA='001'
               AND PRGCODSUC='001'
               --AND PRGCODPRG = PV_CODPRO
               AND PRGESTREG='1';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT  
                    RN,                                 --01 NUMERO DE REGISTRO            
                    CODPRO,                             --02 CODIGO PROGRAMA
                    NOMPRO                              --03 NOMBRE PROGRAMA
            FROM(
            SELECT  
            ROW_NUMBER() OVER (ORDER BY PRGCODPRG,PRGDESPRG) RN,    --01 NUMERO DE REGISTRO            
                    PRGCODPRG   CODPRO,                             --02 CODIGO PROGRAMA
                    PRGDESPRG   NOMPRO                              --03 NOMBRE PROGRAMA
              FROM PRGDATMAE A               
             WHERE PRGCODCIA='001'
               AND PRGCODSUC='001'
               --AND PRGCODPRG = PV_CODPRO
               AND PRGESTREG='1'
                )
            WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;
        
        ELSIF PV_TIPBUS='2' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM PRGDATMAE A               
             WHERE PRGCODCIA='001'
               AND PRGCODSUC='001'
               AND PRGCODPRG = PV_CODPRO
               AND PRGESTREG='1';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT  
                    RN,                                 --01 NUMERO DE REGISTRO            
                    CODPRO,                             --02 CODIGO PROGRAMA
                    NOMPRO                              --03 NOMBRE PROGRAMA
            FROM(
            SELECT  
            ROW_NUMBER() OVER (ORDER BY PRGCODPRG,PRGDESPRG) RN,    --01 NUMERO DE REGISTRO            
                    PRGCODPRG   CODPRO,                             --02 CODIGO PROGRAMA
                    PRGDESPRG   NOMPRO                              --03 NOMBRE PROGRAMA
              FROM PRGDATMAE A               
             WHERE PRGCODCIA='001'
               AND PRGCODSUC='001'
               AND PRGCODPRG = PV_CODPRO
               AND PRGESTREG='1'
                )
            WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;

        ELSIF PV_TIPBUS='3' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM PRGDATMAE A               
             WHERE PRGCODCIA='001'
               AND PRGCODSUC='001'
               AND PRGDESPRG LIKE '%'||PV_NOMPRO||'%'
               AND PRGESTREG='1';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT  
                    RN,                                 --01 NUMERO DE REGISTRO            
                    CODPRO,                             --02 CODIGO PROGRAMA
                    NOMPRO                              --03 NOMBRE PROGRAMA
            FROM(
            SELECT  
            ROW_NUMBER() OVER (ORDER BY PRGCODPRG,PRGDESPRG) RN,    --01 NUMERO DE REGISTRO            
                    PRGCODPRG   CODPRO,                             --02 CODIGO PROGRAMA
                    PRGDESPRG   NOMPRO                              --03 NOMBRE PROGRAMA
              FROM PRGDATMAE A               
             WHERE PRGCODCIA='001'
               AND PRGCODSUC='001'
               AND PRGDESPRG LIKE '%'||PV_NOMPRO||'%'
               AND PRGESTREG='1'
                )
            WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;

        ELSE

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
            
              OPEN RESULTSET FOR        
            SELECT NULL,
                   NULL
                   FROM DUAL;
        
        END IF;
    EXCEPTION
      WHEN DATO_ERR
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';

         OPEN RESULTSET FOR
            SELECT NULL,
                   NULL,
                   NULL
              FROM DUAL;
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;

         OPEN RESULTSET FOR
            SELECT 
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;


         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LISTA_PROGRAMA',
                           PV_TIPBUS
                        || '-'
                        || PV_CODPRO
                        || '-'
                        || PV_NOMPRO);

         COMMIT;

         OPEN RESULTSET FOR
            SELECT
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;

  END PRC_LISTA_PROGRAMA;                             

--24 LISTA PLANES 14/05/2019
   PROCEDURE PRC_LISTA_PLANES (
                               PN_INI               IN      NUMBER,   --01 REGISTRO INICIO
                               PN_FIN               IN      NUMBER,   --02 REGISTRO FIN
                               PV_TIPBUS            IN      VARCHAR2, --03 TIPO DE BUSQUEDA 1. TODOS, 2. POR CODIGO, 3. POR DESCRIPCION
                               PV_CODPLA            IN      VARCHAR2, --04 CODIGO PLANES
                               PV_NOMPLA            IN      VARCHAR2, --05 NOMBRE PLANES
                               PO_TOTAL             OUT     NUMBER,   --06 TOTAL REGISTROS
                               PO_IDTRAN            OUT     VARCHAR2, --07 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT     VARCHAR2, --08 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --09 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --10 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR) IS

      PV_ERROR   VARCHAR2 (4000) := NULL;
      PN_VALOR   NUMBER := 0;
      DATO_ERR   EXCEPTION;

    BEGIN
    
        IF    PV_TIPBUS='1' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM ASE_PLAN A
             WHERE EST_PLAN='S';              

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT  
                    RN,                                 --01 NUMERO DE REGISTRO            
                    CODPLA,                             --02 CODIGO PLAN
                    NOMPLA                              --03 NOMBRE PLAN
            FROM(
            SELECT  
            ROW_NUMBER() OVER (ORDER BY CODPLAN,DES_PLAN) RN,    --01 NUMERO DE REGISTRO            
                    CODPLAN   CODPLA,                             --02 CODIGO PLAN
                    DES_PLAN  NOMPLA                              --03 NOMBRE PLAN
              FROM ASE_PLAN A
             WHERE EST_PLAN='S'               
                )
            WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;

        ELSIF PV_TIPBUS='2' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM ASE_PLAN A
             WHERE CODPLAN =PV_CODPLA
               AND EST_PLAN='S';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT  
                    RN,                                 --01 NUMERO DE REGISTRO            
                    CODPLA,                             --02 CODIGO PLAN
                    NOMPLA                              --03 NOMBRE PLAN
            FROM(
            SELECT  
            ROW_NUMBER() OVER (ORDER BY CODPLAN,DES_PLAN) RN,    --01 NUMERO DE REGISTRO            
                    CODPLAN   CODPLA,                             --02 CODIGO PLAN
                    DES_PLAN  NOMPLA                              --03 NOMBRE PLAN
              FROM ASE_PLAN A
             WHERE CODPLAN =PV_CODPLA
               AND EST_PLAN='S'             
                )
            WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;

        ELSIF PV_TIPBUS='3' THEN

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM ASE_PLAN A
             WHERE DES_PLAN LIKE '%'||PV_NOMPLA||'%'
               AND EST_PLAN='S';

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';
         PO_TOTAL  := PN_VALOR;

              OPEN RESULTSET FOR        
            SELECT  
                    RN,                                 --01 NUMERO DE REGISTRO            
                    CODPLA,                             --02 CODIGO PLAN
                    NOMPLA                              --03 NOMBRE PLAN
            FROM(
            SELECT  
            ROW_NUMBER() OVER (ORDER BY CODPLAN,DES_PLAN) RN,    --01 NUMERO DE REGISTRO            
                    CODPLAN   CODPLA,                             --02 CODIGO PLAN
                    DES_PLAN  NOMPLA                              --03 NOMBRE PLAN
              FROM ASE_PLAN A
             WHERE DES_PLAN LIKE '%'||PV_NOMPLA||'%' 
               AND EST_PLAN='S' 
                )
            WHERE RN BETWEEN PN_INI AND PN_FIN  ORDER BY RN;

        ELSE

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
            
              OPEN RESULTSET FOR        
            SELECT NULL,
                   NULL
                   FROM DUAL;
        
        END IF;
    EXCEPTION
      WHEN DATO_ERR
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';

         OPEN RESULTSET FOR
            SELECT NULL,
                   NULL,
                   NULL
              FROM DUAL;
      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL INTO PO_IDTRAN FROM DUAL;

         PO_FECTRA := TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS');
         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;

         OPEN RESULTSET FOR
            SELECT 
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;
      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '-1';
         PV_ERROR := SQLERRM;
         PO_MENRES := 'NO EXISTEN DATOS-'||PV_ERROR;


         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_LISTA_PLANES',
                           PV_TIPBUS
                        || '-'
                        || PV_CODPLA
                        || '-'
                        || PV_NOMPLA);

         COMMIT;

         OPEN RESULTSET FOR
            SELECT
                   NULL,
                   NULL,
                   NULL
              FROM DUAL;

    END PRC_LISTA_PLANES;                               

   PROCEDURE PRC_LISTA_DIAGNOS_DET (
                               PV_CODDIA            IN      VARCHAR2, --01 CODIGO DIAGNOSTICO
                               PO_IDTRAN            OUT     VARCHAR2, --02 ID DE TRANSACCI�N PARA EL SEGUIMIENTO DE LA PETICI�N PROCESADA
                               PO_FECTRA            OUT     VARCHAR2, --03 FECHA DE LA TRANSACCI�N
                               PO_CODRES            OUT     VARCHAR2, --04 CODIGO DE RESPUESTA DE PETICI�N PROCESADA
                               PO_MENRES            OUT     VARCHAR2, --05 MENSAJE DE RESPUESTA DE LA PETICI�N PROCESADA
                               RESULTSET            OUT     SYS_REFCURSOR)
    IS
      PV_ERROR   VARCHAR2 (4000) := NULL;
      PN_CONTROL NUMBER          := 0;
      PN_VALOR   NUMBER          := 0;
      PN_CONTAR  NUMBER          := 0;
      PN_RESER1  VARCHAR2(32767) := NULL;
      PN_RESER2  VARCHAR2(32767) := NULL;
      PV_USR     VARCHAR2(20)    := NULL;
      DATO_ERR   EXCEPTION;
    
    BEGIN

       SELECT LENGTH(PV_CODDIA) - LENGTH(REPLACE(PV_CODDIA,'|')) 
         INTO PN_CONTAR 
         FROM DUAL;
         
         PN_RESER1:=PV_CODDIA;
         
         PV_USR:=TRIM (USERENV ('SESSIONID'));
       
       FOR N  IN 1 .. PN_CONTAR LOOP

            PN_RESER2:=SISOPE.PCK_WS_CONTRATO_SERVICIO.FNC_LINEA (PN_RESER1, '|');

                INSERT INTO SISOPE.TMP_CODAFI
                VALUES(PN_RESER2,PV_USR);

       END LOOP;
        
       COMMIT;

            SELECT COUNT(1)
              INTO PN_VALOR
              FROM DGNDATMAE A,
                   ADMTABGEN B,
                   TMP_CODAFI C               
             WHERE DGNCODCIA=TBGCODCIA
               AND DGNCODSUC=TBGCODSUC
               AND DGNCODCIA='001'
               AND TBGTABCOD='464'
               AND DGNCODGRU=TBGDATCOD
               AND DGNCODREF=CODAFI;

         IF PN_VALOR < 1
         THEN
            RAISE DATO_ERR;
         END IF;

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '0';
         PO_MENRES := 'DATOS CORRECTOS';

              OPEN RESULTSET FOR        
            SELECT  
                    DGNCODREF   CODDIA,                             --01 CODIGO DIAGNOSTICO
                    DGNDESCRI   NOMDIA,                             --02 NOMBRE DIAGNOSTICO
                    DGNCODGRU   CODGRU,                             --03 CODIGO GRUPO DIAGNOSTICO
                    TRIM(TBGDATSEG) NOMGRU                          --04 NOMBRE GRUPO DIAGNOSTICO
              FROM DGNDATMAE A,
                   ADMTABGEN B,               
                   TMP_CODAFI C               
             WHERE DGNCODCIA=TBGCODCIA
               AND DGNCODSUC=TBGCODSUC
               AND DGNCODCIA='001'
               AND TBGTABCOD='464'
               AND DGNCODGRU=TBGDATCOD
               AND DGNCODREF=CODAFI;

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

    EXCEPTION
    
      WHEN DATO_ERR
      THEN

         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIAG_DET',
                        SUBSTR(PV_CODDIA,1,1000)
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL                                                   --4
                   FROM DUAL;

                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;

      WHEN NO_DATA_FOUND
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIAG_DET',
                        SUBSTR(PV_CODDIA,1,1000)
                        );

         COMMIT;


         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL                                                   --4
                   FROM DUAL;
                   
                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;


      WHEN OTHERS
      THEN
         SELECT SISOPE.SEQ_CONAFI.NEXTVAL,
                TO_CHAR (SYSDATE, 'DD/MM/RRRR HH24:MI:SS')
           INTO PO_IDTRAN, PO_FECTRA
           FROM DUAL;

         PO_CODRES := '1';
         PO_MENRES := 'NO EXISTEN DATOS';
         PV_ERROR := SQLERRM;

         INSERT INTO SISOPE.EVENTO_PCK_WS_CS (IDTRANSACCION,
                                              FECHA,
                                              DESCRIPCION,
                                              PROCEDIMIENTO,
                                              DATOS)
              VALUES (
                        PO_IDTRAN,
                        PO_FECTRA,
                        PV_ERROR,
                        'PRC_DIAG_DET',
                        SUBSTR(PV_CODDIA,1,1000)
                        );

         COMMIT;

         OPEN RESULTSET FOR
            SELECT NULL,                                                   --1
                   NULL,                                                   --2
                   NULL,                                                   --3
                   NULL                                                   --4
                   FROM DUAL;
   
                DELETE SISOPE.TMP_CODAFI WHERE CODUSR=PV_USR;
                COMMIT;
        
    END PRC_LISTA_DIAGNOS_DET;                              

END PCK_WS_CONTRATO_SERVICIO;
/

