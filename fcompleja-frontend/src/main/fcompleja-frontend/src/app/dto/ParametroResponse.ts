import { Parametro } from '../dto/Parametro';

export class ParametroResponse {
  filtroParametro: Parametro[];
  codigoResultado: any;
  mensageResultado: string;
}
