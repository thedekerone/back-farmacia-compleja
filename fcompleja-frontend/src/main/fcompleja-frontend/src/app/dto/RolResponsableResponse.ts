import { RolResponsable } from '../dto/RolResponsable';

export class RolResponsableResponse {
  codigoResultado: any;
  mensageResultado: String;
  rolResponsable: RolResponsable[];
}
