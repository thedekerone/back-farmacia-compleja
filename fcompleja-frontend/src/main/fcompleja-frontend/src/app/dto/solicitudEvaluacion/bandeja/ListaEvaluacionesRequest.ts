export class ListaEvaluacionesRequest {
  codigoEvaluacion: string;
  codigoClinica: string;
  codigoPaciente: string;
  fechaInicio: string;
  fechaFin: string;
  numeroScgSolben: any;
  estadoSolicitudEvaluacion: string;
  tipoScgSolben: string;
  rolResponsable: number;
  estadoScgSolben: string;
  correoLiderTumor: string;
  numeroCartaGarantia: any;
  correoCmac: string;
  autorizadorPertenencia: number;
  tipoEvaluacion: string;
  liderTumor: number;
  reunionCmac: string;
  horaCmac: string;
  flagFiltro: string;
  flacgClinica: string;
}
