import { LineaTratamiento } from '../bandeja/LineaTratamiento';

export class ListaLineaTratamientoResponse {
  lista: LineaTratamiento[];
  codigo: any;
  mensaje: String;
}
