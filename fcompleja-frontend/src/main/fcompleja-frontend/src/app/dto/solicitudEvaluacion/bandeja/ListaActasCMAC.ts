import { ActasMac } from './ActasMac';
import { Participantes } from './Participantes';

export class ListaActasCMAC {
  fecha: String;
  hora: String;
  listaActasMAC: ActasMac[];
  listaParticipantes: Participantes[];
  codArchivo: number;
  codigoActa: any;
}
