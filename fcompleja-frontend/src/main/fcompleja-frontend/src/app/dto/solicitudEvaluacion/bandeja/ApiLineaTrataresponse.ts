import { ListaLineaTratamientoResponse } from '../bandeja/ListaHistLineaTrtatamientoResponse'

export class ApiLineaTrataresponse {
  status: string;
  message: string;
  response: ListaLineaTratamientoResponse;
}
