import { Input } from '@angular/core'
import { ListaEvaluaciones } from '../bandeja/ListaEvaluaciones'

export class ListaEvaluacionesResponse {
    listabandeja: ListaEvaluaciones[];
    codigoResultado: any;
    mensajeResultado: string;
}
