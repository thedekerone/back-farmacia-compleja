import { EvolucionMarcadorRequest } from './EvolucionMarcadorRequest';

export class EvolucionRequest {
  codEvolucion: number;
  nroEvolucion: number;
  nroDescEvolucion: string;
  codMonitoreo: number;
  codMac: number;
  pResEvolucion: number;
  codHistLineaTrat: number;
  fecMonitoreo: Date;
  fecProxMonitoreo: Date;
  pMotivoInactivacion: number;
  fecInactivacion: Date;
  observacion: string;
  pTolerancia: number;
  pToxicidad: number;
  pGrado: number;
  pRespClinica: number;
  pAtenAlerta: number;
  existeToxicidad: any;
  listaMarcadores: EvolucionMarcadorRequest[];
  estado: number;
  usuarioCrea: string;
  fechaCrea: Date;
  usuarioModif: string;
  fechaModif: Date;
  pEstadoMonitoreo: number;
  fecUltimoConsumo: Date;
  codSolEvaluacion: number;

  descCodSolEvaluacion: string;
}
