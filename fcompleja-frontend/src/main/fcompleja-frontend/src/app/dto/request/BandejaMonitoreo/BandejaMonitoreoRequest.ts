export class BandejaMonitoreoRequest {
  codigoPaciente: string;
  estadoMonitoreo: number;
  codigoClinica: string;
  fechaMonitoreo: string;
  codigoResponsableMonitoreo: string;
}
