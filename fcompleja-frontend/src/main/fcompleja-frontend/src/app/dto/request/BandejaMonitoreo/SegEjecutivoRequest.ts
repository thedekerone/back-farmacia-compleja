export class SegEjecutivoRequest {
    codSegEjecutivo: number;
    codMonitoreo: number;
    codEjecutivoMonitoreo: number;
    nomEjecutivoMonitoreo: number;
    fechaRegistro: Date;
    pEstadoSeguimiento: number;
    descEstadoSeguimiento: string;
    detalleEvento: string;
    vistoRespMonitoreo: number;
    usuariocrea: string;
    fechacrea: Date;
    usuarioModif: string;
    fechaModif: Date;
    pEstadoMonitoreo: number;
}
