import { ListaEvaluaciones } from '../solicitudEvaluacion/bandeja/ListaEvaluaciones';
export class ProgramacionCmacRequest {
  fecha: string;
  hora: string;
  codEvaluacion: string;
  codArchivo: number;
  codReporteActaEscaneada: string;
  listaEvaluacion: ListaEvaluaciones[];
}
