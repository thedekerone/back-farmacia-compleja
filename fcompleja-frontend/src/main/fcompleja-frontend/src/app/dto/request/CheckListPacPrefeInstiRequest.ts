import { PdfEvaluacion } from '../reporte/PdfEvaluacion';

export class CheckListPacPrefeInstiRequest{
    codSolicitudEvaluacion: number;
    cumpleCheckListPerfilPac: number;
    cumplePrefeInsti: number;
    codMac: number;
    pertinencia: number;
    condicionPaciente: number;
    tiempoUso: number;
    resultadoAutorizador: number;
    comentario: string;
    codigoRolUsuario: number;
    codigoUsuario: number;
    flagGrabar: number;
    codReporte: number;
    informeAutorizador: PdfEvaluacion;
}
