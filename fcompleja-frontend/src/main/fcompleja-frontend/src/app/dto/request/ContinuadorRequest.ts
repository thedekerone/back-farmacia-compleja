export class ContinuadorRequest {
  codSolicitudEvaluacion: number;
  resultadoAutorizador: number;
  codResponsableMonitoreo: number;
  codResultadoMonitoreo: number;
  comentario: string;
  nroTareaMonitoreo: string;
  fechaMonitoreo: string;
  documento: string;
  codigoRolUsuario: number;
  fechaEstado: string;
  codigoUsuario: number;
  codigoRolMonitoreo: number;
}
