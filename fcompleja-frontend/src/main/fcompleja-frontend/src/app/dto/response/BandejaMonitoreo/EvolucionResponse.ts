export class EvolucionResponse {
  codEvolucion: number;
  nroDescEvolucion: string;
  codMonitoreo: number;
  codDescMonitoreo: string;
  codMac: number;
  pResEvolucion: number;
  descResEvolucion: string;
  codHistLineaTrat: number;
  fecMonitoreo: Date;
  fecProxMonitoreo: Date;
  pMotivoInactivacion: number;
  fecInactivacion: Date;
  observacion: string;
  pTolerancia: number;
  descTolerancia: string;
  pToxicidad: number;
  descToxicidad: string;
  pGrado: number;
  descGrado: string;
  pRespClinica: number;
  descRespClinica: string;
  pAtenAlerta: number;
  descAtenAlerta: string;
  existeToxicidad: string;
  fUltimoConsumo: Date;
  ultimaCantConsumida: number;
  estado: number;
  usuarioCrea: string;
  fechaCrea: Date;
  usuarioModif: string;
  fechaModif: Date;
  pEstadoMonitoreo: number;
}