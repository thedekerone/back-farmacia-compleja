import { Input } from '@angular/core'
import { ListaIndicadorResponse} from '../response/ListaIndicadorResponse'

export class ListadoIndicadoresResponse{
    listaIndicador: ListaIndicadorResponse[];
    codigoResultado: any;
    mensajeResultado: String;
}