import { Input } from '@angular/core'
import { listaCriterioInclusionResponse } from '../response/listaCriterioInclusionResponse'
import { listaCriterioExclusionResponse } from '../response/listaCriterioExclusionResponse'

export class ListaIndicadorResponse{
    codigo: any;
    descripcion: String;
    selected:boolean;
    listaCriterioInclusion:listaCriterioInclusionResponse[];
    listaCriterioExclusion:listaCriterioExclusionResponse[];
}