import { Input } from '@angular/core'
import { ListUsrRol } from '../dto/ListUsrRol'
import { AudiResponse } from './AudiResponse';

export class UsrRolResponse {
  audiResponse: AudiResponse;
  dataList: ListUsrRol[];

}
