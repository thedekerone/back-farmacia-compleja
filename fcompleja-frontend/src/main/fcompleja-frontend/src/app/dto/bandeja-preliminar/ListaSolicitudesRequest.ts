export class ListaSolicitudesRequest {
  codigoPreliminar: any;
  codigoPaciente: string;
  nombrePaciente: string;
  codigoScgSolben: string;
  tipoScgSolben: string;
  codigoClinica: string;
  nombreClinica: string;
  fechaInicio_pre: Date;
  fechaFinPre: Date;
  estadoPre: string;
  autorizadorPertenencia: number;
  flagFiltro: string;
  flacgClinica: string;
  index:number;
  size:number;
}
