import { DetallePreeliminar } from '../dto/DetallePreeliminar';

export class ListaSolicitudesResponse {
  detallePreeliminar: DetallePreeliminar[];
  message: string;
}
