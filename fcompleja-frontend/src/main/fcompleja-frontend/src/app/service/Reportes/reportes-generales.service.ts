import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publish';
import { ReporteIndicadoresRequest } from 'src/app/dto/configuracion/ReporteIndicadoresRequest';
import { WsResponse } from 'src/app/dto/WsResponse';
import { Observable } from 'rxjs';
import { webServiceEndpoint } from 'src/app/common';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable({
  providedIn: 'root'
})
export class ReportesGeneralesService {

  constructor(private http: HttpClient) { }

  public generarReporteSolicitudAutorizaciones(request: ReporteIndicadoresRequest): Observable<WsResponse> {
    return this.http.post<WsResponse>(`${webServiceEndpoint}pub/generarReporteSolicitudAutorizaciones`, request, {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    });
  }
  /**
   * Proposito: Reportes Generales Autorizador
   * @param request
   */
  public generarReportesGenerales(request: ReporteIndicadoresRequest): Observable<WsResponse> {
    return this.http.post<WsResponse>(`${webServiceEndpoint}pub/generarReportesGenerales`, request, {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    });
  }

  /**
   * Proposito: Reportes Generales Monitoreo
   * @param request
   */
  public generarReporteMonitoreo(request: ReporteIndicadoresRequest): Observable<WsResponse> {
    return this.http.post<WsResponse>(`${webServiceEndpoint}pub/generarReporteMonitoreo`, request, {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    });
  }
}
