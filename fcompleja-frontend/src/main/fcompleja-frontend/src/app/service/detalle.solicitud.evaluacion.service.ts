import { Injectable, Pipe } from '@angular/core';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ApiResponse } from '../dto/bandeja-preliminar/detalle-preliminar/ApiResponse';
import { ApiOutResponse } from '../dto/response/ApiOutResponse';

import { webServiceEndpoint } from '../common';
import { DocuHistPacienteResponse } from '../dto/response/DocuHistPacienteResponse';
import { ApiListaIndicador } from '../dto/response/ApiListaIndicador';
import { InsertLineaTratamientoResponse } from '../dto/response/insertaLineaTratamientoResponse';
import { WsResponse } from '../dto/WsResponse';
import { DatePipe } from '@angular/common';
import { CondicionBasalPacienteRequest } from '../dto/request/CondicionBasalPacienteRequest';
import { ResultadoBasalRequest } from '../dto/request/ResultadoBasalRequest';
import { CheckListRequisitoRequest } from '../dto/request/CheckListRequisitoRequest';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { InformeSolEvaReporteRequest } from '../dto/solicitudEvaluacion/detalle/InformacionScgEvaRequest';
import { CheckListPacPrefeInstiRequest } from '../dto/request/CheckListPacPrefeInstiRequest';
import { SolicitudEvaluacionRequest } from '../dto/request/SolicitudEvaluacionRequest';
import { MonitoreoEvolucionRequest } from '../dto/request/BandejaEvaluacion/MonitoreoEvolucionRequest';
import { ContinuadorRequest } from '../dto/request/ContinuadorRequest';

@Injectable({
  providedIn: 'root'
})
export class DetalleSolicitudEvaluacionService {

  httpHeaders: HttpHeaders;
  constructor(private http: HttpClient, private datePipe: DatePipe) { }

  /**
   * Proposito : Detalle de la Solicitud de Evaluacion Solben
   * @param request
   */
  public consultarInformacionScgEva(request: InformeSolEvaReporteRequest): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/consultarInformacionScgEva`, request, httpOptions);
  }

  /**
   * @param request
   * Proposito : Guardar Linea de Tratamiento / Preferencia Institucional Paso 1
   */
  public insertarLineaTratamiento(request): Observable<ApiOutResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    };
    return this.http.post<ApiOutResponse>(`${webServiceEndpoint}api/insertarLineaTratamiento`, request, httpOptions);
  }
  /**
   * @param request codCondicionCancer
   * codSubcondicionCancer
   * nroLineaTratamiento
   * codGrupoDiagnostico
   * Proposito : Se debe traer las subcondiciones correspondientes a los parametros de entrada Paso 1
   */
  public consultarSubCondicionCancer(request): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.random() + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/consultarSubCondicionCancer`, request, httpOptions);
  }

  /**
   * Proposito : consultar la Preferencia Institucional de la Mac Tratamiento PASO 1
   * @param request
   */
  public consultarPreferenciaInsti(request): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.random() + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/consultarPreferenciaInsti`, request, httpOptions);
  }
  public conInsActPreferenciaInstiPre(request): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.random() + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/conInsActPreferenciaInstiPre`, request, httpOptions);
  }

  public insertarHistLineaTrat(request): Observable<InsertLineaTratamientoResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    };
    return this.http.post<InsertLineaTratamientoResponse>(`${webServiceEndpoint}api/insertarHistLineaTrat`, request, httpOptions);
  }

  public consultarCheckListRequisito(request): Observable<DocuHistPacienteResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    };
    return this.http.post<DocuHistPacienteResponse>(`${webServiceEndpoint}api/consultarCheckListRequisito`, request, httpOptions);
  }
  /**
   * Proposito : Precarga Condicion Basal del Paciente - Paso 3
   * @param request
   * DTO : ResultadoBasalRequest
   */
  public consultarResultadoBasal(request: ResultadoBasalRequest): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/consultarResultadoBasal`, request, httpOptions);
  }
  /**
   * Proposito : Guardar cambios Condicion Basal del Paciente - Paso 3
   * @param request
   * DTO : CondicionBasalPacienteRequest
   */
  public insActCondicionBasalPac(request: CondicionBasalPacienteRequest): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/insActCondicionBasalPac`, request, httpOptions);
  }
  /**
   * Proposito : Consultar o Precargar CheckList del Paciente - Paso 4
   * @param request
   */
  public consultarIndicadorCriterio(request): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/consultarIndicadorCriterio`, request, httpOptions);
  }
  /**
   * Proposito : Guardar CheckList del Paciente - Paso 4
   * @param request
   */
  public insertarCheckListPaciente(request): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/insertarCheckListPaciente`, request, httpOptions);
  }
  /**
   * Proposito: Precargar el Paso 5
   * @param request CodSolicitudEvaluacion
   */
  public consultarCheckListPacPrefeInsti(request): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/consultarCheckListPacPrefeInsti`, request, httpOptions);
  }

  public insActCheckListPacPrefeInsti(request: CheckListPacPrefeInstiRequest): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/insActCheckListPacPrefeInsti`, request, httpOptions);
  }
  /**
   * Proposito: Precargar Medicamento Continuador
   * @param request CodSolicitudEvaluacion
   * api/consultarDocumentoContinuador
   */
  public consultarDocumentoContinuador(request: MonitoreoEvolucionRequest): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/getUltimoMonitoreo`, request,httpOptions);
  }

  /**
   * Proposito: Guardar Medicamento Continuador
   * @param request 
   */
  public guardarContinuador(request: ContinuadorRequest){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/insActMedicamentoContinuador`, request,httpOptions);
  }

  /**
   * Proposito: Cargar Documento Medicamento Continuador
   * @param request CodSolicitudEvaluacion
   */
  public insActContinuadorDocumentoCargar(request: CheckListRequisitoRequest): Observable<WsResponse> {
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/insActContinuadorDocumentoCargar`, request,
      {
        headers: new HttpHeaders({
          'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
          'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
        })
      });
  }
  /**
   * Proposito: Eliminar documento Medicamento Continuador
   * @param request CodSolicitudEvaluacion
   */
  public actContinuadorDocumentoElim(request: CheckListRequisitoRequest): Observable<WsResponse> {
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/actContinuadorDocumentoElim`, request,
      {
        headers: new HttpHeaders({
          'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
          'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
        })
      });
  }

  public consultarEvaluacionAutorizador(request): Observable<ApiResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    };
    return this.http.post<ApiResponse>(`${webServiceEndpoint}api/consultarEvaluacionAutorizador`, request, httpOptions);
  }

  public listarIndicadores(request): Observable<ApiListaIndicador> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    };
    return this.http.post<ApiListaIndicador>(`${webServiceEndpoint}api/consultarIndicadorCriterio`, request, httpOptions);
  }

  public cargarArchivo(request): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/insActCheckListReqDocumento`, request, httpOptions);
  }

  public eliminarArchivo(request): Observable<ApiOutResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    };
    return this.http.post<ApiOutResponse>(`${webServiceEndpoint}api/actualizarEliminacionDocumento`, request, httpOptions);
  }

  public actualizarCheckListGuardarDocumento(request): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.random() + '', 'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/actualizarCheckListGuardarDocumento`, request, httpOptions);
  }

  public insertarRptaEva(request): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.random() + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/insertarRptaEva`, request, httpOptions);
  }

  public insertarProgramacionCmac(request): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/registrarProgramacionCmac`, request, httpOptions);
  }

  public listarCodDocumentosChecklist(request: InformeSolEvaReporteRequest): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.random() + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/listarCodDocumentosChecklist`, request, httpOptions);
  }

  public actualizarEvaluacionInformeAutorizador(request: SolicitudEvaluacionRequest): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/actualizarEvaluacionInformeAutorizador`, request, httpOptions);
  }

  public actualizarTipoSolEvaluacion(request: SolicitudEvaluacionRequest): Observable<WsResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json; charset=utf-8',
        'authorization': 'Bearer ' + Cookie.get('access_token_fc'),
        'idTransaccion': Math.floor(Math.random() * (9999 - 1)) + 1 + '',
        'fechaTransaccion': this.datePipe.transform(new Date(), 'dd/MM/yyyy')
      })
    };
    return this.http.post<WsResponse>(`${webServiceEndpoint}api/actualizarTipoSolEvaluacion`, request, httpOptions);
  }

}
