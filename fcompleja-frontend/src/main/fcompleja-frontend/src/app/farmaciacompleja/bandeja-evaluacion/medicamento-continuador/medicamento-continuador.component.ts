import { Component, OnInit, Inject } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import { HistPacienteResponse } from 'src/app/dto/response/HistPacienteResponse';
import { CheckListRequisitoRequest } from 'src/app/dto/request/CheckListRequisitoRequest';
import { DetalleSolicitudEvaluacionService } from 'src/app/service/detalle.solicitud.evaluacion.service';
import { PARAMETRO, MENSAJES, MY_FORMATS_AUNA, FLAG_REGLAS_EVALUACION, ACCESO_EVALUACION, FILEFTP, GRUPO_PARAMETRO, VALIDACION_PARAMETRO, RESULT_EVOLUCION, RESULTADOEVALUACIONAUTO, ESTADOEVALUACION } from 'src/app/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidator } from 'src/app/directives/custom.validator';
import { ApiOutResponse } from 'src/app/dto/response/ApiOutResponse';
import { EvaluacionService } from 'src/app/dto/service/evaluacion.service';
import { ParametroRequest } from 'src/app/dto/ParametroRequest';
import { CoreService } from 'src/app/service/core.service';
import { ListaParametroservice } from 'src/app/service/lista.parametro.service';
import { UsuarioService } from 'src/app/dto/service/usuario.service';
import { ParametroResponse } from 'src/app/dto/ParametroResponse';
import { MessageComponent } from 'src/app/core/message/message.component';
import { MatDialog } from '@angular/material';
import { WsResponse } from 'src/app/dto/WsResponse';
import { BOpcionMenuLocalStorage } from 'src/app/dto/core/BOpcionMenuLocalStorage';
import { DocuHistPacienteResponse } from 'src/app/dto/response/DocuHistPacienteResponse';
import { ArchivoRequest } from 'src/app/dto/request/ArchivoRequest';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';
import { MonitoreoEvolucionRequest } from 'src/app/dto/request/BandejaEvaluacion/MonitoreoEvolucionRequest';
import { DatePipe } from '@angular/common';
import { ContinuadorRequest } from 'src/app/dto/request/ContinuadorRequest';

@Component({
  selector: 'app-medicamento-continuador',
  templateUrl: './medicamento-continuador.component.html',
  styleUrls: ['./medicamento-continuador.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS_AUNA }
  ]
})

export class MedicamentoContinuadorComponent implements OnInit {

  nroMaxFilaAgreArch: number;
  fileupload: File;

  historicoLineaTratRequisito: HistPacienteResponse[] = [];
  chkListRequisito: CheckListRequisitoRequest = new CheckListRequisitoRequest();
  requestGrabar: ContinuadorRequest = new ContinuadorRequest();
  docRequeridos: HistPacienteResponse[] = [];
  docOtros: HistPacienteResponse[] = [];
  todosDocumentos: HistPacienteResponse[] = [];
  totalDocumentosRequeridos: string;
  totalDocumentosOtros: string;
  cmbResultAutorizador: any[] = [];
  btnGrabarContinuador: boolean;
  mensaje: string;
  regresarBandeja: boolean=false;

  parametroRequest: ParametroRequest;

  activarOpenFile: boolean;
  desDocuNuevo: FormControl;
  fileDocOtros: FormControl;

  mostrarDocumentos: boolean;
  mostrarMonitoreo: boolean;
  noPendiente: boolean;

  mensajes: string;
  isLoading: boolean;

  flagResultAuto: boolean;
  resultAutoTxt: String;

  opcionMenu: BOpcionMenuLocalStorage;
  fileRecetaMedica: number;
  txtDescripcion: number; // File otros
  txtResultadoMonitoreo: number;
  txtTareaMonitoreo: number;
  txtResponsableMonitoreo: number;
  txtFechaMonitoreo: number;
  txtResultadoEvaluacion: number;
  txtResultadoAutorizador: number;
  txtComentario: number;
  comentarioTxt: boolean = false;
  btnGrabar: number;
  btnSalir: number;
  flagEvaluacion = FLAG_REGLAS_EVALUACION;
  valorMostrarOpcion = ACCESO_EVALUACION.mostrarOpcion;

  estadoContinuador: string="";

  constructor(
    private adapter: DateAdapter<any>,
    private spinnerService: Ng4LoadingSpinnerService,
    private coreService: CoreService,
    private listaParametroservice: ListaParametroservice,
    private dialog: MatDialog,
    private datePipe: DatePipe,
    @Inject(UsuarioService) private userService: UsuarioService,
    @Inject(EvaluacionService) private solicitud: EvaluacionService,
    private router: Router,
    private detalleSolicitudEvaluacionService: DetalleSolicitudEvaluacionService) {
    this.adapter.setLocale('es-PE');
    this.accesoOpcionMenu();
  }

  ngOnInit() {
    this.inicializarVariables();
  }
  monitoreoFrmGrp: FormGroup = new FormGroup({
    resultAutotizadorTxtFrmCtrl: new FormControl(null),
    rstdoUltMonitoreoFrmCtrl: new FormControl(null),
    tareaMonitoreoFrmCtrl: new FormControl(null),
    responsableUltMonitoreoFrmCtrl: new FormControl(null),
    fechaUltMonitoreoFrmCtrl: new FormControl(null),
    resulSistemaFrmCtrl: new FormControl(null),
    resulAutorizadorFrmCtrl: new FormControl(null, [Validators.required]),
    comentarioFrmCtrl: new FormControl(null)
  });
  get resultAutotizadorTxtFrmCtrl() { return this.monitoreoFrmGrp.get('resultAutotizadorTxtFrmCtrl'); }
  get rstdoUltMonitoreoFrmCtrl() { return this.monitoreoFrmGrp.get('rstdoUltMonitoreoFrmCtrl'); }
  get tareaMonitoreoFrmCtrl() { return this.monitoreoFrmGrp.get('tareaMonitoreoFrmCtrl'); }
  get responsableUltMonitoreoFrmCtrl() { return this.monitoreoFrmGrp.get('responsableUltMonitoreoFrmCtrl'); }
  get fechaUltMonitoreoFrmCtrl() { return this.monitoreoFrmGrp.get('fechaUltMonitoreoFrmCtrl'); }
  get resulSistemaFrmCtrl() { return this.monitoreoFrmGrp.get('resulSistemaFrmCtrl'); }
  get resulAutorizadorFrmCtrl() { return this.monitoreoFrmGrp.get('resulAutorizadorFrmCtrl'); }
  get comentarioFrmCtrl() { return this.monitoreoFrmGrp.get('comentarioFrmCtrl'); }

  public inicializarVariables(): void {
    this.desDocuNuevo = new FormControl(null, [CustomValidator.descripcionInvalida(this.todosDocumentos)]);
    this.parametroRequest = new ParametroRequest();
    this.activarOpenFile = true;
    this.mostrarDocumentos = false;
    this.mostrarMonitoreo = false;
    this.consultarResultAutorizador();
    this.consultarFilaMaxReqParametro();
    this.consultarDocumentos();
    this.consultarUltimoMonitoreo();
  }

  public consultarFilaMaxReqParametro() {
    this.parametroRequest.codigoParametro = PARAMETRO.nroFilaCheckListReq;
    this.listaParametroservice.parametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.nroMaxFilaAgreArch = Number(data.filtroParametro[0].valor1Parametro);
        } else {
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, 'No se obtuvo la cantidad máxima de archivos a subir.', true, false, null);
        }
      },
      error => {
        console.error(error);
        this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al obtener total de archivos a subir', true, false, null);
      }
    );
  }

  public grabarRequestDocumento(): void {
    this.chkListRequisito = new CheckListRequisitoRequest();
    this.chkListRequisito.codSolEva = this.solicitud.codSolEvaluacion;
    this.chkListRequisito.edad = this.solicitud.edad;
  }

  public consultarDocumentos(): void {
    this.mostrarDocumentos = false;
    this.isLoading = true;
    this.grabarRequestDocumento();

    this.detalleSolicitudEvaluacionService.
      consultarCheckListRequisito(this.chkListRequisito).subscribe(
        (data: DocuHistPacienteResponse) => {
          if (data.codResultado === 0) {
            this.docRequeridos = [];
            this.docOtros = [];
            this.todosDocumentos = [];
            this.historicoLineaTratRequisito = (data.historialLineaTrat != null) ? data.historialLineaTrat : [];
            if (data.documentoLineaTrat != null) {
              data.documentoLineaTrat.forEach(documento => {
                documento.estado = (documento.estadoDocumento !== 88) ? false : true;
                const tempDescription = documento.descripcionDocumento;
                documento.descripcionDocumento = (tempDescription.split('(*)<BR>')[0]) ?
                  tempDescription.split('(*)<BR>')[0] : tempDescription;
                documento.subtitle = (tempDescription.split('(*)<BR>')[1]) ?
                  tempDescription.split('(*)<BR>')[1] : '';
                if (documento.tipoDocumento !== PARAMETRO.documentoOtros) {
                  this.docRequeridos.push(documento);
                } else {
                  this.docOtros.push(documento);
                }
                this.todosDocumentos.push(documento);
              });
            }

            this.totalDocumentosRequeridos = this.docRequeridos.length + '';
            this.totalDocumentosOtros = this.docOtros.length + '';
            // this.grabarPaso = data.grabar;

            // this.desDocuNuevo.setValidators(Validators.compose([CustomValidator.descripcionInvalida(this.todosDocumentos)]));
            // this.cargarDatosTabla();

            /*if (this.grabarPaso === '1') {
              this.btnSiguiente.emit(false);
            } else {
              this.btnSiguiente.emit(true);
            }*/
          } else {
            this.openDialogMensaje(data.msgResultado, null, true, false, null);
          }

          this.mostrarDocumentos = true;
          this.isLoading = false;
        },
        error => {
          console.error(error);
          this.mensajes = 'Error al consultar los documentos para la evaluación del medicamento continuador.';
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensajes, true, false, null);
          this.isLoading = false;
          this.mostrarDocumentos = true;
        }
      );
  }

  public consultarResultAutorizador() {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.estadoSolEva;
    this.parametroRequest.codigoParam = VALIDACION_PARAMETRO.perfilAutorPertenencia;
    this.listaParametroservice.consultarParametro(this.parametroRequest).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.cmbResultAutorizador = data.data;
          var indice = this.cmbResultAutorizador.indexOf(2); // obtenemos el indice
          this.cmbResultAutorizador.splice(indice, 1);
          this.obtenerCombo(this.cmbResultAutorizador, null, '-- Seleccionar Resultado --');
          if (this.requestGrabar.codResultadoMonitoreo === RESULT_EVOLUCION.estadoActivo) {
            this.resulAutorizadorFrmCtrl.setValue(ESTADOEVALUACION.estadoAprobadoAutorizador);
          } else if (this.requestGrabar.codResultadoMonitoreo === RESULT_EVOLUCION.estadoInactivo) {
            this.resulAutorizadorFrmCtrl.setValue(ESTADOEVALUACION.estadoRechazadoAutorizador);
          }
        } else {
          console.error('No se encuentra data con el codigo de grupo');
        }
      },
      error => {
        console.error('Error al listar el Resultado del Autorizador');
      }
    );
  }

  public obtenerCombo(lista: any[], valor: number, descripcion: string) {
    if (lista !== null) {
      lista.unshift({
        'codigoParametro': valor,
        'nombreParametro': descripcion
      });
    }
  }

  public consultarUltimoMonitoreo(): void {
    this.flagResultAuto = true;
    const requestMonit: MonitoreoEvolucionRequest = new MonitoreoEvolucionRequest();

    requestMonit.codGrpDiag = this.solicitud.codGrupoDiagnostico;
    requestMonit.codLineaTrat = this.solicitud.codLineaTratamiento;
    requestMonit.fecha = this.solicitud.fechaSolEva;

    // requestMonit.codGrpDiag = '037';
    // requestMonit.codLineaTrat = 6;
    // requestMonit.fecha = '8/06/2019';

    this.detalleSolicitudEvaluacionService.
      consultarDocumentoContinuador(requestMonit).subscribe(
        (response: WsResponse) => {
          if (response.audiResponse.codigoRespuesta === '0') {
            if (response.data !== null) {
              this.noPendiente = true;
              this.mostrarMonitoreo = true;
              this.flagResultAuto = true;
              this.requestGrabar.codResponsableMonitoreo = response.data.codResponsableMonitoreo;
              this.requestGrabar.codResultadoMonitoreo = response.data.codResEvolucion;
              this.rstdoUltMonitoreoFrmCtrl.setValue(response.data.descResEvolucion);
              this.tareaMonitoreoFrmCtrl.setValue(response.data.descCodigoMonitoreo);
              this.responsableUltMonitoreoFrmCtrl.setValue(response.data.nomResponsableMonitoreo);
              this.fechaUltMonitoreoFrmCtrl.setValue(response.data.fechaMonitoreo);
              if (response.data.codResEvolucion === RESULT_EVOLUCION.estadoActivo) {
                this.resulSistemaFrmCtrl.setValue(RESULTADOEVALUACIONAUTO.resultadoAprobado);
                this.resulAutorizadorFrmCtrl.setValue(ESTADOEVALUACION.estadoAprobadoAutorizador);
                // this.comentarioFrmCtrl.disable();
                this.estadoContinuador="1";
              } else if(response.data.codResEvolucion === RESULT_EVOLUCION.estadoInactivo) {
                this.resulSistemaFrmCtrl.setValue(RESULTADOEVALUACIONAUTO.resultadoRechazado);
                this.resulAutorizadorFrmCtrl.setValue(ESTADOEVALUACION.estadoRechazadoAutorizador);
                // this.comentarioFrmCtrl.disable();
                this.estadoContinuador="0";
              } else {
                this.resulSistemaFrmCtrl.setValue(null);
                this.resulAutorizadorFrmCtrl.setValue(null);
              }
              this.resulAutorizadorFrmCtrl.setValue(null);
              this.comentarioFrmCtrl.setValue(null);
            } else {
              this.rstdoUltMonitoreoFrmCtrl.setValue(null);
              this.tareaMonitoreoFrmCtrl.setValue(null);
              this.responsableUltMonitoreoFrmCtrl.setValue(null);
              this.fechaUltMonitoreoFrmCtrl.setValue(null);
              this.resulSistemaFrmCtrl.setValue(null);
              this.resulAutorizadorFrmCtrl.setValue(null);
              this.comentarioFrmCtrl.setValue(null);
              this.comentarioFrmCtrl.enable();
            }
          } else {
            this.noPendiente = true;
            this.mostrarMonitoreo = true;
            this.flagResultAuto = false;
            this.requestGrabar.codResponsableMonitoreo = null;
            this.requestGrabar.codResultadoMonitoreo = null;
            this.resultAutoTxt = 'No se tiene registrado el monitoreo';
            this.resultAutotizadorTxtFrmCtrl.setValue(this.resultAutoTxt);
            this.rstdoUltMonitoreoFrmCtrl.setValue(null);
            this.tareaMonitoreoFrmCtrl.setValue(null);
            this.responsableUltMonitoreoFrmCtrl.setValue(null);
            this.fechaUltMonitoreoFrmCtrl.setValue(null);
            this.resulSistemaFrmCtrl.setValue(null);
            this.resulAutorizadorFrmCtrl.setValue(null);
            this.comentarioFrmCtrl.setValue(null);
            this.comentarioFrmCtrl.enable();
          }
        },
        error => {
          this.mostrarMonitoreo = true;
          this.noPendiente = true;
          console.error(error);
          this.mensajes = 'Error al consultar los documentos para la evaluación del medicamento continuador.';
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensajes, true, false, null);

        }
      );
  }

  public grabarContinuador() {
    this.btnGrabarContinuador = true;
    if (this.validarContinuador()) {
      this.requestGrabar.codSolicitudEvaluacion = this.solicitud.codSolEvaluacion;
      this.requestGrabar.comentario = this.comentarioFrmCtrl.value;
      this.requestGrabar.nroTareaMonitoreo = this.tareaMonitoreoFrmCtrl.value;
      this.requestGrabar.fechaMonitoreo = this.fechaUltMonitoreoFrmCtrl.value;
      this.requestGrabar.resultadoAutorizador = this.resulAutorizadorFrmCtrl.value;
      this.requestGrabar.fechaEstado = this.datePipe.transform(new Date(), 'dd/MM/yyyy HH:mm:ss');
      this.requestGrabar.codigoRolUsuario = this.userService.getCodRol;
      this.requestGrabar.codigoUsuario = this.userService.getCodUsuario;
      this.detalleSolicitudEvaluacionService.guardarContinuador(this.requestGrabar).subscribe(
        (response: WsResponse) => {
          if (response.audiResponse !== null && response.audiResponse.codigoRespuesta === '0' ) {
            this.regresarBandeja = true;
            this.openDialogMensaje(response.audiResponse.mensajeRespuesta, null, true, false, null);
            this.btnGrabarContinuador = true;
          } else {
            this.openDialogMensaje(response.audiResponse.mensajeRespuesta, null, true, false, null);
            this.btnGrabarContinuador = false;
          }
        },
        error => {
          console.error(error);
          this.btnGrabarContinuador = false;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, null, true, false, null);
        }
      );
    } else {
      this.btnGrabarContinuador = false;
      this.mensaje = this.mensaje.substring(0, (this.mensaje.length - 2));
      this.openDialogMensaje(MENSAJES.ERROR_CAMPOS, this.mensaje, true, false, null);
    }
  }

  public validarContinuador():boolean {
    let valido = true;
    /*if (this.resulAutorizadorFrmCtrl.value === null) {
      return false;
    }*/
    if (this.resulSistemaFrmCtrl.value !== null) {
      if (this.resulAutorizadorFrmCtrl.invalid) {
        this.mensaje = '*Por favor, ingresar el resultado final.\n';
        this.resulAutorizadorFrmCtrl.markAsTouched();
        return false;
      }
    }
    if (this.comentarioTxt === true && ( this.comentarioFrmCtrl.value === null ||  this.comentarioFrmCtrl.value.trim() ==='' ) ) {
      this.mensaje = '*Por favor, ingresar la observación.\n';
      return false
    }
    this.mensaje = '*Falta cargar el documento <';
    this.docRequeridos.forEach((docReq: HistPacienteResponse) => {
      if (docReq.estadoDocumento === 89 && docReq.tipoDocumento !== 86) {
        valido = false;
        this.mensaje = this.mensaje + docReq.descripcionDocumento + ',';
      }
    });

    this.mensaje = this.mensaje.substring(0, this.mensaje.length - 1) + '>';
    if (!valido) {
      return false;
    } else {
      return true;
    }
  }

  public verficarResultadoAutorizador() {
    this.requestGrabar.resultadoAutorizador = this.resulAutorizadorFrmCtrl.value;
    if (this.requestGrabar.codResultadoMonitoreo === RESULT_EVOLUCION.estadoActivo &&
      this.resulAutorizadorFrmCtrl.value === ESTADOEVALUACION.estadoAprobadoAutorizador) {
        this.comentarioFrmCtrl.setValue(null);
        // this.comentarioFrmCtrl.disable();
        this.comentarioTxt = false;
    } else if (this.requestGrabar.codResultadoMonitoreo === RESULT_EVOLUCION.estadoInactivo &&
      this.resulAutorizadorFrmCtrl.value === ESTADOEVALUACION.estadoRechazadoAutorizador) {
        this.comentarioFrmCtrl.setValue(null);
        // this.comentarioFrmCtrl.disable();
        this.comentarioTxt = false;
    } else {
      this.comentarioFrmCtrl.enable();
      this.comentarioFrmCtrl.markAsUntouched();
      this.comentarioTxt = true;
    }
  }



  public openFileRequerido(documento: HistPacienteResponse, event) {
    this.fileupload = event.target.files[0];
    if (this.fileupload.size > FILEFTP.tamanioMax) {
      this.mensajes = 'Validación del tamaño de archivo';
      this.openDialogMensaje(
        this.mensajes,
        'Solo se permiten archivos de 2MB como máximo',
        true,
        false,
        'Tamaño archivo: ' + (this.fileupload.size / 1024 / 1024) + 'MB'
      );
      return false;
    }

    this.chkListRequisito = new CheckListRequisitoRequest();
    this.chkListRequisito.codSolEva = this.solicitud.codSolEvaluacion;
    this.chkListRequisito.codCheckListRequisito = documento.codChekListReq;
    this.chkListRequisito.codMac = this.solicitud.codMac;
    this.chkListRequisito.tipoDocumento = documento.tipoDocumento;
    this.chkListRequisito.descripcionDocumento = documento.descripcionDocumento;
    this.chkListRequisito.estadoDocumento = 88;
    this.chkListRequisito.edad = this.solicitud.edad;
    this.chkListRequisito.tipoEvaluacion = 1;
    this.chkListRequisito.codigoRolUsuario = 2;
    this.chkListRequisito.codigoUsuario = 1002;

    documento.descripcionEstado = 'ARCHIVO CARGADO';
    documento.estadoDocumento = 88;
    documento.estado = true;
    documento.cargando = true;

    //this.llamarServicioCargarArchivo();
    this.subirArchivoFTP(documento, 1);
  }

  public verDescripDocumentos(): void {
    if (this.desDocuNuevo.value !== undefined && this.desDocuNuevo.value !== '') {
      this.activarOpenFile = false;
      this.todosDocumentos.forEach((doc: HistPacienteResponse) => {
        if (doc.descripcionDocumento.trim().toUpperCase() === this.desDocuNuevo.value.trim().toUpperCase()) {
          this.activarOpenFile = true;
          return;
        }
      });
    } else if (this.desDocuNuevo.value === undefined || this.desDocuNuevo.value === '') {
      this.activarOpenFile = true;
    }
  }

  public openFile($event): void {
    $event.preventDefault();
    const documento2: HistPacienteResponse = {
      codChekListReq: null,
      lineaTratamiento: null,
      descripcionMac: null,
      tipoDocumento: 87,
      nombreTipoDocumento: 'OTROS',
      descripcionDocumento: (this.desDocuNuevo.value) ? this.desDocuNuevo.value : null,
      subtitle: '',
      urlDescarga: '',
      fechaCarga: null,
      estadoDocumento: 88,
      descripcionEstado: 'ARCHIVO CARGADO',
      estado: true,
      nameFile: this.desDocuNuevo.value,
      codArchivo: null,
      cargando: false
    };

    this.docOtros.push(documento2);

    this.chkListRequisito = {
      codSolEva: this.chkListRequisito.codSolEva,
      codCheckListRequisito: null,
      codContinuadorDoc: null,
      codMac: this.solicitud.codMac,
      tipoDocumento: 87,
      descripcionDocumento: (documento2.descripcionDocumento) ? documento2.descripcionDocumento : null,
      estadoDocumento: 88,
      urlDescarga: null,
      edad: this.solicitud.edad,
      tipoEvaluacion: 2,
      codArchivo: null,
      codigoRolUsuario: 2, // TO-DO SEGUIMIENTO
      codigoUsuario: 1003 // TO-DO SEGUIMIENTO
    };

    this.desDocuNuevo.setValue('');
    this.activarOpenFile = true;

    this.llamarServicioCargarArchivo();
  }



  public llamarServicioCargarArchivo() {

    this.spinnerService.show();
    this.detalleSolicitudEvaluacionService.cargarArchivo(this.chkListRequisito)
      .subscribe((data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.consultarDocumentos();
          this.mensajes = data.audiResponse.mensajeRespuesta;
          this.openDialogMensaje('Archivo cargado...!', this.mensajes, true, false, null);
        } else {
          this.mensajes = data.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
        }
        this.spinnerService.hide();
      },
        error => {
          console.error(error);
          this.spinnerService.hide();
          this.mensajes = 'Error al registar/actualizar documento.';
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensajes, true, false, null);
        }
      );
  }

  public subirArchivoFTP(documento: HistPacienteResponse, tipoDoc: number): void {
    if (typeof this.fileupload === 'undefined' || typeof this.fileupload.name === 'undefined') {
      this.openDialogMensaje('Subida de archivos al FTP', 'Falta seleccionar el archivo a subir.', true, false, null);
    } else {
      const archivoRequest = new ArchivoRequest();

      archivoRequest.archivo = this.fileupload;
      archivoRequest.nomArchivo = documento.descripcionDocumento.replace(/ /gi, '_') + '_' + this.solicitud.codSolEvaluacion + '.pdf';
      archivoRequest.nomArchivo = archivoRequest.nomArchivo.replace('(*)', "");
      archivoRequest.ruta = FILEFTP.rutaEvaluacionRequisitos;

      this.spinnerService.show();

      this.coreService.subirArchivo(archivoRequest).subscribe(
        (response: WsResponse) => {
          if (response.audiResponse.codigoRespuesta === '0') {
            this.mensajes = response.audiResponse.mensajeRespuesta;
            this.chkListRequisito.codArchivo = response.data.codArchivo;
            this.llamarServicioCargarArchivo();
          } else {
            this.mensajes = response.audiResponse.mensajeRespuesta + '.. No se logró eliminar el archivo';
            this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
            if (tipoDoc === 1) {
              this.eliminarRequeridosDoc(documento);
            } else {
              this.eliminarOtrosDoc(documento);
            }
          }
          this.spinnerService.hide();
        },
        error => {
          this.spinnerService.hide();
          console.error(error);
          this.mensajes = 'Error al enviar archivo FTP.';
          this.openDialogMensaje(MENSAJES.ERROR_CARGA_SERVICIO, this.mensajes, true, false, null);
          if (tipoDoc === 1) {
            this.eliminarRequeridosDoc(documento);
          } else {
            this.eliminarOtrosDoc(documento);
          }
        }
      );
    }
  }

  public eliminarRequeridosDoc(documento: HistPacienteResponse) {
    const requeridosTemp = this.docRequeridos;
    documento.estado = false;
    documento.estadoDocumento = 89;

    this.chkListRequisito = {
      codSolEva: this.chkListRequisito.codSolEva,
      codCheckListRequisito: documento.codChekListReq,
      codContinuadorDoc: null,
      codMac: this.solicitud.codMac,
      tipoDocumento: documento.tipoDocumento,
      descripcionDocumento: documento.descripcionDocumento,
      estadoDocumento: 89,
      urlDescarga: null,
      edad: this.solicitud.edad,
      tipoEvaluacion: 1,
      codArchivo: null,
      codigoRolUsuario: this.userService.getCodRol, // TO-DO SEGUIMIENTO
      codigoUsuario: this.userService.getCodUsuario // TO-DO SEGUIMIENTO
    };

    this.llamarEliminarArchivos(requeridosTemp, 1);
  }

  public eliminarOtrosDoc(documento: HistPacienteResponse) {
    const otrosTemp = this.docOtros;
    let contador: number;
    contador = 0;
    this.docOtros.forEach(otroDoc => {
      if (otroDoc.descripcionDocumento === documento.descripcionDocumento) {
        this.docOtros.splice(contador, 1);
      }
      contador++;
    });

    this.chkListRequisito = {
      codSolEva: this.chkListRequisito.codSolEva,
      codCheckListRequisito: documento.codChekListReq,
      codContinuadorDoc: null,
      codMac: this.solicitud.codMac,
      tipoDocumento: 87,
      descripcionDocumento: documento.descripcionDocumento,
      estadoDocumento: 89,
      urlDescarga: null,
      edad: this.solicitud.edad,
      tipoEvaluacion: 1,
      codArchivo: null,
      codigoRolUsuario: this.userService.getCodRol, // TO-DO SEGUIMIENTO
      codigoUsuario: this.userService.getCodUsuario // TO-DO SEGUIMIENTO
    };
    this.llamarEliminarArchivos(otrosTemp, 2);
  }

  public llamarEliminarArchivos(documentos: HistPacienteResponse[], tipo: number) {
    this.detalleSolicitudEvaluacionService.eliminarArchivo(this.chkListRequisito).subscribe(
      (data: ApiOutResponse) => {
        if (data.codResultado === 0) {
          this.openDialogMensaje('Archivo fue eliminado...!', data.msgResultado, true, false, null);
          this.consultarDocumentos();
        } else {
          this.mensajes = data.msgResultado;
          if (tipo === 1) {
            this.docRequeridos = documentos;
          } else {
            this.docOtros = documentos;
          }
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
        }
      },
      error => {
        console.error(error);
        this.mensajes = 'Error, por favor volver a intentar';
        this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensajes, true, false, null);
        if (tipo === 1) {
          this.docRequeridos = documentos;
        } else {
          this.docOtros = documentos;
        }
      }
    );
  }

  public ValidarChecklistRequisito() {

    if (this.docRequeridos === undefined && this.docRequeridos.length === 0) {
      this.mensajes = MENSAJES.ERROR_DOCREQUERIDO;
      return false;
    }

    let valido = true;

    this.mensajes = 'Falta cargar el documento <';
    this.docRequeridos.forEach((docReq: HistPacienteResponse) => {
      if (docReq.estadoDocumento === 89 && docReq.tipoDocumento !== 86) {
        valido = false;
        this.mensajes = this.mensajes + docReq.descripcionDocumento + ',';
      }
    });

    this.mensajes = this.mensajes.substring(0, this.mensajes.length - 1) + '>';

    if (valido) {
      return true;
    } else {
      return false;
    }

  }


  // POP-UP MENSAJES
  public openDialogMensaje(
    message: String,
    message2: String,
    alerta: boolean,
    confirmacion: boolean,
    valor: any): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      disableClose: true,
      width: '400px',
      data: {
        title: MENSAJES.medicNuevo.chkListRequisito.TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(rspta => {
      if( this.regresarBandeja === true ) {
        this.router.navigate(['./app/bandeja-evaluacion']);
      }
    });
  }

  public salirContinuador(): void {
    this.router.navigate(['./app/bandeja-evaluacion']);
  }

  public accesoOpcionMenu() {
    const data = require('src/assets/data/permisosRecursos.json');
    const bandejaEvaluacion = data.bandejaEvaluacion.continuador;
    this.opcionMenu = JSON.parse(localStorage.getItem('opcionMenu'));

    if (this.opcionMenu.opcion.length > 0) {
      this.opcionMenu.opcion.forEach(element => {
        const codOpcion = element.codOpcion;
        switch (codOpcion) {
          case bandejaEvaluacion.fileRecetaMedica:
            this.fileRecetaMedica = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtDescripcion:
            this.txtDescripcion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtResultadoMonitoreo:
            this.txtResultadoMonitoreo = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtTareaMonitoreo:
            this.txtTareaMonitoreo = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtResponsableMonitoreo:
            this.txtResponsableMonitoreo = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtFechaMonitoreo:
            this.txtFechaMonitoreo = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtResultadoEvaluacion:
            this.txtResultadoEvaluacion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtResultadoAutorizador:
            this.txtResultadoAutorizador = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtComentario:
            this.txtComentario = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnGrabar:
            this.btnGrabar = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnSalir:
            this.btnSalir = Number(element.flagAsignacion);
            break;
        }
      });
    }
  }
}
