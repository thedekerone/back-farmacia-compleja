import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CheckListPacPrefeInstiRequest } from 'src/app/dto/request/CheckListPacPrefeInstiRequest';
import { LineaTrataPrefeInstiRequest } from 'src/app/dto/request/LineaTrataPrefeInstiRequest';
import { ParametroRequest } from 'src/app/dto/ParametroRequest';
import { DetalleSolicitudEvaluacionService } from 'src/app/service/detalle.solicitud.evaluacion.service';
import { ListaParametroservice } from 'src/app/service/lista.parametro.service';
import { MatDialog, DateAdapter } from '@angular/material';
import {
  GRUPO_PARAMETRO,
  PARAMETRO,
  PREFERENCIAINSTI,
  VALIDACION_PARAMETRO,
  RESULTADOEVALUACIONAUTO,
  ESTADOEVALUACION,
  MENSAJES,
  EMAIL,
  FILEFTP,
  ROLES,
  FLAG_REGLAS_EVALUACION,
  ACCESO_EVALUACION
} from 'src/app/common';
import { ParametroResponse } from 'src/app/dto/ParametroResponse';
import { WsResponse } from 'src/app/dto/WsResponse';
import { SolicitudEvaluacionRequest } from 'src/app/dto/request/SolicitudEvaluacionRequest';
import { EvaluacionService } from 'src/app/dto/service/evaluacion.service';
import { MessageComponent } from 'src/app/core/message/message.component';
import { UsuarioService } from 'src/app/dto/service/usuario.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { InformeSolEvaReporteRequest } from 'src/app/dto/solicitudEvaluacion/detalle/InformacionScgEvaRequest';
import { ChecklistRequisitoResponse } from 'src/app/dto/response/BandejaEvaluacion/MedicamentoNuevo/ChecklistRequisitoResponse';
import { ParticipanteRequest } from 'src/app/dto/request/BandejaEvaluacion/ParticipanteRequest';
import { ListaFiltroUsuarioRolservice } from 'src/app/service/Lista.usuario.rol.service';
import { EmailDTO } from 'src/app/dto/core/EmailDTO';
import { DatePipe } from '@angular/common';
import { CorreosService } from 'src/app/service/cross/correos.service';
import { OncoWsResponse } from 'src/app/dto/response/OncoWsResponse';
import { Router } from '@angular/router';
import { PdfEvaluacion } from 'src/app/dto/reporte/PdfEvaluacion';
import { ReporteEvaluacionService } from 'src/app/service/Reportes/Evaluacion/reporte-evaluacion.service';
import { CoreService } from 'src/app/service/core.service';
import { ArchivoFTP } from 'src/app/dto/bandeja-preliminar/detalle-preliminar/ArchivoFTP';
import { ArchivoRequest } from 'src/app/dto/request/ArchivoRequest';
import { arch } from 'os';
import { EmailRequest } from 'src/app/dto/request/BandejaEvaluacion/EmailRequest';
import { BandejaEvaluacionService } from 'src/app/service/bandeja.evaluacion.service';
import { CasosEvaluar } from 'src/app/dto/solicitudEvaluacion/bandeja/CasosEvaluar';
import { BOpcionMenuLocalStorage } from 'src/app/dto/core/BOpcionMenuLocalStorage';

@Component({
  selector: 'app-analisis-conclusion',
  templateUrl: './analisis-conclusion.component.html',
  styleUrls: ['./analisis-conclusion.component.scss']
})
export class AnalisisConclusionComponent implements OnInit {

  @Output() btnFinalizar = new EventEmitter<boolean>();

  grabarPaso: string;
  mensaje: string;
  parametroRequest: ParametroRequest;
  informeAutorizador: PdfEvaluacion;

  flagObservaciones: Boolean;

  rbtPerfilPcteP5: any[] = [{
    codigo: 1,
    titulo: 'SI',
    selected: false
  }, {
    codigo: 0,
    titulo: 'NO',
    selected: false
  }];

  rbtPrefInsti: any[] = [{
    codigo: 1,
    titulo: 'SI',
    selected: false
  }, {
    codigo: 0,
    titulo: 'NO',
    selected: false
  }, {
    codigo: 2,
    titulo: 'NO APLICA',
    selected: false
  }];

  rbtPertinencia: any[] = [{
    codigo: 1,
    titulo: 'SI',
    selected: false
  }, {
    codigo: 0,
    titulo: 'NO',
    selected: false
  }];

  public anaConclFrmGrp: FormGroup = new FormGroup({
    'rbtPerfilPcteFrmCtrl': new FormControl(null, [Validators.required]),
    'rbtPrefInstiFrmCtrl': new FormControl(null, [Validators.required]),
    'descripcionMacAnaConFrmCtrl': new FormControl(null),
    'tipoTratamientoGuiaFrmCtrl': new FormControl(null),
    'observacionFrmCtrl': new FormControl(null),
    'rbtPertinenciaFrmCtrl': new FormControl(null, [Validators.required]),
    'condicionPacFrmCtrl': new FormControl(null, [Validators.required]),
    'tiempoUsoFrmCtrl': new FormControl(null, [Validators.required]),
    'resulReglaSisFrmCtrl': new FormControl(null),
    'resultAutorizadorFrmCtrl': new FormControl(null, [Validators.required]),
    'comeAnaConFrmCtrl': new FormControl(null, [])
    // 'comeAnaConFrmCtrl': new FormControl(null, [Validators.required])
  });

  indicadorCheckListPaciente: string;

  resultadoSistema: number;
  pasoFinal: boolean;

  solEvaRequest: SolicitudEvaluacionRequest;
  lineaTratRequest: LineaTrataPrefeInstiRequest;
  chkLstPacPreInsRequest: CheckListPacPrefeInstiRequest;
  cmbCondicionPac: any[] = [];
  cmbTiempoUso: any[] = [];
  cmbResultAutorizador: any[] = [];
  codMacAnalisisConcl: any;
  deshabilitarVistaPreliminar: boolean;
  reglasAnalisisConcl: any[][];
  cumplePrefInsti: number;
  cumpleCheckListPer: number;
  codMac: number;
  boton: boolean;
  txtComentario: boolean;

  opcionMenu: BOpcionMenuLocalStorage;
  chkPerfil: number;
  chkPreferencia: number;
  txtTratamiento: number;
  txtTipoTratamiento: number;
  txtObservaciones: number;
  chkPertinencia: number;
  cmbCondicion: number;
  cmbTiempo: number;
  txtResultado: number;
  btnVista: number;
  cmbResultado: number;
  txtComentarioRol: number;
  flagEvaluacion = FLAG_REGLAS_EVALUACION;
  valorMostrarOpcion = ACCESO_EVALUACION.mostrarOpcion;

  constructor(private adapter: DateAdapter<any>,
    public dialog: MatDialog,
    private router: Router,
    private listaParametroservice: ListaParametroservice,
    private detalleSolicitudEvaluacionService: DetalleSolicitudEvaluacionService,
    private spinnerService: Ng4LoadingSpinnerService,
    private participanteService: ListaFiltroUsuarioRolservice,
    private correoService: CorreosService,
    private coreService: CoreService,
    private datePipe: DatePipe,
    private reporteService: ReporteEvaluacionService,
    private bandejaEvaluacionService: BandejaEvaluacionService,
    @Inject(UsuarioService) private userService: UsuarioService,
    @Inject(EvaluacionService) private solicitud: EvaluacionService) {
    this.adapter.setLocale('es-PE');
    this.accesoOpcionMenu();
  }

  ngOnInit() {
  }

  get rbtPerfilPcteFrmCtrl() { return this.anaConclFrmGrp.get('rbtPerfilPcteFrmCtrl'); }
  get rbtPrefInstiFrmCtrl() { return this.anaConclFrmGrp.get('rbtPrefInstiFrmCtrl'); }
  get descripcionMacAnaConFrmCtrl() { return this.anaConclFrmGrp.get('descripcionMacAnaConFrmCtrl'); }
  get tipoTratamientoGuiaFrmCtrl() { return this.anaConclFrmGrp.get('tipoTratamientoGuiaFrmCtrl'); }
  get observacionFrmCtrl() { return this.anaConclFrmGrp.get('observacionFrmCtrl'); }
  get rbtPertinenciaFrmCtrl() { return this.anaConclFrmGrp.get('rbtPertinenciaFrmCtrl'); }
  get condicionPacFrmCtrl() { return this.anaConclFrmGrp.get('condicionPacFrmCtrl'); }
  get tiempoUsoFrmCtrl() { return this.anaConclFrmGrp.get('tiempoUsoFrmCtrl'); }
  get resulReglaSisFrmCtrl() { return this.anaConclFrmGrp.get('resulReglaSisFrmCtrl'); }
  get resultAutorizadorFrmCtrl() { return this.anaConclFrmGrp.get('resultAutorizadorFrmCtrl'); }
  get comeAnaConFrmCtrl() { return this.anaConclFrmGrp.get('comeAnaConFrmCtrl'); }

  public iniciarAnalisisConclusion() {
    this.rbtPerfilPcteFrmCtrl.disable();
    this.rbtPrefInstiFrmCtrl.disable();
    this.btnFinalizar.emit(true);
    this.indicadorCheckListPaciente = '';
    this.pasoFinal = false;
    this.resultadoSistema = null;
    this.parametroRequest = new ParametroRequest();
    this.chkLstPacPreInsRequest = new CheckListPacPrefeInstiRequest();
    this.solEvaRequest = new SolicitudEvaluacionRequest();
    this.lineaTratRequest = new LineaTrataPrefeInstiRequest();
    this.deshabilitarVistaPreliminar = false;
    this.flagObservaciones = false; // this.comeAnaConFrmCtrl.disable();
    if (this.solicitud.estadoEvaluacion === 19) {
      this.pasoFinal = true;
    }

    this.consultarCondicionPac();
    this.consultarTiempoUso();
    this.consultarResultAutorizador();
    this.consultarCheckListPacPrefeInsti();
  }

  public consultarCondicionPac() {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.condicionPac;
    this.listaParametroservice.listaParametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.cmbCondicionPac = data.filtroParametro;
          this.obtenerCombo(this.cmbCondicionPac, null, '-- Seleccionar Condicion Paciente --');
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar Condicion del Paciente');
      }
    );
  }

  public consultarTiempoUso() {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.tiempoUso;
    this.listaParametroservice.listaParametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.cmbTiempoUso = data.filtroParametro;
          this.obtenerCombo(this.cmbTiempoUso, null, '-- Seleccionar Tiempo de Uso --');
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar Tiempo de Uso');
      }
    );
  }

  public consultarResultAutorizador() {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.estadoSolEva;
    this.parametroRequest.codigoParam = VALIDACION_PARAMETRO.perfilAutorPertenencia;
    this.listaParametroservice.consultarParametro(this.parametroRequest).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.cmbResultAutorizador = data.data;
          this.obtenerCombo(this.cmbResultAutorizador, null, '-- Seleccionar Resultado --');
        } else {
          console.error('No se encuentra data con el codigo de grupo');
        }
      },
      error => {
        console.error('Error al listar el Resultado del Autorizador');
      }
    );
  }

  public funcionPertinencia() {
    this.resulReglaSisFrmCtrl.setValue(null);
  }

  public obtenerResultado() {
    this.reglasAnalisisConcl = [[0, 1, 1], [1, 1, 1], [1, 1, 2]];
    let sistemaAnalisis = [];
    let i = 0;
    if (this.rbtPertinenciaFrmCtrl.value === null) {
      return;
    }
    if (this.condicionPacFrmCtrl.value === null) {
      return;
    }
    if (this.tiempoUsoFrmCtrl.value === null) {
      return;
    }

    sistemaAnalisis = [this.rbtPerfilPcteFrmCtrl.value, this.rbtPertinenciaFrmCtrl.value, this.rbtPrefInstiFrmCtrl.value];

    this.reglasAnalisisConcl.forEach(regla => {
      if (sistemaAnalisis[0] === regla[0] && sistemaAnalisis[1] === regla[1] && sistemaAnalisis[2] === regla[2]) {
        i++;
        this.resulReglaSisFrmCtrl.setValue(RESULTADOEVALUACIONAUTO.resultadoAprobado);
      }
    });
    if (i === 0) {
      this.resulReglaSisFrmCtrl.setValue(RESULTADOEVALUACIONAUTO.resultadoRechazado);
    }
  }

  public verficarResultadoAutorizador(): void {
    const resulAutorizador = this.resultAutorizadorFrmCtrl.value;

    if (!((this.resulReglaSisFrmCtrl.value === RESULTADOEVALUACIONAUTO.resultadoAprobado &&
      resulAutorizador === ESTADOEVALUACION.estadoAprobadoAutorizador) ||
      (this.resulReglaSisFrmCtrl.value === RESULTADOEVALUACIONAUTO.resultadoRechazado &&
        resulAutorizador === ESTADOEVALUACION.estadoRechazadoAutorizador))) {
      this.flagObservaciones = true; this.comeAnaConFrmCtrl.enable();
      this.txtComentario = true;
    } else {
      this.flagObservaciones = false; // this.comeAnaConFrmCtrl.disable();
      this.txtComentario = false;
      this.comeAnaConFrmCtrl.markAsUntouched();
    }
  }

  public consultarCheckListPacPrefeInsti() {
    this.solEvaRequest.codSolicitudEvaluacion = this.solicitud.codSolEvaluacion;
    this.solEvaRequest.codMac = this.solicitud.codMac;
    this.solEvaRequest.codGrpDiag = this.solicitud.codGrupoDiagnostico;
    this.spinnerService.show();
    this.detalleSolicitudEvaluacionService.consultarCheckListPacPrefeInsti(this.solEvaRequest).subscribe(
      (data: WsResponse) => {
        if (data.data != null) {
          if (data.data.analisisConcl.length > 0) {
            // this.descripcionMacAnaConFrmCtrl.setValue(data.data.analisisConcl[0].medicamentoPreferido);
            this.rbtPertinenciaFrmCtrl.setValue(data.data.analisisConcl[0].valPertinencia);
            this.condicionPacFrmCtrl.setValue(data.data.analisisConcl[0].valCondicionPaciente);
            this.tiempoUsoFrmCtrl.setValue(data.data.analisisConcl[0].valTiempoUso);
            this.codMacAnalisisConcl = data.data.analisisConcl[0].codMac;
            this.grabarPaso = data.data.analisisConcl[0].grabar;
          } else {
            this.grabarPaso = '0';
          }
          if (this.grabarPaso === '1') {
            this.btnFinalizar.emit(false);
            this.pasoFinal = true;
          } else {
            this.btnFinalizar.emit(true);
            this.pasoFinal = false;
          }
          if (data.data.tratamiento.length === 1) {
            this.descripcionMacAnaConFrmCtrl.setValue((data.data.tratamiento[0].descripcionTratamiento !== null) ?
              data.data.tratamiento[0].descripcionTratamiento : 'NO REGISTRADO');
            this.tipoTratamientoGuiaFrmCtrl.setValue(
              (data.data.tratamiento[0].descripcionTipoTratamiento !== null) ?
                data.data.tratamiento[0].descripcionTipoTratamiento : 'NO REGISTRADO');

          } else if (data.data.tratamiento.length > 1) {
            let descripcionTratamiento = '';
            data.data.tratamiento.forEach(element => {
              descripcionTratamiento = descripcionTratamiento +
                ((element.descripcionTratamiento !== null) ? element.descripcionTratamiento : 'NO REGISTRADO') + ' / ';
            });
            descripcionTratamiento = descripcionTratamiento.substring(0, descripcionTratamiento.length - 3);
            this.descripcionMacAnaConFrmCtrl.setValue(descripcionTratamiento);

            let descripcionTipoTratamiento = '';
            data.data.tratamiento.forEach(element => {
              descripcionTipoTratamiento = descripcionTipoTratamiento +
                ((element.descripcionTipoTratamiento !== null) ? element.descripcionTipoTratamiento : 'NO REGISTRADO') + ' / ';
            });

            descripcionTipoTratamiento = descripcionTipoTratamiento.substring(0, descripcionTipoTratamiento.length - 3);
            this.tipoTratamientoGuiaFrmCtrl.setValue(descripcionTipoTratamiento);

          } else if (data.data.tratamiento.length === 0) {
            this.descripcionMacAnaConFrmCtrl.setValue(PREFERENCIAINSTI.noRegistrado);
            this.tipoTratamientoGuiaFrmCtrl.setValue(PREFERENCIAINSTI.noRegistrado);
          } else {
            this.descripcionMacAnaConFrmCtrl.setValue(null);
          }

          this.observacionFrmCtrl.setValue((data.data.descripcionMac !== null) ? data.data.descripcionMac : '');
          this.rbtPerfilPcteFrmCtrl.setValue(data.data.cumpleCheckListPer);
          this.rbtPrefInstiFrmCtrl.setValue(data.data.cumplePrefInsti);
          this.indicadorCheckListPaciente = data.data.descripcionIndicacion;
          this.cumplePrefInsti = data.data.cumplePrefInsti;
          this.cumpleCheckListPer = data.data.cumpleCheckListPer;
          this.codMac = data.data.codMac;

          if (data.data.cumplePrefInsti !== this.rbtPrefInstiFrmCtrl.value ||
            data.data.cumpleCheckListPer !== this.rbtPerfilPcteFrmCtrl.value ||
            data.data.codMac !== this.codMacAnalisisConcl) {
            this.deshabilitarVistaPreliminar = true;
          } else {
            this.deshabilitarVistaPreliminar = false;
          }
          this.obtenerResultado();
        } else {
          this.indicadorCheckListPaciente = 'NO SE LOGRÓ CARGAR EL INDICADOR';
          this.rbtPerfilPcteFrmCtrl.setValue(null);
          this.rbtPrefInstiFrmCtrl.setValue(null);
          this.descripcionMacAnaConFrmCtrl.setValue(null);
          this.rbtPertinenciaFrmCtrl.setValue(null);
          this.condicionPacFrmCtrl.setValue(null);
          this.tiempoUsoFrmCtrl.setValue(null);
          this.pasoFinal = true;
          this.resulReglaSisFrmCtrl.setValue(null);
          this.resultAutorizadorFrmCtrl.setValue(null);
          this.comeAnaConFrmCtrl.setValue(null);
          this.grabarPaso = '1';
        }
        this.spinnerService.hide();
      }, error => {
        console.error(error);
        console.error('Error al listar precarga Paso 5');
        this.spinnerService.hide();
        this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al cargar data inicial.', true, false, null);
      }
    );
  }

  public validarResultadoEvaluacion() {

    if ((this.rbtPerfilPcteFrmCtrl.value === 1 || this.rbtPerfilPcteFrmCtrl.value === 0) &&
      (this.rbtPrefInstiFrmCtrl.value === 1 || this.rbtPrefInstiFrmCtrl.value === 2) &&
      this.rbtPertinenciaFrmCtrl.value === 1) {
      this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.resulReglaSis;
      this.parametroRequest.codigoParametro = PARAMETRO.resulReglaSisAprobado;
      this.parametroRequest.codigoParam = null;

      this.listaParametroservice.consultarParametro(this.parametroRequest).subscribe(
        (data: WsResponse) => {
          if (data.audiResponse.codigoRespuesta === '0') {
            this.pasoFinal = true;
            if (data.data.length > 0) {
              this.resulReglaSisFrmCtrl.setValue(data.data[0].nombreParametro);
            } else {
              console.error('No se encontraron resultados');
              this.openDialogMensaje(data.audiResponse.mensajeRespuesta, null, true, false, null);
            }
          } else {
            console.error('Error en obtener resultado segun la regla del sistema');
            this.openDialogMensaje(data.audiResponse.mensajeRespuesta, null, true, false, null);
          }
        },
        error => {
          console.error('Error al obtener el resultado de la evaluación.');
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al obtener el resultado de la evaluación.', true, false, null);
        }
      );
    } else {
      this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.resulReglaSis;
      this.parametroRequest.codigoParametro = PARAMETRO.ResulReglaSisRechazado;
      this.parametroRequest.codigoParam = null;
      this.listaParametroservice.consultarParametro(this.parametroRequest).subscribe(
        (data: WsResponse) => {
          this.pasoFinal = true;
          if (data.audiResponse.codigoRespuesta === '0') {
            if (data.data.length > 0) {
              this.resulReglaSisFrmCtrl.setValue(data.data[0].nombreParametro);
            } else {
              console.error('No se encontraron resultados');
              this.openDialogMensaje(data.audiResponse.mensajeRespuesta, null, true, false, null);
            }
          } else {
            console.error('Error en obtener resultado segun la regla del sistema');
            this.openDialogMensaje(data.audiResponse.mensajeRespuesta, null, true, false, null);
          }
        },
        error => {
          console.error('Error al obtener el resultado de la evaluación.');
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al obtener el resultado de la evaluación.', true, false, null);
        }
      );
    }
  }

  public validarAnalisisConclusion(boton: boolean): boolean {
    this.mensaje = '';
    let valido = true;
    if (this.rbtPerfilPcteFrmCtrl.invalid) {
      this.mensaje += '*Perfil de Paciente no cargo correctamente.\n';
      this.rbtPerfilPcteFrmCtrl.markAsTouched();
      valido = false;
    }

    if (this.rbtPrefInstiFrmCtrl.invalid) {
      this.mensaje += '*Perfil de Preferencias Institucionales no cargo correctamente.\n';
      this.rbtPrefInstiFrmCtrl.markAsTouched();
      valido = false;
    }

    if (this.rbtPertinenciaFrmCtrl.invalid) {
      this.mensaje += '*Seleccionar Pertinencia.\n';
      this.rbtPertinenciaFrmCtrl.markAsTouched();
      valido = false;
    }

    if (this.condicionPacFrmCtrl.invalid) {
      this.mensaje += '*Seleccionar la condición del paciente.\n';
      this.condicionPacFrmCtrl.markAsTouched();
      valido = false;
    }

    if (this.tiempoUsoFrmCtrl.invalid) {
      this.mensaje += '*Seleccionar el tiempo de uso en meses.\n';
      this.tiempoUsoFrmCtrl.markAsTouched();
      valido = false;
    }

    if (!boton) {

      if (this.resultAutorizadorFrmCtrl.invalid) {
        this.mensaje = '*Por favor, ingresar el resultado final.\n';
        this.comeAnaConFrmCtrl.markAsTouched();
        valido = false;
      }

      if (this.txtComentario === true &&
        (this.comeAnaConFrmCtrl.value === null || this.comeAnaConFrmCtrl.value.trim() === '')) {
        this.mensaje = '*Por favor, ingresar las observaciones.\n';
        this.comeAnaConFrmCtrl.markAsTouched();
        valido = false;
      }
    }
    return valido;
  }

  public grabarRequestAnalisis(boton: boolean): void {
    this.chkLstPacPreInsRequest.codSolicitudEvaluacion = this.solicitud.codSolEvaluacion;
    this.chkLstPacPreInsRequest.cumpleCheckListPerfilPac = this.rbtPerfilPcteFrmCtrl.value;
    this.chkLstPacPreInsRequest.cumplePrefeInsti = this.rbtPrefInstiFrmCtrl.value;
    this.chkLstPacPreInsRequest.codMac = this.codMac;
    this.chkLstPacPreInsRequest.pertinencia = this.rbtPertinenciaFrmCtrl.value;
    this.chkLstPacPreInsRequest.condicionPaciente = this.condicionPacFrmCtrl.value;
    this.chkLstPacPreInsRequest.tiempoUso = this.tiempoUsoFrmCtrl.value;
    if (boton) {
      this.chkLstPacPreInsRequest.resultadoAutorizador = null;
      this.chkLstPacPreInsRequest.comentario = null;
      this.chkLstPacPreInsRequest.flagGrabar = 0;
    } else {
      this.chkLstPacPreInsRequest.resultadoAutorizador = this.resultAutorizadorFrmCtrl.value;
      this.chkLstPacPreInsRequest.comentario = this.comeAnaConFrmCtrl.value;
      this.chkLstPacPreInsRequest.flagGrabar = 1;
    }
    this.chkLstPacPreInsRequest.codigoRolUsuario = this.userService.getCodRol;
    this.chkLstPacPreInsRequest.codigoUsuario = this.userService.getCodUsuario;
  }

  public guardarAnalisisConclusion(grabar: boolean) {// TRUE => OPCION GRABAR   FALSE=> OPCION FINALIZAR

    if (this.validarAnalisisConclusion(grabar)) {
      this.grabarRequestAnalisis(grabar);
      this.insActCheckListPacPrefeInsti(grabar);
    } else {
      this.mensaje = this.mensaje.substring(0, (this.mensaje.length - 2));
      this.openDialogMensaje(MENSAJES.ERROR_CAMPOS, this.mensaje, true, false, null);
    }
  }

  public insActCheckListPacPrefeInsti(grabar: boolean): void {
    this.spinnerService.show();
    this.detalleSolicitudEvaluacionService.insActCheckListPacPrefeInsti(this.chkLstPacPreInsRequest).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.deshabilitarVistaPreliminar = false;
          this.validarResultadoEvaluacion();
          this.grabarPaso = '1';
          this.btnFinalizar.emit(false);
          if (!grabar) {
            this.mensaje = data.audiResponse.mensajeRespuesta;
            this.generarReporteAutorizador(false);
          } else {
            this.spinnerService.hide();
            this.mensaje = data.audiResponse.mensajeRespuesta;
            this.openDialogMensaje(this.mensaje, null, true, false, null);
          }

        } else if (data.audiResponse.codigoRespuesta === '1' || data.audiResponse.codigoRespuesta === '2' ||
          data.audiResponse.codigoRespuesta === '3') {
          this.spinnerService.hide();
          this.grabarPaso = '0';
          this.btnFinalizar.emit(true);
          this.mensaje = data.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, null);
        } else {
          this.spinnerService.hide();
          this.grabarPaso = '0';
          this.btnFinalizar.emit(true);
          this.mensaje = data.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, null);
        }
      }, error => {
        console.error(error);
        this.mensaje = 'Error al guardar el análisis y conclusión.';
        this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensaje, true, false, null);
        this.spinnerService.hide();
      }
    );
  }

  public obtenerCombo(lista: any[], valor: number, descripcion: string) {
    if (lista !== null) {
      lista.unshift({
        'codigoParametro': valor,
        'nombreParametro': descripcion
      });
    }
  }

  public guardarRequestInforme(): void {
    this.informeAutorizador = new PdfEvaluacion();
    this.informeAutorizador.codSolicitudEvaluacion = this.solicitud.codSolEvaluacion;
    this.informeAutorizador.codDiagnostico = this.solicitud.codDiagnostico;
    this.informeAutorizador.descripcionDiagnostico = this.solicitud.descDiagnostico;
    this.informeAutorizador.codAfiliado = this.solicitud.codAfiliado;
    this.informeAutorizador.nombrePaciente = this.solicitud.paciente;
    this.informeAutorizador.sexo = this.solicitud.sexoPaciente;
    this.informeAutorizador.nombreMedicoAuditor =
      `${this.userService.getApelPaterno} ${this.userService.getApelMaterno}, ${this.userService.getNombres}`;
    this.informeAutorizador.codArchivo = this.userService.getCodFirma;
    this.informeAutorizador.codUsuario = this.userService.getCodUsuario;
    this.informeAutorizador.codMac = this.solicitud.codMac;
    this.informeAutorizador.codGrupopDiagnostico = Number(this.solicitud.codGrupoDiagnostico);
  }

  public generarReporteAutorizador(vista: boolean): void {
    this.guardarRequestInforme();
    this.reporteService.generarReporteAutorizador(this.informeAutorizador).subscribe(
      (response: WsResponse) => {
        if (response.audiResponse.codigoRespuesta === '0') {
          response.data.contentType = 'application/pdf';
          const blob = this.coreService.crearBlobFile(response.data);
          if (!vista) {
            this.mensaje += '\n*El reporte fue generado correctamente.';
            response.data.nomArchivo = `${response.data.nomArchivo}.pdf`;
            response.data.archivoFile = new File(
              [blob],
              `${response.data.nomArchivo}`,
              {
                type: response.data.contentType,
                lastModified: Date.now()
              });
            this.subirArchivoFTP(response.data);
          } else {
            this.spinnerService.hide();
            const link = document.createElement('a');
            link.target = '_blank';
            link.href = window.URL.createObjectURL(blob);
            link.setAttribute('download', response.data.nomArchivo);
            link.click();
          }
        } else {
          this.mensaje = response.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, null);
          this.spinnerService.hide();
        }
      },
      error => {
        this.spinnerService.hide();
        console.error(error);
        const mensaje = MENSAJES.ERROR_SERVICIO;
        this.openDialogMensaje(mensaje, 'Error al generar el Informe Autorizador.', true, false, null);
      }
    );
  }

  public subirArchivoFTP(archivo: ArchivoFTP) {
    const archivoRequest = new ArchivoRequest();

    archivoRequest.archivo = archivo.archivoFile;
    archivoRequest.nomArchivo = archivo.nomArchivo;
    archivoRequest.ruta = FILEFTP.rutaInformeAutorizador;

    this.coreService.subirArchivo(archivoRequest).subscribe(
      (response: WsResponse) => {
        if (response.audiResponse.codigoRespuesta === '0') {
          const codigoArchInformeEvaluador = response.data.codArchivo;
          this.mensaje += '\n*El informe del autorizador fue subido correctamente';
          this.actualizarSolicitudEvaluacion(response.data);
        } else {
          this.spinnerService.hide();
          this.mensaje = response.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, null);
        }
      }, (error) => {
        this.spinnerService.hide();
        console.error(error);
        const mensaje = MENSAJES.ERROR_SERVICIO;
        this.openDialogMensaje(mensaje, 'Error al subir el archivo.', true, false, null);
      }
    );
  }

  public actualizarSolicitudEvaluacion(archivo: ArchivoFTP) {
    const solEvaRequest = new SolicitudEvaluacionRequest();
    solEvaRequest.codSolicitudEvaluacion = this.solicitud.codSolEvaluacion;
    solEvaRequest.codInformeAutorizador = archivo.codArchivo;
    this.detalleSolicitudEvaluacionService.actualizarEvaluacionInformeAutorizador(solEvaRequest).subscribe(
      (response: WsResponse) => {
        if (response.audiResponse.codigoRespuesta === '0') {
          this.mensaje += '\n*El código de archivo fue actualizado';
          this.enviarCorreoLiderTumor(this.chkLstPacPreInsRequest);
        } else {
          this.mensaje = response.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, null);
          this.spinnerService.hide();
        }
      }, error => {
        console.error(error);
        const mensaje = MENSAJES.ERROR_SERVICIO;
        this.openDialogMensaje(mensaje, 'Error al subir el archivo.', true, false, null);
        this.spinnerService.hide();
      }
    );
  }

  // POP-UP MENSAJES
  public openDialogMensaje(
    message: String,
    message2: String,
    alerta: boolean,
    confirmacion: boolean,
    valor: any): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      disableClose: true,
      width: '400px',
      data: {
        title: MENSAJES.medicNuevo.analiConclusion.TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(rspta => {
    });
  }

  public openDialogMensajeEmail(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.medicNuevo.analiConclusion.TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.router.navigate(['./app/bandeja-evaluacion']);
    });
  }

  public enviarCorreoLiderTumor(clpaciente: CheckListPacPrefeInstiRequest) {

    if (clpaciente.resultadoAutorizador === ESTADOEVALUACION.estadoObservadoAutorizador) { // OBSERVADA POR AUTORIZADOR DE PERTENENCIA
      let request = new InformeSolEvaReporteRequest();
      request.codSolEva = this.solicitud.codSolEvaluacion;

      // OBTENER LIDER TUMOR  => POR GRUPO DE DIAGNOSTICO (SOLO DEBE SER UNO)
      let req = new ParticipanteRequest();
      req.codRol = ROLES.liderTumor; // LIDER TUMOR
      req.codGrpDiag = this.solicitud.codGrupoDiagnostico; // 037

      this.participanteService.listarUsuarioFarmaciaDetallado(req).subscribe(
        (data: WsResponse) => {
          if (data.audiResponse.codigoRespuesta === '0') {
            if (data.data.length > 0) {
              let liderTumor = data.data[0]; // EXISTE UN SOLO LIDER TUMOR POR GRP DIAGNOSTICO
              if (this.validarFormatoEmail(liderTumor.correoElectronico)) { // VALIDA SI EXISTE
                // VALIDACION SI LIDER TUMOR == MEDICO TRATANTE (SI LOS CMP SON IGUALES)
                if (liderTumor.cmpMedico === this.solicitud.codCmp) {
                  this.spinnerService.hide();
                  this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
                  // this.derivarSolicitudACmac();
                } else {
                  this.detalleSolicitudEvaluacionService.listarCodDocumentosChecklist(request).subscribe(
                    (dataCheck: WsResponse) => {
                      if (dataCheck.audiResponse.codigoRespuesta === '0') {
                        // LISTA TODOS LOS DOCUMENTOS ADJUNTOS
                        let rutaDoc = '';
                        dataCheck.data.forEach((element: ChecklistRequisitoResponse) => {
                          if (element.codArchivo != null) {
                            rutaDoc += element.codArchivo + ',';
                          }
                        });
                        rutaDoc = rutaDoc.substr(0, rutaDoc.length - 1);

                        const correoRequest = new EmailDTO();
                        correoRequest.codPlantilla = EMAIL.EVALUACION_LIDER_TUMOR.codigoPlantilla;
                        correoRequest.fechaProgramada = this.datePipe.transform(new Date(), 'dd/MM/yyyy HH:mm:ss');
                        correoRequest.flagAdjunto = EMAIL.EVALUACION_LIDER_TUMOR.flagAdjunto;
                        correoRequest.tipoEnvio = EMAIL.EVALUACION_LIDER_TUMOR.tipoEnvio;
                        correoRequest.usrApp = EMAIL.usrApp;
                        this.correoService.generarCorreo(correoRequest).subscribe(
                          (response: OncoWsResponse) => {
                            if (response.audiResponse.codigoRespuesta === '0') {
                              let result = "";
                              let nombreUsuario = (this.userService.getNombres != null ? this.userService.getNombres + ' ' : "") + (this.userService.getApelPaterno != null ? this.userService.getApelPaterno + ' ' : "") + (this.userService.getApelMaterno != null ? this.userService.getApelMaterno : "");
                              result += response.dataList[0].cuerpo.toString()
                                .replace('{{codSolEvaluacion}}', this.solicitud.numeroSolEvaluacion != null ? this.solicitud.numeroSolEvaluacion : "-----")
                                .replace('{{paciente}}', this.solicitud.paciente != null ? this.solicitud.paciente : "-----")
                                .replace('{{diagnostico}}', this.solicitud.descDiagnostico != null ? this.solicitud.descDiagnostico : "-----")
                                .replace('{{codMedicamento}}', this.solicitud.codMac != null ? this.solicitud.codMac : "-----")
                                .replace('{{descripcionMac}}', this.solicitud.descMAC != null ? this.solicitud.descMAC : "-----")
                                .replace('{{medicoAutorizador}}', nombreUsuario);

                              correoRequest.asunto = EMAIL.EVALUACION_LIDER_TUMOR.asunto + this.solicitud.paciente;
                              correoRequest.cuerpo = result;
                              correoRequest.codigoEnvio = response.dataList[0].codigoEnvio;
                              correoRequest.destinatario = liderTumor.correoElectronico;
                              correoRequest.ruta = rutaDoc;
                              correoRequest.codigoPlantilla = correoRequest.codPlantilla;
                              this.correoService.enviarCorreoGenerico(correoRequest).subscribe(
                                (response: OncoWsResponse) => {
                                  this.spinnerService.hide();
                                  this.mensaje += '\n*' + MENSAJES.PRELIMINAR.EXITO_ENVIAR_CORREO_LT;
                                  this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
                                }, error => {
                                  this.spinnerService.hide();
                                  console.error(error);
                                  this.mensaje += '\n*' + MENSAJES.PRELIMINAR.ERROR_ENVIAR_CORREO;
                                  this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
                                });
                              //PASOS FINALES LUEGO DE ENVIAR EMAIL A LIDER TUMOR
                              let evaluacion = new CasosEvaluar(request.codSolEva, null, null, null, null, null);
                              correoRequest.listaCasosEvaluar = [evaluacion];
                              this.actualizarEstCorreoLidTumSolEvalucion(correoRequest);
                            } else {
                              this.spinnerService.hide();
                              this.mensaje += '\n*Ocurrio un error de envio correo al lider tumor';
                              this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
                            }
                          }, error => {
                            console.error(error);
                            this.spinnerService.hide();
                            this.mensaje += '\n*Ocurrio un error de envio correo al lider tumor';
                            this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
                          }
                        );
                      } else {
                        this.spinnerService.hide();
                        this.mensaje += '\n*Error correo al lider tumor : no se obtuvo adjuntos';
                        this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
                      }
                    }, error => {
                      this.spinnerService.hide();
                      this.mensaje += '\n*Error correo al lider tumor : no se obtuvo adjuntos';
                      this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
                    });
                }
              } else {
                this.spinnerService.hide();
                this.mensaje += '\n*Error correo al lider tumor : email vacio o formato incorrecto';
                this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
              }
            } else {
              this.spinnerService.hide();
              this.mensaje += '\n*Error correo al lider tumor : no existe lider tumor para el grupo diagnostico';
              this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
            }
          } else {
            this.spinnerService.hide();
            this.mensaje += '\n*Error correo al lider tumor : no se pudo obtener la cuenta';
            this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
          }
        }, error => {
          this.spinnerService.hide();
          this.mensaje += '\n*Error correo al lider tumor : no se pudo obtener la cuenta';
          this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
        });
    } else {
      this.spinnerService.hide();
      this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
    }
  }

  public actualizarEstCorreoLidTumSolEvalucion(email: EmailDTO) {
    this.correoService.actualizarEstCorreoLidTumSolEvalucion(email).subscribe(
      (response: WsResponse) => {

      }, error => {
        console.error(error);
      });
  }

  public derivarSolicitudACmac() {
    const req = new EmailRequest();
    req.codSolicitudEvaluacion = this.solicitud.codSolEvaluacion;
    req.estadoSolicitudEvaluacion = Number(PARAMETRO.observadorPorLiderTumor);

    //alert('IMPLEMENTAR DERIVAR SOLICITUD A CMAC => SE DEBE ACTUALIZAR EL ESTADO DE LA SOLICITUD A CMAC (ESTADO => OBSERVADO POR LIDER TUMOR (COD: 25))');
    this.bandejaEvaluacionService.actualizarEstadoSolEvaluacion(req).subscribe(
      (res: WsResponse) => {
        if (res.audiResponse.codigoRespuesta == '0') {
          this.mensaje += '\n*Se derivo la solicitud al CMAC';
          this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
        } else {
          this.mensaje += '\n*Error al derivar la solicitud al CMAC';
          this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
        }
      }, error => {
        console.error(error);
        this.mensaje += '\n*Error al derivar la solicitud al CMAC';
        this.openDialogMensajeEmail(MENSAJES.INFO_SUCCESS, this.mensaje, true, false, null);
      });
  }

  public validarFormatoEmail(email: string): boolean {
    var regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (email) {
      if (regex.test(email)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public accesoOpcionMenu() {

    const data = require('src/assets/data/permisosRecursos.json');
    const bandejaEvaluacion = data.bandejaEvaluacion.paso5;
    this.opcionMenu = JSON.parse(localStorage.getItem('opcionMenu'));

    if (this.opcionMenu.opcion.length > 0) {
      this.opcionMenu.opcion.forEach(element => {
        const codOpcion = element.codOpcion;
        switch (codOpcion) {
          case bandejaEvaluacion.chkPerfil:
            this.chkPerfil = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.chkPreferencia:
            this.chkPreferencia = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtTratamiento:
            this.txtTratamiento = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtTipoTratamiento:
            this.txtTipoTratamiento = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtObservaciones:
            this.txtObservaciones = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.chkPertinencia:
            this.chkPertinencia = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbCondicion:
            this.cmbCondicion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbTiempo:
            this.cmbTiempo = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtResultado:
            this.txtResultado = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnVista:
            this.btnVista = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbResultado:
            this.cmbResultado = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtComentarioRol:
            this.txtComentarioRol = Number(element.flagAsignacion);
            break;
        }
      });
    }
  }
}
