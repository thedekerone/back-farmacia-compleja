import { Component, OnInit, ViewChild, Inject, Output, EventEmitter } from '@angular/core';
import { HistPacienteResponse } from 'src/app/dto/response/HistPacienteResponse';
import { CheckListRequisitoRequest } from 'src/app/dto/request/CheckListRequisitoRequest';
import { FormControl, Validators } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort, DateAdapter, MatDialog } from '@angular/material';
import { PARAMETRO, MENSAJES, FILEFTP, FLAG_REGLAS_EVALUACION, ACCESO_EVALUACION } from 'src/app/common';
import { ParametroRequest } from 'src/app/dto/ParametroRequest';
import { DetalleSolicitudEvaluacionService } from 'src/app/service/detalle.solicitud.evaluacion.service';
import { ListaParametroservice } from 'src/app/service/lista.parametro.service';
import { ParametroResponse } from 'src/app/dto/ParametroResponse';
import { DocuHistPacienteResponse } from 'src/app/dto/response/DocuHistPacienteResponse';
import { EvaluacionService } from 'src/app/dto/service/evaluacion.service';
import { CustomValidator } from 'src/app/directives/custom.validator';
import { MessageComponent } from 'src/app/core/message/message.component';
import { WsResponse } from 'src/app/dto/WsResponse';
import { ArchivoRequest } from 'src/app/dto/request/ArchivoRequest';
import { ApiOutResponse } from 'src/app/dto/response/ApiOutResponse';
import { CoreService } from 'src/app/service/core.service';
import { ArchivoFTP } from 'src/app/dto/bandeja-preliminar/detalle-preliminar/ArchivoFTP';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UsuarioService } from 'src/app/dto/service/usuario.service';
import { BOpcionMenuLocalStorage } from 'src/app/dto/core/BOpcionMenuLocalStorage';

@Component({
  selector: 'app-checklist-requisitos',
  templateUrl: './checklist-requisitos.component.html',
  styleUrls: ['./checklist-requisitos.component.scss']
})
export class ChecklistRequisitosComponent implements OnInit {

  fileupload: File;

  grabarPaso: string;

  mensajes: string;
  @Output() btnSiguiente = new EventEmitter<boolean>();

  mostrarDocumentos: boolean;

  parametroRequest: ParametroRequest;

  nroMaxFilaAgreArch: any;
  historicoLineaTratRequisito: HistPacienteResponse[] = [];
  docRequeridos: HistPacienteResponse[] = [];
  docOtros: HistPacienteResponse[] = [];
  todosDocumentos: HistPacienteResponse[] = [];
  totalDocumentosRequeridos: string;
  totalDocumentosOtros: string;
  rptaPerfilPaciente: number;
  nameDocumento: any;
  fileDocumento: any;
  fileDocRequerido: any;
  chkListRequisito: CheckListRequisitoRequest;
  invalidDescription: boolean;

  desDocuNuevo: FormControl;
  activarOpenFile: boolean;

  inputFile: FormControl = new FormControl(null);

  dataSource: MatTableDataSource<HistPacienteResponse>;
  columnsGrilla = [{
    codAcceso: ACCESO_EVALUACION.paso2.item,
    columnDef: 'codChekListReq',
    header: 'N°',
    cell: (docHistPcte: HistPacienteResponse) => `${docHistPcte.codChekListReq}`
  }, {
    codAcceso: ACCESO_EVALUACION.paso2.lineaTratamiento,
    columnDef: 'lineaTratamiento',
    header: 'LINEA TRATAMIENTO',
    cell: (docHistPcte: HistPacienteResponse) => `${docHistPcte.lineaTratamiento}`
  }, {
    codAcceso: ACCESO_EVALUACION.paso2.medicamento,
    columnDef: 'descripcionMac',
    header: 'MEDICAMENTO',
    cell: (docHistPcte: HistPacienteResponse) => `${docHistPcte.descripcionMac}`
  }, {
    codAcceso: ACCESO_EVALUACION.paso2.tipoDocumento,
    columnDef: 'nombreTipoDocumento',
    header: 'TIPO DE DOCUMENTO',
    cell: (docHistPcte: HistPacienteResponse) => `${docHistPcte.nombreTipoDocumento}`
  }, {
    codAcceso: ACCESO_EVALUACION.paso2.descripcionDocumento,
    columnDef: 'descripcionDocumento',
    header: 'DESCRIPCIÓN DOCUMENTO',
    cell: (docHistPcte: HistPacienteResponse) => `${docHistPcte.descripcionDocumento}`
  }, {
    codAcceso: ACCESO_EVALUACION.paso2.fechaCarga,
    columnDef: 'fechaCarga',
    header: 'FECHA DE CARGA',
    cell: (docHistPcte: HistPacienteResponse) => `${docHistPcte.fechaCarga}`
  }];
  displayedColumns: String[];
  isLoading: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  opcionMenu: BOpcionMenuLocalStorage;
  fileRecetaMedica: number;
  fileInformeMedica: number;
  fileEvaluacionGeriatrica: number;
  txtDescripcion: number; // Opcion para Otros archivos.
  fileArchivo: number;
  btnAgregar: number;
  btnEliminar: number;
  btnCargarArchivo: number;
  flagEvaluacion = FLAG_REGLAS_EVALUACION;
  valorMostrarOpcion = ACCESO_EVALUACION.mostrarOpcion;

  constructor(private adapter: DateAdapter<any>,
    private spinnerService: Ng4LoadingSpinnerService,
    public dialog: MatDialog,
    private coreService: CoreService,
    private listaParametroservice: ListaParametroservice,
    private detalleSolicitudEvaluacionService: DetalleSolicitudEvaluacionService,
    @Inject(UsuarioService) private userService: UsuarioService,
    @Inject(EvaluacionService) private solicitud: EvaluacionService) {
    this.adapter.setLocale('es-PE');
    this.accesoOpcionMenu();
  }

  ngOnInit() {
    this.desDocuNuevo = new FormControl('', CustomValidator.descripcionInvalida(this.todosDocumentos));
    this.definirTablaCheckListRequisito();
  }

  public definirTablaCheckListRequisito(): void {
    this.historicoLineaTratRequisito = [];
    this.displayedColumns = [];

    this.columnsGrilla.forEach(c => {
      if (this.flagEvaluacion) {
        this.displayedColumns.push(c.columnDef);
      }else{
        this.opcionMenu.opcion.forEach(element => {
          if (c.codAcceso && c.codAcceso === element.codOpcion && Number(element.flagAsignacion) === ACCESO_EVALUACION.mostrarOpcion) {
            this.displayedColumns.push(c.columnDef);
          }
        });
      }
    });

    if (this.flagEvaluacion) {
      this.displayedColumns.push('descargar');
    }else{
      this.opcionMenu.opcion.forEach(element => {
        if (element.codOpcion === ACCESO_EVALUACION.paso2.descargar) {
          this.displayedColumns.push('descargar');
        }
      });
    }
  }

  // -----------  INICIO PASO 2 DESARROLLO   --------------
  public iniciarCheckListRequisito() {
    this.isLoading = false;
    this.dataSource = null;
    this.desDocuNuevo = new FormControl('', CustomValidator.descripcionInvalida(this.todosDocumentos));
    this.parametroRequest = new ParametroRequest();
    this.historicoLineaTratRequisito = [];
    this.activarOpenFile = true;
    this.chkListRequisito = new CheckListRequisitoRequest();
    this.mostrarDocumentos = false;
    this.consultarFilaMaxReqParametro();
    this.consultarCheckListRequisito();
  }

  public cargarDatosTabla(): void {
    if (this.historicoLineaTratRequisito.length && this.historicoLineaTratRequisito.length > 0) {
      this.dataSource = new MatTableDataSource(this.historicoLineaTratRequisito);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  public consultarFilaMaxReqParametro() {
    this.parametroRequest.codigoParametro = PARAMETRO.nroFilaCheckListReq;
    this.listaParametroservice.parametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.nroMaxFilaAgreArch = data.filtroParametro[0].valor1Parametro;
        } else {
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, data.mensageResultado, true, false, null);
        }
      },
      error => {
        console.error(error);
        this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al obtener total de archivos', true, false, null);
      }
    );
  }

  public grabarRequest(): void {
    this.chkListRequisito.codSolEva = this.solicitud.codSolEvaluacion;
    this.chkListRequisito.edad = this.solicitud.edad;
    this.chkListRequisito.tipoEvaluacion = 1;
    this.chkListRequisito.codigoRolUsuario = this.userService.getCodRol;
    this.chkListRequisito.codigoUsuario = this.userService.getCodUsuario;
  }

  public docRequeridosRol() {
    let docRequeridosAux: HistPacienteResponse[] = [];
    this.docRequeridos.forEach(doc => {
        this.opcionMenu.opcion.forEach(element => {
          if (doc.tipoDocumento === ACCESO_EVALUACION.paso2.codigoRecetaParametro && ACCESO_EVALUACION.paso2.recetaMedica === element.codOpcion && Number(element.flagAsignacion) === ACCESO_EVALUACION.mostrarOpcion) {
            docRequeridosAux.push(doc);
          }
          if (doc.tipoDocumento === ACCESO_EVALUACION.paso2.codigoInformeParametro && ACCESO_EVALUACION.paso2.informeMedico === element.codOpcion && Number(element.flagAsignacion) === ACCESO_EVALUACION.mostrarOpcion) {
            docRequeridosAux.push(doc);
          }
          if (doc.tipoDocumento === ACCESO_EVALUACION.paso2.codigoEvaluacionParametro && ACCESO_EVALUACION.paso2.evaluacionGeriatrica === element.codOpcion && Number(element.flagAsignacion) === ACCESO_EVALUACION.mostrarOpcion) {
            docRequeridosAux.push(doc);
          }
        });
    });
    this.docRequeridos = docRequeridosAux;

    var docOtrosAux: HistPacienteResponse[] = [];
    this.docOtros.forEach(doc => {
      this.opcionMenu.opcion.forEach(element => {
        if (doc.tipoDocumento === ACCESO_EVALUACION.paso2.codigoOtroParametro && ACCESO_EVALUACION.paso2.otro === element.codOpcion && Number(element.flagAsignacion) === ACCESO_EVALUACION.mostrarOpcion) {
          docOtrosAux.push(doc);
        }
      });
    });
    this.docOtros = docOtrosAux;

  }

  public consultarCheckListRequisito() {
    this.mostrarDocumentos = false;
    this.isLoading = true;

    this.dataSource = null;
    this.historicoLineaTratRequisito = [];

    this.grabarRequest();

    this.detalleSolicitudEvaluacionService.
      consultarCheckListRequisito(this.chkListRequisito).subscribe(
        (data: DocuHistPacienteResponse) => {
          if (data.codResultado === 0) {
            this.docRequeridos = [];
            this.docOtros = [];
            this.todosDocumentos = [];
            this.historicoLineaTratRequisito = (data.historialLineaTrat != null) ? data.historialLineaTrat : [];
            if (data.documentoLineaTrat != null) {
              data.documentoLineaTrat.forEach(documento => {
                documento.estado = (documento.estadoDocumento !== 88) ? false : true;
                const tempDescription = documento.descripcionDocumento;
                documento.descripcionDocumento = (tempDescription.split('(*)<BR>')[0]) ?
                  tempDescription.split('(*)<BR>')[0] : tempDescription;
                documento.subtitle = (tempDescription.split('(*)<BR>')[1]) ?
                  tempDescription.split('(*)<BR>')[1] : '';
                if (documento.tipoDocumento !== PARAMETRO.documentoOtros) {
                  this.docRequeridos.push(documento);
                } else {
                  this.docOtros.push(documento);
                }
                this.todosDocumentos.push(documento);
              });
            }
            
            if (!this.flagEvaluacion) {
              this.docRequeridosRol();
            }


            this.totalDocumentosRequeridos = this.docRequeridos.length + '';
            this.totalDocumentosOtros = this.docOtros.length + '';
            this.grabarPaso = data.grabar;

            this.desDocuNuevo.setValidators(Validators.compose([CustomValidator.descripcionInvalida(this.todosDocumentos)]));
            this.cargarDatosTabla();

            if (this.grabarPaso === '1') {
              this.btnSiguiente.emit(false);
            } else {
              this.btnSiguiente.emit(true);
            }
          } else {
            this.openDialogMensaje(data.msgResultado, null, true, false, null);
          }

          this.mostrarDocumentos = true;
          this.isLoading = false;
        },
        error => {
          console.error('Error al consultar CheckList Requisito');
          console.error(error);
          this.isLoading = false;
          this.mostrarDocumentos = true;
        }
      );
  }

  public verDescripDocumentos() {
    if (this.desDocuNuevo.value !== undefined && this.desDocuNuevo.value !== '') {
      this.activarOpenFile = false;
      this.todosDocumentos.forEach((doc) => {
        if (doc.descripcionDocumento.trim().toUpperCase() === this.desDocuNuevo.value.trim().toUpperCase()) {
          this.activarOpenFile = true;
          return;
        }
      });
    } else if (this.desDocuNuevo.value === undefined || this.desDocuNuevo.value === '') {
      this.activarOpenFile = true;
    }
  }

  public openFileRequerido(documento: HistPacienteResponse, event) {
    this.fileupload = event.target.files[0];

    if (this.fileupload.size > FILEFTP.tamanioMax) {
      this.mensajes = 'Validación del tamaño de archivo';
      this.openDialogMensaje(
        this.mensajes,
        'Solo se permiten archivos de 2MB como máximo',
        true,
        false,
        'Tamaño archivo: ' + (this.fileupload.size / 1024 / 1024) + 'MB'
      );
      return false;
    } else if ( this.fileupload.type !== FILEFTP.filePdf ) {
      this.openDialogMensaje(
          'Validación del tipo de archivo',
          'Solo se permiten archivos PDF',
          true,
          false,
          'Tipo de archivo: ' + this.fileupload.type + 'MB'
      );
      return false;
    }

    this.chkListRequisito = new CheckListRequisitoRequest();
    this.chkListRequisito.codSolEva = this.solicitud.codSolEvaluacion;
    this.chkListRequisito.codCheckListRequisito = documento.codChekListReq;
    this.chkListRequisito.codMac = this.solicitud.codMac;
    this.chkListRequisito.tipoDocumento = documento.tipoDocumento;
    this.chkListRequisito.descripcionDocumento = documento.descripcionDocumento;
    this.chkListRequisito.estadoDocumento = 88;
    this.chkListRequisito.edad = this.solicitud.edad;
    this.chkListRequisito.tipoEvaluacion = 1;
    this.chkListRequisito.codigoRolUsuario = this.userService.getCodRol;
    this.chkListRequisito.codigoUsuario = this.userService.getCodUsuario;

    documento.descripcionEstado = 'ARCHIVO CARGADO';
    documento.estadoDocumento = 88;
    documento.estado = true;
    documento.cargando = true;

    this.subirArchivoFTP(documento, 1);
  }

  public openFile(event) {
    if (this.docOtros.length < Number(this.nroMaxFilaAgreArch)) {
      this.fileupload = event.target.files[0];
      if (this.fileupload.size > FILEFTP.tamanioMax) {
        this.mensajes = 'Validación del tamaño de archivo';
        this.openDialogMensaje(
          this.mensajes,
          'Solo se permiten archivos de 2MB como máximo',
          true,
          false,
          'Tamaño archivo: ' + (this.fileupload.size / 1024 / 1024) + 'MB'
        );
        return false;
      }
      const documento2: HistPacienteResponse = {
        codChekListReq: null,
        lineaTratamiento: null,
        descripcionMac: null,
        tipoDocumento: 87,
        nombreTipoDocumento: 'OTROS',
        descripcionDocumento: (this.desDocuNuevo.value) ? this.desDocuNuevo.value : null,
        subtitle: '',
        urlDescarga: '',
        fechaCarga: null,
        estadoDocumento: 88,
        descripcionEstado: 'ARCHIVO CARGADO',
        estado: true,
        nameFile: this.desDocuNuevo.value,
        codArchivo: null,
        cargando: true
      };

      this.docOtros.push(documento2);

      this.chkListRequisito = {
        codSolEva: this.chkListRequisito.codSolEva,
        codCheckListRequisito: null,
        codContinuadorDoc: null,
        codMac: this.solicitud.codMac,
        tipoDocumento: 87,
        descripcionDocumento: (documento2.descripcionDocumento) ? documento2.descripcionDocumento : null,
        estadoDocumento: 88,
        urlDescarga: null,
        edad: this.solicitud.edad,
        tipoEvaluacion: 1,
        codigoRolUsuario: this.userService.getCodRol,
        codigoUsuario: this.userService.getCodUsuario,
        codArchivo: null
      };

      this.desDocuNuevo.setValue('');
      this.activarOpenFile = true;
      this.subirArchivoFTP(documento2, 2);
    } else {
      this.mensajes = 'Máximo Archivos: ' + this.nroMaxFilaAgreArch;
      this.openDialogMensaje('Cantidad de archivos adicionales superada.', this.mensajes, true, false, null);
    }
  }

  public subirArchivoFTP(documento: HistPacienteResponse, tipoDoc: number): void {
    if (typeof this.fileupload === 'undefined' || typeof this.fileupload.name === 'undefined') {
      this.openDialogMensaje('Subida de archivos al FTP', 'Falta seleccionar el archivo a subir.', true, false, null);
    } else {
      const archivoRequest = new ArchivoRequest();

      archivoRequest.archivo = this.fileupload;
      archivoRequest.nomArchivo = documento.descripcionDocumento.replace(/ /gi, '_') + '_' + this.solicitud.codSolEvaluacion + '.pdf';
      archivoRequest.nomArchivo = archivoRequest.nomArchivo.replace('(*)', "");
      archivoRequest.ruta = FILEFTP.rutaEvaluacionRequisitos;

      this.spinnerService.show();

      this.coreService.subirArchivo(archivoRequest).subscribe(
        (response: WsResponse) => {
          if (response.audiResponse.codigoRespuesta === '0') {
            this.mensajes = response.audiResponse.mensajeRespuesta;
            this.chkListRequisito.codArchivo = response.data.codArchivo;
            this.llamarServicioCargarArchivo();
          } else {
            this.mensajes = response.audiResponse.mensajeRespuesta + '.. No se logró eliminar el archivo';
            this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
            if (tipoDoc === 1) {
              this.eliminarRequeridosDoc(documento);
            } else {
              this.eliminarOtrosDoc(documento);
            }
          }
          this.spinnerService.hide();
        },
        error => {
          this.spinnerService.hide();
          console.error(error);
          this.mensajes = 'Error al enviar archivo FTP.';
          this.openDialogMensaje(MENSAJES.ERROR_CARGA_SERVICIO, this.mensajes, true, false, null);
          if (tipoDoc === 1) {
            this.eliminarRequeridosDoc(documento);
          } else {
            this.eliminarOtrosDoc(documento);
          }
        }
      );
    }
  }

  public llamarServicioCargarArchivo() {
    this.spinnerService.show();
    this.detalleSolicitudEvaluacionService.cargarArchivo(this.chkListRequisito)
      .subscribe((data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.consultarCheckListRequisito();
          this.mensajes = data.audiResponse.mensajeRespuesta;
          this.openDialogMensaje('Archivo cargado...!', this.mensajes, true, false, null);
        } else {
          this.mensajes = data.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
        }
        this.spinnerService.hide();
      },
        error => {
          console.error(error);
          this.spinnerService.hide();
          this.mensajes = 'Error al registar/actualizar documento.';
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensajes, true, false, null);
        }
      );
  }

  public eliminarRequeridosDoc(documento: HistPacienteResponse) {
    const requeridosTemp = this.docRequeridos;
    documento.estado = false;
    documento.estadoDocumento = 89;

    this.chkListRequisito = {
      codSolEva: this.chkListRequisito.codSolEva,
      codCheckListRequisito: documento.codChekListReq,
      codContinuadorDoc: null,
      codMac: this.solicitud.codMac,
      tipoDocumento: documento.tipoDocumento,
      descripcionDocumento: documento.descripcionDocumento,
      estadoDocumento: 89,
      urlDescarga: null,
      edad: this.solicitud.edad,
      tipoEvaluacion: 1,
      codArchivo: null,
      codigoRolUsuario: this.userService.getCodRol,
      codigoUsuario: this.userService.getCodUsuario
    };

    this.llamarEliminarArchivos(requeridosTemp, 1);
  }

  public eliminarOtrosDoc(documento: HistPacienteResponse) {
    const otrosTemp = this.docOtros;
    let contador: number;
    contador = 0;
    this.docOtros.forEach(otroDoc => {
      if (otroDoc.descripcionDocumento === documento.descripcionDocumento) {
        this.docOtros.splice(contador, 1);
      }
      contador++;
    });

    this.chkListRequisito = {
      codSolEva: this.chkListRequisito.codSolEva,
      codCheckListRequisito: documento.codChekListReq,
      codContinuadorDoc: null,
      codMac: this.solicitud.codMac,
      tipoDocumento: 87,
      descripcionDocumento: documento.descripcionDocumento,
      estadoDocumento: 89,
      urlDescarga: null,
      edad: this.solicitud.edad,
      tipoEvaluacion: 1,
      codArchivo: null,
      codigoRolUsuario: this.userService.getCodRol,
      codigoUsuario: this.userService.getCodUsuario
    };

    this.llamarEliminarArchivos(otrosTemp, 2);
  }

  public llamarEliminarArchivos(documentos: HistPacienteResponse[], tipo: number) {
    this.detalleSolicitudEvaluacionService.eliminarArchivo(this.chkListRequisito).subscribe(
      (data: ApiOutResponse) => {
        if (data.codResultado === 0) {
          this.openDialogMensaje('Archivo fue eliminado...!', data.msgResultado, true, false, null);
          this.consultarCheckListRequisito();
        } else {
          this.mensajes = data.msgResultado;
          if (tipo === 1) {
            this.docRequeridos = documentos;
          } else {
            this.docOtros = documentos;
          }
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
        }
      },
      error => {
        console.error(error);
        this.mensajes = 'Error, por favor volver a intentar';
        this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensajes, true, false, null);
        if (tipo === 1) {
          this.docRequeridos = documentos;
        } else {
          this.docOtros = documentos;
        }
      }
    );
  }

  public ValidarChecklistRequisito(): boolean {

    if (this.docRequeridos === undefined && this.docRequeridos.length === 0) {
      this.mensajes = MENSAJES.ERROR_DOCREQUERIDO;
      return false;
    }

    let valido = true;

    this.mensajes = 'Falta cargar el documento <';
    this.docRequeridos.forEach((docReq: HistPacienteResponse) => {
      if (docReq.estadoDocumento === 89 && docReq.tipoDocumento !== 86) {
        valido = false;
        this.mensajes = this.mensajes + docReq.descripcionDocumento + ',';
      }
    });

    this.mensajes = this.mensajes.substring(0, this.mensajes.length - 1) + '>';
    if (valido) {
      return true;
    } else {
      return false;
    }
  }

  public guardarChecklistRequisito() {
    if (this.ValidarChecklistRequisito()) {
      this.spinnerService.show();
      this.detalleSolicitudEvaluacionService.
        actualizarCheckListGuardarDocumento(this.chkListRequisito).subscribe(
          (data: WsResponse) => {
            if (data.audiResponse.codigoRespuesta === '0') {
              this.grabarPaso = '1';
              this.btnSiguiente.emit(false);
              this.openDialogMensaje(data.audiResponse.mensajeRespuesta, 'Se guardaron los cambios', true, false, null);
            } else {
              this.grabarPaso = '0';
              this.btnSiguiente.emit(true);
              this.mensajes = data.audiResponse.mensajeRespuesta;
              this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
            }
            this.spinnerService.hide();
          }, error => {
            this.btnSiguiente.emit(true);
            this.mensajes = 'Error, por favor volver a intentar';
            this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensajes, true, false, null);
            console.error(error);
            this.spinnerService.hide();
          }
        );
    } else {
      // POPUP PARA ENVIAR MENSAJE DE CAMPOS REQUERIDOS
      console.error(this.mensajes);
      this.openDialogMensaje(MENSAJES.ERROR_VALIDA_DOC, this.mensajes, true, false, null);
    }

  }

  public descargarDocumento(documento: HistPacienteResponse): void {
    const archivoRqt = new ArchivoFTP();
    archivoRqt.codArchivo = documento.codArchivo;
    archivoRqt.nomArchivo = documento.nameFile;
    archivoRqt.ruta = FILEFTP.rutaEvaluacionRequisitos;
    this.spinnerService.show();
    this.coreService.descargarArchivoFTP(archivoRqt).subscribe(
      (response: WsResponse) => {
        if (response.audiResponse.codigoRespuesta === '0') {
          response.data.contentType = 'application/pdf';
          const blob = this.coreService.crearBlobFile(response.data);
          const link = document.createElement('a');
          link.target = '_blank';
          link.href = window.URL.createObjectURL(blob);
          link.setAttribute('download', response.data.nomArchivo);
          link.click();
          this.spinnerService.hide();
        } else {
          this.mensajes = response.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
          this.spinnerService.hide();
        }
      }, (error) => {
        console.error(error);
        this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
        this.spinnerService.hide();
      }
    );
  }

  // POP-UP MENSAJES
  public openDialogMensaje(
    message: String,
    message2: String,
    alerta: boolean,
    confirmacion: boolean,
    valor: any): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      disableClose: true,
      width: '400px',
      data: {
        title: MENSAJES.medicNuevo.chkListRequisito.TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(rspta => {
    });
  }

  public accesoOpcionMenu() {
    const data = require('src/assets/data/permisosRecursos.json');
    const bandejaEvaluacion = data.bandejaEvaluacion.paso2;
    this.opcionMenu = JSON.parse(localStorage.getItem('opcionMenu'));

    if (this.opcionMenu.opcion.length > 0) {
      this.opcionMenu.opcion.forEach(element => {
        const codOpcion = element.codOpcion;
        switch (codOpcion) {
          case bandejaEvaluacion.fileRecetaMedica:
                this.fileRecetaMedica = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.fileInformeMedica:
                this.fileInformeMedica = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.fileEvaluacionGeriatrica:
                this.fileEvaluacionGeriatrica = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.txtDescripcion:
                this.txtDescripcion = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.fileArchivo:
                this.fileArchivo = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.btnAgregar:
                this.btnAgregar = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.btnEliminar:
                this.btnEliminar = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.btnCargarArchivo:
                this.btnCargarArchivo = Number(element.flagAsignacion);
                break;
        }
      });
    }
  }
}
