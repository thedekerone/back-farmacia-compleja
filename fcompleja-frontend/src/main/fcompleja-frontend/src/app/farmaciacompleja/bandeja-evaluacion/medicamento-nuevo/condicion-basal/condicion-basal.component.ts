import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ResultadoBasalRequest } from 'src/app/dto/request/ResultadoBasalRequest';
import { ResultadoBasalDetResponse } from 'src/app/dto/response/BandejaEvaluacion/MedicamentoNuevo/ResultadoBasalDetResponse';
import { CondicionBasalPacienteRequest } from 'src/app/dto/request/CondicionBasalPacienteRequest';
import { MetastasisResponse } from 'src/app/dto/response/BandejaEvaluacion/MedicamentoNuevo/MetastasisResponse';
import { MatTableDataSource, DateAdapter, MatDialog } from '@angular/material';
import { ParametroRequest } from 'src/app/dto/ParametroRequest';
import { GRUPO_PARAMETRO, PARAMETRO, MENSAJES, FLAG_REGLAS_EVALUACION, ACCESO_EVALUACION } from 'src/app/common';
import { ParametroResponse } from 'src/app/dto/ParametroResponse';
import { ListaParametroservice } from 'src/app/service/lista.parametro.service';
import { DetalleSolicitudEvaluacionService } from 'src/app/service/detalle.solicitud.evaluacion.service';
import { CustomValidator } from 'src/app/directives/custom.validator';
import { WsResponse } from 'src/app/dto/WsResponse';
import { EvaluacionService } from 'src/app/dto/service/evaluacion.service';
import { MessageComponent } from 'src/app/core/message/message.component';
import { UsuarioService } from 'src/app/dto/service/usuario.service';
import { DatePipe } from '@angular/common';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BOpcionMenuLocalStorage } from 'src/app/dto/core/BOpcionMenuLocalStorage';

@Component({
  selector: 'app-condicion-basal',
  templateUrl: './condicion-basal.component.html',
  styleUrls: ['./condicion-basal.component.scss']
})
export class CondicionBasalComponent implements OnInit {
  componenteCargado: boolean;
  mostrarColumnaRango: boolean;

  rbtExisteToxi: any[] = [{
    codigo: 1,
    titulo: 'SI',
    selected: false
  }, {
    codigo: 0,
    titulo: 'NO',
    selected: false
  }];

  formGroup1Mensaje = {
    'antResulBasalFrmCtrl': [
      { type: 'maxlength', message: 'M\u00e1ximo 4000 caracteres' },
      { type: 'required', message: 'Requerido *' }
    ]
  };

  mensaje: string;
  mensajeResultadoBasal: string;
  grabarPaso: string;
  maxDate: Date;

  @Output() btnSiguiente = new EventEmitter<boolean>();

  parametroRequest: ParametroRequest;

  condBasalFrmGrp: FormGroup = new FormGroup({
    'antResulBasalFrmCtrl': new FormControl(null, [Validators.required, Validators.maxLength(4000)]),
    'lineaMetaFrmCtrl': new FormControl(null, Validators.required),
    'lugarMetaFrmCtrl': new FormControl(null, Validators.required),
    'ecogFrmCtrl': new FormControl(null, Validators.required),
    'rgbExisteToxiFrmCtrl': new FormControl(null, Validators.required),
    'tipoToxicidadFrmCtrl': new FormControl({ value: null, disabled: true }, Validators.required),
    'existeToxiFrmCtrl': new FormControl(null, Validators.required)
  });

  tableBasalFrmGrp: FormGroup;

  get antResulBasalFrmCtrl() { return this.condBasalFrmGrp.get('antResulBasalFrmCtrl'); }
  get lineaMetaFrmCtrl() { return this.condBasalFrmGrp.get('lineaMetaFrmCtrl'); }
  get lugarMetaFrmCtrl() { return this.condBasalFrmGrp.get('lugarMetaFrmCtrl'); }
  get ecogFrmCtrl() { return this.condBasalFrmGrp.get('ecogFrmCtrl'); }
  get rgbExisteToxiFrmCtrl() { return this.condBasalFrmGrp.get('rgbExisteToxiFrmCtrl'); }
  get tipoToxicidadFrmCtrl() { return this.condBasalFrmGrp.get('tipoToxicidadFrmCtrl'); }
  get existeToxiFrmCtrl() { return this.condBasalFrmGrp.get('existeToxiFrmCtrl'); }

  resultadoBasalRequest: ResultadoBasalRequest;
  resultadoBasalDetalle: ResultadoBasalDetResponse[];

  condicionBasalRequest: CondicionBasalPacienteRequest;

  metastasisLista: MetastasisResponse[];
  tablaBasal: ResultadoBasalDetResponse[];

  cmbLineaMetastasis: any[] = [];
  cmbLugarMetastasis: any[] = [];
  cmbEcog: any[] = [];
  cmbTipoToxicidad: any[] = [];

  nroMaxFilaLineaMeta: any;
  resultadoBasal: boolean;
  barResultBasal: boolean;
  cmbSeleccionado: number;
  tipoToxicidadValue: number;

  columnsMetastasis = [{
    codAcceso: ACCESO_EVALUACION.paso3.lineaMetastasis,
    columnDef: 'descripcionLineaMetastasis',
    header: 'LINEA METASTASIS',
    cell: (metasColum: MetastasisResponse) => `${metasColum.descripcionLineaMetastasis}`
  }, {
    codAcceso: ACCESO_EVALUACION.paso3.lugarMetastasis,
    columnDef: 'descripcionLugarMetastasis',
    header: 'LUGAR METASTASIS',
    cell: (metasColum: MetastasisResponse) => `${metasColum.descripcionLugarMetastasis}`
  }];

  columnsResBasal = [{
    codAcceso: ACCESO_EVALUACION.paso3.colMarcador,
    columnDef: 'descripcionExamenMed'
  }, {
    codAcceso: ACCESO_EVALUACION.paso3.colRango,
    columnDef: 'rango'
  }, {
    codAcceso: ACCESO_EVALUACION.paso3.colResultado,
    columnDef: 'resultado'
  }, {
    codAcceso: ACCESO_EVALUACION.paso3.colFechaResultado,
    columnDef: 'fecResultado'
  }];

  dataSourceMetas: MatTableDataSource<MetastasisResponse>;
  displayedColumnsMetas: string[];
  isLoadingMetas: boolean;

  dataSourceResBasal: MatTableDataSource<ResultadoBasalDetResponse>;
  displayedColumnsResBasal: string[];
  isLoadingBasal: boolean;

  resultadoNumberFrmCtrl: FormControl = new FormControl(null, Validators.required);
  resultadoTextFrmCtrl: FormControl = new FormControl(null, Validators.required);
  resultadoCmbFrmCtrl: FormControl = new FormControl(null, Validators.required);

  opcionMenu: BOpcionMenuLocalStorage;
  txtAntecedentes: number;
  cmbLineaMetastasisRol: number;
  cmbLugarMetastasisRol: number;
  btnAgregar: number;
  cmbEcogRol: number;
  txtExisteToxicidad: number;
  cmbTipoToxicidadRol: number;
  flagEvaluacion = FLAG_REGLAS_EVALUACION;
  valorMostrarOpcion = ACCESO_EVALUACION.mostrarOpcion;

  constructor(private listaParametroservice: ListaParametroservice,
    private spinerService: Ng4LoadingSpinnerService,
    private dialog: MatDialog,
    private datePipe: DatePipe,
    private detalleSolicitudEvaluacionService: DetalleSolicitudEvaluacionService,
    private adapter: DateAdapter<any>,
    @Inject(UsuarioService) private userService: UsuarioService,
    @Inject(EvaluacionService) private solicitud: EvaluacionService) {
    this.componenteCargado = false;
    this.adapter.setLocale('es-PE');
    this.accesoOpcionMenu();
  }

  ngOnInit() {
    this.definirTablaCondicionBasalMetas();
    this.definirTablaCondicionBasalResultado();
  }

  get fc1() { return this.condBasalFrmGrp.controls; }

  public definirTablaCondicionBasalMetas(): void {
    this.metastasisLista = [];
    this.displayedColumnsMetas = [];
    this.columnsMetastasis.forEach(c => {
      if (this.flagEvaluacion) {
        this.displayedColumnsMetas.push(c.columnDef);
      } else {
        this.opcionMenu.opcion.forEach(element => {
          if (c.codAcceso && c.codAcceso === element.codOpcion && Number(element.flagAsignacion) === ACCESO_EVALUACION.mostrarOpcion) {
            this.displayedColumnsMetas.push(c.columnDef);
          }
        });
      }
    });
    if (this.flagEvaluacion) {
      this.displayedColumnsMetas.push('accion');
    } else {
      this.opcionMenu.opcion.forEach(element => {
        if (element.codOpcion === ACCESO_EVALUACION.paso3.eliminar) {
          this.displayedColumnsMetas.push('accion');
        }
      });
    }
    this.dataSourceMetas = null;

    this.displayedColumnsResBasal = ['position'];
    this.columnsResBasal.forEach(c => {
      if (this.flagEvaluacion) {
        this.displayedColumnsResBasal.push(c.columnDef);
      } else {
        this.opcionMenu.opcion.forEach(element => {
          if (c.codAcceso && c.codAcceso === element.codOpcion && Number(element.flagAsignacion) === ACCESO_EVALUACION.mostrarOpcion) {
            this.displayedColumnsResBasal.push(c.columnDef);
          }
        });
      }
    });
  }

  public definirTablaCondicionBasalResultado(): void {
    this.resultadoBasalDetalle = [];
    this.dataSourceResBasal = null;
    this.resultadoBasal = false;

    this.existeToxiFrmCtrl.setValue(0);
    this.tipoToxicidadFrmCtrl.disable();
  }

  public iniciarCondicionBasalPaciente() {

    this.maxDate = new Date();
    this.parametroRequest = new ParametroRequest();
    this.resultadoBasalRequest = new ResultadoBasalRequest();
    this.resultadoBasalRequest.codSolEva = this.solicitud.codSolEvaluacion;
    this.resultadoBasalRequest.codMac = this.solicitud.codMac;
    this.resultadoBasalRequest.codGrpDiag = this.solicitud.codGrupoDiagnostico;

    this.consultarLineaMetastasis();
    this.consultarLugarMetastasis();
    this.consultarEcog();
    this.consultarTipoToxicidad();
    this.consultarLineaMetParametro();

    this.consultarResultadoBasal();
  }

  public cargarTablaMetastasis() {
    if (this.metastasisLista.length > 0) {
      this.dataSourceMetas = new MatTableDataSource(this.metastasisLista);
    }
  }

  public addLineaMetastasis($event) {
    $event.preventDefault();
    let validar = false;
    const metasNew: MetastasisResponse = new MetastasisResponse();

    if (this.lineaMetaFrmCtrl.value === null) {
      this.mensaje = 'Debe seleccionar Linea de Metastasis';
      this.lineaMetaFrmCtrl.markAsTouched();
      validar = true;
    }

    if (this.lugarMetaFrmCtrl.value === null) {
      this.mensaje = (validar) ? this.mensaje + ' y Lugar de Metastasis' : 'Debe seleccionar el lugar de Metastasis';
      validar = true;
      this.lugarMetaFrmCtrl.markAsTouched();
    }

    if (!validar && this.metastasisLista.length > 0) {
      this.metastasisLista.forEach((meta: MetastasisResponse) => {
        if (meta.lineaMetastasis === this.lineaMetaFrmCtrl.value.codigoParametro &&
          meta.lugarMetastasis === this.lugarMetaFrmCtrl.value.codigoParametro) {
          this.mensaje = 'Los valores ya fueron ingresados';
          validar = true;
          this.lineaMetaFrmCtrl.setValue(null);
          this.lugarMetaFrmCtrl.setValue(null);
          return;
        }
      });
    }

    if (validar) {
      this.openDialogMensaje(MENSAJES.ERROR_CAMPOS, this.mensaje, true, false, null);
      return;
    }

    metasNew.lineaMetastasis = this.lineaMetaFrmCtrl.value.codigoParametro;
    metasNew.descripcionLineaMetastasis = this.lineaMetaFrmCtrl.value.nombreParametro;
    metasNew.lugarMetastasis = this.lugarMetaFrmCtrl.value.codigoParametro;
    metasNew.descripcionLugarMetastasis = this.lugarMetaFrmCtrl.value.nombreParametro;
    metasNew.estado = 1;

    this.metastasisLista.push(metasNew);
    this.dataSourceMetas = new MatTableDataSource(this.metastasisLista);

    this.lineaMetaFrmCtrl.setValue(null);
    this.lugarMetaFrmCtrl.setValue(null);
    this.lineaMetaFrmCtrl.markAsUntouched();
    this.lugarMetaFrmCtrl.markAsUntouched();
  }

  public eliminarLineaMetastasis($event, row: MetastasisResponse): void {
    $event.preventDefault();
    const index: number = this.metastasisLista.findIndex(d => d === row);

    this.metastasisLista.forEach((item) => {
      if (item === row) {
        this.metastasisLista.splice(index, 1);
        this.dataSourceMetas = new MatTableDataSource(this.metastasisLista);
        return;
      }
    });
  }

  /**Parametro Linea de Metastasis */
  public consultarLineaMetastasis() {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.lineaMetastasis;
    this.listaParametroservice.listaParametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.cmbLineaMetastasis = data.filtroParametro;
          this.obtenerCombo(this.cmbLineaMetastasis, null, '-- Seleccionar Línea Metástasis --');
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar Línea Metástasis');
      }
    );

  }

  /**Parametro Lugar de Metastasis */
  public consultarLugarMetastasis() {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.lugarMetastasis;
    this.listaParametroservice.listaParametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.cmbLugarMetastasis = data.filtroParametro;
          this.obtenerCombo(this.cmbLugarMetastasis, null, '-- Seleccionar Lugar Metástasis --');
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar Lugar Metástasis');
      }
    );
  }

  /**Parametro Ecog */
  public consultarEcog() {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.ecog;
    this.listaParametroservice.listaParametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.cmbEcog = data.filtroParametro;
          this.obtenerCombo(this.cmbEcog, null, '-- Seleccionar Ecog --');
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar Ecog');
      }
    );

  }

  /**Parametro Tipo Toxicidad */
  public consultarTipoToxicidad() {
    this.parametroRequest.codigoGrupo = GRUPO_PARAMETRO.tipoToxicidad;
    this.listaParametroservice.listaParametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 0) {
          this.cmbTipoToxicidad = data.filtroParametro;
          this.obtenerCombo(this.cmbTipoToxicidad, null, '--Seleccionar Tipo Toxicidad--');
        } else {

        }
      },
      error => {
        console.error('Error al listar Tipo Toxicidad');
      }
    );
  }

  public consultarLineaMetParametro() {
    this.parametroRequest.codigoParametro = PARAMETRO.nroFilaLineaMetastasis;
    this.listaParametroservice.parametro(this.parametroRequest).subscribe(
      (data: ParametroResponse) => {
        this.nroMaxFilaLineaMeta = data.filtroParametro[0].valor1Parametro;
      },
      error => {
        console.error('Error al consultar Parametro');
      }
    );
  }

  public crearFormControlBasal(): void {
    if (this.resultadoBasalDetalle.length > 0) {
      const frmCtrl = {};
      this.resultadoBasalDetalle.forEach((resultado: ResultadoBasalDetResponse) => {
        const fechaTemp: Date = (resultado.fecResultado != null) ? resultado.fecResultado : null;

        frmCtrl[`f${resultado.codResultadoBasal}`] = new FormControl(null, [Validators.required]);
        frmCtrl[`f${resultado.codResultadoBasal}`].setValue((fechaTemp !== null) ?
          new Date(this.datePipe.transform(fechaTemp, 'yyyy/MM/dd')) : new Date(this.datePipe.transform(new Date(), 'yyyy/MM/dd')));
        switch (resultado.tipoIngresoResul) {
          case 122:
            frmCtrl[`r${resultado.codResultadoBasal}`] =
              new FormControl(
                resultado.resultado,
                [
                  Validators.required,
                  CustomValidator.checkLimit(resultado.minLength, resultado.maxLength)
                ]
              );
            this.mostrarColumnaRango = true;
            break;
          case 123:
            if (resultado.resultado != null) {
              this.cmbSeleccionado = Number(resultado.resultado);
            }
            frmCtrl[`r${resultado.codResultadoBasal}`] = new FormControl(this.cmbSeleccionado, [Validators.required]);
            break;
          case 124:
            frmCtrl[`r${resultado.codResultadoBasal}`] = new FormControl(resultado.resultado, [Validators.required]);
            break;
        }
      });

      if (this.mostrarColumnaRango) {
        this.displayedColumnsResBasal = ['position', 'descripcionExamenMed', 'rango', 'resultado', 'fecResultado'];
      } else {
        this.displayedColumnsResBasal = ['position', 'descripcionExamenMed', 'resultado', 'fecResultado'];
      }

      this.tableBasalFrmGrp = new FormGroup(frmCtrl);
      this.resultadoBasal = true;
    } else {

    }
  }

  public consultarResultadoBasal() {
    this.barResultBasal = true;
    this.btnSiguiente.emit(true);
    this.dataSourceResBasal = null;
    this.isLoadingMetas = true;
    this.resultadoBasalDetalle = [];
    this.isLoadingBasal = true;

    this.detalleSolicitudEvaluacionService.consultarResultadoBasal(this.resultadoBasalRequest).subscribe(
      (response: WsResponse) => {
        if (response.audiResponse.codigoRespuesta === '0') {
          if (response.data != null) {
            this.metastasisLista = response.data.metastasis;
            this.resultadoBasalDetalle = response.data.resultadoBasal;
            this.tipoToxicidadFrmCtrl.setValue(response.data.tipoToxicidad);
            this.antResulBasalFrmCtrl.setValue(response.data.antecedente);
            this.ecogFrmCtrl.setValue(response.data.ecog);
            if (response.data.existeToxicidad) {
              this.existeToxiFrmCtrl.setValue(response.data.existeToxicidad);
            } else {
              this.existeToxiFrmCtrl.setValue(0);
              this.tipoToxicidadFrmCtrl.disable();
            }

            if (response.data.existeToxicidad == 1) {
              this.tipoToxicidadFrmCtrl.enable();
            } else {
              this.tipoToxicidadFrmCtrl.disable();
            }
            //MARCAR COMO UNTOCHED
            this.grabarPaso = response.data.grabar;
            if (this.grabarPaso === '1') {
              this.lineaMetaFrmCtrl.markAsUntouched();
              this.lugarMetaFrmCtrl.markAsUntouched();
              this.btnSiguiente.emit(false);
            }
            this.crearFormControlBasal();
            this.dataSourceMetas = new MatTableDataSource(this.metastasisLista);
            this.dataSourceResBasal = new MatTableDataSource(this.resultadoBasalDetalle);
          }
        } else if (response.audiResponse.codigoRespuesta === '1') {
          this.mensajeResultadoBasal = response.audiResponse.mensajeRespuesta;
        } else {
          this.mensaje = response.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, null);
        }
        this.isLoadingMetas = false;
        this.isLoadingBasal = false;
        this.barResultBasal = false;
      },
      error => {
        console.error(error);
        this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al precargar condición basal.', true, false, null);
        this.isLoadingMetas = false;
        this.isLoadingBasal = false;
      }
    );
  }

  public Validacion(): boolean {
    let valido = true;
    this.mensaje = '';
    const auxAntecendente = (this.antResulBasalFrmCtrl.value === null) ? 0 : this.antResulBasalFrmCtrl.value.length;
    if (auxAntecendente === 0 || auxAntecendente > 4000) {
      valido = false;
      this.mensaje = this.mensaje + '*Corregir antecendetes de importancia';
      this.antResulBasalFrmCtrl.markAsTouched();
    }

    // if (this.metastasisLista.length === 0) {
    //   valido = false;
    //   this.lineaMetaFrmCtrl.markAsTouched();
    //   this.lugarMetaFrmCtrl.markAsTouched();
    //   this.mensaje = this.mensaje + '\n*Agregar linea y lugar de metástasis.';
    // }

    if (this.ecogFrmCtrl.value === null) {
      valido = false;
      this.ecogFrmCtrl.markAsTouched();
      this.mensaje = this.mensaje + '\n*Seleccionar ECOG.';
    }

    if (this.existeToxiFrmCtrl.value === null) {
      valido = false;
      this.existeToxiFrmCtrl.markAsTouched();
      this.mensaje = this.mensaje + '\n*Indicar si existe toxicidad.';
    }

    if (this.existeToxiFrmCtrl.value === 1 && this.tipoToxicidadFrmCtrl.value === null) {
      valido = false;
      this.tipoToxicidadFrmCtrl.markAsTouched();
      this.mensaje = this.mensaje + '\n*Seleccionar tipo de toxicidad.';
    }

    if (this.resultadoBasalDetalle.length == 0) {
      valido = false;
      this.mensaje = this.mensaje + '\n*No existe la configuracion de condicion basal para ' + this.solicitud.descMAC + ' y ' + this.solicitud.descGrupoDiagnostico + ', solicite la configuracion al administrador.';
    } else {
      if (this.tableBasalFrmGrp.invalid) {
        valido = false;
        this.resultadoBasalDetalle.forEach((resultado: ResultadoBasalDetResponse) => {
          this.tableBasalFrmGrp.controls['r' + resultado.codResultadoBasal].markAsTouched();
          this.tableBasalFrmGrp.controls['f' + resultado.codResultadoBasal].markAsTouched();
        });
        this.mensaje = this.mensaje + '\n*Llenar y/o corregir los resultados de la tabla.';
      }
    }

    return valido;
  }

  public guardarParametros(): void {

    this.condicionBasalRequest = new CondicionBasalPacienteRequest();
    this.condicionBasalRequest.codSolEvaluacion = this.solicitud.codSolEvaluacion;
    this.condicionBasalRequest.antecedenteImp = this.antResulBasalFrmCtrl.value;
    this.condicionBasalRequest.lineaLugarMetastasis = this.metastasisLista;
    this.condicionBasalRequest.ecog = this.ecogFrmCtrl.value;
    this.condicionBasalRequest.existeToxicidad = this.existeToxiFrmCtrl.value;
    this.condicionBasalRequest.tipoToxicidad = this.tipoToxicidadFrmCtrl.value;
    this.condicionBasalRequest.resultadoBasal = [];
    this.condicionBasalRequest.codigoRolUsuario = this.userService.getCodRol;
    this.condicionBasalRequest.codigoUsuario = this.userService.getCodUsuario;

    this.resultadoBasalDetalle.forEach((resultado: ResultadoBasalDetResponse) => {
      let resultadoBasal: ResultadoBasalDetResponse = new ResultadoBasalDetResponse();
      resultadoBasal = resultado;
      resultadoBasal.resultado = this.tableBasalFrmGrp.controls['r' + resultado.codResultadoBasal].value;
      const fechaTemp = this.tableBasalFrmGrp.controls['f' + resultado.codResultadoBasal].value;
      resultadoBasal.fecResultado = fechaTemp;
      this.condicionBasalRequest.resultadoBasal.push(resultadoBasal);
    });
  }

  public insActCondicionBasalPac() {
    this.spinerService.show();
    this.detalleSolicitudEvaluacionService.insActCondicionBasalPac(this.condicionBasalRequest).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.grabarPaso = '1';
          this.btnSiguiente.emit(false);
          this.openDialogMensaje(data.audiResponse.mensajeRespuesta, null, true, false, null);
        } else {
          this.grabarPaso = '1';
          this.btnSiguiente.emit(true);
          this.mensaje = data.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, null);
        }
        this.spinerService.hide();
      }, error => {
        console.error(error);
        this.grabarPaso = '1';
        this.btnSiguiente.emit(true);
        this.mensaje = 'Error al grabar/modificar Condición basal del Paciente.';
        this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, null);
        this.spinerService.hide();
      }
    );
  }

  public guardarCondicionBasal() {
    if (this.Validacion()) {
      this.guardarParametros();
      this.insActCondicionBasalPac();
    } else {
      this.openDialogMensaje(MENSAJES.ERROR_CAMPOS, this.mensaje, true, false, null);
    }
  }

  public obtenerCombo(lista: any[], valor: number, descripcion: string) {
    if (lista !== null) {
      lista.unshift({
        'codigoParametro': valor,
        'nombreParametro': descripcion
      });
    }
  }

  public verificarToxicidad(event): void {
    if (this.existeToxiFrmCtrl.value === 0) {
      this.tipoToxicidadFrmCtrl.setValue(null);
      this.tipoToxicidadFrmCtrl.disable();
    } else {
      this.tipoToxicidadFrmCtrl.setValue(null);
      this.tipoToxicidadFrmCtrl.enable();
    }
  }

  // POP-UP MENSAJES
  public openDialogMensaje(
    message: String,
    message2: String,
    alerta: boolean,
    confirmacion: boolean,
    valor: any): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      disableClose: true,
      width: '400px',
      data: {
        title: MENSAJES.medicNuevo.condBasalPcte.TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(rspta => {
    });
  }

  public accesoOpcionMenu() {

    const data = require('src/assets/data/permisosRecursos.json');
    const bandejaEvaluacion = data.bandejaEvaluacion.paso3;
    this.opcionMenu = JSON.parse(localStorage.getItem('opcionMenu'));

    if (this.opcionMenu.opcion.length > 0) {
      this.opcionMenu.opcion.forEach(element => {
        const codOpcion = element.codOpcion;
        switch (codOpcion) {
          case bandejaEvaluacion.txtAntecedentes:
            this.txtAntecedentes = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbLineaMetastasisRol:
            this.cmbLineaMetastasisRol = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbLugarMetastasisRol:
            this.cmbLugarMetastasisRol = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnAgregar:
            this.btnAgregar = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbEcogRol:
            this.cmbEcogRol = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtExisteToxicidad:
            this.txtExisteToxicidad = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.cmbTipoToxicidadRol:
            this.cmbTipoToxicidadRol = Number(element.flagAsignacion);
            break;
        }
      });
    }
  }
}
