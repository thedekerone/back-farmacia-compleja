import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiListaIndicador } from 'src/app/dto/response/ApiListaIndicador';
import { ListaIndicadorRequest } from 'src/app/dto/medicamento-nuevo/ListaIndicadorRequest';
import { IndicacionCriterioRequest } from 'src/app/dto/request/IndicacionCriterioRequest';
import { DetalleSolicitudEvaluacionService } from 'src/app/service/detalle.solicitud.evaluacion.service';
import { WsResponse } from 'src/app/dto/WsResponse';
import { EvaluacionService } from 'src/app/dto/service/evaluacion.service';
import { MENSAJES, FLAG_REGLAS_EVALUACION, ACCESO_EVALUACION } from 'src/app/common';
import { MessageComponent } from 'src/app/core/message/message.component';
import { MatDialog } from '@angular/material';
import { UsuarioService } from 'src/app/dto/service/usuario.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BOpcionMenuLocalStorage } from 'src/app/dto/core/BOpcionMenuLocalStorage';

@Component({
  selector: 'app-checklist-paciente',
  templateUrl: './checklist-paciente.component.html',
  styleUrls: ['./checklist-paciente.component.scss']
})
export class ChecklistPacienteComponent implements OnInit {

  @Output() btnSiguiente = new EventEmitter<boolean>();
  grabarPaso: string;
  rptaPerfilPaciente: number;
  mensajes: string;
  noCriteriosFlag: boolean;
  barCriterios: boolean;
  noCriterios: string;

  flagComentarios: Boolean;

  rbtPerfilPcte: any[] = [{
    codigo: 1,
    titulo: 'SI',
    selected: false
  }, {
    codigo: 0,
    titulo: 'NO',
    selected: false
  }];

  rbtCriterios: any[] = [{
    codigo: 1,
    titulo: 'SI',
    selected: false
  }, {
    codigo: 0,
    titulo: 'NO',
    selected: false
  }];

  indicadorFrmCtrl: FormControl = new FormControl(null, [Validators.required]);
  rbgPerfilPcteFrmCtrl: FormControl = new FormControl(null, [Validators.required]);
  comePerfcteFrmCtrl: FormControl = new FormControl(null, []);
  // comePerfcteFrmCtrl: FormControl = new FormControl(null, [Validators.required]);
  disableComentario: boolean;

  ApiListaIndicador: ApiListaIndicador;
  listaIndicador: any[] = [];
  criterioInclusion: any[] = [];
  criterioExclusion: any[] = [];
  listaTemporal: any;
  rbgPerfilPcte: any;
  listaIndicadorRequest: ListaIndicadorRequest;
  step: number;
  indicacionCriterio: IndicacionCriterioRequest;

  opcionMenu: BOpcionMenuLocalStorage;
  chkIndicacion: number;
  chkCriteriosInclusion: number;
  chkCriteriosExclusion: number;
  chkPaciente: number;
  txtComentario: number;
  flagEvaluacion = FLAG_REGLAS_EVALUACION;
  valorMostrarOpcion = ACCESO_EVALUACION.mostrarOpcion;

  constructor(@Inject(EvaluacionService) private solicitud: EvaluacionService,
    @Inject(UsuarioService) private userService: UsuarioService,
    private spinerService: Ng4LoadingSpinnerService,
    private dialog: MatDialog,
    private detalleSolicitudEvaluacionService: DetalleSolicitudEvaluacionService) {
      this.accesoOpcionMenu();
  }

  ngOnInit() {
    this.inicializarVariables();
  }

  public inicializarVariables(): void {
    this.ApiListaIndicador = new ApiListaIndicador();
    this.listaIndicador = [];
    this.criterioInclusion = [];
    this.criterioExclusion = [];
  }

  public setStep(index: number): void {
    this.step = index;
  }

  public iniciarChkListPaciente() {
    this.btnSiguiente.emit(true);
    this.rbgPerfilPcteFrmCtrl.disable();
    this.disableComentario = true;
    this.flagComentarios = false; // this.comePerfcteFrmCtrl.disable();
    this.listaIndicadorRequest = new ListaIndicadorRequest();
    this.indicacionCriterio = new IndicacionCriterioRequest();
    this.precargaChkListPaciente();
  }

  public precargaChkListPaciente() {
    this.barCriterios = true;
    this.noCriteriosFlag = true;
    this.listaIndicadorRequest.codSolEvaluacion = this.solicitud.codSolEvaluacion;
    this.listaIndicadorRequest.codMac = this.solicitud.codMac;
    // this.listaIndicadorRequest.codGrpDiag = '10';
    this.listaIndicadorRequest.codGrpDiag = this.solicitud.codGrupoDiagnostico;
    this.detalleSolicitudEvaluacionService.consultarIndicadorCriterio(this.listaIndicadorRequest).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.noCriteriosFlag = false;
          this.grabarPaso = data.data.grabar;
          if (this.grabarPaso === '1') {
            this.btnSiguiente.emit(false);
          }
          this.listaIndicador = data.data.listaIndicador;
          this.rbgPerfilPcteFrmCtrl.setValue(data.data.valCumpleChkListPer);
          if (this.rbgPerfilPcteFrmCtrl.value != null) {
            this.rbgPerfilPcteFrmCtrl.enable();
          }
          if (data.data.comentario != null && data.data.comentario.trim() !== '') {
            this.flagComentarios = true; this.comePerfcteFrmCtrl.enable();
            this.comePerfcteFrmCtrl.setValue(data.data.comentario);
          }
          this.listaIndicador.forEach(element => {
            element.selected = false;
            if (element.codigo === data.data.codigoIndicacionPre) {
              this.indicadorFrmCtrl.setValue(element.codigo);
              this.contadorIndicador(element, 'listaCriterioInclusion', 'criterioInclusion', true);
              this.contadorIndicador(element, 'listaCriterioExclusion', 'criterioExclusion', true);
              element.selected = true;
              this.step = element.codigo;
            }
            element.listaCriterioInclusion.forEach(inclusion => {
              inclusion.selected = false;
              inclusion.totalDesc = inclusion.descripcion.length;
              if (data.data.inclusionPrecarga != null) {
                data.data.inclusionPrecarga.forEach(inclusionPre => {
                  inclusion.selected = false;
                  if (inclusion.codigo === inclusionPre.codigo) {
                    inclusion.selected = true;
                    if (inclusionPre.criterioInclusion === 90 || inclusionPre.criterioInclusion === 1) {
                      inclusion.parametro = 1;
                      inclusion.criterioInclusion = 90;
                    } else if (inclusionPre.criterioInclusion === 91 || inclusionPre.criterioInclusion === 0) {
                      inclusion.parametro = 0;
                      inclusion.criterioInclusion = 91;
                    }
                  }
                });
              }
            });
            element.listaCriterioExclusion.forEach(exclusion => {
              exclusion.selected = false;
              exclusion.totalDesc = exclusion.descripcion.length;
              if (data.data.exclusionPrecarga != null) {
                data.data.exclusionPrecarga.forEach(exclusionPre => {
                  exclusion.selected = false;
                  if (exclusion.codigo === exclusionPre.codigo) {
                    exclusion.selected = true;
                    if (exclusionPre.criterioExclusion === 90 || exclusionPre.criterioExclusion === 1) {
                      exclusion.parametro = 1;
                      exclusion.criterioExclusion = 90;
                    } else if (exclusionPre.criterioExclusion === 91 || exclusionPre.criterioExclusion === 0) {
                      exclusion.parametro = 0;
                      exclusion.criterioExclusion = 91;
                    }
                  }
                });
              }
            });
          });
        } else {
          this.noCriteriosFlag = true;
          this.noCriterios = `No existen indicadores registrados para Mac: ${this.solicitud.descMAC}
            y Grupo Diagnostico ${this.solicitud.descGrupoDiagnostico}`;
          this.mensajes = data.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
        }

        this.barCriterios = false;
      },
      error => {
        console.error(error);
        this.mensajes = 'Error al listar los indicadores y sus criterios.';
        this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
        this.barCriterios = false;
        this.noCriteriosFlag = false;
      }
    );
  }

  public crearRequestGuardar(): void {
    this.listaIndicadorRequest = new ListaIndicadorRequest();
    this.listaIndicadorRequest.codSolEvaluacion = this.solicitud.codSolEvaluacion;
    this.listaIndicadorRequest.codMac = this.solicitud.codMac;
    this.listaIndicadorRequest.codGrpDiag = this.solicitud.codGrupoDiagnostico;
    this.listaIndicadorRequest.codigoIndicacion = this.step;
    this.listaIndicadorRequest.valCumpleChkListPer = this.rbgPerfilPcteFrmCtrl.value;
    this.listaIndicador.forEach(element => {
      if (element.codigo === this.listaIndicadorRequest.codigoIndicacion) {
        this.indicacionCriterio = element;
      }
    });
    this.listaIndicadorRequest.indicacionCriterio = this.indicacionCriterio;
    this.listaIndicadorRequest.comentario = this.comePerfcteFrmCtrl.value;
    this.listaIndicadorRequest.codigoRolUsuario = this.userService.getCodRol;
    this.listaIndicadorRequest.codigoUsuario = this.userService.getCodUsuario;
  }

  public insertarCheckListPaciente() {
    this.crearRequestGuardar();
    this.spinerService.show();
    this.detalleSolicitudEvaluacionService.
      insertarCheckListPaciente(this.listaIndicadorRequest).subscribe(
        (data: WsResponse) => {
          if (data.audiResponse.codigoRespuesta === '0') {
            this.grabarPaso = '1';
            this.btnSiguiente.emit(false);
            this.openDialogMensaje(MENSAJES.INFO_SUCCESS, null, true, false, null);
          } else {
            this.grabarPaso = '0';
            this.btnSiguiente.emit(false);
            this.mensajes = data.audiResponse.mensajeRespuesta;
            this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensajes, true, false, null);
          }
          this.spinerService.hide();
        }, error => {
          console.error(error);
          this.grabarPaso = '0';
          this.btnSiguiente.emit(false);
          this.mensajes = 'Error al grabar el perfil del paciente.';
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensajes, true, false, null);
          this.spinerService.hide();
        }
      );
  }

  public activarRadioBtn(item) {
    item.selected = true;
    this.listaIndicador.forEach(indicador => {
      if (item !== indicador) {
        indicador.selected = false;
      }
    });
    this.indicadorFrmCtrl.setValue(item);
  }

  public contadorIndicador(indicador, criterio, objeto, precarga) {
    let Si = 0;
    let No = 0;
    setTimeout(() => {
      indicador[criterio].forEach(x => {
        if (x.parametro === 1) {
          x[objeto] = 90;
          Si++;
        } else if (x.parametro === 0) {
          x[objeto] = 91;
          No++;
        }
      });
      indicador[criterio].totalSi = Si;
      indicador[criterio].totalNo = No;
      indicador[criterio].rptaCriterio = false;
      this.validandoCriterio(indicador, criterio, precarga);
    }, 500);
  }

  public validandoCriterio(indicador, criterio, precarga) {
    const criterioIndicador = indicador[criterio];
    if (criterioIndicador.length === (criterioIndicador.totalSi + criterioIndicador.totalNo)) {
      criterioIndicador.rptaCriterio = true;
      this.validarPerfilPaciente(indicador, precarga);
    } else {
      criterioIndicador.rptaCriterio = false;
    }
    return false;
  }



  public validarPerfilPaciente(indicador, precarga) {
    this.rptaPerfilPaciente = 0;
    if (indicador.listaCriterioInclusion.rptaCriterio && indicador.listaCriterioExclusion.rptaCriterio && !precarga) {
      this.rbgPerfilPcteFrmCtrl.enable();
      if (indicador.listaCriterioInclusion.totalSi === indicador.listaCriterioInclusion.length &&
        indicador.listaCriterioExclusion.totalNo === indicador.listaCriterioExclusion.length) {
        this.rbtPerfilPcte.forEach(element => {
          if (element.codigo === 1) {
            this.rptaPerfilPaciente = element.codigo;
            element.selected = true;
          } else {
            element.selected = false;
          }
        });
        this.rbgPerfilPcteFrmCtrl.setValue(1);
      } else {
        this.rbtPerfilPcte.forEach(element2 => {
          if (element2.codigo === 0) {
            this.rptaPerfilPaciente = element2.codigo;
            element2.selected = true;
          } else {
            element2.selected = false;
          }
        });
        this.rbgPerfilPcteFrmCtrl.setValue(0);
      }
      this.indicadorFrmCtrl.setValue(indicador);
    } else {
      this.rbgPerfilPcteFrmCtrl.enable();
      this.indicadorFrmCtrl.setValue(indicador);
    }
  }

  public verificarPerfil(perfilRpta) {
    const cambioPerfil: number = perfilRpta.codigo;
    if (this.rptaPerfilPaciente === cambioPerfil && this.grabarPaso !== '1') {
      this.flagComentarios = false; // this.comePerfcteFrmCtrl.disable();
      this.comePerfcteFrmCtrl.markAsUntouched();
      this.disableComentario = true;
    } else {
      this.flagComentarios = true; this.comePerfcteFrmCtrl.enable();
      this.disableComentario = false;
    }
  }

  public ValidarChkListPaciente() {
    if (this.indicadorFrmCtrl.value === null) {
      this.mensajes = 'Selecionar la Lista de Indicaciones del Medicamento de Alta Complejidad';
      return false;
    }

    let valido = false;

    if (this.rbgPerfilPcteFrmCtrl.value != null) {
      valido = true;
    }

    if (valido) {
      if (this.flagComentarios) {
        if (this.comePerfcteFrmCtrl.value !== null && this.comePerfcteFrmCtrl.value !== '') {
          return true;
        } else {
          this.mensajes = 'Debe ingresar un comentario, el perfil generado fue cambiado';
          this.comePerfcteFrmCtrl.markAsTouched();
          return false;
        }
      } else {
        return true;
      }
      /*if (this.comePerfcteFrmCtrl.enabled) {
        if (this.comePerfcteFrmCtrl.value !== null && this.comePerfcteFrmCtrl.value !== '') {
          return true;
        } else {
          this.mensajes = 'Debe ingresar un comentario, el perfil generado fue cambiado';
          this.comePerfcteFrmCtrl.markAsTouched();
          return false;
        }
      } else {
        return true;
      }
      return true;*/
    } else {
      this.mensajes = 'Falta seleccionar los criterios';
      return false;
    }
  }

  public guardarChkListPaciente() {
    if (this.ValidarChkListPaciente()) {
      this.insertarCheckListPaciente();
    } else {
      this.openDialogMensaje(MENSAJES.ERROR_CAMPOS, this.mensajes, true, false, null);
    }
  }
  // -------------  FIN PASO 4    ----------------

  // POP-UP MENSAJES
  public openDialogMensaje(
    message: String,
    message2: String,
    alerta: boolean,
    confirmacion: boolean,
    valor: any): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      disableClose: true,
      width: '400px',
      data: {
        title: MENSAJES.medicNuevo.chkListPaciente.TITLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(rspta => {
    });
  }

  public accesoOpcionMenu() {

    const data = require('src/assets/data/permisosRecursos.json');
    const bandejaEvaluacion = data.bandejaEvaluacion.paso4;
    this.opcionMenu = JSON.parse(localStorage.getItem('opcionMenu'));

    if (this.opcionMenu.opcion.length > 0) {
      this.opcionMenu.opcion.forEach(element => {
        const codOpcion = element.codOpcion;
        switch (codOpcion) {
          case bandejaEvaluacion.chkIndicacion:
                this.chkIndicacion = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.chkCriteriosInclusion:
                this.chkCriteriosInclusion = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.chkCriteriosExclusion:
                this.chkCriteriosExclusion = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.chkPaciente:
                this.chkPaciente = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.txtComentario:
                this.txtComentario = Number(element.flagAsignacion);
                break;
        }
      });
    }
  }

}
