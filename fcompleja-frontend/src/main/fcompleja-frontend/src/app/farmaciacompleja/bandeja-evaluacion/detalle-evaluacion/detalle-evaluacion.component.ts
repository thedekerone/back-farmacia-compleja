import { Component, OnInit, ViewChild, forwardRef, Inject } from '@angular/core';

// Others
import { Router } from '@angular/router';
import { SolbenRequest } from 'src/app/dto/request/SolbenRequest';
import { ESTADOMONITOREOMED, ESTADOEVALUACION, TIPOSCGSOLBEN, MENSAJES, MY_FORMATS_AUNA, EMAIL, PARAMETRO, FILEFTP, ROLES, TIPO_SOL_EVA, ACCESO_EVALUACION, FLAG_REGLAS_EVALUACION, ESTADO_LINEA_TRAT } from 'src/app/common';
import { ListaParametroservice } from 'src/app/service/lista.parametro.service';
import { EvaluacionAutorizadorRequest } from 'src/app/dto/request/EvaluacionAutorizadorRequest';
import { WsResponse } from 'src/app/dto/WsResponse';
import { LineaTratamiento } from 'src/app/dto/solicitudEvaluacion/bandeja/LineaTratamiento';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginatorIntlEspanol } from 'src/app/directives/matpaginator-translate';
import {
  MatPaginatorIntl,
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialog,
  DateAdapter,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS
} from '@angular/material';
import { InformeSolEvaReporteRequest } from 'src/app/dto/solicitudEvaluacion/detalle/InformacionScgEvaRequest';
import { listaLineaTratamientoRequest } from 'src/app/dto/solicitudEvaluacion/bandeja/ListaHisLineaTratamientoRequest';
import { DetalleSolicitudEvaluacionService } from 'src/app/service/detalle.solicitud.evaluacion.service';
import { BandejaEvaluacionService } from 'src/app/service/bandeja.evaluacion.service';
import { PreguntaLineaTratComponent } from './registro.historico.dialog.component';
import { ApiResponse } from 'src/app/dto/bandeja-preliminar/detalle-preliminar/ApiResponse';
import { ApiLineaTrataresponse } from 'src/app/dto/solicitudEvaluacion/bandeja/ApiLineaTrataresponse';
import { EvaluacionLiderTumorComponent } from '../evaluacion.lider.tumor/evaluacion.lider.tumor.component';
import { InfoSolben } from 'src/app/dto/bandeja-preliminar/detalle-preliminar/InfoSolben';
import { MessageComponent } from 'src/app/core/message/message.component';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { EvaluacionService } from 'src/app/dto/service/evaluacion.service';
import { DatePipe } from '@angular/common';
import { EmailDTO } from 'src/app/dto/core/EmailDTO';
import * as _moment from 'moment';
import { CorreosService } from 'src/app/service/cross/correos.service';
import { OncoWsResponse } from 'src/app/dto/response/OncoWsResponse';
import { UsuarioService } from 'src/app/dto/service/usuario.service';
import { EnvioCorreoRequest } from 'src/app/dto/core/EnvioCorreoRequest';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ArchivoRequest } from 'src/app/dto/request/ArchivoRequest';
import { CoreService } from 'src/app/service/core.service';
import { ArchivoFTP } from 'src/app/dto/bandeja-preliminar/detalle-preliminar/ArchivoFTP';
import { SolicitudEvaluacionRequest } from 'src/app/dto/request/SolicitudEvaluacionRequest';
import { BOpcionMenuLocalStorage } from 'src/app/dto/core/BOpcionMenuLocalStorage';

@Component({
  selector: 'app-detalle-evaluacion',
  templateUrl: './detalle-evaluacion.component.html',
  styleUrls: ['./detalle-evaluacion.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS_AUNA },
    {
      provide: MatPaginatorIntl,
      useClass: forwardRef(() => MatPaginatorIntlEspanol)
    }]
})
export class DetalleEvaluacionComponent implements OnInit {

  proBarTabla: boolean;
  mostrarBtnAlertMon : boolean=false;

  datosFrmGroup: FormGroup;
  detalleFrmGroup: FormGroup;

  public codSolEvaluacion: any;
  public flagLiderTumor: string;
  public step: number;

  public solbenRequest: SolbenRequest = new SolbenRequest();
  public evaAutorizadorRequest: EvaluacionAutorizadorRequest = new EvaluacionAutorizadorRequest();

  public mostrarBoton: boolean;

  request: InformeSolEvaReporteRequest = new InformeSolEvaReporteRequest();
  listaLineaTratamientoRequest: listaLineaTratamientoRequest = new listaLineaTratamientoRequest();
  rpta = {};
  listaHistoriaLineaTrata: LineaTratamiento[] = [];
  linea: String;

  codMac: any;
  codAfiliado: any;

  mostraEvaluacion: boolean;

  resultadoEvaluacionLiderTumor: any[] = [];
  correoRequest: EmailDTO;
  envioCorreoRequest: EnvioCorreoRequest;
  archivoRqt: ArchivoFTP;

  ESTADOEVALUACION: any = ESTADOEVALUACION;

  // DECLARACION DE LAS VARIABLES FORMGROUP Y FORMCONTROL DETALLE EVALUACION
  detEvaluacionFrmGrp: FormGroup = new FormGroup({
    codSolEvaluacionFrmCtrl: new FormControl(null),
    estadoDescripFrmCtrl: new FormControl(null),
    descripCodMacFrmCtrl: new FormControl(null),
    descripMacFrmCtrl: new FormControl(null),
    codScgSolbenFrmCtrl: new FormControl(null),
    estadoScgSolbenFrmCtrl: new FormControl(null),
    fechaScgSolbenFrmCtrl: new FormControl(null),
    tipoScgSolbenFrmCtrl: new FormControl(null),
    nroCartaGarantiaFrmCtrl: new FormControl(null),
    clinicaFrmCtrl: new FormControl(null),
    medicoTrataPrescripFrmCtrl: new FormControl(null),
    cmpMedicoFrmCtrl: new FormControl(null),
    fechaRecetaFrmCtrl: new FormControl(null),
    fechaQuimioterapiaFrmCtrl: new FormControl(null),
    fechaHospitalInicioFrmCtrl: new FormControl(null),
    fechaHospitalFinFrmCtrl: new FormControl(null),
    descripMedicamentoFrmCtrl: new FormControl(null),
    esquemaFrmCtrl: new FormControl(null),
    personaContactoFrmCtrl: new FormControl(null),
    totalPresupuestoFrmCtrl: new FormControl(null),
    pacienteFrmCtrl: new FormControl(null),
    edadFrmCtrl: new FormControl(null),
    descripDiagFrmCtrl: new FormControl(null),
    codDiagFrmCtrl: new FormControl(null),
    descripGrupoDiagFrmCtrl: new FormControl(null),
    contratanteFrmCtrl: new FormControl(null),
    planFrmCtrl: new FormControl(null),
    codAfiliadoFrmCtrl: new FormControl(null),
    fechaAfiliacionFrmCtrl: new FormControl(null),
    estadioClinicoFrmCtrl: new FormControl(null),
    tnmFrmCtrl: new FormControl(null),
    observacionFrmCtrl: new FormControl(null)
  });

  get codSolEvaluacionFrmCtrl() { return this.detEvaluacionFrmGrp.get('codSolEvaluacionFrmCtrl'); }
  get estadoDescripFrmCtrl() { return this.detEvaluacionFrmGrp.get('estadoDescripFrmCtrl'); }
  get descripCodMacFrmCtrl() { return this.detEvaluacionFrmGrp.get('descripCodMacFrmCtrl'); }
  get descripMacFrmCtrl() { return this.detEvaluacionFrmGrp.get('descripMacFrmCtrl'); }
  get codScgSolbenFrmCtrl() { return this.detEvaluacionFrmGrp.get('codScgSolbenFrmCtrl'); }
  get estadoScgSolbenFrmCtrl() { return this.detEvaluacionFrmGrp.get('estadoScgSolbenFrmCtrl'); }
  get fechaScgSolbenFrmCtrl() { return this.detEvaluacionFrmGrp.get('fechaScgSolbenFrmCtrl'); }
  get tipoScgSolbenFrmCtrl() { return this.detEvaluacionFrmGrp.get('tipoScgSolbenFrmCtrl'); }
  get nroCartaGarantiaFrmCtrl() { return this.detEvaluacionFrmGrp.get('nroCartaGarantiaFrmCtrl'); }
  get clinicaFrmCtrl() { return this.detEvaluacionFrmGrp.get('clinicaFrmCtrl'); }
  get medicoTrataPrescripFrmCtrl() { return this.detEvaluacionFrmGrp.get('medicoTrataPrescripFrmCtrl'); }
  get cmpMedicoFrmCtrl() { return this.detEvaluacionFrmGrp.get('cmpMedicoFrmCtrl'); }
  get fechaRecetaFrmCtrl() { return this.detEvaluacionFrmGrp.get('fechaRecetaFrmCtrl'); }
  get fechaQuimioterapiaFrmCtrl() { return this.detEvaluacionFrmGrp.get('fechaQuimioterapiaFrmCtrl'); }
  get fechaHospitalInicioFrmCtrl() { return this.detEvaluacionFrmGrp.get('fechaHospitalInicioFrmCtrl'); }
  get fechaHospitalFinFrmCtrl() { return this.detEvaluacionFrmGrp.get('fechaHospitalFinFrmCtrl'); }
  get descripMedicamentoFrmCtrl() { return this.detEvaluacionFrmGrp.get('descripMedicamentoFrmCtrl'); }
  get esquemaFrmCtrl() { return this.detEvaluacionFrmGrp.get('esquemaFrmCtrl'); }
  get personaContactoFrmCtrl() { return this.detEvaluacionFrmGrp.get('personaContactoFrmCtrl'); }
  get totalPresupuestoFrmCtrl() { return this.detEvaluacionFrmGrp.get('totalPresupuestoFrmCtrl'); }
  get pacienteFrmCtrl() { return this.detEvaluacionFrmGrp.get('pacienteFrmCtrl'); }
  get edadFrmCtrl() { return this.detEvaluacionFrmGrp.get('edadFrmCtrl'); }
  get descripDiagFrmCtrl() { return this.detEvaluacionFrmGrp.get('descripDiagFrmCtrl'); }
  get codDiagFrmCtrl() { return this.detEvaluacionFrmGrp.get('codDiagFrmCtrl'); }
  get descripGrupoDiagFrmCtrl() { return this.detEvaluacionFrmGrp.get('descripGrupoDiagFrmCtrl'); }
  get contratanteFrmCtrl() { return this.detEvaluacionFrmGrp.get('contratanteFrmCtrl'); }
  get planFrmCtrl() { return this.detEvaluacionFrmGrp.get('planFrmCtrl'); }
  get codAfiliadoFrmCtrl() { return this.detEvaluacionFrmGrp.get('codAfiliadoFrmCtrl'); }
  get fechaAfiliacionFrmCtrl() { return this.detEvaluacionFrmGrp.get('fechaAfiliacionFrmCtrl'); }
  get estadioClinicoFrmCtrl() { return this.detEvaluacionFrmGrp.get('estadioClinicoFrmCtrl'); }
  get tnmFrmCtrl() { return this.detEvaluacionFrmGrp.get('tnmFrmCtrl'); }
  get observacionFrmCtrl() { return this.detEvaluacionFrmGrp.get('observacionFrmCtrl'); }

  infoSolben: InfoSolben;
  mostrarCampoDetalle: number;
  mostrarFechaReceta: number;
  mostrarFechaQuimio: number;
  mostrarFechaHospital: number;

  flagVerActaCmac: number;
  reporteActaCmac: string;
  flagVerInforme: number;
  reportePdf: string;
  mensaje: string;
  codRolLiderTum: number;
  codUsrLiderTum: number;
  usrLiderTum: string;
  codArchFichaTec: number;
  codArchCompMed: number;

  public isLoading: boolean;
  public dataSource: MatTableDataSource<LineaTratamiento>;

  public columnsGrilla = [{
    codAcceso: ACCESO_EVALUACION.detalle.lineaTratamiento,
    columnDef: 'lineaTratamiento',
    header: 'LÍNEA DE TRATAMIENTO',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.lineaTratamiento !== null) ? `${lineaTrat.lineaTratamiento}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.medicamentoSolicitado,
    columnDef: 'macSolicitado',
    header: 'MEDICAMENTO SOLICITADO',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.macSolicitado !== null) ? `${lineaTrat.macSolicitado}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.numeroSolicitud,
    columnDef: 'codEvaluacion',
    header: 'N° SOLICITUD EVALUACIÓN',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.codEvaluacion !== null) ? `${lineaTrat.codEvaluacion}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.fechaAprobacion,
    columnDef: 'fec_Aprobacion',
    header: 'FECHA DE APROBACIÓN',
    cell: (lineaTrat: LineaTratamiento) =>
      (lineaTrat.fechaAprobacion !== null) ? `${this.datePipe.transform(lineaTrat.fechaAprobacion, 'dd/MM/yyyy')}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.fechaInicio,
    columnDef: 'fecchaInicio',
    header: 'FECHA INICIO',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.fechaInicio !== null) ? `${this.datePipe.transform(lineaTrat.fechaInicio, 'dd/MM/yyyy')}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.fechaFin,
    columnDef: 'fechaFin',
    header: 'FECHA FIN',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.fechaFin !== null) ? `${this.datePipe.transform(lineaTrat.fechaFin, 'dd/MM/yyyy')}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.numeroCursos,
    columnDef: 'nroCurso',
    header: 'N° CURSOS',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.nroCurso !== null) ? `${lineaTrat.nroCurso}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.tipoTumor,
    columnDef: 'tipoTumor',
    header: 'TIPO TUMOR',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.tipoTumor !== null) ? `${lineaTrat.tipoTumor}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.respuestaAlcanzada,
    columnDef: 'respAlcansada',
    header: 'RESPUESTA ALCANZADA',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.respAlcansada !== null) ? `${lineaTrat.respAlcansada}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.estado,
    columnDef: 'estado',
    header: 'ESTADO',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.estado !== null) ? `${lineaTrat.estado}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.motivoInactivacion,
    columnDef: 'motivoInactivacion',
    header: 'MOTIVO INACTIVACIÓN',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.motivoInactivacion !== null) ? `${lineaTrat.motivoInactivacion}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.medicoTratante,
    columnDef: 'medicoTratantePrescriptor',
    header: 'MÉDICO TRATANTE',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.medicoTratantePrescriptor !== null) ? `${lineaTrat.medicoTratantePrescriptor}` : ''
  }, {
    codAcceso: ACCESO_EVALUACION.detalle.montoAutorizado,
    columnDef: 'montoAutorizado',
    header: 'MONTO AUTORIZADO',
    cell: (lineaTrat: LineaTratamiento) => (lineaTrat.montoAutorizado !== null) ? `${lineaTrat.montoAutorizado}` : ''
  }];

  public displayedColumns: string[];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  opcionMenu: BOpcionMenuLocalStorage;
  txtCodigoSolicitud: number;
  txtEstadoSolicitud: number;
  txtCodigoMac: number;
  txtDescripcionMac: number;
  btnInforme: number;
  btnActaMac: number;
  txtNroSCG: number;
  txtEstadoSCG: number;
  txtFechaSCG: number;
  txtTipoSCG: number;
  txtNroCartaGarantiaDet: number;
  txtClinicaDet: number;
  txtMedicoTratante: number;
  txtCMP: number;
  txtFechaReceta: number;
  txtFechaQuimioterapia: number;
  txtFechaHospitalizacion: number;
  txtMedicamentos: number;
  txtEsquemaQuimioterapia: number;
  txtPersonaContacto: number;
  txtTotalPresupuesto: number;
  txtPacienteDet: number;
  txtEdad: number;
  txtDiagnostico: number;
  txtCie10: number;
  txtGrupoDiagnostico: number;
  txtContratante: number;
  txtPlan: number;
  txtCodigoAfiliado: number;
  txtFechaAfiliacion: number;
  txtEstadioClinico: number;
  txtTNM: number;
  txtObservacion: number;
  btnEnviarAlertaMonitoreo: number;
  btnRegistrarEvaAutorizador: number;
  btnRegistrarEvaLiderTumor: number;

  flagEvaluacion = FLAG_REGLAS_EVALUACION;
  valorMostrarOpcion = ACCESO_EVALUACION.mostrarOpcion;

  constructor(public dialog: MatDialog,
    private adapter: DateAdapter<any>,
    private detalleServicioSolEva: DetalleSolicitudEvaluacionService,
    private bandejaEvaluacionService: BandejaEvaluacionService,
    private router: Router,
    private listaParametroservice: ListaParametroservice,
    private datePipe: DatePipe,
    private correoService: CorreosService,
    private coreService: CoreService,
    private spinnerService: Ng4LoadingSpinnerService,
    @Inject(EvaluacionService) private solicitud: EvaluacionService,
    @Inject(UsuarioService) private userService: UsuarioService
  ) {
    this.adapter.setLocale('es-PE');
    this.accesoOpcionMenu();
  }

  ngOnInit() {
    this.capturarRegistroEval();
    this.inicializarVariables();
    this.definirTablaHistoria();
    this.consultarInformacionScgEva();
  }

  public inicializarVariables(): void {
    this.step = 0;

    this.codSolEvaluacion = this.solicitud.numeroSolEvaluacion;
    this.codMac = this.solicitud.codMac;
    this.request.codSolEva = this.solicitud.codSolEvaluacion;
    this.flagLiderTumor = this.solicitud.flagLiderTumor;
    this.dataSource = null;
    this.isLoading = false;
    this.infoSolben = new InfoSolben();
    this.mostrarCampoDetalle = TIPOSCGSOLBEN.mostrarCampoDetalle;
    this.estadioClinicoFrmCtrl.setValue('---');
    this.tnmFrmCtrl.setValue('---');

    if (typeof this.solicitud.codSolEvaluacion !== 'undefined') {
      this.solicitud.setEvaluacion = this.solicitud;
    }

    this.verificarTipoEvaluacion();
  }

  public definirTablaHistoria(): void {
    this.displayedColumns = [];
    this.columnsGrilla.forEach(c => {
      if (this.flagEvaluacion) {
        this.displayedColumns.push(c.columnDef);
      } else {
        this.opcionMenu.opcion.forEach(element => {
          if (c.codAcceso && c.codAcceso === element.codOpcion && Number(element.flagAsignacion) === ACCESO_EVALUACION.mostrarOpcion) {
            this.displayedColumns.push(c.columnDef);
          }
        });
      }
    });
  }

  public verificarTipoEvaluacion() {
    switch (this.solicitud.estadoEvaluacion + '') {
      case PARAMETRO.aprobadoEstado:
      case PARAMETRO.aprobadoTumorEstado:
      case PARAMETRO.aprobadoCMACEstado:
      case PARAMETRO.rechazadoEstado:
      case PARAMETRO.rechazadoTumorEstado:
      case PARAMETRO.rechazadoCMACEstado:
        this.mostraEvaluacion = false;
        break;
      default:
        this.mostraEvaluacion = true;
        break;
    }
  }

  public capturarRegistroEval(): void {
    if (typeof this.solicitud.codSolEvaluacion === 'undefined') {
      this.solicitud = this.solicitud.getEvaluacion;
    }
  }

  public cargarDatosTabla(): void {
    if (this.listaHistoriaLineaTrata.length > 0) {
      this.dataSource = new MatTableDataSource(this.listaHistoriaLineaTrata);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  public registrarEvaluacionAutorizador() {
    this.openDialogRegHistorico();
  }

  public openDialogRegHistorico(): void {

    if (this.validarEstadoSolicitud()) {
      const dialogRef = this.dialog.open(PreguntaLineaTratComponent, {
        disableClose: true,
        width: '500px',
        data: { codMacEvaluacion: this.codMac, codAfiliado: this.solicitud.codAfiliado }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 1) {
          this.router.navigate(['./app/registro-linea-tratamiento']);
        } else {
          const codMacEvaluacion = this.solicitud.codMac;
          const codAfiliado = this.solicitud.codAfiliado;
          this.consultar(codAfiliado, codMacEvaluacion);
        }
      });
    }
  }

  public validarEstadoSolicitud(): boolean {
    if (this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoAprobadoAutorizador) {
      this.mensaje = 'La solicitud de evaluación ya fue aprobada.';
      this.openDialogMensaje(this.mensaje, null, true, false, this.solicitud.numeroSolEvaluacion);
      return false;
    }

    if (this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoRechazadoAutorizador) {
      this.mensaje = 'La solicitud de evaluación fue rechazada.';
      this.openDialogMensaje(this.mensaje, null, true, false, this.solicitud.numeroSolEvaluacion);
      return false;
    }

    if (this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoObservadoAutorizador) {
      this.mensaje = 'La solicitud de evaluación se encuentra observada.';
      this.openDialogMensaje(this.mensaje, null, true, false, this.solicitud.numeroSolEvaluacion);
      return false;
    }

    if (this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoAprobadoLiderTumor) {
      this.mensaje = 'La solicitud de evaluación se encuentra observada.';
      this.openDialogMensaje(this.mensaje, null, true, false, this.solicitud.numeroSolEvaluacion);
      return false;
    }

    if (this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoRechazadoLiderTumor) {
      this.mensaje = 'La solicitud de evaluación se encuentra observada.';
      this.openDialogMensaje(this.mensaje, null, true, false, this.solicitud.numeroSolEvaluacion);
      return false;
    }

    if (this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoObservadoLiderTumor) {
      this.mensaje = 'La solicitud de evaluación se encuentra observada por Lider Tumor.';
      this.openDialogMensaje(this.mensaje, null, true, false, this.solicitud.numeroSolEvaluacion);
      return false;
    }

    if (this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoAprobadoCMAC) {
      this.mensaje = 'La solicitud de evaluación se encuentra aprobada por Autorizador CMAC.';
      this.openDialogMensaje(this.mensaje, null, true, false, this.solicitud.numeroSolEvaluacion);
      return false;
    }

    if (this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoRechazadoCMAC) {
      this.mensaje = 'La solicitud de evaluación se encuentra rechazada por CMAC.';
      this.openDialogMensaje(this.mensaje, null, true, false, this.solicitud.numeroSolEvaluacion);
      return false;
    }

    if (this.descripGrupoDiagFrmCtrl.value === null) {
      this.mensaje = 'Error de comunicacion con el Servicio Oncosys, no se obtiene el Grupo de Diagnostico';
      return false;
    }

    return true;
  }

  public consultar(codAfiliado: any, codMacEvaluacion: any) {

    this.solbenRequest.codAfiliado = codAfiliado;
    this.spinnerService.show();
    this.detalleServicioSolEva.consultarEvaluacionAutorizador(this.solbenRequest).subscribe(
      (data: ApiResponse) => {
        if (data.status === '0') {
          if (data.response.length > 0) {
            const codMacHistorico = data.response[0].codMac;
            const estadoMonitoreoMed = data.response[0].estadoMonitoreoMedic;
            this.solicitud.nroLineaTratamiento = data.response[0].nroLineaTratamiento;
            if (codMacEvaluacion === codMacHistorico) {
              this.solicitud.codLineaTratamiento = data.response[0].codHistLineaTrat;
              //this.solicitud.fechaSolEva = data.response[0].fecSolEva;
              this.solicitud.codGrupoDiagnostico = data.response[0].codGrpDiag;
              // insertar parametro P_ESTADO = MEDICAMENTO CONTINUADOR
              this.router.navigate(['/app/medicamento-continuador']);
              this.actualizarTipoSolEvaluacion(this.solicitud.numeroSolEvaluacion, false); // FALSE MED-CONTINUADOR
              this.spinnerService.hide();
              // P_TIPO_EVA
            } else if (ESTADOMONITOREOMED.estadoInactivo === estadoMonitoreoMed) {
              this.router.navigate(['./app/medicamento-nuevo']);
              this.actualizarTipoSolEvaluacion(this.solicitud.numeroSolEvaluacion, true); // FALSE MED-NUEVO
              this.spinnerService.hide();
              // INSERTAR parametro P_ESTADO = MED NUEVO
            } else if (codMacEvaluacion !== codMacHistorico && ESTADOMONITOREOMED.estadoActivo === estadoMonitoreoMed) {
              this.enviarEmailMonitoreoImplicito();
            }
          } else {
            this.solicitud.nroLineaTratamiento = 0;
            this.actualizarTipoSolEvaluacion(this.solicitud.numeroSolEvaluacion, true);// FALSE MED-NUEVO
            this.spinnerService.hide();
            this.router.navigate(['./app/medicamento-nuevo']);
          }
        } else {
          console.error(data);
          this.mensaje = 'No se logró obtener la información correctamente.';
          this.openDialogMensaje(this.mensaje, null, true, false, null);
          this.spinnerService.hide();
        }
      },
      error => {
        console.error(error);
        this.mensaje = 'Error al consultar el tipo de evaluación del autorizador.';
        this.openDialogMensaje(this.mensaje, null, true, false, null);
        this.spinnerService.hide();
      }
    );
  }

  public openDialogRegEvaluacionLTumor(): void {
    const dialogRef = this.dialog.open(PreguntaLineaTratComponent, {
      disableClose: true,
      width: '600px',
      data: { codMacEvaluacion: this.codMac, codAfiliado: this.solicitud.codAfiliado }
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  public guardarDetalleEvaluacion(): void {
    this.solicitud.codDiagnostico = this.infoSolben.codDiagnostico;
    this.solicitud.descDiagnostico = this.infoSolben.diagnostico;
    this.solicitud.codAfiliado = this.infoSolben.codAfiliado;
    this.solicitud.paciente = this.infoSolben.paciente;
    this.solicitud.sexoPaciente = this.infoSolben.sexoPaciente;
    this.solicitud.descDiagnostico = this.infoSolben.diagnostico;
    this.solicitud.codGrupoDiagnostico = this.infoSolben.codGrupoDiagnostico;
    this.solicitud.descGrupoDiagnostico = this.infoSolben.grupoDiagnostico;
    this.solicitud.edad = this.infoSolben.edad;
    this.solicitud.codArchFichaTec = this.codArchFichaTec;
    this.solicitud.codArchCompMed = this.codArchCompMed;
    this.solicitud.codInformePDF = Number(this.reportePdf);
    this.solicitud.codCmacPDF = Number(this.reporteActaCmac);
    this.solicitud.codCmp = this.infoSolben.cmpMedico;
    this.solicitud.fechaSolEva = this.infoSolben.fecSolEva;
  }

  /**Funcionalidad de Detalle Solicitud Evaluacion */
  public consultarInformacionScgEva() {
    this.proBarTabla = true;
    this.detalleServicioSolEva
      .consultarInformacionScgEva(this.request)
      .subscribe(
        (data: WsResponse) => {
          if (data.audiResponse.codigoRespuesta === '0') {
            this.infoSolben = data.data.solbenBean;
            this.flagVerActaCmac = data.data.flagVerActaCmac;
            this.reporteActaCmac = data.data.reporteActaCmac;
            this.flagVerInforme = data.data.flagVerInforme;
            this.reportePdf = data.data.reportePdf;
            this.codArchCompMed = data.data.codArchComplMed;
            this.codArchFichaTec = data.data.codArchFichaTec;
            this.guardarDetalleEvaluacion();
            if (this.infoSolben.codAfiliado != null) {
              this.listaLineaTratamientoRequest.codigoAfiliado = this.infoSolben.codAfiliado;
              this.listaHistorialdelineas();
            }

            if (this.infoSolben.estadoSolEva === ESTADOEVALUACION.estadoObservadoAutorizador) {
              this.validacionLiderTumor();
            }

            this.mostrarInformacionSCG();
          } else {
            this.mensaje = 'Error al consultar la información de la Evaluación.';
            this.openDialogMensaje(MENSAJES.INFO_NO_DATA, this.mensaje, true, false, null);
          }
          this.proBarTabla = false;
        },
        error => {
          console.error(error);
          this.mensaje = 'Error al obtener el detalle de la Evaluacion.';
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensaje, true, false, null);
          this.proBarTabla = false;
        }
      );
  }

  public mostrarInformacionSCG(): void {
    this.codSolEvaluacionFrmCtrl.setValue(this.codSolEvaluacion);
    this.estadoDescripFrmCtrl.setValue(this.infoSolben.descripEstadoSolEva);
    this.descripCodMacFrmCtrl.setValue(this.infoSolben.descripCodMac);
    this.descripMacFrmCtrl.setValue(this.infoSolben.descripcionMac);
    this.codScgSolbenFrmCtrl.setValue(this.infoSolben.nroSCGSolben);
    this.estadoScgSolbenFrmCtrl.setValue(this.infoSolben.estadoSCGSolben);
    this.fechaScgSolbenFrmCtrl.setValue(this.datePipe.transform(this.infoSolben.fecSCGSolben, 'dd/MM/yyyy') + ' ' + this.infoSolben.horaSCGSolben);
    this.tipoScgSolbenFrmCtrl.setValue(this.infoSolben.tipoSolben);
    this.nroCartaGarantiaFrmCtrl.setValue(this.infoSolben.nroCartaGarantia);
    this.clinicaFrmCtrl.setValue(this.infoSolben.clinica);
    this.medicoTrataPrescripFrmCtrl.setValue(this.infoSolben.medicoTratante);
    this.cmpMedicoFrmCtrl.setValue(this.infoSolben.cmpMedico);
    this.mostrarFechaReceta = (this.infoSolben.codTipoSolben === TIPOSCGSOLBEN.farmaciaCompleja ||
      this.infoSolben.codTipoSolben === TIPOSCGSOLBEN.quimioAmbulatoria) ?
      TIPOSCGSOLBEN.mostrarCampoDetalle : TIPOSCGSOLBEN.ocultarCampoDetalle;
    this.fechaRecetaFrmCtrl.setValue(this.infoSolben.fechaReceta);
    this.mostrarFechaQuimio = (this.infoSolben.codTipoSolben === TIPOSCGSOLBEN.quimioAmbulatoria) ?
      TIPOSCGSOLBEN.mostrarCampoDetalle : TIPOSCGSOLBEN.ocultarCampoDetalle;
    this.fechaQuimioterapiaFrmCtrl.setValue(this.infoSolben.fechaQuimio);
    this.mostrarFechaHospital = (this.infoSolben.codTipoSolben === TIPOSCGSOLBEN.hospitalizacion) ?
      TIPOSCGSOLBEN.mostrarCampoDetalle : TIPOSCGSOLBEN.ocultarCampoDetalle;
    this.fechaHospitalInicioFrmCtrl.setValue(this.infoSolben.fechaHospitalInicio);
    this.fechaHospitalFinFrmCtrl.setValue(this.infoSolben.fechaHospitalFin);
    this.descripMedicamentoFrmCtrl.setValue((this.infoSolben.medicamentos !== null) ?
      this.infoSolben.medicamentos.replace(/\|/g, ',') : null);
    this.esquemaFrmCtrl.setValue((this.infoSolben.esquemaQuimio !== null) ?
      this.infoSolben.esquemaQuimio.replace(/\|/g, ',') : null);
    this.personaContactoFrmCtrl.setValue(this.infoSolben.personaContacto);
    this.totalPresupuestoFrmCtrl.setValue(this.infoSolben.totalPresupuesto); // TO-DO FALTA DECIMAL
    this.pacienteFrmCtrl.setValue(this.infoSolben.paciente);
    this.edadFrmCtrl.setValue(this.infoSolben.edad);
    this.descripDiagFrmCtrl.setValue(this.infoSolben.diagnostico);
    this.codDiagFrmCtrl.setValue(this.infoSolben.codDiagnostico);
    this.descripGrupoDiagFrmCtrl.setValue(this.infoSolben.grupoDiagnostico);
    this.contratanteFrmCtrl.setValue(this.infoSolben.contratante);
    this.planFrmCtrl.setValue(this.infoSolben.plan);
    this.codAfiliadoFrmCtrl.setValue(this.infoSolben.codAfiliado);
    this.fechaAfiliacionFrmCtrl.setValue(this.infoSolben.fechaAfiliado);
    this.estadioClinicoFrmCtrl.setValue('---');
    this.tnmFrmCtrl.setValue('---');
    this.observacionFrmCtrl.setValue(this.infoSolben.observacion);
  }

  public listaHistorialdelineas() {
    this.dataSource = null;
    this.listaHistoriaLineaTrata = [];
    this.isLoading = true;
    this.bandejaEvaluacionService
      .listarHistorialLinea(this.listaLineaTratamientoRequest)
      .subscribe(
        (data: ApiLineaTrataresponse) => {
          console.log('DATA LINEAS');
          if (data.status === '0') {
            this.listaHistoriaLineaTrata = (data.response.lista != null) ? data.response.lista : [];
            this.cargarDatosTabla();
            this.mostrarBotonEnvioEmail();
          } else {
            this.openDialogMensaje(data.message, null, true, false, null);
          }
          this.isLoading = false;
        },
        error => {
          console.error('Error al lista el historial de linea de tratamiento');
          this.isLoading = false;
        }
      );
  }

  public validacionLiderTumor() {
    if (this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoAprobadoLiderTumor ||
      this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoRechazadoLiderTumor ||
      this.solicitud.estadoEvaluacion === ESTADOEVALUACION.estadoObservadoLiderTumor) {
      this.mostrarBoton = false;
      return;
    }

    this.evaAutorizadorRequest.cmpMedico = this.infoSolben.cmpMedico;
    this.evaAutorizadorRequest.codGrpDiag = this.infoSolben.codGrupoDiagnostico;
    const estadoEvaluacion = this.solicitud.estadoEvaluacion;

    this.listaParametroservice
      .consultarPValidacionAutorizador(this.evaAutorizadorRequest)
      .subscribe((response: WsResponse) => {
        if (estadoEvaluacion === ESTADOEVALUACION.estadoObservadoAutorizador) {
          if (response.audiResponse.codigoRespuesta === '0') {
            this.mostrarBoton = true;
            this.solicitud.codRolLiderTum = response.data.codigoRol;
            this.solicitud.codUsrLiderTum = response.data.codUsuario;
            this.solicitud.usrLiderTum = response.data.nombreUsuarioRol;
          } else if (response.audiResponse.codigoRespuesta === '2' || response.audiResponse.codigoRespuesta === '3' ||
            response.audiResponse.codigoRespuesta === '4') {
            this.mensaje = response.audiResponse.mensajeRespuesta;
            this.mostrarBoton = false;
            this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensaje, true, false, null);
          } else {
            this.mensaje = response.audiResponse.mensajeRespuesta;
            this.mostrarBoton = false;
            this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensaje, true, false, null);
          }
        } else {
          this.mostrarBoton = false;
        }
      },
        error => {
          console.error(error);
          this.mensaje = 'Error al obtener la validación para Evaluación del Lider Tumor';
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, this.mensaje, true, false, null);
        }
      );
  }

  public openDialog(): void {
    if (this.solicitud.codUsrLiderTum === null || this.solicitud.usrLiderTum === null) {
      this.mensaje = 'No se encuentra configurado los datos del Líder de Tumor para el Grupo de Diagnóstico.';
      this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, this.solicitud.descGrupoDiagnostico);
      return;
    } else if (this.solicitud.usrLiderTum === null || this.solicitud.usrLiderTum.trim() === '') {
      this.mensaje = 'No se encuentra el nombre del usuario Líder de Tumor asignado al Grupo de Diagnóstico.';
      this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, this.solicitud.descGrupoDiagnostico);
      return;
    }
    const dialogRef = this.dialog.open(EvaluacionLiderTumorComponent, {
      disableClose: true,
      width: '550px',
      data: { title: 'EVALUACION DEL LIDER TUMOR' }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  public setStep(index: number): void {
    this.step = index;
  }

  public descargarDocumento(): void {
    this.spinnerService.show();
    this.coreService.descargarArchivoFTP(this.archivoRqt).subscribe(
      (response: WsResponse) => {
        if (response.audiResponse.codigoRespuesta === '0') {
          response.data.contentType = 'application/pdf';
          const blob = this.coreService.crearBlobFile(response.data);
          const link = document.createElement('a');
          link.target = '_blank';
          link.href = window.URL.createObjectURL(blob);
          link.setAttribute('download', response.data.nomArchivo);
          link.click();
          this.spinnerService.hide();
        } else {
          this.mensaje = response.audiResponse.mensajeRespuesta;
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, null);
          this.spinnerService.hide();
        }
      }, (error) => {
        console.error(error);
        this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, this.mensaje, true, false, null);
        this.spinnerService.hide();
      }
    );
  }

  public visualizarInformeAutorizador(): void {
    this.archivoRqt = new ArchivoFTP();
    this.archivoRqt.codArchivo = this.solicitud.codInformePDF;
    this.archivoRqt.ruta = FILEFTP.rutaInformeAutorizador;
    this.descargarDocumento();
  }

  public visualizarActaPDF() {
    this.archivoRqt = new ArchivoFTP();
    this.archivoRqt.codArchivo = this.solicitud.codCmacPDF;
    this.archivoRqt.ruta = FILEFTP.rutaInformeAutorizador;
    this.descargarDocumento();
  }

  public openDialogMensaje(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.EVALUACION.DETALLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  public openDialogMensajeEstadoSolicitud(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.EVALUACION.DETALLE,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.consultarInformacionScgEva();
    });
  }

  public enviarEmailMonitoreo() {
    this.spinnerService.show();
    const date = new Date();
    this.correoRequest = new EmailDTO();
    this.correoRequest.codPlantilla = EMAIL.EVALUACION_MONITOREO.codigoPlantilla;
    this.correoRequest.fechaProgramada = _moment(date).format('DD/MM/YYYY HH:mm');
    this.correoRequest.flagAdjunto = EMAIL.EVALUACION_MONITOREO.flagAdjunto;
    this.correoRequest.tipoEnvio = EMAIL.EVALUACION_MONITOREO.tipoEnvio;
    this.correoRequest.usrApp = EMAIL.usrApp;

    this.correoService.generarCorreo(this.correoRequest).subscribe(
      (response: OncoWsResponse) => {
        let result = '';
        let codigoEnvio;
        const lista: any = response.dataList;

        result += lista[0].cuerpo.toString()
          .replace('{{paciente}}', this.pacienteFrmCtrl.value != null ? this.pacienteFrmCtrl.value : ' ')
          .replace('{{descripcionMac}}', this.descripMacFrmCtrl.value != null ? this.descripMacFrmCtrl.value : ' ')
          .replace('{{nroScgSolben}}', this.codScgSolbenFrmCtrl.value != null ? this.codScgSolbenFrmCtrl.value : ' ')
          .replace('{{codSolEvaluacion}}', this.codSolEvaluacionFrmCtrl.value != null ? this.codSolEvaluacionFrmCtrl.value : ' ')
          .replace('{{medicoAutorizador}}', this.userService.getNombres + ' ' + this.userService.getApelPaterno + ' ' + this.userService.getApelMaterno);
        codigoEnvio = lista[0].codigoEnvio;

        this.correoRequest.asunto = EMAIL.EVALUACION_MONITOREO.asunto.concat(this.pacienteFrmCtrl.value != null ? this.pacienteFrmCtrl.value : ' ');
        this.correoRequest.cuerpo = result;
        this.correoRequest.codigoEnvio = codigoEnvio;
        this.correoRequest.ruta = '';
        this.correoRequest.codigoPlantilla = this.correoRequest.codPlantilla;
        this.correoRequest.codigoGrupoDiagnostico = this.infoSolben.codGrupoDiagnostico;
        this.correoRequest.edadPaciente = this.infoSolben.edad;
        this.correoRequest.codRol = ROLES.responsableMonitoreo;
        this.envioCorreoRequest = new EnvioCorreoRequest();
        this.envioCorreoRequest.codSolicitudEvaluacion = this.codSolEvaluacionFrmCtrl.value;
        this.envioCorreoRequest.codigoEnvio = codigoEnvio;
        this.envioCorreoRequest.usrApp = EMAIL.usrApp;

        this.correoService.finalizarEnvioCorreo(this.correoRequest).subscribe(
          (response: OncoWsResponse) => {
            if (response.audiResponse.codigoRespuesta == '0') {
              this.spinnerService.hide();
              this.verConfirmacion("Envio de correo", "su correo está en proceso de envio", null);
              this.correoService.actualizarCodigoEnvio(this.envioCorreoRequest).subscribe(
                (response: OncoWsResponse) => {
                }, error => {
                  console.error(error);
                });
            } else {
              this.spinnerService.hide();
              this.verConfirmacion("Envio de correo", response.audiResponse.mensajeRespuesta, null);
            }
          }, error => {
            this.spinnerService.hide();
            console.error(error);
            this.verConfirmacion("Envio de correo", "Error al enviar correo", null);
          });
      }, error => {
        this.spinnerService.hide();
        console.error(error);
        this.verConfirmacion("Envio de correo", "Error al enviar correo", null);
      }
    );
  }

  public verConfirmacion(titulo: string, message: string, message2: string): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: titulo,
        message: message,
        message2: message2,
        alerta: true,
        confirmacion: false,
        valor: null
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        if (result == 1) {//DESEA MANTENER SIN REGISTRO EL MARCADOR 1=>SI 0=>NO

        } else {

        }
      }
    });
  }

  public enviarEmailMonitoreoImplicito() {
    const date = new Date();
    this.correoRequest = new EmailDTO();
    this.correoRequest.codPlantilla = EMAIL.EVALUACION_MONITOREO.codigoPlantilla;
    this.correoRequest.fechaProgramada = _moment(date).format('DD/MM/YYYY HH:mm');
    this.correoRequest.flagAdjunto = EMAIL.EVALUACION_MONITOREO.flagAdjunto;
    this.correoRequest.tipoEnvio = EMAIL.EVALUACION_MONITOREO.tipoEnvio;
    this.correoRequest.usrApp = EMAIL.usrApp;

    this.correoService.generarCorreo(this.correoRequest).subscribe(
      (response: OncoWsResponse) => {
        let result = '';
        let codigoEnvio;
        const lista: any = response.dataList;

        result += lista[0].cuerpo.toString()
          .replace('{{paciente}}', this.pacienteFrmCtrl.value != null ? this.pacienteFrmCtrl.value : " ")
          .replace('{{descripcionMac}}', this.descripMacFrmCtrl.value != null ? this.descripMacFrmCtrl.value : " ")
          .replace('{{nroScgSolben}}', this.codScgSolbenFrmCtrl.value != null ? this.codScgSolbenFrmCtrl.value : " ")
          .replace('{{codSolEvaluacion}}', this.codSolEvaluacionFrmCtrl.value != null ? this.codSolEvaluacionFrmCtrl.value : " ")
          .replace('{{medicoAutorizador}}', this.userService.getNombres + ' ' + this.userService.getApelPaterno + ' ' + this.userService.getApelMaterno);
        codigoEnvio = lista[0].codigoEnvio;

        this.correoRequest.asunto = EMAIL.EVALUACION_MONITOREO.asunto.concat(this.pacienteFrmCtrl.value != null ? this.pacienteFrmCtrl.value : " ");
        this.correoRequest.cuerpo = result;
        this.correoRequest.codigoEnvio = codigoEnvio;
        this.correoRequest.ruta = "";
        this.correoRequest.codigoPlantilla = this.correoRequest.codPlantilla;
        this.correoRequest.codigoGrupoDiagnostico = this.infoSolben.codGrupoDiagnostico;
        this.correoRequest.edadPaciente = this.infoSolben.edad;
        this.correoRequest.codRol = ROLES.responsableMonitoreo;
        this.envioCorreoRequest = new EnvioCorreoRequest();
        this.envioCorreoRequest.codSolicitudEvaluacion = this.codSolEvaluacionFrmCtrl.value;
        this.envioCorreoRequest.codigoEnvio = codigoEnvio;
        this.envioCorreoRequest.usrApp = EMAIL.usrApp;

        this.correoService.finalizarEnvioCorreo(this.correoRequest).subscribe(
          (response: OncoWsResponse) => {
            if (response.audiResponse.codigoRespuesta == '0') {
              this.spinnerService.hide();
              this.verConfirmacion("Envio de correo", null, "*Existe un monitoreo pendiente de registro de resultado.\n*Se envio un correo al Responsable de Monitoreo (verifique el estado de envio del email con el boton 'VER ESTADO DE CORREO')");
              this.correoService.actualizarCodigoEnvio(this.envioCorreoRequest).subscribe(
                (response: OncoWsResponse) => {
                }, error => {
                  console.error(error);
                });
            } else {
              this.spinnerService.hide();
              this.verConfirmacion("Envio de correo", null, "*Existe un monitoreo pendiente de registro de resultado.\n*" + response.audiResponse.mensajeRespuesta);
            }
          }, error => {
            this.spinnerService.hide();
            console.error(error);
            this.verConfirmacion("Envio de correo", null, "*Existe un monitoreo pendiente de registro de resultado.\n*" + response.audiResponse.mensajeRespuesta);
          });
      }, error => {
        this.spinnerService.hide();
        console.error(error);
        this.verConfirmacion("Envio de correo", null, "*Existe un monitoreo pendiente de registro de resultado.\n*Error de generacion de correo para envio al Responsable de Monitoreo");
      }
    );
  }

  public refrescarEstadoSolicitud() {
    this.spinnerService.show();
    this.envioCorreoRequest = new EnvioCorreoRequest();
    this.envioCorreoRequest.codSolicitudEvaluacion = this.codSolEvaluacionFrmCtrl.value;
    this.envioCorreoRequest.usrApp = EMAIL.usrApp;
    this.correoService.verificarCodigoEnvio(this.envioCorreoRequest).subscribe(
      (response: OncoWsResponse) => {
        if (response.audiResponse.codigoRespuesta === '0') {
          this.mensaje = MENSAJES.EVALUACION.CORREO_ENVIADO;
          this.openDialogMensajeEstadoSolicitud(this.mensaje, null, true, false, null);
        } else {
          this.openDialogMensajeEstadoSolicitud(response.audiResponse.mensajeRespuesta, '', true, false, null);
        }
        this.spinnerService.hide();
      }, error => {
        this.spinnerService.hide();
        this.openDialogMensajeEstadoSolicitud(MENSAJES.EVALUACION.ERROR_VERIFICAR_CORREO, null, true, false, null);
        console.error(error);
      });
  }

  public actualizarTipoSolEvaluacion(codSolEva: string, tipo: boolean) {
    let solEvaRequest = new SolicitudEvaluacionRequest();
    solEvaRequest.codSolicitudEvaluacion = Number(codSolEva);
    if (tipo) {//SI IGUAL TRUE => NUEVO   -   FALSE=> CONTINUADOR
      solEvaRequest.pTipoEva = TIPO_SOL_EVA.medicamentoNuevo;
    } else {
      solEvaRequest.pTipoEva = TIPO_SOL_EVA.continuador;
    }

    this.detalleServicioSolEva.actualizarTipoSolEvaluacion(solEvaRequest).subscribe(
      (response: WsResponse) => {
      }, error => {
        console.error(error);
      });
  }

  public mostrarBotonEnvioEmail(){
    if(this.listaHistoriaLineaTrata[0].pEstado==ESTADO_LINEA_TRAT.estadoActivo){
      this.mostrarBtnAlertMon = true;
    }
  }

  public accesoOpcionMenu() {

    const data = require('src/assets/data/permisosRecursos.json');
    const bandejaEvaluacion = data.bandejaEvaluacion.detalle;
    this.opcionMenu = JSON.parse(localStorage.getItem('opcionMenu'));

    if (this.opcionMenu.opcion.length > 0) {
      this.opcionMenu.opcion.forEach(element => {
        const codOpcion = element.codOpcion;
        switch (codOpcion) {
          case bandejaEvaluacion.txtCodigoSolicitud:
            this.txtCodigoSolicitud = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtEstadoSolicitud:
            this.txtEstadoSolicitud = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtCodigoMac:
            this.txtCodigoMac = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtDescripcionMac:
            this.txtDescripcionMac = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnInforme:
            this.btnInforme = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnActaMac:
            this.btnActaMac = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtNroSCG:
            this.txtNroSCG = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtEstadoSCG:
            this.txtEstadoSCG = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtFechaSCG:
            this.txtFechaSCG = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtTipoSCG:
            this.txtTipoSCG = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtNroCartaGarantiaDet:
            this.txtNroCartaGarantiaDet = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtClinicaDet:
            this.txtClinicaDet = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtMedicoTratante:
            this.txtMedicoTratante = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtCMP:
            this.txtCMP = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtFechaReceta:
            this.txtFechaReceta = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtFechaQuimioterapia:
            this.txtFechaQuimioterapia = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtFechaHospitalizacion:
            this.txtFechaHospitalizacion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtMedicamentos:
            this.txtMedicamentos = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtEsquemaQuimioterapia:
            this.txtEsquemaQuimioterapia = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtPersonaContacto:
            this.txtPersonaContacto = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtTotalPresupuesto:
            this.txtTotalPresupuesto = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtPacienteDet:
            this.txtPacienteDet = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtEdad:
            this.txtEdad = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtDiagnostico:
            this.txtDiagnostico = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtCie10:
            this.txtCie10 = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtGrupoDiagnostico:
            this.txtGrupoDiagnostico = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtContratante:
            this.txtContratante = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtPlan:
            this.txtPlan = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtCodigoAfiliado:
            this.txtCodigoAfiliado = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtFechaAfiliacion:
            this.txtFechaAfiliacion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtEstadioClinico:
            this.txtEstadioClinico = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtTNM:
            this.txtTNM = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.txtObservacion:
            this.txtObservacion = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnEnviarAlertaMonitoreo:
            this.btnEnviarAlertaMonitoreo = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnRegistrarEvaAutorizador:
            this.btnRegistrarEvaAutorizador = Number(element.flagAsignacion);
            break;
          case bandejaEvaluacion.btnRegistrarEvaLiderTumor:
            this.btnRegistrarEvaLiderTumor = Number(element.flagAsignacion);
            break;
        }
      });
    }
  }
}
