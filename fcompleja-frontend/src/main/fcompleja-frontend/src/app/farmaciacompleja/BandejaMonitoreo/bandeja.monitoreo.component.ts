import { Component, OnInit, ViewChild, forwardRef, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Parametro } from 'src/app/dto/Parametro';
import {
  MatDialog,
  MatPaginator,
  MatSort,
  MatTableDataSource,
  DateAdapter,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS,
  MatPaginatorIntl,
  MatIconRegistry
} from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MY_FORMATS_AUNA, ROLES, GRUPO_PARAMETRO, ACCESO_MONITOREO, FLAG_REGLAS_MONITOREO, ESTADO_MONITOREO } from 'src/app/common';
import { MatPaginatorIntlEspanol } from 'src/app/directives/matpaginator-translate';
import { DomSanitizer } from '@angular/platform-browser';
import { BandejaMonitoreoService } from 'src/app/service/BandejaMonitoreo/bandeja.monitoreo.service';
import { BandejaMonitoreoRequest } from 'src/app/dto/request/BandejaMonitoreo/BandejaMonitoreoRequest';
import { WsResponse } from 'src/app/dto/WsResponse';
import { ListaFiltroUsuarioRolservice } from 'src/app/service/Lista.usuario.rol.service';
import { ListaParametroservice } from 'src/app/service/lista.parametro.service';
import { ParametroRequest } from 'src/app/dto/ParametroRequest';
import { ExcelDownloadResponse } from 'src/app/dto/ExcelDownloadResponse';
import { ExcelExportService } from 'src/app/service/excel.bandeja.pre.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BuscarPacienteComponent } from 'src/app/modal/buscar-paciente/buscar-paciente.component';
import { Paciente } from 'src/app/dto/Paciente';
import { BuscarClinicaComponent } from 'src/app/modal/buscar-clinica/buscar-clinica.component';
import { Clinica } from 'src/app/dto/Clinica';
import { DatePipe } from '@angular/common';
import { UsuarioService } from 'src/app/dto/service/usuario.service';
import { MonitoreoResponse } from 'src/app/dto/response/BandejaMonitoreo/MonitoreoResponse';
import { ParticipanteRequest } from 'src/app/dto/request/BandejaEvaluacion/ParticipanteRequest';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BOpcionMenuLocalStorage } from 'src/app/dto/core/BOpcionMenuLocalStorage';
import * as _moment from 'moment';

const moment = _moment;

@Component({
  selector: 'app-bandeja.monitoreo',
  templateUrl: './bandeja.monitoreo.component.html',
  styleUrls: ['./bandeja.monitoreo.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: MY_FORMATS_AUNA
    },
    {
      provide: MatPaginatorIntl,
      useClass: forwardRef(() => MatPaginatorIntlEspanol)
    }
  ]
})

export class BandejaMonitoreoComponent implements OnInit {
  banMonitoreoFrmGrp: FormGroup = new FormGroup({
    pacienteFrmCtrl: new FormControl(null),
    estadoMonitoreoFrmCtrl: new FormControl(null),
    clinicaFrmCtrl: new FormControl(null),
    fechaMonitoreoFrmCtrl: new FormControl(null),
    responsableFrmCtrl: new FormControl(null)
  });
  get pacienteFrmCtrl() { return this.banMonitoreoFrmGrp.get('pacienteFrmCtrl'); }
  get estadoMonitoreoFrmCtrl() { return this.banMonitoreoFrmGrp.get('estadoMonitoreoFrmCtrl'); }
  get clinicaFrmCtrl() { return this.banMonitoreoFrmGrp.get('clinicaFrmCtrl'); }
  get fechaMonitoreoFrmCtrl() { return this.banMonitoreoFrmGrp.get('fechaMonitoreoFrmCtrl'); }
  get responsableFrmCtrl() { return this.banMonitoreoFrmGrp.get('responsableFrmCtrl'); }

  listaEstadosMonitoreo: Parametro[] = [];
  listaResponsableMonitoreo: any[] = [];
  listaMonitoreo: MonitoreoResponse[] = [];
  listaMonitoreoExcel = new BandejaMonitoreoRequest();
  ExportExcelBandMonitoreo: string;
  clinicaBusqueda: Clinica = new Clinica();
  pacienteBusqueda: Paciente = new Paciente();

  public isLoading: boolean;
  public columnsGrilla = [
    {
      codAcceso: ACCESO_MONITOREO.bandeja.codigoTarea,
      columnDef: 'codigoDescripcionMonitoreo',
      header: 'CODIGO TAREA MONITOREO',
      cell: (monitoreo: MonitoreoResponse) => `${monitoreo.codigoDescripcionMonitoreo}`
    }, {
      codAcceso: ACCESO_MONITOREO.bandeja.numeroSolicitud,
      columnDef: 'codigoEvaluacion',
      header: 'N° SOLICITUD EVALUACIÓN',
      cell: (monitoreo: MonitoreoResponse) => `${monitoreo.codigoEvaluacion}`
    }, {
      codAcceso: ACCESO_MONITOREO.bandeja.fechaAprobacion,
      columnDef: 'fechAprobacion',
      header: 'FECHA DE APROBACION',
      cell: (monitoreo: MonitoreoResponse) => this.datePipe.transform(monitoreo.fechAprobacion, 'dd/MM/yyyy')
    }, {
      codAcceso: ACCESO_MONITOREO.bandeja.numeroSGC,
      columnDef: 'codigoScgSolben',
      header: 'N° SCG SOLBEN',
      cell: (monitoreo: MonitoreoResponse) => `${monitoreo.codigoScgSolben}`
    },
    /*{
      codAcceso: ACCESO_MONITOREO.bandeja.medicamentoSolicitado,
      columnDef: 'nroCartaGarantia',
      header: 'N° CG SOLBEN',
      cell: (monitoreo: MonitoreoResponse) => monitoreo.nroCartaGarantia!=null?`${monitoreo.nroCartaGarantia}`:''
    },*/
    {
      codAcceso: ACCESO_MONITOREO.bandeja.medicamentoSolicitado,
      columnDef: 'nomMedicamento',
      header: 'MEDICAMENTO SOLICITADO',
      cell: (monitoreo: MonitoreoResponse) => `${monitoreo.nomMedicamento}`
    }, {
      codAcceso: ACCESO_MONITOREO.bandeja.lineaTratamiento,
      columnDef: 'numeroLineaTratamiento',
      header: 'LINEA TRATAMIENTO ACTUAL',
      cell: (monitoreo: MonitoreoResponse) => monitoreo.numeroLineaTratamiento != null ? `L${monitoreo.numeroLineaTratamiento}` : ''
    }, {
      codAcceso: ACCESO_MONITOREO.bandeja.paciente,
      columnDef: 'nombrePaciente',
      header: 'PACIENTE',
      cell: (monitoreo: MonitoreoResponse) => monitoreo.nombrePaciente != null ? `${monitoreo.nombrePaciente}` : ''
    }, {
      codAcceso: ACCESO_MONITOREO.bandeja.estado,
      columnDef: 'nomEstadoMonitoreo',
      header: 'ESTADO DE MONITOREO',
      cell: (monitoreo: MonitoreoResponse) => `${monitoreo.nomEstadoMonitoreo}`
    }, {
      codAcceso: ACCESO_MONITOREO.bandeja.fechaProgramada,
      columnDef: 'fechaProximoMonitoreo',
      header: 'FECHA PRÓXIMO MONITOREO',
      cell: (monitoreo: MonitoreoResponse) => this.datePipe.transform(monitoreo.fechaProximoMonitoreo, 'dd/MM/yyyy')
    }, {
      codAcceso: ACCESO_MONITOREO.bandeja.responsable,
      columnDef: 'nomResponsableMonitoreo',
      header: 'RESPONSABLE DE MONITOREO',
      cell: (monitoreo: MonitoreoResponse) => monitoreo.nomResponsableMonitoreo != null ? `${monitoreo.nomResponsableMonitoreo}` : ''
    }];
  public displayedColumns: string[];
  public dataSource: MatTableDataSource<MonitoreoResponse>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  opcionMenu: BOpcionMenuLocalStorage;
  txtPaciente: number;
  txtClinica: number;
  cmbResponsable: number;
  cmbEstado: number;
  txtFecha: number;
  btnBuscar: number;
  btnExportar: number;
  flagEvaluacion = FLAG_REGLAS_MONITOREO;
  valorMostrarOpcion = ACCESO_MONITOREO.mostrarOpcion;

  totalDisplayedColumns: number = 0;

  constructor(
    public dialog: MatDialog,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private bandejaMonitoreoService: BandejaMonitoreoService,
    private usuarioRolService: ListaFiltroUsuarioRolservice,
    private parametroservice: ListaParametroservice,
    private excelExportService: ExcelExportService,
    private router: Router,
    private datePipe: DatePipe,
    private spinnerService: Ng4LoadingSpinnerService,
    @Inject(UsuarioService) private userService: UsuarioService) {
    this.accesoOpcionMenu();
  }

  ngOnInit() {
    const validarIntervalo = setInterval(() => {
      if (this.userService.getCodRol) {

        this.inicializarVariables();
        this.definirTablaHistoria();
        this.iconRegistry.addSvgIcon('excel-icon', this.sanitizer.bypassSecurityTrustResourceUrl('./assets/img/icon-excel-2.svg'));

        // if (this.userService.getCodRol == ROLES.responsableMonitoreo) {
        //   this.responsableFrmCtrl.disable();
        //   this.responsableFrmCtrl.setValue(this.userService.getCodUsuario);
        // }else{
        // }
        clearInterval(validarIntervalo);
      }
    }, 100);
  }

  public inicializarVariables(): void {
    this.dataSource = null;
    this.isLoading = false;
    this.cargarComboResponsableMonit();
    this.cargarComboEstadoMonit();

    this.responsableFrmCtrl.setValue(0);
    this.banMonitoreoFrmGrp.get('fechaMonitoreoFrmCtrl').setValue(new Date());
    if (this.userService.getCodRol == ROLES.ejecutivoSeguimiento) {
      this.banMonitoreoFrmGrp.get('estadoMonitoreoFrmCtrl').setValue(ESTADO_MONITOREO.pendienteInformacion);
    } else {
      this.banMonitoreoFrmGrp.get('estadoMonitoreoFrmCtrl').setValue(ESTADO_MONITOREO.pendienteMonitoreo);
    }
    this.busquedaMonitoreo();
  }

  public definirTablaHistoria(): void {
    this.displayedColumns = [];
    this.columnsGrilla.forEach(c => {
      this.opcionMenu.opcion.forEach(element => {
        if (c.codAcceso && c.codAcceso === element.codOpcion && Number(element.flagAsignacion) === ACCESO_MONITOREO.mostrarOpcion) {
          this.displayedColumns.push(c.columnDef);
        }
      });
    });
    this.opcionMenu.opcion.forEach(element => {
      if (element.codOpcion === ACCESO_MONITOREO.bandeja.revisarDetalle) {
        this.displayedColumns.push('verDetalle');
      }
    });
    this.totalDisplayedColumns = this.displayedColumns.length;
  }

  public cargarDatosTabla(): void {
    if (this.listaMonitoreo.length > 0) {
      this.dataSource = new MatTableDataSource(this.listaMonitoreo);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  public busquedaMonitoreo(): void {
    this.isLoading = true;
    this.dataSource = null;
    this.listaMonitoreo = [];
    const request = new BandejaMonitoreoRequest();
    request.codigoPaciente = this.pacienteBusqueda.codafir;
    request.estadoMonitoreo = this.banMonitoreoFrmGrp.get('estadoMonitoreoFrmCtrl').value;
    request.codigoClinica = this.clinicaBusqueda.codcli;
    request.fechaMonitoreo = this.banMonitoreoFrmGrp.get('fechaMonitoreoFrmCtrl').value;
    request.codigoResponsableMonitoreo = this.banMonitoreoFrmGrp.get('responsableFrmCtrl').value;
    this.bandejaMonitoreoService.consultarMonitoreo(request).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.listaMonitoreo = data.data;
          this.listaMonitoreoExcel = request;
        } else {

        }
        this.cargarDatosTabla();
        this.isLoading = false;
      },
      error => {
        console.error('Error al listar monitoreo');
        this.isLoading = false;
      }
    );
  }

  public exportExcelMonitoreo(): void {
    this.spinnerService.show();
    const request = new BandejaMonitoreoRequest();
    request.codigoClinica = this.listaMonitoreoExcel.codigoClinica;
    request.codigoPaciente = this.listaMonitoreoExcel.codigoPaciente;
    request.codigoResponsableMonitoreo = this.listaMonitoreoExcel.codigoResponsableMonitoreo;
    request.estadoMonitoreo = this.listaMonitoreoExcel.estadoMonitoreo;
    request.fechaMonitoreo = this.listaMonitoreoExcel.fechaMonitoreo;
    this.excelExportService.ExportExcelBandejaMonitoreo(request).subscribe(
      (data: ExcelDownloadResponse) => {
        this.spinnerService.hide();
        this.ExportExcelBandMonitoreo = data.url;
        const urlMonitoreo = this.ExportExcelBandMonitoreo;
        const win = window.open(urlMonitoreo);
        win.blur();
      },
      error => {
        this.spinnerService.hide();
        console.error();
      }
    );
  }

  public verDetalleMonitoreo(row: MonitoreoResponse): void {
    localStorage.setItem('monitoreo', JSON.stringify(row));
    this.router.navigate(['/app/monitoreo-paciente']);
  }

  public cargarComboEstadoMonit() {
    let request = new ParametroRequest();
    request.codigoGrupo = GRUPO_PARAMETRO.estadoMonitoreo;
    this.parametroservice.consultarParametro(request).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          //SIN FILTRO
          let param = new Parametro();
          param.codigoParametro = '';
          param.nombreParametro = 'TODOS';
          this.listaEstadosMonitoreo.push(param);

          data.data.forEach(elem => {
            this.listaEstadosMonitoreo.push(elem);
          });

          //GUARDAR VARIABLE LISTA MONITOREO
          localStorage.setItem('listaEstadosMonitoreo', JSON.stringify(this.listaEstadosMonitoreo));
          //ESTABLECE DATO PENDIENTE Y LISTA MONITOREOS
          //const toSelect = this.listaEstadosMonitoreo.find(c => c.codigoParametro == 118);
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar parametros');
      }
    );
  }

  public cargarComboResponsableMonit() {
    let request = new ParticipanteRequest();
    request.codRol = ROLES.responsableMonitoreo;

    this.usuarioRolService.listarUsuarioFarmacia(request).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.listaResponsableMonitoreo = data.data;
          this.listaResponsableMonitoreo.forEach(element => {
            element.nombrecompleto = element.apellidos + ', ' + element.nombres;
          });
          this.listaResponsableMonitoreo.unshift({ "codUsuario": 0, "nombrecompleto": "TODOS" });
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar el Usuario del Rol Responsable monitoreo');
      }
    );
  }

  // POPPUP PACIENTE
  public abrirBuscarPaciente(): void {
    const dialogRef = this.dialog.open(BuscarPacienteComponent, {
      width: '640px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result: Paciente) => {
      if (result !== null) {
        this.pacienteFrmCtrl.setValue(result.apepat + ' ' + result.apemat + ', ' + result.nombr1 + ' ' + result.nombr2);
        this.pacienteBusqueda = result;
      } else {
        this.pacienteFrmCtrl.setValue(null);
        this.pacienteBusqueda = new Paciente();
      }
    });
  }

  public limpiarControl(evt: any, form: string) {
    if (form == 'pacienteFrmCtrl') {
      this.banMonitoreoFrmGrp.get(form).setValue(null);
      this.pacienteBusqueda = new Paciente();
    }

    if (form == 'clinicaFrmCtrl') {
      this.banMonitoreoFrmGrp.get(form).setValue(null);
      this.clinicaBusqueda = new Clinica();
    }
  }

  // POPPUP CLINICA
  public abrirBuscarClinica(): void {
    const dialogRef = this.dialog.open(BuscarClinicaComponent, {
      width: '640px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result: Clinica) => {
      if (result !== null) {
        this.clinicaFrmCtrl.setValue(result.nomcli);
        this.clinicaBusqueda = result;
      } else {
        this.clinicaFrmCtrl.setValue(null);
        this.clinicaBusqueda = new Clinica();
      }
    });
  }

  public accesoOpcionMenu() {

    const data = require('src/assets/data/permisosRecursos.json');
    const bandejaMonitoreo = data.bandejaMonitoreo.busqueda;
    this.opcionMenu = JSON.parse(localStorage.getItem('opcionMenu'));

    if (this.opcionMenu.opcion.length > 0) {
      this.opcionMenu.opcion.forEach(element => {
        const codOpcion = element.codOpcion;
        switch (codOpcion) {
          case bandejaMonitoreo.txtPaciente:
            this.txtPaciente = Number(element.flagAsignacion);
            break;
          case bandejaMonitoreo.txtClinica:
            this.txtClinica = Number(element.flagAsignacion);
            break;
          case bandejaMonitoreo.cmbResponsable:
            this.cmbResponsable = Number(element.flagAsignacion);
            break;
          case bandejaMonitoreo.cmbEstado:
            this.cmbEstado = Number(element.flagAsignacion);
            break;
          case bandejaMonitoreo.txtFecha:
            this.txtFecha = Number(element.flagAsignacion);
            break;
          case bandejaMonitoreo.btnBuscar:
            this.btnBuscar = Number(element.flagAsignacion);
            break;
          case bandejaMonitoreo.btnExportar:
            this.btnExportar = Number(element.flagAsignacion);
            break;
        }
      });
    }
  }

}
