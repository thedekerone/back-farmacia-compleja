import { Component, AfterViewInit, ViewChild, forwardRef } from '@angular/core';
import { PAG_SIZ_SMALL, PAG_OBT_SMALL, MENSAJES, MY_FORMATS_AUNA } from 'src/app/common';
import { MatPaginator, MatDialog, MatTableDataSource, DateAdapter, MAT_DATE_FORMATS, MatPaginatorIntl, MAT_DATE_LOCALE, MatSort } from '@angular/material';
import { MarcadorService } from 'src/app/service/Configuracion/marcador.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatProgressButtonOptions } from 'mat-progress-buttons';
import { ConfiguracionService } from 'src/app/service/configuracion.service';
import { MessageComponent } from 'src/app/core/message/message.component';
import { WsResponse } from 'src/app/dto/WsResponse';
import { ExamenMedico } from 'src/app/dto/ExamenMedico';
import { RegistrarParticipanteComponent } from './registrar/registrar.component';
import { Participante } from 'src/app/dto/Participante';
import { EditarParticipanteComponent } from './editar/editar.component';
import { MatPaginatorIntlEspanol } from 'src/app/directives/matpaginator-translate';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
// import { RegistrarExamenMedicoComponent } from './registrar/registrar.component';
// import { EditarExamenMedicoComponent } from './editar/editar.component';

@Component({
  selector: 'app-participantes',
  templateUrl: './participantes.component.html',
  styleUrls: ['./participantes.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS_AUNA },
    { provide: MatPaginatorIntl, useClass: forwardRef(() => MatPaginatorIntlEspanol) }
  ]
})
export class ParticipantesComponent implements AfterViewInit {

  // TABLA
  pageSize:number = PAG_SIZ_SMALL;
  pageSizeOptions:number[] = PAG_OBT_SMALL;

  displayedColumns: string[] = ['codParticipanteLargo', 'apellidos', 'descripcionRol', 'correoElectronico',
                                'estadoParticipante', 'opciones'];
  //dataSource: Marcador[] = [];
  dataSource: MatTableDataSource<Participante> = new MatTableDataSource([]);

  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  // FORMULARIO
  marcadorForm: FormGroup;
  marcadorSubmitted = false;
  marcadorFormMessages = {};
  marcadorBtnOpts: MatProgressButtonOptions = {
    active: false,
    text: 'BUSCAR',
    spinnerSize: 19,
    raised: true,
    stroked: false,
    buttonColor: 'primary',
    spinnerColor: 'accent',
    fullWidth: false,
    disabled: false,
    mode: 'indeterminate',
  };

  spinnerGrupo:boolean = true;
  listaGrupo: any[] = [];


  marcadorRequest: ExamenMedico = new ExamenMedico();

  constructor(
    //public dialogRef: MatDialogRef<ExamenesMedicosComponent>,
    //@Inject(MAT_DIALOG_DATA) public data: DialogMarcadoresData,
    public dialog: MatDialog,
    private marcadorService: MarcadorService,
    public confService: ConfiguracionService
  ) {
    this.marcadorForm = new FormGroup({
      'nombre': new FormControl(null, [])
    });
    this.cargarParametro();
  }

  ngAfterViewInit(): void { }

  get fc() { return this.marcadorForm.controls; }

  /*public onClose(): void {
    this.dialogRef.close(null);
  }*/

  public cargarParametro(): void {
    this.marcadorBtnOpts.active = true;

    this.isLoadingResults = true;

    this.dataSource = null;

    var participanteRequest: Participante = new Participante();
    participanteRequest.apellidos = this.marcadorForm.get('nombre').value;

    this.marcadorService.
    buscarParticipantes(participanteRequest)
    .subscribe(
      (respuesta: WsResponse) => {
        this.marcadorBtnOpts.active = false;
        this.isLoadingResults = false;
        if (respuesta.audiResponse.codigoRespuesta != '0'){
          console.error('Error al listar Participante');
          this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al listar Participante', true, false, null);
        }else{
          //this.dataSource.data = respuesta.data;
          this.dataSource = new MatTableDataSource(respuesta.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.resultsLength = respuesta.data.length;
        }
      },
      error => {
        this.marcadorBtnOpts.active = false;
        this.isLoadingResults = false;
        console.error(error);
        this.openDialogMensaje(MENSAJES.ERROR_SERVICIO, 'Error al listar Participante', true, false, null);
      }
    );
  }

  public openDialogMensaje(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any
  ): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.CONF.PARTICIPANTE_SISTEMA,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });
    dialogRef.afterClosed().subscribe(result => {

    });
  }

  accionMarcador() {
    this.marcadorSubmitted = true;

    if (this.marcadorForm.invalid) {
      return;
    }

    this.cargarParametro();
  }

  openRegistrarMarcador(): void {
    const dialogRef = this.dialog.open(RegistrarParticipanteComponent, {
      width: '640px',
      disableClose: true,
      autoFocus: false,
      data: { }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.accionMarcador();
      }
    });
  }

  openEditarMarcador(marcador): void {
    const dialogRef = this.dialog.open(EditarParticipanteComponent, {
      width: '640px',
      disableClose: true,
      autoFocus: false,
      data: {
        participante: marcador
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.accionMarcador();
      }
    });
  }

}
