import { Component, OnInit, Inject } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatDialog } from '@angular/material';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from '@angular/material/core';
import * as _moment from 'moment';
import { Router } from '@angular/router';
import { MessageComponent } from 'src/app/core/message/message.component';
import { MENSAJES, MY_FORMATS_AUNA, ACCESO_EVALUACION, FLAG_REGLAS_EVALUACION } from './../../common';

// request - response

import { ParametroRequest } from '../../dto/ParametroRequest';
import { ParametroResponse } from '../../dto/ParametroResponse';
import { RegistroHistLineaTratRequest } from '../../dto/solicitudEvaluacion/detalle/RegistroHistLineaTratRequest';
import { InsertLineaTratamientoResponse } from '../../dto/response/insertaLineaTratamientoResponse';

// service
import { ListaParametroservice } from '../../service/lista.parametro.service';
import { DetalleSolicitudEvaluacionService } from '../../service/detalle.solicitud.evaluacion.service';
import { Parametro } from 'src/app/dto/Parametro';
import { DatePipe } from '@angular/common';
import { WsResponse } from 'src/app/dto/WsResponse';
import { BuscarMacComponent } from 'src/app/modal/buscar-mac/buscar-mac.component';
import { MACResponse } from 'src/app/dto/configuracion/MACResponse';
import { DiagnosticoService } from 'src/app/dto/service/diagnostico.service';
import { EvaluacionService } from 'src/app/dto/service/evaluacion.service';
import { PacienteService } from 'src/app/dto/service/paciente.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BOpcionMenuLocalStorage } from 'src/app/dto/core/BOpcionMenuLocalStorage';
@Component({
  selector: 'app-registro-linea-tratamiento',
  templateUrl: './registro.linea.tratamiento.component.html',
  styleUrls: ['./registro.linea.tratamiento.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: MY_FORMATS_AUNA
    }
  ]
})

export class RegistroLineaTratamientoComponent implements OnInit {
  public registroFrmGroup: FormGroup;
  public activarBtn: boolean;
  public activarBtnLinea: boolean;
  public activarBtnSalir: boolean;
  public registraLinea: number;
  public activarBtnRespuesta: boolean;

  ParametroRequest: ParametroRequest = new ParametroRequest();
  ParametroResponse: ParametroResponse = new ParametroResponse();
  nuevaLineaTratRequest: RegistroHistLineaTratRequest;

  listaMotivoInactivacion: Parametro[];
  listaTipoTumor: Parametro[];
  listaNroCurso: Parametro[];
  listaRespuestAlcanzada: Parametro[];
  listaLugarProgresion: Parametro[];
  codMac: number;
  codLargoMac: string;
  diagnosticoTxt: string;
  pacienteTxt: string;

  opcionMenu: BOpcionMenuLocalStorage;
  txtPacienteReg: number;
  txtDiagnosticoReg: number;
  txtMedicamento: number;
  txtFechaInicio: number;
  txtFechaFin: number;
  cmbMotivoInactivacion: number;
  txtFechaInactivacion: number;
  cmbNumeroCursos: number;
  cmbTipoTumor: number;
  cmbRespuestaAlcanzada: number;
  cmbLugarProgresion: number;
  txtCMPReg: number;
  txtApellidosNombres: number;
  btnGrabar: number;
  btnSalir: number;
  btnRegistrar: number;
  flagEvaluacion = FLAG_REGLAS_EVALUACION;
  valorMostrarOpcion = ACCESO_EVALUACION.mostrarOpcion;

  constructor(
    private adapter: DateAdapter<any>,
    public dialog: MatDialog,
    private listaParametroservice: ListaParametroservice,
    private detalleSolicitudEvaluacionService: DetalleSolicitudEvaluacionService,
    private router: Router,
    private datePipe: DatePipe,
    private spinnerService: Ng4LoadingSpinnerService,
    @Inject(EvaluacionService) private solicitud: EvaluacionService
  ) {
    this.adapter.setLocale('es-PE');
    this.accesoOpcionMenu();
  }

  ngOnInit() {
    this.inicializarVariables();
    this.configurarForm();
    this.cargarCombos();
  }

  public inicializarVariables(): void {
    this.listaMotivoInactivacion = [];
    this.listaTipoTumor = [];
    this.nuevaLineaTratRequest = new RegistroHistLineaTratRequest();
    this.registraLinea = 0;
    this.activarBtn = false;
    this.activarBtnSalir = true;

    this.diagnosticoTxt = this.solicitud.codDiagnostico +
    ' - ' + this.solicitud.descDiagnostico;
    this.pacienteTxt = this.solicitud.paciente;
  }

  public cargarCombos() {
    this.listarMotivoInactivacion();
    this.listarTipoTumor();
    this.listarNroCurso();
    this.listarRespuestAlcanzada();
    this.listarLugarProgresion();
    this.motivoInactivoFrmCtrl.setValue(0);
    this.nroCursosFrmCtrl.setValue(0);
    this.tipoTumorFrmCtrl.setValue(0);
    this.rptaAlcanzadaFrmCtrl.setValue(0);
    this.lugarRecuRecaFrmCtrl.setValue(0);
    this.pacienteFrmCtrl.setValue(this.solicitud.paciente);
    this.diagnosticoFrmCtrl.setValue(this.diagnosticoTxt);
    this.pacienteFrmCtrl.setValue(this.pacienteTxt);
  }

  public configurarForm(): void {
    this.registroFrmGroup = new FormGroup({
      pacienteFrmCtrl: new FormControl(null),
      diagnosticoFrmCtrl: new FormControl(null),
      medicamentoFrmCtrl: new FormControl(null, Validators.required),
      fechaFormControl: new FormControl(null, [Validators.required]),
      fechaFinFormControl: new FormControl(null, [Validators.required]),
      motivoInactivoFrmCtrl: new FormControl(null),
      fechaInactivaFrmCtrl: new FormControl(null),
      nroCursosFrmCtrl: new FormControl(null),
      tipoTumorFrmCtrl: new FormControl(null),
      rptaAlcanzadaFrmCtrl: new FormControl(null),
      lugarRecuRecaFrmCtrl: new FormControl(null),
      medicoTrataFrmCtrl: new FormControl(null, [Validators.required]),
      codigoCMPFrmCtrl: new FormControl(null)
    });
  }

  get pacienteFrmCtrl() { return this.registroFrmGroup.get('pacienteFrmCtrl'); }
  get diagnosticoFrmCtrl() { return this.registroFrmGroup.get('diagnosticoFrmCtrl'); }
  get medicamentoFrmCtrl() { return this.registroFrmGroup.get('medicamentoFrmCtrl'); }
  get fechaFormControl() { return this.registroFrmGroup.get('fechaFormControl'); }
  get fechaFinFormControl() { return this.registroFrmGroup.get('fechaFinFormControl'); }
  get motivoInactivoFrmCtrl() { return this.registroFrmGroup.get('motivoInactivoFrmCtrl'); }
  get fechaInactivaFrmCtrl() { return this.registroFrmGroup.get('fechaInactivaFrmCtrl'); }
  get nroCursosFrmCtrl() { return this.registroFrmGroup.get('nroCursosFrmCtrl'); }
  get tipoTumorFrmCtrl() { return this.registroFrmGroup.get('tipoTumorFrmCtrl'); }
  get rptaAlcanzadaFrmCtrl() { return this.registroFrmGroup.get('rptaAlcanzadaFrmCtrl'); }
  get lugarRecuRecaFrmCtrl() { return this.registroFrmGroup.get('lugarRecuRecaFrmCtrl'); }
  get medicoTrataFrmCtrl() { return this.registroFrmGroup.get('medicoTrataFrmCtrl'); }
  get codigoCMPFrmCtrl() { return this.registroFrmGroup.get('codigoCMPFrmCtrl'); }

  public obtenerCombo(lista: any[], valor: number, descripcion: string): void {
    lista.unshift({
      codigoParametro: valor,
      nombreParametro: descripcion
    });
  }

  public listarMotivoInactivacion(): void {
    this.ParametroRequest.codigoGrupo = '43';
    this.listaParametroservice.listaParametro(this.ParametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 1) {
          console.error('No se encuentra data con el codigo de grupo');
        } else {
          this.listaMotivoInactivacion = data.filtroParametro;
          this.obtenerCombo(this.listaMotivoInactivacion, 0, '-- NO REGISTRADO --');
          this.fechaFormControl.setValue(new Date());
          this.fechaFinFormControl.setValue(new Date());
          this.fechaInactivaFrmCtrl.setValue(new Date());
        }
      },
      error => {
        console.error('Error al listar el Motivo de Inactivacion');
      }
    );
  }

  public listarNroCurso(): void {
    this.ParametroRequest.codigoGrupo = '33';
    this.listaParametroservice.listaParametro(this.ParametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 1) {
          console.error('No se encuentra data con el codigo de grupo');
        } else {
          this.listaNroCurso = data.filtroParametro;
          this.obtenerCombo(this.listaNroCurso, 0, '-- NO REGISTRADO --');
        }
      },
      error => {
        console.error('Error al listar Nro de Grupo');
      }
    );
  }

  public listarTipoTumor(): void {
    this.ParametroRequest.codigoGrupo = '34';
    this.listaParametroservice.listaParametro(this.ParametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 1) {
          console.error('No se encuentra data con el codigo de grupo');
        } else {
          this.listaTipoTumor = data.filtroParametro;
          this.obtenerCombo(this.listaTipoTumor, 0, '-- NO REGISTRADO --');
        }
      },
      error => {
        console.error('Error al listar el Tipo de Tumor');
      }
    );
  }

  public CmbLider(event: Event) {
    this.listaRespuestAlcanzada = [];
    this.ParametroRequest.codigoParam = this.tipoTumorFrmCtrl.value;
    this.listarRespuestAlcanzada();
  }

  public listarRespuestAlcanzada() {
    this.ParametroRequest.codigoGrupo = '36';
    this.listaParametroservice.consultarParametro(this.ParametroRequest).subscribe(
      (data: WsResponse) => {
        if (data.audiResponse.codigoRespuesta === '0') {
          this.listaRespuestAlcanzada = data.data;
          this.obtenerCombo(this.listaRespuestAlcanzada, 0, '-- NO REGISTRADO --');
          // this.activarBtnRespuesta =true;
          if (this.tipoTumorFrmCtrl.value === 0) {
            this.activarBtnRespuesta = true;
          } else {
            this.activarBtnRespuesta = false;
          }
        } else {
          console.error(data);
        }
      },
      error => {
        console.error('Error al listar parametros');
      }
    );
  }

  public listarLugarProgresion() {
    this.ParametroRequest.codigoGrupo = '35';
    this.listaParametroservice.listaParametro(this.ParametroRequest).subscribe(
      (data: ParametroResponse) => {
        if (data.codigoResultado === 1) {
          console.error('No se encuentra data con el codigo de grupo');
        } else {
          this.listaLugarProgresion = data.filtroParametro;
          this.obtenerCombo(this.listaLugarProgresion, 0, '-- NO REGISTRADO --');
        }
      },
      error => {
        console.error('Error al listar Nro de Grupo');
      }
    );
  }

  public registrarLineaTratamiento() {
    this.grupoBotones(true);

    if (this.registroFrmGroup.invalid) {
      this.medicamentoFrmCtrl.markAsTouched();
      this.fechaFormControl.markAsTouched();
      this.fechaFinFormControl.markAsTouched();
      this.medicoTrataFrmCtrl.markAsTouched();
      this.grupoBotones(false);
      this.openDialogMensaje('Validar campos requeridos', null, true, false, null, null);
      return;
    }

    this.guardarParametrosRequest();
    this.spinnerService.show();
    this.detalleSolicitudEvaluacionService
      .insertarHistLineaTrat(this.nuevaLineaTratRequest)
      .subscribe(
        (data: InsertLineaTratamientoResponse) => {
          if (data.codResultado === -2) {
            this.openDialogMensaje(data.msgResultado, data.msgResultado, true, false, null, null);
            this.spinnerService.hide();
          } else if (data.codResultado === 0) {
            this.openDialogMensaje(MENSAJES.INFO_ACEPTAR, data.msgResultado, true, false, null, null);
            this.activarBtnSalir = false;
            this.activarBtnLinea = true;
            this.spinnerService.hide();
          } else {
            this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, data.msgResultado, true, false, null, null);
            this.spinnerService.hide();
          }
          this.grupoBotones(false);
        },
        error => {
          console.error('Error al registrar Linea de Tratamiento');
          this.grupoBotones(false);
          this.openDialogMensaje(MENSAJES.ERROR_NOFUNCION, null, true, false, null, null);
          this.spinnerService.hide();
        }
      );
  }

  public guardarParametrosRequest(): void {
    this.nuevaLineaTratRequest.codAfiliado = this.solicitud.codAfiliado;
    this.nuevaLineaTratRequest.codSolicitudEvaluacion = this.solicitud.codSolEvaluacion;
    this.nuevaLineaTratRequest.fechaInicio = this.datePipe.transform(this.fechaFormControl.value, 'dd/MM/yyyy');
    this.nuevaLineaTratRequest.fechaFin = this.datePipe.transform(this.fechaFinFormControl.value, 'dd/MM/yyyy');
    this.nuevaLineaTratRequest.fechaInactivacion = this.datePipe.transform(this.fechaInactivaFrmCtrl.value, 'dd/MM/yyyy');
    this.nuevaLineaTratRequest.motivoInactivacion = this.motivoInactivoFrmCtrl.value;
    this.nuevaLineaTratRequest.numeroCurso = this.nroCursosFrmCtrl.value;
    this.nuevaLineaTratRequest.tipoTumor = this.tipoTumorFrmCtrl.value;
    this.nuevaLineaTratRequest.respuestaAlcanzada = this.rptaAlcanzadaFrmCtrl.value;
    this.nuevaLineaTratRequest.lugarProgresion = this.lugarRecuRecaFrmCtrl.value;
    this.nuevaLineaTratRequest.codigoMedicoTratante = this.codigoCMPFrmCtrl.value;
    this.nuevaLineaTratRequest.medicoTratante = this.medicoTrataFrmCtrl.value;

    //COD MAC
    this.nuevaLineaTratRequest.codMac = this.codMac;
  }

  // BUSQUEDA MEDICAMENTO MAC
  public openDialogMAC($event: Event): void {
    $event.preventDefault();
    this.medicamentoFrmCtrl.markAsUntouched();
    const dialogRef = this.dialog.open(BuscarMacComponent, {
      width: '500px',
      disableClose: true,
      data: {
        title: 'BÚSQUEDA MAC'
      }
    });

    dialogRef.afterClosed().subscribe((result: MACResponse) => {
      if (result != null) {
        this.codMac = result.codigo;
        this.codLargoMac = result.codigoLargo;
        this.medicamentoFrmCtrl.setValue(result.descripcion);
      }
    });
  }

  // POP-UP MENSAJES
  public openDialogMensaje(
    message: string,
    message2: string,
    alerta: boolean,
    confirmacion: boolean,
    valor: any,
    tipo: string
  ): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      width: '400px',
      disableClose: true,
      data: {
        title: MENSAJES.TITLE_NUEVA_LINEA,
        message: message,
        message2: message2,
        alerta: alerta,
        confirmacion: confirmacion,
        valor: valor
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (tipo === 'salir' && result === 1) {
        this.router.navigate(['./app/detalle-evaluacion']);
      } else if (tipo === 'otraLinea' && result === 0) {
        this.router.navigate(['./app/detalle-evaluacion']);
      } else {
        return;
      }
    });
  }

  resgistraOtraLineaTratamiento($event: Event) {
    // this.registraLinea = 2;
    // this.registrarLineaTratamiento();
    $event.preventDefault();
    this.limpiarControles();
    // this.fechaFormControl.markAsUntouched();
    // this.fechaFinFormControl.markAsUntouched();
    // this.fechaInactivaFrmCtrl.markAsUntouched();
    this.fechaFormControl.setValue(new Date());
    this.fechaFinFormControl.setValue(new Date());
    this.fechaInactivaFrmCtrl.setValue(new Date());
    this.medicamentoFrmCtrl.markAsUntouched();
    this.medicoTrataFrmCtrl.markAsUntouched();
    this.activarBtnLinea = false;
    this.activarBtnSalir = true;
  }

  public salirForm() {
    this.openDialogMensaje(MENSAJES.INFO_SALIR, MENSAJES.INFO_SALIR2, false, true, null, 'salir');
  }

  public validarFechaInicio() {
    const dateInicio = this.fechaFormControl.value;
    const dateFin = this.fechaFinFormControl.value;
    const dateActual = new Date();
    if (this.fechaFinFormControl.value !== null) {
      if (dateInicio > dateFin) {
        this.openDialogMensaje('Fecha inicial debe ser menor que la fecha final', null, true, false, null, null);
        this.fechaFormControl.setValue(null);
      } else if (dateInicio === dateFin) {
        this.openDialogMensaje('Fecha inicial debe ser distinta a la fecha final', null, true, false, null, null);
        this.fechaFormControl.setValue(null);
      } else if (dateInicio > dateActual) {
        this.openDialogMensaje('Fecha no debe de ser mayor al actual', null, true, false, null, null);
        this.fechaFormControl.setValue(null);
      }
    }
  }

  public validarFechaFin() {
    const dateInicio = this.fechaFormControl.value;
    const dateFin = this.fechaFinFormControl.value;
    const dateActual = new Date();
    if (this.fechaFormControl.value !== null) {
      if (dateInicio > dateFin) {
        this.openDialogMensaje('Fecha final debe ser mayor a la fecha inicial', null, true, false, null, null);
        this.fechaFinFormControl.setValue(null);
      } else if (dateInicio === dateFin) {
        this.openDialogMensaje('Fecha final debe ser distinta a la fecha inicial', null, true, false, null, null);
        this.fechaFinFormControl.setValue(null);
      } else if (dateFin > dateActual) {
        this.openDialogMensaje('Fecha no debe de ser mayor al actual', null, true, false, null, null);
        this.fechaFinFormControl.setValue(null);
      }
    }
  }

  public validarFechaInactivacion() {
    const dateInactiva = this.fechaInactivaFrmCtrl.value;
    const dateActual = new Date();
    if (dateInactiva > dateActual) {
      this.openDialogMensaje('Fecha no debe de ser mayor al actual', null, true, false, null, null);
      this.fechaInactivaFrmCtrl.setValue(null);
    }
  }

  public grupoBotones(activar: boolean) {
    this.activarBtn = activar;
  }

  public limpiarControles(): void {
    this.medicamentoFrmCtrl.setValue(null);
    this.fechaFormControl.setValue(null);
    this.fechaFinFormControl.setValue(null);
    this.motivoInactivoFrmCtrl.setValue(null);
    this.fechaInactivaFrmCtrl.setValue(null);
    this.nroCursosFrmCtrl.setValue(null);
    this.tipoTumorFrmCtrl.setValue(null);
    this.rptaAlcanzadaFrmCtrl.setValue(null);
    this.lugarRecuRecaFrmCtrl.setValue(null);
    this.codigoCMPFrmCtrl.setValue(null);
    this.medicoTrataFrmCtrl.setValue(null);
  }

  public accesoOpcionMenu() {

    const data = require('src/assets/data/permisosRecursos.json');
    const bandejaEvaluacion = data.bandejaEvaluacion.registrar;
    this.opcionMenu = JSON.parse(localStorage.getItem('opcionMenu'));

    if (this.opcionMenu.opcion.length > 0) {
      this.opcionMenu.opcion.forEach(element => {
        const codOpcion = element.codOpcion;
        switch (codOpcion) {
          case bandejaEvaluacion.txtPacienteReg:
                this.txtPacienteReg = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.txtDiagnosticoReg:
                this.txtDiagnosticoReg = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.txtMedicamento:
                this.txtMedicamento = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.txtFechaInicio:
                this.txtFechaInicio = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.txtFechaFin:
                this.txtFechaFin = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.cmbMotivoInactivacion:
                this.cmbMotivoInactivacion = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.txtFechaInactivacion:
                this.txtFechaInactivacion = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.cmbNumeroCursos:
                this.cmbNumeroCursos = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.cmbTipoTumor:
                this.cmbTipoTumor = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.cmbRespuestaAlcanzada:
                this.cmbRespuestaAlcanzada = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.cmbLugarProgresion:
                this.cmbLugarProgresion = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.txtCMPReg:
                this.txtCMPReg = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.txtApellidosNombres:
                this.txtApellidosNombres = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.btnGrabar:
                this.btnGrabar = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.btnSalir:
                this.btnSalir = Number(element.flagAsignacion);
                break;
          case bandejaEvaluacion.btnRegistrar:
                this.btnRegistrar = Number(element.flagAsignacion);
                break;
        }
      });
    }
  }
}
